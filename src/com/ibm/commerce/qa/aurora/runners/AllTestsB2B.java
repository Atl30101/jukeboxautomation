package com.ibm.commerce.qa.aurora.runners;
/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 * 
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import junit.framework.JUnit4TestAdapter;
import junit.framework.TestSuite;

import com.ibm.commerce.qa.aurorab2b.tests.FB2BSTOREB2BA_01;
import com.ibm.commerce.qa.aurorab2b.tests.FB2BSTOREB2BA_03;
import com.ibm.commerce.qa.aurorab2b.tests.FB2BSTOREB2BA_04;
import com.ibm.commerce.qa.aurorab2b.tests.FB2BSTOREB2BA_05;
import com.ibm.commerce.qa.aurorab2b.tests.FB2BSTOREB2BA_06;
import com.ibm.commerce.qa.aurorab2b.tests.FB2BSTOREB2BA_07;
import com.ibm.commerce.qa.aurorab2b.tests.FB2BSTOREB2BA_08;
import com.ibm.commerce.qa.aurorab2b.tests.FB2BSTOREB2BA_10;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_00;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_04;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_06;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_07;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_09_01;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_09_08;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_10;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_11;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_12;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_13;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_14;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_16;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_18;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_21;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_24;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_28;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_29;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_31;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_36;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_41;
import com.ibm.commerce.qa.aurorab2b.tests.FV2STOREB2B_49;

/**
 * a junit class that runs all other tests in this bucket
 * except:
 * Scenario 49, is used as a sample only. should not be run
 * Scenario 30, involves session timeout testing, and takes 2.5 hours to run
 *
 */
public class AllTestsB2B{
    /**
     * adds all tests to a junit bucket
     * @return junit suite of tests
     */
    public static junit.framework.Test suite(){
        TestSuite suite = new TestSuite();
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_00.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_10.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_11.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_12.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_13.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_09_01.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_09_08.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_14.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_16.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_18.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_21.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_24.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_28.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_29.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_31.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_36.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_41.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_49.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_04.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_06.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2B_07.class));
        suite.addTest(new JUnit4TestAdapter(FB2BSTOREB2BA_01.class));
        suite.addTest(new JUnit4TestAdapter(FB2BSTOREB2BA_03.class));
        suite.addTest(new JUnit4TestAdapter(FB2BSTOREB2BA_04.class));
        suite.addTest(new JUnit4TestAdapter(FB2BSTOREB2BA_05.class));
        suite.addTest(new JUnit4TestAdapter(FB2BSTOREB2BA_06.class));
        suite.addTest(new JUnit4TestAdapter(FB2BSTOREB2BA_07.class));
        suite.addTest(new JUnit4TestAdapter(FB2BSTOREB2BA_08.class));
        suite.addTest(new JUnit4TestAdapter(FB2BSTOREB2BA_10.class));
       
        
        return suite;
    }
}