package com.ibm.commerce.qa.aurora.runners;
/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 * 
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import junit.framework.JUnit4TestAdapter;
import junit.framework.TestSuite;

import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_00;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_01;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_02;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_04;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_05;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_07;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_07b;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_08;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_09;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_10;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_11;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_12;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_13;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_14;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_14b;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_14c;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_15;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_16;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_17;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_18;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_18b;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_19;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_20;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_21;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_26;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_28;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_29;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_31;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_32;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_33;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_35;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_36;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_37;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_38;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_39;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_41;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_42;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_43;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_50;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_51;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_52;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_53;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_54;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_55;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_65;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_72;

/**
 * a junit class that runs all other tests in this bucket
 * except:
 * Scenario 49, is used as a sample only. should not be run
 * Scenario 30, involves session timeout testing, and takes 2.5 hours to run
 *
 */
public class AllTests{
    /**
     * adds all tests to a junit bucket
     * @return junit suite of tests
     */
    public static junit.framework.Test suite(){
        TestSuite suite = new TestSuite();
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_00.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_01.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_02.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_04.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_05.class));
        // suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_06.class)); //requires specific languages to be installed
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_07.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_07b.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_08.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_09.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_10.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_11.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_12.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_13.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_14.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_14b.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_14c.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_15.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_16.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_17.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_18.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_18b.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_19.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_20.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_21.class));
        //suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_25.class));   //This test case requires additional solr configuration
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_26.class));
        
        //suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_27.class));  // This test case requires cache to be disabled.
        
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_28.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_29.class));
        
        //suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_30.class));  //these are timeout testcases which take longer to run, enable them when desired
        
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_31.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_32.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_33.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_35.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_36.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_37.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_38.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_39.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_41.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_42.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_43.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_50.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_51.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_52.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_53.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_54.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_55.class));
//        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_58.class));
//        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_59.class));
//        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_60.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_65.class));
        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_72.class));
        //suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_73.class)); //Saravanan needs to add setup and tearDown() steps.
//        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_SOLR_SEARCH_01.class));
//        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_SOLR_SEARCH_02.class));
//        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_SOLR_SEARCH_04.class));
//        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_SOLR_SEARCH_05.class));
//        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_SOLR_SEARCH_06.class));
//        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_SOLR_SEARCH_09.class));
//        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_SOLR_SEARCH_12.class));
//        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_SOLR_SEARCH_13.class));
//        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_SOLR_SEARCH_15.class));
//        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_SOLR_SEARCH_16.class));
//        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_SOLR_SEARCH_17.class));
//        suite.addTest(new JUnit4TestAdapter(FV2STOREB2C_SOLR_SEARCH_18.class));
        
        return suite;
    }
}