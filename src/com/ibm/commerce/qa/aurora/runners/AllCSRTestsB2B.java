package com.ibm.commerce.qa.aurora.runners;
/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 * 
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import junit.framework.JUnit4TestAdapter;
import junit.framework.TestSuite;

import com.ibm.commerce.qa.aurorab2b.tests.FSTOREB2BCSR_00;
import com.ibm.commerce.qa.aurorab2b.tests.FSTOREB2BCSR_01;
import com.ibm.commerce.qa.aurorab2b.tests.FSTOREB2BCSR_02;
import com.ibm.commerce.qa.aurorab2b.tests.FSTOREB2BCSR_03;
import com.ibm.commerce.qa.aurorab2b.tests.FSTOREB2BCSR_04;
import com.ibm.commerce.qa.aurorab2b.tests.FSTOREB2BCSR_13;
import com.ibm.commerce.qa.aurorab2b.tests.FSTOREB2BCSR_14;
import com.ibm.commerce.qa.aurorab2b.tests.FSTOREB2BCSR_20;
import com.ibm.commerce.qa.aurorab2b.tests.FSTOREB2BCSR_21;


/**
 * a junit class that runs all other tests in this bucket
 * except:
 * Scenario 49, is used as a sample only. should not be run
 * Scenario 30, involves session timeout testing, and takes 2.5 hours to run
 *
 */
public class AllCSRTestsB2B{
    /**
     * adds all tests to a junit bucket
     * @return junit suite of tests
     */
    public static junit.framework.Test suite(){
        TestSuite suite = new TestSuite();
        suite.addTest(new JUnit4TestAdapter(FSTOREB2BCSR_00.class));
        suite.addTest(new JUnit4TestAdapter(FSTOREB2BCSR_01.class));
        suite.addTest(new JUnit4TestAdapter(FSTOREB2BCSR_02.class));
        suite.addTest(new JUnit4TestAdapter(FSTOREB2BCSR_03.class));
        suite.addTest(new JUnit4TestAdapter(FSTOREB2BCSR_04.class));
        suite.addTest(new JUnit4TestAdapter(FSTOREB2BCSR_13.class));
        suite.addTest(new JUnit4TestAdapter(FSTOREB2BCSR_14.class));
        suite.addTest(new JUnit4TestAdapter(FSTOREB2BCSR_20.class));  
        suite.addTest(new JUnit4TestAdapter(FSTOREB2BCSR_21.class)); 


        return suite;
    }
}