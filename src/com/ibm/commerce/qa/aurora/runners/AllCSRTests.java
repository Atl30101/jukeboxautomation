package com.ibm.commerce.qa.aurora.runners;
/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 * 
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import junit.framework.JUnit4TestAdapter;
import junit.framework.TestSuite;

import com.ibm.commerce.qa.aurora.tests.FSTOREB2CCSR_00;
import com.ibm.commerce.qa.aurora.tests.FSTOREB2CCSR_01;
import com.ibm.commerce.qa.aurora.tests.FSTOREB2CCSR_02;
import com.ibm.commerce.qa.aurora.tests.FSTOREB2CCSR_03;
import com.ibm.commerce.qa.aurora.tests.FSTOREB2CCSR_04;
import com.ibm.commerce.qa.aurora.tests.FSTOREB2CCSR_05;
import com.ibm.commerce.qa.aurora.tests.FSTOREB2CCSR_06;
import com.ibm.commerce.qa.aurora.tests.FSTOREB2CCSR_07;
import com.ibm.commerce.qa.aurora.tests.FSTOREB2CCSR_13;
import com.ibm.commerce.qa.aurora.tests.FSTOREB2CCSR_14;
import com.ibm.commerce.qa.aurora.tests.FSTOREB2CCSR_21;
import com.ibm.commerce.qa.aurora.tests.FSTOREB2CCSR_22;
import com.ibm.commerce.qa.aurora.tests.FSTOREB2CCSR_23;
import com.ibm.commerce.qa.aurora.tests.FSTOREB2CCSR_24;


/**
 * a junit class that runs all other tests in this bucket
 * except:
 * Scenario 49, is used as a sample only. should not be run
 * Scenario 30, involves session timeout testing, and takes 2.5 hours to run
 *
 */
public class AllCSRTests{
    /**
     * adds all tests to a junit bucket
     * @return junit suite of tests
     */
    public static junit.framework.Test suite(){
        TestSuite suite = new TestSuite();
        suite.addTest(new JUnit4TestAdapter(FSTOREB2CCSR_00.class));
        suite.addTest(new JUnit4TestAdapter(FSTOREB2CCSR_01.class));
        suite.addTest(new JUnit4TestAdapter(FSTOREB2CCSR_02.class));
        suite.addTest(new JUnit4TestAdapter(FSTOREB2CCSR_03.class));
        suite.addTest(new JUnit4TestAdapter(FSTOREB2CCSR_04.class));
        suite.addTest(new JUnit4TestAdapter(FSTOREB2CCSR_05.class));
        suite.addTest(new JUnit4TestAdapter(FSTOREB2CCSR_06.class));     
        suite.addTest(new JUnit4TestAdapter(FSTOREB2CCSR_07.class)); 
        suite.addTest(new JUnit4TestAdapter(FSTOREB2CCSR_13.class)); 
        suite.addTest(new JUnit4TestAdapter(FSTOREB2CCSR_14.class));
        suite.addTest(new JUnit4TestAdapter(FSTOREB2CCSR_21.class));
        suite.addTest(new JUnit4TestAdapter(FSTOREB2CCSR_22.class));
        suite.addTest(new JUnit4TestAdapter(FSTOREB2CCSR_23.class));
        suite.addTest(new JUnit4TestAdapter(FSTOREB2CCSR_24.class));
        
        return suite;
    }
}