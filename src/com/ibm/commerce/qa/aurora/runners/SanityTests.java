/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2014
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */ 
/**
 * 
 */
package com.ibm.commerce.qa.aurora.runners;

import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;

import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_00;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_01;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_02;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_04;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_05;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_06;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_07;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_07b;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_08;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_09;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_10;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_11;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_12;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_13;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_14;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_14b;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_14c;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_15;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_16;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_17;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_18;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_18b;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_19;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_20;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_21;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_25;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_26;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_27;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_28;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_29;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_30;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_31;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_32;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_33;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_35;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_36;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_37;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_38;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_39;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_41;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_42;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_43;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_50;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_51;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_52;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_53;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_54;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_55;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_58;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_59;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_60;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_65;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_72;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_SOLR_SEARCH_01;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_SOLR_SEARCH_02;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_SOLR_SEARCH_04;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_SOLR_SEARCH_05;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_SOLR_SEARCH_06;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_SOLR_SEARCH_09;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_SOLR_SEARCH_12;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_SOLR_SEARCH_13;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_SOLR_SEARCH_15;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_SOLR_SEARCH_16;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_SOLR_SEARCH_17;
import com.ibm.commerce.qa.aurora.tests.FV2STOREB2C_SOLR_SEARCH_18;

/**
 *
 *
 */
@RunWith(Categories.class)
@IncludeCategory(Sanity.class)
@SuiteClasses({FV2STOREB2C_00.class,
	FV2STOREB2C_01.class,
	FV2STOREB2C_02.class,
	FV2STOREB2C_04.class,
	FV2STOREB2C_05.class,
	FV2STOREB2C_06.class,
	FV2STOREB2C_07.class,
	FV2STOREB2C_07b.class,
	FV2STOREB2C_08.class,
	FV2STOREB2C_09.class,
	FV2STOREB2C_10.class,
	FV2STOREB2C_11.class,
	FV2STOREB2C_12.class,
	FV2STOREB2C_13.class,
	FV2STOREB2C_14.class,
	FV2STOREB2C_14b.class,
	FV2STOREB2C_14c.class,
	FV2STOREB2C_15.class,
	FV2STOREB2C_16.class,
	FV2STOREB2C_17.class,
	FV2STOREB2C_18.class,
	FV2STOREB2C_18b.class,
	FV2STOREB2C_19.class,
	FV2STOREB2C_20.class,
	FV2STOREB2C_21.class,
	FV2STOREB2C_25.class,
	FV2STOREB2C_26.class,
	FV2STOREB2C_27.class,
	FV2STOREB2C_28.class,
	FV2STOREB2C_29.class,
	FV2STOREB2C_30.class,
	FV2STOREB2C_31.class,
	FV2STOREB2C_32.class,
	FV2STOREB2C_33.class,
	FV2STOREB2C_35.class,
	FV2STOREB2C_36.class,
	FV2STOREB2C_37.class,
	FV2STOREB2C_38.class,
	FV2STOREB2C_39.class,
	FV2STOREB2C_41.class,
	FV2STOREB2C_42.class,
	FV2STOREB2C_43.class,
	FV2STOREB2C_50.class,
	FV2STOREB2C_51.class,
	FV2STOREB2C_52.class,
	FV2STOREB2C_53.class,
	FV2STOREB2C_54.class,
	FV2STOREB2C_55.class,
	FV2STOREB2C_58.class,
	FV2STOREB2C_59.class,
	FV2STOREB2C_60.class,
	FV2STOREB2C_65.class,
	FV2STOREB2C_72.class, 
	FV2STOREB2C_SOLR_SEARCH_01.class,
    FV2STOREB2C_SOLR_SEARCH_02.class,
    FV2STOREB2C_SOLR_SEARCH_04.class,
    FV2STOREB2C_SOLR_SEARCH_05.class,
    FV2STOREB2C_SOLR_SEARCH_06.class,
    FV2STOREB2C_SOLR_SEARCH_09.class,
    FV2STOREB2C_SOLR_SEARCH_12.class,
    FV2STOREB2C_SOLR_SEARCH_13.class,
    FV2STOREB2C_SOLR_SEARCH_15.class,
    FV2STOREB2C_SOLR_SEARCH_16.class,
    FV2STOREB2C_SOLR_SEARCH_17.class,
    FV2STOREB2C_SOLR_SEARCH_18.class
    })
public class SanityTests {

}
