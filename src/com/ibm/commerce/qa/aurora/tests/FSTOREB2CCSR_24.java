package com.ibm.commerce.qa.aurora.tests;


	/*
	 *-----------------------------------------------------------------
	 * Licensed Materials - Property of IBM
	 *
	 * 
	 *
	 * WebSphere Commerce
	 *
	 * (C) Copyright IBM Corp. 2012
	 *
	 * US Government Users Restricted Rights - Use, duplication or
	 * disclosure restricted by GSA ADP Schedule Contract with
	 * IBM Corp.
	 *-----------------------------------------------------------------
	 */

	import java.util.logging.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.casl.keys.CaslKeysFactory;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CustomerServiceOrderSummaryPage;
import com.ibm.commerce.qa.aurora.page.MyOrderHistoryPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderDetailPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.CustomerServiceFindOrderWidget;
import com.ibm.commerce.qa.aurora.widget.CustomerServiceOrderCommentsSlideBarWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.wte.util.WcConfigManager;


	/** 
	 * Test scenario to test various use cases associated with Dropdown menu context
	 * Refer to each test case for a detailed use case description
	 */
	@RunWith(GuiceTestRunner.class)
	@TestModules(AuroraModule.class)
	public class FSTOREB2CCSR_24 extends AbstractAuroraSingleSessionTests
	{
		/**
		 * The internal copyright field.
		 */
		public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

		//A Variable to retrieve data from the data file. 
		@DataProvider
		private final TestDataProvider dsm;
		private final CaslFixturesFactory f_CaslFixtures;
		

		
		/**
		 * Test Class object constructor.
		 * 
		 * @param log
		 * 			   logging object 
		 * @param config
		 * 			   object to work with getConfig().properties file
		 * @param session
		 * 			   factory to create browser sessions
		 * @param dataSetManager
		 * 			   object to work with data files
		 * @param p_caslFixtures 
		 */		
		@Inject
		public FSTOREB2CCSR_24(
				Logger log, 
				WcConfigManager config,
				WcWteTestRule wcWebTestRule,
				CaslFoundationTestRule caslTestRule,
				TestDataProvider dataSetManager,
				CaslFixturesFactory p_caslFixtures, CaslKeysFactory p_caslKeysFactory)
		{
			super(log, wcWebTestRule, caslTestRule);
			this.dsm = dataSetManager;
			f_CaslFixtures = p_caslFixtures;
		}

		
		

		
		/**
		 * Test case to add a non empty order comment 
		 */
		@Test
		public void testFSTOREB2CCSR_2401()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			
			//Open the store in the browser.
			AuroraFrontPage frontPage1 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//Log in as CSR
			frontPage1.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//search for order using the orderId for completed shopping checkout 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage1.getHeaderWidget()
					.goToCustomerService()
					.getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
			findOrderWidget.verifyOrderSearchResultIsDisplayed();
			
			OrderDetailPage orderDetailPage = findOrderWidget.clickOrderID(orderId);
			CustomerServiceOrderCommentsSlideBarWidget orderCommentsSlideBarWidget = orderDetailPage.getCommentsBar().slideOrderComments();
			orderCommentsSlideBarWidget.addComment(dsm.getInputParameter("COMMENT"));
			
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = orderDetailPage.getHeaderWidget().goToShoppingCartPage();
			
			shopCart.clickLockOrder().getCommentsBar().slideOrderComments()
			.addComment(dsm.getInputParameter("COMMENT1"));
			
			//Go to shipping and billing page
			ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
			//Confirm the correct Shipping Address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Confirm the correct Billing address is selected
			shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
				.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
			
			shippingAndBilling.getCommentsBar().slideOrderComments()
			.addComment(dsm.getInputParameter("COMMENT2"));
			
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
			
			singleOrderSummary.getCommentsBar().slideOrderComments()
			.addComment(dsm.getInputParameter("COMMENT3"));
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
			
			orderConfirmation.getCommentsBar().slideOrderComments()
			.addComment(dsm.getInputParameter("COMMENT4"));
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();	
			
		}

		/**
		 * Test case to add a empty order comment and get appropriate error message 
		 */
		@Test
		public void testFSTOREB2CCSR_2402()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			 
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = frontPage.getHeaderWidget().goToShoppingCartPage();
			
			//Go to shipping and billing page
			ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
			//Confirm the correct Shipping Address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Confirm the correct Billing address is selected
			shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
				.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
							
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
							
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();	
			
			//Open the store in the browser.
			AuroraFrontPage frontPage1 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//Log in as CSR
			frontPage1.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//search for order using the orderId for completed shopping checkout 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage1.getHeaderWidget()
					.goToCustomerService()
					.getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId)
			.submitSearch();
			findOrderWidget.verifyOrderSearchResultIsDisplayed();
			
			OrderDetailPage orderDetailPage = findOrderWidget.clickOrderID(orderId);
			CustomerServiceOrderCommentsSlideBarWidget orderCommentsSlideBarWidget = orderDetailPage.getCommentsBar().slideOrderComments();
			orderCommentsSlideBarWidget.addCommentNoErrorCheck(" ");
			
			
			//need a different verification method to check error message
			orderDetailPage.verifyErrorMsgTxt(dsm.getInputParameter("ERROR_MSG"));
			
		}	
		
		/**
		 * Test case to add a non empty order comment with special characters 
		 */
		@Test
		public void testFSTOREB2CCSR_2403()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			 
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = frontPage.getHeaderWidget().goToShoppingCartPage();
			
			//Go to shipping and billing page
			ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
			//Confirm the correct Shipping Address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Confirm the correct Billing address is selected
			shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
				.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
							
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
							
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();	
			
			//Open the store in the browser.
			AuroraFrontPage frontPage1 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//Log in as CSR
			frontPage1.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//search for order using the orderId for completed shopping checkout 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage1.getHeaderWidget()
					.goToCustomerService()
					.getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
			findOrderWidget.verifyOrderSearchResultIsDisplayed();
			
			OrderDetailPage orderDetailPage = findOrderWidget.clickOrderID(orderId);
			CustomerServiceOrderCommentsSlideBarWidget orderCommentsSlideBarWidget = orderDetailPage.getCommentsBar().slideOrderComments();
			orderCommentsSlideBarWidget.addComment(dsm.getInputParameter("COMMENT"));
			
		    
			
		}
		
		/**
		 * Test case to add a non empty order comment with 3000 characters 
		 * tests two scenarios when order comment = 3000 characters
		 *  and exceeds 3000 characters 
		 */
		@Test
		public void testFSTOREB2CCSR_2404()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			 
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = frontPage.getHeaderWidget().goToShoppingCartPage();
			
			//Go to shipping and billing page
			ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
			//Confirm the correct Shipping Address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Confirm the correct Billing address is selected
			shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
				.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
							
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
							
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();	
			
			//Open the store in the browser.
			AuroraFrontPage frontPage1 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//Log in as CSR
			frontPage1.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//search for order using the orderId for completed shopping checkout 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage1.getHeaderWidget()
					.goToCustomerService()
					.getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
			findOrderWidget.verifyOrderSearchResultIsDisplayed();
			
			OrderDetailPage orderDetailPage = findOrderWidget.clickOrderID(orderId);
			CustomerServiceOrderCommentsSlideBarWidget orderCommentsSlideBarWidget = orderDetailPage.getCommentsBar().slideOrderComments();
			//this comment should be added 
			orderCommentsSlideBarWidget.addComment(dsm.getInputParameter("COMMENT=3000"));
			
			//this comment should not be added 
			orderCommentsSlideBarWidget.addCommentNoErrorCheck(dsm.getInputParameter("COMMENT>3000"));
			
			//verify the error msg when character of a comment being added exceeds 3000 char's 
			orderDetailPage.verifyErrorMsgTxt(dsm.getInputParameter("ERROR_MSG"));
			
			
			
		}
		
		/**
		 * Test case to add a non empty order comment and then cancel the order
		 */
		@Test
		public void testFSTOREB2CCSR_2405()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();	
			
			//Open the store in the browser.
			AuroraFrontPage frontPage1 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//Log in as CSR
			frontPage1.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//search for order using the orderId for completed shopping checkout 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage1.getHeaderWidget()
					.goToCustomerService()
					.getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
			findOrderWidget.verifyOrderSearchResultIsDisplayed();
			
			OrderDetailPage orderDetailPage = findOrderWidget.clickOrderID(orderId);
			
			//cancel the order from the order detail page 
			MyOrderHistoryPage orderHistoryPage = orderDetailPage.csrCancelsOrder();
			
		   orderDetailPage = orderHistoryPage.goToOrderDetailPageWithOrderId(orderId);
		   
		   CustomerServiceOrderCommentsSlideBarWidget orderCommentsSlideBarWidget = orderDetailPage.getCommentsBar().slideOrderComments();
			orderCommentsSlideBarWidget.addComment(dsm.getInputParameter("COMMENT"));
		   //orderSummaryPage.getOrderSummary().verifyStatus(dsm.getInputParameter("ORDER_STATUS"));
			
		}
		
		/**
		 * Test case to add a non empty order comment on the shop cart page
		 *  and checkout on the order summary page
		 */
		@Test
		public void testFSTOREB2CCSR_2406()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			
			//Open the store in the browser.
			AuroraFrontPage frontPage1 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//Log in as CSR
			frontPage1.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//search for order using the orderId for completed shopping checkout 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage1.getHeaderWidget()
					.goToCustomerService()
					.getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
			findOrderWidget.verifyOrderSearchResultIsDisplayed();
			
			//add a new comment to the order on the Order Summary Page  
			CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget.clickActionButton(orderId).clickOrderSummary();
		
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = orderSummaryPage.clickOnCheckOut();
			
			shopCart.clickLockOrder().getCommentsBar().slideOrderComments()
			.addComment(dsm.getInputParameter("COMMENT1"));
			
			//Go to shipping and billing page
			ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
			//Confirm the correct Shipping Address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Confirm the correct Billing address is selected
			shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
				.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
				
			shippingAndBilling.getCommentsBar().slideOrderComments()
			.addComment(dsm.getInputParameter("COMMENT2"));
			
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
			
			orderConfirmation.getCommentsBar().slideOrderComments()
			.addComment(dsm.getInputParameter("COMMENT4"));
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();	
		}
		
		/**
		 * Test case to add a non empty order comment and re-order on the order summary page
		 */
		@Test
		public void testFSTOREB2CCSR_2407()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			 
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = frontPage.getHeaderWidget().goToShoppingCartPage();
			
			//Go to shipping and billing page
			ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
			//Confirm the correct Shipping Address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Confirm the correct Billing address is selected
			shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
				.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
							
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
							
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();	
			
			
			AuroraFrontPage frontPage1 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			//Log in as CSR
			frontPage1.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//search for order using the orderId for completed shopping checkout 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage1.getHeaderWidget()
					.goToCustomerService()
					.getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
			findOrderWidget.verifyOrderSearchResultIsDisplayed();
			
			OrderDetailPage orderDetailPage = findOrderWidget.clickOrderID(orderId);
			CustomerServiceOrderCommentsSlideBarWidget orderCommentsSlideBarWidget = orderDetailPage.getCommentsBar().slideOrderComments();
			orderCommentsSlideBarWidget.addComment(dsm.getInputParameter("COMMENT"));
			
			for(int i = 2; i<= 16; i++){
				orderCommentsSlideBarWidget
			    .addComment("this is order # " + i);
			}
		    
			
			orderCommentsSlideBarWidget.verifyLastComment(dsm.getInputParameter("LAST_COMMENT"));
		}
		
		@Test
		public void testFSTOREB2CCSR_2408()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			
			//Open the store in the browser.
			AuroraFrontPage frontPage1 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//Log in as CSR
			frontPage1.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//search for order using the orderId for completed shopping checkout 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage1.getHeaderWidget()
					.goToCustomerService()
					.getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
			findOrderWidget.verifyOrderSearchResultIsDisplayed();
			
			OrderDetailPage orderDetailPage = findOrderWidget.clickOrderID(orderId);
			CustomerServiceOrderCommentsSlideBarWidget orderCommentsSlideBarWidget = orderDetailPage
					.getCommentsBar();
			
			//order comments slider should NOT be visible on the order detail page unless it is clicked on
			try{ 
			orderDetailPage.isSliderCommentTextAreaVisible();
			}catch(IllegalStateException e)	{
				System.out.println("OrderCommentsSlideBarWidget should not be available.");
			}
			
			orderCommentsSlideBarWidget.slideOrderComments();
			orderCommentsSlideBarWidget.addComment(dsm.getInputParameter("COMMENT"));
			
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = orderDetailPage.getHeaderWidget().goToShoppingCartPage();
			
			//order comments slider should NOT be visible on the shop cart page unless it is clicked on
			try{
				shopCart.isSliderCommentTextAreaVisible();
			}catch(IllegalStateException e){
				System.out.println("OrderCommentsSlideBarWidget should not be available.");
			}
			
			shopCart.clickLockOrder().getCommentsBar().slideOrderComments()
			.addComment(dsm.getInputParameter("COMMENT1"));
			
			//Go to shipping and billing page
			ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
			//Confirm the correct Shipping Address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Confirm the correct Billing address is selected
			shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
				.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
			
			try{
				shippingAndBilling.isSliderCommentTextAreaVisible();
			}catch(IllegalStateException e){
				System.out.println("OrderCommentsSlideBarWidget should not be available.");
			}
			shippingAndBilling.getCommentsBar().slideOrderComments()
			.addComment(dsm.getInputParameter("COMMENT2"));
			
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
			
			try{
				singleOrderSummary.isSliderCommentTextAreaVisible();
			}catch(IllegalStateException e){
				System.out.println("OrderCommentsSlideBarWidget should not be available.");
			}
			
			singleOrderSummary.getCommentsBar().slideOrderComments()
			.addComment(dsm.getInputParameter("COMMENT3"));
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
			
			try{
				orderConfirmation.isSliderCommentTextAreaVisible();
			}catch(IllegalStateException e){
				System.out.println("OrderCommentsSlideBarWidget should not be available.");
			}
			orderConfirmation.getCommentsBar().slideOrderComments()
			.addComment(dsm.getInputParameter("COMMENT4"));
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();	
			
		}
	}
