package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Logger;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.CustomerRegisterPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.MyRecuringOrdersPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.RecurringChildOrderPage;
import com.ibm.commerce.qa.aurora.page.RecurringOrderDetailPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
/** 		
 * Scenario: FV2STOREB2C_52
 * Details: Shopper Completes a recurring Order 
 * Pre-requisites: 
 * 1. subscription feature should be enabled
 * 2. SubscriptionSchedulerCmc scheduler should be running. and interval should be less than one minute.
 * 3. Recurring item inventory should be available for this store.
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_52 extends  AbstractAuroraSingleSessionTests {

	@DataProvider
	private final TestDataProvider dsm;
	
	String loginId = RandomStringUtils.randomAlphanumeric(8);
	String password = "wcs1admin";

	private CaslFixturesFactory f_caslFixtures;
	
		
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dataSetManager
	 * @param p_caslFixtures 
	 */
	@Inject
	public FV2STOREB2C_52(Logger log, CaslFoundationTestRule caslTestRule, WcWteTestRule wcWebTestRule, TestDataProvider dataSetManager,
			CaslFixturesFactory p_caslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		f_caslFixtures = p_caslFixtures;
	}
	
	
	// In this pre set up step , creating a register user that will be use in further test cases.
	/**
	 * @throws Exception
	 */
	@Before
	public void customerRegistration() throws Exception
	{
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		
		frontPage.getHeaderWidget().signIn().registerCustomer()
		//type user name
		.typeLogonId(loginId)
		//type first name
		.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
		//type last name
		.typeLastName(dsm.getInputParameter("LAST_NAME"))
		//type password
		.typePassword(password)
		//Verify password
		.typeVerifyPassword(password)
		//type street address
		.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
		//Select country
		.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
		//type or select state
		.selectStateOrProvince(dsm.getInputParameter("STATE"))
		//type city
		.typeCity(dsm.getInputParameter("CITY"))
		//type zipcode
		.typeZipCode(dsm.getInputParameter("ZIPCODE"))
		//type E-mail
		.typeEmail(dsm.getInputParameter("EMAIL"))
		//type home phone number
		.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
		//Select gender
		.selectGender(dsm.getInputParameter("GENDER"))
		//type mobile phone number
		.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
		//Update Allow Me Option check box
		.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
		//Check if preferred language drop down is visible
		.verifyPreferredLanguageDropDownListPresent()	
		//Select preferred language
		//.selectPreferedCurrency(dsm.getInputParameter("PREFERRED_CURRENCY"))
		//Check if preferred currency drop down is visible
	     .verifyPreferredCurrencyDropDownListPresent()
		//Selected preferred language
		//.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
		//Check if birthday selection drop down is visible
		.verifyBirthdaySelectionPresent()
		//Select  birth year
		.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
		//Select birth month
		.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
		//Select birth day
		.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
		//Submit registration, the u is taken to the MyAccounts page
		.submit();
	}
	
	/**
	 * Registered user schedules a recurring Order 
	 * Pre-conditions: <ul>
	 * 					<li>SubscriptionSchedulerCmd job must be added to scheduler, 60s interval</li>
	 * 					<li>Tester knows the Store URL (http://<hostname>/webapp/wcs/stores/servlet/en/aurora)</li>
	 * 					<li>User is guest user</li>				
	 * 				   </ul>
	 * Flex flow options: none
	 * Post conditions: Registered user should be able to successfully place the recurring order
	 * 
	 * @throws Exception
	 */
	@Test
	public void FV2STOREB2C_5201() throws Exception{
		
		
		//opening the store front page
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Sign In  into the store
		MyAccountMainPage myAccountPage =  frontPage.getHeaderWidget().signIn().typeUsername(loginId).typePassword(password).signIn().closeSignOutDropDownWidget().goToMyAccount();
		//MyAccountMainPage myAccountPage =  frontPage.getHeaderWidget().signIn().enterUsername("testUser").enterPassword("wcs1admin").clickSignInButton();
		
			
		//Go to department page
		CategoryPage subCat = myAccountPage.getFooterWidget().goToSiteMapPage().goToCategoryPageByName(CategoryPage.class, dsm.getInputParameter("CATEGORY_NAME"));
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCTNAME")).addToCart();
		
			
		//Go to the shopCart page and schedule this order as recurring order
		ShopCartPage shopCartPage = productDisplayPage.getHeaderWidget().goToShoppingCartPage().checkScheduleThisOrderCheckBox();
		
		//Go to the shipping billing page
		ShippingAndBillingPage shippingBillingPage = shopCartPage.continueToNextStep();
		
		//Getting the today date
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String dateNow = formatter.format(currentDate.getTime());
		  
		//Entering the order frequency and order start date.
		shippingBillingPage = shippingBillingPage.selectOrderFrequency(dsm.getInputParameter("ORDER_FREQUENCY")).typeStartDate(dateNow);
		
		//Entering the payment method and credit card details
		shippingBillingPage.selectPaymentMethod(dsm.getInputParameter("PAYMENTMETHOD")).typeCreditCardNumber(dsm.getInputParameter("CREDIT_CARD_NO"));
		
		//go the order summary page
		OrderSummarySingleShipPage orderSummaryPage = shippingBillingPage.singleShipNext();
		
		//Go to order confirmation page . verifying the recurring order got placed successfully
		OrderConfirmationPage orderConformationPage = orderSummaryPage.completeOrder().verifyScheduledOrderSuccessful();
		
		//Get order no from order confirmation page.
		String orderNumber = orderConformationPage.getScheduledOrderNumber();
		
		//create child order
		OrdersFixture orders = f_caslFixtures.createOrdersFixture(loginId, password, getConfig().getStoreName());
		
		orders.createChildOrder(Integer.parseInt(orderNumber),"Y","N");
		
		//Go to recurring order page
		MyRecuringOrdersPage recurringOrderPage = orderConformationPage.getHeaderWidget().goToMyAccount().getSidebar().goToRecurringOrdersPage();
		
		
		//verifying order no present in recurring order page.
		recurringOrderPage = recurringOrderPage.verifyOrdreNumberIsPresent(orderNumber);
		
		//verifying the recurring order status
		recurringOrderPage = recurringOrderPage.verifyOrderStatus("Active" , orderNumber);
		
		
		
		//Go to the recurring order details page
		RecurringOrderDetailPage recurringOrderDetailPage = recurringOrderPage.goToRecurringOrderDetailPage(orderNumber);
		
		//Go to child order page
		RecurringChildOrderPage recurringChildOrdrePage = recurringOrderDetailPage.goToChildOrderPage();
		
		
		
		//verifying atleast one child order is present
		recurringChildOrdrePage.verifyAtLeatOneChildOrderPresent();

	
	}
		
	

	/**
	 * Guest user Schedules a recurring Order after he registers in the store 
	 * Pre-conditions: <ul>
	 * 					<li>SubscriptionSchedulerCmd job must be added to scheduler, 60s interval</li>
	 * 					<li>Tester knows the Store URL (http://<hostname>/webapp/wcs/stores/servlet/en/aurora)</li>
	 * 					<li>User is guest user</li>				
	 * 				   </ul>
	 * Flex flow options: none
	 * Post conditions: Converted Register user should able to place the recurring order
	 * 
	 * @throws Exception
	 */
	@Test
	public void FV2STOREB2C_5203() throws Exception{
		
		
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Go to department page
		CategoryPage subCat = frontPage.getFooterWidget().goToSiteMapPage().goToCategoryPageByName(CategoryPage.class, dsm.getInputParameter("CATEGORY_NAME"));
		
		//Go to product display page and add this item to shop cart page.
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCTNAME")).addToCart();
		
		//go to the customer registration page.
		CustomerRegisterPage registrationPage = productDisplayPage.getHeaderWidget().signIn().registerCustomer();
		
	
		//Registering the customer
		String tempLoginId = RandomStringUtils.randomAlphanumeric(8);
		MyAccountMainPage myAccountpPage = registrationPage
		//type username
		.typeLogonId(tempLoginId)
		//type first name
		.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
		//type last name
		.typeLastName(dsm.getInputParameter("LAST_NAME"))
		//type password
		.typePassword(dsm.getInputParameter("PASSWORD"))
		//Verify password
		.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
		//type street address
		.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
		//Select country
		.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
		//type or select state
		.selectStateOrProvince(dsm.getInputParameter("STATE"))
		//type city
		.typeCity(dsm.getInputParameter("CITY"))
		//type zipcode
		.typeZipCode(dsm.getInputParameter("ZIPCODE"))
		//type E-mail
		.typeEmail(dsm.getInputParameter("EMAIL"))
		//type home phone number
		.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
		//Select gender
		.selectGender(dsm.getInputParameter("GENDER"))
		//type mobile phone number
		.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
		//Update Allow Me Option check box
		.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
		//Check if preferred language drop down is visible
		.verifyPreferredLanguageDropDownListPresent()	
		//Select preferred language
		//.selectPreferedCurrency(dsm.getInputParameter("PREFERRED_CURRENCY"))
		//Check if preferred currency drop down is visible
		.verifyPreferredCurrencyDropDownListPresent()
		//Selected preferred language
		//.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
		//Check if birthday selection drop down is visible
		.verifyBirthdaySelectionPresent()
		//Select  birth year
		.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
		//Select birth month
		.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
		//Select birth day
		.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
		//Submit registration, the uesr is taken to the MyAccounts page
		.submit();
		
	
		
		//Go to the shopcart page and check the recurring order check box	
		ShopCartPage shopCartPage = myAccountpPage.getHeaderWidget().goToShoppingCartPage().checkScheduleThisOrderCheckBox();
		
		//Go to the shipping billing page
		ShippingAndBillingPage shippingBillingPage = shopCartPage.continueToNextStep();
		
		//Getting the tomorrow date
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String dateNow = formatter.format(currentDate.getTime());
		  
		//Entering the order frequency and order start date.
		shippingBillingPage = shippingBillingPage.selectOrderFrequency(dsm.getInputParameter("ORDER_FREQUENCY")).typeStartDate(dateNow);
		
		//Entering the payment method and credit card details
		shippingBillingPage.selectPaymentMethod(dsm.getInputParameter("PAYMENTMETHOD")).typeCreditCardNumber(dsm.getInputParameter("CREDIT_CARD_NO"));
		
		//go the order summary page
		OrderSummarySingleShipPage orderSummaryPage = shippingBillingPage.singleShipNext();
		
		//Go to order confirmation page . verifying the recurring order got placed successfully
		OrderConfirmationPage orderConformationPage = orderSummaryPage.completeOrder().verifyScheduledOrderSuccessful();
		
		//Get order no from order confirmation page.
		String orderNumber = orderConformationPage.getScheduledOrderNumber();
		
		//create child order
		OrdersFixture orders =f_caslFixtures.createOrdersFixture( tempLoginId, dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.createChildOrder(Integer.parseInt(orderNumber),"Y","N");
		
		//Go to recurring order page
		MyRecuringOrdersPage recurringOrderPage = orderConformationPage.getHeaderWidget().goToMyAccount().getSidebar().goToRecurringOrdersPage();
		
		
		//verifying order no present in recurring order page.
		recurringOrderPage = recurringOrderPage.verifyOrdreNumberIsPresent(orderNumber);
		
		//verifying the recurring order status
		recurringOrderPage = recurringOrderPage.verifyOrderStatus("Active" , orderNumber);
		
		
		
		//Go to the recurring order details page
		RecurringOrderDetailPage recurringOrderDetailPage = recurringOrderPage.goToRecurringOrderDetailPage(orderNumber);
		
		//Go to child order page
		RecurringChildOrderPage recurringChildOrdrePage = recurringOrderDetailPage.goToChildOrderPage();
		
		
		
		//verifying atleast one child order is present
		recurringChildOrdrePage.verifyAtLeatOneChildOrderPresent();
		
	

	}
	

	
		
	
}
