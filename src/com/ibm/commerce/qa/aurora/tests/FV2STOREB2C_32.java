package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.Accelerator;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.url.DeltaUpdates;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/**
 * Test Scenario FV2STOREB2C_32
 * The objective of this test scenario is to test inventory management on the store.
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_32 extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	@DataProvider(apolloDSM=true)
	
	//A Variable to retrieve data from the data file.
	private final TestDataProvider dsm;
	
	//A Variable to perform CMC tasks.
	private final CMC cmc;
	
	//A Variable to perform Accelerator tasks.
	private final Accelerator accelerator;

	//A Variable to perform delta update wait.
	private final DeltaUpdates deltaUpdate;
	
	//The generated productId
	private String productId;
	
	//The catalog Id of the store
	private String masterCatalogId;
	
	private final CaslFixturesFactory f_CaslFixtures;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param accelerator
	 * 			   object to perform Accelerator tasks
	 * @param cmc 
	 * 			   object to perform CMC tasks
	 * @param deltaUpdate 
	 * 				object to perform delta update wait operations
	 */
	@Inject
	public FV2STOREB2C_32(Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			Accelerator accelerator,
			CMC cmc,
			DeltaUpdates deltaUpdate,
			CaslFixturesFactory p_CaslFixtures){
		
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		this.accelerator = accelerator;
		this.cmc = cmc;
		this.deltaUpdate = deltaUpdate;
		f_CaslFixtures = p_CaslFixtures;
	}

	/**
	 * Setup function to create new product with two items, one with inventory and another item with no 
	 * inventory. Back ordering will be disabled.
	 * 
	 * @throws Exception
	 */
	@Before
	public void productAndInventorySetup() throws Exception
	{
		String skuItemId1, skuItemId2;
		
		dsm.setDataBlock("CMClogon");

		//Log into cmc
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		cmc.setLocale(dsm.getInputParameter("locale"));

		//Select the store to work on
		cmc.selectStore(getConfig().getStoreName());
		
		//Select the catalog to work on
		masterCatalogId = cmc.selectCatalog();
		String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
		
		dsm.setDataBlock("createProduct");
		
		//Create a new product filling in all required parameters
		try
		{
		productId = cmc.createProduct(dsm.getInputParameter("parentCategory"),dsm.getInputParameter("partNumber"),dsm.getInputParameter("productName"),dsm.getInputParameter("published"));
		}
		catch (IllegalStateException e)
		{
			productId = cmc.getCatalogEntryId(dsm.getInputParameter("partNumber"));
		}
		dsm.setDataBlock("offerPrice1");
		
		//Create List price
		cmc.createListPriceForCatalogEntry(productId);
		
		//Create offer price
		cmc.createOfferPriceForCatalogEntry(productId);
		
		//Add defining product attribute dictionary attribute - Color
		String[] attributeStringId = cmc.getAttributeDictionaryAttributeIdsForCatalogEntry(cmc.getCatalogEntryId("BCL014_1402"));
		cmc.addProductAttributeDictionaryAttribute(productId, attributeStringId[1], "0");
		
		String[][] attributeDictionaryData = cmc.getAllowedValAttrDictAttributeValue(attributeStringId[1]);

		//get list of attribute dictionary attribute value ID's 
		String[] attributeDictionaryValuesId = attributeDictionaryData[0];
		
		//get list of attribute dictionary attribute identifier names
		String[] attributeDictionaryValueIdentifiers = attributeDictionaryData[1];
		
		int attrValue1 = 0;
		int attrValue2 = 0;
		dsm.setDataBlock("addProductAttribute");
		
		//Go through the list of Identifiers and get the index of the attributes name needed
		for(int i = 0; i < attributeDictionaryValueIdentifiers.length; i++)
		{
			if(attributeDictionaryValueIdentifiers[i].equals(dsm.getInputParameter("attributeValue1")))
			{
				attrValue1 = i;
			}
			else if (attributeDictionaryValueIdentifiers[i].equals(dsm.getInputParameter("attributeValue2")))
			{
				attrValue2 = i;
			}
		}
		
		dsm.setDataBlock("createSKU1");
		
		//Create first SKU for the product, filling in all required fields
		skuItemId1 = cmc.createSKU(productId,dsm.getInputParameter("partNumber"),dsm.getInputParameter("skuName"),dsm.getInputParameter("published"));
		
		//Add an attribute value for this SKU
		cmc.addSKUDefiningAttributeDictionaryAttributeValue(skuItemId1, attributeStringId[1], attributeDictionaryValuesId[attrValue1], "0");
		
		dsm.setDataBlock("createSKU2");
		
		//Create second SKU for the product, filling in all required fields
		skuItemId2 = cmc.createSKU(productId,dsm.getInputParameter("partNumber"),dsm.getInputParameter("skuName"),dsm.getInputParameter("published"));
		
		//Add an attribute value for this SKU
		cmc.addSKUDefiningAttributeDictionaryAttributeValue(skuItemId2, attributeStringId[1], attributeDictionaryValuesId[attrValue2], "0");
		
		dsm.setDataBlock("offerPrice1");

		//Create list price.
		cmc.createListPriceForCatalogEntry(skuItemId1);
		cmc.createListPriceForCatalogEntry(skuItemId2);
		
		//Create offer price.
		cmc.createOfferPriceForCatalogEntry(skuItemId1);
		cmc.createOfferPriceForCatalogEntry(skuItemId2);
		
		//deltaUpdate.waitForDeltaWithinApproximateTimeout(masterCatalogId, 600);
		// Trigger a delta update to ensure the product will be visible.
		deltaUpdate.beforeWaitForDelta(masterCatalogId);
		getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
		deltaUpdate.waitForDelta(masterCatalogId, 120);


		cmc.logoff();
		
		dsm.setDataBlock("acceleratorLogon");
		accelerator.setFulfillmentCenterName(getConfig().getStoreName());
		//Log onto accelerator
		accelerator.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"), 
						  dsm.getInputParameter("storeName"), dsm.getInputParameter("langId"));
//		
		dsm.setDataBlock("changeFulfillment");

		//Disable 'allow back order' on new product
		accelerator.changeProductFulfillment(productId);
		
		dsm.setDataBlock("addReceipt");
		
		//Add adhoc receipt for the second SKU item, leaving the first created SKU with no inventory.
		accelerator.addAdHocReceiptForSKU(dsm.getInputParameter("skuName"), dsm.getInputParameter("quantity"),
										  dsm.getInputParameter("unitCost"), dsm.getInputParameter("currency"));
		
		//Log off accelerator 
		accelerator.logoff();
		
		
		
		
	}
	
	/**
	 * Perform checkout flow of a new item that has inventory available available.
	 * 
	 * @throws Exception 
	 */
	@Test
	public void testFV2STOREB2C_3201() throws Exception
	{
		dsm.setDataBlock("testFV2STOREB2C_3201");
		
		//Open the store in browser using store preview URL
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Open Sign In/Register page
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter account User Name/Password and sign in
		HeaderWidget header = signInPage.typeUsername(dsm.getInputParameter("store_user_name"))
		.typePassword(dsm.getInputParameter("store_user_password")).signIn();
		
		//Open sub category department page
		CategoryPage subCat  = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("top_category_name"), 
				 dsm.getInputParameter("sub_category_name"),dsm.getInputParameter("item_category_name"));

		//Select a product to view
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("productName"));
		
		//Select product attribute of item that has no inventory
		productDisplayPage.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("attributeName"), dsm.getInputParameter("sku1_atrributeValue"));
		
		//Select add to cart expected to receive an error message
		productDisplayPage.addToCartFail(dsm.getInputParameter("error_message"));
		
		productDisplayPage.getDefiningAttributesWidget().unselectAttributeFromDropdown(dsm.getInputParameter("attributeName"), dsm.getInputParameter("sku1_atrributeValue"));
		
		//Select product attribute of item that has inventory.
		productDisplayPage.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("attributeName"), dsm.getInputParameter("sku2_atrributeValue"));
		
		//Select add to cart
		productDisplayPage.addToCart();
		
		//Get the header section object
		HeaderWidget headerSection = productDisplayPage.getHeaderWidget();
		
		//Click on 'Shopping Cart' link on header
		ShopCartPage shopCartPage = headerSection.goToShoppingCartPage();
		
		//Verify product is successfully added to cart
		headerSection.verifyMiniCartTotalPrice(dsm.getInputParameter("total"));
		
		//Check out shopping cart product
		ShippingAndBillingPage shippingAndBilling = shopCartPage.continueToNextStep();
		
		//Select a billing method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("billing_method"));
		
		//Select next button and continue to order summary page
		OrderSummarySingleShipPage orderSummary = shippingAndBilling.singleShipNext();
		
		//Complete checkout order.
		orderSummary.completeOrder();
	}
	
	
	/**
	 * Perform check out flow of new item that has no quantity in inventory.
	 * 
	 * @throws Exception 
	 */
	@Test
	public void testFV2STOREB2C_3203() throws Exception
	{
		
		dsm.setDataBlock("testFV2STOREB2C_3203");

		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Open Sign In/Register page
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter account User Name/Password and sign in
		HeaderWidget header = signInPage.typeUsername(dsm.getInputParameter("store_user_name"))
		.typePassword(dsm.getInputParameter("store_user_password")).signIn().closeSignOutDropDownWidget();
		
		//Open sub category department page
		CategoryPage subCat  = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("top_category_name"), 
				dsm.getInputParameter("sub_category_name"), dsm.getInputParameter("item_category_name"));

		//Select a product to view
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("productName"));
		
		//Select product attribute of item that has no inventory.
		productDisplayPage.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("attributeName"), dsm.getInputParameter("sku1_atrributeValue"));
		
		//Select add to cart expected to receive an error message
		productDisplayPage.addToCartFail(dsm.getInputParameter("error_message"));
		
	}
	
	
	/**
	 * Tear down ran after every test case
	 * @throws Exception 
	 * 
	 */
	@After
	public void tearDown() throws Exception
	{
	try
	{
		dsm.setDataBlock("tearDown");
		
		//Remove all items from the shopping cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		orders.removeAllItemsFromCart();
		orders.deletePaymentMethod();
		
		//login to CMC
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		
		//Select the store to work on
		cmc.selectStore();
		
		//Select the catalog to work on
		cmc.selectCatalog();
		
		String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
		
		//delete the product
		cmc.deleteCatalogEntry(productId);
		
		deltaUpdate.beforeWaitForDelta(masterCatalogId);
		getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
		deltaUpdate.waitForDelta(masterCatalogId, 60);

		//log off from CMC
		cmc.logoff();
	}
	catch(RuntimeException e)
	{
		getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
	}
		
		
	}
 }
