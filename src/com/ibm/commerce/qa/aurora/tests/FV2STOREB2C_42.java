package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.BOPISAddressPage;
import com.ibm.commerce.qa.aurora.page.BOPISShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.BopisStoreSelectionPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.page.StoreSelectionPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/**
 * This Scenario is related to physical store. 
 * 
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_42 extends  AbstractAuroraSingleSessionTests {

	@DataProvider
	private final TestDataProvider dsm;
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dataSetManager
	 */
	@Inject
	public FV2STOREB2C_42(Logger log, CaslFoundationTestRule caslTestRule, WcWteTestRule wcWebTestRule, TestDataProvider dataSetManager)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		
	}
	
	/** Shopper searches for physical stores via drop-down menu that finds some stores
	 * Post Condition : 
	 * Stores that match the search criteria are returned with store address, phone number and business hours. 
	 * At least the following results are returned: 
	 * Found Stores = Alton Towers Plaza, Bathurst Plaza, Bay Plaza"
	 * @throws Exception
	 */
	@Category(Sanity.class)
	@Test
	public void FV2STOREB2C_4201() throws Exception
	{
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		HeaderWidget header = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		StoreSelectionPage storeSelectionPage = header.goToStoreLocator();
		
		//remove all store present in store list
		storeSelectionPage = storeSelectionPage.removeAllStoreFromStoreList();
		
		
		//opening sub category page 
		CategoryPage subCat = storeSelectionPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CATEGORY"),dsm.getInputParameter("SUB_CATEGORY"), dsm.getInputParameter("CATEGORY_ITEM"));
		
		
		//clicking on product image
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//selecting the swatch attribute
		productDisplayPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE1"))
												.selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE2"));
		
		//Adding product to shop cart
		ShopCartPage shopCartPage = productDisplayPage.addToCart().getHeaderWidget().goToShoppingCartPage();
		
		//select pick up at store option
		shopCartPage =  shopCartPage.selectPickUpAtStore();
		
		//click on continue button
		BopisStoreSelectionPage storePage = shopCartPage.continueAsPickupAtStore();
		
		//search for a store	
		storePage = storePage.selectCountry(dsm.getInputParameter("COUNTRY"))
												.selectState(dsm.getInputParameter("STATE"))
												.selectCity(dsm.getInputParameter("CITY"))
												.submitSearch();
		
		//validating the result has returned as expected 
		storePage = storePage.verifyStoreExistsInSearchList(dsm.getInputParameter("STORE1"))
												.verifyStoreExistsInSearchList(dsm.getInputParameter("STORE2"))
												.verifyStoreExistsInSearchList(dsm.getInputParameter("STORE3"));
		
		//add an store to store list
		storePage = storePage.addStoreToList(dsm.getInputParameter("STORE1"));
		
		//select store from store list
		storePage.selectStoreFromList(dsm.getInputParameter("STORE1"));
		
		//Go to Checkout address selection page
		BOPISAddressPage address = storePage.nextButton();
		
		//select pay in store check box
		//address = address.selectPayInStore();
		
		//Go to shipping Billing page
		BOPISShippingAndBillingPage shippingAndBillingPage = address.goToShippingBillingPage();
		
		//Selecting payment method
		shippingAndBillingPage = shippingAndBillingPage.selectPaymentMethod(dsm.getInputParameter("PAYMENT_METHOD"));
		
		//Go to order summanry page
		OrderSummarySingleShipPage orderSummaryPage = shippingAndBillingPage.singleShipNext();
		
		//Go to order summary page by clicking on order button
		OrderConfirmationPage orderConformationPage = orderSummaryPage.completeOrder();
		
		
		//validating the order
		orderConformationPage.verifyOrderSuccessful("There is insufficient inventory to fulfill your entire order at the moment. You will be contacted by our customer service representatives.");
		
		
		
	}
	
	
}
