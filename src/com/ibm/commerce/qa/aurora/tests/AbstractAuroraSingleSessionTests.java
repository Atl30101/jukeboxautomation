package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Logger;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.rules.TestRule;

import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.rules.impl.DefaultJUnitLifeCycleRule;
import com.ibm.commerce.qa.wte.framework.test.WebSession;
import com.ibm.commerce.qa.wte.util.WcConfigManager;

/**
 * Abstract Aurora Tests class, helps avoid code duplication. Other buckets
 * should not just re-use this. If any bucket wants to make a superclass for
 * their test classes, then consider such a superclass as a convenience and
 * create a custom one per bucket.
 */
public abstract class AbstractAuroraSingleSessionTests
{
	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

	@ClassRule
	public static final TestRule CLASS_RULE = DefaultJUnitLifeCycleRule.getStaticClassRuleInstance();

	@Rule
	public final WcWteTestRule fWcWteTestRule;

	@Rule
	public final CaslFoundationTestRule fCaslTestRule;

	private final Logger fLog;

	protected AbstractAuroraSingleSessionTests(
			Logger pLog,
			WcWteTestRule pWcWteTestRule,
			CaslFoundationTestRule pCaslTestRule
			)
	{
		fLog = pLog;

		fWcWteTestRule = pWcWteTestRule;
		
		fCaslTestRule = pCaslTestRule;
	}

	/**
	 * @return the logger
	 */
	protected Logger getLog()
	{
		return fLog;
	}

	/**
	 * @return the sessionFactory
	 */
	protected WebSession getSession()
	{
		return fWcWteTestRule.getSession();
	}

	/**
	 * @return the config
	 */
	protected WcConfigManager getConfig()
	{
		return fWcWteTestRule.getConfig();
	}
}
