package com.ibm.commerce.qa.aurora.tests;
/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.IBMCopyright;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/** Test scenario to test Multiple words search in store basic search				
 *  Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_SOLR_SEARCH_13 extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = IBMCopyright.SHORT_COPYRIGHT;

	//A variable to hold the name of the data file where input parameters can be found.
	//$ANALYSIS-IGNORE
	protected final String dataFileName = "src/data/FV2STOREB2C_SOLR_SEARCH_13_Data.xml";

	@DataProvider
	private final TestDataProvider dsm;
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dsm
	 */
	@Inject
	public FV2STOREB2C_SOLR_SEARCH_13(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dsm)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dsm;
	}
	
	/** Test case to test Multiple words search in store basic search
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1301() throws Exception 
	{
		dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1301", "testFV2STOREB2C_SOLR_SEARCH_1301_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//search for a product
		SearchResultsPage searchResultsPage = aurorafrontPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCHTERM_1"));
		
		//check for results
		searchResultsPage.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		searchResultsPage.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_2"));
		searchResultsPage.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_3"));
	}
	

	/** Test case to test Multiple words search with double quotation
	 * Post conditions: Search results page displays the product match for search keyword.
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1302() throws Exception 
	{
		dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1302", "testFV2STOREB2C_SOLR_SEARCH_1302_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//search for a product
		String search_term = dsm.getInputParameter("SEARCHTERM_1");
		ProductDisplayPage productDisplayPage = aurorafrontPage.getHeaderWidget().performSearch(ProductDisplayPage.class, "\"" + search_term + "\"");

		//check for results
		productDisplayPage.getNamePartNumberAndPriceWidget().verifyCatEntryName(dsm.getInputParameter("EXPECTED_RESULT_1"));
	}
		
}