package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CustomerRegisterPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.PreviewReviewPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.page.SignInPage;
import com.ibm.commerce.qa.aurora.page.WriteReviewPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 * Scenario: FV2STOREB2C_60
 * Details: BazaarVoice Create and Submit a Review
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_60 extends AbstractAuroraSingleSessionTests
{
	static String productName = null;
		
	@DataProvider
	private final TestDataProvider dsm;
	private final CMC cmc;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param cmc CMC object
	 */
	@Inject
	public FV2STOREB2C_60(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,CMC cmc)
	{
		super(log, wcWebTestRule, caslTestRule);
		
		this.dsm = dataSetManager;
		this.cmc = cmc;
	}
	
	 /**
	 * Set the environment up ensuring Rating & Reviews feature is enabled. 
	 * 
	 * @throws Exception 
	 */
	@Before
	 public void setUp() throws Exception{		
		productName = dsm.getInputParameter("PRODUCTNAME");
		String cmcUserID = dsm.getInputParameter("CMCLOGONID");
		String cmcPassword = dsm.getInputParameter("CMCPASSWORD");
		bazaarvoiceFlexFlowOption(cmcUserID,cmcPassword,true);
	}
	
	 /**
	 *  Clean up.
	 * 
	 * @throws Exception 
	 */
	@After
	 public void tearDown() throws Exception{
	 }
	
	/**
	 * 
	 * To verify that a registered, logged-in shopper can create a review from the review summary widget
	 * 
	 * This test case will test Bazaarvoice write a review feature when user is already logged in to Commerce. 
	 * The test case will ensure that user is directed to the write a review page. 
	 * 
	 * 
	 * @throws Exception 
	 * 
	 */
	@Test
	public void testFV2STOREB2C_6001() throws Exception
	{	
		final long random = Math.round(Math.random()*10000); //4 digit random
		String userID = dsm.getInputParameter("LOGONID") +random;
		registerNewReviewUser(userID);
				
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		SignInDropdownWidget   signIn  = frontPage.getHeaderWidget().signIn();
		signIn.typeUsername(userID);
		signIn.typePassword(dsm.getInputParameter("PASSWORD"));
		signIn.signIn().closeSignOutDropDownWidget();	
		
		SearchResultsPage searchResults = frontPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		ProductDisplayPage productDisplay = searchResults.getCatalogEntryListWidget().goToProductPageByName(productName);	
			
		WriteReviewPage writeReview = productDisplay.writeAReviewNoAuthRequired();
		populateWriteReviewData(writeReview, userID);
		PreviewReviewPage previewReviewPage = writeReview.preview(); 
		previewReviewPage.submit();
	}
	
	/**
	 * To verify that a registered, not logged-in shopper can create a review from the review list
	 * 
	 * This test case will test Bazaarvoice write a review feature when user is not already logged
	 * in to Commerce. The test case will ensure that user is directed to the write a review page. 
	 * 
	 * @throws Exception 
	 */
	@Test
	public void testFV2STOREB2C_6003() throws Exception
	{
		String requireAuthentication = dsm.getInputParameter("REQUIRE_AUTH");
		
		final long random = Math.round(Math.random()*10000); //4 digit random
		String userID = dsm.getInputParameter("LOGONID") +random;
		MyAccountMainPage myAccountMainPage;
		myAccountMainPage = registerNewReviewUser(userID);
		myAccountMainPage.getHeaderWidget().openSignOutDropDownWidget().signOut();	
		
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		SearchResultsPage searchResults = frontPage.getHeaderWidget().performSearch(SearchResultsPage.class,dsm.getInputParameter("SEARCH_TERM"));
		
		ProductDisplayPage productDisplay = searchResults.getCatalogEntryListWidget().goToProductPageByName(productName);	
	
		WriteReviewPage writeReview;
		if(requireAuthentication.equalsIgnoreCase("true")){
			getLog().info("USER requireAuthentication : " + requireAuthentication);
			SignInPage   signIn = productDisplay.writeAReview();	
			signIn.typeUsername(userID);
			signIn.typePassword(dsm.getInputParameter("PASSWORD"));
			writeReview = signIn.signInToWriteReviewPage();
			
		}else{
			getLog().info("USER requireAuthentication : " + requireAuthentication);
			writeReview = productDisplay.writeAReviewNoAuthRequired();
		}
		
		populateWriteReviewData(writeReview, userID);		
		PreviewReviewPage previewReviewPage = writeReview.preview(); 
		previewReviewPage.submit();		
	}
	
	
	/**
	 * registerNewReviewUser() creates new user for use in the Bazaarvoice test case. 
	 * 
	 * @param userIDToCreate user id to create.
	 * @return MyAccountMainPage
	 * @throws Exception
	 */
	public MyAccountMainPage registerNewReviewUser(String userIDToCreate) throws Exception
	{
		getLog().info("Enter registerNewReviewUser() : " + userIDToCreate );
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();

		CustomerRegisterPage customerRegPage = signInPage.registerCustomer();
		MyAccountMainPage myAccountMainPage = customerRegPage.typeLogonId(userIDToCreate)
		.verifyPreferredLanguageDropDownListPresent()
		.verifyPreferredCurrencyDropDownListPresent()
		.typePassword(dsm.getInputParameter("PASSWORD"))
		.typeVerifyPassword(dsm.getInputParameter("PASSWORD"))
		.typeLastName(dsm.getInputParameter("LASTNAME"))
		.typeCity(dsm.getInputParameter("CITY"))
		.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
		.typeZipCode(dsm.getInputParameter("ZIP"))
		.typeEmail(dsm.getInputParameter("EMAIL"))
		.typeFirstName(userIDToCreate)
		.typePhoneNumber(dsm.getInputParameter("PHONE"))
		.selectGender(dsm.getInputParameter("GENDER"))
		.submit();
		return myAccountMainPage;
	}

	/**
	 * populateWriteReviewData() populates the "Write A Review" page with required content
	 * such as rating, the review, and nickname.
	 * 
	 * @param writeReview Write A Review page. 
	 * @param nickName the nickname to use when creating this review.
	 */
	public void populateWriteReviewData(WriteReviewPage writeReview, String nickName){
		getLog().info("Enter populateWriteReviewData() : " + writeReview +", " + nickName );
		writeReview.rateProduct(dsm.getInputParameter("RATING"));
		writeReview.rateValue(dsm.getInputParameter("VALUE"));
		writeReview.rateQuality(dsm.getInputParameter("QUALITY"));
		writeReview.typeSummary(dsm.getInputParameter("SUMMARY"));
		writeReview.typeReview(dsm.getInputParameter("REVIEW"));
		writeReview.typeNickname(nickName);
		writeReview.uploadPhotoButton();
	}

	/**
	 * Enable or disable Bazaarvoice flexflow option.
	 * 
	 * @param cmcUserid CMC Administrator userid.
	 * @param cmcPasswqord CMC Administrator password 
	 * @param enable whether to enable or disable the feature 
	 * @throws Exception 
	 */
	public void bazaarvoiceFlexFlowOption(String cmcUserid, String cmcPasswqord, Boolean enable) throws Exception
	{
		String[] featureName = {"RatingReviewIntegration"};
				
		cmc.logon(cmcUserid, cmcPasswqord);
		if(enable){
			cmc.modifyChangeFlowOptions(featureName, null);	
		}else{
			cmc.modifyChangeFlowOptions(null,featureName);
		}
		
		cmc.logoff();
			
	}

}
