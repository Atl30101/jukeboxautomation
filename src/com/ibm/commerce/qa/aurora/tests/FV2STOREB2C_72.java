package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2013
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


import java.util.logging.Logger;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AdvancedSearchPage;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/** Test scenario to Advanced Search
 *  Refer to each test case for a detailed use case description
 */

@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_72 extends AbstractAuroraSingleSessionTests  
{		
	//A Variable to retrieve data from the data file.
		@DataProvider
		private final TestDataProvider dsm;
	
	/**
	 * @param log
	 * @param config 
	 * @param session
	 * @param dataSetManager 
	 */
	@Inject
	public FV2STOREB2C_72(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
	}

/** Test case to test Advanced Search with price range
		 * Post conditions: The search results returns product matches, as well as a list of matches for unstructured content.  
 */
	@Test
	public void testFV2STOREB2C_72_01() 
	{
	
		//Open store
		AuroraFrontPage auroraFrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
				
		//click Advanced Search link
		AdvancedSearchPage advancedsearchpage=auroraFrontPage.getFooterWidget().goToAdvancedSearchPage();
				
		//Enter search term
		advancedsearchpage.typeSearchFor(dsm.getInputParameter("SEARCHTERM_1"));
		
		//Enter the price from
		advancedsearchpage.typeMinimumPrice(dsm.getInputParameter("PRICE_FROM"));
				
		//Enter the price to
		advancedsearchpage.typeMaximumPrice(dsm.getInputParameter("PRICE_TO"));
				
		//click search button
		SearchResultsPage searchresultpage=advancedsearchpage.submitSearchButton();
			
		//verify the results are present
		searchresultpage.verifyTextPresent(dsm.getInputParameter("SEARCHTERM_1"));
		
		//verify the results are selected correctly
		searchresultpage.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		searchresultpage.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_2"));
		
		//Select Invalid Price range
		searchresultpage.getFooterWidget().goToAdvancedSearchPage();
		//Enter search term
		advancedsearchpage.typeSearchFor(dsm.getInputParameter("SEARCHTERM_1"));
				
		//Enter the price from
		advancedsearchpage.typeMinimumPrice(dsm.getInputParameter("PRICE_FROM_INVALID"));
						
		//Enter the price to
		advancedsearchpage.typeMaximumPrice(dsm.getInputParameter("PRICE_TO_INVALID"));
		
		//click search button
	     advancedsearchpage.submitUnsuccessfulSearchButton();
		
		//Verify invalid price range
		advancedsearchpage.verifyInvalidPriceRangeTooltipPresent();
		
	}

/** Test case to test Advanced Search all words
	 * Post conditions: The search results returns product matches, as well as a list of matches for unstructured content.   
	 
*/
	
	@Test
	public void testFV2STOREB2C_72_02() 
	{
	
		//Open store
		AuroraFrontPage auroraFrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
						
		//click Advanced Search link
		AdvancedSearchPage advancedsearchpage=auroraFrontPage.getFooterWidget().goToAdvancedSearchPage();
						
		//Enter search term
		advancedsearchpage.typeSearchFor(dsm.getInputParameter("SEARCHTERM_1"));
		advancedsearchpage.selectSearchForType(dsm.getInputParameter("SEARCHTERM_TYPE"));
			
		//Enter Search field
		advancedsearchpage.selectLocatedIn(dsm.getInputParameter("SEARCH_TYPE"));
			
		//Enter the price from
		advancedsearchpage.typeMinimumPrice(dsm.getInputParameter("PRICE_FROM"));
			
		//Enter the price to
		advancedsearchpage.typeMaximumPrice(dsm.getInputParameter("PRICE_TO"));
			
		//click search button
		SearchResultsPage searchresultpage=advancedsearchpage.submitSearchButton();
				
		///verify the results are present
		searchresultpage.verifyTextPresent(dsm.getInputParameter("SEARCHTERM_1"));
		searchresultpage.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
	}

/** Test case to test Advanced Search any words
	 * Post conditions: The search results returns product matches, as well as a list of matches for unstructured content. 
	 
*/
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_72_03() 
	{
		
		//Open store
		AuroraFrontPage auroraFrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
				
		//click Advanced Search link
		AdvancedSearchPage advancedsearchpage=auroraFrontPage.getFooterWidget().goToAdvancedSearchPage();
				
		//Enter search term
		advancedsearchpage.typeSearchFor(dsm.getInputParameter("SEARCHTERM_1"));
		
		//Enter Search field product name and description
		advancedsearchpage.selectLocatedIn(dsm.getInputParameter("SEARCH_TYPE"));
		
		//Enter the price from
		advancedsearchpage.typeMinimumPrice(dsm.getInputParameter("PRICE_FROM"));
				
		//Enter the price to
		advancedsearchpage.typeMaximumPrice(dsm.getInputParameter("PRICE_TO"));
			
		
		
		//click search button
		SearchResultsPage searchresultpage=advancedsearchpage.submitSearchButton();
			
		//verify the results are present
		searchresultpage.verifyTextPresent(dsm.getInputParameter("SEARCHTERM_1"));
		
		//verify the results are selected correctly
		searchresultpage.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		searchresultpage.getCatalogEntryListWidget().verifyProductIsNotPresent(dsm.getInputParameter("EXPECTED_RESULT_2"));
				
		//Go to Advanced search Page
		searchresultpage.getFooterWidget().goToAdvancedSearchPage();
		
		//Enter search term
		advancedsearchpage.typeSearchFor(dsm.getInputParameter("SEARCHTERM_1"));
				
		//Enter Search field product name and description
		advancedsearchpage.selectLocatedIn(dsm.getInputParameter("SEARCH_TYPE"));
		
		//Enter the price from
		advancedsearchpage.typeMinimumPrice(dsm.getInputParameter("PRICE_FROM"));
				
		//Enter the price to
		advancedsearchpage.typeMaximumPrice(dsm.getInputParameter("PRICE_TO"));
		
		//Enter Excluded word
         advancedsearchpage.typeExclude(dsm.getInputParameter("EXCLUDED_WORD"));
         
        //click search button
 	     searchresultpage=advancedsearchpage.submitSearchButton();
         
       //verify the results are selected correctly   
       //verify the results are present
 		searchresultpage.verifyTextPresent(dsm.getInputParameter("SEARCHTERM_1"));
         
         
 		searchresultpage.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_3"));
 		searchresultpage.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_4"));
		
	}	

/** Test case to test Advanced Search Exact phrase
	 * Post conditions: The search results returns product matches, as well as a list of matches for unstructured content. 
	 
*/
	
	@Test
	public void testFV2STOREB2C_72_04() 
	{
	
		//Open store
		AuroraFrontPage auroraFrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
				
		//click Advanced Search link
		AdvancedSearchPage advancedsearchpage=auroraFrontPage.getFooterWidget().goToAdvancedSearchPage();
				
		//Enter search term
		advancedsearchpage.typeSearchFor(dsm.getInputParameter("SEARCHTERM_1"));
		advancedsearchpage.selectSearchForType(dsm.getInputParameter("SEARCHTERM_TYPE"));
		
		//Enter Search field product name and description
		advancedsearchpage.selectLocatedIn(dsm.getInputParameter("SEARCH_TYPE"));
					
		
		//click search button
		SearchResultsPage searchresultpage=advancedsearchpage.submitSearchButton();
			
		//verify the results are present
		searchresultpage.verifyTextPresent(dsm.getInputParameter("SEARCHTERM_1"));
		searchresultpage.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		
					
			
	}	
/** Test case to test Advanced Search in Specified departments
	 * Post conditions: The search results returns product matches, as well as a list of matches for unstructured content. 
	 
*/
	
	@Test
	public void testFV2STOREB2C_72_06() 
	{
	
		//Open store
		AuroraFrontPage auroraFrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
				
		//click Advanced Search link
		AdvancedSearchPage advancedsearchpage=auroraFrontPage.getFooterWidget().goToAdvancedSearchPage();
				
		//Enter search term
		advancedsearchpage.typeSearchFor(dsm.getInputParameter("SEARCHTERM_1"));
		advancedsearchpage.selectSearchForType(dsm.getInputParameter("SEARCHTERM_TYPE"));
	
		//Choose Catagory
		advancedsearchpage.selectSearchIn(dsm.getInputParameter("CATAGORY"));
		
	
		//click search button
		SearchResultsPage searchresultpage=advancedsearchpage.submitSearchButton();
			
		//verify the results are present
		searchresultpage.verifyTextPresent(dsm.getInputParameter("SEARCHTERM_1"));
	
		//verify the results are selected correctly
		searchresultpage.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		
				
		
	}
	
	
/** Test case to test Advanced Search in Specified departments , brands
	 * Post conditions: The search results returns product matches, as well as a list of matches for unstructured content. 
	 
*/
	
	@Test
	public void testFV2STOREB2C_72_07() 
	{
	
		//Open store
		AuroraFrontPage auroraFrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
				
		//click Advanced Search link
		AdvancedSearchPage advancedsearchpage=auroraFrontPage.getFooterWidget().goToAdvancedSearchPage();
				
		//Enter search term
		advancedsearchpage.typeSearchFor(dsm.getInputParameter("SEARCHTERM_1"));
		advancedsearchpage.selectSearchForType(dsm.getInputParameter("SEARCHTERM_TYPE"));
	
		//Choose Catagory
		advancedsearchpage.selectSearchIn(dsm.getInputParameter("CATAGORY"));
		
		//Enter Search field product name and description
		 advancedsearchpage.selectLocatedIn(dsm.getInputParameter("SEARCH_TYPE"));
		 
		//Enter the brand
		advancedsearchpage.typeBrands(dsm.getInputParameter("BRANDS"));
		
	
		//click search button
		SearchResultsPage searchresultpage=advancedsearchpage.submitSearchButton();
			
		//verify the results are present
		searchresultpage.verifyTextPresent(dsm.getInputParameter("SEARCHTERM_1"));
	
		//verify the results are selected correctly
		searchresultpage.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		
				
		
	}
	
	
/** Test case to test Advanced Search -Specify number of results per page
	 * Post conditions: The search results returns product matches, as well as a list of matches for unstructured content. 
	 
*/
	
	@Test
	public void testFV2STOREB2C_72_08() 
	{
	
		//Open store
		AuroraFrontPage auroraFrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
				
		//click Advanced Search link
		AdvancedSearchPage advancedsearchpage=auroraFrontPage.getFooterWidget().goToAdvancedSearchPage();
				
		//Enter search term
		advancedsearchpage.typeSearchFor(dsm.getInputParameter("SEARCHTERM_1"));
		advancedsearchpage.selectSearchForType(dsm.getInputParameter("SEARCHTERM_TYPE"));
			
		 //Number of result per page is 12
		advancedsearchpage.selectNumberOfResults(dsm.getInputParameter("PAGENUMBER"));
		
		//click search button
		SearchResultsPage searchresultpage=advancedsearchpage.submitSearchButton();
			
		//verify the results are present
		searchresultpage.verifyTextPresent(dsm.getInputParameter("SEARCHTERM_1"));
	
		//verify the results are selected correctly
		searchresultpage.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		
		//Go to Advanced search page with result per page is 20
		searchresultpage.getFooterWidget().goToAdvancedSearchPage();
		
		//Enter search term
		advancedsearchpage.typeSearchFor(dsm.getInputParameter("SEARCHTERM_2"));
		advancedsearchpage.selectSearchForType(dsm.getInputParameter("SEARCHTERM_TYPE"));
					
		 //Number of result per page is 20
		 advancedsearchpage.selectNumberOfResults(dsm.getInputParameter("PAGENUMBER_1"));
		 
		//click search button
		searchresultpage=advancedsearchpage.submitSearchButton();
		 
		//verify the results are present
		searchresultpage.verifyTextPresent(dsm.getInputParameter("SEARCHTERM_2"));
		
		//verify the results are selected correctly
		searchresultpage.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		
		//Go to Advanced search page with result per page is 40
         searchresultpage.getFooterWidget().goToAdvancedSearchPage();
		
		//Enter search term
		advancedsearchpage.typeSearchFor(dsm.getInputParameter("SEARCHTERM_3"));
		advancedsearchpage.selectSearchForType(dsm.getInputParameter("SEARCHTERM_TYPE"));
					
		 //Number of result per page is 40
		 advancedsearchpage.selectNumberOfResults(dsm.getInputParameter("PAGENUMBER_2"));
		 
		//click search button
		searchresultpage=advancedsearchpage.submitSearchButton();
		 
		//verify the results are present
		searchresultpage.verifyTextPresent(dsm.getInputParameter("SEARCHTERM_3"));
		
		//verify the results are selected correctly
		searchresultpage.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));		
		
	}	
	
	
	
}