package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2010
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.FooterWidget;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.StoreManagement;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/** 		
 * FV2STOREB2C_36
 * The objective of this test scenario is testing flex flow option 'Enable/Disable Quick Order' in store front.
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_36 extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	@DataProvider(apolloDSM=true)
	
	//A Variable to retrieve data from the data file.
	private final TestDataProvider dsm;
	
	//A Variable to enable/disable flex flow options from CMC.
	private final StoreManagement storeManagement;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param storeManagement
	 * 			   object to work with CMC flex flow options
	 */
	@Inject
	public FV2STOREB2C_36(Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			StoreManagement storeManagement){
		
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		this.storeManagement = storeManagement;
	}

	/**
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		//Setup appropriate change flow options
		storeManagement.modifyChangeFlowOptions(null, new String[] {dsm.getInputParameter("QUICK_ORDER_FLOW"), 
				dsm.getInputParameter("TRACKING_FLOW")});
	}
	
	/**
	 * Test case to Enable/Disable Quick Order (enabled by default).
	 * 
	 * @throws Exception 
	 */
	@Test
	public void testFV2STOREB2C_3601() throws Exception
	{
		
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Open Sign In/Register page
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
				
		//Enter account User Name/Password and sign in
		HeaderWidget header = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD")).signIn().closeSignOutDropDownWidget();
		
		//Clicks on the home link
		frontPage = header.goToFrontPage();

		//Get the footer section object
		FooterWidget footerSection = frontPage.getFooterWidget();
		
		//verify that quick order link is present in Header.
		footerSection.verifyQuickOrderNotPresent();

		//Return to the parent front page object
		frontPage = footerSection.returnToParentPage(AuroraFrontPage.class);
		
		//Sign out from user account.
		frontPage.getHeaderWidget().openSignOutDropDownWidget().signOut();
		
		//Enable quick order
		storeManagement.enableChangeFlowOption(dsm.getInputParameter("QUICK_ORDER_FLOW"));

		//Open the store in the browser
		frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Open Sign In/Register page
		signInPage = frontPage.getHeaderWidget().signIn();
				
		//Enter account User Name/Password and sign in
		header = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD")).signIn().closeSignOutDropDownWidget();
		
		//Clicks on the home link.
		frontPage = header.goToFrontPage();

		//Get the pages footer section
		footerSection = frontPage.getFooterWidget();
		
		//Verify that quick order link is visible
		footerSection.verifyQuickOrderPresent()
		
		//Open quick order page.
		.goToQuickOrderPage();
		
	}
	 
	/**
	 * Tear down ran after every test case
	 * @throws Exception 
	 */
	@After
	public void tearDown() throws Exception{
	try
	{
		//Perform change flow modification
		storeManagement.modifyChangeFlowOptions(new String[] {dsm.getInputParameter("QUICK_ORDER_FLOW"), 
				dsm.getInputParameter("TRACKING_FLOW")}, null);
	}
	catch(RuntimeException e)
	{
		getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
	}
	}
 }
