package com.ibm.commerce.qa.aurora.tests;


	/*
	 *-----------------------------------------------------------------
	 * Licensed Materials - Property of IBM
	 *
	 * 
	 *
	 * WebSphere Commerce
	 *
	 * (C) Copyright IBM Corp. 2012
	 *
	 * US Government Users Restricted Rights - Use, duplication or
	 * disclosure restricted by GSA ADP Schedule Contract with
	 * IBM Corp.
	 *-----------------------------------------------------------------
	 */

	import java.util.logging.Logger;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.casl.keys.CaslKeysFactory;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.CustomerServiceMainPage;
import com.ibm.commerce.qa.aurora.page.NonRegisteredShippingBillingInfoPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.CustomerServiceFindOrderWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.wte.util.WcConfigManager;


	/** 
	 * Test scenario to test various use cases associated with Dropdown menu context
	 * Refer to each test case for a detailed use case description
	 */
	@RunWith(GuiceTestRunner.class)
	@TestModules(AuroraModule.class)
	public class FSTOREB2CCSR_06 extends AbstractAuroraSingleSessionTests
	{
		/**
		 * The internal copyright field.
		 */
		public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

		//A Variable to retrieve data from the data file. 
		@DataProvider
		private final TestDataProvider dsm;
		private final CaslFixturesFactory f_CaslFixtures;
		

		
		/**
		 * Test Class object constructor.
		 * 
		 * @param log
		 * 			   logging object 
		 * @param config
		 * 			   object to work with getConfig().properties file
		 * @param session
		 * 			   factory to create browser sessions
		 * @param dataSetManager
		 * 			   object to work with data files
		 * @param p_caslFixtures 
		 */		
		@Inject
		public FSTOREB2CCSR_06(
				Logger log, 
				WcConfigManager config,
				WcWteTestRule wcWebTestRule,
				CaslFoundationTestRule caslTestRule,
				TestDataProvider dataSetManager,
				CaslFixturesFactory p_caslFixtures, CaslKeysFactory p_caslKeysFactory)
		{
			super(log, wcWebTestRule, caslTestRule);
			this.dsm = dataSetManager;
			f_CaslFixtures = p_caslFixtures;
		}

		
		
		/** Test case to Search for an order using the shopper's first and last name
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0601()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//Get LOGONID of a shopper that we are trying to locate
			String firstName = dsm.getInputParameter("FIRST_NAME");
			String lastName = dsm.getInputParameter("LAST_NAME");
			
			CustomerServiceMainPage CustomerServicePage = frontPage.getHeaderWidget().goToCustomerService();
			
			CustomerServicePage.getFindOrderWidget()
			.typeFirstName(firstName).typeLastName(lastName).submitSearch()
			.verifyOrderSearchResultIsDisplayed();
			
			CustomerServicePage.getSidebarWidget().gotoFindOrderPage().getFindOrder()
			.typeFirstName(firstName).typeLastName(lastName).submitSearch()
			.verifyOrderSearchResultIsDisplayed();
		}
		

		/** Test case to search for a field that would return zero results
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0602()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//Get LOGONID of a shopper that we are trying to locate
			String firstName = dsm.getInputParameter("ZERO_RESULT_FIRSTNAME");

			
			frontPage.getHeaderWidget().goToCustomerService()
			.getFindOrderWidget()
			.typeFirstName(firstName).submitSearch()
			
			//Verify that search did not return any result
			.verifySearchErrorMessageIsDisplayed();
			
		}
		
		/** Test case to enter invalid firstname and last name that would return zero results
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0603()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//Get LOGONID of a shopper that we are trying to locate
			String firstName = dsm.getInputParameter("INVALID_FIRSTNAME");
			String lastName = dsm.getInputParameter("INVALID_LASTNAME");

			
			frontPage.getHeaderWidget().goToCustomerService()
			.getFindOrderWidget()
			.typeFirstName(firstName)
			.typeLastName(lastName).submitSearch()
			
			//Verify that search did not return any result
			.verifySearchErrorMessageIsDisplayed();
			
		}
		
		/**
		 * Test case to enter all search fields then click Clear Filter
		 */
		@Test
		public void testFSTOREB2CCSR_0604()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			frontPage.getHeaderWidget()
			.goToCustomerService().getFindOrderWidget()
			
			.typeOrderNumber(dsm.getInputParameter("SHOPPER_ORDER_ID"))
			.typeFirstName(dsm.getInputParameter("SHOPPER_FIRST_NAME"))
			.typeLastName(dsm.getInputParameter("SHOPPER_LAST_NAME"))
			.typeFirstName(dsm.getInputParameter("START_DATE"))
			.typeLastName(dsm.getInputParameter("END_DATE"))
			.typeStreetAddressLine1(dsm.getInputParameter("SHOPPER_ADDRESS"))
			.typePhoneNumber(dsm.getInputParameter("SHOPPER_PHONE_NUMBER"))
			.selectCountryOrRegion(dsm.getInputParameter("SHOPPER_COUNTRY"))
			.typeZipCode(dsm.getInputParameter("SHOPPER_ZIPCODE"))
			.typeEmail(dsm.getInputParameter("SHOPPER_EMAIL"))
			.selectStateOrProvince(dsm.getInputParameter("SHOPPER_STATE"))
			.selectStateOrProvince(dsm.getInputParameter("SHOPPER_CITY"))
			
			.submitSearch().clearFilter();
			
		}


		
			

		/** Test case to  Search for a shopper then collapse and expand the search box
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0606()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget()
			.typeFirstName(dsm.getInputParameter("SHOPPER_FIRST_NAME"))
			.submitSearch()
			
			.clickExpandCollapseToggleButton();
				
		}
		
		/** Test case to  Click on a search result to see more information
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0609()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			int index = dsm.getInputParameterAsNumber("POSITION_INDEX",Integer.class);
			
			frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget()
			.typeFirstName(dsm.getInputParameter("SHOPPER_FIRST_NAME"))
			.typeLastName(dsm.getInputParameter("SHOPPER_LAST_NAME"))
			.submitSearch()
			
			//Locate and verify that customer appears in serach result
			 .clickExpandSearchResultButtonByPositon(index)
			 .verifyOrderDetailsExpandedByPosition(index);
		}
		
		
		/** Test case to  View shopper's details
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0610()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			int index = dsm.getInputParameterAsNumber("POSITION_INDEX",Integer.class);
			
			frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget()
			.typeFirstName(dsm.getInputParameter("SHOPPER_FIRST_NAME"))
			.typeLastName(dsm.getInputParameter("SHOPPER_LAST_NAME"))
			.submitSearch()
			
			//Click on action button and click on Access customer account
			.clickActionButtonByPosition(index)
			.clickAccessCustomerAccount();		

		}
		
		
		/** Test case to  Disable shopper's account
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0611()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			int index = dsm.getInputParameterAsNumber("POSITION_INDEX", Integer.class);
			
			frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget()
			.typeFirstName(dsm.getInputParameter("SHOPPER_FIRST_NAME"))
			.typeLastName(dsm.getInputParameter("SHOPPER_LAST_NAME"))
			.submitSearch()
			
			//Click on action button and click on Disable Customer Account
			.clickActionButtonByPosition(index)
			.clickDisableCustomerAccount();
			
		}
		
		/** Test case to enable shopper's account
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0612()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			int index = dsm.getInputParameterAsNumber("POSITION_INDEX",Integer.class);
			
			frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget()
			.typeFirstName(dsm.getInputParameter("SHOPPER_FIRST_NAME"))
			.typeLastName(dsm.getInputParameter("SHOPPER_LAST_NAME"))
			.submitSearch()
			
			//Click on action button and click on Enable Customer Account
			.clickActionButtonByPosition(index)
			.clickEnableCustomerAccount();
		}
		
		
		/** Test case to Search by Shipping Information

		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0613()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget()
			.typeFirstName(dsm.getInputParameter("SHOPPER_FIRST_NAME"))
			.typeLastName(dsm.getInputParameter("SHOPPER_LAST_NAME"))
			.typeStreetAddressLine1(dsm.getInputParameter("SHOPPER_ADDRESS"))
			.submitSearch()
			
			.verifyOrderSearchResultIsDisplayed();
		}
		

		
		/**
		 * Test to search for an order placed by Guest User
		 */
		@Test
		public void testFSTOREB2CCSR_0615() 
		{
			//Open the store in the browser
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
										

			//Select a category and sub category to view the list of products within
			CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CATEGORY_NAME"), 
					 dsm.getInputParameter("SUB_CATEGORY_NAME") , dsm.getInputParameter("CATEGORY_ITEM_NAME"));

			//Select a product from the list of visible products under the chosen sub-category
			ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().
					goToProductPageByImagePosition(Integer.parseInt(dsm.getInputParameter("PRODUCT_NUMBER")));
		
			//Select swatch values
			productDisplayPage.getDefiningAttributesWidget()
			
			.selectAttributeSwatch(dsm.getInputParameter("SWATCH_ATTRIBUTE_1"))
			
			//Select swatch values
			.selectAttributeSwatch(dsm.getInputParameter("SWATCH_ATTRIBUTE_2"));
			
			//Add the item to the cart
			productDisplayPage.addToCart();
			
			//Opens the shopping cart page
			ShopCartPage shopCartPage = productDisplayPage.getHeaderWidget().goToShoppingCartPage();
			
			//Clicks the guest user checkout button
			NonRegisteredShippingBillingInfoPage nonRegisteredShippingBillingPage = shopCartPage.continueAsGuestUser();
			
			//Enter recipient information
			nonRegisteredShippingBillingPage.typeBillRecipient(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
			
			//Enter last name
			.typeBillLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
			
			//Enter billing address
			.typeBillAddress(dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
			
			//Enter billing city
			.typeBillCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
			
			//Enter billing country
			.selectBillCountry(dsm.getInputParameter("ADDRESS_BOOK_COUNTRY"))
			
			//Enter billing province
			.selectBillState(dsm.getInputParameter("ADDRESS_BOOK_STATE"))
			
			//Enter billing phone number
			.typeBillPhone(dsm.getInputParameter("ADDRESS_PHONE_NUMBER"))
			
			//Enter billing zip code
			.typeBillZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
			
			//Enter e-mail address
			.typeBillEmail(dsm.getInputParameter("ADDRESS_BOOK_EMAIL"))
			
			//Click the check box to make the shipping information the same as the billing information
			.applyToBothShippingBilling();
			
			//Opens the shipping and billing method page
			ShippingAndBillingPage shippingAndBillingPage = nonRegisteredShippingBillingPage.submitSuccessfulForm();
			
			//Selects a user-defined billing method
			shippingAndBillingPage.selectPaymentMethod(dsm.getInputParameter("BILLLING_METHOD"));
			
			//Opens the order summary page
			OrderSummarySingleShipPage orderSummaryPage = shippingAndBillingPage.singleShipNext();
			
			//Submits the order
			OrderConfirmationPage orderConfirmationPage = orderSummaryPage.completeOrder();

			//Confirm that the order was successfully submitted
			orderConfirmationPage.verifyOrderSuccessful(dsm.getInputParameter("ORDER_SUCCESSFUL_MESSAGE"));
			
			String guestOrderId = orderConfirmationPage.getOrderNumber();
			
			getSession().state().resetWithCurrentState();
			
			//Open the store in the browser
			frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget()
			.typeOrderNumber(guestOrderId)	.submitSearch().verifyOrderSearchResultIsDisplayed();
			
			
		}
		


		 /**
		  * Test case to  Search for shopper's currently locked order
		  */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0616()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			String orderId = orders.getCurrentOrderId();
			
			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			CustomerServiceFindOrderWidget FindOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget()
			.typeOrderNumber(orderId).submitSearch();
			
			FindOrderWidget.clickActionButton(orderId).clickLockOrder().verifyOrderIsLocked(orderId);
			
			frontPage.getHeaderWidget().goToFrontPage().getHeaderWidget().goToCustomerService().getFindOrderWidget()
					.typeOrderNumber(orderId).submitSearch()
					.verifyOrderSearchResultIsDisplayed()
					
					.clickActionButton(orderId).clickUnlockOrder().verifyOrderIsUnLocked(orderId);
			
			//cleanup shopper's cart
			orders.removeAllItemsFromCart();
		}
		

	
	}
