package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2013
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.IBMCopyright;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AdvancedSearchPage;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.SearchBrandPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.DeltaUpdates;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/** Test scenario to filter search results based on selected facets
 *  Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_SOLR_SEARCH_16 extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = IBMCopyright.SHORT_COPYRIGHT;
	
	//A variable to hold the name of the data file where input parameters can be found.
	//$ANALYSIS-IGNORE
	protected final String dataFileName = "src/data/FV2STOREB2C_SOLR_SEARCH_16_Data.xml";
	

	@DataProvider
	private final TestDataProvider dsm;
	
	
	DeltaUpdates deltaUpdate;

	private CaslFixturesFactory f_CaslFactory;

	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dsm
	 * @param deltaUpdate
	 * @param p_CaslFactory 
	 */
	@Inject
	public FV2STOREB2C_SOLR_SEARCH_16(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dsm,
			DeltaUpdates deltaUpdate,
			CaslFixturesFactory p_CaslFactory)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dsm;
		this.deltaUpdate = deltaUpdate;
		f_CaslFactory = p_CaslFactory;
	}
	
	/**
	 * Test Case to verify that the number of suggested brands get downsized
	 * while the user is typing the keyword
	 * 
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1606() throws Exception 
	{
		dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1606", "testFV2STOREB2C_SOLR_SEARCH_1606_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Type search term - brand (ar)
		HeaderWidget headerSection = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
	
		//verify highlighted string
		headerSection.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_1"));
		
		//check for keywords in search pop up
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_2"));
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_3"));
		
		//continue typing search term - brand (arom)
		//Type search term - brand (ar)
		headerSection = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_2"), false, true);
	
		//verify highlighted string
		headerSection.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_2"));
		
		
		
		//check for keywords in search pop up
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		
		//check for brands that shouldn't be present
		headerSection.verifyKeywordNotInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_2"));
		headerSection.verifyKeywordNotInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_3"));
	}
	
	
	/**	 
	 * Test Case To verify the brand auto suggest feature works from other pages other than the home page, would execute search with the
	 * entered search term	 * 
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1607() 
	{
         dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1607", "testFV2STOREB2C_SOLR_SEARCH_1607_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Type search term - brand 
		HeaderWidget headerSection = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
	
		//verify highlighted string
		headerSection.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_1"));
		
		//check for keywords in search pop up
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_2"));
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_3"));
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_4"));
		
		SearchBrandPage sb=headerSection.selectBrandByName(dsm.getInputParameter("BRANDNAME"));
		sb.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		
		//Go to category page furniture and verify brandname in autosuggest search
		CategoryPage subCat=headerSection.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"),dsm.getInputParameter("SUB_CAT"));
		subCat.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
		
		//verify highlighted string
		headerSection.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_1"));
				
		//check for keywords in search pop up
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_2"));
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_3"));
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_4"));
				
		 sb=headerSection.selectBrandByName(dsm.getInputParameter("BRANDNAME"));
		 sb.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		
		//Go to product page  and verify brandname in autosuggest search
		 subCat=headerSection.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"),dsm.getInputParameter("SUB_CAT"));
		 ProductDisplayPage pd=subCat.getCatalogEntryListWidget().goToProductPagebyIdentifier(dsm.getInputParameter("PRODUCTNAME"));
		 pd=pd.addToCart();
		 pd.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
		//verify highlighted string
		headerSection.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_1"));
					
		//check for keywords in search pop up
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_2"));
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_3"));
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_4"));
					
		sb=headerSection.selectBrandByName(dsm.getInputParameter("BRANDNAME"));
		sb.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		
		//Go To shopcart page and verify brandname in autosuggest search		
		ShopCartPage sp=pd.getHeaderWidget().goToShoppingCartPage().verifyItemInShopCart(dsm.getInputParameter("PRODUCTNAME"));
		sp.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
		//verify highlighted string
		headerSection.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_1"));
							
		//check for keywords in search pop up
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_2"));
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_3"));
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_4"));
							
		sb=headerSection.selectBrandByName(dsm.getInputParameter("BRANDNAME"));
		sb.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		
		sp.getHeaderWidget().goToShoppingCartPage().removeAllItems();
			

	}

	/**	 
	 * To verify that products added to shopping cart from the brand page can be checked out successfully 
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1609() 
	{
         dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1609", "testFV2STOREB2C_SOLR_SEARCH_1609_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		SignInDropdownWidget   signIn = aurorafrontPage.getHeaderWidget().signIn();
		
		signIn.typeUsername(dsm.getInputParameter("USERNAME")).typePassword(dsm.getInputParameter("PASSWORD")).signIn().closeSignOutDropDownWidget();
		
		//Type search term - brand 
		HeaderWidget headerSection = signIn.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
				
		SearchBrandPage sb=headerSection.selectBrandByName(dsm.getInputParameter("BRANDNAME"));
		
		//Click product
		ProductDisplayPage pd=sb.goToProductPagebyIdentifier(dsm.getInputParameter("PRODUCTNAME"));
		
				
		//Add to cart
		pd.addToCart();
		
		//complete order using WC commerce service layer
		OrdersFixture orders = f_CaslFactory.createOrdersFixture(dsm.getInputParameter("USERNAME"), dsm.getInputParameter("PASSWORD"),getConfig().getStoreName());

		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();
		
		}

	/**	 
	 * Test Case To verify that brand suggestion navigation and advanced search for the same brand all return the same result
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1611() 
	{
         dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1611", "testFV2STOREB2C_SOLR_SEARCH_1611_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Type search term - brand (
		HeaderWidget headerSection = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
		
		//Select a brand
		SearchBrandPage sb=headerSection.selectBrandByName(dsm.getInputParameter("BRANDNAME"));
		
		//Verify the result
		sb.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		sb.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_2"));
							
		//Go to Advanced search page
		AdvancedSearchPage advancedsearchpage=sb.getFooterWidget().goToAdvancedSearchPage();
		
		//Enter search term
		advancedsearchpage.typeSearchFor(dsm.getInputParameter("SEARCHTERM_2"));
		
		//Enter  brand 
		advancedsearchpage.typeBrands(dsm.getInputParameter("BRANDNAME"));			
				
		//click search button
		advancedsearchpage.submitSearchButton();
		
		//Verify the result
		sb.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		sb.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_2"));
		

	}
	
	
	
	
	/**
		 * Test Case to verify that performing search with a search term that
		 * matches something in the suggested brands, would execute search with the
		 * entered search term
		 * 
		 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1618() throws Exception 
	{
		dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1618", "testFV2STOREB2C_SOLR_SEARCH_1618_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Type search term
		HeaderWidget headerSection = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
	
		//verify highlighted string
		headerSection.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_1"));
		
		//check for keywords in search pop up
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		
		//Perform search
		SearchResultsPage searchResultsPage = aurorafrontPage.getHeaderWidget().executeSearch(SearchResultsPage.class);
		
		//check for results
		searchResultsPage.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		searchResultsPage.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_2"));
		searchResultsPage.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_3"));
		
		
	}
	
	
	
}