package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.NonRegisteredShippingBillingInfoPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.QuickOrderPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.StoreManagement;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 * Scenario: FV2STOREB2C_19
 * Details: This scenario will test Quick Order for both Registered and Guest Shopper.
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_19 extends AbstractAuroraSingleSessionTests
{
	
	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	private CaslFixturesFactory f_CaslFactory;
	//A Variable to retrieve data from the data file.

	@DataProvider
	private final TestDataProvider dsm;
	
	//A Variable to enable/disable CMC options

	//A Variable to enable/disable flex flow options from CMC.
	private final StoreManagement storeManagement;

		
	

	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param storeManagement
	 * 				object to work the cmc store 
	 * @param p_CaslFixtures 
	 */
	@Inject
	public FV2STOREB2C_19(Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			StoreManagement storeManagement,
			CaslFixturesFactory p_CaslFixtures){
		
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		this.storeManagement = storeManagement;
		f_CaslFactory= p_CaslFixtures;
		
	}
	
	/**
	 * Test to quick order an item with valid SKU and valid quantity, as a registered shopper.
	 * @throws Exception
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_1901() throws Exception
	{	
		
		//Enable quick order if not already enabled.		
		storeManagement.enableChangeFlowOption(dsm.getInputParameter("QUICK_ORDER"));
		
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter a valid username
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				
				//Enter a valid password
				.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
				
				//Click Sign In
				.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//Click on Quick Order link in the footer
		QuickOrderPage quickOrder = myAccount.getFooterWidget().goToQuickOrderPage();
		
		
		//Fill the first row with a SKU and quantity
		//StyleHome Modern Rimmed 3 Piece Sofa Set
		quickOrder.addRowToQuickOrder(Integer.valueOf(dsm.getInputParameter("ROW_NO")), dsm.getInputParameter("SKU_NO"), dsm.getInputParameter("QTY"))
		
		//Click on the order button
		.order()
		
		//Verify the order success message
		.verifySuccessMessage();
		
		
		//complete order using WC commerce service layer
		OrdersFixture orders = f_CaslFactory.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"),getConfig().getStoreName());

		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();
		

	}
	/**
	 * Test to quick order an item with valid SKU and in-valid quantity, as a registered shopper.
	 */
	@Test
	public void testFV2STOREB2C_1902() 
	{
		//must enable quick order from CMC
		//Open Auroraesite store
				AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
				
				//Click on the SignIn page link on the header
				SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
				
				//Enter a valid username
				MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
						
						//Enter a valid password
						.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
						
						//Click Sign In
						.signIn().closeSignOutDropDownWidget().goToMyAccount();
				
				//Click on Quick Order link in the footer
				myAccount.getFooterWidget().goToQuickOrderPage()
				
				//Fill the first row with a SKU and quantity	
				//StyleHome Modern Rimmed 3 Piece Sofa Set
				.addRowToQuickOrder(Integer.valueOf(dsm.getInputParameter("ROW_NO")), dsm.getInputParameter("SKU_NO"), dsm.getInputParameter("QTY"))
				
				//Click on the "Order" button
				.order()
				
				//Verify the tooltip error message
				.verifyErrorTooltipPresent(dsm.getInputParameter("ERROR_MESSAGE"));
	}
	/**
	 * Test to quick order an without entering SKU and quantity, as a registered shopper.
	 */	
	@Test
	public void testFV2STOREB2C_1903()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter a valid username
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				
				//Enter a valid password
				.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
				
				//Click Sign In button
				.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//Click on Quick Order link in the footer
		myAccount.getFooterWidget().goToQuickOrderPage()
		
		//Click on the "Order" button
		.order();
	}
	/**
	 * Test to quick order more than 1 item with valid SKU and valid quantity, as a registered shopper.
	 */
	@Test
	public void testFV2STOREB2C_1904()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter a valid username
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				
				//Enter a valid password
				.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
				
				//Click Sign In Button
				.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//Click on Quick Order link in the footer
		QuickOrderPage quickOrder = myAccount.getFooterWidget().goToQuickOrderPage()

		//Fill the first row with a SKU and quantity	
		//StyleHome Modern Rimmed 3 Piece Sofa Set
		.addRowToQuickOrder(Integer.valueOf(dsm.getInputParameter("ROW_NO_1")), dsm.getInputParameter("SKU_NO_1"), dsm.getInputParameter("QTY_1"))
		
		//Fill the second row with a SKU and quantity	
		.addRowToQuickOrder(Integer.valueOf(dsm.getInputParameter("ROW_NO_2")), dsm.getInputParameter("SKU_NO_2"), dsm.getInputParameter("QTY_2"))
		
		//Click on the order button
		.order()
		
		//Verify the order success message
		.verifySuccessMessage();
		
		//Go to category page from the Department Dropdown 
		CategoryPage subCat = quickOrder.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//Go to the product page of an item
		ProductDisplayPage product = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"))
				
		//.selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE"))
		//Add the item to the cart
		.addToCart();
		
		//Go to the shopping cart from the header
		product.getHeaderWidget().goToShoppingCartPage();
		
		//Click on Quick Order link in the footer
		quickOrder = product.getFooterWidget().goToQuickOrderPage()
				
		//Fill the first row with a SKU and quantity	

		.addRowToQuickOrder(Integer.valueOf(dsm.getInputParameter("ROW_NO_3")), dsm.getInputParameter("SKU_NO_3"), dsm.getInputParameter("QTY_3"))
		
		//Click on the order button
		.order()
		
		//Verify the order success message
		.verifySuccessMessage();
		
		//complete order using WC commerce service layer
		OrdersFixture orders = f_CaslFactory.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"),getConfig().getStoreName());

		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();
		
	}
	/**
	 * Test to quick order some items with valid SKU's and in-valid SKU's, as a registered shopper.
	 */
	@Test
	public void testFV2STOREB2C_1905() {
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter a valid username
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				
				//Enter a valid password
				.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
				
				//Click Sign In button
				.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//Click on Quick Order link in the footer
		myAccount.getFooterWidget().goToQuickOrderPage()
		
		//Fill the first row with a SKU and quantity	
		//StyleHome Modern Rimmed 3 Piece Sofa Set
		.addRowToQuickOrder(Integer.valueOf(dsm.getInputParameter("ROW_NO_1")), dsm.getInputParameter("SKU_NO_1"), dsm.getInputParameter("QTY_1"))
		
		//Fill the second row with a SKU and quantity	
		.addRowToQuickOrder(Integer.valueOf(dsm.getInputParameter("ROW_NO_2")), dsm.getInputParameter("SKU_NO_2"), dsm.getInputParameter("QTY_2"))
		
		//Click on the order button
		.order()
		
		//Verify the invalid SKU error message
		.verifyErrorMessage(dsm.getInputParameter("ERROR_MESSAGE"));		
	}
	/**
	 * Test to quick order some items with in-valid SKU's and in-valid quantity, as a guest shopper.
	 */
	@Test
	public void testFV2STOREB2C_1906()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on Quick Order link in the footer
		frontPage.getFooterWidget().goToQuickOrderPage()
		
		//Fill the first row with a SKU and quantity	
		.addRowToQuickOrder(Integer.valueOf(dsm.getInputParameter("ROW_NO_1")), dsm.getInputParameter("SKU_NO_1"), dsm.getInputParameter("QTY_1"))
		
		//Click on the Order button
		.order()
		
		//Verify the invalid input error message
		.verifyErrorTooltipPresent(dsm.getInputParameter("ERROR_MESSAGE_TOOLTIP"))
		
		//Enter invalid inputs in the same row
		.addRowToQuickOrder(Integer.valueOf(dsm.getInputParameter("ROW_NO_1")), dsm.getInputParameter("SKU_NO_1"), dsm.getInputParameter("QTY_2"))
		
		//Click on the Order Button
		.order()
		
		//Verify the invalid input error message
		.verifyErrorMessage(dsm.getOutputParameter("ERROR_MESSAGE_HEADER"));
	}
	/**
	 * Test to quick order some items with valid SKU's and valid quantity, as a guest shopper.
	 */
	@Test
	public void testFV2STOREB2C_1907()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on Quick Order link in the footer
		QuickOrderPage quickOrder = frontPage.getFooterWidget().goToQuickOrderPage()
				
		//Fill the first row with a SKU and quantity	
		.addRowToQuickOrder(Integer.valueOf(dsm.getInputParameter("ROW_NO")), dsm.getInputParameter("SKU_NO"), dsm.getInputParameter("QTY"))
		
		//Click on the Order button
		.order();
		
		//Verify the item is added to the cart
		quickOrder.getHeaderWidget().verifyMessage(dsm.getInputParameter("MESSAGE_ADDED_TO_SHOPCART"))
		.closeMessageArea();
		
		//Go to Shopping Cart from the header
		ShopCartPage shopCart = quickOrder.getHeaderWidget().goToShoppingCartPage();
		
		//Click the Continue as Guest User button
		NonRegisteredShippingBillingInfoPage nonRegisteredBilling= shopCart.continueAsGuestUser();
		
		//Enter recipient
		ShippingAndBillingPage shipping = nonRegisteredBilling.typeBillRecipient(dsm.getInputParameter("RECIPIENT"))
				
		//Enter last name
		.typeBillLastName(dsm.getInputParameter("LAST_NAME"))
		
		//Enter street address
		.typeBillAddress(dsm.getInputParameter("ADDRESS"))
		
		//Enter city
		.typeBillCity(dsm.getInputParameter("CITY"))
		
		//Enter country
		.selectBillCountry(dsm.getInputParameter("COUNTRY"))
		
		//Enter state
		.selectBillState(dsm.getInputParameter("STATE"))
		
		//Enter zipcode
		.typeBillZipCode(dsm.getInputParameter("ZIP_CODE"))
		
		//Enter E-mail
		.typeBillEmail(dsm.getInputParameter("EMAIL"))
		
		//Enter phone number
		.typeBillPhone(dsm.getInputParameter("PHONE_NUMBER"))
		
		//Check the Apply to both Shipping and Billing checkbox
		.applyToBothShippingBilling()
		
		//Registration successful
		.submitSuccessfulForm();
		
		//Select the billing method
		OrderSummarySingleShipPage orderSummary = shipping.selectPaymentMethod(dsm.getInputParameter("BILLING_METHOD"))
				
		//Continue as single ship 
		.singleShipNext();
		
		//Complete the order
		orderSummary.completeOrder();
	}

	/**
	 * Tear down ran after every test case
	 */
	@After
	public void tearDown() 
	{
		try
		{
			OrdersFixture orders = f_CaslFactory.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"),getConfig().getStoreName());
			orders.removeAllItemsFromCart();
			orders.deletePaymentMethod();
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}
	}
}
