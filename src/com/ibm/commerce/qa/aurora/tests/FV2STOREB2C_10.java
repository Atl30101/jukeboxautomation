package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/**
 * Scenario: FV2STOREB2C_10
 * Details: Test checkout flows.
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_10 extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	@DataProvider(apolloDSM=true)
	
	//A Variable to retrieve data from the data file.
	private final TestDataProvider dsm;
	
	private final CaslFixturesFactory f_CaslFixtures;


	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   factory to work with pages and elements
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_CaslFixtures 
	 */
	@Inject
	public FV2STOREB2C_10(Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_CaslFixtures){
		
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		f_CaslFixtures = p_CaslFixtures;
		
	}	
	
	
	/**
	 * Add to cart as Registered shopper and checkout as Registered shopper
	 * 
	 */
	@Test
	public void testFV2STOREB2C_1001() {
	
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Open Sign In/Register page.
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();

		//Enter account User Name/Password and sign in.
		signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD")).signIn().closeSignOutDropDownWidget();
		
		//Open Sub category Apparel->Dresses.
		CategoryPage subCat = signInPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//Open dress product detail page.
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select product attribute value.
		productDisplayPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SIZE_ATTRIBUTE"));
		
		//Add product to Shopping Cart.
		productDisplayPage.addToCart();
		
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		
		orders.addPayLaterPaymentMethod();

		orders.completeOrder();
		
	}
	
	/**
	 * Test case to add an item to the cart as a guest user and checkout the item as registered shopper.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_1002() {
		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Open Sub category Apparel->Dresses.
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class,dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), 
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//Open dress product detail page.
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select product attribute value.
		productDisplayPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SIZE_ATTRIBUTE"));
		
		//Add product to Shopping Cart.
		productDisplayPage.addToCart();
		
		//Go to Shop Cart detail page.
		ShopCartPage shopCart = productDisplayPage.getHeaderWidget().goToShoppingCartPage();

		//Verify the recently added item is in shopping cart.
		shopCart.verifyItemInShopCart(dsm.getInputParameter("PRODUCT_NAME"));	
		
		//Enter account User Name/Password, sign in, and continue the checkout process.
		shopCart.signInAndContinue(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"));

		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();
	}
	
	/**
	 * Tear down ran after every test case
	 */
	@After
	public void tearDown(){
	
		try
		{
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
			orders.removeAllItemsFromCart();
			orders.deletePaymentMethod();
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

		
	}
	
}