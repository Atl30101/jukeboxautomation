package com.ibm.commerce.qa.aurora.tests;


/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.QuickInfoPopup;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;



/**
 * Scenario: FV2STOREB2C_09
 * Details: Test cases to update items in the shopping cart.
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_09 extends AbstractAuroraSingleSessionTests
{
	
	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	private final CaslFixturesFactory f_CaslFixtures;

	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 */
	@Inject
	public FV2STOREB2C_09(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_CaslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		f_CaslFixtures = p_CaslFixtures;
	}
	
	/**
	 * Test to update the quantity of items in the shopping cart with valid values
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_0901()
	{		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		HeaderWidget headerWidget = signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		//cleanup
		cartCleanup(headerWidget);
		
		//Go to department page				
		CategoryPage subCat = headerWidget.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//click on product image on Category Display page.
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select the product swatches
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR")).selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));
		
		//click Add to Cart from Product Display page.
		productDisplay.addToCart();
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//Confirm the price on minishopping cart
		productDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
		
		//Change the quantity of the item
		shopCart.changeQuantity(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_NEW_QTY"));
		
		shopCart.verifyOrderTotal(dsm.getInputParameter("NEW_TOTAL"));
	}
	
	/**
	 * Test to update the quantity of items in the shopping cart with invalid values 
	 */
	@Test
	public void testFV2STOREB2C_0902() 
	{
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		HeaderWidget headerWidget = signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		//cleanup
		cartCleanup(headerWidget);

		//Go to department page				
		CategoryPage subCat = headerWidget.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//click on product image on Category Display page.
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select the product swatches
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR")).selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));
		
		//click Add to Cart from Product Display page.
		productDisplay.addToCart();
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//Confirm the price on minishopping cart
		productDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
		
		//Change the quantity of the item
		shopCart.changeQuantityRequest(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_NEW_QTY"));
		
		//Verify tool tip present
		shopCart.verifyErrorTooltipPresent(dsm.getInputParameter("ERROR"));
	}
	
	/**
	 * Test to update the attribute(s) of an item in the shopping cart 
	 */
	@Test
	public void testFV2STOREB2C_0903() 
	{
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		HeaderWidget headerWidget = signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		cartCleanup(headerWidget);
		
		//Go to department page				
		CategoryPage subCat = headerWidget.goToCategoryPageByHierarchy(CategoryPage.class,dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//click on product image on Category Display page.
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select the product attribute
		productDisplay.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("ATTRIBUTE_NAME1"),(dsm.getInputParameter("ATTRIBUTE1")));
		
		//Select the product attribute
		productDisplay.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("ATTRIBUTE_NAME2"),(dsm.getInputParameter("ATTRIBUTE2")));
		
		//click Add to Cart from Product Display page.
		productDisplay.addToCart();
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//Confirm the price on minishopping cart
		productDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
		
		//Click the Change Attribute link
		QuickInfoPopup quickInfo = shopCart.displayItemQuickInfo(dsm.getInputParameter("ITEM_POSITION"));
		
		//Change Attributes
		quickInfo.updateValueForAttribute(dsm.getInputParameter("ATTRIBUTE_NAME1a"), dsm.getInputParameter("ATTRIBUTE1_NEW"));
		
		//Change Attributes
		quickInfo.updateValueForAttribute(dsm.getInputParameter("ATTRIBUTE_NAME2a"), dsm.getInputParameter("ATTRIBUTE2_NEW"));
		
		//Click on Add to Cart button
		shopCart = quickInfo.updateCartItem(ShopCartPage.class);
		
	}			
	
	/**
	 * Test to delete an item from the shopping cart when there are more than 1 items in the shopping cart 
	 */
	@Test
	public void testFV2STOREB2C_0905()
	{		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		HeaderWidget headerWidget = signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		cartCleanup(headerWidget);
		
		//Go to department page				
		CategoryPage subCat = headerWidget.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//click on product image on Category Display page.
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select the product swatches
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR")).selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));
		
		//click Add to Cart from Product Display page.
		productDisplay.addToCart();
		
		//Go to store home page
		frontPage = productDisplay.getHeaderWidget().goToFrontPage();
		
		//Go to department page				
		subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT2"), dsm.getInputParameter("SUB_CAT2"));
		
		//click on product image on Category Display page.
		productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME2"));
				
		//click Add to Cart from Product Display page.
		productDisplay.addToCart();
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//Confirm the price on minishopping cart
		productDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
		
		//Remove the first item from the list
		shopCart.removeFromCart(dsm.getInputParameter("ITEM_POSITION"));	
		
		//Verify the order total is updated
		shopCart.verifyOrderTotal(dsm.getInputParameter("TOTAL_NEW"));
		
		//Verify the expected item is in shopping cart
		shopCart.verifyItemInShopCart(dsm.getInputParameter("PRODUCT_NAME2"));
		
		//Verify the item is NOT in shopping cart
		shopCart.verifyItemNotInShopCart(dsm.getInputParameter("PRODUCT_NAME"));
	}
	
	/**
	 * Test to delete an item from the shopping cart when there is only one item in the shopping cart 
	 */
	@Test
	public void testFV2STOREB2C_0906() 
	{
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		HeaderWidget headerWidget = signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		cartCleanup(headerWidget);
		
		//Go to department page				
		CategoryPage subCat = headerWidget.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//click on product image on Category Display page.
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select the product swatches
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR")).selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));
		
		//click Add to Cart from Product Display page.
		productDisplay.addToCart();
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//Confirm the price on minishopping cart
		productDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
		
		//Remove the first item from the list
		shopCart.removeFromCart(dsm.getInputParameter("ITEM_POSITION"));	
		
		//Confirm shop cart is empty
		shopCart.verifyShoppingCartIsEmpty();	
	}
	
	
	/**
	 * Test to view item details from the shopping cart (AJAX checkout enabled)
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_0907()
	{
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		HeaderWidget headerWidget = signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		cartCleanup(headerWidget);
		
		//Go to department page				
		CategoryPage subCat = headerWidget.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//click on product image on Category Display page.
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select the product swatches
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR")).selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));
		
		//click Add to Cart from Product Display page.
		productDisplay.addToCart();
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//Confirm the price on minishopping cart
		productDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
		
		//Confirm product is shown on page
		shopCart.verifyItemInShopCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Go to product display page
		productDisplay = shopCart.goToProductPageByImageNumber(dsm.getInputParameter("ITEM_NUMBER"));
		
		//Confirm product is shown on page
		productDisplay.getNamePartNumberAndPriceWidget().verifyCatEntryName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//click on Shopping Cart link from the header
		shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//remove items from cart
		shopCart.removeAllItems();					
	}

	//cleanup method to remove all items from the cart before proceeding.
	private void cartCleanup(HeaderWidget header)
	{
			try {
				header.verifyMiniCartItemCount("0");
			} catch (Exception e) {
				//cart isn't 0, cleaning up
				header.goToShoppingCartPage().removeAllItems();	
			}
		
	}
	
	/**
	 * Tear down ran after every test case
	 */
	@After
	public void tearDown(){
	
		try
		{
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
			orders.removeAllItemsFromCart();
			orders.deletePaymentMethod();
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

		
	}
	
	
	
}
