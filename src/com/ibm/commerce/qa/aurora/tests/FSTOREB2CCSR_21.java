package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.junit.After;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.casl.keys.CaslKeysFactory;
import com.ibm.commerce.casl.rest.base.RestResponse;
import com.ibm.commerce.casl.rest.base.RestSession;
import com.ibm.commerce.casl.rest.fixtures.CaslRestFixture;
import com.ibm.commerce.casl.rest.fixtures.CaslRestFixtureFactory;
import com.ibm.commerce.casl.rest.fixtures.CaslRestOrderFixture;
import com.ibm.commerce.casl.rest.fixtures.utils.CaslRestFixtureModule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CustomerServiceFindOrderPage;
import com.ibm.commerce.qa.aurora.page.CustomerServiceMainPage;
import com.ibm.commerce.qa.aurora.page.CustomerServiceOrderSummaryPage;
import com.ibm.commerce.qa.aurora.page.MyOrderHistoryPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderDetailPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.CustomerServiceFindOrderWidget;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.rules.impl.DefaultJUnitLifeCycleRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 * Scenario: FV2.STOREB2C.CSR_21
 * Details: Access the Customer Service Order Summary Page
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class, CaslRestFixtureModule.class})
public class FSTOREB2CCSR_21 extends AbstractAuroraSingleSessionTests
{

    /**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	private boolean doTearDown = true;
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	private final CaslFixturesFactory f_CaslFixtures;
	private CaslKeysFactory f_CaslKeysFactory;
	private RestSession f_RestSession;
	private CaslRestFixtureFactory f_CaslRestFixtures;
	
	@ClassRule
	public static final TestRule CLASS_RULE = DefaultJUnitLifeCycleRule.getStaticClassRuleInstance();

	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_CaslFixtures 
	 */	@Inject
	public FSTOREB2CCSR_21(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_CaslFixtures, CaslKeysFactory p_caslKeysFactory,
			RestSession p_RestSession, CaslRestFixtureFactory p_CaslRestFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		f_CaslFixtures = p_CaslFixtures;
		f_CaslKeysFactory = p_caslKeysFactory;
		f_RestSession = p_RestSession;
		f_CaslRestFixtures = p_CaslRestFixtures;
		
	}
	 /** 
	  * Customer Service Representative can access Order Summary Page for a completed order on storefront
	  */
	 @Test
	 public void testFSTOREB2CCSR_2101()
	 {
				doTearDown = false;
				//Place a Subscription Order as a shopper
				OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
				//add item to cart
				orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
				//get OrderId
				String orderId = orders.getCurrentOrderId();
				//Add payment and complete Order
				orders.addPayLaterPaymentMethod();
				orders.completeOrder();
				
				//Open the store in the browser
				AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
						
				//Opens the Sign In page in browser.
				SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
				
				//Login as CSR
				signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
					.signIn();
				
				//Find Completed Order
				CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
				findOrderWidget.typeOrderNumber(orderId).submitSearch();
				findOrderWidget.verifyOrderSearchResultIsDisplayed();
				
				CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget.clickActionButton(orderId).clickOrderSummary();
				//Verify Order is correct
				orderSummaryPage.verifyOrderId(orderId);
				orderSummaryPage.getOrderSummary().verifyOrderedBy(dsm.getInputParameter("SHOPPER_NAME"));	
				orderSummaryPage.getOrderTable().toggleOrderSummary().verifyItemPresent(dsm.getInputParameter("SKU"));
			    
				orderSummaryPage.getComments().toggleOrderComments().getWriteAndDisplayCommentsWidgets().addNewComments(dsm.getInputParameter("COMMENT"));
				Assert.assertTrue("Cancel Order button is not visible on page !", orderSummaryPage.isCancelButtonVisibleOnPage());
		
	 }
	/** 
	  * Customer Service Representative can access Order Summary Page for pending order on storefront
	  */
	 @Test
	 public void testFSTOREB2CCSR_2102()
	 {
				doTearDown = false;
				//Create a Pending Order
				OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
				//add item to cart
				orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
				//get OrderId
				String orderId = orders.getCurrentOrderId();
				
				//Open the store in the browser
				AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
						
				//Opens the Sign In page in browser.
				SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
				
				//Login as CSR
				signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
					.signIn();
				
				//Find Completed Order
				CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
				findOrderWidget.typeOrderNumber(orderId).submitSearch();
				findOrderWidget.verifyOrderSearchResultIsDisplayed();
				
				CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget.clickActionButton(orderId).clickOrderSummary();
				//Verify Order is correct
				orderSummaryPage.verifyOrderId(orderId);
				orderSummaryPage.getOrderSummary().verifyOrderedBy(dsm.getInputParameter("SHOPPER_NAME"));	
				orderSummaryPage.getOrderTable().toggleOrderSummary().verifyItemPresent(dsm.getInputParameter("SKU"));
			    
				orderSummaryPage.getComments().toggleOrderComments().getWriteAndDisplayCommentsWidgets().addNewComments(dsm.getInputParameter("COMMENT"));
				Assert.assertFalse("Cancel Order button is visible on page !", orderSummaryPage.isCancelButtonVisibleOnPage());
				ShopCartPage shopCartPage = orderSummaryPage.clickOnCheckOut();
				shopCartPage.clickLockOrder().continueToNextStep().selectPaymentMethod(dsm.getInputParameter("BILLLING_METHOD")).singleShipNext().completeOrder().verifyOrderSuccessful();
				
	 }
	/** 
	  * Customer Service Representative can access Order Summary Page for a cancelled order on storefront
	  */
	 @Test
	 public void testFSTOREB2CCSR_2103()
	 {
				doTearDown = false;
				//Create a Pending Order
				OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
				//add item to cart
				orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
				//get OrderId
				String orderId = orders.getCurrentOrderId();
				orders.addPayLaterPaymentMethod();
				orders.completeOrder();
				
				//Open the store in the browser
				AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
						
				//Opens the Sign In page in browser.
				SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
				
				//Login as CSR
				signIn.typeUsername(dsm.getInputParameter("CSS_LOGONID")).typePassword(dsm.getInputParameter("CSS_PASSWORD"))
					.signIn();
				
				//Find Completed Order and cancel it
				CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
				findOrderWidget.typeOrderNumber(orderId).submitSearch();
				findOrderWidget.verifyOrderSearchResultIsDisplayed();
				OrderDetailPage orderDetailPage = findOrderWidget.clickOrderID(orderId);
				orderDetailPage = orderDetailPage.getCommentsBar().slideOrderComments().addComment(dsm.getInputParameter("COMMENT")).createParentPage(OrderDetailPage.class);
				MyOrderHistoryPage ordersPage = orderDetailPage.csrCancelsOrder();
				
				//CSR views Cancelled Order Summary Page
				findOrderWidget = ordersPage.getHeaderWidget().gotoCustomerServiceWithTerminateModal().clickYes().getFindOrderWidget();
				findOrderWidget.typeOrderNumber(orderId).submitSearch();
				findOrderWidget.verifyOrderSearchResultIsDisplayed();
				CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget.clickActionButton(orderId).clickOrderSummary();
				//Verify Order is correct
				orderSummaryPage.verifyOrderId(orderId);
				orderSummaryPage.getOrderSummary().verifyOrderedBy(dsm.getInputParameter("SHOPPER_NAME"));	
				orderSummaryPage.getOrderSummary().verifyStatus(dsm.getInputParameter("ORDER_STATUS"));
				orderSummaryPage.getOrderTable().toggleOrderSummary().verifyItemPresent(dsm.getInputParameter("SKU"));
			    
				orderSummaryPage.getComments().toggleOrderComments().getWriteAndDisplayCommentsWidgets().verifyCommentAdded(dsm.getInputParameter("COMMENT"));
				Assert.assertFalse("Cancel button is present on Page!", orderSummaryPage.isCancelButtonVisibleOnPage());
				
	 }
	/** 
	  * Customer Service Representative can access Order Summary Page of a parent and child subscription order on storefront
	  */
	 @Test
	 public void testFSTOREB2CCSR_2104()
	 {
				doTearDown = false;
				//Place a Subscription Order as a shopper
				OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
				//add item to cart
				orders.addItem(dsm.getInputParameter("SUBSCRIPTION_SKU"), 1.0);
				//get ParentOrderId
				String parentorderId = orders.getCurrentOrderId();
				//Add payment and complete Order
				orders.addPayLaterPaymentMethod();
				orders.completeOrder();
				
				
				orders.createChildOrderForSubscription(Integer.parseInt(parentorderId), "N", "N");
				
				//Open the store in the browser
				AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
						
				//Opens the Sign In page in browser.
				SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
				
				//Login as Shopper to get Child Subscription OrderId
				signIn.typeUsername(dsm.getInputParameter("SHOPPER_LOGONID")).typePassword(dsm.getInputParameter("SHOPPER_PASSWORD"))
				.signIn();
				
				OrderDetailPage childOrderPage = frontPage.getHeaderWidget().goToMyAccount().goToFirstOrderDetailPage();
				
				String childOrderId = childOrderPage.getOrderId();
				
				HeaderWidget header = childOrderPage.getHeaderWidget().openSignOutDropDownWidget().signOut();
				
				//Log in to the store as a CSR admin.
				header.signIn().typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
					.signIn();
				
				//Find Parent Order
				CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
				findOrderWidget.typeOrderNumber(parentorderId).submitSearch();
				findOrderWidget.verifyOrderSearchResultIsDisplayed();
				
				CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget.clickActionButton(parentorderId).clickOrderSummary();
				//Verify Order is correct
				orderSummaryPage.verifyOrderId(parentorderId);
				orderSummaryPage.getOrderSummary().verifyOrderedBy(dsm.getInputParameter("SHOPPER_NAME"));	
				orderSummaryPage.getOrderTable().toggleOrderSummary().verifyItemPresent(dsm.getInputParameter("SUBSCRIPTION_SKU"));
			    
				orderSummaryPage.getComments().toggleOrderComments().getWriteAndDisplayCommentsWidgets().addNewComments(dsm.getInputParameter("COMMENT"));
				
				//Find Child Order
				CustomerServiceFindOrderPage findOrderPage = orderSummaryPage.getSidebarWidget().gotoFindOrderPage();
				findOrderWidget = findOrderPage.getFindOrder().typeOrderNumber(childOrderId).submitSearch().verifyOrderSearchResultIsDisplayed();
				
				orderSummaryPage = findOrderWidget.clickActionButton(childOrderId).clickOrderSummary();
				//Verify Order is correct
				orderSummaryPage.verifyOrderId(childOrderId);
				orderSummaryPage.getOrderSummary().verifyOrderedBy(dsm.getInputParameter("SHOPPER_NAME"));	
				orderSummaryPage.getOrderTable().toggleOrderSummary().verifyItemPresent(dsm.getInputParameter("SUBSCRIPTION_SKU"));
			    
		
	 }
	 /** 
	  * Customer Service Representative can access Order Summary Page of a parent and child recurring order on storefront
	  */
	 @Test
	 public void testFSTOREB2CCSR_2105()
	 {
				doTearDown = false;
				//Place a Subscription Order as a shopper
				OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
				//add item to cart
				orders.addItem(dsm.getInputParameter("RECURRING_ITEM_SKU"), 10.0);
				//Add payment and complete Order
				orders.addPayLaterPaymentMethod();
				//get ParentOrderId
				String parentorderId = orders.getCurrentOrderId();
				orders.completeRecurringOrder(dsm.getInputParameterAsNumber("FREQUENCY", Double.class), dsm.getInputParameter("FREQ_UNITS"));
				
				
				orders.createChildOrder(Integer.parseInt(parentorderId), "Y", "Y");
				
				//Open the store in the browser
				AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
						
				//Opens the Sign In page in browser.
				SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
				
				//Login as Shopper to get Child Subscription OrderId
				signIn.typeUsername(dsm.getInputParameter("SHOPPER_LOGONID")).typePassword(dsm.getInputParameter("SHOPPER_PASSWORD"))
				.signIn();
				
				OrderDetailPage childOrderPage = frontPage.getHeaderWidget().goToMyAccount().goToFirstOrderDetailPage();
				
				String childOrderId = childOrderPage.getOrderId();
				
				HeaderWidget header = childOrderPage.getHeaderWidget().openSignOutDropDownWidget().signOut();
				
				//Log in to the store as a CSR admin.
				header.signIn().typeUsername(dsm.getInputParameter("CSS_LOGONID")).typePassword(dsm.getInputParameter("CSS_PASSWORD"))
					.signIn();
				
				//Find Parent Order
				CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
				findOrderWidget.typeOrderNumber(parentorderId).submitSearch();
				findOrderWidget.verifyOrderSearchResultIsDisplayed();
				
				CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget.clickActionButton(parentorderId).clickOrderSummary();
				//Verify Order is correct
				orderSummaryPage.verifyOrderId(parentorderId);
				orderSummaryPage.getOrderSummary().verifyOrderedBy(dsm.getInputParameter("SHOPPER_NAME"));	
				orderSummaryPage.getOrderTable().toggleOrderSummary().verifyItemPresent(dsm.getInputParameter("RECURRING_ITEM_SKU"));
			    
				orderSummaryPage.getComments().toggleOrderComments().getWriteAndDisplayCommentsWidgets().addNewComments(dsm.getInputParameter("COMMENT"));
				
				//Find Child Order
				CustomerServiceFindOrderPage findOrderPage = orderSummaryPage.getSidebarWidget().gotoFindOrderPage();
				findOrderWidget = findOrderPage.getFindOrder().typeOrderNumber(childOrderId).submitSearch().verifyOrderSearchResultIsDisplayed();
				
				orderSummaryPage = findOrderWidget.clickActionButton(childOrderId).clickOrderSummary();
				//Verify Order is correct
				orderSummaryPage.verifyOrderId(childOrderId);
				orderSummaryPage.getOrderSummary().verifyOrderedBy(dsm.getInputParameter("SHOPPER_NAME"));	
				orderSummaryPage.getOrderTable().toggleOrderSummary().verifyItemPresent(dsm.getInputParameter("RECURRING_ITEM_SKU"));
			    
		
	 }
	/** 
	  * Customer Service Representative can access Order Summary Page for pending order on storefront
	  */
	 @Test
	 public void testFSTOREB2CCSR_2106()
	 {
				doTearDown = false;
				//Create an Order
				OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
				//add item to cart
				orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
				//get OrderId
				String orderId = orders.getCurrentOrderId();
				orders.addPayLaterPaymentMethod();
				orders.completeOrder();
				
				//Open the store in the browser
				AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
						
				//Opens the Sign In page in browser.
				SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
				
				//Login as CSR
				signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
					.signIn();
				
				//Find Completed Order and cancel it
				CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
				findOrderWidget.typeOrderNumber(orderId).submitSearch();
				findOrderWidget.verifyOrderSearchResultIsDisplayed();
				OrderDetailPage orderDetailPage = findOrderWidget.clickOrderID(orderId);
				orderDetailPage.verifyOrderNumberPresent(orderId);
				
				orderDetailPage.getHeaderWidget().openSignOutDropDownWidget().signOut();
				
				
	 }
	/** 
	  * Customer Service Representative can access Order Summary Page for order locked by another Customer Service 
	  * Representative on storefront
	  */
	 @Test
	 public void testFSTOREB2CCSR_2107()
	 {		
		 		doTearDown = true;
				//Create a Pending Order
				OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
				//add item to cart
				orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
				//get OrderId
				String orderId = orders.getCurrentOrderId();
				
				//Open the store in the browser
				AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
				
				//Opens the Sign In page in browser.
				SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
				
				//Login as first CSR
				CustomerServiceMainPage customerSrvcPg = signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
					.signIn().goToCustomerService();
				
				//Find Completed Order
				CustomerServiceFindOrderWidget findOrderWidget = customerSrvcPg.getFindOrderWidget();
				findOrderWidget.typeOrderNumber(orderId).submitSearch();
				findOrderWidget.verifyOrderSearchResultIsDisplayed().clickActionButton(orderId).clickLockOrder();
				
				customerSrvcPg.getHeaderWidget().openSignOutDropDownWidget().signOut();
				
				//Login as second CSR
			    signIn = frontPage.getHeaderWidget().signIn();
			    signIn.typeUsername(dsm.getInputParameter("CSR2_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
				.signIn();
			    
			    findOrderWidget = customerSrvcPg.getFindOrderWidget().typeOrderNumber(orderId).submitSearch();
				CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget.clickActionButton(orderId).clickOrderSummary();
				//Verify Order is correct
				orderSummaryPage.verifyOrderId(orderId);
				orderSummaryPage.getOrderSummary().verifyOrderedBy(dsm.getInputParameter("SHOPPER_NAME"));	
				orderSummaryPage.getOrderTable().toggleOrderSummary().verifyItemPresent(dsm.getInputParameter("SKU"));
			    
				Assert.assertFalse("Cancel Order button is visible on page !", orderSummaryPage.isCancelButtonVisibleOnPage());
				ShopCartPage shopCartPage = orderSummaryPage.clickOnCheckOut();
				shopCartPage.getCommentsBar().slideOrderComments().addComment((dsm.getInputParameter("COMMENT"))).createParentPage(ShopCartPage.class);
				shopCartPage.getCommentsBar().slideOrderComments();
				shopCartPage.clickTakeOverLock();
				OrderConfirmationPage confirmPage = shopCartPage.continueToNextStep().selectPaymentMethod(dsm.getInputParameter("BILLLING_METHOD")).singleShipNext().completeOrder().verifyOrderSuccessful();
				confirmPage.getHeaderWidget().openSignOutDropDownWidget().signInAsYourself();
				
				doTearDown = false;
	 }
	/**
		 * Tear down ran after every test case
		 */
		@After
		public void tearDown(){
			
			 if(doTearDown){
			
				OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
				String orderId = orders.getCurrentOrderId();
				String storeId = Integer.toString(f_CaslKeysFactory.createToolingKeys().getStoreIdByName(getConfig().getStoreName()));
				
				CaslRestFixture fixture = f_CaslRestFixtures.createCaslRestFixture(f_RestSession);
				RestResponse response = fixture.sendRestLogonRequest(dsm.getInputParameter("CSR_LOGONID"), dsm.getInputParameter("CSR_PASSWORD"), storeId);
				
				CaslRestOrderFixture orderFixture = f_CaslRestFixtures.createCaslRestOrderFixture(f_RestSession);
				orderFixture.sendRestUnockOrderRequest(orderId, storeId);
				assert response != null;
				System.out.println(response);
				
			 }
		}
}