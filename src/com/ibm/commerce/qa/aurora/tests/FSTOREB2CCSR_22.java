package com.ibm.commerce.qa.aurora.tests;


	/*
	 *-----------------------------------------------------------------
	 * Licensed Materials - Property of IBM
	 *
	 * 
	 *
	 * WebSphere Commerce
	 *
	 * (C) Copyright IBM Corp. 2012
	 *
	 * US Government Users Restricted Rights - Use, duplication or
	 * disclosure restricted by GSA ADP Schedule Contract with
	 * IBM Corp.
	 *-----------------------------------------------------------------
	 */

	import java.util.logging.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.casl.keys.CaslKeysFactory;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CustomerServiceOrderSummaryPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.CustomerServiceFindOrderWidget;
import com.ibm.commerce.qa.aurora.widget.CustomerServiceOrderCommentsWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.Accelerator;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.wte.util.WcConfigManager;


	/** 
	 * Test scenario to test various use cases associated with Dropdown menu context
	 * Refer to each test case for a detailed use case description
	 */
	@RunWith(GuiceTestRunner.class)
	@TestModules(AuroraModule.class)
	public class FSTOREB2CCSR_22 extends AbstractAuroraSingleSessionTests
	{
		/**
		 * The internal copyright field.
		 */
		public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

		//A Variable to retrieve data from the data file. 
		@DataProvider
		private final TestDataProvider dsm;
		private final CaslFixturesFactory f_CaslFixtures;
		//A Variable to perform Accelerator tasks.
		private final Accelerator accelerator;
		

		
		/**
		 * Test Class object constructor.
		 * 
		 * @param log
		 * 			   logging object 
		 * @param config
		 * 			   object to work with getConfig().properties file
		 * @param session
		 * 			   factory to create browser sessions
		 * @param dataSetManager
		 * 			   object to work with data files
		 * @param p_caslFixtures 
		 */		
		@Inject
		public FSTOREB2CCSR_22(
				Logger log, 
				WcConfigManager config,
				WcWteTestRule wcWebTestRule,
				CaslFoundationTestRule caslTestRule,
				TestDataProvider dataSetManager,
				CaslFixturesFactory p_caslFixtures, CaslKeysFactory p_caslKeysFactory,
				Accelerator accelerator)
		{
			super(log, wcWebTestRule, caslTestRule);
			this.dsm = dataSetManager;
			f_CaslFixtures = p_caslFixtures;
			this.accelerator = accelerator;
		}

		
		

		
		/**
		 * Test case to add a non empty order comment 
		 */
		@Test
		public void testFSTOREB2CCSR_2201()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			 
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = frontPage.getHeaderWidget().goToShoppingCartPage();
			
			//Go to shipping and billing page
			ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
			//Confirm the correct Shipping Address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Confirm the correct Billing address is selected
			shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
				.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
							
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
							
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();	
			
			//Open the store in the browser.
			AuroraFrontPage frontPage1 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//Log in as CSR
			frontPage1.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//search for order using the orderId for completed shopping checkout 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage1.getHeaderWidget()
					.goToCustomerService()
					.getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
			findOrderWidget.verifyOrderSearchResultIsDisplayed();
			
			//add a new comment to the order on the Order Summary Page  
			CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget.clickActionButton(orderId).clickOrderSummary();
			orderSummaryPage.getComments().toggleOrderComments()
			.getWriteAndDisplayCommentsWidgets()
			.addNewComments(dsm.getInputParameter("COMMENT"));
			
		  
			
		}

		/**
		 * Test case to add a empty order comment and get appropriate error message 
		 */
		@Test
		public void testFSTOREB2CCSR_2202()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			 
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = frontPage.getHeaderWidget().goToShoppingCartPage();
			
			//Go to shipping and billing page
			ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
			//Confirm the correct Shipping Address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Confirm the correct Billing address is selected
			shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
				.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
							
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
							
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();	
			
			//Open the store in the browser.
			AuroraFrontPage frontPage1 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//Log in as CSR
			frontPage1.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//search for order using the orderId for completed shopping checkout 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage1.getHeaderWidget()
					.goToCustomerService()
					.getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId)
			.submitSearch();
			findOrderWidget.verifyOrderSearchResultIsDisplayed();
			
			//add a new comment to the order on the Order Summary Page  
			CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget.clickActionButton(orderId)
					.clickOrderSummary();
			orderSummaryPage.getComments().toggleOrderComments()
			.getWriteAndDisplayCommentsWidgets()
			.addNewCommentsErrorCheck(" ");
			
			
			//verify error message is correct
			orderSummaryPage.verifyErrorMsgTxt(dsm.getInputParameter("ERROR_MSG"));
			
		}	
		
		/**
		 * Test case to add a non empty order comment with special characters 
		 */
		@Test
		public void testFSTOREB2CCSR_2203()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			 
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = frontPage.getHeaderWidget().goToShoppingCartPage();
			
			//Go to shipping and billing page
			ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
			//Confirm the correct Shipping Address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Confirm the correct Billing address is selected
			shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
				.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
							
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
							
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();	
			
			//Open the store in the browser.
			AuroraFrontPage frontPage1 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//Log in as CSR
			frontPage1.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//search for order using the orderId for completed shopping checkout 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage1.getHeaderWidget()
					.goToCustomerService()
					.getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
			findOrderWidget.verifyOrderSearchResultIsDisplayed();
			
			//add a new comment to the order on the Order Summary Page  
			CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget.clickActionButton(orderId).clickOrderSummary();
			orderSummaryPage.getComments().toggleOrderComments()
			.getWriteAndDisplayCommentsWidgets()
			.addNewComments(dsm.getInputParameter("COMMENT"));
			
		    
			
		}
		
		/**
		 * Test case to add a non empty order comment with 3000 characters 
		 * tests two scenarios when order comment = 3000 characters
		 *  and exceeds 3000 characters 
		 */
		@Test
		public void testFSTOREB2CCSR_2204()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			 
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = frontPage.getHeaderWidget().goToShoppingCartPage();
			
			//Go to shipping and billing page
			ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
			//Confirm the correct Shipping Address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Confirm the correct Billing address is selected
			shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
				.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
							
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
							
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();	
			
			//Open the store in the browser.
			AuroraFrontPage frontPage1 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//Log in as CSR
			frontPage1.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//search for order using the orderId for completed shopping checkout 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage1.getHeaderWidget()
					.goToCustomerService()
					.getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
			findOrderWidget.verifyOrderSearchResultIsDisplayed();
			
			//add a new comment to the order on the Order Summary Page  
			CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget.clickActionButton(orderId).clickOrderSummary();
			
			//add comment with exactly 3000 characters 
			CustomerServiceOrderCommentsWidget comentswidget = orderSummaryPage.getComments().toggleOrderComments();
			comentswidget.getWriteAndDisplayCommentsWidgets()
			.addNewComments(dsm.getInputParameter("COMMENT=3000"));
			
			//add comments that exceeds 3000 characters 
			comentswidget.getWriteAndDisplayCommentsWidgets()
			.addNewCommentsErrorCheck(dsm.getInputParameter("COMMENT>3000"));
			
			//need a different verification method to check error message
			orderSummaryPage.verifyErrorMsgTxt(dsm.getInputParameter("ERROR_MSG"));
			
		}
		
		/**
		 * Test case to add a non empty order comment and then cancel the order
		 */
		@Test
		public void testFSTOREB2CCSR_2205()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			 
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = frontPage.getHeaderWidget().goToShoppingCartPage();
			
			//Go to shipping and billing page
			ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
			//Confirm the correct Shipping Address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Confirm the correct Billing address is selected
			shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
				.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
							
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
							
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();	
			
			//Open the store in the browser.
			AuroraFrontPage frontPage1 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//Log in as CSR
			frontPage1.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//search for order using the orderId for completed shopping checkout 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage1.getHeaderWidget()
					.goToCustomerService()
					.getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
			findOrderWidget.verifyOrderSearchResultIsDisplayed();
			
			//add a new comment to the order on the Order Summary Page  
			CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget.clickActionButton(orderId).clickOrderSummary();
			orderSummaryPage.getComments().toggleOrderComments()
			.getWriteAndDisplayCommentsWidgets()
			.addNewComments(dsm.getInputParameter("COMMENT"));
			
		   orderSummaryPage.clickOnCancel();
		   
		   orderSummaryPage.getOrderSummary().verifyStatus(dsm.getInputParameter("ORDER_STATUS"));
			
		}
		
		/**
		 * Test case to add a non empty order comment and checkout on the order summary page
		 */
		@Test
		public void testFSTOREB2CCSR_2206()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			
			//Open the store in the browser.
			AuroraFrontPage frontPage1 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//Log in as CSR
			frontPage1.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//search for order using the orderId for completed shopping checkout 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage1.getHeaderWidget()
					.goToCustomerService()
					.getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
			findOrderWidget.verifyOrderSearchResultIsDisplayed();
			
			//add a new comment to the order on the Order Summary Page  
			CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget.clickActionButton(orderId).clickOrderSummary();
			orderSummaryPage.getComments().toggleOrderComments()
			.getWriteAndDisplayCommentsWidgets()
			.addNewComments(dsm.getInputParameter("COMMENT"));
			
			
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = orderSummaryPage.clickOnCheckOut();
			
			//Go to shipping and billing page
			ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
			//Confirm the correct Shipping Address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Confirm the correct Billing address is selected
			shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
				.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
							
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
							
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();	
		}
		
		/**
		 * Test case to add a non empty order comment check out by another CSR user 
		 */
		@Test
		public void testFSTOREB2CCSR_2208()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			
			//Open the store in the browser.
			AuroraFrontPage frontPage1 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//Log in as CSR
			frontPage1.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//search for order using the orderId for completed shopping checkout 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage1.getHeaderWidget()
					.goToCustomerService()
					.getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
			findOrderWidget.verifyOrderSearchResultIsDisplayed();
			
			findOrderWidget.clickActionButton(orderId).clickLockOrder();
			
			//Open the store in the browser.
			AuroraFrontPage frontPage2 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//Log in as another CSR user 
			frontPage2.signIn(dsm.getInputParameter("LOGONID2"), dsm.getInputParameter("PASSWORD"));
			
			//search for order using shopper's first and last names
			findOrderWidget.typeFirstName(dsm.getInputParameter("SHOPPER_FIRSTNAME"))
			.typeLastName(dsm.getInputParameter("SHOPPER_LASTNAME"))
			.submitSearch();
			
			//add a new comment to the order on the Order Summary Page  
			CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget.clickActionButton(orderId).clickOrderSummary();
			orderSummaryPage.getComments().toggleOrderComments()
			.getWriteAndDisplayCommentsWidgets()
			.addNewComments(dsm.getInputParameter("COMMENT"));
			
			
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = orderSummaryPage.clickOnCheckOut();
			
			//second CSR users takes over the lock on the shopping cart page
			shopCart.clickTakeOverLock();
			
			//Go to shipping and billing page
			ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
			//Confirm the correct Shipping Address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Confirm the correct Billing address is selected
			shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
				.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
							
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
							
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();
		}

		/**
		 * Test case to add an order comment from accelerator and view it on the order summary page
		 * 
		 */
		@Test
		public void testFSTOREB2CCSR_2209() throws Exception
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			orders.addPayLaterPaymentMethod();
			orders.completeOrder();
			
			dsm.setDataBlock("acceleratorLogon");
			//Login to accelerator
			accelerator.logon(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"), getConfig().getStoreName(), getConfig().getDefaultLangId());
			accelerator.cancelOrderB2C(orderId);
		
			//Open the store in the browser.
			AuroraFrontPage frontPage1 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			dsm.setDataBlock("testFSTOREB2CCSR_2209");
			//Log in as CSR
			frontPage1.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//search for order using the orderId for completed shopping checkout 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage1.getHeaderWidget()
					.goToCustomerService()
					.getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
			findOrderWidget.verifyOrderSearchResultIsDisplayed();
			
			CustomerServiceOrderSummaryPage orderSummary = findOrderWidget.clickActionButton(orderId).clickOrderSummary();
			orderSummary.getOrderSummary().verifyStatus(dsm.getInputParameter("ORDER_STATUS"));
			orderSummary.getComments().toggleOrderComments().getWriteAndDisplayCommentsWidgets().verifyCommentAdded(dsm.getInputParameter("COMMENTBY"));
			
		}
	}
