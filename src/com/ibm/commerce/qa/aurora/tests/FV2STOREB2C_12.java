package com.ibm.commerce.qa.aurora.tests;


/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderSummaryMultipleShipPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.RegisteredShippingBillingInfoPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.MemberFixture;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;





/** Scenario: FV2STOREB2C_12Y
 *  Details: Test cases to checkout a shopper by selecting shipment, shipping address and shipping methods.
 */
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class})
public class FV2STOREB2C_12 extends AbstractAuroraSingleSessionTests
{

	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	@DataProvider
	private final TestDataProvider dsm;

	private CaslFixturesFactory f_CaslFixtures;

	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * @param p_CaslFixtures 
	 */
	@Inject
	public FV2STOREB2C_12(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_CaslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		f_CaslFixtures = p_CaslFixtures;
	}
	
	/** Test case to Checkout with ship as complete for single shipment while shipping to a newly created address.
	 */ 
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_1201()
	{		
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY",Double.class));
		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();

		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
		
		//Go to shipping and billing page
		ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
		
		//Confirm ship as complete option is selected.
		shippingAndBilling.verifyShipAsCompleteIsChecked();
		
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm the correct shipping address is selected
		shippingAndBilling.verifyShippingAddressSelected(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
		
		//Create a new shipping address
		RegisteredShippingBillingInfoPage shippingAndBillingInfo = shippingAndBilling.createShippingAddress();
		
		//Enter info and submit the new address creating request
		shippingAndBillingInfo.typeRecipient(dsm.getInputParameter("RECIPIENT"))
		 .typeLastName(dsm.getInputParameter("LAST_NAME"))		 
		 .typeAddress(dsm.getInputParameter("ADDRESS"))
		 .typeCity(dsm.getInputParameter("CITY"))
		 .selectCountry(dsm.getInputParameter("COUNTRY"))
		 .selectStateOrProvince(dsm.getInputParameter("PROVINCE"))
		 .typeZipCode(dsm.getInputParameter("ZIP_CODE"))
		 .typePhone(dsm.getInputParameter("PHONE_NUMBER"))
		 .typeEmail(dsm.getInputParameter("EMAIL"))
		 .applyToBothShippingBilling()
		 .submitSuccessfulForm();
		
		shippingAndBilling.selectShippingAddress(dsm.getInputParameter("RECIPIENT"));
		
		//Wait for the new shipping address to display on the page
		shippingAndBilling.waitForShippingAddress(dsm.getInputParameter("RECIPIENT"));
		
		//Checks 'Ship As Complete' check box on on shipping billing page.
		shippingAndBilling.selectShipAsComplete();
		
		//select Pay later as the payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
		
		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	
		
		//Confirm product name
		singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm item quantity
		singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_QTY"));
		
		//Confirm item each price
		singleOrderSummary.verifyItemEachPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_EACH_TOTAL"));
		
		//Confirm total price of the item.
		singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_TOTAL"));
		
		//Confirm the total price of the order
		singleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL"));						
		
		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
		
		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();		
	}
	
	/** Test case to Checkout with shipping instructions for single shipment while shipping to an already created address.
	 */
	@Test
	public void testFV2STOREB2C_1202() 
	{
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY",Double.class));
		
		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
		
		//Verify Item in shop cart page
		shopCart.verifyOrderTotal(dsm.getInputParameter("TOTAL"));
		
		//Verify Item in shop cart page
		shopCart.verifyItemInShopCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Go to Next Page
		ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
		
		//Confirm ship as complete option is selected.
		shippingAndBilling.verifyShipAsCompleteIsChecked();
		
		//Confirm the correct shipping address is selected
		shippingAndBilling.verifyShippingAddressSelected(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
		
		//Checks 'Ship As Complete' check box on on shipping billing page.
		shippingAndBilling.selectShipAsComplete();
		
		//Select the new shipping address
		shippingAndBilling.selectShippingAddress(dsm.getInputParameter("RECIPIENT"));
		
		//Checks 'Add Shipping Instruction' check box and type shipping instructions on on shipping billing page.
		shippingAndBilling.addShippingInstructions(dsm.getInputParameter("SHIPPING_INSTUCTIONS"));
		
		//select Pay later as the payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
		
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm ship as complete option is selected.
		shippingAndBilling.verifyShipAsCompleteIsChecked();
		
		//Verify shop cart is not empty
		shippingAndBilling.verifyShoppingCartIsNOTEmpty();
		
		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	
		
		//Confirm product name
		singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm item quantity
		singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_QTY"));
		
		//Confirm item each price
		singleOrderSummary.verifyItemEachPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_EACH_TOTAL"));
		
		//Confirm total price of the item.
		singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_TOTAL"));
		
		//Confirm shipping address
		singleOrderSummary.verifyShippingAddress(dsm.getInputParameter("RECIPIENT"));
		
		//Confirm the total price of the order
		singleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL"));	
		
		//Verifies Billing Method on order summary page.
		singleOrderSummary.verifyBillingMethod(dsm.getInputParameter("PAY_METHOD"),dsm.getInputParameter("PAYMENT_NUMBER"));
		
		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
		
		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();
	}
	
	/** 
	 * Test case to Checkout by requesting a specific shipping date for single shipment while editing a shipping address that already exists.
	 */
	@Test
	public void testFV2STOREB2C_1203() 
	{
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY",Double.class));
		
		
		//Get the date to input as shipping date
		Calendar calendar = Calendar.getInstance();
	    DateFormat format = new SimpleDateFormat( "yyyy" );
	    Date date = calendar.getTime();
	    String year = format.format(date);
	    int month = calendar.get(Calendar.MONTH) + 1;
	    
	    if(month != 12)
	    {
	    	month = month + 1;
	    }else{
	    	year = Integer.toString(Integer.parseInt(year) + 1);
	    	month = 1;
	    }
	    
	    String shippingDate = month+"/"+"28"+"/"+ year; 
		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();

		//Log in to the store.
		signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();			
		
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
		
		//Click on check out button. 
		ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
		
		//Confirm ship as complete option is selected.
		shippingAndBilling.verifyShipAsCompleteIsChecked();
		
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm the correct shipping address is selected
		shippingAndBilling.verifyShippingAddressSelected(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
		
		//Create a new shipping address
		RegisteredShippingBillingInfoPage shippingAndBillingInfo = shippingAndBilling.createShippingAddress();
		
		//Enter info and submit the new address creating request
		shippingAndBillingInfo.typeRecipient(dsm.getInputParameter("RECIPIENT"))
		 .typeLastName(dsm.getInputParameter("LAST_NAME"))		 
		 .typeAddress(dsm.getInputParameter("ADDRESS"))
		 .typeCity(dsm.getInputParameter("CITY"))
		 .selectCountry(dsm.getInputParameter("COUNTRY"))
		 .selectStateOrProvince(dsm.getInputParameter("PROVINCE"))
		 .typeZipCode(dsm.getInputParameter("ZIP_CODE"))
		 .typePhone(dsm.getInputParameter("PHONE_NUMBER"))
		 .typeEmail(dsm.getInputParameter("EMAIL"))
		 .applyToBothShippingBilling()
		 .submitSuccessfulForm();
		
		//Select the new shipping address
		shippingAndBilling.selectShippingAddress(dsm.getInputParameter("RECIPIENT"));
		
		//Wait for the new shipping address to display on the page
				shippingAndBilling.waitForShippingAddress(dsm.getInputParameter("RECIPIENT"));
		
		//Edit the selected shipping address
		shippingAndBilling.editShippingAddress();
		
		//Confirm ship and bill info page is loaded completely
		shippingAndBillingInfo.verifyIsNotOnShipBillPage();
		
		//Update phone number 
		shippingAndBillingInfo.typePhone(dsm.getInputParameter("UPDATE_PHONE_NUMBER")).submitSuccessfulForm();
		
		//Checks 'Ship As Complete' check box on on shipping billing page.
		shippingAndBilling.selectShipAsComplete();
		
		//Enter a shipping date
		shippingAndBilling.addShippingDate(dsm.getInputParameter("INVALID_SHIPPING_DATE1"));
		
		//As soon the date is entered, the expected tooltip error message is displayed		
		shippingAndBilling.verifyToolTipIsDisplayed(dsm.getInputParameter("TOOLTIP"));
		
		//Enter a shipping date
		shippingAndBilling.addShippingDate(dsm.getInputParameter("INVALID_SHIPPING_DATE2"));
		
		//Confirm header message is shown
		shippingAndBilling.getHeaderWidget().verifyErrorMessage(dsm.getInputParameter("HEADERMESSAGE"));
		
		//Enter a shipping date
		shippingAndBilling.addShippingDate(shippingDate);
		
		//select Pay later as the payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
		
		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	
		
		//Confirm product name
		singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm item quantity
		singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_QTY"));
		
		//Confirm item each price
		singleOrderSummary.verifyItemEachPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_EACH_TOTAL"));
		
		//Confirm total price of the item.
		singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_TOTAL"));
		
		//Confirm shipping address
		singleOrderSummary.verifyShippingAddress(dsm.getInputParameter("RECIPIENT"));
		
		//Confirm the total price of the order
		singleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL"));	
		
		//Verifies Billing Method on order summary page.
		singleOrderSummary.verifyBillingMethod(dsm.getInputParameter("PAY_METHOD"),dsm.getInputParameter("PAYMENT_NUMBER"));
		
		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
		
		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();
	}			
	
	/** Test case to Checkout by selecting same shipping address for all items for multiple shipment.
	 */
	@Test
	public void testFV2STOREB2C_1204() 
	{		
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY",Double.class));
		
		orders.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY",Double.class));
		
		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
		
		//Confirm the price on minishopping cart
		shopCart.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
		
		ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
		
		//Confirm ship as complete option is selected.
		shippingAndBilling.verifyShipAsCompleteIsChecked();
		
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm the correct shipping address is selected
		shippingAndBilling.verifyShippingAddressSelected(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
		
		//Clicks 'Multiple Shipment' button on Shipping and Billing Page.
		shippingAndBilling.chooseMultipleShipping();
		
		//Selects Shipping Address from drop down list on shipping billing page for the first item in shopping cart.
		shippingAndBilling.getMultipleshipment().selectShippingAddressByPosition(dsm.getInputParameter("RECIPIENT1"), dsm.getInputParameter("ITEM_POSITION1"));
		
		//Selects Shipping Address from drop down list on shipping billing page for the second item in shopping cart.
		shippingAndBilling.getMultipleshipment().selectShippingAddressByPosition(dsm.getInputParameter("RECIPIENT1"), dsm.getInputParameter("ITEM_POSITION2"));
		
		//Checks 'Ship As Complete' check box on on shipping billing page.
		shippingAndBilling.selectShipAsComplete();
		
		//select Pay later as the payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
		
		//click next button
		OrderSummaryMultipleShipPage multipleOrderSummary = shippingAndBilling.multipleShipNext();
		
		//Confirm the first item's product name
		multipleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION1"), dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm item quantity
		multipleOrderSummary.verifyItemQTY(dsm.getInputParameter("ITEM_POSITION1"), dsm.getInputParameter("ITEM_QTY"));
		
		//Confirm item each price
		multipleOrderSummary.verifyItemEachPrice(dsm.getInputParameter("ITEM_POSITION1"),dsm.getInputParameter("ITEM_EACH_TOTAL"));
		
		//Confirm total price of the item.
		multipleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION1"),dsm.getInputParameter("ITEM_TOTAL"));
		
		//Confirm shipping address
		multipleOrderSummary.verifyShippingAddress(dsm.getInputParameter("RECIPIENT1"), dsm.getInputParameter("ITEM_POSITION1"));
		
		//Confirm the total price of the order
		multipleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL"));	
		
		//Verifies Billing Method on order summary page.
		multipleOrderSummary.verifyBillingMethod(dsm.getInputParameter("PAY_METHOD"));
		
		//click Order button
		OrderConfirmationPage orderConfirmation = multipleOrderSummary.completeOrder();
		
		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();
	}
	
	/** Test case to Checkout by selecting multiple shipping addresses for multiple shipment.
	  */
	@Test
	public void testFV2STOREB2C_1205() 
	{
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY",Double.class));
		
		orders.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY",Double.class));
		
		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
						
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
		
		//Confirm the price on minishopping cart
		shopCart.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
		
		//Click on check out button
		ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
		
		//Confirm the correct shipping address is selected
		shippingAndBilling.verifyShippingAddressSelected(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
		
		//Create a new shipping address
		RegisteredShippingBillingInfoPage shippingAndBillingInfo = shippingAndBilling.createShippingAddress();
		
		//Enter info and submit the new address creating request
		shippingAndBillingInfo.typeRecipient(dsm.getInputParameter("RECIPIENT1"))
		 .typeLastName(dsm.getInputParameter("LAST_NAME"))		 
		 .typeAddress(dsm.getInputParameter("ADDRESS"))
		 .typeCity(dsm.getInputParameter("CITY"))
		 .selectCountry(dsm.getInputParameter("COUNTRY"))
		 .selectStateOrProvince(dsm.getInputParameter("PROVINCE"))
		 .typeZipCode(dsm.getInputParameter("ZIP_CODE"))
		 .typePhone(dsm.getInputParameter("PHONE_NUMBER"))
		 .typeEmail(dsm.getInputParameter("EMAIL"))
		 .applyToBothShippingBilling()
		 .submitSuccessfulForm();
		
		shippingAndBilling.selectShippingAddress(dsm.getInputParameter("RECIPIENT1"));
		
		//Wait for the new shipping address to display on the page
		shippingAndBilling.waitForShippingAddress(dsm.getInputParameter("RECIPIENT1"));
		
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Clicks 'Multiple Shipment' button on Shipping and Billing Page.
		shippingAndBilling.chooseMultipleShipping();
		
		//Selects Shipping Address from drop down list on shipping billing page for the first item in shopping cart.
		shippingAndBilling.getMultipleshipment().selectShippingAddressByPosition(dsm.getInputParameter("RECIPIENT1"), dsm.getInputParameter("ITEM_POSITION1"));
		
		//Selects Shipping Address from drop down list on shipping billing page for the second item in shopping cart.
		shippingAndBilling.getMultipleshipment().selectShippingAddressByPosition(dsm.getInputParameter("RECIPIENT2"), dsm.getInputParameter("ITEM_POSITION2"));
		
		//Unchecks 'Ship As Complete' check box on shipping billing page.
		shippingAndBilling.unSelectShipAsComplete();
		
		//select Pay later as the payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
		
		//click next button
		OrderSummaryMultipleShipPage multipleOrderSummary = shippingAndBilling.multipleShipNext();
		
		//Confirm the first item's product name
		multipleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION1"), dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm item quantity
		multipleOrderSummary.verifyItemQTY(dsm.getInputParameter("ITEM_POSITION1"), dsm.getInputParameter("ITEM_QTY"));
		
		//Confirm item each price
		multipleOrderSummary.verifyItemEachPrice(dsm.getInputParameter("ITEM_POSITION1"),dsm.getInputParameter("ITEM_EACH_TOTAL"));
		
		//Confirm total price of the item.
		multipleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION1"),dsm.getInputParameter("ITEM_TOTAL"));
		
		//Confirm shipping address
		multipleOrderSummary.verifyShippingAddress(dsm.getInputParameter("RECIPIENT1"), dsm.getInputParameter("ITEM_POSITION1"));
		
		//Confirm shipping address
		multipleOrderSummary.verifyShippingAddress(dsm.getInputParameter("RECIPIENT2"), dsm.getInputParameter("ITEM_POSITION2"));
		
		//Confirm the total price of the order
		multipleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL"));	
		
		//Verifies Billing Method on order summary page.
		multipleOrderSummary.verifyBillingMethod(dsm.getInputParameter("PAY_METHOD"));
		
		//click Order button
		OrderConfirmationPage orderConfirmation = multipleOrderSummary.completeOrder();
		
		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();	
	}
	
	
	/** Test case to Checkout by selecting multiple shipping addresses and multiple shipping methods for multiple shipment.
	  */
	@Test
	public void testFV2STOREB2C_1206() 
	{
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY",Double.class));
		
		orders.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY",Double.class));
		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
		
		//Click on check out button
		ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
		
		//Confirm the correct shipping address is selected
		shippingAndBilling.verifyShippingAddressSelected(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
		
		//Create a new shipping address
		RegisteredShippingBillingInfoPage shippingAndBillingInfo = shippingAndBilling.createShippingAddress();
		
		//Enter info and submit the new address creating request
		shippingAndBillingInfo.typeRecipient(dsm.getInputParameter("RECIPIENT1"))
		 .typeLastName(dsm.getInputParameter("LAST_NAME"))		 
		 .typeAddress(dsm.getInputParameter("ADDRESS"))
		 .typeCity(dsm.getInputParameter("CITY"))
		 .selectCountry(dsm.getInputParameter("COUNTRY"))
		 .selectStateOrProvince(dsm.getInputParameter("PROVINCE"))
		 .typeZipCode(dsm.getInputParameter("ZIP_CODE"))
		 .typePhone(dsm.getInputParameter("PHONE_NUMBER"))
		 .typeEmail(dsm.getInputParameter("EMAIL"))
		 .applyToBothShippingBilling()
		 .submitSuccessfulForm();
		
		//Wait for the new shipping address to display on the page
		shippingAndBilling.selectShippingAddress(dsm.getInputParameter("RECIPIENT1"));
		//Wait for the new shipping address to display on the page
		shippingAndBilling.waitForShippingAddress(dsm.getInputParameter("RECIPIENT1"));
		
		
		//Confirm ship as complete option is selected.
		shippingAndBilling.verifyShipAsCompleteIsChecked();
		
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
				
		//Clicks 'Multiple Shipment' button on Shipping and Billing Page.
		shippingAndBilling.chooseMultipleShipping();
		
		//Selects Shipping Address from drop down list on shipping billing page for the first item in shopping cart.
		shippingAndBilling.getMultipleshipment().selectShippingAddressByPosition(dsm.getInputParameter("RECIPIENT1"), dsm.getInputParameter("ITEM_POSITION1"));
		
		//Selects Shipping Address from drop down list on shipping billing page for the second item in shopping cart.
		shippingAndBilling.getMultipleshipment().selectShippingAddressByPosition(dsm.getInputParameter("RECIPIENT2"), dsm.getInputParameter("ITEM_POSITION2"));
		
		//Selects Shipping Method from drop down list on shipping billing page for the first item in shopping cart.
		shippingAndBilling.getMultipleshipment().selectShippingMethodByPosition(dsm.getInputParameter("SHIPPING_METHOD1"),  dsm.getInputParameter("ITEM_POSITION1"));
		
		//Selects Shipping Method from drop down list on shipping billing page for the first item in shopping cart.
		shippingAndBilling.getMultipleshipment().selectShippingMethodByPosition(dsm.getInputParameter("SHIPPING_METHOD2"),  dsm.getInputParameter("ITEM_POSITION2"));		
		
		//Unchecks 'Ship As Complete' check box on shipping billing page.
		shippingAndBilling.unSelectShipAsComplete();
		
		//select Pay later as the payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
		
		//click next button
		OrderSummaryMultipleShipPage multipleOrderSummary = shippingAndBilling.multipleShipNext();
		
		//Confirm the first item's product name
		multipleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION1"), dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm item quantity
		multipleOrderSummary.verifyItemQTY(dsm.getInputParameter("ITEM_POSITION1"), dsm.getInputParameter("ITEM_QTY"));
		
		//Confirm item each price
		multipleOrderSummary.verifyItemEachPrice(dsm.getInputParameter("ITEM_POSITION1"),dsm.getInputParameter("ITEM_EACH_TOTAL"));
		
		//Confirm total price of the item.
		multipleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION1"),dsm.getInputParameter("ITEM_TOTAL"));
		
		//Confirm shipping address for the first item
		multipleOrderSummary.verifyShippingAddress(dsm.getInputParameter("RECIPIENT1"), dsm.getInputParameter("ITEM_POSITION1"));
		
		//Confirm shipping address for the second item
		multipleOrderSummary.verifyShippingAddress(dsm.getInputParameter("RECIPIENT2"), dsm.getInputParameter("ITEM_POSITION2"));
		
		//Confirm shipping method for the first item
		multipleOrderSummary.verifyShippingMethodByPosition(dsm.getInputParameter("SHIPPING_METHOD1"), dsm.getInputParameter("ITEM_POSITION1"));
		
		//Confirm shipping method for the second item
		multipleOrderSummary.verifyShippingMethodByPosition(dsm.getInputParameter("SHIPPING_METHOD2"), dsm.getInputParameter("ITEM_POSITION2"));
		
		//Confirm the total price of the order
		multipleOrderSummary.verifyOrderTotal(dsm.getInputParameter("ORDERSUMMARYTOTAL"));	
		
		//Verifies Billing Method on order summary page.
		multipleOrderSummary.verifyBillingMethod(dsm.getInputParameter("PAY_METHOD"));
		
		//click Order button
		OrderConfirmationPage orderConfirmation = multipleOrderSummary.completeOrder();
		
		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();	
	}
	
	/**
	 * Perform teardown operations after the test is done, whether it is successful or not.
	 */
	@After
	public void testScenarioTearDown()
	{
		try
		{
			getLog().info("testScenarioTearDown running");
			
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
			
			orders.removeAllItemsFromCart();
			orders.deletePaymentMethod();
			
			MemberFixture member = f_CaslFixtures.createMemberFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
			
			member.deleteAddress(dsm.getInputParameter("RECIPIENT"));				
				
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

	}
		
}
