package com.ibm.commerce.qa.aurora.tests;


	/*
	 *-----------------------------------------------------------------
	 * Licensed Materials - Property of IBM
	 *
	 * 
	 *
	 * WebSphere Commerce
	 *
	 * (C) Copyright IBM Corp. 2012
	 *
	 * US Government Users Restricted Rights - Use, duplication or
	 * disclosure restricted by GSA ADP Schedule Contract with
	 * IBM Corp.
	 *-----------------------------------------------------------------
	 */

	import java.util.logging.Logger;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.CustomerServiceFindCustomerWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.OrgAdminConsole;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.wte.util.WcConfigManager;


	/** 
	 * Test scenario to test various use cases associated with Dropdown menu context
	 * Refer to each test case for a detailed use case description
	 */
	@RunWith(GuiceTestRunner.class)
	@TestModules(AuroraModule.class)
	public class FSTOREB2CCSR_02 extends AbstractAuroraSingleSessionTests
	{
		/**
		 * The internal copyright field.
		 */
		public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

		//A Variable to retrieve data from the data file. 
		@DataProvider
		private final TestDataProvider dsm;
		private OrgAdminConsole oac;
		
		/**
		 * Test Class object constructor.
		 * 
		 * @param log
		 * 			   logging object 
		 * @param config
		 * 			   object to work with getConfig().properties file
		 * @param session
		 * 			   factory to create browser sessions
		 * @param dataSetManager
		 * 			   object to work with data files
		 * @param p_caslFixtures 
		 */		
		@Inject
		public FSTOREB2CCSR_02(
				Logger log, 
				WcConfigManager config,
				WcWteTestRule wcWebTestRule,
				CaslFoundationTestRule caslTestRule,
				TestDataProvider dataSetManager,
				OrgAdminConsole oac,
				CaslFixturesFactory p_caslFixtures)
		{
			super(log, wcWebTestRule, caslTestRule);
			this.dsm = dataSetManager;
			this.oac = oac;
		}


		/** Test case for Search results only brings up registered shoppers for B2C store
		 * @throws Exception 
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0205() throws Exception
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			oac.logon(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"));		
			
				
			String shopperLogonID = dsm.getInputParameter("SHOPPER_LOGONID")+System.currentTimeMillis();
			//Create a CSR account 
			
			oac.createNewUser(shopperLogonID,					
					dsm.getInputParameter("SHOPPER_FIRST_NAME"), 
					dsm.getInputParameter("SHOPPER_LAST_NAME"), 
					dsm.getInputParameter("SHOPPER_USERPASSWORD"), 
					dsm.getInputParameter("SHOPPER_PASSWORD_VERIFY"), 
					dsm.getInputParameter("SHOPPER_POLICYID"), 
					dsm.getInputParameter("SHOPPER_STATUS"), 
					dsm.getInputParameter("SHOPPER_LANGUAGE"), 
					dsm.getInputParameter("SHOPPER_PARENTORG"), 
					dsm.getInputParameter("SHOPPER_ADDRESS"), 
					dsm.getInputParameter("SHOPPER_CITY"), 
					dsm.getInputParameter("SHOPPER_STATE"), 
					dsm.getInputParameter("SHOPPER_COUNTRY"), 
					dsm.getInputParameter("SHOPPER_ZIPCODE"), 
					dsm.getInputParameter("SHOPPER_EMAIL"), 
					dsm.getInputParameter("SHOPPER_PASSWORD_EXPIRED"));


			//Verify customer result does not show up
			CustomerServiceFindCustomerWidget FindCustomerWidget = frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
			.typeLogonId(shopperLogonID).submitSearch()
			.verifySearchErrorMessageIsDisplayed();
			
			
			//Assign role as a Registered Customer
			oac.assignRoleToUser(shopperLogonID,
			dsm.getInputParameter("SHOPPER_PARENTORG"), dsm.getInputParameter("SHOPPER_ROLE"));	

			//Verify customer result DOES show up
			FindCustomerWidget.typeLogonId(shopperLogonID).submitSearch()
					.verifyCustomerSearchResultIsDisplayed();

		}
		
			
	}
