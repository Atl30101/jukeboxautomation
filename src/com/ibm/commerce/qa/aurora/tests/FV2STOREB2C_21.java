package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.ApparelSubCategoryPage;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.ComparePage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.CatalogEntryListWidget;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**			
* Scenario: FV2STOREB2C_21
* Details: This scenario will compare attributes of selected items.
*
*/
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_21 extends AbstractAuroraSingleSessionTests
{
	
	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

	/**
	 * Compare page used for verification.
	 */
	private ComparePage compare;
	
	/**
	 * Data Set Manager.
	 */
	@DataProvider
	private final TestDataProvider dsm;

	private CaslFixturesFactory f_CaslFixtures;
	
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dataSetManager
	 * @param p_CaslFixtures 
	 */
	@Inject
	public FV2STOREB2C_21(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_CaslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		
		f_CaslFixtures = p_CaslFixtures;
	}	
	
	/**
	 * Test to compare two kits added to compare zone from department page
	 * Pre-conditions: <ul>
	 * 					<li>Tester knows the Store URL (http://<hostname>/webapp/wcs/stores/servlet/Madisons/index.jsp)</li>
	 * 					<li>Product Quick Info, AJAX add to shopping cart, AJAX checkout, AJAX My Account, Product drag-and-drop are enabled</li>				
	 * 				   </ul>
	 * Flex flow options: Compare Products is enabled.
	 * Post conditions: Compare page is loaded.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_2101()throws Exception
	{	
		// Open Store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Sign in
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		signIn.typeUsername(dsm.getInputParameter("USER_NAME")).typePassword(dsm.getInputParameter("USER_PASSWORD"));
		HeaderWidget header= signIn.signIn().closeSignOutDropDownWidget();
		
		CategoryPage subCat;

		//Navigate to department Page
		subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT1"),dsm.getInputParameter("SUB_CAT1_1"),dsm.getInputParameter("SUB_CAT1_2"));

		// Bundles and Kits cannot be selected for compare,  
		//Verify Compare Box of kit cannot be selected
		subCat.getCatalogEntryListWidget().verifyAllowCompareCheckboxNotVisible(dsm.getInputParameter("KIT"));
		
		//navigate to a department page
		subCat = subCat.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT2"),dsm.getInputParameter("SUB_CAT2_1"),dsm.getInputParameter("SUB_CAT2_2"));

		//Verify Compare Box of bundle cannot be selected
		subCat.getCatalogEntryListWidget().verifyAllowCompareCheckboxNotVisible(dsm.getInputParameter("BUNDLE"));

	}

	/**
	 * Test to compare two products added to compare zone from department page
	 * Pre-conditions: <ul>
	 * 					<li>Tester knows the Store URL (http://<hostname>/webapp/wcs/stores/servlet/Madisons/index.jsp)</li>
	 * 					<li>Product Quick Info, AJAX add to shopping cart, AJAX checkout, AJAX My Account, Product drag-and-drop are enabled</li>				
	 * 				   </ul>
	 * Flex flow options: Compare Products is enabled.
	 * Post conditions: Compare page is loaded.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_2102()throws Exception
	{	
		//Open Store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Sign in
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		signIn.typeUsername(dsm.getInputParameter("USER_NAME")).typePassword(dsm.getInputParameter("USER_PASSWORD"));
		HeaderWidget header= signIn.signIn().closeSignOutDropDownWidget();
		
		ApparelSubCategoryPage department;
		
		//Navigate to department page
		department = header.goToCategoryPageByHierarchy(ApparelSubCategoryPage.class, dsm.getInputParameter("TOP_CAT"),dsm.getInputParameter("SUB_CAT"));
		CategoryPage subCat = department.getCategoryNavigationWidget().goToCategoryLink( dsm.getInputParameter("CATEGORY"), CategoryPage.class);
		
		CatalogEntryListWidget catentryWidget = subCat.getCatalogEntryListWidget();
		catentryWidget = catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD1"));
		catentryWidget = catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD2"));
		
		compare = catentryWidget.compare(dsm.getInputParameter("PROD1"));
	}	
	
	/**
	 * Test to compare items by adding items from the search results page
	 * Pre-conditions: <ul>
	 * 					<li>Tester knows the Store URL (http://<hostname>/webapp/wcs/stores/servlet/Madisons/index.jsp)</li>
	 * 					<li>Product Quick Info, AJAX add to shopping cart, AJAX checkout, AJAX My Account, Product drag-and-drop are enabled</li>				
	 * 				   </ul>
	 * Flex flow options: Compare Products is enabled.
	 * Post conditions: Compare page is loaded.
	 *
	 */
	@Test
	public void testFV2STOREB2C_2103()
	{
		//Open Store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Sign In
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		signIn.typeUsername(dsm.getInputParameter("USER_NAME")).typePassword(dsm.getInputParameter("USER_PASSWORD"));
		HeaderWidget header= signIn.signIn().closeSignOutDropDownWidget();
		
		//Search for items
		SearchResultsPage search = header.performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		
		CatalogEntryListWidget catentryWidget = search.getCatalogEntryListWidget();
		
		//Compare 4 Items without failing
		catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD1"));
		catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD2"));
		catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD3"));
		catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD4"));
		
		compare = catentryWidget.compare(dsm.getInputParameter("PROD4"));
		
	}
	
	/**
	 * Test shoppers can't add more than 4 items to the compare zone.
	 * Pre-conditions: <ul>
	 * 					<li>Tester knows the Store URL (http://<hostname>/webapp/wcs/stores/servlet/Madisons/index.jsp)</li>
	 * 					<li>Product Quick Info, AJAX add to shopping cart, AJAX checkout, AJAX My Account, Product drag-and-drop are enabled</li>				
	 * 				   </ul>
	 * Flex flow options: Compare Products is enabled.
	 * Post conditions: Compare page is loaded.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_2104()
	{
		//Open Store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Sign In
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		signIn.typeUsername(dsm.getInputParameter("USER_NAME")).typePassword(dsm.getInputParameter("USER_PASSWORD"));
		HeaderWidget header= signIn.signIn().closeSignOutDropDownWidget();
		
		//Search for an Item
		SearchResultsPage search = header.performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		
		CatalogEntryListWidget catentryWidget = search.getCatalogEntryListWidget();
		//Select 4 items for comparison
		catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD1"));	
		catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD2"));	
		catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD3"));	
		catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD4"));	
		
		//verify a 5th item cannot be selected for compare
		catentryWidget.verifyCompareCheckboxNotSelectable(dsm.getInputParameter("PROD5"));
	}

	/**
	 * Test to remove an item from the Compare Product page
	 * Pre-conditions: <ul>
	 * 					<li>Tester knows the Store URL (http://<hostname>/webapp/wcs/stores/servlet/Madisons/index.jsp)</li>
	 * 					<li>Product Quick Info, AJAX add to shopping cart, AJAX checkout, AJAX My Account, Product drag-and-drop are enabled</li>				
	 * 				   </ul>
	 * Flex flow options: Compare Products is enabled.
	 * Post conditions: Compare page is loaded.
	 * 
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_2105()
	{
		//Open Store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Sign In
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		signIn.typeUsername(dsm.getInputParameter("USER_NAME")).typePassword(dsm.getInputParameter("USER_PASSWORD"));
		HeaderWidget header= signIn.signIn().closeSignOutDropDownWidget();
		
		//Search for Items
		SearchResultsPage search = header.performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		
		CatalogEntryListWidget catentryWidget = search.getCatalogEntryListWidget();
		//Click compare Items
		catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD1"));	
		catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD2"));	
		catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD3"));	
		
		//Compare Items
		compare = catentryWidget.compare(dsm.getInputParameter("PROD3"));

		//Remove First Item
		compare = compare.remove(1);
		
		compare.verifyComparePageNotEmpty();
		
	}
	
	/**
	 * Test to remove all items from the Compare Product page
	 * Pre-conditions: <ul>
	 * 					<li>Tester knows the Store URL (http://<hostname>/webapp/wcs/stores/servlet/Madisons/index.jsp)</li>
	 * 					<li>Product Quick Info, AJAX add to shopping cart, AJAX checkout, AJAX My Account, Product drag-and-drop are enabled</li>				
	 * 				   </ul>
	 * Flex flow options: Compare Products is enabled.
	 * Post conditions: Compare page is loaded.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_2106()
	{
		//Open Store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Sign In
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		signIn.typeUsername(dsm.getInputParameter("USER_NAME")).typePassword(dsm.getInputParameter("USER_PASSWORD"));
		HeaderWidget header= signIn.signIn().closeSignOutDropDownWidget();
		
		
		ApparelSubCategoryPage department = header.goToCategoryPageByHierarchy(ApparelSubCategoryPage.class, dsm.getInputParameter("TOP_CAT"),dsm.getInputParameter("SUB_CAT"));
		CategoryPage subCat = department.getCategoryNavigationWidget().goToCategoryLink(dsm.getInputParameter("CATEGORY"), CategoryPage.class);

		CatalogEntryListWidget catentryWidget = subCat.getCatalogEntryListWidget();
		//Click compare checkboxes
		catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD1"));	
		catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD2"));	
		catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD3"));	
		
		//click compare
		compare = catentryWidget.compare(dsm.getInputParameter("PROD1"));
	
		//Remove all items
		compare = compare.remove(1);
		compare = compare.remove(1);
		compare = compare.remove(1);
		
		compare.verifyComparePageEmpty();
		
		
	}
			
	/**
	 * Test to compares items as  Guest shopper
	 * Pre-conditions: <ul>
	 * 					<li>Tester knows the Store URL (http://<hostname>/webapp/wcs/stores/servlet/Madisons/index.jsp)</li>
	 * 					<li>Product Quick Info, AJAX add to shopping cart, AJAX checkout, AJAX My Account, Product drag-and-drop are enabled</li>				
	 * 				   </ul>
	 * Flex flow options: Compare Products is enabled.
	 * Post conditions: Compare page is loaded.
	 *
	 */
	@Test
	public void testFV2STOREB2C_2107()
	{
		//Open Store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Sign in
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		signIn.typeUsername(dsm.getInputParameter("USER_NAME")).typePassword(dsm.getInputParameter("USER_PASSWORD"));
		HeaderWidget header= signIn.signIn().closeSignOutDropDownWidget();
		
		//Navigate to department page
		ApparelSubCategoryPage department = header.goToCategoryPageByHierarchy(ApparelSubCategoryPage.class, dsm.getInputParameter("TOP_CAT"),dsm.getInputParameter("SUB_CAT"));
		CategoryPage subCat = department.getCategoryNavigationWidget().goToCategoryLink(dsm.getInputParameter("CATEGORY"), CategoryPage.class);
		
		CatalogEntryListWidget catentryWidget = subCat.getCatalogEntryListWidget();
		catentryWidget = catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD1"));
		catentryWidget = catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD2"));
		
		compare = catentryWidget.compare(dsm.getInputParameter("PROD1"));
		
		compare.remove(1).verifyItemIsPresent(dsm.getInputParameter("PROD2"));
		
	}
	
	/**
	 * Shopper compares items and adds one of the items to cart and then completes a checkout flow
	 */
	@Test
	public void testFV2STOREB2C_2108()
	{	
		//Open Store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Sign in
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		signIn.typeUsername(dsm.getInputParameter("USER_NAME")).typePassword(dsm.getInputParameter("USER_PASSWORD"));
		HeaderWidget header= signIn.signIn().closeSignOutDropDownWidget();
		
		//Navigate to department page
		ApparelSubCategoryPage department = header.goToCategoryPageByHierarchy(ApparelSubCategoryPage.class, dsm.getInputParameter("TOP_CAT"),dsm.getInputParameter("SUB_CAT"));
		CategoryPage subCat = department.getCategoryNavigationWidget().goToCategoryLink(dsm.getInputParameter("CATEGORY"), CategoryPage.class);
		
		CatalogEntryListWidget catentryWidget = subCat.getCatalogEntryListWidget();
		catentryWidget = catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD1"));
		catentryWidget = catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD2"));
		
		compare = catentryWidget.compare(dsm.getInputParameter("PROD1"));
		
		compare.addToCartWithAttributes(dsm.getInputParameter("PROD1")).selectAttributeSwatch("Teal").selectAttributeSwatch("M").addToCart(ComparePage.class);
		
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("USER_NAME"), dsm.getInputParameter("USER_PASSWORD"), getConfig().getStoreName());
		
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();
		
	}
	
	/**
	 * Shopper compares 3 items and adds 2 of them to cart then completes a checkout flow
	 */
	@Test
	public void testFV2STOREB2C_2109()
	{	
		//Open Store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Sign in
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		signIn.typeUsername(dsm.getInputParameter("USER_NAME")).typePassword(dsm.getInputParameter("USER_PASSWORD"));
		HeaderWidget header= signIn.signIn().closeSignOutDropDownWidget();
		
		ApparelSubCategoryPage department = header.goToCategoryPageByHierarchy(ApparelSubCategoryPage.class, dsm.getInputParameter("TOP_CAT"),dsm.getInputParameter("SUB_CAT"));
		CategoryPage subCat = department.getCategoryNavigationWidget().goToCategoryLink(dsm.getInputParameter("CATEGORY"), CategoryPage.class);
		
		CatalogEntryListWidget catentryWidget = subCat.getCatalogEntryListWidget();
		catentryWidget = catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD1"));
		catentryWidget = catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD2"));
		catentryWidget = catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD3"));
		
		compare = catentryWidget.compare(dsm.getInputParameter("PROD1"));
		
		compare.addToCartWithAttributes(dsm.getInputParameter("PROD1")).selectAttributeSwatch("Teal").selectAttributeSwatch("M").addToCart(ComparePage.class);
		compare.addToCartWithAttributes(dsm.getInputParameter("PROD2")).selectAttributeSwatch("Blue").selectAttributeSwatch("M").addToCart(ComparePage.class);
		
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("USER_NAME"), dsm.getInputParameter("USER_PASSWORD"), getConfig().getStoreName());
		
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();
		
	}	
	
	/**
	 * Shopper goes back to the category page by clicking the "back to category" button

	 */
	@Test
	public void testFV2STOREB2C_2110()
	{	
		//Open Store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Sign in
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		signIn.typeUsername(dsm.getInputParameter("USER_NAME")).typePassword(dsm.getInputParameter("USER_PASSWORD"));
		HeaderWidget header= signIn.signIn().closeSignOutDropDownWidget();
		
		ApparelSubCategoryPage department = header.goToCategoryPageByHierarchy(ApparelSubCategoryPage.class, dsm.getInputParameter("TOP_CAT"),dsm.getInputParameter("SUB_CAT"));
		CategoryPage subCat = department.getCategoryNavigationWidget().goToCategoryLink(dsm.getInputParameter("CATEGORY"), CategoryPage.class);
		
		CatalogEntryListWidget catentryWidget = subCat.getCatalogEntryListWidget();
		catentryWidget = catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD1"));
		catentryWidget = catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD2"));
		
		compare = catentryWidget.compare(dsm.getInputParameter("PROD1"));
		
		compare.clickGoBackLink(CategoryPage.class);
		
	}
	
	/**
	 * Shopper goes back to the category page by clicking the browser back button

	 */
	@Test
	public void testFV2STOREB2C_2111()
	{	
		//Open Store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Sign in
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		signIn.typeUsername(dsm.getInputParameter("USER_NAME")).typePassword(dsm.getInputParameter("USER_PASSWORD"));
		HeaderWidget header= signIn.signIn().closeSignOutDropDownWidget();
		
		ApparelSubCategoryPage department = header.goToCategoryPageByHierarchy(ApparelSubCategoryPage.class, dsm.getInputParameter("TOP_CAT"),dsm.getInputParameter("SUB_CAT"));
		CategoryPage subCat = department.getCategoryNavigationWidget().goToCategoryLink(dsm.getInputParameter("CATEGORY"), CategoryPage.class);
		
		CatalogEntryListWidget catentryWidget = subCat.getCatalogEntryListWidget();
		catentryWidget = catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD1"));
		catentryWidget = catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD2"));
		
		compare = catentryWidget.compare(dsm.getInputParameter("PROD1"));
		
		getSession().goBack(CategoryPage.class);
	}
	
	/**
	 * Shopper compares products with attributes and ones without attributes, adds all of them to cart and checks out
	 */
	@Test
	public void testFV2STOREB2C_2112()
	{	
		//Open Store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Sign in
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		signIn.typeUsername(dsm.getInputParameter("USER_NAME")).typePassword(dsm.getInputParameter("USER_PASSWORD"));
		HeaderWidget header= signIn.signIn().closeSignOutDropDownWidget();
		
		SearchResultsPage searchPage;
		
		//Navigate to department page
		searchPage = header.performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		
		CatalogEntryListWidget catentryWidget = searchPage.getCatalogEntryListWidget();
		catentryWidget = catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD1"));
		catentryWidget = catentryWidget.allowCompareProduct(dsm.getInputParameter("PROD2"));
		
		compare = catentryWidget.compare(dsm.getInputParameter("PROD2"));
		
		compare.goToQuickInfoPopupByPosition(dsm.getInputParameter("POS")).selectTheFirstOptionForAttributes().addToCart(ComparePage.class);
		
		compare.addToCartByPosition(dsm.getInputParameterAsNumber("POS2",Integer.class));
		
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("USER_NAME"), dsm.getInputParameter("USER_PASSWORD"), getConfig().getStoreName());
		
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();
		
	}
}
