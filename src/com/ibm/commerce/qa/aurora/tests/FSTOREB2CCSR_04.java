package com.ibm.commerce.qa.aurora.tests;


	/*
	 *-----------------------------------------------------------------
	 * Licensed Materials - Property of IBM
	 *
	 * 
	 *
	 * WebSphere Commerce
	 *
	 * (C) Copyright IBM Corp. 2012
	 *
	 * US Government Users Restricted Rights - Use, duplication or
	 * disclosure restricted by GSA ADP Schedule Contract with
	 * IBM Corp.
	 *-----------------------------------------------------------------
	 */

	import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.casl.keys.CaslKeysFactory;
import com.ibm.commerce.casl.keys.ToolingKeys;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.CustomerRegisterPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.CustomerServiceFindCustomerWidget;
import com.ibm.commerce.qa.aurora.widget.CustomerServiceSidebarWidget;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.wte.util.WcConfigManager;


	/** 
	 * Test scenario to test various use cases associated with Dropdown menu context
	 * Refer to each test case for a detailed use case description
	 */
	@RunWith(GuiceTestRunner.class)
	@TestModules(AuroraModule.class)
	public class FSTOREB2CCSR_04 extends AbstractAuroraSingleSessionTests
	{
		/**
		 * The internal copyright field.
		 */
		public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

		//A Variable to retrieve data from the data file. 
		@DataProvider
		private final TestDataProvider dsm;
		private CaslKeysFactory f_caslKeysFactory;
		boolean doTearDown = false;

		
		/**
		 * Test Class object constructor.
		 * 
		 * @param log
		 * 			   logging object 
		 * @param config
		 * 			   object to work with getConfig().properties file
		 * @param session
		 * 			   factory to create browser sessions
		 * @param dataSetManager
		 * 			   object to work with data files
		 * @param p_caslFixtures 
		 */		
		@Inject
		public FSTOREB2CCSR_04(
				Logger log, 
				WcConfigManager config,
				WcWteTestRule wcWebTestRule,
				CaslFoundationTestRule caslTestRule,
				TestDataProvider dataSetManager,
				CaslFixturesFactory p_caslFixtures, CaslKeysFactory p_caslKeysFactory)
		{
			super(log, wcWebTestRule, caslTestRule);
			this.dsm = dataSetManager;
			f_caslKeysFactory = p_caslKeysFactory;
		}


	
		
		/** Test case to  Add item to cart
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0402()
		{
			doTearDown = true;
			
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//Get LOGONID of a shopper that we are trying to locate
			String ShopperLogonId = dsm.getInputParameter("SHOPPER_LOGONID");

			
			CustomerServiceFindCustomerWidget FindCustomerWidget = frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
			.typeLogonId(ShopperLogonId)
			.submitSearch();
			
			//Query to return unique user id (USER_ID) 
			ToolingKeys p = f_caslKeysFactory.createToolingKeys(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"));
			final String USER_ID = p.findPersonIdByLogonId(ShopperLogonId);

			

			//Click category link from department drop down
			CategoryPage subCat = FindCustomerWidget.AccessCustomerAccount(USER_ID)
					.getHeaderWidget()//.openShopCartWidget().clickLockCartButton()
					.goToCategoryPageByHierarchy(CategoryPage.class, 
							dsm.getInputParameter("TOP_CAT"), 
							dsm.getInputParameter("SUB_CAT"), 
							dsm.getInputParameter("CATEGORY_ITEM"));
			
			//Click product name from category page 
			ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
			
			//Select swatches from product display page
			productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR"));
			productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));
			
			//Click add to cart
			productDisplay.addToCart();
			
			//Go to shop cart page
			productDisplay.getHeaderWidget().goToShoppingCartPage()
				.verifyItemInShopCart(dsm.getInputParameter("PRODUCT_NAME"))
				.clickUnlockOrder();
			
		}
		
		
		/** Test case to  Add item to wishlist
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0403()
		{
			doTearDown = true;
			
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//Get LOGONID of a shopper that we are trying to locate
			String ShopperLogonId = dsm.getInputParameter("SHOPPER_LOGONID");

			
			CustomerServiceFindCustomerWidget FindCustomerWidget = frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
			.typeLogonId(ShopperLogonId)
			.submitSearch();
			
			//Query to return unique user id (USER_ID) 
			ToolingKeys p = f_caslKeysFactory.createToolingKeys(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"));
			final String USER_ID = p.findPersonIdByLogonId(ShopperLogonId);

			

			//Click category link from department drop down
			CategoryPage subCat = FindCustomerWidget.AccessCustomerAccount(USER_ID).getHeaderWidget()
					.goToCategoryPageByHierarchy(CategoryPage.class, 
							dsm.getInputParameter("TOP_CAT"), 
							dsm.getInputParameter("SUB_CAT"), 
							dsm.getInputParameter("CATEGORY_ITEM"));
			
			//Click product name from category page 
			ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
			
			//Select swatches from product display page
			productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR"));
			productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));
			
			//Click add to cart
			productDisplay.getShopperActionsWidget().addToWishlistLoggedIn();
			//Go to Wish List
			productDisplay.getHeaderWidget().goToMyWishList();
		}
		
		/** Test case to  Register a guest shopper
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0404()
		{
			
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			CustomerServiceSidebarWidget CustomerServiceSidebarWidget = frontPage.getHeaderWidget().goToCustomerService().getSidebarWidget();

			CustomerRegisterPage crp= CustomerServiceSidebarWidget.gotoAddCustomerPage();
			
			String userID = crp.getUniqueID();
			
			crp
		
			//type username
			.typeLogonId(userID)
			
			//type first name
			.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
			
			//type last name
			.typeLastName(dsm.getInputParameter("LAST_NAME"))
			
			//type street address
			.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
			
			//Select country
			.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
			
			//type or select state
			.selectStateOrProvince(dsm.getInputParameter("STATE"))
			
			//type city
			.typeCity(dsm.getInputParameter("CITY"))
			
			//type zipcode
			.typeZipCode(dsm.getInputParameter("ZIPCODE"))
			
			//type E-mail
			.typeEmail(dsm.getInputParameter("EMAIL"))
			
			//type home phone number
			.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
			
			//Select gender
			.selectGender(dsm.getInputParameter("GENDER"))
			
			//type mobile phone number
			.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
			
			
			//Check if preferred language drop down is visible
			.verifyPreferredLanguageDropDownListPresent()			
			
			
			//Selected preferred language
			.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
			
			//Check if birthday selection drop down is visible
			.verifyBirthdaySelectionPresent()
			
			//Select  birth year
			.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
			
			//Select birth month
			.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
			
			//Select birth day
			.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
			
			//Submit registration, the user is taken to the MyAccounts page
			.submit();
		}
		
		
		
		/**
		 * Tear down ran after every test case
		 */
		@After
		public void tearDown(){
			try
			{
			if (doTearDown) 
			{
				//Continue browser session starting at the home page.
				HeaderWidget headerSection = getSession().continueToPage(getConfig().getStoreUrl(), HeaderWidget.class);

				//Remove all items from the shopping cart
				headerSection.goToShoppingCartPage().clickLockOrder().removeAllItems().clickUnlockOrder()
				.getHeaderWidget().openSignOutDropDownWidget().signOut();
				}
			}
			catch(RuntimeException e)
			{
				getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
			}

		}
			
			
		
	}
