package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.CustomerRegisterPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.NonRegisteredShippingBillingInfoPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.DefiningAttributesWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.wte.framework.page.BrowserType;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.wte.util.WcConfigManager;


/** 
 * Test scenario to test various use cases associated with user registration
 * Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_01 extends AbstractAuroraSingleSessionTests
{
	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

	//A Variable to retrieve data from the data file. 
	@DataProvider
	private final TestDataProvider dsm;

	private CaslFixturesFactory f_caslFixtures;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with getConfig().properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_caslFixtures 
	 */		
	@Inject
	public FV2STOREB2C_01(
			Logger log, 
			WcConfigManager config,
			WcWteTestRule wcWebTestRule,
			CaslFoundationTestRule caslTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_caslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		f_caslFixtures = p_caslFixtures;
	}
		
	/** Test case to register a Shopper from the store home page with valid inputs
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_0101()
	{
		
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Click on the registration button on the SignIn page
		CustomerRegisterPage crp = signInPage.registerCustomer();
		
		String userID = crp.getUniqueID();
		
				//type username
				crp.typeLogonId(userID)
				
				//type first name
				.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
				
				//type last name
				.typeLastName(dsm.getInputParameter("LAST_NAME"))
				
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				
				//Verify password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
				
				//type street address
				.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
				
				//Select country
				.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
				
				//type or select state
				.selectStateOrProvince(dsm.getInputParameter("STATE"))
				
				//type city
				.typeCity(dsm.getInputParameter("CITY"))
				
				//type zipcode
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				
				//type E-mail
				.typeEmail(dsm.getInputParameter("EMAIL"))
				
				//type home phone number
				.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
				
				//Select gender
				.selectGender(dsm.getInputParameter("GENDER"))
				
				//type mobile phone number
				.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
				
				
				//Check if preferred language drop down is visible
				.verifyPreferredLanguageDropDownListPresent()			
				
				
				//Selected preferred language
				.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
				
				//Check if birthday selection drop down is visible
				.verifyBirthdaySelectionPresent()
				
				//Select  birth year
				.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
				
				//Select birth month
				.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
				
				//Select birth day
				.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
				
				//Submit registration, the uesr is taken to the MyAccounts page
				.submit();					
	}
	
		
	
	/** Test case to register a Shopper from the store home page with valid inputs but press cancel before submitting the form
	 */
	@Test
	public void testFV2STOREB2C_0102()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Click on the registration button on the SignIn page
		CustomerRegisterPage crp = signInPage.registerCustomer();
		
		String userID = crp.getUniqueID();

				//type username
				crp.typeLogonId(userID)
				
				//type first name
				.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
				
				//type last name
				.typeLastName(dsm.getInputParameter("LAST_NAME"))
				
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				
				//Verify password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
				
				//type street address
				.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
				
				//Select country
				.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
				
				//type or select state
				.selectStateOrProvince(dsm.getInputParameter("STATE"))
				
				//type city
				.typeCity(dsm.getInputParameter("CITY"))
				
				//type zip code
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				
				//type E-mail
				.typeEmail(dsm.getInputParameter("EMAIL"))
				
				//type home phone number
				.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
				
				//Select gender
				.selectGender(dsm.getInputParameter("GENDER"))
				
				//type mobile phone number
				.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
				
				//Update Allow Me Option check box
				.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
				
				//Check if preferred language drop down is visible
				.verifyPreferredLanguageDropDownListPresent()	
				
				
				//Selected preferred language
				.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
				
				//Check if birthday selection drop down is visible
				.verifyBirthdaySelectionPresent()
				
				//Select  birth year
				.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
				
				//Select birth month
				.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
				
				//Select birth day
				.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
				
				//Click on the Cancel button, registration is canceled
				.cancel();					
	}
	

	
	/** Test case to place an order using guest user and register user after.
	 */	
	@Test
	public void testFV2STOREB2C_0103()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//cleanup if needed
		cartCleanup(frontPage);
		
		//Go to category page Apparel -> Dress
		ProductDisplayPage pdp =frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), 
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"))		
					.getCatalogEntryListWidget()
					.goToProductPageByName(dsm.getInputParameter("ITEM_NAME"));		 
		
		//Select appropriate attributes 
		DefiningAttributesWidget test = pdp.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE_VALUE1"));
		
		test.selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE_VALUE2"));
		
		//Add to Shopping Cart
		ShopCartPage scp = pdp.addToCart().getHeaderWidget().goToShoppingCartPage();
		
		//Click "Continue Checkout as a Guest" button
		NonRegisteredShippingBillingInfoPage sbp =scp.continueAsGuestUser();
		
		//type recipient
		ShippingAndBillingPage sbp2 =sbp.typeBillRecipient(dsm.getInputParameter("RECIPIENT"))
				
			//enter first name
			.typeBillFirstName(dsm.getInputParameter("FIRST_NAME"))
			
			//enter last name
			.typeBillLastName(dsm.getInputParameter("LAST_NAME"))
			
			//enter street address
			.typeBillAddress(dsm.getInputParameter("STREET_ADDRESS"))
			
			//enter city
			.typeBillCity(dsm.getInputParameter("CITY"))
			
			//enter country
			.selectBillCountry(dsm.getInputParameter("COUNTRY"))
			
			//enter state
			.selectBillState(dsm.getInputParameter("STATE"))
			
			//enter zip code
			.typeBillZipCode(dsm.getInputParameter("ZIPCODE"))
			
			//enter E-mail
			.typeBillEmail(dsm.getInputParameter("EMAIL"))
			
			//enter phone number
			.typeBillPhone(dsm.getInputParameter("PHONE_NUMBER"))
			
			//Check "Same as Billing" checkbox
			.applyToBothShippingBilling()
			
			//Click on the Next button
			.submitSuccessfulForm();
		
		//Select billing method and continue as single ship
		OrderSummarySingleShipPage sabp= sbp2.selectPaymentMethod(dsm.getInputParameter("BILLING_METHOD")).singleShipNext();
		
		//Click on "Order" button
		OrderConfirmationPage ocp = sabp.completeOrder();
		
		String orderId = ocp.getOrderNumber();

		//Click on the "Sign up as a New User" link
		CustomerRegisterPage crp = ocp.goToSignUp();
		
		//Create a unique id
		String userID = crp.getUniqueID();

				//type username
				crp.typeLogonId(userID)
				
				//type first name
				.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
				
				//type last name
				.typeLastName(dsm.getInputParameter("LAST_NAME"))
				
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				
				//Verify password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
				
				//type street address
				.typeStreetAddressLine1(dsm.getInputParameter("STREET_ADDRESS"))
				
				//type country
				.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
				
				//type state
				.selectStateOrProvince(dsm.getInputParameter("STATE"))
				
				//type city
				.typeCity(dsm.getInputParameter("CITY"))
				
				//type zipcode
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				
				//type E-mail
				.typeEmail(dsm.getInputParameter("EMAIL"))
				
				//type phone number
				.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
				
				//Select gender
				.selectGender(dsm.getInputParameter("GENDER"))
				
				//type mobile phone number
				.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
				
				//Update the "Remember Me" option check box
				.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
				
				//Check if preferred language drop down is visible
				.verifyPreferredLanguageDropDownListPresent()	
				
				//Select preferred language
				.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
				
				//Check if birthday selection drop down is visible
				.verifyBirthdaySelectionPresent()
				
				//Select birth year
				.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
				
				//Select birth month
				.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
				
				//Select birth day
				.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
				
				//Submit registration, the uesr is taken to the MyAccounts page
				.submit();
				
			//verify order is moved to user's my account
			OrdersFixture orders = f_caslFixtures.createOrdersFixture(userID, dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
			
			orders.verifyOrderExists(Integer.parseInt(orderId));
	}
	
	/** Test case to register a Shopper with duplicate Logon ID
	 */
	@Test
	public void testFV2STOREB2C_0104()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Click on the registration button on the SignIn page
		CustomerRegisterPage crp = signInPage.registerCustomer();
		
		//Create a unique ID for the user
		String userID = crp.getUniqueID();

				//type a duplicate username
				crp.typeLogonId(dsm.getInputParameter("LOGONID"))
				
				//type first name
				.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
				
				//type last name
				.typeLastName(dsm.getInputParameter("LAST_NAME"))
				
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				
				//Verify password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
				
				//type street address
				.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
				
				//Select country
				.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
				
				//type or select state
				.selectStateOrProvince(dsm.getInputParameter("STATE"))
				
				//type city
				.typeCity(dsm.getInputParameter("CITY"))
				
				//type zipcode
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				
				//type E-mail
				.typeEmail(dsm.getInputParameter("EMAIL"))
				
				//type home phone number
				.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
				
				//Select gender
				.selectGender(dsm.getInputParameter("GENDER"))
				
				//type mobile phone number
				.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
				
				//Update Allow Me Option check box
				.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
				
				//Check if preferred language drop down is visible
				.verifyPreferredLanguageDropDownListPresent()	
				
				
				//Select preferred language
				.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
				
				//Check if birthday selection drop down is visible
				.verifyBirthdaySelectionPresent()
				
				//Select birth year
				.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
				
				//Select birth month
				.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
				
				//Select birth day
				.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
				
				//Click on "Submit" button, registration not successful
				.submitUnsuccessfulRegistration()
				
				.verifyDuplicateLogonError()
				
				//type a new uesrname
				.typeLogonId(userID)
				
				//Click on "Submit" button, registration successful
				.submit();
	}
	
	/**
	 * Test case to register a Shopper with an Empty mandatory field
	 */	
	@Test
	public void testFV2STOREB2C_0105()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Click on the registration button on the SignIn page		
		CustomerRegisterPage crp = signInPage.registerCustomer();
		
		//Create a unique ID for the user
		String userID = crp.getUniqueID();

				crp
				
				//type first
				.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
				
				//type last name
				.typeLastName(dsm.getInputParameter("LAST_NAME"))
				
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				
				//Verify password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
				
				//type street address
				.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
				
				//Select country
				.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
				
				//type or select state
				.selectStateOrProvince(dsm.getInputParameter("STATE"))
				
				//type city
				.typeCity(dsm.getInputParameter("CITY"))
				
				//type zipcode
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				
				//type E-mail
				.typeEmail(dsm.getInputParameter("EMAIL"))
				
				//type home phone number
				.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
				
				//Select gender
				.selectGender(dsm.getInputParameter("GENDER"))
				
				//type mobile phone number
				.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
				
				//Update Allow Me Option check box
				.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
				
				//Check if preferred language drop down is visible
				.verifyPreferredLanguageDropDownListPresent()		
				
				
				//Select preferred language
				.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
				
				//Check if birthday selection drop down is present
				.verifyBirthdaySelectionPresent()
				
				//Select birth year
				.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
				
				//Select birth month
				.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
				
				//Select birth day
				.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
				
				//Click on "Submit" button, registration not successful
				.submitUnsuccessfulRegistration()
				
			//Check if a tooltip is displayed with empty username field message	
			.verifyEmptyTooltipPresent()
			
			//type username
			.typeLogonId(userID)
			
			//Click on "Submit" button, registration successful
			.submit();
	}
	
	/** Test case to register a Shopper with two or more Empty fields
	 */
	@Test
	public void testFV2STOREB2C_0106()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Click on the registration button on the SignIn page
		CustomerRegisterPage crp = signInPage.registerCustomer();
		
		//Create a unique ID for the user
		String userID = crp.getUniqueID();

				crp
				
				//type first name
				.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
				
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				
				//Verify password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
				
				//type street address
				.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
				
				//Select country
				.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
				
				//Select state
				.selectStateOrProvince(dsm.getInputParameter("STATE"))
				
				//Select city
				.typeCity(dsm.getInputParameter("CITY"))
				
				//Select zipcode
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				
				//type E-mail
				.typeEmail(dsm.getInputParameter("EMAIL"))
				
				//type home phone number				
				.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
				
				//Select gender
				.selectGender(dsm.getInputParameter("GENDER"))
				
				//type mobile phone number
				.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
				
				//Update Allow Me Option check box
				.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
				
				//Check if preferred language drop down is visible
				.verifyPreferredLanguageDropDownListPresent()		
				
				
				//Select preferred language
				.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
				
				//Check is birthday selection dropdown is present
				.verifyBirthdaySelectionPresent()
				
				//Select birth year
				.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
				
				//Select birth month
				.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
				
				//Select birth day
				.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
				
				//Click on "Submit" button, registration not successful
				.submitUnsuccessfulRegistration()
				
			//Check if a tooltip is displayed with empty username field message	
			.verifyEmptyTooltipPresent()
			
			//type username
			.typeLogonId(userID)
			
			//Click on "Submit" button, registration not successful
			.submitUnsuccessfulRegistration()
			
			//Check if a tooltip is displayed with empty username field message	
			.verifyEmptyTooltipPresent()
			
			//type last name
			.typeLastName(dsm.getInputParameter("LAST_NAME"))
			
			//Click on "Submit" button, registration successful
			.submit();
	}
	
	/**
	 * Test case to register a Shopper with invalid values
	 */
	@Test
	public void testFV2STOREB2C_0107()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Click on the registration button on the SignIn page
		CustomerRegisterPage crp = signInPage.registerCustomer();
		
		//Create unique ID for the user
		String userID = crp.getUniqueID();

				crp
				
				//type username
				.typeLogonId(userID)
				
				//type first name
				.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
				
				//type last name
				.typeLastName(dsm.getInputParameter("LAST_NAME"))
				
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				
				//Verify password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
				
				//type street address
				.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
				
				//Select country
				.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
				
				//type or select state
				.selectStateOrProvince(dsm.getInputParameter("STATE"))
				
				//type city
				.typeCity(dsm.getInputParameter("CITY"))
				
				//type zipcode
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				
				//type invalid E-mail
				.typeEmail(dsm.getInputParameter("EMAIL"))
				
				//type home phone number
				.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER_RETRY"))
				
				//Select gender
				.selectGender(dsm.getInputParameter("GENDER"))
				
				//type mobile phone number
				.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
				
				//Update Allow Me Option check box
				.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
				
				//Check if preferred language drop down is visible
				.verifyPreferredLanguageDropDownListPresent()
				
				
				//Selected preferred language
				.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
				
				//Check if birthday selection drop down is visible
				.verifyBirthdaySelectionPresent()
				
				//Select  birth year
				.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
				
				//Select birth month
				.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
				
				//Select birth day
				.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
				
				//Click on "Submit" button, registration not successful
				.submitUnsuccessfulRegistration()
				
			//Check if a tooltip is displayed with invalid email field message	
			.verifyInvalidTooltipPresent()
			
			//type valid E-mail
			.typeEmail(dsm.getInputParameter("EMAIL_RETRY"))
			
			//type invalid phone number
			.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
			
			//Click on "Submit" button, registration not successful
			.submitUnsuccessfulRegistration()
			
			//Check if a tooltip is displayed with invalid phone number field message	
			.verifyInvalidTooltipPresent()
			
			//type valid phone number
			.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER_RETRY"))
			
			//Click on "Submit" button, registration successful
				.submit();
	}
	
	/**
	 * Test case to register a Shopper with invalid password
	 */
	@Test
	public void testFV2STOREB2C_0108()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Click on the registration button on the SignIn page		
		CustomerRegisterPage crp = signInPage.registerCustomer();
		
		//Create a unique ID for the user
		String userID = crp.getUniqueID();

				crp
				
				//type username
				.typeLogonId(userID)
				
				//type first name
				.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
				
				//type last name
				.typeLastName(dsm.getInputParameter("LAST_NAME"))
				
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				
				//Verify password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
				
				//type street address
				.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
				
				//Select country
				.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
				
				//type or select state
				.selectStateOrProvince(dsm.getInputParameter("STATE"))
				
				//type city
				.typeCity(dsm.getInputParameter("CITY"))
				
				//type zipcode
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				
				//type E-mail
				.typeEmail(dsm.getInputParameter("EMAIL"))
				
				//type home phone number
				.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
				
				//Select gender
				.selectGender(dsm.getInputParameter("GENDER"))
				
				//type mobile phone number
				.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
				
				//Update Allow Me Option check box
				.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
				
				//Check if preferred language drop down is visible
				.verifyPreferredLanguageDropDownListPresent()			
				
				
				
				//Select preferred language
				.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
				
				//Check if birthday selection drop down is visible
				.verifyBirthdaySelectionPresent()
				
				//Select  birth year
				.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
				
				//Select birth month
				.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
				
				//Select birth day
				.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
				
				//Click on "Submit" button, registration not successful
				.submitUnsuccessfulRegistration()
				
				//Error message is displayed at the top of the registration page
				.verifyPasswordErrorMessage()
				
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD_RETRY"))
				
				//Verify password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY_RETRY"))
				
				//Click on "Submit" button, registration successful
				.submit();
			
	}
	
	/**
	 * Test case to register a Shopper with mismatched password
	 * Pre-conditions: <ul>
	 * 					<li>Tester knows the Store URL (http://<hostname>/webapp/wcs/stores/servlet/en/auroraesite)</li>				
	 * 				</ul>
	 * 
	 * Post conditions: Shopper is not registered and user is taken to home page.
	 */	
	@Test
	public void testFV2STOREB2C_0109()
	{
		//Open Auroraesite store

		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		//Click on the SignIn page link on the header

		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		//Click on the registration button on the SignIn page
		
		CustomerRegisterPage crp = signInPage.registerCustomer();
		
		//Create a unique ID for the user
		String userID = crp.getUniqueID();

				crp
				
				//type username
				.typeLogonId(userID)
				
				//type first name
				.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
				
				//type last name
				.typeLastName(dsm.getInputParameter("LAST_NAME"))
				
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				
				//type incorrect password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
				
				//type street address
				.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
				
				//Select country
				.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
				
				//type or select state
				.selectStateOrProvince(dsm.getInputParameter("STATE"))
				
				//type city
				.typeCity(dsm.getInputParameter("CITY"))
				
				//type zipcode
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				
				//type E-mail
				.typeEmail(dsm.getInputParameter("EMAIL"))
				
				//type home phone number
				.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
				
				//Select gender
				.selectGender(dsm.getInputParameter("GENDER"))
				
				//type mobile phone number
				.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
				
				//Update Allow Me Option check box
				.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
				
				//Check if preferred language drop down is visible
				.verifyPreferredLanguageDropDownListPresent()	
				
				
				
				//Selected preferred language
				.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
				
				//Check if birthday selection drop down is visible
				.verifyBirthdaySelectionPresent()
				
				//Select  birth year
				.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
				
				//Select birth month
				.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
				
				//Select birth day
				.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
				
				//Click on "Submit" button, registration not successful
				.submitUnsuccessfulRegistration()
				
				//Check if a tooltip is displayed with password mismatch message	
				.verifyMismatchTooltipPresent()
				
				//Verify with correct password 
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY_RETRY"))
				
				//Click on "Submit" button, registration successful
				.submit();
	}
	
	/**
	 * Register a Child Shopper
	 *
	 * Note: Test case only works in firefox due to accept alert limitation in IE.
	 */	
	@Test
	public void testFV2STOREB2C_0110()
	{
		if(getConfig().getBrowserType() == BrowserType.FIREFOX){
			//Open Auroraesite store
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
			
			//Click on the SignIn page link on the header
			SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
			
			//Click on the registration button on the SignIn page
			CustomerRegisterPage crp = signInPage.registerCustomer();
			
			//Create a unique user ID for the user
			String userID = crp.getUniqueID();
	
					crp
					
					//type username
					.typeLogonId(userID)
					
					//type first name
					.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
					
					//type last name
					.typeLastName(dsm.getInputParameter("LAST_NAME"))
					
					//type password
					.typePassword(dsm.getInputParameter("PASSWORD"))
					
					//Verify password
					.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
					
					//type street address
					.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
					
					//Select country
					.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
					
					//type or select state
					.selectStateOrProvince(dsm.getInputParameter("STATE"))
					
					//type city
					.typeCity(dsm.getInputParameter("CITY"))
					
					//type zipcode
					.typeZipCode(dsm.getInputParameter("ZIPCODE"))
					
					//type E-mail
					.typeEmail(dsm.getInputParameter("EMAIL"))
					
					//type home phone number
					.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
					
					//Select gender
					.selectGender(dsm.getInputParameter("GENDER"))
					
					//type mobile phone number
					.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
					
					//Update Allow Me Option check box
					.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
					
					//Check if preferred language drop down is visible
					.verifyPreferredLanguageDropDownListPresent()	
					
					
					
					//Selected preferred language
					.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
					
					//Check if birthday selection drop down is visible
					.verifyBirthdaySelectionPresent()
					
					//Select  birth year
					.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
					
					//Select birth month
					.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
					
					//Select birth day
					.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
					
					//Accept the child customer policy alert
					.acceptAgeAlert()
					
					//Click on "Submit" button, registration successful
					.submit();
		}
		else
		{
			getLog().warning("Test case cannot be run in Internet Explorer due to a browser" +
					"limitation on accept alert popup.");
		}
	}
	
	/**
	 * Test case to register a Shopper with send e-mails about store promotions option selected
	 */		
	@Test
	public void testFV2STOREB2C_0111()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Click on the registration button on the SignIn page
		CustomerRegisterPage crp = signInPage.registerCustomer();
		
		//Create a unique ID for the user
		String userID = crp.getUniqueID();

		MyAccountMainPage mmp=		
				crp
				
				//type username
				.typeLogonId(userID)
				
				//type first name
				.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
				
				//type last name
				.typeLastName(dsm.getInputParameter("LAST_NAME"))
				
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				
				//Verify password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
				
				//type street address
				.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
				
				//Select country
				.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
				
				//type or select state
				.selectStateOrProvince(dsm.getInputParameter("STATE"))
				
				//type city
				.typeCity(dsm.getInputParameter("CITY"))
				
				//type zipcode
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				
				//type E-mail
				.typeEmail(dsm.getInputParameter("EMAIL"))
				
				//type home phone number
				.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
				
				//Select gender
				.selectGender(dsm.getInputParameter("GENDER"))
				
				//type mobile phone number
				.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
				
				//Update Allow Me Option check box
				.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
				
				//Check if preferred language drop down is visible
				.verifyPreferredLanguageDropDownListPresent()
				
			
				
				//Selected preferred language
				.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
				
				//Check if birthday selection drop down is visible
				.verifyBirthdaySelectionPresent()
				
				//Select  birth year
				.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
				
				//Select birth month
				.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
				
				//Select birth day
				.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
				
				//Update Send Alerts by Email checkbox
				.allowSendAlertsByEmail(Boolean.valueOf(dsm.getInputParameter("SEND_UPDATES")))
				
				//Submit registration, the user is taken to the MyAccounts page
				.submit();	
		
			//Click on the My Personal Information link on the left sidebar
			mmp.getSidebar().goToMyPersonalInfoPage()
			
			//Check that Send Email Promotion checkbox is updated
			.verifySendEmailUpdatesSelected();
	}
	
	/**
	 * Test case to register a Shopper with send SMS notification to Mobile Phone option selected
	 */	
	@Test
	public void testFV2STOREB2C_0112()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Click on the registration button on the SignIn page
		CustomerRegisterPage crp = signInPage.registerCustomer();
		
		//Create a unique ID for the user
		String userID = crp.getUniqueID();

		MyAccountMainPage mmp=	
				crp
				
				//type username
				.typeLogonId(userID)
				
				//type first name
				.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
				
				//type last name
				.typeLastName(dsm.getInputParameter("LAST_NAME"))
				
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				
				//Verify password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
				
				//type street address
				.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
				
				//Select country
				.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
				
				//type or select state
				.selectStateOrProvince(dsm.getInputParameter("STATE"))
				
				//type city
				.typeCity(dsm.getInputParameter("CITY"))
				
				//type zipcode
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				
				//type E-mail
				.typeEmail(dsm.getInputParameter("EMAIL"))
				
				//type home phone number
				.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
				
				//Select gender
				.selectGender(dsm.getInputParameter("GENDER"))
				
				//type mobile phone number
				.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
				
				//Update Allow Me Option check box
				.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
				
				//Check if preferred language drop down is visible
				.verifyPreferredLanguageDropDownListPresent()		
				
				
				
				//Selected preferred language
				.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
				
				//Check if birthday selection drop down is visible
				.verifyBirthdaySelectionPresent()
				
				//Select  birth year
				.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
				
				//Select birth month
				.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
				
				//Select birth day
				.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
				
				//Update SMS Notification checkbox
				.allowSendSMSMobileNotifications(Boolean.valueOf(dsm.getInputParameter("SEND_MOBILE_NOTIFICATION")))
				
				//Submit registration, the uesr is taken to the MyAccounts page
				.submit();	
		
				//Click on the My Personal Information link on the left sidebar
				mmp.getSidebar().goToMyPersonalInfoPage()
				
				//Check that the SMS Notification checkbox is updated
				.verifySendMobileNotificationsSelected();
	}
	
	/**
	 * Test case to register a Shopper with send SMS promotions to Mobile Phone option selected
	 */	
	@Test
	public void testFV2STOREB2C_0113()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Click on the registration button on the SignIn page
		CustomerRegisterPage crp = signInPage.registerCustomer();
		
		//Create a unique ID for the user
		String userID = crp.getUniqueID();
		

		MyAccountMainPage mmp=		
				crp
				
				//type username
				.typeLogonId(userID)
				
				//type first name
				.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
				
				//type last name
				.typeLastName(dsm.getInputParameter("LAST_NAME"))
				
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				
				//Verify password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
				
				//type street address
				.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
				
				//Select country
				.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
				
				//type or select state
				.selectStateOrProvince(dsm.getInputParameter("STATE"))
				
				//type city
				.typeCity(dsm.getInputParameter("CITY"))
				
				//type zipcode
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				
				//type E-mail
				.typeEmail(dsm.getInputParameter("EMAIL"))
				
				//type home phone number
				.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
				
				//Select gender
				.selectGender(dsm.getInputParameter("GENDER"))
				
				//type mobile phone number
				.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
				
				//Update Allow Me Option check box
				.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
				
				//Check if preferred language drop down is visible
				.verifyPreferredLanguageDropDownListPresent()
				
				
				
				//Selected preferred language
				.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
				
				//Check if birthday selection drop down is visible
				.verifyBirthdaySelectionPresent()
				
				//Select  birth year
				.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
				
				//Select birth month
				.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
				
				//Select birth day
				.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
				
				//Update SMS Mobile Promotion checkbox
				.allowSendSMSMobilePromotions(Boolean.valueOf(dsm.getInputParameter("SEND_MOBILE_PROMOTION")))
				
				//Submit registration, the uesr is taken to the MyAccounts page
				.submit();	
		
				//Click on the My Personal Information link on the left sidebar
				mmp.getSidebar().goToMyPersonalInfoPage()
				
				//Check that the SMS Mobile Promotion checkbox is updated
				.verifySendMobilePromotionsSelected();
	}
	
	/** Test case to Register a Shopper by clicking Sign In/Register link from the Quick Links drop down menu
	 */
	@Test
	public void testFV2STOREB2C_0114()
	{
		dsm.setDataLocation("testFV2STOREB2C_0101", "testFV2STOREB2C_0101");
		
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signInViaQuickLinks();
		
		//Click on the registration button on the SignIn page
		CustomerRegisterPage crp = signInPage.registerCustomer();
		
		String userID = crp.getUniqueID();
		
				//type username
				crp.typeLogonId(userID)
				
				//type first name
				.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
				
				//type last name
				.typeLastName(dsm.getInputParameter("LAST_NAME"))
				
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				
				//Verify password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
				
				//type street address
				.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
				
				//Select country
				.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
				
				//type or select state
				.selectStateOrProvince(dsm.getInputParameter("STATE"))
				
				//type city
				.typeCity(dsm.getInputParameter("CITY"))
				
				//type zipcode
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				
				//type E-mail
				.typeEmail(dsm.getInputParameter("EMAIL"))
				
				//type home phone number
				.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
				
				//Select gender
				.selectGender(dsm.getInputParameter("GENDER"))
				
				//type mobile phone number
				.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
				
				//Update Allow Me Option check box
				.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
				
				//Check if preferred language drop down is visible
				.verifyPreferredLanguageDropDownListPresent()			
				
				
				
				//Selected preferred language
				.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
				
				//Check if birthday selection drop down is visible
				.verifyBirthdaySelectionPresent()
				
				//Select  birth year
				.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
				
				//Select birth month
				.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
				
				//Select birth day
				.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
				
				//Submit registration, the uesr is taken to the MyAccounts page
				.submit();					
	}
	
	
	
	/**
	 * cleanup method to remove all items from the cart before proceeding.
	 *
	 * @param page A page object to be used for navigation
	 */
	
	
	private void cartCleanup(AuroraFrontPage page)
	{
		try {
			page.getHeaderWidget().verifyMiniCartItemCount("0");
		} catch (Exception e) {
			//cart isn't 0, cleaning up
			ShopCartPage shopCart = page.getHeaderWidget().goToShoppingCartPage().removeAllItems();
			shopCart.getHeaderWidget().goToFrontPage();
		}
		
	}
	
	/**
	 * Tear down ran after every test case
	 */
	@After
	public void tearDown(){
	
		try
		{
			OrdersFixture orders = f_caslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
			orders.deletePaymentMethod();
			orders.removeAllItemsFromCart();
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

		
	}
}
