package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.QuickCheckoutProfilePage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.StoreManagement;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/** 		
 * Scenario FV2STOREB2C_37
 * The objective of this test scenario is testing the flow option 'Enable/Disable Quick checkout' in store front.
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_37 extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	@DataProvider(apolloDSM=true)
	
	//A Variable to retrieve data from the data file.
	private final TestDataProvider dsm;
	
	//A Variable to enable/disable flex flow options from CMC.
	private final StoreManagement storeManagement;
	
	private final CaslFixturesFactory f_CaslFixtures;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param storeManagement
	 * 			   object to work with CMC flex flow options
	 */
	@Inject
	public FV2STOREB2C_37(Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			StoreManagement storeManagement,
			CaslFixturesFactory p_CaslFixtures){
		
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		this.storeManagement = storeManagement;
		f_CaslFixtures = p_CaslFixtures;
	}
	
	/**
	 * Test case to Enable quick checkout and perform quick check out flow, and then to
	 * disable quick check out and verify that it is disabled on store.
	 * 
	 * @throws Exception flex flow update fail in CMC.
	 */
	@Test
	public void testFV2STOREB2C_3701() throws Exception {
	
		//Calculate the current year.
		Calendar calendar = Calendar.getInstance();
	    DateFormat format = new SimpleDateFormat( dsm.getInputParameter("YEAR_FORMAT") );
	    Date date = calendar.getTime();
		
    	//Enable quick check from CMC
		storeManagement.enableChangeFlowOption(dsm.getInputParameter("QUICK_CHECKOUT"));

		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Open Sign In/Register page
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
				
		//type account User Name/Password and sign in
		MyAccountMainPage myAccountPage = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD")).signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//Open quick checkout profile from side bar.
		QuickCheckoutProfilePage quickCheckoutProfilePage = myAccountPage.getSidebar().goToQuickCheckoutProfilePage();
		
		//type in Billing Address details.
		quickCheckoutProfilePage.typeBillingFirstName(dsm.getInputParameter("FIRST_NAME"))
		.typeBillingLastName(dsm.getInputParameter("LAST_NAME"))
		.typeBillingAddressField1(dsm.getInputParameter("ADDRESS"))
		.typeBillingCity(dsm.getInputParameter("CITY"))
		.selectBillingCountry(dsm.getInputParameter("COUNTRY"))
		.selectBillingStateOrProvince(dsm.getInputParameter("STATE"))
		.typeBillingZipCode(dsm.getInputParameter("ZIP_CODE"))
		.typeBillingEmail(dsm.getInputParameter("EMAIL"))
		
		//Selects payment method.
		.selectPaymentMethod(dsm.getInputParameter("PAYMENT_METHOD"))
		
		//type in credit card details.
		.typeCardNumber(dsm.getInputParameter("CARD_NUMBER"))
		.selectExpirationMonth(dsm.getInputParameter("CARD_EXPIRY_MONTH"))
		.selectExpirationYear(format.format(date))
		
		//type in Shipping Address details.
		.allowUseSameAsBillingOption(true)

		//Select shipping method.
		.selectShippingMethodByName(dsm.getInputParameter("EXPECTED_SHIPPING_METHOD"));
		
		//Update quick checkout details
		myAccountPage = quickCheckoutProfilePage.update();
		
		//Get the pages header section object
		HeaderWidget headerSection = myAccountPage.getHeaderWidget();
		
		//Verify the success message
		headerSection.verifyMessage(dsm.getInputParameter("PROFILE_UPDATE_SUCCESS_MESSAGE"));
		
		//Open sub-category page.
		CategoryPage subCat  = headerSection.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), 
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//Select a product.
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select product attribute value.
		productDisplayPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("PRODUCT_ATTRIBUTE"));
		
		//Select 'add to cart' button on product display page.
		productDisplayPage.addToCart();

		//Get the pages header section object
		headerSection = productDisplayPage.getHeaderWidget();
		
		//Go to Shopping Cart page from Header
		ShopCartPage shopCartPage = headerSection.goToShoppingCartPage();
		
		//Verify shopping cart subtotal
		headerSection.verifyMiniCartTotalPrice(dsm.getInputParameter("SUBTOTAL"));
		
		//Verify and select 'Quick Checkout' button on Shop Cart Page
		ShippingAndBillingPage shippingAndBillingPage = shopCartPage.quickCheckout();
		
		//Verify that the default shipping address is selected
		shippingAndBillingPage.verifyShippingAddress(dsm.getInputParameter("EXPECTED_SHIPPING_ADDRESS"))

		//Verify that the billing method selected
		.verifyPaymentMethod(dsm.getInputParameter("EXPECTED_BILLING_METHOD"))

		//Verify the shipping method selected
		.verifyShippingMethod(dsm.getInputParameter("EXPECTED_SHIPPING_METHOD"))
		
		//Verify ship as complete option is set
		.verifyShipAsCompleteIsChecked()
		
		//Verify previously added item is present
		.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"))
		
		//Verify the billing address profile detail
		.verifyBillingAddressDetails(dsm.getInputParameter("FIRST_LAST_NAME_POSITION"), dsm.getInputParameter("FIRST_NAME") + " " + dsm.getInputParameter("LAST_NAME"))
		.verifyBillingAddressDetails(dsm.getInputParameter("STREET_ADDRESS_POSITION"), dsm.getInputParameter("ADDRESS"))
		.verifyBillingAddressDetails(dsm.getInputParameter("CITY_STATE_POSITION"), dsm.getInputParameter("CITY") + " " + dsm.getInputParameter("STATE"))
		.verifyBillingAddressDetails(dsm.getInputParameter("COUNTRY_ZIPCODE_POSITION"), dsm.getInputParameter("COUNTRY") + " " + dsm.getInputParameter("ZIP_CODE"))
		.verifyBillingAddressDetails(dsm.getInputParameter("EMAIL_POSITION"), dsm.getInputParameter("EMAIL"))
		
		//Verify the shipping address profile details
		.verifyShippingAddressDetails(dsm.getInputParameter("FIRST_LAST_NAME_POSITION"), dsm.getInputParameter("FIRST_NAME") + " " + dsm.getInputParameter("LAST_NAME"))
		.verifyShippingAddressDetails(dsm.getInputParameter("STREET_ADDRESS_POSITION"), dsm.getInputParameter("ADDRESS"))
		.verifyShippingAddressDetails(dsm.getInputParameter("CITY_STATE_POSITION"), dsm.getInputParameter("CITY") + " " + dsm.getInputParameter("STATE"))
		.verifyShippingAddressDetails(dsm.getInputParameter("COUNTRY_ZIPCODE_POSITION"), dsm.getInputParameter("COUNTRY") + " " + dsm.getInputParameter("ZIP_CODE"))
		.verifyShippingAddressDetails(dsm.getInputParameter("EMAIL_POSITION"), dsm.getInputParameter("EMAIL"));

		//Continue to Shipping and Billing Page
		OrderSummarySingleShipPage orderSummarySingleShipPage = shippingAndBillingPage.singleShipNext();
		
		//Complete the order by selecting the order button
		OrderConfirmationPage orderConfirmationPage = orderSummarySingleShipPage.completeOrder();
		
		//Verify the order is successful and displays the correct success message
		orderConfirmationPage.verifyOrderSuccessful(dsm.getInputParameter("ORDER_SUCCESSFUL_MESSAGE"));
				
		//Log out User
		orderConfirmationPage.getHeaderWidget().openSignOutDropDownWidget().signOut();
		
		//Disable quick checkout
		storeManagement.disableChangeFlowOption(dsm.getInputParameter("QUICK_CHECKOUT"));	
		
		//Open Sign In/Register page
		signInPage = frontPage.getHeaderWidget().signIn();
		
		//type account User Name/Password and sign in
		HeaderWidget header = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD")).signIn().closeSignOutDropDownWidget();
		
		//Open sub-category page
		subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), 
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//Select a product
		productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));

		//Select product attribute value
		productDisplayPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("PRODUCT_ATTRIBUTE"));
		
		//Select 'add to cart' on product display page
		productDisplayPage.addToCart();
		
		//Go to shopping cart page
		shopCartPage = productDisplayPage.getHeaderWidget().goToShoppingCartPage();
		
		//Verify that the 'Quick Checkout' button is not visible
		shopCartPage.verifyQuickCheckOutIsDisabled();
		

		
	}

	
	/**
	 * Tear down ran after every test case
	 * @throws Exception 
	 */
	@After
	public void tearDown() throws Exception{
		
		try{
			//re-enable quick checkout flow.
		
		storeManagement.enableChangeFlowOption(dsm.getInputParameter("QUICK_CHECKOUT"));
		
		//Continue browser session starting at the home page
		HeaderWidget headerSection = getSession().continueToPage(getConfig().getStoreUrl(), HeaderWidget.class);
		
		//Go to shopping cart page
		ShopCartPage shopCartPage = headerSection.goToShoppingCartPage();
		
		//Remove all items from the shopping cart
		//shopCartPage.removeAllItems();
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		orders.removeAllItemsFromCart();
		orders.deletePaymentMethod();
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}
		
	}

 }
