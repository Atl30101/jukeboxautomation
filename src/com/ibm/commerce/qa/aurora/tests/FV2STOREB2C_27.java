package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.DepartmentPage;
import com.ibm.commerce.qa.aurora.page.ErrorPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.url.DeltaUpdates;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/**			
* Scenario: FV2STOREB2C_27
* Details: This scenario will set Price range for the New Product and check it from store front.
*
* 
*/  
@RunWith(GuiceTestRunner.class) 
@TestModules(AuroraModule.class)
public class FV2STOREB2C_27 extends AbstractAuroraSingleSessionTests
{
	@DataProvider
	private final TestDataProvider dsm;
	private final CaslFixturesFactory f_CaslFixtures;
	private CMC cmc;
	

	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dataSetManager
	 * @param cmc
	 * @param deltaUpdate 
	 */
	@Inject
	public FV2STOREB2C_27(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CMC cmc,
			DeltaUpdates deltaUpdate,
			CaslFixturesFactory p_CaslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		this.cmc = cmc;
		this.deltaUpdate = deltaUpdate;
		f_CaslFixtures = p_CaslFixtures;
	}
	
	/**A variable to hold the catalog entry identifier created**/
	private String productId;
	
	/**A variable to hold the master catalog**/
	private String masterCatalogId;

	/**A variable to hold delta update**/
	private DeltaUpdates deltaUpdate;
	
	/**
	 * A test to login to CMC and Create a product using CMC and view from store front.
	 * 
	 * This test case requires WC's cache to be disabled.
	 * Annotation: com.ibm.commerce.qa.aurora.annotations/WCCacheDisabledOnly.java.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_2701() throws Exception
	{

		//login to CMC
		dsm.setDataLocation("testFV2STOREB2C_2701", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore();
		masterCatalogId = cmc.selectCatalog();
		String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");

		//create a new product
		dsm.setDataLocation("testFV2STOREB2C_2701", "createProduct");
		String partNumber = dsm.getInputParameter("partNumber");
		productId = cmc.createProduct(dsm.getInputParameter("parentCategory"),partNumber,dsm.getInputParameter("productName"),dsm.getInputParameter("published"));
		
		//set first offer price
		dsm.setDataLocation("testFV2STOREB2C_2701", "offerPrice1");
		cmc.createOfferPriceForCatalogEntry(productId);
		cmc.createListPriceForCatalogEntry(productId);
		
		//set second offer price
		dsm.setDataLocation("testFV2STOREB2C_2701", "offerPrice2");
		cmc.createOfferPriceForCatalogEntry(productId);
		
		//set third offer price
		dsm.setDataLocation("testFV2STOREB2C_2701", "offerPrice3");
		cmc.createOfferPriceForCatalogEntry(productId);
		
		//create attributes for the created product
		dsm.setDataLocation("testFV2STOREB2C_2701", "createProductAttribute");
		String attributeId = cmc.createProductDefiningAttribute(productId);
		
		//create attributes values for the product
		cmc.createProductDefiningAttributeValues(productId, attributeId, dsm.getInputParameter("attributeValue1"),"1");
		cmc.createProductDefiningAttributeValues(productId, attributeId, dsm.getInputParameter("attributeValue2"),"2");
		
		//create an SKU for the product
		dsm.setDataLocation("testFV2STOREB2C_2701", "createSKU");
		String itemId = cmc.createSKU(productId,dsm.getInputParameter("partNumber"),dsm.getInputParameter("skuName"),dsm.getInputParameter("published"));
		cmc.addSKUDefiningAttributeValue(productId,itemId,attributeId,dsm.getInputParameter("attributeValue"),dsm.getInputParameter("languageId"));
		
		//set first offer price and list price for the SKU
		dsm.setDataLocation("testFV2STOREB2C_2701", "offerPrice1");
		cmc.createOfferPriceForCatalogEntry(itemId);
		cmc.createListPriceForCatalogEntry(itemId);
		
		//set second offer price
		dsm.setDataLocation("testFV2STOREB2C_2701", "offerPrice2");
		cmc.createOfferPriceForCatalogEntry(itemId);
		
		//set third offer price
		dsm.setDataLocation("testFV2STOREB2C_2701", "offerPrice3");
		cmc.createOfferPriceForCatalogEntry(itemId);
		
		//Wait until delta update completes before timeout
		
		deltaUpdate.beforeWaitForDelta(masterCatalogId);
		getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
		deltaUpdate.waitForDelta(masterCatalogId, 60);
		
		//log off from CMC
		cmc.logoff();
		
		dsm.setDataLocation("testFV2STOREB2C_2701", "testFV2STOREB2C_2701");
		//open store front
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Login with valid user id and password
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		signIn = signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"));
		HeaderWidget header = signIn.signIn().closeSignOutDropDownWidget();
				
		//click top category from drop down
		DepartmentPage dept = header.goToDepartmentByName(DepartmentPage.class, dsm.getInputParameter("TOP_CATEGORY_NAME"));
		
		//verify new product present
		dept.getCatalogEntryListWidget().verifyProductIsPresent(productId);
		
		//Add the item to cart.
		ProductDisplayPage pdp = dept.getCatalogEntryListWidget().goToProductPagebyId(productId).addToCartRequest();
		
		//Go to shop cart page
		ShopCartPage cart = pdp.getHeaderWidget().goToShoppingCartPage();
		
		//verify item in shop cart
		cart = cart.verifyItemInShopCart(dsm.getInputParameter("ITEM_NAME"));

		//Sign out of store
		signIn = cart.getHeaderWidget().openSignOutDropDownWidget().signOut().signIn();
		
		//login to CMC
		dsm.setDataLocation("testFV2STOREB2C_2701", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
		
		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore();
		cmc.selectCatalog();
		
		//change the product to non buyable
		dsm.setDataLocation("testFV2STOREB2C_2701", "changeProduct_1");
		cmc.updateProduct(productId);
		
		masterCatalogId = cmc.selectCatalog();
		//Wait until delta update completes before timeout
		deltaUpdate.beforeWaitForDelta(masterCatalogId);
		getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
		deltaUpdate.waitForDelta(masterCatalogId, 600);
		
		//log off from CMC
		cmc.logoff();

		dsm.setDataLocation("testFV2STOREB2C_2701", "testFV2STOREB2C_2701");
		//Login with valid user id and password
		frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		signIn = frontPage.getHeaderWidget().signIn();
		signIn = signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"));
		header = signIn.signIn().closeSignOutDropDownWidget();
		
		//click top category from drop down
		SearchResultsPage search = header.performSearch(SearchResultsPage.class, dsm.getInputParameter("ITEM_NAME"));
		
		search.verifyEmptySearchResults();
		
		//Go to shop cart page
		cart = dept.getHeaderWidget().goToShoppingCartPage();
		
		//verify item in shop cart
		cart = cart.verifyItemInShopCart(dsm.getInputParameter("ITEM_NAME"));

		//Sign out of store
		signIn = cart.getHeaderWidget().openSignOutDropDownWidget().signOut().signIn();
				
		//login to CMC
		dsm.setDataLocation("testFV2STOREB2C_2701", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore();
		cmc.selectCatalog();
		previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
		
		//change the product to buyable
		dsm.setDataLocation("testFV2STOREB2C_2701", "changeProduct_2");
		cmc.updateProduct(productId);
		
		
		//Wait until delta update completes before timeout
		deltaUpdate.beforeWaitForDelta(masterCatalogId);
		getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
		deltaUpdate.waitForDelta(masterCatalogId, 60);
		
		//log off from CMC
		cmc.logoff();
		
		dsm.setDataLocation("testFV2STOREB2C_2701", "testFV2STOREB2C_2701");
		//Login with valid user id and password
		frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		signIn = frontPage.getHeaderWidget().signIn();
		signIn = signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"));
		header = signIn.signIn().closeSignOutDropDownWidget();
		
		//click top category from drop down
		dept = header.goToDepartmentByName(DepartmentPage.class, dsm.getInputParameter("TOP_CATEGORY_NAME"));
		
		dept.getCatalogEntryListWidget().verifyProductIsPresent(productId);
		
		//Go to shop cart page
		cart = dept.getHeaderWidget().goToShoppingCartPage();
		
		//verify item in shop cart
		cart = cart.verifyItemInShopCart(dsm.getInputParameter("ITEM_NAME"));
		
		
		//delete product and verify in storefront
		dsm.setDataLocation("testFV2STOREB2C_2701", "CMClogon");
		//login to CMC
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.selectStore();
		cmc.selectCatalog();
		
		//delete the new product created
		cmc.deleteCatalogEntry(productId);
		
		//log off from CMC
		cmc.logoff();
		
		cart = cart.getHeaderWidget().goToShoppingCartPage();
		
		ErrorPage error = cart.goToErrorPageByImageNumber("1");

		error.verifyPErrorMessage("This product has either been removed or is no longer available for sale.");
		
		cart = error.getHeaderWidget().goToShoppingCartPage();
		//empty shopping cart
		cart.removeAllItems();

		//Sign out of store
		cart.getHeaderWidget().openSignOutDropDownWidget().signOut();
		
	}
	
	/**
	 * Perform tear down operations such as stopping the test harness. This method will also delete the products created in this scenario.
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception
	{
	try
	{
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		signIn = signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"));
		
		//signIn.signIn().goToShoppingCartPage().removeAllItems().getHeaderWidget().openSignOutDropDownWidget().signOut();
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		orders.removeAllItemsFromCart();
		orders.deletePaymentMethod();
		
		//login to CMC
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.selectStore();
		cmc.selectCatalog();
		
		//delete the new product created
		cmc.deleteCatalogEntry(productId);
		
		//log off from CMC
		cmc.logoff();
	}
	catch(RuntimeException e)
	{
		getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
	}
	}
}
