package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

//Import the task libraries for use in this test script.
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.foundation.common.datatypes.AddressUsageEnumerationType;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.MultiplePaymentSection;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.QuickInfoPopup;
import com.ibm.commerce.qa.aurora.page.RegisteredShippingBillingInfoPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.MemberFixture;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.casl.fixtures.databuilders.IAddressBuilder;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/** 
 * Scenario FV2STOREB2C_18
 * This objective of this test scenario is to test shopper updating information from Shipping and Billing
 * Display page (checkout function).
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_18b extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

	@DataProvider(apolloDSM=true)
	
	//A Variable to retrieve data from the data file.
	private final TestDataProvider dsm;
	
	private final CMC cmc;
	
	//A Variable to determine if the address book needs to be removed in tear down.
	private boolean addressBookCleanup = false;
	
	//A Variable to determine if the promotion needs to be removed in tear down.
	private boolean promotionCleanup = false;
	
	//The promotion Id of the created promotion.
	private String promotionId;
	
	private CaslFixturesFactory f_CaslFixtures;

	private IAddressBuilder f_addressBuilder;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param cmc 
	 * 			   object to perform CMC tasks
	 * @param p_CaslFixtures 
	 * @param p_addressBuilder 
	 */
	@Inject
	public FV2STOREB2C_18b(Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CMC cmc, CaslFixturesFactory p_CaslFixtures,
			IAddressBuilder p_addressBuilder){
		
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		this.cmc = cmc;
		f_CaslFixtures = p_CaslFixtures;
		f_addressBuilder = p_addressBuilder;
	}
	
	/**
	 * Helper method to create address book entry.
	 * 
	 */
	public void createAddressBookEntrySetup()
	{
		MemberFixture member = f_CaslFixtures.createMemberFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		//Enter new address information
		f_addressBuilder.selectAddressType(AddressUsageEnumerationType.SHIPPING_AND_BILLING_LITERAL)
			.withFirstName(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
			.withLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
			.withStreetAddress(dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
			.withCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
			.withCountry(dsm.getInputParameter("ADDRESS_BOOK_COUNTRY"))
			.withState(dsm.getInputParameter("ADDRESS_BOOK_STATE"))
			.withZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"));
		
		member.createAddress(dsm.getInputParameter("ADDRESS_BOOK_RECIPIENT"), f_addressBuilder.build(), dsm.getInputParameter("ADDRESS_BOOK_EMAIL"));
		
		//Indicate that address must be removed at tear down
		addressBookCleanup = true;
		
	}
	
	/**
	 * Helper method to execute the sign in action for each test case.
	 * 
	 * @return a new my account page object.
	 */
	public MyAccountMainPage executeSignIn()
	{
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Open Sign In/Register page
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter account User Name/Password and sign in
		MyAccountMainPage accountPage = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD")).signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		return accountPage;
	}
	
	
	/**
	 * Test case to ensure Shopper can update the advanced shipping options (shipping instruction and shipping
	 * date) from the Shipping and Billing Display page.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_1806()
	{
		//Get the date to input as shipping date
		Calendar calendar = Calendar.getInstance();
	    DateFormat format = new SimpleDateFormat( "yyyy" );
	    Date date = calendar.getTime();
	    String year = format.format(date);
	    int month = calendar.get(Calendar.MONTH) + 1;
	    
	    if(month != 12)
	    {
	    	month = month + 1;
	    }else{
	    	year = Integer.toString(Integer.parseInt(year) + 1);
	    	month = 1;
	    }
	    
	    String shippingDate = month+"/"+"28"+"/"+ year; 
		
	    
	    String[] months = {"January", "February",
	    		  "March", "April", "May", "June", "July",
	    		  "August", "September", "October", "November",
	    		  "December"};
	    
	    //Get the format of the display date expected on the order summary page
	    String verifyDate = months[month-1] + " " + 28 + ", " + year;
	    
	    //add product to cart
		OrdersFixture order = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		
		order.removeAllItemsFromCart();
		
		order.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY", Double.class));
	    
		//Execute user login
		MyAccountMainPage accountPage = executeSignIn();
				
		//Click home page link
		AuroraFrontPage frontPage = accountPage.getHeaderWidget().goToFrontPage();

		//Get the header section object
		HeaderWidget headerSection = frontPage.getHeaderWidget();
		
		//Clicks on 'Shopping Cart' link on header
		ShopCartPage shopCartPage = headerSection.goToShoppingCartPage();
		
		//Clicks on 'Checkout' button on Shop Cart Page.
		ShippingAndBillingPage shippingAndBillingPage = shopCartPage.continueToNextStep();
		
		//checks 'request shipping date' check box and enter shipping date on Shipping and Billing Page.
		shippingAndBillingPage.addShippingDate(shippingDate)

	    //checks 'ship as complete' check box on Shipping and Billing Page.
		.selectShipAsComplete()
		
		//checks 'add shipping instructions' check box and enter instructions on Shipping and Billing Page.
		.addShippingInstructions(dsm.getInputParameter("SHIPPING_INSTRUCTIONS"))
				
    	//select billing method from drop down list in Shipping Billing Page.
	    .selectPaymentMethod(dsm.getInputParameter("BILLING_METHOD"))
		
		.verifyShoppingCartIsNOTEmpty();
	    
	    //Clicks on 'Next' button link on Shipping and Billing Page.
		OrderSummarySingleShipPage orderSummaryPage = shippingAndBillingPage.singleShipNext();
		
		//Verifies Billing Method on order summary page.
		orderSummaryPage.verifyBillingMethod(dsm.getInputParameter("EXPECTED_BILLING_METHOD"), dsm.getInputParameter("PAYMENT_NUMBER"))
		
		//Verify the shipping instructions
		.verifyShippingInstructions(dsm.getInputParameter("SHIPPING_INSTRUCTIONS"))
		
		//Verify the shipping date.
		.verifyShipDate(verifyDate);
		
		//Clicks on 'Order' button on Order Summary Page.
		OrderConfirmationPage orderConfirmationPage = orderSummaryPage.completeOrder();
		
		//Verifies the order Confirmation.
		orderConfirmationPage.verifyOrderSuccessful(dsm.getInputParameter("ORDER_SUCCESSFUL_MESSAGE"));
	}
	
	/**
	 * Test case to ensure Shopper can update the billing address from the Shipping and Billing Display page.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_1807(){
		 //add product to cart
		OrdersFixture order = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		
		order.removeAllItemsFromCart();
		
		order.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY", Double.class));
	    
		//Open the store in the browser
		createAddressBookEntrySetup();
		
		MyAccountMainPage accountPage = executeSignIn();

		//Get the header section object
		HeaderWidget headerSection = accountPage.getHeaderWidget();
		
		//Clicks on 'Shopping Cart' link on header.
		ShopCartPage shopCartPage = headerSection.goToShoppingCartPage();
		
		//Clicks on 'Checkout' button on Shop Cart Page.
		ShippingAndBillingPage shippingAndBillingPage = shopCartPage.continueToNextStep();
		
		//Select the new billing address
		shippingAndBillingPage.selectBillingAddressByName(dsm.getInputParameter("ADDRESS_BOOK_RECIPIENT"));
		
		//click 'edit address' and update address Shipping and Billing Page.
		RegisteredShippingBillingInfoPage shippingBillingInfoPage = shippingAndBillingPage.editBillingAddress();
		
		//Enter a new phone number
		shippingBillingInfoPage.typePhone(dsm.getInputParameter("UPDATE_PHONE"));
		
		//Submit billing address update.
		shippingAndBillingPage = shippingBillingInfoPage.submitSuccessfulForm();
		
		//select billing method from drop down list in Shipping Billing Page.
	    shippingAndBillingPage.selectPaymentMethod(dsm.getInputParameter("BILLING_METHOD"))
	    
	    //Verify the billing address phone details
	    .verifyBillingAddressDetails("5", dsm.getInputParameter("UPDATE_PHONE"));
	    
	    //Clicks on 'Next' button link on Shipping and Billing Page.
		OrderSummarySingleShipPage orderSummaryPage = shippingAndBillingPage.singleShipNext();
		
		//Verifies Billing Method on order summary page.
		orderSummaryPage.verifyBillingMethod(dsm.getInputParameter("EXPECTED_BILLING_METHOD"), dsm.getInputParameter("PAYMENT_NUMBER"));
		
		//Clicks on 'Order' button on Order Summary Page.
		OrderConfirmationPage orderConfirmationPage = orderSummaryPage.completeOrder();
		
		//Verifies the order Confirmation.
		orderConfirmationPage.verifyOrderSuccessful(dsm.getInputParameter("ORDER_SUCCESSFUL_MESSAGE"));
	}
	
	/**
	 * Test case to ensure Shopper updates the billing method from the Shipping and Billing Display page
	 * 
	 */
	@Test
	public void testFV2STOREB2C_1808(){
		
	    Calendar calendar = Calendar.getInstance();
	    DateFormat format = new SimpleDateFormat( "yyyy" );
	    Date date = calendar.getTime();
		
	    //add product to cart
		OrdersFixture order = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		
		order.removeAllItemsFromCart();
		
		order.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY", Double.class));
	    
		//Execute user login
		MyAccountMainPage accountPage = executeSignIn();
		
		//Open the store in the browser
		createAddressBookEntrySetup();

		
		//Get the header section object
		HeaderWidget headerSection = accountPage.getHeaderWidget();
		
		//Clicks on 'Shopping Cart' link on header
		ShopCartPage shopCartPage = headerSection.goToShoppingCartPage();
		
		//Clicks on 'Checkout' button on Shop Cart Page.
		ShippingAndBillingPage shippingAndBillingPage = shopCartPage.continueToNextStep();
		
	    //Select shipping address from drop down list in Shipping Billing Page.
	    shippingAndBillingPage.selectShippingAddress(dsm.getInputParameter("SHIPPING_ADDRESS_LABEL"));
	    
	    //clicks on edit address link in Shipping Billing Page.
	    RegisteredShippingBillingInfoPage shippingAndBillingInfoPage = shippingAndBillingPage.editShippingAddress();
	    
	  //Confirm ship and bill info page is loaded completely
	    shippingAndBillingInfoPage.verifyIsNotOnShipBillPage();
	    
	    //Enter email update information
	    shippingAndBillingInfoPage.typeEmail(dsm.getInputParameter("UPDATE_EMAIL"));
	    
	    //Submit shipping address update.
	    shippingAndBillingPage = shippingAndBillingInfoPage.submitSuccessfulForm();
	    
    	//select billing method from drop down list in Shipping Billing Page.
	    shippingAndBillingPage.selectPaymentMethod(dsm.getInputParameter("BILLING_METHOD"))
	    
	    //Verify Shipping address email details.
	    .verifyShippingAddressDetails(dsm.getInputParameter("PHONE_DETAILS_POSITION"), dsm.getInputParameter("UPDATE_EMAIL"));
	    
	    //Clicks on 'Next' button link on Shipping and Billing Page.
		OrderSummarySingleShipPage orderSummaryPage = shippingAndBillingPage.singleShipNext();
		
		//click 'back' button on order summary page.
		orderSummaryPage.goBack();
		
		//select billing method from drop down list in Shipping Billing Page.
	    shippingAndBillingPage.selectPaymentMethod(dsm.getInputParameter("CHANGE_BILLING_METHOD"))
	    
	    //Enters credit card information in Shipping and Billing Page. 
	    .typeCardInformation(dsm.getInputParameter("CARD_NUMBER"), dsm.getInputParameter("CVV_NUMBER"), dsm.getInputParameter("CARD_EXPIRY_MONTH"), format.format(date));
	    
	    //Clicks on 'Next' button link on Shipping and Billing Page.
		orderSummaryPage = shippingAndBillingPage.singleShipNext();
		
		//Verifies Billing Method on order summary page.
		orderSummaryPage.verifyBillingMethod(dsm.getInputParameter("EXPECTED_BILLING_METHOD"), dsm.getInputParameter("PAYMENT_NUMBER"))
		
		//Verify the credit card expire month.
		.verifyCreditCardExpireMonth(dsm.getInputParameter("CARD_EXPIRY_MONTH"))
		
		//Verify the credit card expire year.
		.verifyCreditCardExpireYear(format.format(date));
		
		//Clicks on 'Order' button on Order Summary Page.
		OrderConfirmationPage orderConfirmationPage = orderSummaryPage.completeOrder();
		
		//Verifies the order Confirmation.
		orderConfirmationPage.verifyOrderSuccessful(dsm.getInputParameter("ORDER_SUCCESSFUL_MESSAGE"));
		
	}
	
	/**
	 * Test case to update the number of payment methods from the shipping and billing page.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_1809() 
	{
		//add product to cart
		OrdersFixture order = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		
		order.removeAllItemsFromCart();
		
		order.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY", Double.class));
	    
		//Execute user login
		MyAccountMainPage accountPage = executeSignIn();

		//Get the header section object
		HeaderWidget headerSection = accountPage.getHeaderWidget();
		
		//Clicks on 'Shopping Cart' link on header.
		ShopCartPage shopCartPage = headerSection.goToShoppingCartPage();
		
		//Clicks on 'Checkout' button on Shop Cart Page.
		ShippingAndBillingPage shippingAndBillingPage = shopCartPage.continueToNextStep();
		
		//Verify the shipping method selected
		shippingAndBillingPage.verifyShippingMethod(dsm.getInputParameter("EXPECTED_SHIPPING_METHOD"))
		
		//Verify ship as complete option is set
		.verifyShipAsCompleteIsChecked()
		
		//Verify previously added item is present
		.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"))
		
		//Selects number of payment methods from drop down list in Shipping and Billing Page
		.selectNoOfPaymentMethods(dsm.getInputParameter("NUMBER_OF_PAYMENT_METHODS"));

		//Get the multiple payment section object
	    MultiplePaymentSection multiplePaymentSection = shippingAndBillingPage.getMultiplepayment();
	    
	    //select billing method from drop down list for first payment in Shipping Billing Page
	    multiplePaymentSection.selectBillingMethodByPaymentsPosition(dsm.getInputParameter("BILLING_METHOD1"), dsm.getInputParameter("PAYMENT_NUMBER1"));
	    
	    //modify the amount to be charged to first address
	    multiplePaymentSection.typeAmountByPaymentsPosition(dsm.getInputParameter("PAYMENT_AMOUNT1"), dsm.getInputParameter("PAYMENT_NUMBER1"));		    
	    
	    //select billing method from drop down list for second payment in Shipping Billing Page
	    multiplePaymentSection.selectBillingMethodByPaymentsPosition(dsm.getInputParameter("BILLING_METHOD2"), dsm.getInputParameter("PAYMENT_NUMBER2"));
	    
	    //modify the amount to be charged for the second payment
	    multiplePaymentSection.typeAmountByPaymentsPosition(dsm.getInputParameter("PAYMENT_AMOUNT2"), dsm.getInputParameter("PAYMENT_NUMBER2"));
	    
	    //Clicks on 'Next' button link on Shipping and Billing Page.
		OrderSummarySingleShipPage orderSummaryPage = shippingAndBillingPage.singleShipNext();
		
	    //Verify the first Billing Method on order summary page.
		orderSummaryPage.verifyBillingMethod(dsm.getInputParameter("BILLING_METHOD1"), dsm.getInputParameter("PAYMENT_NUMBER1"))
		
		//Verify the second Billing Method on order summary page.
		.verifyBillingMethod(dsm.getInputParameter("BILLING_METHOD2"), dsm.getInputParameter("PAYMENT_NUMBER2"));
		
		//Clicks on 'Order' button on Order Summary Page.
		OrderConfirmationPage orderConfirmationPage = orderSummaryPage.completeOrder();
		
		//Verifies the order Confirmation.
		orderConfirmationPage.verifyOrderSuccessful(dsm.getInputParameter("ORDER_SUCCESSFUL_MESSAGE"));
	}
	
	/** 
	 * Test case to ensure Shopper can update the promotion code from the Shipping and Billing Display page.
	 * @throws Exception 
	 * 
	 */
	@Test
	public void testFV2STOREB2C_1810() throws Exception
	{
		/*--- Create Promotion code ---**/
		
		dsm.setDataBlock("CMClogon");
		
		//Log into cmc
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		cmc.setLocale(dsm.getInputParameter("locale"));

		//Select the store to work on
		cmc.selectStore();
		
		//Create promotion object with type "Precentage of an order" and assign a Promotion Code
		dsm.setDataBlock("CreatePromotionCMC");
		promotionId = cmc.createPromotion("new promotion", "OrderLevelPercentDiscount");
		
		//Give the promotion a discount percentage of 7%
		dsm.setDataBlock("createElementforOrderLevel");
		cmc.createPromotionElementforOrderLevelPromotion("0", "7", "DiscountRange", "DiscountRange");
		
		//Active the promotion
		cmc.activatePromotion(promotionId);
		
		//Set flag to indicate that promotion must be delete
		promotionCleanup = true;
		
		dsm.setDataBlock("LogoutCMC");
		cmc.logoff();
		
		
		/*---- Test promotion code in store ----**/
		
		dsm.setDataBlock("testFV2STOREB2C_1810");
		
		//add product to cart
		OrdersFixture order = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		
		order.removeAllItemsFromCart();
		
		order.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY", Double.class));
	    
		//Execute user login.
		MyAccountMainPage accountPage = executeSignIn();

		//Get the header section object
		HeaderWidget headerSection = accountPage.getHeaderWidget();
		
		//Clicks on 'Shopping Cart' link on header.
		ShopCartPage shopCartPage = headerSection.goToShoppingCartPage();
		
		//Verify item is in shopping cart
		shopCartPage.verifyItemInShopCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Clicks on 'Checkout' button on Shop Cart Page.
		ShippingAndBillingPage shippingAndBillingPage = shopCartPage.continueToNextStep();
		
	    //Type promotion code and clicks on 'Apply' button in Shipping Billing Page. 
	    shippingAndBillingPage.typePromoCode(dsm.getInputParameter("PROMOTION_CODE"))
	    
	    //verify the promotion is applied
	    .verifyPromoCode(dsm.getInputParameter("PROMO_POSITION"), dsm.getInputParameter("PROMOTION_CODE"))

	    //select billing method from drop down list in Shipping Billing Page.
	    .selectPaymentMethod(dsm.getInputParameter("BILLING_METHOD"));
	    
	    //Clicks on 'Next' button link on Shipping and Billing Page.
		OrderSummarySingleShipPage orderSummaryPage = shippingAndBillingPage.singleShipNext();
		
		//Confirm the order subtotal reflects the promotion code update
		orderSummaryPage.verifyOrderTotal(dsm.getInputParameter("PROMO_SUBTOTAL"))
		
		 //Verifies Billing Method on order summary page.
		.verifyBillingMethod(dsm.getInputParameter("EXPECTED_BILLING_METHOD"), dsm.getInputParameter("PAYMENT_NUMBER"));
		
		//Clicks on 'Order' button on Order Summary Page.
		OrderConfirmationPage orderConfirmationPage = orderSummaryPage.completeOrder();
		
		//Verifies the order Confirmation.
		orderConfirmationPage.verifyOrderSuccessful(dsm.getInputParameter("ORDER_SUCCESSFUL_MESSAGE"));
		
	}
	
	/**
	 * Test case to ensure Shopper can update the attributes of an item from the Shipping and Billing Display page.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_1811() 
	{
		//add product to cart
		OrdersFixture order = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		
		order.removeAllItemsFromCart();
		
		order.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY", Double.class));
		//Execute user login.
		MyAccountMainPage accountPage = executeSignIn();

		//Get the header section object
		HeaderWidget headerSection = accountPage.getHeaderWidget();
		
		//Clicks on 'Shopping Cart' link on header
		ShopCartPage shopCartPage = headerSection.goToShoppingCartPage();
		
		//Clicks on 'Checkout' button on Shop Cart Page
		ShippingAndBillingPage shippingAndBillingPage = shopCartPage.continueToNextStep();
		
		//Verify previously added item is present
		shippingAndBillingPage.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
		
	    //clicks on 'change attribute' link in Shipping Billing Page
	    QuickInfoPopup quickInfoPopup = shippingAndBillingPage.changeAttributes(dsm.getInputParameter("CHANGE_ATTRIBUTE_PRODUCT_POSITION"));
	    
	    //selects first attribute from drop down list in Shipping Billing Page.
	    quickInfoPopup.updateValueForAttribute(dsm.getInputParameter("ATTRIBUTE_NAME1"), dsm.getInputParameter("CHANGE_ATTRIBUTE_VALUE_1"))
		
		//selects second attribute from drop down list in Shipping Billing Page.
	    .updateValueForAttribute(dsm.getInputParameter("ATTRIBUTE_NAME2"), dsm.getInputParameter("CHANGE_ATTRIBUTE_VALUE_2"));
		
		//clicks 'Replace with Cart Item' link on quick info pop up in Shipping Billing Page.
		shippingAndBillingPage = quickInfoPopup.updateCartItem(ShippingAndBillingPage.class);
		
		//select billing method from drop down list in Shipping Billing Page.
	    shippingAndBillingPage.selectPaymentMethod(dsm.getInputParameter("BILLING_METHOD"));

	    //Clicks on 'Next' button link on Shipping and Billing Page.
		OrderSummarySingleShipPage orderSummaryPage = shippingAndBillingPage.singleShipNext();
		
		 //Verifies Billing Method on order summary page.
		orderSummaryPage.verifyBillingMethod(dsm.getInputParameter("EXPECTED_BILLING_METHOD"), dsm.getInputParameter("PAYMENT_NUMBER"))
		
		//Verify the product attribute.
		.verifyProductAttribute(dsm.getInputParameter("PRODUCT_POSITION"), dsm.getInputParameter("ATTRIBUTE_POSITION1")
				, dsm.getInputParameter("CHANGE_ATTRIBUTE_VALUE_2"))

		//Verify the product attribute.
		.verifyProductAttribute(dsm.getInputParameter("PRODUCT_POSITION"), dsm.getInputParameter("ATTRIBUTE_POSITION2")
				, dsm.getInputParameter("CHANGE_ATTRIBUTE_VALUE_1"))
		
		//Verify order total on order summary page.
		.verifyOrderTotal(dsm.getInputParameter("SUBTOTAL"));
				
		//Clicks on 'Order' button on Order Summary Page.
		OrderConfirmationPage orderConfirmationPage = orderSummaryPage.completeOrder();
		
		//Verifies the order Confirmation.
		orderConfirmationPage.verifyOrderSuccessful(dsm.getInputParameter("ORDER_SUCCESSFUL_MESSAGE"));
	}
	
	/**
	 * Tear down ran after every test case
	 */
	@After
	public void tearDown()
	{
		try
		{
			OrdersFixture order = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
			order.removeAllItemsFromCart();
			order.deletePaymentMethod();
			if(addressBookCleanup == true){
				
				MemberFixture member = f_CaslFixtures.createMemberFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
				
				member.deleteAddress(dsm.getInputParameter("ADDRESS_BOOK_RECIPIENT"));
				
				
			}
			
			if(promotionCleanup == true)
			{

				//Log into cmc
				dsm.setDataBlock("CMClogon");
				cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
				cmc.setLocale(dsm.getInputParameter("locale"));
				cmc.setTimeZone("America/Montreal");
				
				//Select the store to work on
				cmc.selectStore();

				//Deactivate and delete promotion
				cmc.deactivatePromotion(promotionId);
				cmc.deletePromotion(promotionId);
			}
			promotionCleanup = false;
			addressBookCleanup = false;
					}
		catch(Exception e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

	}
	
	
}
