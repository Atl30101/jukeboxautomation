package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.CatalogEntryListWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.url.DeltaUpdates;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/**			
* Scenario: FV2STOREB2C_43
* Details: This scenario will test basic shopper flows from extended e-sites.
*
* 
*/
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_43 extends AbstractAuroraSingleSessionTests
{

	@DataProvider
	private final TestDataProvider dsm;
	private final CaslFixturesFactory f_CaslFixtures;
	private CMC cmc;
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dataSetManager
	 * @param cmc
	 * @param deltaUpdate 
	 */
	@Inject
	public FV2STOREB2C_43(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CMC cmc,
			DeltaUpdates deltaUpdate,
			CaslFixturesFactory p_CaslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		this.cmc = cmc;
		this.deltaUpdate = deltaUpdate;
		f_CaslFixtures = p_CaslFixtures;
	}

	/**A variable to hold the catalog entry identifier created**/
	private String productId;
	
	/**A variable to hold the master catalog**/
	private String masterCatalogId;

	/**A variable to hold delta update**/
	private DeltaUpdates deltaUpdate;
	
	/**
	 * A test to login to CMC and create a product in asset store and view it from the inherited sample store and place and order.
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_4301() throws Exception
	{	
		//login to CMC
		cmc.logon(dsm.getEnvParameter("logonId"), dsm.getEnvParameter("password"));
		
		//select store and catalog
		cmc.setLocale(dsm.getEnvParameter("locale"));
		cmc.selectStore(getConfig().getStoreName());
		cmc.selectCatalog();
		String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
		
		//create a new product
		dsm.setDataLocation("testFV2STOREB2C_4301", "createProduct");
		String partNumber = dsm.getInputParameter("partNumber");
		productId = cmc.createProduct(dsm.getInputParameter("parentCategory"),partNumber,dsm.getInputParameter("productName"),dsm.getInputParameter("published"));
		
		//set first offer price and list price
		dsm.setDataLocation("testFV2STOREB2C_4301", "offerPrice1");
		cmc.createOfferPriceForCatalogEntry(productId);
		cmc.createListPriceForCatalogEntry(productId);
		
		dsm.setDataLocation("testFV2STOREB2C_4301", "offerPrice2");
		cmc.createOfferPriceForCatalogEntry(productId);
				
		//Add defining product attribute dictionary attribute - Color
		String[] attributeStringId = cmc.getAttributeDictionaryAttributeIdsForCatalogEntry(cmc.getCatalogEntryId("BCL014_1402"));
		cmc.addProductAttributeDictionaryAttribute(productId, attributeStringId[1], "0");
		
		String[][] attributeDictionaryData = cmc.getAllowedValAttrDictAttributeValue(attributeStringId[1]);

		//get list of attribute dictionary attribute value ID's 
		String[] attributeDictionaryValuesId = attributeDictionaryData[0];
		
		//get list of attribute dictionary attribute identifier names
		String[] attributeDictionaryValueIdentifiers = attributeDictionaryData[1];
		
		int attrValue1 = 0;
		int attrValue2 = 0;
		dsm.setDataBlock("addProductAttribute");
		
		//Go through the list of Identifiers and get the index of the attributes name needed
		for(int i = 0; i < attributeDictionaryValueIdentifiers.length; i++)
		{
			if(attributeDictionaryValueIdentifiers[i].equals(dsm.getInputParameter("attributeValue1")))
			{
				attrValue1 = i;
			}
			else if (attributeDictionaryValueIdentifiers[i].equals(dsm.getInputParameter("attributeValue2")))
			{
				attrValue2 = i;
			}
		}
		
		//create a SKU for the product
		dsm.setDataLocation("testFV2STOREB2C_4301", "createSKU1");
		
		String itemId1 = cmc.createSKU(productId,dsm.getInputParameter("partNumber"),dsm.getInputParameter("skuName"),dsm.getInputParameter("published"));
		cmc.addSKUDefiningAttributeDictionaryAttributeValue(itemId1, attributeStringId[1], attributeDictionaryValuesId[attrValue1], "0");
		
		//create a SKU for the product
		dsm.setDataLocation("testFV2STOREB2C_4301", "createSKU2");
		String itemId2 = cmc.createSKU(productId,dsm.getInputParameter("partNumber"),dsm.getInputParameter("skuName"),dsm.getInputParameter("published"));
		cmc.addSKUDefiningAttributeDictionaryAttributeValue(itemId2, attributeStringId[1], attributeDictionaryValuesId[attrValue2], "0");
		
		masterCatalogId = cmc.selectCatalog();
		
		//Wait until delta update completes before timeout
		deltaUpdate.beforeWaitForDelta(masterCatalogId);
		getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
		deltaUpdate.waitForDelta(masterCatalogId, 60);
		
		//log off from CMC
		cmc.logoff();
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_4301","testFV2STOREB2C_4301");
		
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//SignIn
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn().typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"));
		
		//click the category
		frontPage = signIn.signIn().closeSignOutDropDownWidget().goToFrontPage();
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CATEGORY"), dsm.getInputParameter("SUB_CATEGORY"), dsm.getInputParameter("SUB_ITEM"));
		
		CatalogEntryListWidget catentryWidget = subCat.getCatalogEntryListWidget();
		
		//Add the item to cart.
		ProductDisplayPage pdp = catentryWidget.goToProductPagebyId(productId);
		pdp.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("ATTR_NAME"), dsm.getInputParameter("ATTR_VALUE"));
			
			pdp.addToCart();
		
		//Go to shop cart page
		ShopCartPage shopCart = pdp.getHeaderWidget().goToShoppingCartPage();
		
		//click checkout
		ShippingAndBillingPage shipBill = shopCart.continueToNextStep();
		
		//select billing method
		shipBill.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
		
		//click next button
		OrderSummarySingleShipPage summary = shipBill.singleShipNext();
		
		//click order button
		OrderConfirmationPage comfirm = summary.completeOrder();
		
		//verify that the order has been placed
		comfirm.getHeaderWidget().openSignOutDropDownWidget().signOut();
	}
	
	/**
	 * A test to update offer prices in asset store and inherited sample stores and view it from the inherited sample store
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_4302() throws Exception
	{	
		//login to CMC
		cmc.logon(dsm.getEnvParameter("logonId"), dsm.getEnvParameter("password"));
		
		//select store and catalog
		cmc.setLocale(dsm.getEnvParameter("locale"));
		cmc.selectStore(getConfig().getStoreName());
		cmc.selectCatalog();
		String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
		//create a new product
		dsm.setDataLocation("testFV2STOREB2C_4302", "createProduct");
		String partNumber = dsm.getInputParameter("partNumber");
		productId = cmc.createProduct(dsm.getInputParameter("parentCategory"),partNumber,dsm.getInputParameter("productName"),dsm.getInputParameter("published"));
		
		//set first offer price and list price
		dsm.setDataLocation("testFV2STOREB2C_4302", "offerPrice1");
		cmc.createOfferPriceForCatalogEntry(productId);
		cmc.createListPriceForCatalogEntry(productId);
		
		dsm.setDataLocation("testFV2STOREB2C_4302", "offerPrice2");
		cmc.createOfferPriceForCatalogEntry(productId);
		
		
		//Add defining product attribute dictionary attribute - Color
		String[] attributeStringId = cmc.getAttributeDictionaryAttributeIdsForCatalogEntry(cmc.getCatalogEntryId("BCL014_1402"));
		cmc.addProductAttributeDictionaryAttribute(productId, attributeStringId[1], "0");
		
		String[][] attributeDictionaryData = cmc.getAllowedValAttrDictAttributeValue(attributeStringId[1]);

		//get list of attribute dictionary attribute value ID's 
		String[] attributeDictionaryValuesId = attributeDictionaryData[0];
		
		//get list of attribute dictionary attribute identifier names
		String[] attributeDictionaryValueIdentifiers = attributeDictionaryData[1];
		
		int attrValue1 = 0;
		int attrValue2 = 0;
		dsm.setDataBlock("addProductAttribute");
		
		//Go through the list of Identifiers and get the index of the attributes name needed
		for(int i = 0; i < attributeDictionaryValueIdentifiers.length; i++)
		{
			if(attributeDictionaryValueIdentifiers[i].equals(dsm.getInputParameter("attributeValue1")))
			{
				attrValue1 = i;
			}
			else if (attributeDictionaryValueIdentifiers[i].equals(dsm.getInputParameter("attributeValue2")))
			{
				attrValue2 = i;
			}
		}
		//create an SKU for the product
		dsm.setDataLocation("testFV2STOREB2C_4302", "createSKU1");
		String itemId1 = cmc.createSKU(productId,dsm.getInputParameter("partNumber"), dsm.getInputParameter("skuName"),dsm.getInputParameter("published"));
		cmc.addSKUDefiningAttributeDictionaryAttributeValue(itemId1, attributeStringId[0], attributeDictionaryValuesId[attrValue1], "0");
			
		//create an SKU for the product
		dsm.setDataLocation("testFV2STOREB2C_4302", "createSKU2");
		String itemId2 = cmc.createSKU(productId,dsm.getInputParameter("partNumber"), dsm.getInputParameter("skuName"),dsm.getInputParameter("published"));
		cmc.addSKUDefiningAttributeDictionaryAttributeValue(itemId2, attributeStringId[1], attributeDictionaryValuesId[attrValue2], "0");
		
		dsm.setDataLocation("testFV2STOREB2C_4302", "sku1_offerPrice1");
		cmc.createOfferPriceForCatalogEntry(itemId1);
		
		dsm.setDataLocation("testFV2STOREB2C_4302", "sku1_offerPrice2");
		cmc.createOfferPriceForCatalogEntry(itemId1);
		
		dsm.setDataLocation("testFV2STOREB2C_4302", "sku2_offerPrice1");
		cmc.createOfferPriceForCatalogEntry(itemId2);
		
		dsm.setDataLocation("testFV2STOREB2C_4302", "sku2_offerPrice2");
		cmc.createOfferPriceForCatalogEntry(itemId2);
		
		masterCatalogId = cmc.selectCatalog();
		
		//Wait until delta update completes before timeout
		deltaUpdate.beforeWaitForDelta(masterCatalogId);
		getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
		deltaUpdate.waitForDelta(masterCatalogId, 60);
		
		//log off from CMC
		cmc.logoff();
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_4302","testFV2STOREB2C_4302");
		
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//SignIn
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn().typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"));
		
		//click the category
		frontPage = signIn.signIn().closeSignOutDropDownWidget().goToFrontPage();
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CATEGORY"), dsm.getInputParameter("SUB_CATEGORY"), dsm.getInputParameter("SUB_ITEM"));
		
		CatalogEntryListWidget catentryWidget = subCat.getCatalogEntryListWidget();
		
		//Add the item to cart.
		ProductDisplayPage pdp = catentryWidget.goToProductPagebyId(productId);
		pdp.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("ATTR_NAME"), dsm.getInputParameter("ATTR_VALUE"));
		
		pdp.addToCart();
		
		//Go to shop cart page
		ShopCartPage shopCart = pdp.getHeaderWidget().goToShoppingCartPage();
		
		//change the quantity
		shopCart.changeQuantity("1", dsm.getInputParameter("NEW_QUANTITY"));
		
		//change the quantity
		shopCart.verifyQuantityIsUpdated("1", dsm.getInputParameter("NEW_QUANTITY"));
		
		//click checkout
		ShippingAndBillingPage shipBill = shopCart.continueToNextStep();
				
		//select billing method
		shipBill.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
		
		//click next button
		OrderSummarySingleShipPage summary = shipBill.singleShipNext();
		
		//click order button
		OrderConfirmationPage comfirm = summary.completeOrder();
		
		//verify that the order has been placed
		comfirm.getHeaderWidget().openSignOutDropDownWidget().signOut();
		
	}
	
	/**
	 * Perform teardown operations after the test is done, whether it is successful or not.
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception
	{
	try
	{
			//Open the store in the browser
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
			
			//SignIn
			SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn().typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"));
			frontPage = signIn.signIn().goToFrontPage();
			
			//Clear Shopping Cart
			//frontPage.getHeaderWidget().goToShoppingCartPage().removeAllItems().getHeaderWidget().openSignOutDropDownWidget().signOut();
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
			orders.removeAllItemsFromCart();
			orders.deletePaymentMethod();
			
			//login to CMC
			cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
			
			//select store and catalog
			cmc.selectStore(getConfig().getStoreName());
			cmc.selectCatalog();
			String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
			
			//delete the new product created
			cmc.deleteCatalogEntry(productId);
			
			masterCatalogId = cmc.selectCatalog();
			//Wait until delta update completes before timeout
			deltaUpdate.beforeWaitForDelta(masterCatalogId);
			getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
			deltaUpdate.waitForDelta(masterCatalogId, 60);
			
			//log off from CMC
			cmc.logoff();
	}
	catch(RuntimeException e)
	{
		getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
	}
	}

}
