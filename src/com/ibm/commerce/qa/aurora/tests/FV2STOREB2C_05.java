package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.MyAddressBookPage;
import com.ibm.commerce.qa.aurora.page.MyPersonalInfoPage;
import com.ibm.commerce.qa.aurora.page.QuickCheckoutProfilePage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.casl.util.CaslModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/**
 * Scenario: FV2STOREB2C_05
 * Registered shopper makes profile update				
 */
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class, CaslModule.class})
public class FV2STOREB2C_05 extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	@DataProvider(apolloDSM=true)
	
	//A Variable to retrieve data from the data file.
	private final TestDataProvider dsm;
	
	//A Variable to determine if the address book needs to be removed in tear down.
	private boolean addressBookCleanup = false;
	private final CaslFixturesFactory f_CaslFixtures;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 */
	@Inject
	public FV2STOREB2C_05(Logger log, 
			WcWteTestRule wcWebTestRule,
			CaslFoundationTestRule caslTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_CaslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		f_CaslFixtures = p_CaslFixtures;
	}

	
	/**
	 * Helper method to execute the sign in action for each test case.
	 * 
	 * @return a new my account page object.
	 */
	public MyAccountMainPage executeSignIn()
	{
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Open Sign In/Register page
		HeaderWidget headerWidget = frontPage.getHeaderWidget().signIn()
		
		//Enter account User Name/Password and sign in
		.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD")).signIn().closeSignOutDropDownWidget();
		
		MyAccountMainPage accountPage = headerWidget.goToMyAccount();
		return accountPage;
	}
	
	/**
	 * Test case to update email address in personal information page with valid value.
	 * 
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_0501() {
		
		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Select edit personal information link
		MyPersonalInfoPage personalInfoPage = myAccountPage.goToPersonalInformationPageForEditing();
		
		//Update email address and submit personal information update
		myAccountPage = personalInfoPage.typeEMail(dsm.getInputParameter("PERSONAL_INFORMATION_PAGE_EMAIL_ADDRESS")).update();
		
		//Sign Out from account
		HeaderWidget headerSection = myAccountPage.getHeaderWidget();
		
		//Sign out user from account
		headerSection= headerSection.openSignOutDropDownWidget().signOut();
		
		SignInDropdownWidget  signInPage = headerSection.signIn();
		
		//Enter account User Name/Password and re-sign in
		myAccountPage = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD")).signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//Verify updated email address visible on My Account Page 
		myAccountPage.verifyEmailAddress(dsm.getInputParameter("PERSONAL_INFORMATION_PAGE_EMAIL_ADDRESS"));
				
	}
	
	/**
	 * Test case to add a new shipping address in my address book page.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_0502() {
		
		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open my address book page
		MyAddressBookPage addressBook = myAccountPage.getSidebar().goToMyAddressBook();
		
		//Add a new address to my address book
		addressBook.addNewAddress();
		
		//Enter new address data
		addressBook.selectAddressType(dsm.getInputParameter("ADDRESS_BOOK_ADDRESS_TYPE"))
		.typeRecipient(dsm.getInputParameter("ADDRESS_BOOK_RECIPIENT"))
		.typeFirstName(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
		.typeLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
		.typeStreetAddress(dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
		.typeCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
		.typeZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
		.typePhoneNumber(dsm.getInputParameter("ADDRESS_BOOK_PHONE_NUMBER"))
		.typeEMail(dsm.getInputParameter("ADDRESS_BOOK_EMAIL"));
		
		//Submit personal information update
		addressBook.submitSuccessfulNewAddress();
		
		//Verify address added successfully
		addressBook.getHeaderWidget().verifyMessage(dsm.getInputParameter("CONFIRMATION_MESSAGE"));

		//Indicate that address must be removed at tear down
		addressBookCleanup = true;
	}

	/**
	 * Test case to add a new billing address in my address book page.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_0503() {
		
		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open my address book page
		MyAddressBookPage addressBook = myAccountPage.getSidebar().goToMyAddressBook();
		
		//Add a new address to my address book
		addressBook.addNewAddress();
		
		//Enter new address data
		addressBook.selectAddressType(dsm.getInputParameter("ADDRESS_BOOK_ADDRESS_TYPE"))
		.typeRecipient(dsm.getInputParameter("ADDRESS_BOOK_RECIPIENT"))
		.typeFirstName(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
		.typeLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
		.typeStreetAddress(dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
		.typeCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
		.typeZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
		.typePhoneNumber(dsm.getInputParameter("ADDRESS_BOOK_PHONE_NUMBER"))
		.typeEMail(dsm.getInputParameter("ADDRESS_BOOK_EMAIL"));
		
		//Submit personal information update
		addressBook.submitSuccessfulNewAddress();
		
		//Verify address added successfully
		addressBook.getHeaderWidget().verifyMessage(dsm.getInputParameter("CONFIRMATION_MESSAGE"));
		
		//Indicate that address must be removed at tear down
		addressBookCleanup = true;
	}
	
	/**
	 * Test case to add a new Shipping and Billing address in my address book page.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_0504() {

		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open my address book page
		MyAddressBookPage addressBook = myAccountPage.getSidebar().goToMyAddressBook();
		
		//Add a new address to my address book
		addressBook.addNewAddress();
		
		//Enter new address data
		addressBook.selectAddressType(dsm.getInputParameter("ADDRESS_BOOK_ADDRESS_TYPE"))
		.typeRecipient(dsm.getInputParameter("ADDRESS_BOOK_RECIPIENT"))
		.typeFirstName(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
		.typeLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
		.typeStreetAddress(dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
		.typeCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
		.typeZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
		.typePhoneNumber(dsm.getInputParameter("ADDRESS_BOOK_PHONE_NUMBER"))
		.typeEMail(dsm.getInputParameter("ADDRESS_BOOK_EMAIL"));
		
		//Submit personal information update
		addressBook.submitSuccessfulNewAddress();
		
		//Verify address added successfully
		addressBook.getHeaderWidget().verifyMessage(dsm.getInputParameter("CONFIRMATION_MESSAGE"));
		
		//Indicate that address must be removed at tear down
		addressBookCleanup = true;
	}
	
	/**
	 * Test case to add a new shipping address to the address book with an invalid email value
	 * 
	 */
	@Test
	public void testFV2STOREB2C_0505() {

		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open my address book page
		MyAddressBookPage addressBook = myAccountPage.getSidebar().goToMyAddressBook();
		
		//Add a new address to my address book
		addressBook.addNewAddress();

		//Enter new address data
		addressBook.selectAddressType(dsm.getInputParameter("ADDRESS_BOOK_ADDRESS_TYPE"))
		.typeRecipient(dsm.getInputParameter("ADDRESS_BOOK_RECIPIENT"))
		.typeFirstName(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
		.typeLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
		.typeStreetAddress(dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
		.typeCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
		.typeZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
		.typePhoneNumber(dsm.getInputParameter("ADDRESS_BOOK_PHONE_NUMBER"))
		.typeEMail(dsm.getInputParameter("ADDRESS_BOOK_EMAIL"));
		
		//Submit personal information update
		addressBook.submitUnsuccessfulNewAddress();
		
		//Verify error message pop up on address book Page
		addressBook.verifyErrorTooltipPresent(dsm.getInputParameter("TOOLTIP_ERROR_MESSAGE"));
	}
	
	/**
	 * Test case to update the street address on the personal information page with a blank value.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_0506() {

		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open personal information page
		MyPersonalInfoPage personalInfoPage = myAccountPage.getSidebar().goToMyPersonalInfoPage();
		
		//Update street address 
		personalInfoPage.typeStreetAddress(dsm.getInputParameter("PERSONAL_INFORMATION_PAGE_STREET_ADDRESS"));
		
		//Submit personal information update
		personalInfoPage.updateFail();
		
		//Verify error message pop up on personal information page
		personalInfoPage.verifyErrorTooltipPresent(dsm.getInputParameter("TOOLTIP_ERROR_MESSAGE"));
	}
	
	/**
	 * Test case to add a new shipping address to the address book with an empty
	 * value for last name.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_0507() {

		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open my address book page
		MyAddressBookPage addressBook = myAccountPage.getSidebar().goToMyAddressBook();
		
		//Add a new address to my address book
		addressBook.addNewAddress();

		//Enter new address data
		addressBook.selectAddressType(dsm.getInputParameter("ADDRESS_BOOK_ADDRESS_TYPE"))
		.typeRecipient(dsm.getInputParameter("ADDRESS_BOOK_RECIPIENT"))
		.typeFirstName(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
		.typeLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
		.typeStreetAddress(dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
		.typeCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
		.typeZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
		.typePhoneNumber(dsm.getInputParameter("ADDRESS_BOOK_PHONE_NUMBER"))
		.typeEMail(dsm.getInputParameter("ADDRESS_BOOK_EMAIL"));
		
		//Submit personal information update
		addressBook.submitUnsuccessfulNewAddress();
		
		//Verify error message pop up on address book Page
		addressBook.verifyErrorTooltipPresent(dsm.getInputParameter("TOOLTIP_ERROR_MESSAGE"));
	}
	
	/**
	 * Test case to remove an address on the my address book page and confirm that
	 * the address has been removed.
	 * 
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_0508() {

		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open my address book page
		MyAddressBookPage addressBook = myAccountPage.getSidebar().goToMyAddressBook();
		
		//Add a new address to my address book
		addressBook.addNewAddress();
		
		//Enter new address data
		addressBook.selectAddressType(dsm.getInputParameter("ADDRESS_BOOK_ADDRESS_TYPE"))
		.typeRecipient(dsm.getInputParameter("ADDRESS_BOOK_RECIPIENT"))
		.typeFirstName(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
		.typeLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
		.typeStreetAddress(dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
		.typeCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
		.typeZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
		.typePhoneNumber(dsm.getInputParameter("ADDRESS_BOOK_PHONE_NUMBER"))
		.typeEMail(dsm.getInputParameter("ADDRESS_BOOK_EMAIL"));
		
		//Submit personal information update
		addressBook.submitSuccessfulNewAddress();
		
		//Wait for message area to close
		addressBook.getHeaderWidget().closeMessageArea();
		
		//Remove the created address
		addressBook.removeAddress(dsm.getInputParameter("ADDRESS_BOOK_RECIPIENT"));
		
		//Confirms that the removed address is no longer in the address list
		addressBook.verifyAddressIsNotInList(dsm.getInputParameter("ADDRESS_BOOK_RECIPIENT"));
	}
	
	/**
	 * Test case to create a profile on the quick checkout profile page with valid
	 * values.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_0509() {
		
		Calendar calendar = Calendar.getInstance();
	    DateFormat format = new SimpleDateFormat( dsm.getInputParameter("YEAR_FORMAT" ));
	    Date date = calendar.getTime();
		
		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open my quick checkout profile page
		QuickCheckoutProfilePage quickCheckProfile = myAccountPage.getSidebar().goToQuickCheckoutProfilePage();
		
		//Enter quick checkout profile data
		quickCheckProfile.typeBillingLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
		.typeBillingFirstName(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
		.typeBillingAddressField1(dsm.getInputParameter("ADDRESS_BOOK_BILLING_ADDRESS"))
		.typeBillingCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
		.selectBillingCountry(dsm.getInputParameter("ADDRESS_BOOK_COUNTRY"))
		.selectBillingStateOrProvince(dsm.getInputParameter("ADDRESS_BOOK_STATE"))
		.typeBillingZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
		.typeBillingEmail(dsm.getInputParameter("ADDRESS_BOOK_EMAIL"))
		.typeBillingPhoneNumber(dsm.getInputParameter("ADDRESS_BOOK_PHONE_NUMBER"))
	    .selectPaymentMethod(dsm.getInputParameter("PAYMENT_METHOD"))
	    .typeCardNumber(dsm.getInputParameter("CREDIT_CARD_NUMBER"))
	    .selectExpirationMonth(dsm.getInputParameter("EXPIRY_MONTH"))
	    .selectExpirationYear(format.format(date))
	    .typeShippingLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
	    .typeShippingFirstName(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
	    .typeShippingAddressField1(dsm.getInputParameter("ADDRESS_BOOK_SHIPPING_ADDRESS"))
	    .typeShippingCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
	    .selectShippingCountry(dsm.getInputParameter("ADDRESS_BOOK_COUNTRY"))
	    .selectShippingStateOrProvince(dsm.getInputParameter("ADDRESS_BOOK_STATE"))
	    .typeShippingZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
	    .typeShippingEmail(dsm.getInputParameter("ADDRESS_BOOK_EMAIL"))
	    .typeShippingPhoneNumber(dsm.getInputParameter("ADDRESS_BOOK_PHONE_NUMBER"))
	    .selectShippingMethod(Integer.parseInt(dsm.getInputParameter("SHIP_TYPE_METHOD")));
		
		//Submit the quick checkout profile update
		quickCheckProfile.update();
		
		//Verify quick checkout profile created successfully
		quickCheckProfile.getHeaderWidget().verifyMessage(dsm.getInputParameter("PROFILE_UPDATE_SUCCESS_MESSAGE"));
		
	}
	
	/**
	 * Test case to update the payment method on the quick checkout profile page with valid
	 * values and confirm that the information remains updated.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_0510() {

		Calendar calendar = Calendar.getInstance();
	    DateFormat format = new SimpleDateFormat(dsm.getInputParameter("YEAR_FORMAT" ));
	    Date date = calendar.getTime();
	    
		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open my quick checkout profile page
		QuickCheckoutProfilePage quickCheckProfile = myAccountPage.getSidebar().goToQuickCheckoutProfilePage();
		
		//Update quick checkout profile data
		quickCheckProfile.selectPaymentMethod(dsm.getInputParameter("PAYMENT_METHOD"))
		.typeCardNumber(dsm.getInputParameter("CREDIT_CARD_NUMBER"))
		.selectExpirationMonth(dsm.getInputParameter("EXPIRY_MONTH"))
		.selectExpirationYear(format.format(date));
		
		//Submit the quick checkout profile update
		quickCheckProfile.update();
		
		//Verify quick checkout profile updated successfully
		HeaderWidget headerSection = quickCheckProfile.getHeaderWidget().verifyMessage(dsm.getInputParameter("PROFILE_UPDATE_SUCCESS_MESSAGE"));
		
		//Open My Account page and then open Quick Checkout Profile page in store
		MyPersonalInfoPage personalInfoPage = headerSection.goToMyAccount().getSidebar().goToMyPersonalInfoPage();
		
		quickCheckProfile = personalInfoPage.getSidebar().goToQuickCheckoutProfilePage();
		
		//Verify the payment method remains updated
		quickCheckProfile.verifyPaymentMethod(dsm.getInputParameter("PAYMENT_METHOD"));
		
	
	}
	
	/**
	 * Test case to update quick checkout profile with an available shipping methods.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_0511() {
		
		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open my quick checkout profile page
		QuickCheckoutProfilePage quickCheckoutProfile = myAccountPage.getSidebar().goToQuickCheckoutProfilePage();
		
		//Update quick checkout profile data
		quickCheckoutProfile.selectShippingMethod(dsm.getInputParameterAsNumber("SHIP_TYPE_METHOD", Integer.class));
		
		//Submit the quick checkout profile update
		quickCheckoutProfile.update();

		//Verify quick checkout profile updated successfully
		quickCheckoutProfile.getHeaderWidget().verifyMessage(dsm.getInputParameter("PROFILE_UPDATE_SUCCESS_MESSAGE"));
		
		//verification
		myAccountPage = quickCheckoutProfile.getHeaderWidget().goToMyAccount();
		
		quickCheckoutProfile = myAccountPage.getSidebar().goToQuickCheckoutProfilePage();
		
		quickCheckoutProfile.verifyShippingMethod(dsm.getInputParameterAsNumber("SHIP_TYPE_METHOD", Integer.class));
		
	}
	
	/**
	 * Test case to update Quick checkout profile with an invalid mandatory value.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_0512() {
		
		Calendar calendar = Calendar.getInstance();
	    DateFormat format = new SimpleDateFormat(dsm.getInputParameter("YEAR_FORMAT" ));
	    Date date = calendar.getTime();
		
		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open my quick checkout profile page
		QuickCheckoutProfilePage quickCheckoutProfile = myAccountPage.getSidebar().goToQuickCheckoutProfilePage();
		
		//Update quick checkout profile data
		quickCheckoutProfile.selectPaymentMethod(dsm.getInputParameter("PAYMENT_METHOD"))
		.typeCardNumber(dsm.getInputParameter("CREDIT_CARD_NUMBER"))
		.selectExpirationMonth(dsm.getInputParameter("EXPIRY_MONTH"))
		.selectExpirationYear(format.format(date));
		
		//Submit the quick checkout profile update
		quickCheckoutProfile.updateFail();
		
		//Verify quick checkout profile update fail with error message
		quickCheckoutProfile.getHeaderWidget().verifyErrorMessage(dsm.getInputParameter("MESSAGE_TEXT"));
		
	}
	
	/**
	 * Test case to update the shipping address with a valid phone number.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_0513()
	{
		
		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open my quick checkout profile page
		MyAddressBookPage addressBook = myAccountPage.getSidebar().goToMyAddressBook();
		
		//Updates the address information
		addressBook.selectAddress(dsm.getInputParameter("CHANGE_ADDRESS_BOOK_ADDRESS_ID"))
		.selectAddressType(dsm.getInputParameter("ADDRESS_BOOK_ADDRESS_TYPE"))
		.typePhoneNumber(dsm.getInputParameter("ADDRESS_BOOK_PHONE_NUMBER"));
		
		//Submit address book information update
		addressBook.updateSuccessfulAddress();
		
		//Verify address added successfully
		addressBook.getHeaderWidget().verifyMessage(dsm.getInputParameter("CONFIRMATION_MESSAGE")).closeMessageArea();
	
		//Set default address
		addressBook.selectAddress(dsm.getInputParameter("CHANGE_ADDRESS_BOOK_ADDRESS_ID"))
		.selectAddressType(dsm.getInputParameter("ADDRESS_BOOK_ADDRESS_TYPE")).updateSuccessfulAddress();
		
	}
	
	/**
	 * Test case to update the quick checkout profile billing address with a
	 * valid city.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_0514() {

		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open my quick checkout profile page
		QuickCheckoutProfilePage quickCheckoutProfile = myAccountPage.getSidebar().goToQuickCheckoutProfilePage();
		
		//Update quick checkout profile data
		quickCheckoutProfile.typeBillingCity(dsm.getInputParameter("ADDRESS_CITY"));
		
		//Submit the quick checkout profile update
		quickCheckoutProfile.update();
		
		//Verify quick checkout profile updated successfully
		quickCheckoutProfile.getHeaderWidget().verifyMessage(dsm.getInputParameter("CONFIRMATION_MESSAGE"));	
	}
	
	/**
	 * Test case to update the quick checkout profile billing address with an invalid email.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_0515() {

		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open my quick checkout profile page
		QuickCheckoutProfilePage quickCheckoutProfile = myAccountPage.getSidebar().goToQuickCheckoutProfilePage();
		
		//Update quick checkout profile data
		quickCheckoutProfile.typeBillingEmail(dsm.getInputParameter("ADDRESS_EMAIL"));
		
		//Submit the quick checkout profile update expected to fail
		quickCheckoutProfile.updateFailTooltip();

		//Verify error message pop up on quick checkout page
		quickCheckoutProfile.verifyEmailFormatErrorTooltipPresent();
		
	}
	
	/**
	 * Test case to update the quick checkout profile with an empty credit card number.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_0516() 
	{

		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open my quick checkout profile page
		QuickCheckoutProfilePage quickCheckoutProfile = myAccountPage.getSidebar().goToQuickCheckoutProfilePage();
		
		//Update quick checkout profile data
		quickCheckoutProfile.typeCardNumber(dsm.getInputParameter("CREDIT_CARD_NUMBER"));
		
		//Submit the quick checkout profile update expected to fail
		quickCheckoutProfile.updateFailTooltip();
		
		//Verify error message pop up on quick check out page
		quickCheckoutProfile.verifyEmptyFieldErrorTooltipPresent();
	}
	
	/**
	 * Tear down ran after every test case
	 */
	@After
	public void tearDown() {
		
		try
		{
			//Open up browser session continuing at the header section
			HeaderWidget headerSection = getSession().continueToPage(getConfig().getStoreUrl(), HeaderWidget.class);
			
			if(addressBookCleanup == true){
				
				//Go to my account page
				MyAccountMainPage myAccountPage = headerSection.goToMyAccount();
				
				//Open address book page from left side bar on my account page
				MyAddressBookPage addressBook = myAccountPage.getSidebar().goToMyAddressBook();
				
				//Remove address from address book
				addressBook.removeAddress(dsm.getInputParameter("ADDRESS_BOOK_RECIPIENT"));
				
			}
			
			addressBookCleanup = false;

			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());	
			orders.deletePaymentMethod();
			orders.removeAllItemsFromCart();
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

	}
	
}