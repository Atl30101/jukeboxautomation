package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.ApparelSubCategoryPage;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 * Scenario: FV2STOREB2C_65
 * Details: Navigate image map contents
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_65 extends AbstractAuroraSingleSessionTests
{
	
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	

	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 */	@Inject
	public FV2STOREB2C_65(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
	}
	/**
	 * Navigate hotspots in image map
	 */
	
	@Test
	public void testFV2STOREB2C_6501()
	{
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Navigate to women's category page
		ApparelSubCategoryPage deptCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(ApparelSubCategoryPage.class, dsm.getInputParameter("TOP_CAT"), dsm.getInputParameter("SUB_CAT"));
		
    	//Navigate to Dresses department page by clicking the link in the main image
		CategoryPage dressDepartment = deptCat.goToPageByImageAreaLink(dsm.getInputParameter("DRESS_CAT_NAME"), CategoryPage.class);
		
		//Make sure it navigates to the blouses page
		dressDepartment.getHeadingWidget().verifyHeadingText(dsm.getInputParameter("DRESS_CAT_NAME"));
		
		//Go back to the women's category page
		deptCat = getSession().goBack(ApparelSubCategoryPage.class);
		
    	//Navigate to handbags department page by clicking the link in the main image
		CategoryPage handbagDepartment = deptCat.goToPageByImageAreaLink(dsm.getInputParameter("HANDBAG_CAT_NAME"), CategoryPage.class);
		
    	//Make sure it navigates to the handbags page
    	handbagDepartment.getHeadingWidget().verifyHeadingText(dsm.getInputParameter("HANDBAG_CAT_NAME"));
	}
	
	
}
