package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.BundleDisplayPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.KitDisplayPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.MyOrderHistoryPage;
import com.ibm.commerce.qa.aurora.page.NonRegisteredShippingBillingInfoPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 * Scenario: FV2STOREB2C_07
 * Details: Test add to shopping cart flows.
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_07b extends AbstractAuroraSingleSessionTests
{

    /**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	
	private final CaslFixturesFactory f_CaslFixtures;
	

	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_CaslFixtures 
	 */	@Inject
	public FV2STOREB2C_07b(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_CaslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		f_CaslFixtures =  p_CaslFixtures;
		
	}
	/**
	 * Test case to add a product with attributes to the shopping cart without selecting attribute values as a registered shopper
	 *
	 *
	 */
	
	@Test
	public void testFV2STOREB2C_0705()
	{
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		//Navigate to category page
		CategoryPage subCat = signIn.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"),dsm.getInputParameter("SUB_CAT"), dsm.getInputParameter("CATEGORY_ITEM"));
		
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		productDisplay.addToCartRequest();
		
		productDisplay.getHeaderWidget().verifyErrorMessage(dsm.getInputParameter("NOT_ADDED_TO_CART_MESSAGE"));
		
		
	}
	
	/**
	 * Test case to add a Product to the shopping cart with 0 quantity as a registered shopper
	 *
	 *
	 */
	
	@Test
	public void testFV2STOREB2C_0706()
	{
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		//Navigate to category page
		CategoryPage subCat = signIn.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"),dsm.getInputParameter("SUB_CAT"), dsm.getInputParameter("CATEGORY_ITEM"));
		
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		productDisplay.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("ATTRIBUTE_NAME1"), dsm.getInputParameter("ATTRIBUTE_VALUE1"));
		
		productDisplay.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("ATTRIBUTE_NAME2"), dsm.getInputParameter("ATTRIBUTE_VALUE2"));
		
		productDisplay.getShopperActionsWidget().updateQuantity(dsm.getInputParameter("NEW_QUANTITY"));
			
		productDisplay.addToCartRequest();
		
		productDisplay.getHeaderWidget().verifyErrorMessage(dsm.getInputParameter("NOT_ADDED_TO_CART_MESSAGE"));	
	}
	
	
	/**
	 * Add a product with attributes to the shopping cart from the product details page as a registered shopper
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_0707() 
	{
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		//cleanup
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.removeAllItemsFromCart();
		
		//Go to department page				
		CategoryPage subCat = signIn.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"),dsm.getInputParameter("SUB_CAT"), dsm.getInputParameter("CATEGORY_ITEM"));
		
		//Go to product display page
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//set attributes with the specified values
		productDisplay.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("ATTRIBUTE_NAME"), dsm.getInputParameter("ATTRIBUTE_VALUE"));
		
		//set attributes with the specified values
		productDisplay.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("ATTRIBUTE_NAME_2"), dsm.getInputParameter("ATTRIBUTE_VALUE_2"));
		
		//click on add to cart button from product display page
		productDisplay.addToCart();
		
		productDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//Confirm the item count in mini shopping cart
		productDisplay.getHeaderWidget().verifyMiniCartItemCount(dsm.getInputParameter("NUMBER_OF_ITEMS"));
		
		//Confirm the item total price in mini shopping cart
		productDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
		
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();
	}
	
	/**
	 * Add a product to the shopping cart from the sub category page as a registered shopper
	 * 
	 */
	@Test
	public void testFV2STOREB2C_0708() 
	{
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		//cleanup
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.removeAllItemsFromCart();
		
		//Navigate to category page
		CategoryPage department =signIn.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"), dsm.getInputParameter("SUB_CAT"));
		
		//Click product on Department ESPOT
		ProductDisplayPage productDisplay = department.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//click Add to Cart from Product Display page.
		productDisplay.addToCart();
		
		productDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//Confirm the price on minishopping cart
		productDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
		
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();		
	}
	
	/**
	 * Test case to add products, kits and bundles to the shopping cart as a guest shopper
	 *
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_0709() 
	{		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
	
		//Navigate to category page
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class,dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//Go to product display page
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select the product swatches
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR")).selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));
		
		//click Add to Cart from Product Display page.
		productDisplay.addToCart();
		
		//Go back to the home page
		productDisplay.getHeaderWidget().goToFrontPage();
		
		//Navigate to category page
		subCat = productDisplay.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CATEGORY_NAME_KIT"),dsm.getInputParameter("SUB_CATEGORY_NAME_KIT"));
		
		//Go to product display page
		KitDisplayPage kitDisplay = subCat.getCatalogEntryListWidget().goToKitPagebyIdentifier(dsm.getInputParameter("KIT_NAME"));
		
		//click Add to Cart from Product Display page.
		kitDisplay.addToCart();
		
		//Go back to the home page
		productDisplay.getHeaderWidget().goToFrontPage();
		
		//Perform search keyword
		SearchResultsPage searchResult = productDisplay.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		
		//Click product on search result page
		BundleDisplayPage bundleDisplay = searchResult.getCatalogEntryListWidget().goToBundleDisplayPageByName(dsm.getInputParameter("BUNDLE_NAME"));
		
		//click Add to Cart from Product Display page.
		bundleDisplay.addToCart();
						
		//Go to shop cart page
		ShopCartPage shopCart = bundleDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//click on the Continue Checkout button
		NonRegisteredShippingBillingInfoPage guestUserShipBillInfo = shopCart.continueAsGuestUser();
		
		//Enter info on Shipping and Billing info page
		ShippingAndBillingPage shippingAndBilling = guestUserShipBillInfo.typeBillRecipient(dsm.getInputParameter("RECIPIENT"))
							 .typeBillLastName(dsm.getInputParameter("LAST_NAME"))
							 .typeBillFirstName(dsm.getInputParameter("FIRST_NAME"))
							 .typeBillAddress(dsm.getInputParameter("STREET_ADDRESS"))
							 .typeBillCity(dsm.getInputParameter("CITY"))
							 .selectBillCountry(dsm.getInputParameter("COUNTRY"))
							 .selectBillState(dsm.getInputParameter("PROVINCE"))
							 .typeBillZipCode(dsm.getInputParameter("POSTAL_CODE"))
							 .typeBillPhone(dsm.getInputParameter("PHONE_NUMBER"))
							 .typeBillEmail(dsm.getInputParameter("EMAIL"))
							 .applyToBothShippingBilling()
							 .submitSuccessfulForm();
		
		//Confirm ship as complete option is selected.
		shippingAndBilling.verifyShipAsCompleteIsChecked();
		
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm the correct shipping address is selected
		shippingAndBilling.verifyShippingAddressSelected(dsm.getInputParameter("RECIPIENT"));
		
		//select Cash on delivery as the payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
		
		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	
		
		//Confirm product name
		singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm item quantity
		singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_QTY"));
		
		//Confirm item each price
		singleOrderSummary.verifyItemEachPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_EACH_TOTAL"));
		
		//Confirm total price of the item.
		singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_TOTAL"));
		
		//Confirm order subtotal 
		singleOrderSummary.verifyOrderSubtotal(dsm.getInputParameter("ORDER_SUBTOTAL"));
		
		//Confirm the total price of the order
		singleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL"));						
		
		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
		
		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();	
	}
	
	/**
	 * Add a product to the shopping cart from the previous order by re-ordering as a registered shopper
	 *
	 */
	
	@Test
	public void testFV2STOREB2C_0710() 
	{		
		//complete flow to reorder using service layer
		
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.removeAllItemsFromCart();
		
		orders.addItem(dsm.getInputParameter("SKU_NAME"), dsm.getInputParameterAsNumber("ITEM_QTY", Double.class));
		
		String orderId = orders.getCurrentOrderId();
		
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();	
		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		MyAccountMainPage myAccount= signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		
		//Go to my order history page
		MyOrderHistoryPage orderHistory = myAccount.getSidebar().goToOrderHistoryPage();
		
		//Click re-order button
		ShopCartPage shopCartPage = orderHistory.clickReOrderButton(orderId);
		
		shopCartPage.verifyItemInShopCart(dsm.getInputParameter("SKU_NAME"));
		
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();		
		
	}
	
	/**
	 * Add a product to the shopping cart from the Search Results Page
	 * 
	 */
	@Test
	public void testFV2STOREB2C_0711()
	{
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		//cleanup
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.removeAllItemsFromCart();
		
		//Navigate to Search Results PAge
		SearchResultsPage searchResults = signIn.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH"));
		
		//Click product on Department ESPOT
		ProductDisplayPage productDisplay = searchResults.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//click Add to Cart from Product Display page.
		productDisplay.addToCart();
		
		productDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//Confirm the price on minishopping cart
		productDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
		
		
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();		
	}
	
	/**
	 * Add a Product to the shopping cart with an invalid quantity as a registered shopper
	 * 
	 */
	@Test
	public void testFV2STOREB2C_0712()
	{
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		//cleanup
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.removeAllItemsFromCart();
		
		//Navigate to Search Results PAge
		SearchResultsPage searchResults = signIn.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH"));
		
		//Click product on Department ESPOT
		ProductDisplayPage productDisplay = searchResults.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		productDisplay.getShopperActionsWidget().updateQuantity(dsm.getInputParameter("ITEM_QTY"));
		
		//click Add to Cart from Product Display page.
		productDisplay.addToCartFail(dsm.getInputParameter("ERROR_MSG"));
			
	}
	
	/**
	 * Perform teardown operations after the test is done, whether it is successful or not.
	 */
	@After
	public void testScenarioTearDown()
	{
		try
		{
			getLog().info("testScenarioTearDown running");
			
			//remove all product in cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
			
			orders.removeAllItemsFromCart();
			orders.deletePaymentMethod();
				
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

	}
	
}
