package com.ibm.commerce.qa.aurora.tests;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.GenericCategoryPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.url.DeltaUpdates;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


/**			
* Scenario: FV2STOREB2C_28
* Details: This scenario will test Catalog Category Management.
*
* 
*/
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_28 extends AbstractAuroraSingleSessionTests
{
	@DataProvider
	private final TestDataProvider dsm;
	
	private CMC cmc;
	private final CaslFixturesFactory f_CaslFixtures;
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dataSetManager
	 * @param cmc
	 * @param deltaUpdate 
	 */
	@Inject
	public FV2STOREB2C_28(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CMC cmc,
			DeltaUpdates deltaUpdate,
			CaslFixturesFactory p_CaslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		this.cmc = cmc;
		this.deltaUpdate = deltaUpdate;
		f_CaslFixtures = p_CaslFixtures;
	}
	
	/**A variable to hold the master catalog**/
	private String masterCatalogId;

	/**A variable to hold delta update**/
	private DeltaUpdates deltaUpdate;
	
	/**A variable to hold the catalog group identifier created**/
	private String catGroupId;
	
	private String catEntryId;
	
	/**
	 * A test to Create a master category using CMC and view from store front.
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_2801() throws Exception
	{	
		
		//login to CMC
		dsm.setDataLocation("testFV2STOREB2C_2801", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore();
		cmc.selectCatalog();
		String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
		
		String defaultCat = cmc.getDefaultCatalog();
		cmc.removeDefaultSalesCatalog(defaultCat);
		
		//Create a new Category
		dsm.setDataLocation("testFV2STOREB2C_2801", "createCatalogGroup");
		catGroupId = cmc.createCatalogGroup("0",dsm.getInputParameter("identifierId"),dsm.getInputParameter("CATALOG_NAME"),dsm.getInputParameter("published"));
		
		dsm.setDataLocation("testFV2STOREB2C_2801", "createProduct");
		String partNumber = dsm.getInputParameter("partNumber");
		catEntryId = cmc.createProduct(dsm.getInputParameter("parentCategory"),partNumber,dsm.getInputParameter("productName"),dsm.getInputParameter("published"));
		
		//set first offer price and list price
		dsm.setDataLocation("testFV2STOREB2C_2801", "offerPrice1");
		cmc.createOfferPriceForCatalogEntry(catEntryId);
		cmc.createListPriceForCatalogEntry(catEntryId);
		
		//set second offer price
		dsm.setDataLocation("testFV2STOREB2C_2801", "offerPrice2");
		cmc.createOfferPriceForCatalogEntry(catEntryId);
		
		//set third offer price
		dsm.setDataLocation("testFV2STOREB2C_2801", "offerPrice3");
		cmc.createOfferPriceForCatalogEntry(catEntryId);
		
		//create attributes for the created product
		dsm.setDataLocation("testFV2STOREB2C_2801", "createProductAttribute");
		String attributeId = cmc.createProductDefiningAttribute(catEntryId);
		
		//create attributes values for the product
		cmc.createProductDefiningAttributeValues(catEntryId, attributeId, dsm.getInputParameter("attributeValue1"),"1");
		cmc.createProductDefiningAttributeValues(catEntryId, attributeId, dsm.getInputParameter("attributeValue2"),"2");
		
		//create an SKU for the product
		dsm.setDataLocation("testFV2STOREB2C_2801", "createSKU");
		String itemId = cmc.createSKU(catEntryId,dsm.getInputParameter("partNumber"),dsm.getInputParameter("skuName"),dsm.getInputParameter("published"));
		cmc.addSKUDefiningAttributeValue(catEntryId,itemId,attributeId,dsm.getInputParameter("attributeValue"),dsm.getInputParameter("languageId"));
		
		
		//set first offer price and list price for the SKU
		dsm.setDataLocation("testFV2STOREB2C_2801", "offerPrice1");
		cmc.createOfferPriceForCatalogEntry(itemId);
		cmc.createListPriceForCatalogEntry(itemId);
		
		//set second offer price
		dsm.setDataLocation("testFV2STOREB2C_2801", "offerPrice2");
		cmc.createOfferPriceForCatalogEntry(itemId);
		
		//set third offer price
		dsm.setDataLocation("testFV2STOREB2C_2801", "offerPrice3");
		cmc.createOfferPriceForCatalogEntry(itemId);
		
		dsm.setDataLocation("testFV2STOREB2C_2801","testFV2STOREB2C_2801");
		
		masterCatalogId = cmc.selectCatalog();
		
		//Wait until delta update completes before timeout
		deltaUpdate.beforeWaitForDelta(masterCatalogId);
		getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
		deltaUpdate.waitForDelta(masterCatalogId, 60);
		
		//log off from CMC
		cmc.logoff();
		
		//Login with valid user id and password
		AuroraFrontPage	frontPage  = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn().typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"));
		
		HeaderWidget header = signIn.signIn().closeSignOutDropDownWidget();
		
		GenericCategoryPage dept = header.goToDepartmentByName(GenericCategoryPage.class, dsm.getInputParameter("CATEGORY_NAME"));
		
		//verify newly created product is present in the new category
		dept.getCatalogEntryListWidget().verifyProductIsPresent(catEntryId);

		//Click on log out link
		dept.getHeaderWidget().openSignOutDropDownWidget().signOut();
		
	}
	
	/**
	 * Perform tear down operations such as stopping the test harness. This method will also delete the catalog groups created in this scenario.
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception
	{	
		try
		{
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
//		//Login with valid user id and password
//		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
//		signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"));
//		
////		//click on Shopping Cart link from the header
////		ShopCartPage cart = signIn.signIn().goToShoppingCartPage();
////		cart = cart.removeAllItems();
		//Clean up shopping cart and payment methods
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		orders.removeAllItemsFromCart();
		orders.deletePaymentMethod();
		
		//login to CMC
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.selectStore();
		cmc.selectCatalog();
		String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
		//Delete catalog entries
		cmc.deleteCatalogEntry(catEntryId);
		
		//Delete the Category created
		cmc.deleteCatalogGroup(catGroupId);
		
		//Wait until delta update completes before timeout
		deltaUpdate.beforeWaitForDelta(masterCatalogId);
		getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
		deltaUpdate.waitForDelta(masterCatalogId, 60);
		
		//log off from CMC
		cmc.logoff();
	
	}
	catch(RuntimeException e)
	{
		getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
	}
	}
}
