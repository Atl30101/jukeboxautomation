package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.BundleDisplayPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.ComparePage;
import com.ibm.commerce.qa.aurora.page.KitDisplayPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.MyWishListPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.QuickInfoPopup;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 * Scenario: FV2STOREB2C_07
 * Details: Test add to shopping cart flows.
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_07 extends AbstractAuroraSingleSessionTests
{

    /**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	
	private final CaslFixturesFactory f_CaslFixtures;

	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_CaslFixtures 
	 */	@Inject
	public FV2STOREB2C_07(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_CaslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		
		f_CaslFixtures = p_CaslFixtures;
		
	}
	
	/**
	 * Test case to add a product to the shopping cart from the product's details page as a registered shopper
	 *
	 * @throws Exception
	 */	
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_0701() throws Exception
	{
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		MyAccountMainPage myAccount = signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//cleanup
		cartCleanup(myAccount);
		
		//Go to department page				
		CategoryPage subCat= myAccount.getHeaderWidget()
				.goToCategoryPageByHierarchy(CategoryPage.class, 
						dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
						dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME") , 
						dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//click on product image on Category Display page.
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select the product swatches
		productDisplay
			.getDefiningAttributesWidget()
			.selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR"))
			.selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));
		
		//click Add to Cart from Product Display page.
		productDisplay.addToCart();
		
		productDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//Confirm the price on minishopping cart
		productDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));

		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();
	
	}
	
	/** 
	 * Add a Bundle to the shopping cart from the search results page as a registered shopper
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_0702() throws Exception
	{		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
						
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
						
		//Log in to the store.
		MyAccountMainPage myAccount = signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();				
		
		//cleanup
		cartCleanup(myAccount);
		
		//Perform search for keywork bundle
		SearchResultsPage searchResult = myAccount.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		
		//Click on bundle image
		BundleDisplayPage bundleDisplay = searchResult.getCatalogEntryListWidget().goToBundleDisplayPageBySKU(dsm.getInputParameter("BUNDLE_SKU"));
		
		//Update bundle quantity
		bundleDisplay.updateBundleQuantity(dsm.getInputParameter("FIRST_ITEM_IN_BUNDLE"), (dsm.getInputParameter("FIRST_ITEM_IN_BUNDLE_QUANTITY")));
		
		//Update bundle quantity
		bundleDisplay.updateBundleQuantity(dsm.getInputParameter("SECOND_ITEM_IN_BUNDLE"), (dsm.getInputParameter("SECOND_ITEM_IN_BUNDLE_QUANTITY")));
		
		//Add to cart
		bundleDisplay.addToCart();
		
		//Verify product is in mini cart
		bundleDisplay.getHeaderWidget().verifyProductNameInMiniCart(dsm.getInputParameter("FIRST_ITEM_IN_BUNDLE"));
		
		//Verify product is in mini cart
		bundleDisplay.getHeaderWidget().verifyProductNameInMiniCart(dsm.getInputParameter("SECOND_ITEM_IN_BUNDLE"));
		
		bundleDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//Confirm the item count in mini shopping cart
		bundleDisplay.getHeaderWidget().verifyMiniCartItemCount(dsm.getInputParameter("NUMBER_OF_ITEMS"));
		
		//Confirm the item total price in mini shopping cart
		bundleDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
				
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.addPayLaterPaymentMethod();
		
		// If this is not the second order placed by this user; then the expected price is $495.00 instead of $515.00
		orders.completeOrder();				
	}
	
	/**
	 * Add a Kit to the shopping cart from the Wish List of a registered shopper
	 *
	 * 
	 * @throws Exception
	 */
	
	@Test
	public void testFV2STOREB2C_0703() throws Exception
	{
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		MyAccountMainPage myAccount = signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//cleanup
		cartCleanup(myAccount);
		
		//Navigate to category page
		CategoryPage subCat = myAccount.getHeaderWidget()
				.goToCategoryPageByHierarchy(CategoryPage.class, 
						dsm.getInputParameter("TOP_CAT"), 
						dsm.getInputParameter("CATEGORY_ITEM"));
		
		//Go to product display page
		KitDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToKitPagebyIdentifier(dsm.getInputParameter("KIT_NAME"));
		
		//Add kit to wish list
		productDisplay.addToWishlistLoggedIn();
		
		//Go to my account page
		myAccount = productDisplay.getHeaderWidget().goToMyAccount();
		
		//Go to wish list page
		MyWishListPage wishList = myAccount.getSidebar().goToWishListPag();
		
		//Select a wish list
		wishList.selectWishList(dsm.getInputParameter("WISHLISTNAME"));
		
		//Click Add to cart to add product
		wishList.addToCart(dsm.getInputParameter("KIT_NAME"));
		
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();					
	}
	
	
	
	
	
	/**
	 * Add a SKU to the shopping cart from the product compare page as a registered shopper
	 *
	 * 
	 * @throws Exception
	 */
	
	@Test
	public void testFV2STOREB2C_0704() throws Exception
	{
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		MyAccountMainPage myAccount = signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//cleanup
		cartCleanup(myAccount);
		
		//Navigate to category page
		CategoryPage subCat = myAccount.getHeaderWidget()
				.goToCategoryPageByHierarchy(CategoryPage.class, 
						dsm.getInputParameter("TOP_CAT"),
						dsm.getInputParameter("SUB_CAT"), 
						dsm.getInputParameter("CATEGORY_ITEM"));
		
		//Select compare box that appears on product
		subCat.getCatalogEntryListWidget().allowProductsForComparisonByPosition(new Integer(dsm.getInputParameter("ITEM_POSITION_1")));
		
		//Select compare box that appears on product
		subCat.getCatalogEntryListWidget().allowProductsForComparisonByPosition(new Integer(dsm.getInputParameter("ITEM_POSITION_2")));
		
		//Click compare button
		ComparePage productCompare = subCat.getCatalogEntryListWidget().compareProductsByPostion(new Integer(dsm.getInputParameter("ITEM_POSITION_1")));
		
		//Click add to cart button
		QuickInfoPopup quickInfo = productCompare.goToQuickInfoPopupByPosition(dsm.getInputParameter("ITEM_POSITION"));
		
		//Select attribute for product
		quickInfo.updateValueForAttribute(dsm.getInputParameter("ATTRIBUTE_NAME1"), dsm.getInputParameter("ATTRIBUTE_VALUE1"));
		
		//Select attribute for product
		quickInfo.updateValueForAttribute(dsm.getInputParameter("ATTRIBUTE_NAME2"), dsm.getInputParameter("ATTRIBUTE_VALUE2"));
		
		//Click add to cart on quick info
		productCompare = quickInfo.addToCart(ComparePage.class);
		
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();				
	}
	
	//cleanup method to remove all items from the cart before proceeding.
	
	private void cartCleanup(MyAccountMainPage page)
	{
		//cart isn't 0, cleaning up
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		orders.removeAllItemsFromCart();
		
	}
	
	/**
	 * Perform teardown operations after the test is done, whether it is successful or not.
	 */
	@After
	public void testScenarioTearDown()
	{
		try
		{
			getLog().info("testScenarioTearDown running");
			
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());	
			orders.deletePaymentMethod();
			orders.removeAllItemsFromCart();
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

	}
	
	
}
