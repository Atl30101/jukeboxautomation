package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.IBMCopyright;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;




//Declares a new test scenario called FV2STOREB2C_SOLR_SEARCH_01 which contains one or more test case methods.
//All test scenario classes must extend StoreModelTestCase.

/**
 * Scenario: FV2STOREB2C_SOLR_SEARCH_01 Objective: To specify a search term in the search box
 * Pre-requisites: Tester knows the Store URL
 * (http://<hostname>/webapp/wcs/stores/servlet/en/auroraesite) 
 * Search based navigation is enabled
 *  Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_SOLR_SEARCH_01 extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = IBMCopyright.SHORT_COPYRIGHT;

	//A variable to hold the name of the data file where input parameters can be found.
	//$ANALYSIS-IGNORE
	protected final String dataFileName = "src/data/FV2STOREB2C_SOLR_SEARCH_01_Data.xml";

	@DataProvider
	private final TestDataProvider dsm;
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dsm
	 */
	@Inject
	public FV2STOREB2C_SOLR_SEARCH_01(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dsm)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dsm;
	}
	
	
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Test case to test shopper chooses a keyword from a list of suggestions using the mouse
	 * Preconditions: <ul>
	 * 						<li>None</li>
	 * 				   </ul>
	 * Post conditions: Search results page displays the product match for the keyword search.
	 * Change flow options: Search based navigation is enabled
	 * 
	 * @throws Exception
	 */
	
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_0102() throws Exception {
		dsm.setDataFile(dataFileName);
		// Tell which data block to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_0102", "testFV2STOREB2C_SOLR_SEARCH_0102_DATABLOCK");
		
		// Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		HeaderWidget header = frontPage.getHeaderWidget();
		
		// Type a character in search term input field
		header.typeSearchTerm(dsm.getInputParameter("SEARCH_TERM_1"), true, true);
		
		//Verify some keywords are in search pup up window
		header.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		
		//Verify the search term entered is highlighted in search pop up window.
		header.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_1"));

		//Type one more character in search term input field
		header.typeSearchTerm(dsm.getInputParameter("SEARCH_TERM_2"), false, true);
		
		//Verify some keywords are in search pop up window
		header.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_3"));
		
		//Verify the search term entered is highlighted in search pop up window.
		header.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_2"));
		
		//Select a keyword from the search pop up window		
		SearchResultsPage searchResults =  header.selectProductKeywordInSearchSuggestion(dsm.getInputParameter("EXPECTED_KEYWORD_4"));

		//verify the expected results are present
		searchResults.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("PRODUCT1"));
		searchResults.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("PRODUCT2"));
	}

	/**
	 * Test case to test Shopper enters a keyword that is not found by the search
	 * Post conditions: The item suggestion does not return any matching
	 * keywords and the pop up does not appear.
	 * 
	 * @throws Exception
	 */
	@Category(Sanity.class)
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_0104() throws Exception {
		dsm.setDataFile(dataFileName);
		// Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_0104", "testFV2STOREB2C_SOLR_SEARCH_0104_DATABLOCK");

		// Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Type search term input field
		frontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCH_TERM_NON_EXIST"), false, false);
	}

	/**
	 * Test case to test back button/refresh button when entering search keyword or running a search
	 * Post conditions: 1. Search is initiated and search
	 * results are displayed. 2. When search results page is refreshed, results
	 * are retained. 3. When the back button is pressed, the customer returns to
	 * their previous page.
	 * 
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_0107() throws Exception {
		dsm.setDataFile(dataFileName);
		// Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_0107", "testFV2STOREB2C_SOLR_SEARCH_0107_DATABLOCK");

		// Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//perform search
		SearchResultsPage searchResults = frontPage.getHeaderWidget().performSearch(SearchResultsPage.class,dsm.getInputParameter("searchString1"));

		
		//verify result is present
		searchResults.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("expectedResult1"));
		//perform search
		searchResults.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("searchString2"));
		//verify result is present		
		searchResults.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("expectedResult2"));
		//Click the browser back button
		getSession().goBack(SearchResultsPage.class);
		//verify expectedResult1 is present when back button is pressed
		searchResults.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("expectedResult1"));
	}

	/**
	 * Test case to test Shopper enters a keyword in the search box in upper case letters.
	 * Post conditions: Search results return products matching the
	 * text entered in the search, ignoring the case of the letters
	 * 
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_0108() throws Exception {
		dsm.setDataFile(dataFileName);

		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_0108", "testFV2STOREB2C_SOLR_SEARCH_0108_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//search for a keyword
		SearchResultsPage searchResults = frontPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM_COFFEE"));
		//verify the expected results are present
		searchResults.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("product1"));
		searchResults.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("product2"));
	}
///////////////////////////////////////////////////////////////////////////////////////////////

}