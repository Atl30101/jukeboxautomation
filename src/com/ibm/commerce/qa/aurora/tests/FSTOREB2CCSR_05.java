package com.ibm.commerce.qa.aurora.tests;


	/*
	 *-----------------------------------------------------------------
	 * Licensed Materials - Property of IBM
	 *
	 * 
	 *
	 * WebSphere Commerce
	 *
	 * (C) Copyright IBM Corp. 2012
	 *
	 * US Government Users Restricted Rights - Use, duplication or
	 * disclosure restricted by GSA ADP Schedule Contract with
	 * IBM Corp.
	 *-----------------------------------------------------------------
	 */

	import java.util.logging.Logger;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.casl.keys.CaslKeysFactory;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.wte.util.WcConfigManager;


	/** 
	 * Test scenario to test various use cases associated with Dropdown menu context
	 * Refer to each test case for a detailed use case description
	 */
	@RunWith(GuiceTestRunner.class)
	@TestModules(AuroraModule.class)
	public class FSTOREB2CCSR_05 extends AbstractAuroraSingleSessionTests
	{
		/**
		 * The internal copyright field.
		 */
		public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

		//A Variable to retrieve data from the data file. 
		@DataProvider
		private final TestDataProvider dsm;

		
		/**
		 * Test Class object constructor.
		 * 
		 * @param log
		 * 			   logging object 
		 * @param config
		 * 			   object to work with getConfig().properties file
		 * @param session
		 * 			   factory to create browser sessions
		 * @param dataSetManager
		 * 			   object to work with data files
		 * @param p_caslFixtures 
		 */		
		@Inject
		public FSTOREB2CCSR_05(
				Logger log, 
				WcConfigManager config,
				WcWteTestRule wcWebTestRule,
				CaslFoundationTestRule caslTestRule,
				TestDataProvider dataSetManager,
				CaslFixturesFactory p_caslFixtures, CaslKeysFactory p_caslKeysFactory)
		{
			super(log, wcWebTestRule, caslTestRule);
			this.dsm = dataSetManager;
		}


		
		/**  Modify general information on the shopper's My Account page
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0501()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//Get LOGONID of a shopper that we are trying to locate
			String ShopperLogonId = dsm.getInputParameter("SHOPPER_LOGONID");

			
			MyAccountMainPage myAccountPage = frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
			.typeLogonId(ShopperLogonId).submitSearch().clickActionButtonByPosition(1).clickAccessCustomerAccount();
			
			myAccountPage.goToPersonalInformationPageForEditing()
				.typeFirstName(dsm.getInputParameter("UPDATED_FIRST_NAME"))
				.typeLastName(dsm.getInputParameter("UPDATED_LAST_NAME"))
				.typeEMail(dsm.getInputParameter("UPDATED_EMAIL"))
				.typeStreetAddress(dsm.getInputParameter("UPDATED_ADDRESS"))
				
				.update()
				
				.verifyFirstOrLastName(dsm.getInputParameter("UPDATED_FIRST_NAME"))
				.verifyFirstOrLastName(dsm.getInputParameter("UPDATED_LAST_NAME"))
				.verifyEmailAddress(dsm.getInputParameter("UPDATED_EMAIL"))
				.verifyUserAddress(dsm.getInputParameter("UPDATED_ADDRESS"));
			
			myAccountPage.goToPersonalInformationPageForEditing()
				.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
				.typeLastName(dsm.getInputParameter("LAST_NAME"))
				.typeEMail(dsm.getInputParameter("EMAIL"))
				.typeStreetAddress(dsm.getInputParameter("ADDRESS"))
				
				.update()
				
				.verifyFirstOrLastName(dsm.getInputParameter("FIRST_NAME"))
				.verifyFirstOrLastName(dsm.getInputParameter("LAST_NAME"))
				.verifyEmailAddress(dsm.getInputParameter("EMAIL"))
				.verifyUserAddress(dsm.getInputParameter("ADDRESS"));
			
		}
	}
