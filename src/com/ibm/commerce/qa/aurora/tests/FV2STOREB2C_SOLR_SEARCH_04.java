package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.IBMCopyright;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;





//Declares a new test scenario called FV2STOREB2C_SOLR_SEARCH_01 which contains one or more test case methods.
//All test scenario classes must extend StoreModelTestCase.

/**
 * Scenario: FV2STOREB2C_SOLR_SEARCH_04 Objective: To specify a search term in the search box
 * Pre-requisites: Tester knows the Store URL
 * (http://<hostname>/webapp/wcs/stores/servlet/en/auroraesite) 
 * Search based navigation is enabled
 *  Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_SOLR_SEARCH_04 extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = IBMCopyright.SHORT_COPYRIGHT;

	//A variable to hold the name of the data file where input parameters can be found.
	//$ANALYSIS-IGNORE
	protected final String dataFileName = "data/FV2STOREB2C_SOLR_SEARCH_04_Data.xml";

	@DataProvider
	private final TestDataProvider dsm;
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dsm
	 */
	@Inject
	public FV2STOREB2C_SOLR_SEARCH_04(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dsm)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dsm;
	}
	
	///////////////////////////////////////////
	
	/**
	 * Test case to test Shopper toggles different pages in the search results with sorting enabled
	 * Post conditions: Search results page displays the
	 * product match for the keyword search
	 * 
	 * @throws Exception
	 */
	@Category(Sanity.class)
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_0403() throws Exception {
		dsm.setDataFile(dataFileName);
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_0403", "testFV2STOREB2C_SOLR_SEARCH_0403_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Search the word sand
		SearchResultsPage searchResults = frontPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCHTERM_1"));

		//Sort the search results
		searchResults.getCatalogEntryListWidget().sortBy(dsm.getInputParameter("SORT_BY"));
		
		//verify the expected results are present
		searchResults.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("prod1"));
		//Go to the next search results page
		searchResults.getCatalogEntryListWidget().goToNextPage();
		//verify the expected results are present
		searchResults.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("prod2"));
		//Go to the previous search results page
		searchResults.getCatalogEntryListWidget().goToPreviousPage();
		//verify the expected results are present
		searchResults.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("prod1"));
	}

	
	
}