package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.casl.keys.CaslKeysFactory;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.CustomerServiceMainPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.MyRecuringOrdersPage;
import com.ibm.commerce.qa.aurora.page.NonRegisteredShippingBillingInfoPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderSummaryMultipleShipPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.RegisteredShippingBillingInfoPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.CustomerServiceFindCustomerWidget;
import com.ibm.commerce.qa.aurora.widget.CustomerServiceFindOrderWidget;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 * Scenario: FV2STOREB2C_07
 * Details: Test add to shopping cart flows.
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FSTOREB2CCSR_07 extends AbstractAuroraSingleSessionTests
{

    /**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	boolean doTearDown = true;
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	private final CaslFixturesFactory f_CaslFixtures;
	

	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_CaslFixtures 
	 */	@Inject
	public FSTOREB2CCSR_07(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_CaslFixtures, CaslKeysFactory p_caslKeysFactory)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		f_CaslFixtures = p_CaslFixtures;
		
	}
	 /** 
	  * Test Case to lock shopper's order on Find an order page
	  */
	 @Test
	 public void testFV2STOREB2C_0701()
	 {
		 doTearDown = false;
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
		String orderId = orders.getCurrentOrderId();
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget  signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store as a CSR admin.
		signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
			.signIn();
		
		CustomerServiceFindOrderWidget findOrderWidget = frontPage
				.getHeaderWidget()
				.goToCustomerService()
				.getFindOrderWidget();
		
		findOrderWidget.typeOrderNumber(orderId).submitSearch();
		
		//Lock order
		findOrderWidget.clickActionButton(orderId).clickLockOrder();
				
	 }
	 /** 
	  * Test Case to unlock shopper's order on Find an order page
	  */
	 @Test
	 public void testFV2STOREB2C_0702()
	 {
		 doTearDown = true;
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		String orderId = orders.getCurrentOrderId();
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget  signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store as a CSR admin.
		signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
			.signIn();
		
		CustomerServiceFindOrderWidget findOrderWidget = frontPage
				.getHeaderWidget()
				.goToCustomerService()
				.getFindOrderWidget();
		
		findOrderWidget.typeOrderNumber(orderId).submitSearch();
		
		//UnLock order
		findOrderWidget.clickActionButton(orderId).clickUnlockOrder()
		.verifyOrderIsUnLocked(orderId);
	 }
	 /** 
	  * Test Case to fill in shipping information for order after CSR has taken over the order
	  */
	 @Test
	 public void testFV2STOREB2C_0703()
	 {
		 doTearDown = true;
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
		String orderId = orders.getCurrentOrderId();
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
			
		//Log in to the store as a CSR admin.
		signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
			.signIn();
			
		CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
		findOrderWidget.typeOrderNumber(orderId).submitSearch();
		
		//Access Customer Account
		findOrderWidget.clickActionButton(orderId).clickAccessCustomerAccount();
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
							
		//Go to shipping and billing page
		ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
		
		//Confirm ship as complete option is selected.
		shippingAndBilling.verifyShipAsCompleteIsChecked();
				
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
				
		//Confirm the correct shipping address is selected
		shippingAndBilling.verifyShippingAddressSelected(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
				
		//Create a new shipping address
		RegisteredShippingBillingInfoPage shippingAndBillingInfo = shippingAndBilling.createShippingAddress();
		
		String RECIPIENT = dsm.getInputParameter("RECIPIENT")+System.currentTimeMillis();
				
		//Enter info and submit the new address creating request
		shippingAndBillingInfo.typeRecipient(RECIPIENT)
			.typeLastName(dsm.getInputParameter("LAST_NAME"))		 
			.typeAddress(dsm.getInputParameter("ADDRESS"))
			.typeCity(dsm.getInputParameter("CITY"))
			.selectCountry(dsm.getInputParameter("COUNTRY"))
			.selectStateOrProvince(dsm.getInputParameter("PROVINCE"))
			.typeZipCode(dsm.getInputParameter("ZIP_CODE"))
			.typePhone(dsm.getInputParameter("PHONE_NUMBER"))
			.typeEmail(dsm.getInputParameter("EMAIL"))
			.submitSuccessfulForm();
				
		shippingAndBilling.selectShippingAddress(RECIPIENT);
				
		//Wait for the new shipping address to display on the page
		shippingAndBilling.waitForShippingAddress(RECIPIENT);
				
		//Checks 'Ship As Complete' check box on on shipping billing page.
		shippingAndBilling.selectShipAsComplete();
	
	 }
	 /** 
	  * Test Case to fill in billing information for order after CSR has taken over the order
	  */
	 @Test
	 public void testFV2STOREB2C_0704()
	 {
		 doTearDown = true;
		 
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
		String orderId = orders.getCurrentOrderId();
			
		//Opens the Sign In page in browser.
		SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
				
		//Log in to the store as a CSR admin.
		signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
			.signIn();
				
		CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
		findOrderWidget.typeOrderNumber(orderId).submitSearch();
			
		//Access Customer Account
		findOrderWidget.clickActionButton(orderId).clickAccessCustomerAccount();
			
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
								
		//Go to shipping and billing page
		ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
						
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
		//Confirm the correct Billing address is selected
		shippingAndBilling.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
					
		//Create a new Billing address
		RegisteredShippingBillingInfoPage shippingAndBillingInfo = shippingAndBilling.createBillingAddress();
		
		String RECIPIENT = dsm.getInputParameter("RECIPIENT")+System.currentTimeMillis();
					
		//Enter info and submit the new address creating request
		shippingAndBillingInfo.typeRecipient(RECIPIENT)
			.typeLastName(dsm.getInputParameter("LAST_NAME"))		 
			.typeAddress(dsm.getInputParameter("ADDRESS"))
			.typeCity(dsm.getInputParameter("CITY"))
			.selectCountry(dsm.getInputParameter("COUNTRY"))
			.selectStateOrProvince(dsm.getInputParameter("PROVINCE"))
			.typeZipCode(dsm.getInputParameter("ZIP_CODE"))
			.typePhone(dsm.getInputParameter("PHONE_NUMBER"))
			.typeEmail(dsm.getInputParameter("EMAIL"))
			.submitSuccessfulForm();
					
			shippingAndBilling.selectBillingAddressByName(RECIPIENT);
					
			//Wait for the new shipping address to display on the page
			shippingAndBilling.waitForBillingAddress(RECIPIENT);
			

	 }
	 /** 
	  * Test Case to complete an order for a shopper after CSR has taken over the order
	  */
	 @Test
	 public void testFV2STOREB2C_0705()
	 {
		 doTearDown = true;
			
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
		String orderId = orders.getCurrentOrderId();
			
		//Opens the Sign In page in browser.
		SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
				
		//Log in to the store as a CSR admin.
		signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
			.signIn();
				
		CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
		findOrderWidget.typeOrderNumber(orderId).submitSearch();
			
		//Access Customer Account
		findOrderWidget.clickActionButton(orderId).clickAccessCustomerAccount();
			
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
								
		//Go to shipping and billing page
		ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
								
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
					
		//Confirm the correct Shipping Address is selected
		shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
			.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
		
		//Confirm the correct Billing address is selected
		shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
			.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
					
		//select Pay later as the payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
						
		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	
						
		//Confirm product name
		singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("PRODUCT_NAME"));
						
		//Confirm item quantity
		singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_QTY"));
						
		//Confirm item each price
		singleOrderSummary.verifyItemEachPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_EACH_TOTAL"));
						
		//Confirm total price of the item.
		singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_TOTAL"));
	
		//Verifies Billing Method on order summary page.
		singleOrderSummary.verifyBillingMethod(dsm.getInputParameter("PAY_METHOD"),dsm.getInputParameter("PAYMENT_NUMBER"));		
						
		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
						
		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();	
		
	
	 }
	 
	 /** 
	  * Test Case to remove items from shopping cart after CSR has taken over the order
	  */
	 @Test
	 public void testFV2STOREB2C_0706()
	 {
		 doTearDown = true;
			
			//Open the store in the browser
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			String orderId = orders.getCurrentOrderId();
			
			//Opens the Sign In page in browser.
			SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
				
			//Log in to the store as a CSR admin.
			signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
				.signIn();
				
			CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
			
			//Access Customer Account
			findOrderWidget.clickActionButton(orderId).clickAccessCustomerAccount();
			
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
								
			//Go to shipping and billing page
			shopCart.clickLockOrder()
				.verifyItemInShopCart(dsm.getInputParameter("PRODUCT_NAME"))
				.removeFromCart(dsm.getInputParameter("ITEM_NUMBER_IN_CART"))
				.verifyItemRemoved("The item was removed from the shopping cart");
		
		tearDown();
	 
	 }
	 /** 
	  * Test Case to remove all items from shopping cart after CSR has taken over the order
	  */
	 @Test
	 public void testFV2STOREB2C_0707()
	 {
		 doTearDown = true;
		 
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
		orders.addItem(dsm.getInputParameter("SKU_2"), dsm.getInputParameterAsNumber("QTY_2", Double.class));
		String orderId = orders.getCurrentOrderId();
				
		//Opens the Sign In page in browser.
		SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
						
		//Log in to the store as a CSR admin.
		signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
			.signIn();
		
		//Search for order 
		CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
		findOrderWidget.typeOrderNumber(orderId).submitSearch();
		
		//Access Customer Account
		findOrderWidget.clickActionButton(orderId).clickAccessCustomerAccount();
		
		//Lock Cart
		signIn.getHeaderWidget().openShopCartWidget().clickLockCartButton();
		
		//click on Shopping Cart link from the header
		signIn.getHeaderWidget().goToShoppingCartPage()
			.verifyItemInShopCart(dsm.getInputParameter("PRODUCT_NAME"))
			.verifyItemInShopCart(dsm.getInputParameter("PRODUCT_NAME2"))
			.removeAllItems()
			.verifyEmptyShoppingCartMessage("Your shopping cart is empty. If you see something you would like to add to your shopping cart when shopping, click Add to Cart.")			
			.clickUnlockOrder();
		
	 }
	 /** 
	  * Test Case to access an already-locked order and complete the order after CSR has taken over the order
	  */
	 @Test
	 public void testFV2STOREB2C_0708()
	 {
		doTearDown = true;
		
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
		String orderId = orders.getCurrentOrderId();
		
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
		//Opens the Sign In page in browser.
		SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
		
		try
		{					
		//Log in to the store as a CSR admin.
		signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
			.signIn();
		
		CustomerServiceMainPage CSRMainPage = signIn.getHeaderWidget().goToCustomerService();
		
		CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
 
		findOrderWidget.typeOrderNumber(orderId).submitSearch().clickActionButton(orderId).clickLockOrder();
		
		CSRMainPage.getHeaderWidget().goToCustomerService().getFindOrderWidget().typeOrderNumber(orderId)
			.submitSearch().clickActionButton(orderId).clickAccessCustomerAccount();
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
		
		//Go to shipping and billing page
		ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
									
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
		//Confirm the correct Shipping Address is selected
		shippingAndBilling.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
		//Confirm the correct Billing address is selected
		shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("SHOPPER_LOGONID")).verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
		//select Pay later as the payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
							
		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	
							
		//Confirm product name
		singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("PRODUCT_NAME"));
							
		//Confirm item quantity
		singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_QTY"));
							
		//Confirm item each price
		singleOrderSummary.verifyItemEachPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_EACH_TOTAL"));
							
		//Confirm total price of the item.
		singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_TOTAL"));
							
		//Confirm the total price of the order
		singleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL"));						
			
		//Verifies Billing Method on order summary page.
		singleOrderSummary.verifyBillingMethod(dsm.getInputParameter("PAY_METHOD"),dsm.getInputParameter("PAYMENT_NUMBER"));
			
		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
							
		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();	
		}
		
		catch(RuntimeException e) {
		
		//Continue browser session starting at the home page.
		HeaderWidget headerSection = getSession().continueToPage(getConfig().getStoreUrl(), HeaderWidget.class);
			
		//Remove all items from the shopping cart
		headerSection.goToShoppingCartPage().removeAllItems();
		HeaderWidget shopCartWidget = headerSection.openShopCartWidget();
		if(headerSection.openShopCartWidget().isCartLocked())
		{
			shopCartWidget.clickUnlockCartButton();
		}
			
		headerSection.openSignOutDropDownWidget().signOut();
		
		}
	 }
	 /** 
	  * Test Case to cancel a shopper's already-placed order after CSR has taken over the order
	  */
	 @Test
	 public void testFV2STOREB2C_0709()
	 {
		 
		doTearDown = true;
		
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//add product to cart and complete order
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
		String orderId = orders.getCurrentOrderId();
		orders.deletePaymentMethod();
		orders.addPayLaterPaymentMethod();
		orders.completeOrder();
				
		//Opens the Sign In page in browser.
		SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
							
		//Log in to the store as a CSR admin.
		signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
			.signIn();
							
		//Search for order 
		CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
		findOrderWidget.typeOrderNumber(orderId).submitSearch();
						
		//Access Customer Account
		findOrderWidget.clickActionButton(orderId).clickAccessCustomerAccount().goToOrderHistoryToViewAllOrders()
		.clickActionButton(orderId)
		.clickCancelOrderButton()
		.verifyCanceledOrder(orderId);
		
	 }	
	 /** 
	  * Test Case to reorder a previous order for a shopper after CSR has taken over the order
	  */
	 @Test
	 public void testFV2STOREB2C_0711()
	 {
		 doTearDown = true;
		 
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
								
		//Opens the Sign In page in browser.
		SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
							
		//Log in to the store as a CSR admin.
		signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
			.signIn();
		
		
		CustomerServiceFindCustomerWidget findCustomerWidget = frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget();
		findCustomerWidget.typeLogonId(dsm.getInputParameter("SHOPPER_LOGONID")).submitSearch();
		
		//Access Customer Account
		MyAccountMainPage myAccountPage = findCustomerWidget.clickActionButtonByPosition(1).clickAccessCustomerAccount();
		//myAccountPage.getHeaderWidget().openShopCartWidget().clickLockCartButton();
		
		ShopCartPage shopCart = myAccountPage.goToOrderHistoryToViewAllOrders()
				.clickActionButtonByPosition(1).clickReorderButton();
							
		//Go to shipping and billing page
		ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
									
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
		//Confirm the correct Shipping Address is selected
		shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
			.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
		//Confirm the correct Billing address is selected
		shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
			.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
		//select Pay later as the payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
							
		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	
							
		//Confirm product name
		singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("PRODUCT_NAME"));
							
		//Confirm item quantity
		singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_QTY"));
							
		//Confirm item each price
		singleOrderSummary.verifyItemEachPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_EACH_TOTAL"));
							
		//Confirm total price of the item.
		singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_TOTAL"));					
			
		//Verifies Billing Method on order summary page.
		singleOrderSummary.verifyBillingMethod(dsm.getInputParameter("PAY_METHOD"),dsm.getInputParameter("PAYMENT_NUMBER"));		
							
		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
							
		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();			
		
	 
	 }
	 /** 
	  * Test Case to complete a recurring order for a shopper after CSR has taken over the order
	  */
	 @Test
	 public void testFV2STOREB2C_0712()
	 {	
		 doTearDown = true;
		 
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
		String orderId = orders.getCurrentOrderId();
				
		//Opens the Sign In page in browser.
		SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
								
		//Log in to the store as a CSR admin.
		signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
			.signIn();
							
		//Search for order 
		CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
		findOrderWidget.typeOrderNumber(orderId).submitSearch();
				
		//Access Customer Account
		findOrderWidget.clickActionButton(orderId).clickAccessCustomerAccount();
	
		signIn.getHeaderWidget().openShopCartWidget().clickLockCartButton();
	
		//click on Shopping Cart link from the header
		//click on Schedule this order as a recurring order
		//Confirm schedule this order as a recurring order is checked
		ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage()
				.checkScheduleThisOrderCheckBox()
				.verifyScheduleThisOrderCheckBoxIsPresent();
		
		//Go to shipping and billing page
		ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
								
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
					
		//Confirm the correct Shipping Address is selected
		shippingAndBilling.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
		
		//Select order frequency
		shippingAndBilling.selectOrderFrequency(dsm.getInputParameter("ORDER_FREQUENCY"));
	
		//select start date
		String StartDate = new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime());
		shippingAndBilling.typeStartDate(StartDate);
		
		//Confirm the correct Billing address is selected
		shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS")).verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
					
		//select Pay later as the payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
						
		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	
						
		//Confirm product name
		singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("PRODUCT_NAME"));
						
		//Confirm item quantity
		singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_QTY"));
						
		//Confirm item each price
		singleOrderSummary.verifyItemEachPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_EACH_TOTAL"));
						
		//Confirm total price of the item.
		singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_TOTAL"));
						
		//Confirm the total price of the order
		//singleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL"));						
						
		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
						
		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful("Your recurring order has been scheduled.");	
		
		
	 
	 }
	 /** 
	  * Test Case to complete a subscription order for a shopper after CSR has taken over the order
	  */
	 @Test
	 public void testFV2STOREB2C_0713()
	 {
		 doTearDown = true;
		 
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
		String orderId = orders.getCurrentOrderId();
		
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
								
		//Opens the Sign In page in browser.
		SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
							
		//Log in to the store as a CSR admin.
		signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
			.signIn();
							
		//Search for order 
		CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
		findOrderWidget.typeOrderNumber(orderId).submitSearch();
						
		//Access Customer Account
		findOrderWidget.clickActionButton(orderId).clickAccessCustomerAccount();
		
		signIn.getHeaderWidget().openShopCartWidget().clickLockCartButton();
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
						
		//Go to shipping and billing page
		ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
								
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
					
		//Confirm the correct Shipping Address is selected
		shippingAndBilling.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
		
		//Confirm the correct Billing address is selected
		shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS")).verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
					
		//select Pay later as the payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
						
		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	
							
		//Confirm product name
		singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("PRODUCT_NAME"));
							
		//Confirm item quantity
		singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_QTY"));
							
		//Confirm item each price
		singleOrderSummary.verifyItemEachPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_EACH_TOTAL"));
						
		//Confirm total price of the item.
		singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_TOTAL"));
						
		//Confirm the total price of the order
		singleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL"));						
						
		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
						
		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();			 
	 }
	 
	 /** 
	  * Test case to Checkout with shipping instructions for single shipment when CSR has taken over an order.
	  */
		@Test
		public void testFV2STOREB2C_0714() 
		{
			doTearDown = true;
			
			//Open the store in the browser
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			String orderId = orders.getCurrentOrderId();
			
			//Opens the Sign In page in browser.
			SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
								
			//Log in to the store as a CSR admin.
			signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
				.signIn();
						
			//Search for order 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
							
			//Access Customer Account
			findOrderWidget.clickActionButton(orderId).clickAccessCustomerAccount();
			
			signIn.getHeaderWidget().openShopCartWidget().clickLockCartButton();
			
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
			
			//Verify Item in shop cart page
			shopCart.verifyOrderTotal(dsm.getInputParameter("TOTAL"));
			
			//Verify Item in shop cart page
			shopCart.verifyItemInShopCart(dsm.getInputParameter("PRODUCT_NAME"));
			
			//Go to Next Page
			ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
			
			//Confirm ship as complete option is selected.
			shippingAndBilling.verifyShipAsCompleteIsChecked();
			
			//Confirm the correct shipping address is selected
			shippingAndBilling.verifyShippingAddressSelected(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Checks 'Ship As Complete' check box on on shipping billing page.
			shippingAndBilling.selectShipAsComplete();
			
			//Select the new shipping address
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("RECIPIENT"));
			
			//Checks 'Add Shipping Instruction' check box and type shipping instructions on on shipping billing page.
			shippingAndBilling.addShippingInstructions(dsm.getInputParameter("SHIPPING_INSTUCTIONS"));
			
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
			
			//Confirm ship as complete option is selected.
			shippingAndBilling.verifyShipAsCompleteIsChecked();
			
			//Verify shop cart is not empty
			shippingAndBilling.verifyShoppingCartIsNOTEmpty();
			
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	
			
			//Confirm product name
			singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("PRODUCT_NAME"));
			
			//Confirm item quantity
			singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_QTY"));
			
			//Confirm item each price
			singleOrderSummary.verifyItemEachPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_EACH_TOTAL"));
			
			//Confirm total price of the item.
			singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_TOTAL"));
			
			//Confirm shipping address
			singleOrderSummary.verifyShippingAddress(dsm.getInputParameter("RECIPIENT"));
			
			//Confirm the total price of the order
			singleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL"));	
			
			//Verifies Billing Method on order summary page.
			singleOrderSummary.verifyBillingMethod(dsm.getInputParameter("PAY_METHOD"),dsm.getInputParameter("PAYMENT_NUMBER"));
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
			
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();
		}
		
		/** 
		  * Test case to Checkout with shipping instructions for multiple shipment when CSR has taken over an order.
		  */
		@Test
		public void testFV2STOREB2C_0715() 
		{
			doTearDown = true;
			
			//Open the store in the browser
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			String orderId = orders.getCurrentOrderId();
			
			//Opens the Sign In page in browser.
			SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
									
			//Log in to the store as a CSR admin.
			signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
				.signIn();
						
			//Search for order 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
							
			//Access Customer Account
			findOrderWidget.clickActionButton(orderId).clickAccessCustomerAccount();
			
			//Lock Order
			signIn.getHeaderWidget().openShopCartWidget().clickLockCartButton();
			
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
		
			//Confirm the price on minishopping cart
			shopCart.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
			
			//Click on check out button
			ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
			
			//Confirm the correct shipping address is selected
			shippingAndBilling.verifyShippingAddressSelected(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Create a new shipping address
			RegisteredShippingBillingInfoPage shippingAndBillingInfo = shippingAndBilling.createShippingAddress();
			
			String RECIPIENT1 = dsm.getInputParameter("RECIPIENT1")+System.currentTimeMillis();
			
			//Enter info and submit the new address creating request
			shippingAndBillingInfo.typeRecipient(RECIPIENT1)
			 .typeLastName(dsm.getInputParameter("LAST_NAME2"))		 
			 .typeAddress(dsm.getInputParameter("ADDRESS"))
			 .typeCity(dsm.getInputParameter("CITY"))
			 .selectCountry(dsm.getInputParameter("COUNTRY"))
			 .selectStateOrProvince(dsm.getInputParameter("PROVINCE"))
			 .typeZipCode(dsm.getInputParameter("ZIP_CODE"))
			 .typePhone(dsm.getInputParameter("PHONE_NUMBER"))
			 .typeEmail(dsm.getInputParameter("EMAIL"))
			 .applyToBothShippingBilling()
			 .submitSuccessfulForm();
			
			shippingAndBilling.selectShippingAddress(RECIPIENT1);
			
			//Wait for the new shipping address to display on the page
			shippingAndBilling.waitForShippingAddress(RECIPIENT1);
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
			
			//Clicks 'Multiple Shipment' button on Shipping and Billing Page.
			shippingAndBilling.chooseMultipleShipping();
			
			//Selects Shipping Address from drop down list on shipping billing page for the first item in shopping cart.
			shippingAndBilling.getMultipleshipment().selectShippingAddressByPosition(RECIPIENT1, dsm.getInputParameter("ITEM_POSITION1"));
			
			//Selects Shipping Address from drop down list on shipping billing page for the second item in shopping cart.
			shippingAndBilling.getMultipleshipment().selectShippingAddressByPosition(dsm.getInputParameter("RECIPIENT2"), dsm.getInputParameter("ITEM_POSITION2"));
			
			//Unchecks 'Ship As Complete' check box on shipping billing page.
			shippingAndBilling.unSelectShipAsComplete();
			
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
			
			//click next button
			OrderSummaryMultipleShipPage multipleOrderSummary = shippingAndBilling.multipleShipNext();
			
			//Confirm the first item's product name
			multipleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION1"), dsm.getInputParameter("PRODUCT_NAME"));
			
			//Confirm item quantity
			multipleOrderSummary.verifyItemQTY(dsm.getInputParameter("ITEM_POSITION1"), dsm.getInputParameter("ITEM_QTY"));
			
			//Confirm item each price
			multipleOrderSummary.verifyItemEachPrice(dsm.getInputParameter("ITEM_POSITION1"),dsm.getInputParameter("ITEM_EACH_TOTAL"));
			
			//Confirm total price of the item.
			multipleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION1"),dsm.getInputParameter("ITEM_TOTAL"));
			
			//Confirm shipping address
			multipleOrderSummary.verifyShippingAddress(RECIPIENT1, dsm.getInputParameter("ITEM_POSITION1"));
			
			//Confirm shipping address
			multipleOrderSummary.verifyShippingAddress(dsm.getInputParameter("RECIPIENT2"), dsm.getInputParameter("ITEM_POSITION2"));
			
			//Confirm the total price of the order
			multipleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL"));	
			
			//Verifies Billing Method on order summary page.
			multipleOrderSummary.verifyBillingMethod(dsm.getInputParameter("PAY_METHOD"));
			
			//click Order button
			OrderConfirmationPage orderConfirmation = multipleOrderSummary.completeOrder();
			
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();	
		}
		
		
		/** 
		  * Test case to Checkout with single payment instructions when CSR has taken over an order.
		  */
		@Test
		public void testFV2STOREB2C_0716() 
		{
			doTearDown = true;
			
			//Open the store in the browser
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			String orderId = orders.getCurrentOrderId();
			
			//Opens the Sign In page in browser.
			SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
									
			//Log in to the store as a CSR admin.
			signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
				.signIn();
			
			//Search for order 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
							
			//Access Customer Account
			findOrderWidget.clickActionButton(orderId).clickAccessCustomerAccount();
		
			//Lock Order
			signIn.getHeaderWidget().openShopCartWidget().clickLockCartButton();
			
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
		
			//click on the checkout button
			ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
			
			//Confirm ship as complete option is selected.
			shippingAndBilling.verifyShipAsCompleteIsChecked();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
			
			//Confirm the correct shipping address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddressSelected(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//select Visa as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
			
			//Enter credit card number
			shippingAndBilling.typeCreditCardNumber(dsm.getInputParameter("CARD_NUM"));
			
			//Select a valid expiry Year
			shippingAndBilling.selectExpirationYear(dsm.getInputParameter("CARD_YEAR"));
			
			//Select a valid expiry Month
			shippingAndBilling.selectExpirationMonth(dsm.getInputParameter("CARD_MONTH"));
			
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	
			
			//Confirm payment page on order summary page
			singleOrderSummary.verifyBillingMethod(dsm.getInputParameter("PAY_METHOD"),dsm.getInputParameter("PAYMENT_NUMBER"));
			
			//Confirm product name
			singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("PRODUCT_NAME"));
			
			//Confirm item quantity
			singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_QTY"));
			
			//Confirm total price of the item.
			singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_TOTAL"));
			
			//Confirm the total price of the order
			singleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL"));						
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
			
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();
			
		}
	
		/** 
		  * Test case to Checkout with multiple payment instructions when CSR has taken over an order.
		  */
		@Test
		public void testFV2STOREB2C_0717() 
		{
			doTearDown = true;
			
			//Open the store in the browser
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
						
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			String orderId = orders.getCurrentOrderId();
			
			//Opens the Sign In page in browser.
			SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
			//Log in to the store as a CSR admin.
			signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
				.signIn();
						
			//Search for order 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
							
			//Access Customer Account
			findOrderWidget.clickActionButton(orderId).clickAccessCustomerAccount();
		
			//Lock Order
			signIn.getHeaderWidget().openShopCartWidget().clickLockCartButton();
			
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
		
			//click on the checkout button
			ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
			
			//Confirm ship as complete option is selected.
			shippingAndBilling.verifyShipAsCompleteIsChecked();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
			
			//Confirm the correct shipping address is selected
			shippingAndBilling.verifyShippingAddressSelected(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Click on edit link on billing section
			RegisteredShippingBillingInfoPage shippingAndBillingInfo =	shippingAndBilling.createShippingAddress();
			
			String RECIPIENT_1 = dsm.getInputParameter("RECIPIENT_1")+System.currentTimeMillis();
			
			//Enter info and submit the new address creating request
			shippingAndBillingInfo.typeRecipient(RECIPIENT_1)
			 .typeLastName(dsm.getInputParameter("LAST_NAME"))		 
			 .typeAddress(dsm.getInputParameter("STREET_ADDRESS"))
			 .typeCity(dsm.getInputParameter("CITY"))
			 .selectCountry(dsm.getInputParameter("COUNTRY"))
			 .selectStateOrProvince(dsm.getInputParameter("PROVINCE"))
			 .typeZipCode(dsm.getInputParameter("ZIPCODE"))
			 .typePhone(dsm.getInputParameter("PHONE_NUMBER"))
			 .typeEmail(dsm.getInputParameter("EMAIL"))
			 .applyToBothShippingBilling()
			 .submitSuccessfulForm();
			
			shippingAndBilling.selectShippingAddress(RECIPIENT_1);
			//Wait for billing address to display
			shippingAndBilling.waitForShippingAddress(RECIPIENT_1);
					
			//Click on edit link on billing section
			RegisteredShippingBillingInfoPage billingInfo = shippingAndBilling.createBillingAddress();
			
			//Confirm ship and bill info page is completely loaded
			billingInfo.verifyIsNotOnShipBillPage();	
			
			String RECIPIENT_2 = dsm.getInputParameter("RECIPIENT_2")+System.currentTimeMillis();
			
			//Enter info and submit the new address creating request
			billingInfo
			 .typeLastName(dsm.getInputParameter("LAST_NAME"))
			 .typeRecipient(RECIPIENT_2)
			 .typeAddress(dsm.getInputParameter("STREET_ADDRESS"))
			 .typeCity(dsm.getInputParameter("CITY"))
			 .selectCountry(dsm.getInputParameter("COUNTRY"))
			 .selectStateOrProvince(dsm.getInputParameter("PROVINCE"))
			 .typeZipCode(dsm.getInputParameter("ZIPCODE"))
			 .typePhone(dsm.getInputParameter("PHONE_NUMBER"))
			 .typeEmail(dsm.getInputParameter("EMAIL"))
			 .applyToBothShippingBilling()
			 .submitSuccessfulForm();
			
			//Wait for billing address to display
			shippingAndBilling.waitForBillingAddress(RECIPIENT_2);

			//Select number of payment
			shippingAndBilling.selectNoOfPaymentMethods(dsm.getInputParameter("NUMBER_OF_PAYMENTS_LABEL"));
			
			//Select billing address for first number of payment
			shippingAndBilling.getMultiplepayment().selectBillingAddressByPosition(RECIPIENT_1, dsm.getInputParameter("ITEM_POSITION1"));
			
			//update new amount
			shippingAndBilling.getMultiplepayment().typeAmountByPaymentsPosition(dsm.getInputParameter("ITEM1_AMOUNT"), dsm.getInputParameter("ITEM_POSITION1"));		
			
			//Select billing method
			shippingAndBilling.getMultiplepayment().selectBillingMethodByPaymentsPosition(dsm.getInputParameter("PAY_METHOD"), dsm.getInputParameter("ITEM_POSITION1"));
					
			//Select billing address for first number of payment
			shippingAndBilling.getMultiplepayment().selectBillingAddressByPosition(RECIPIENT_1, dsm.getInputParameter("ITEM_POSITION2"));		

			//Select billing method
			shippingAndBilling.getMultiplepayment().selectBillingMethodByPaymentsPosition(dsm.getInputParameter("PAY_METHOD"), dsm.getInputParameter("ITEM_POSITION2"));				
			
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	
			
			//Confirm payment page on order summary page
			singleOrderSummary.verifyBillingMethod(dsm.getInputParameter("PAY_METHOD"),dsm.getInputParameter("PAYMENT_NUMBER"));
			
			//Confirm billing address for first payment		
			singleOrderSummary.verifyBillingAddressByPaymentNumber(RECIPIENT_1, dsm.getInputParameter("ITEM_POSITION1"));
			
			//Confirm billing address for second payment	
			singleOrderSummary.verifyBillingAddressByPaymentNumber(RECIPIENT_1, dsm.getInputParameter("ITEM_POSITION2"));
			
			//Confirm product name
			singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION1"), dsm.getInputParameter("PRODUCT_NAME"));
			
			//Confirm item quantity
			singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION1"), dsm.getInputParameter("ITEM_QTY"));
			
			//Confirm total price of the item.
			singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION1"),dsm.getInputParameter("ITEM_TOTAL"));
			
			//Confirm the total price of the order
			singleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL_ORDER_SUMMARY"));						
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
			
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();
		}
		
		/** 
		  * Test case to place an order on behalf of guest shopper
		  */
		@Test
		public void testFV2STOREB2C_0718() 
		{
			//Open the store in the browser
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
										
			//Opens the Sign In page in browser.
			SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
									
			//Log in to the store as a CSR admin.
			signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
				.signIn();
						
			frontPage.getHeaderWidget().goToCustomerService().getSidebarWidget().gotoShopAsGuestPage();
			
			//Select a category and sub category to view the list of products within
			CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CATEGORY_NAME"), 
					 dsm.getInputParameter("SUB_CATEGORY_NAME") , dsm.getInputParameter("CATEGORY_ITEM_NAME"));

			//Select a product from the list of visible products under the chosen sub-category
			ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().
					goToProductPageByImagePosition(Integer.parseInt(dsm.getInputParameter("PRODUCT_NUMBER")));
		
			//Select swatch values
			productDisplayPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_ATTRIBUTE_1"))
			
			//Select swatch values
			.selectAttributeSwatch(dsm.getInputParameter("SWATCH_ATTRIBUTE_2"));
			
			//Add the item to the cart
			productDisplayPage.addToCart();
			
			//Opens the shopping cart page
			ShopCartPage shopCartPage = productDisplayPage.getHeaderWidget().goToShoppingCartPage();
			
			//Clicks the guest user checkout button
			NonRegisteredShippingBillingInfoPage nonRegisteredShippingBillingPage = shopCartPage.continueAsGuestUser();
			
			//Enter recipient information
			nonRegisteredShippingBillingPage.typeBillRecipient(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
			
			//Enter last name
			.typeBillLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
			
			//Enter billing address
			.typeBillAddress(dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
			
			//Enter billing city
			.typeBillCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
			
			//Enter billing country
			.selectBillCountry(dsm.getInputParameter("ADDRESS_BOOK_COUNTRY"))
			
			//Enter billing province
			.selectBillState(dsm.getInputParameter("ADDRESS_BOOK_STATE"))
			
			//Enter billing phone number
			.typeBillPhone(dsm.getInputParameter("ADDRESS_PHONE_NUMBER"))
			
			//Enter billing zip code
			.typeBillZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
			
			//Enter e-mail address
			.typeBillEmail(dsm.getInputParameter("ADDRESS_BOOK_EMAIL"))
			
			//Click the check box to make the shipping information the same as the billing information
			.applyToBothShippingBilling();
			
			//Opens the shipping and billing method page
			ShippingAndBillingPage shippingAndBillingPage = nonRegisteredShippingBillingPage.submitSuccessfulForm();
			
			//Selects a user-defined billing method
			shippingAndBillingPage.selectPaymentMethod(dsm.getInputParameter("BILLLING_METHOD"));
			
			//Opens the order summary page
			OrderSummarySingleShipPage orderSummaryPage = shippingAndBillingPage.singleShipNext();
			
			//Submits the order
			OrderConfirmationPage orderConfirmationPage = orderSummaryPage.completeOrder();

			//Confirm that the order was successfully submitted
			orderConfirmationPage.verifyOrderSuccessful(dsm.getInputParameter("ORDER_SUCCESSFUL_MESSAGE"));
		}
		 /** 
		  * Test Case to cancel a shopper's recurring/subscription order after CSR has taken over the order
		  */
		 @Test
		 public void testFV2STOREB2C_0719()
		 {
			 doTearDown = true;
			//Open the store in the browser
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			orders.deletePaymentMethod();
			orders.addPayLaterPaymentMethod();
			String orderId = orders.getCurrentOrderId();
			orders.completeRecurringOrder(dsm.getInputParameterAsNumber("FREQUENCY", Double.class), dsm.getInputParameter("FREQ_UNITS"));			
		
			
			
			//Opens the Sign In page in browser.
			SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
								
			//Log in to the store as a CSR admin.
			signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
				.signIn();
						
			CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId)
				.submitSearch();
				
			//Access Customer Account
			MyAccountMainPage myAccountPage = findOrderWidget.clickActionButtonByPosition(1).clickAccessCustomerAccount();
			
			//Cancel Recurring Order by position
			MyRecuringOrdersPage recurringOrderPage =  myAccountPage.goToRecurringOrdersPage();
			
			recurringOrderPage.cancelRecurringOrderByPosition(recurringOrderPage.getPositionFromOrderNumber(orderId))
			.cancelRecurringOrderConfirmationYes()
			.verifyCanceledOrder(orderId);
		
		 }
		 
		 /** 
		  * Test case to lock/unlock order in mini shopping cart; 
		  */
		@Test
		public void testFV2STOREB2C_0720() 
		{
			
			//Open the store in the browser
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			String orderId = orders.getCurrentOrderId();
			
			//Opens the Sign In page in browser.
			SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
									
			//Log in to the store as a CSR admin.
			signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
				.signIn();
						
			//Search for order 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
							
			//Access Customer Account
			findOrderWidget.clickActionButton(orderId).clickAccessCustomerAccount();
			
			//lock Order
			signIn.getHeaderWidget().openShopCartWidget().clickLockCartButton().closeMessageArea();
			
			//Search for order 
			frontPage.getHeaderWidget().gotoCustomerServiceWithTerminateModal().clickYes().getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
							
			//Access Customer Account
			findOrderWidget.clickActionButton(orderId).clickAccessCustomerAccount();
			
			//Unlock Order
			signIn.getHeaderWidget().openShopCartWidget().clickUnlockCartButton();
		
		}
		
		/**
		 * Tear down ran after every test case
		 */
		@After
		public void tearDown(){
			try
			{
			if (doTearDown) 
			{
				//Continue browser session starting at the home page.
				HeaderWidget headerSection = getSession().continueToPage(getConfig().getStoreUrl(), HeaderWidget.class);

				//Remove all items from the shopping cart
				headerSection.goToShoppingCartPage().clickLockOrder().removeAllItems().clickUnlockOrder()
				.getHeaderWidget().openSignOutDropDownWidget().signOut();
			
			
			}
			}
			catch(RuntimeException e)
			{
				getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
			}

		}
		

}


