package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2013
 * 
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.url.DeltaUpdates;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
/**			
* Scenario: FV2STOREB2C_50
* Details: Setup product attribute swatch and view them on product details page
*
*/
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_50 extends AbstractAuroraSingleSessionTests
{
	@DataProvider
	private final TestDataProvider dsm;
	
	private CMC cmc;
	
	/** Used to verify if a product is present **/
	public static final String CAT_PRODUCT_IMAGE_LINK = "CatalogEntryProdImg_";
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dataSetManager
	 * @param cmc
	 * @param deltaUpdate 
	 */
	@Inject
	public FV2STOREB2C_50(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CMC cmc,
			DeltaUpdates deltaUpdate)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		this.cmc = cmc;
		this.deltaUpdate = deltaUpdate;
	}
	
	/**A variable to hold the catalog entry identifier created**/
	private String productId;
	
	/**A variable to hold the master catalog**/
	private String masterCatalogId;

	/**A variable to hold delta update**/
	private DeltaUpdates deltaUpdate;	
	
	private AuroraFrontPage 	auroraFront;
	private SearchResultsPage 	searchResults;
	private ProductDisplayPage 	productDisplay;

	private boolean 			test5005 = false;
	
	
	/**
	 * Verify swatch images display on product display page which has swatch attributes.
	 * @throws Exception
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_5002() throws Exception
	{
		//Open the store
		auroraFront = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Search for an item
		searchResults = auroraFront.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH"));
		
		//Click a Product and go to product display page
		productDisplay = searchResults.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT"));
	    
		//Click Swatch attribute of product
	    productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR"));
	}	
	/**
	 * Verify after selecting the first swatch attribute, the second swatch attribute 
	 * will be grayed out when the two attributes do not resolve to a SKU.
	 * @throws Exception
	 *
	 * NOTE: The fix for this testcase is included in APAR JR43587, 
	 * please apply the fix before running the test.
	 * 
	 * */
	public void testFV2STOREB2C_5005() throws Exception
	{
		test5005 = true;
		
		dsm.setDataLocation("testFV2STOREB2C_5005", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore(dsm.getInputParameter("storename"));
		cmc.selectCatalog();
	
		//Delete Black 2XL SKU
		dsm.setDataLocation("testFV2STOREB2C_5005", "deleteSKU");
		String skuId = cmc.getCatalogEntryId(dsm.getInputParameter("partNumber"));
		cmc.deleteCatalogEntry(skuId);
		
		masterCatalogId = cmc.selectCatalog();
		//Wait until delta update completes before timeout
		deltaUpdate.waitForDeltaWithinApproximateTimeout(masterCatalogId, 600);
	
		//Confrim SKU items no longer able to be selected 
		dsm.setDataLocation("testFV2STOREB2C_5005", "testFV2STOREB2C_5005");	
		
		auroraFront = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Search for an item
		searchResults = auroraFront.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH"));
		
		//Click a Product and go to product display page
		productDisplay = searchResults.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PROD_NAME"));
	    
		//Click Swatch attribute of product
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR"));
		
		//Click Swatch attribute of that was deleted
		try
		{	productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));	}
		catch(RuntimeException e)
		{	return;	}

		throw new Exception("SKU is still able to be selected");
		
	}	
	
	
	/**
	 * Verify swatch images display on category display page in list view.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_5007() throws Exception
	{
		//Open the store
		auroraFront = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Search for an item
		searchResults = auroraFront.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH"));
		
		//Switch to list view
		searchResults.getCatalogEntryListWidget().selectDetailedView();
		
		//Click a Product and go to product display page
		searchResults.getCatalogEntryListWidget().selectAttributeSwatchByProdIdentifier(dsm.getInputParameter("SWATCH_COLOR"), dsm.getInputParameter("PRODUCT"));
	}
	
	
	/**
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		try
		{
		if(test5005)
		{	dsm.setDataLocation("tearDown", "CMClogon");
			cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
			//select store and catalog
			cmc.setLocale(dsm.getInputParameter("locale"));
			cmc.selectStore(dsm.getInputParameter("storename"));
			cmc.selectCatalog();
			String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
			
			dsm.setDataLocation("tearDown", "tearDown");
			productId = cmc.getCatalogEntryId(dsm.getInputParameter("ProdPartNum"));
	
			//create an SKU for the product
			dsm.setDataLocation("tearDown", "createSKU");
			String skuCatEntryId = cmc.createSKU(productId,dsm.getInputParameter("partNumber"),dsm.getInputParameter("skuName"),dsm.getInputParameter("published"));
	
			cmc.createSKUDefiningAttrDictAttributeValue(skuCatEntryId, productId, dsm.getInputParameter("attributeValColorSeq"), dsm.getInputParameter("attributeValColor"));
			cmc.createSKUDefiningAttrDictAttributeValue(skuCatEntryId, productId, dsm.getInputParameter("attributeValSizeSeq"), dsm.getInputParameter("attributeValSize"));
			
			dsm.setDataLocation("tearDown", "updateSKU");
			cmc.updateProduct(skuCatEntryId);
			dsm.setDataLocation("tearDown", "updateSKUImage");
			cmc.updateProduct(skuCatEntryId);
			
			//set first offer price and list price
			dsm.setDataLocation("tearDown", "offerPrice");
			cmc.createOfferPriceForCatalogEntry(skuCatEntryId);
			cmc.createListPriceForCatalogEntry(skuCatEntryId);
			
			masterCatalogId = cmc.selectCatalog();
			//Wait until delta update completes before timeout
			deltaUpdate.beforeWaitForDelta(masterCatalogId);
			getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
			deltaUpdate.waitForDelta(masterCatalogId, 60);
			
			cmc.logoff();	
		}
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}
	}
}