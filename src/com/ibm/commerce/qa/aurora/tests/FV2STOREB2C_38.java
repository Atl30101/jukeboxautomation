package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.url.DeltaUpdates;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 *  Scenario: FV2STOREB2C_38
 * Details: Product attachments
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_38 extends AbstractAuroraSingleSessionTests
{	
	/**A variable to hold CMC**/
	private CMC cmc;
	
	@DataProvider
	private final TestDataProvider dsm;
	
	/**A variable to hold delta update**/
	private DeltaUpdates deltaUpdate;

	/**A variable to hold the catalog entry created**/
	private String catentryId;
	
	/**A variable to hold the attachment identifier created**/
	private String attachmentId;
	
	/**A variable to hold on the attachment reference identifier created**/
	private String attachmentRefId;
		
	/**A variable to hold the master catalog**/
	private String masterCatalogId;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * @param cmc
	 * @param deltaUpdate
	 */
	@Inject
	public FV2STOREB2C_38(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CMC cmc,
			DeltaUpdates deltaUpdate)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		this.cmc = cmc;
		this.deltaUpdate = deltaUpdate;
	}
	
	
	/**
	 * A test to login to CMC and create an attachment for product.
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_3801() throws Exception
	{			

		//login to CMC
		dsm.setDataLocation("testFV2STOREB2C_3801", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
							
		//select the locale.
		cmc.setLocale(dsm.getInputParameter("locale"));		
		cmc.selectStore();
		masterCatalogId = cmc.selectCatalog();
		
				
		//create a new product		
		Date dateNow = new Date();
		SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyyMMddhhmmss");
		StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format( dateNow ) );
		String uniqueID = nowYYYYMMDD.toString();
			    
		dsm.setDataLocation("testFV2STOREB2C_3801", "createProduct");
		String partNumber = dsm.getInputParameter("partNumber") + uniqueID;
		catentryId = cmc.createProduct(dsm.getInputParameter("parentCategory"),partNumber,dsm.getInputParameter("productname"),dsm.getInputParameter("published"));
				
		dsm.setDataLocation("testFV2STOREB2C_3801", "createAttachment");
		attachmentId = cmc.createAttachment(dsm.getInputParameter("identifier")+uniqueID, dsm.getInputParameter("PATH"),catentryId);		
		attachmentRefId = cmc.createCatalogEntryAttachmentReference(dsm.getInputParameter("usage"),dsm.getInputParameter("sequence"), catentryId, attachmentId);						
				
		//Log Off CMC
		cmc.logoff();
		
		//login to CMC
		dsm.setDataBlock("CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
					
		//select the locale.
		cmc.setLocale(dsm.getInputParameter("locale"));		
		cmc.selectStore();
		masterCatalogId = cmc.selectCatalog();
		String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
		
		//Wait until delta update completes before timeout
		deltaUpdate.beforeWaitForDelta(masterCatalogId);
		getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
		deltaUpdate.waitForDelta(masterCatalogId, 60);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_3801","testFV2STOREB2C_3801");	
		
		//Open store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Go to Sign In Page
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Sign in Store
		MyAccountMainPage myAccount = signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
		.signIn().closeSignOutDropDownWidget().goToMyAccount();

		//Click the Top category in menu
		//department = myAccount.getHeaderWidget().goToCategoryPageFromDeptSubMenu(dsm.getInputParameter("TOP_CAT"), dsm.getInputParameter("SUB_CAT"));
		CategoryPage subCat =  myAccount.getFooterWidget().goToSiteMapPage().goToCategoryPageByName(CategoryPage.class, dsm.getInputParameter("SUB_CAT"));
		
		//click on product image.
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPagebyIdentifier(catentryId);
		
		//verify that attachment is displayed.
		productDisplay.verifyAttachment();		
		
		//verify that attachment is displayed.
		productDisplay.verifyAttachmentNameIsPresent(dsm.getInputParameter("AttachmentName1"));
	}

	
	/**
	 * Perform teardown operations after the test is done, whether it is successful or not.
	 * @throws Exception
	 */
	@After
	public void testScenarioTearDown() throws Exception
	{
		try
		{
		getLog().info("testScenarioTearDown running");
		
		//login to CMC
		dsm.setDataLocation("testFV2STOREB2C_3801", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore();
		cmc.selectCatalog();
		
		//delete new attachment reference with product created.
		dsm.setDataLocation("testFV2STOREB2C_3801", "createProduct");
		cmc.deleteCatalogEntryAttachmentReference(attachmentId, attachmentRefId, catentryId);
		
		//delete the new product created 
		cmc.deleteCatalogEntry(catentryId);
		
		//delete the attachment created.
		cmc.deleteAttachment(attachmentId);
		
		//log off from CMC
		cmc.logoff();
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}
	}
	
}


