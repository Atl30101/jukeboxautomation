package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.casl.keys.CaslKeysFactory;
import com.ibm.commerce.casl.keys.ToolingKeys;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CustomerServiceOrderSummaryPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderDetailPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.CustomerServiceFindCustomerWidget;
import com.ibm.commerce.qa.aurora.widget.CustomerServiceFindOrderWidget;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 * Scenario: FV2STOREB2C_13
 * Details: Test add to shopping cart flows.
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FSTOREB2CCSR_13 extends AbstractAuroraSingleSessionTests
{

    /**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	private boolean doTearDown = true;
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	private final CaslFixturesFactory f_CaslFixtures;
	private CaslKeysFactory f_caslKeysFactory;
	

	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_CaslFixtures 
	 */	@Inject
	public FSTOREB2CCSR_13(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_CaslFixtures, CaslKeysFactory p_caslKeysFactory)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		f_caslKeysFactory = p_caslKeysFactory;
		f_CaslFixtures = p_CaslFixtures;
		
	}
	 /** 
	  * Customer Service Supervisor can search for shoppers on storefront
	  */
	 @Test
	 public void testFSTOREB2CCSR_1307()
	 {
		 doTearDown = false;
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
				
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
			
		//Log in to the store as a CSS admin.
		signIn.typeUsername(dsm.getInputParameter("CSS_LOGONID")).typePassword(dsm.getInputParameter("CSS_PASSWORD"))
			.signIn();
		
		CustomerServiceFindCustomerWidget findCustomerWidget = frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget();
		findCustomerWidget.typeLogonId(dsm.getInputParameter("LOGONID")).submitSearch();
			
		//Query to return unique user id (USER_ID)
		ToolingKeys p = f_caslKeysFactory.createToolingKeys(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"));
		final String USER_ID = p.findPersonIdByLogonId(dsm.getInputParameter("SHOPPER_LOGONID"));
			
		findCustomerWidget.verifyShopperIsDisplayedInSearchResult(USER_ID);
	 }
	 /** 
	  * Customer Service Supervisor can search for orders on storefront
	  */
	 @Test
	 public void testFSTOREB2CCSR_1308()
	 {
		 doTearDown = false;
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//add product to cart and complete order
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
		String orderId = orders.getCurrentOrderId();
		orders.deletePaymentMethod();
		orders.addPayLaterPaymentMethod();
		orders.completeOrder();			
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
				
		//Log in to the store as a CSS admin.
		signIn.typeUsername(dsm.getInputParameter("CSS_LOGONID")).typePassword(dsm.getInputParameter("CSS_PASSWORD"))
			.signIn();
				
		CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
		findOrderWidget.typeOrderNumber(orderId)
			.submitSearch();
		
		findOrderWidget.verifyOrderSearchResultIsDisplayed();
		
								
	 }
	 /** 
	  *  Customer Service Supervisor can access shopper's account
	  */
	 @Test
	 public void testFSTOREB2CCSR_1309()
	 {
		 doTearDown = false;
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
				
		//Opens the Sign In page in browser.
		SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
				
		//Log in to the store as a CSS admin.
		signIn.typeUsername(dsm.getInputParameter("CSS_LOGONID")).typePassword(dsm.getInputParameter("CSS_PASSWORD"))
			.signIn();
				
		CustomerServiceFindCustomerWidget findCustomerWidget = frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget();
		findCustomerWidget.typeLogonId(dsm.getInputParameter("SHOPPER_LOGONID")).submitSearch();
			
		//Query to return unique user id (USER_ID)
		ToolingKeys p = f_caslKeysFactory.createToolingKeys(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"));
		final String USER_ID = p.findPersonIdByLogonId(dsm.getInputParameter("SHOPPER_LOGONID"));
			
		findCustomerWidget.AccessCustomerAccount(USER_ID);
						
	 }
	 /** 
	  *  Customer Service Supervisor can lock/unlock a shopper's order
	  */
	 @Test
	 public void testFSTOREB2CCSR_1310()
	 {
		 doTearDown = false;
		 
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
		orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
		String orderId = orders.getCurrentOrderId();
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget  signIn = frontPage.getHeaderWidget().signIn();
			
		//Log in to the store as a CSS admin.
		signIn.typeUsername(dsm.getInputParameter("CSS_LOGONID")).typePassword(dsm.getInputParameter("CSS_PASSWORD"))
			.signIn();
			
		CustomerServiceFindOrderWidget findOrderWidget = frontPage
			.getHeaderWidget()
			.goToCustomerService()
			.getFindOrderWidget();
			
		findOrderWidget.typeOrderNumber(orderId).submitSearch();
		findOrderWidget.clickActionButton(orderId).clickLockOrder().verifyOrderIsLocked(orderId);
		
		
		findOrderWidget.typeOrderNumber(orderId).submitSearch();
		findOrderWidget.clickActionButton(orderId).clickUnlockOrder().verifyOrderIsUnLocked(orderId);
		
		//cleanup shopper's cart
		orders.removeAllItemsFromCart();
		
						
	 }
	 /** 
	  *  Customer Service Supervisor can register a shopper
	  */
	 @Test
	 public void testFSTOREB2CCSR_1311()
	 {
		 doTearDown = false;
		//Open the store in the browser
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
										
			//Opens the Sign In page in browser.
			SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
									
			//Log in to the store as a CSS admin.
			signIn.typeUsername(dsm.getInputParameter("CSS_LOGONID")).typePassword(dsm.getInputParameter("CSS_PASSWORD"))
				.signIn();
						
			frontPage.getHeaderWidget().goToCustomerService().getSidebarWidget().gotoAddCustomerPage()
					.typeLogonId(dsm.getInputParameter("SHOPPER_LOGONID")+System.currentTimeMillis())
					.typeFirstName(dsm.getInputParameter("FIRST_NAME2"))
					.typeLastName(dsm.getInputParameter("LAST_NAME2"))		 
					.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
					.typeCity(dsm.getInputParameter("CITY"))
					.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
					.typeStateOrProvince(dsm.getInputParameter("PROVINCE"))
					.typeZipCode(dsm.getInputParameter("ZIP_CODE"))
					.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
					.typeEmail(dsm.getInputParameter("EMAIL"))
					.submit();
				
							
	 }
	 /** 
	  *  Customer Service Supervisor can shop on behalf of a shopper
	  */
	 @Test
	 public void testFSTOREB2CCSR_1312()
	 {
		 doTearDown = true;
		 
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
		String orderId = orders.getCurrentOrderId();
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
						
		//Log in to the store as a CSS admin.
		signIn.typeUsername(dsm.getInputParameter("CSS_LOGONID")).typePassword(dsm.getInputParameter("CSS_PASSWORD"))
			.signIn();
						
		CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
		
		findOrderWidget.typeOrderNumber(orderId).submitSearch();
			
		//Access Customer Account
		findOrderWidget.clickActionButton(orderId).clickAccessCustomerAccount();
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
							
		//Go to shipping and billing page
		ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
									
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
		//Confirm the correct Shipping Address is selected
		shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
			.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
		//Confirm the correct Billing address is selected
		shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
			.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
		//select Pay later as the payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
							
		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	
							
		//Confirm product name
		singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("PRODUCT_NAME"));
							
		//Confirm item quantity
		singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_QTY"));
							
		//Confirm item each price
		singleOrderSummary.verifyItemEachPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_EACH_TOTAL"));
							
		//Confirm total price of the item.
		singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_TOTAL"));
							
		//Confirm the total price of the order
		//singleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL"));	
			
		//Verifies Billing Method on order summary page.
		singleOrderSummary.verifyBillingMethod(dsm.getInputParameter("PAY_METHOD"),dsm.getInputParameter("PAYMENT_NUMBER"));		
							
		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
							
		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();	
		
	  // header.openShopCartWidget().clickUnlockCartButton();
						
	 }
	 
	 /** 
	  * Customer Service Supervisor can access order summary page 
	  */
	 @Test
	 public void testFSTOREB2CCSR_1317()
	 {
		 doTearDown = false;
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//add product to cart and complete order
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
		orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
		String orderId = orders.getCurrentOrderId();
		orders.deletePaymentMethod();
		orders.addPayLaterPaymentMethod();
		orders.completeOrder();			
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
				
		//Log in to the store as a CSS admin.
		signIn.typeUsername(dsm.getInputParameter("CSS_LOGONID")).typePassword(dsm.getInputParameter("CSS_PASSWORD"))
			.signIn();
			
		//use find order widget to search for order 
		CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
		findOrderWidget.typeOrderNumber(orderId)
			.submitSearch();
		
		//verify if search results are displayed for order ID 
		findOrderWidget.verifyOrderSearchResultIsDisplayed();
		
		//verify is CSS can access order summary page by verifying order id
		CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget.clickActionButton(orderId).clickOrderSummary();
		orderSummaryPage.verifyOrderId(orderId);
		
								
	 }
	 
	 /** 
	  * Customer Service Supervisor can access order comments slider  
	  */
	 @Test
	 public void testFSTOREB2CCSR_1318()
	 {
		 doTearDown = false;
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//add product to cart and complete order
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
		orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
		String orderId = orders.getCurrentOrderId();
		orders.deletePaymentMethod();
		orders.addPayLaterPaymentMethod();
		orders.completeOrder();			
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
				
		//Log in to the store as a CSS admin.
		signIn.typeUsername(dsm.getInputParameter("CSS_LOGONID")).typePassword(dsm.getInputParameter("CSS_PASSWORD"))
			.signIn();
			
		//use find order widget to search for order 
		CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
		findOrderWidget.typeOrderNumber(orderId)
			.submitSearch();
		
		//verify if search results are displayed for order ID 
		findOrderWidget.verifyOrderSearchResultIsDisplayed();
		
		//go to Order Details page
		OrderDetailPage orderDetailPage = findOrderWidget.clickOrderID(orderId);
		orderDetailPage.getCommentsBar().slideOrderComments();
		
		orderDetailPage.isSliderCommentTextAreaVisible();
		
								
	 }
	 
	 /** 
	  * Customer Service Supervisor can add order comments 
	  */
	 @Test
	 public void testFSTOREB2CCSR_1320()
	 {
		 doTearDown = false;
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//add product to cart and complete order
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
		orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
		String orderId = orders.getCurrentOrderId();
		orders.deletePaymentMethod();
		orders.addPayLaterPaymentMethod();
		orders.completeOrder();			
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
				
		//Log in to the store as a CSS admin.
		signIn.typeUsername(dsm.getInputParameter("CSS_LOGONID")).typePassword(dsm.getInputParameter("CSS_PASSWORD"))
			.signIn();
			
		//use find order widget to search for order 
		CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
		findOrderWidget.typeOrderNumber(orderId)
			.submitSearch();
		
		//verify if search results are displayed for order ID 
		findOrderWidget.verifyOrderSearchResultIsDisplayed();
		
		//verify  CSS can add order comments via order summary page
		CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget.clickActionButton(orderId).clickOrderSummary();
		orderSummaryPage.getComments().toggleOrderComments()
		.getWriteAndDisplayCommentsWidgets()
		.addNewComments(dsm.getInputParameter("COMMENT"));
		
		
								
	 }
		
		/**
		 * Tear down ran after every test case
		 */
		@After
		public void tearDown(){
			try
			{
			if (doTearDown) 
			{
				//Continue browser session starting at the home page.
				HeaderWidget headerSection = getSession().continueToPage(getConfig().getStoreUrl(), HeaderWidget.class);

				//Remove all items from the shopping cart
				headerSection.goToShoppingCartPage().clickLockOrder().removeAllItems().clickUnlockOrder()
				.getHeaderWidget().openSignOutDropDownWidget().signOut();
			
			}
			}
			catch(RuntimeException e)
			{
				getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
			}

		}
}