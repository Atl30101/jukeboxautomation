package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.casl.keys.util.CaslKeysModule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.LanguageCurrencyPopUp;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.MyPersonalInfoPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.casl.util.CaslModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 * Scenario: FV2STOREB2C_06
 * Test scenario to test various use cases associated with user preferred language and currency.
 * Refer to each test case for a detailed use case description 
 */
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class, CaslModule.class, CaslKeysModule.class})
public class FV2STOREB2C_06 extends AbstractAuroraSingleSessionTests
{

	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	 
	 
	@DataProvider
	private final TestDataProvider dsm;
	private final CaslFixturesFactory f_CaslFixtures;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 */
	@Inject
	public FV2STOREB2C_06(
			Logger log, 
			WcWteTestRule wcWebTestRule,
			CaslFoundationTestRule caslTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_CaslFixtures)
			
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		f_CaslFixtures = p_CaslFixtures;
	}
	
	/** Test case to Change the currency of a registered shopper from the My Account page.
	 * @throws Exception
	 */

	@Test
	public void testFV2STOREB2C_0601() throws Exception
	{		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		MyAccountMainPage myAccount = signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//Click Home link on the Header.
		MyPersonalInfoPage personalInfo = myAccount.getSidebar().goToMyPersonalInfoPage();
		
		//Select preferred Currency from drop down list on Personal Information Page.
		personalInfo.typePreferredCurrency(dsm.getInputParameter("SELECTED_CURRENCY"));
		
		//Update the changes		
		myAccount = personalInfo.update();
		
		//Go to home page
		myAccount.getHeaderWidget().goToFrontPage();
		
		//Navigate to category page
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), 
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME") , dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//Verify the currency displayed on mini shop cart
		// (If cart is empty, it does not display the totals or currency.  Need to add an item to the cart.)
		// Click product name from category page 
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		// Select swatches from product display page
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR")).selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));
		
	}
	
	/** Test case to Change the currency of a registered shopper from the Home page.
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_0602() throws Exception
	{
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
			.signIn();

		//Click Home link on the Header.
		frontPage = signIn.getHeaderWidget().goToFrontPage();
		
		//Open Language/Currency pop-up
		LanguageCurrencyPopUp languageCurrencyPopUp = frontPage.getHeaderWidget().goToLanguageCurrencyPopUp();
		
		//Select preferred Currency from drop down list
		languageCurrencyPopUp.selectCurrency(dsm.getInputParameter("SELECTED_CURRENCY"));
		
		//Apply the changes
		languageCurrencyPopUp.apply(AuroraFrontPage.class);		
		
		//Navigate to category page
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), 
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		// If cart is empty, it does not display the totals or currency.  Need to add an item to the cart.
		// Click product name from category page 
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));

		// Select swatches from product display page
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR")).selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));

		
		MyAccountMainPage myAccount = productDisplay.getHeaderWidget().goToMyAccount();
		
		//Click my personal information link
		MyPersonalInfoPage personalInfo = myAccount.getSidebar().goToMyPersonalInfoPage();
		
		//Verify preferred currency in Personal Information page.
		personalInfo.verifyPreferredCurrency(dsm.getInputParameter("CURRENCY"));				
	}
	
	/** 
	 * Test case to Change the preferred language of a registered shopper from the My Account page.
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_0603() throws Exception
	{
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		MyAccountMainPage myAccount = signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();

		//Click my personal information link
		MyPersonalInfoPage personalInfo = myAccount.getSidebar().goToMyPersonalInfoPage();
		
		//Select preferred Language from drop down list on Personal Information Page.
		personalInfo.typePreferredLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"));
		
		//Click update button
		personalInfo.update();
		
		//Log Out of the store.
		signIn = myAccount.getHeaderWidget().openSignOutDropDownWidget().signOut().signIn();
		
		//Log in to the store.
		myAccount = signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
		.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//Click my personal information link
		personalInfo = myAccount.getSidebar().goToMyPersonalInfoPage();
		
		//Verify preferred currency in Personal Information page.
		personalInfo.verifySelectedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"));
		
		//Open language and currency pop-up
		LanguageCurrencyPopUp languageCurrencyPopUp = personalInfo.getHeaderWidget().goToLanguageCurrencyPopUp();
		
		//Verify the language
		languageCurrencyPopUp.verifyLanguage(dsm.getInputParameter("EXPECTED_LANGUAGE"));	
	}
	
	
	/** 
	 * Test case to Change the preferred language of a registered shopper from the Store home page.
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_0604() throws Exception
	{		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		MyAccountMainPage myAccount = signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();;

		//Open Language/Currency pop-up
		LanguageCurrencyPopUp languageCurrencyPopUp = myAccount.getHeaderWidget().goToLanguageCurrencyPopUp();
		
		//Select preferred Language from drop down list
		languageCurrencyPopUp.selectLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"));
		
		//Apply the changes
		languageCurrencyPopUp.apply(MyAccountMainPage.class);

		//Navigate to category page
		CategoryPage subCat = myAccount.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"), dsm.getInputParameter("SUB_CAT"), dsm.getInputParameter("CATEGORY_ITEM"));
		
		//Verify product language
		subCat.getCatalogEntryListWidget().verifyProductLanguage(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Sign out of the store
		signIn = subCat.getHeaderWidget().openSignOutDropDownWidget().signOut().signIn();
		
		//Log in to the store
		myAccount = signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
		.signIn().closeSignOutDropDownWidget().goToMyAccount();;				
	
		//Click my personal information link
		MyPersonalInfoPage personalInfo = myAccount.getSidebar().goToMyPersonalInfoPage();
		
		//Verify preferred currency in Personal Information page.
		personalInfo.verifySelectedLanguage(dsm.getInputParameter("EXPECTED_LANGUAGE"));
	}
	
	/** Test case to Change the currency and the language of a registered shopper from the My Account Page.
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_0605() throws Exception
	{
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		MyAccountMainPage myAccount = signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();;				
		
		//Click my personal information link
		MyPersonalInfoPage personalInfo = myAccount.getSidebar().goToMyPersonalInfoPage();
		
		//Select preferred Language from drop down list on Personal Information Page.
		personalInfo.typePreferredLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"));	
		
		//Select preferred Currency from drop down list on Personal Information Page.
		personalInfo.typePreferredCurrency(dsm.getInputParameter("PREFERRED_CURRENCY"));
		
		//Update the changes		
		myAccount = personalInfo.update();
		
		//Click home page link
		frontPage = myAccount.getHeaderWidget().goToFrontPage();
		
		//Navigate to category page
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"), dsm.getInputParameter("SUB_CAT"), dsm.getInputParameter("CATEGORY_ITEM"));
		
		//Verify product language
		subCat.getCatalogEntryListWidget().verifyProductLanguage(dsm.getInputParameter("PRODUCT_NAME"));
		
		// If cart is empty, it does not display the totals or currency.  Need to add an item to the cart.
		// Click product name from category page 
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));

		// Select swatches from product display page
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR")).selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));
		
		// Click add to cart
		productDisplay.addToCartRequest();

		
		
		//Verify the currency displayed on mini shop cart
		subCat.getHeaderWidget().verifyMiniCartCurrency(dsm.getInputParameter("EXPECTED_CURRENCY"));
	}
	
	/** Test case to Change the Currency and the Language of a Guest Shopper from Home Page.
	 * 	 
	 *  @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_0606() throws Exception
	{
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
						
		//Open Language/Currency pop-up
		LanguageCurrencyPopUp languageCurrencyPopUp = frontPage.getHeaderWidget().goToLanguageCurrencyPopUp();
		
		//Select preferred Currency from drop down list
		languageCurrencyPopUp.selectCurrency(dsm.getInputParameter("PREFERRED_CURRENCY"));
		
		//Select preferred Language from drop down list
		languageCurrencyPopUp.selectLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"));
		
		//Apply the changes
		frontPage = languageCurrencyPopUp.apply(AuroraFrontPage.class);		
		
		//Navigate to category page
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"), dsm.getInputParameter("SUB_CAT"), dsm.getInputParameter("CATEGORY_ITEM"));
		
		//Verify product language
		subCat.getCatalogEntryListWidget().verifyProductLanguage(dsm.getInputParameter("PRODUCT_NAME"));

		// If cart is empty, it does not display the totals or currency.  Need to add an item to the cart.
		// Click product name from category page 
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));

		// Select swatches from product display page
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR")).selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));
		
		// Click add to cart
		productDisplay.addToCartRequest();

		//Verify the currency displayed on mini shop cart
		productDisplay.getHeaderWidget().verifyMiniCartCurrency(dsm.getInputParameter("EXPECTED_CURRENCY"));
		
	}	
	
	/**
	 * Perform teardown operations after the test is done, whether it is successful or not.
	 * @throws Exception
	 */
	@After
	public void testScenarioTearDown() throws Exception
	{
		try
		{
			getLog().info("testScenarioTearDown running");
			
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//Opens the Sign In page in browser.
			SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
			
			//Log on store
			signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
			.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount()
			.getSidebar().goToMyPersonalInfoPage()		
			//Reset Shopper personal preferred language and currency 
			.typePreferredLanguage(dsm.getInputParameter("ORIGINAL_LANGUAGE"))
			.typePreferredCurrency(dsm.getInputParameter("ORIGINAL_CURRENCY"))
			.update();
			
			// Empty the cart
			// Remove all items from the shopping cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
			orders.removeAllItemsFromCart();
			orders.deletePaymentMethod();
			
			signIn.getHeaderWidget().openSignOutDropDownWidget().signOut();
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

		
	}
}
