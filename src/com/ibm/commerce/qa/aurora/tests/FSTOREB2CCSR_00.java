package com.ibm.commerce.qa.aurora.tests;


	/*
	 *-----------------------------------------------------------------
	 * Licensed Materials - Property of IBM
	 * 
	 * WebSphere Commerce
	 *
	 * (C) Copyright IBM Corp. 2012
	 *
	 * US Government Users Restricted Rights - Use, duplication or
	 * disclosure restricted by GSA ADP Schedule Contract with
	 * IBM Corp.
	 *-----------------------------------------------------------------
	 */

	import java.util.logging.Logger;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CustomerRegisterPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.casl.util.CaslModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.OrgAdminConsole;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;


	/** Scenario: PreSetup
	 *  Details: This test should be run before any other tests in the bucket to ensure that the users common to many test
	 *  cases are registered in the store and tooling as appropriate.
	 */
	@Category(Sanity.class)
	@RunWith(GuiceTestRunner.class)
	@TestModules({AuroraModule.class, CaslModule.class})
	public class FSTOREB2CCSR_00 extends AbstractAuroraSingleSessionTests
	{
		   /**
		    * The internal copyright field.
	     	*/
			public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
			
			//A variable to send requests to OrgadminConsole
			private OrgAdminConsole oac;
			
			@DataProvider 
			private final TestDataProvider dsm;
			private final CaslFixturesFactory f_CaslFixtures;
			boolean doTearDown = false;
			
			/**
			 * Test Class object constructor.
			 * 
			 * @param log
			 * @param config
			 * @param session
			 * @param oac
			 * @param dataSetManager
			 */
			@Inject
			public FSTOREB2CCSR_00(
					Logger log, 
					CaslFoundationTestRule caslTestRule,
					WcWteTestRule wcWteTestRule,
					OrgAdminConsole oac,
					TestDataProvider dataSetManager,CaslFixturesFactory p_CaslFixtures)
			{
				super(log, wcWteTestRule, caslTestRule);
				this.oac = oac;
				this.dsm = dataSetManager;
				f_CaslFixtures = p_CaslFixtures;
			}

		  /**
			* Creates administrative users that will be utilized in other scenarios thru out this bucket.
			* Pre-conditions: <ul>
			*					<li>Tester knows the Store URL (http://<hostname>/webapp/wcs/stores/servlet/en/auroraesite)</li>
			* 				</ul>
			* Post conditions: Registration Successful. The user is taken to My Account page.
			* @throws Exception
			*/
			
			
		@Test
		public void CreateCSRUsers() throws Exception
		{
			//Logon to orgadminconsole using the admin account
			oac.logon(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"));
			
			//Create a CSR account 
			oac.createNewUser(dsm.getInputParameter("CSR_USERID"), 
					dsm.getInputParameter("CSR_FIRST_NAME") , 
					dsm.getInputParameter("CSR_LAST_NAME"), 
					dsm.getInputParameter("CSR_USERPASSWORD"), 
					dsm.getInputParameter("CSR_PASSWORD_VERIFY"), 
					dsm.getInputParameter("CSR_POLICYID"), 
					dsm.getInputParameter("CSR_STATUS"), 
					dsm.getInputParameter("CSR_LANGUAGE"), 
					dsm.getInputParameter("CSR_PARENTORG"), 
					dsm.getInputParameter("CSR_ADDRESS"), 
					dsm.getInputParameter("CSR_CITY"), 
					dsm.getInputParameter("CSR_STATE"), 
					dsm.getInputParameter("CSR_COUNTRY"), 
					dsm.getInputParameter("CSR_ZIPCODE"), 
					dsm.getInputParameter("CSR_EMAIL"), 
					dsm.getInputParameter("CSR_PASSWORD_EXPIRED"));
			
			
			//Assign role as a Customer Service Rep
			oac.assignRoleToUser(dsm.getInputParameter("CSR_USERID"),
			dsm.getInputParameter("CSR_PARENTORG"), dsm.getInputParameter("CSR_ROLE"));		
			
			//Create a CSR account 2
			oac.createNewUser(dsm.getInputParameter("CSR_USERID_2"), 
					dsm.getInputParameter("CSR_FIRST_NAME") , 
					dsm.getInputParameter("CSR_LAST_NAME"), 
					dsm.getInputParameter("CSR_USERPASSWORD"), 
					dsm.getInputParameter("CSR_PASSWORD_VERIFY"), 
					dsm.getInputParameter("CSR_POLICYID"), 
					dsm.getInputParameter("CSR_STATUS"), 
					dsm.getInputParameter("CSR_LANGUAGE"), 
					dsm.getInputParameter("CSR_PARENTORG"), 
					dsm.getInputParameter("CSR_ADDRESS"), 
					dsm.getInputParameter("CSR_CITY"), 
					dsm.getInputParameter("CSR_STATE"), 
					dsm.getInputParameter("CSR_COUNTRY"), 
					dsm.getInputParameter("CSR_ZIPCODE"), 
					dsm.getInputParameter("CSR_EMAIL"), 
					dsm.getInputParameter("CSR_PASSWORD_EXPIRED"));
			
			
			//Assign role as a Customer Service Rep 2
			oac.assignRoleToUser(dsm.getInputParameter("CSR_USERID_2"),
			dsm.getInputParameter("CSR_PARENTORG"), dsm.getInputParameter("CSR_ROLE"));		

			//Create a CSS account
			oac.createNewUser(dsm.getInputParameter("CSS_USERID"), 
					dsm.getInputParameter("CSS_FIRST_NAME") , 
					dsm.getInputParameter("CSS_LAST_NAME"), 
					dsm.getInputParameter("CSS_USERPASSWORD"), 
					dsm.getInputParameter("CSS_PASSWORD_VERIFY"), 
					dsm.getInputParameter("CSS_POLICYID"), 
					dsm.getInputParameter("CSS_STATUS"), 
					dsm.getInputParameter("CSS_LANGUAGE"), 
					dsm.getInputParameter("CSS_PARENTORG"), 
					dsm.getInputParameter("CSS_ADDRESS"), 
					dsm.getInputParameter("CSS_CITY"), 
					dsm.getInputParameter("CSS_STATE"), 
					dsm.getInputParameter("CSS_COUNTRY"), 
					dsm.getInputParameter("CSS_ZIPCODE"), 
					dsm.getInputParameter("CSS_EMAIL"), 
					dsm.getInputParameter("CSS_PASSWORD_EXPIRED"));
			
			
			//Assign role as a Customer Service Rep
			oac.assignRoleToUser(dsm.getInputParameter("CSS_USERID"),
			dsm.getInputParameter("CSS_PARENTORG"), dsm.getInputParameter("CSS_ROLE"));				
		}
		
		
		/**
		 * Test case to register a Shopper from the store home page with valid inputs. This shopper can be used across multiple test cases in this bucket.
		 * @throws Exception
		 */
		@Test
		public void setupCustomer1() throws Exception{
			//Open Auroraesite store
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
			
			//Click on the SignIn page link on the header
			SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
			
			//attempt to sign in:
			try {
				signInPage.typeUsername(dsm.getInputParameter("LOGONID"))
					.typePassword(dsm.getInputParameter("PASSWORD"))
					.signIn();
			} catch (Exception e1) {
				//still on the sign in page, user isn't registered
				//Click on the registration button on the SignIn page
				CustomerRegisterPage crp= signInPage.registerCustomer();

				crp
				//type username
				.typeLogonId(dsm.getInputParameter("LOGONID"))
				
				//type first name
				.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
				
				//type last name
				.typeLastName(dsm.getInputParameter("LAST_NAME"))
				
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				
				//Verify password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
				
				//type street address
				.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
				
				//Select country
				.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
				
				//type or select state
				.selectStateOrProvince(dsm.getInputParameter("STATE"))
				
				//type city
				.typeCity(dsm.getInputParameter("CITY"))
				
				//type zipcode
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				
				//type E-mail
				.typeEmail(dsm.getInputParameter("EMAIL"))
				
				//type home phone number
				.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
				
				//Select gender
				.selectGender(dsm.getInputParameter("GENDER"))
				
				//type mobile phone number
				.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
				
				//Select  birth year
				.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
				
				//Select birth month
				.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
				
				//Select birth day
				.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
				//submit user registration
				.submit();
			}
		}	
		
		
		/**
		 * Test case to register a Shopper from the store home page with valid inputs. This shopper can be used across multiple test cases in this bucket.
		 * @throws Exception
		 */
		@Test
		public void setupCustomer2() throws Exception{
			//Open Auroraesite store
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
			
			//Click on the SignIn page link on the header
			SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();

			
			//attempt to sign in:
			try {
				signInPage.typeUsername(dsm.getInputParameter("LOGONID2"))
					.typePassword(dsm.getInputParameter("PASSWORD"))
					.signIn();
			} catch (Exception e1) {
				//still on the sign in page, user isn't registered
				//Click on the registration button on the SignIn page
				CustomerRegisterPage crp= signInPage.registerCustomer();

				crp
				//type username
				.typeLogonId(dsm.getInputParameter("LOGONID2"))
				
				//type first name
				.typeFirstName(dsm.getInputParameter("FIRST_NAME2"))
				
				//type last name
				.typeLastName(dsm.getInputParameter("LAST_NAME2"))
				
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				
				//Verify password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
				
				//type street address
				.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
				
				//Select country
				.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
				
				//type or select state
				.selectStateOrProvince(dsm.getInputParameter("STATE"))
				
				//type city
				.typeCity(dsm.getInputParameter("CITY"))
				
				//type zipcode
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				
				//type E-mail
				.typeEmail(dsm.getInputParameter("EMAIL"))
				
				//type home phone number
				.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
				
				//Select gender
				.selectGender(dsm.getInputParameter("GENDER"))
				
				//type mobile phone number
				.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
				
				//Select  birth year
				.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
				
				//Select birth month
				.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
				
				//Select birth day
				.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
				//submit user registration
				.submit();
			}
		}	
		
		
		
		/**
		 * Test case to register a Shopper from the store
		 *  page with valid inputs. This shopper can be used across multiple test cases in this bucket.
		 * @throws Exception
		 */
		@Test
		public void setupCustomer3() throws Exception{
			//Open Auroraesite store
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
			
			//Click on the SignIn page link on the header
			SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
			
			//attempt to sign in:
			try {
				signInPage.typeUsername(dsm.getInputParameter("LOGONID3"))
					.typePassword(dsm.getInputParameter("PASSWORD"))
					.signIn();
			} catch (Exception e1) {
				//still on the sign in page, user isn't registered
				//Click on the registration button on the SignIn page
				CustomerRegisterPage crp= signInPage.registerCustomer();

				crp
				//type username
				.typeLogonId(dsm.getInputParameter("LOGONID3"))
				
				//type first name
				.typeFirstName(dsm.getInputParameter("FIRST_NAME3"))
				
				//type last name
				.typeLastName(dsm.getInputParameter("LAST_NAME3"))
				
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				
				//Verify password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
				
				//type street address
				.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
				
				//Select country
				.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
				
				//type or select state
				.selectStateOrProvince(dsm.getInputParameter("STATE"))
				
				//type city
				.typeCity(dsm.getInputParameter("CITY"))
				
				//type zipcode
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				
				//type E-mail
				.typeEmail(dsm.getInputParameter("EMAIL"))
				
				//type home phone number
				.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
				
				//Select gender
				.selectGender(dsm.getInputParameter("GENDER"))
				
				//type mobile phone number
				.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
				
				//Select  birth year
				.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
				
				//Select birth month
				.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
				
				//Select birth day
				.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
				//submit user registration
				.submit();
			}
		}	
		
		/**
		 * Test case to register a Shopper from the store home page with valid inputs. This shopper can be used across multiple test cases in this bucket.
		 * @throws Exception
		 */
		@Test
		public void setupCustomer4() throws Exception{
			//Open Auroraesite store
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
			
			//Click on the SignIn page link on the header
			SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
			
			//attempt to sign in:
			try {
				signInPage.typeUsername(dsm.getInputParameter("LOGONID4"))
					.typePassword(dsm.getInputParameter("PASSWORD"))
					.signIn();
			} catch (Exception e1) {
				//still on the sign in page, user isn't registered
				//Click on the registration button on the SignIn page
				CustomerRegisterPage crp= signInPage.registerCustomer();

				crp
				//type username
				.typeLogonId(dsm.getInputParameter("LOGONID4"))
				
				//type first name
				.typeFirstName(dsm.getInputParameter("FIRST_NAME4"))
				
				//type last name
				.typeLastName(dsm.getInputParameter("LAST_NAME4"))
				
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				
				//Verify password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
				
				//type street address
				.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
				
				//Select country
				.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
				
				//type or select state
				.selectStateOrProvince(dsm.getInputParameter("STATE"))
				
				//type city
				.typeCity(dsm.getInputParameter("CITY"))
				
				//type zipcode
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				
				//type E-mail
				.typeEmail(dsm.getInputParameter("EMAIL"))
				
				//type home phone number
				.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
				
				//Select gender
				.selectGender(dsm.getInputParameter("GENDER"))
				
				//type mobile phone number
				.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
				
				//Select  birth year
				.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
				
				//Select birth month
				.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
				
				//Select birth day
				.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
				//submit user registration
				.submit();
			}
		}	
		/**
		 * Place customer order
		 * 
		 */
		@Test
		public void setupCustomerOrder() throws Exception{
			
			doTearDown = true;
			
			//Place order 1
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY",Double.class));
			orders.deletePaymentMethod();
			orders.addPayLaterPaymentMethod();
			orders.completeOrder();
			
			
			//Place order 2
			orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID2"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU2"), dsm.getInputParameterAsNumber("QTY",Double.class));
			orders.deletePaymentMethod();
			orders.addPayLaterPaymentMethod();
			orders.completeOrder();
		}
		
		
		
	}

