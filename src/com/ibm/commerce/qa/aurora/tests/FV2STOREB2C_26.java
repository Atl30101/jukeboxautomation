package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/**			
* Scenario: FV2STOREB2C_25
* Details: This scenario will test the creation of merchandising associations between catalog entries and then validate them in the storefront.
*
* 
*/
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_26 extends AbstractAuroraSingleSessionTests
{

	/**
 	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

	
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	
	private CMC cmc;


	private CaslFixturesFactory f_CaslFixtures;
	
	
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param cmc 
	 * 			   object to perform CMC tasks
	 * @param p_CaslFixtures 
	 */
	@Inject
	public FV2STOREB2C_26(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CMC cmc, CaslFixturesFactory p_CaslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		this.cmc = cmc;
		f_CaslFixtures = p_CaslFixtures;
	}

	/**A variable to hold merchandise association identifier created**/
	private String associationId;
	
	/**A variable to hold the catalog entry identifier**/
	private String catalogEntryId;
	
	/**A variable to hold the catalog entry identifier**/
	private String assocCatalogEntryId;
	
	/**
	 * A test to Merchandise associate a product to another product as a replacement.
	 * @throws Exception
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_2601() throws Exception
	{	
		//login to CMC
		dsm.setDataLocation("testFV2STOREB2C_2601", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore();
		cmc.selectCatalog();
		
		//get the catentryId of the product
		dsm.setDataLocation("testFV2STOREB2C_2601", "createMerchandiseAssociate");
		catalogEntryId = cmc.getCatalogEntryId(dsm.getInputParameter("PRODUCT_1"));
		assocCatalogEntryId = cmc.getCatalogEntryId(dsm.getInputParameter("PRODUCT_2"));


		//create Merchandising Association for the given product 
		associationId = cmc.createMerchandisingAssociation(catalogEntryId, assocCatalogEntryId,dsm.getInputParameter("name"),dsm.getInputParameter("quantity"),null);
		
		//log off from CMC
		cmc.logoff();
			
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_2601","testFV2STOREB2C_2601");
		
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Login with valid user id and password
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		signIn.typeUsername(dsm.getInputParameter("USER_NAME")).typePassword(dsm.getInputParameter("USER_PASSWORD"));
		
		
		//Wait for the logon to succeed before proceeding.
		HeaderWidget header = signIn.signIn().closeSignOutDropDownWidget();
		
		//clear the shopping cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("USER_NAME"), dsm.getInputParameter("USER_PASSWORD"), getConfig().getStoreName());
		orders.removeAllItemsFromCart();
		
		
		//Click the Top category in menu
		CategoryPage subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CATEGORY_NAME"),dsm.getInputParameter("SUB_CATEGORY_NAME"));
		
		//click on the product image
		ProductDisplayPage prodPage = subCat.getCatalogEntryListWidget().goToProductPageByImageID(catalogEntryId);
	
		//add to cart the Merchandising Association products
		prodPage.AddToCartMerchandisingAssociation(Long.parseLong(assocCatalogEntryId));

		//click the shopping cart link from header
		ShopCartPage shopCart = prodPage.getHeaderWidget().goToShoppingCartPage();

		//verify both products have been added
		shopCart.verifyItemInShopCart(dsm.getInputParameter("PRODUCT_2"));
		//click on checkout
		ShippingAndBillingPage bill = shopCart.continueToNextStep();
		
		//select billing method
		bill.selectPaymentMethod(dsm.getInputParameter("BILLING_METHOD"));
		
		//click on next button
		OrderSummarySingleShipPage summary = bill.singleShipNext();
		
		//click on order button
		OrderConfirmationPage confirm = summary.completeOrder();
		
		//verify the order is placed
		confirm.verifyOrderSuccessful();
		
		confirm.getHeaderWidget().goToShoppingCartPage().removeAllItems().getHeaderWidget().openSignOutDropDownWidget().signOut();
	}
	
	/**
	 * Perform teardown operations after the test is done, whether it is successful or not.
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception
	{
	try
	{	
		
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("USER_NAME"), dsm.getInputParameter("USER_PASSWORD"), getConfig().getStoreName());
		orders.removeAllItemsFromCart();
		orders.deletePaymentMethod();
		
		//login to CMC
		dsm.setDataLocation("testFV2STOREB2C_2601", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.selectStore();
		cmc.selectCatalog();
		
		cmc.deleteMerchandisingAssociation(associationId,catalogEntryId,assocCatalogEntryId);
		
		//log off from CMC
		cmc.logoff();
	}
	catch(RuntimeException e)
	{
		getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
	}
	}
	
}
