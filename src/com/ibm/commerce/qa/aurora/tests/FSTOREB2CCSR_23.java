package com.ibm.commerce.qa.aurora.tests;


	/*
	 *-----------------------------------------------------------------
	 * Licensed Materials - Property of IBM
	 *
	 * 
	 *
	 * WebSphere Commerce
	 *
	 * (C) Copyright IBM Corp. 2012
	 *
	 * US Government Users Restricted Rights - Use, duplication or
	 * disclosure restricted by GSA ADP Schedule Contract with
	 * IBM Corp.
	 *-----------------------------------------------------------------
	 */

	import java.util.logging.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.casl.keys.CaslKeysFactory;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CustomerServiceOrderSummaryPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.CustomerServiceFindOrderWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.wte.util.WcConfigManager;



	/** 
	 * Test scenario to test various use cases associated with Dropdown menu context
	 * Refer to each test case for a detailed use case description
	 */
	@RunWith(GuiceTestRunner.class)
	@TestModules(AuroraModule.class)
	public class FSTOREB2CCSR_23 extends AbstractAuroraSingleSessionTests
	{
		/**
		 * The internal copyright field.
		 */
		public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

		//A Variable to retrieve data from the data file. 
		@DataProvider
		private final TestDataProvider dsm;
		private final CaslFixturesFactory f_CaslFixtures;
		

		
		/**
		 * Test Class object constructor.
		 * 
		 * @param log
		 * 			   logging object 
		 * @param config
		 * 			   object to work with getConfig().properties file
		 * @param session
		 * 			   factory to create browser sessions
		 * @param dataSetManager
		 * 			   object to work with data files
		 * @param p_caslFixtures 
		 */		
		@Inject
		public FSTOREB2CCSR_23(
				Logger log, 
				WcConfigManager config,
				WcWteTestRule wcWebTestRule,
				CaslFoundationTestRule caslTestRule,
				TestDataProvider dataSetManager,
				CaslFixturesFactory p_caslFixtures, CaslKeysFactory p_caslKeysFactory)
		{
			super(log, wcWebTestRule, caslTestRule);
			this.dsm = dataSetManager;
			f_CaslFixtures = p_caslFixtures;
		}

		
		

		
		/**
		 * Test case to expand and collapse the order summary section 
		 */
		@Test
		public void testFSTOREB2CCSR_2301()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			 
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = frontPage.getHeaderWidget().goToShoppingCartPage();
			
			//Go to shipping and billing page
			ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
			//Confirm the correct Shipping Address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Confirm the correct Billing address is selected
			shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
				.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
							
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
							
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();	
			
			
			//Log in as CSR
			AuroraFrontPage frontPage1 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			frontPage1.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//search for order using the orderId for completed shopping checkout 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage1.getHeaderWidget()
					.goToCustomerService()
					.getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
			findOrderWidget.verifyOrderSearchResultIsDisplayed();
			
			//collapse the order summary table and verify that the correct SKU is in the table
			CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget
					.clickActionButton(orderId).clickOrderSummary();
			orderSummaryPage.getOrderTable().toggleOrderSummary()
			.verifyItemPresent(dsm.getInputParameter("SKU"));
			
		  
			
		}

		/**
		 * Test case to add expand and collapse the order comments section 
		 * verify that if order comments section is collapsed then 
		 */
		@Test
		public void testFSTOREB2CCSR_2302()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			 
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = frontPage.getHeaderWidget().goToShoppingCartPage();
			
			//Go to shipping and billing page
			ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
			//Confirm the correct Shipping Address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Confirm the correct Billing address is selected
			shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
				.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
							
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
							
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();	
			
			
			AuroraFrontPage frontPage1 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			//Log in as CSR
			frontPage1.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//search for order using the orderId for completed shopping checkout 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage1.getHeaderWidget()
					.goToCustomerService()
					.getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId)
			.submitSearch();
			findOrderWidget.verifyOrderSearchResultIsDisplayed();
			
			CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget.clickActionButton(orderId)
					.clickOrderSummary();
			
			//order comments section should NOT be visible 
			try{
			//add a new comment to the order on the Order Summary Page without expanding the order comments section 

			orderSummaryPage.getComments().getWriteAndDisplayCommentsWidgets();
			orderSummaryPage.isOrderCommentVisibleOnPage();
			}catch(IllegalStateException e)	{
				System.out.println("OrderCommentWriteAndDisplayWidget should not be available.");
			}
			
			//order comments section should be visible 

			orderSummaryPage.getComments().toggleOrderComments().getWriteAndDisplayCommentsWidgets();
			orderSummaryPage.isOrderCommentVisibleOnPage();
			
		}	
		
		/**
		 * Test case to verify pagination for the order comments section 
		 */
		@Test
		public void testFSTOREB2CCSR_2303()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			 
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = frontPage.getHeaderWidget().goToShoppingCartPage();
			
			//Go to shipping and billing page
			ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
			//Confirm the correct Shipping Address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Confirm the correct Billing address is selected
			shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
				.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
							
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
							
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();	
			
			
			AuroraFrontPage frontPage1 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			//Log in as CSR
			frontPage1.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//search for order using the orderId for completed shopping checkout 
			CustomerServiceFindOrderWidget findOrderWidget = frontPage1.getHeaderWidget()
					.goToCustomerService()
					.getFindOrderWidget();
			findOrderWidget.typeOrderNumber(orderId).submitSearch();
			findOrderWidget.verifyOrderSearchResultIsDisplayed();
			
			//add a new comment to the order on the Order Summary Page  
			CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget.clickActionButton(orderId).clickOrderSummary();
			
			orderSummaryPage.getComments().toggleOrderComments()
			.getWriteAndDisplayCommentsWidgets()
		    .addNewComments("this is order # 1");
			
			for(int i = 2; i<= 16; i++){
				orderSummaryPage.getComments()
				.getWriteAndDisplayCommentsWidgets()
			    .addNewComments("this is order # " + i);
			}
		    
			orderSummaryPage.getComments()
			.getWriteAndDisplayCommentsWidgets().verifyLastComment("this is order # 2");
			 
		}
		
		
	}
