package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.IBMCopyright;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;




//Declares a new test scenario called FV2STOREB2C_SOLR_SEARCH_01 which contains one or more test case methods.
//All test scenario classes must extend StoreModelTestCase.

/**
 * Scenario: FV2STOREB2C_SOLR_SEARCH_05 Objective: To specify a search term in the search box
 * Pre-requisites: Tester knows the Store URL
 * (http://<hostname>/webapp/wcs/stores/servlet/en/auroraesite) 
 * Search based navigation is enabled
 *  Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_SOLR_SEARCH_05 extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = IBMCopyright.SHORT_COPYRIGHT;

	//A variable to hold the name of the data file where input parameters can be found.
	//$ANALYSIS-IGNORE
	protected final String dataFileName = "data/FV2STOREB2C_SOLR_SEARCH_05_Data.xml";

	@DataProvider
	private final TestDataProvider dsm;
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dsm
	 */
	@Inject
	public FV2STOREB2C_SOLR_SEARCH_05(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dsm)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dsm;
	}

	////////////////////////////////////////////////////////////
	/** Test case to test Specified keyword is a commonly misspelled word
	 * Post conditions: Search results page displays the
	 * product match for the keyword search
	 * @throws Exception
	 */
	@Category(Sanity.class)
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_0501() throws Exception {
		dsm.setDataFile(dataFileName);
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_0501", "testFV2STOREB2C_SOLR_SEARCH_0501_DATABLOCK");
		
		//Open store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Search a misspelled word 
		SearchResultsPage searchResults = frontPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCHTERM_1"));

		//verify the recommendation results are present
		searchResults.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("prod1"));
		//verify the recommendation results are present
		searchResults.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("prod2"));
		
	}	
	
	/** Test case to test Specified keyword matches a sensitive keyword that maps to a specific view name
     *
	 * Post conditions: Search results page displays the
	 * product match for the keyword search
	 * @throws Exception
	 */
	
	// no default land page so this test case is invalid for fep5 but should be added once there is a default
	// landing page
//	@DataProvider(auto=false)
//	@Test
//	public void testFV2STOREB2C_SOLR_SEARCH_0503() throws Exception {
//		dsm.setDataFile(dataFileName);
//		//Tell which test case to use for input parameters
//		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_0503", "testFV2STOREB2C_SOLR_SEARCH_0503_DATABLOCK");
//		
//		//Open store
//		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
//		
//		//Search for a word with special mapping
//		SearchResultsPage searchResults = frontPage.getHeaderWidget().performSearch(dsm.getInputParameter("SEARCHTERM_1"));
//		Thread.sleep(10000);
//		//verify the required text is present
//		//assertEquals("The expected search results are not present.", true, Common.verifyText(getOutputParameter("EXPECTED_RESULT_1"), test.getDefaultTimeout()));	
//		searchResults.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
//	}	
	
	
	
}