package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage.ItemPriceRange;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.url.DeltaUpdates;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/**			
* Scenario: FV2STOREB2C_25
* Details: This scenario will compare attributes of selected items.
*
*/
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_25 extends AbstractAuroraSingleSessionTests
{

	/**
	 * The internal copyright field.
	 */
	 public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	private final CaslFixturesFactory f_CaslFixtures;
	
	private CMC cmc;
	
	/** Used to verify if a product is present **/
	public static final String CAT_PRODUCT_IMAGE_LINK = "CatalogEntryProdImg_";
	

	/**
	 * Test Class object constructor.
	 * 
	 * @param log logging object 
	 * @param config object to work with config.properties file
	 * @param session factory to create browser sessions
	 * @param dataSetManager  object to work with data files
	 * @param cmc object to perform CMC tasks
	 * @param deltaUpdate
	 */
	@Inject
	public FV2STOREB2C_25(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CMC cmc,
			DeltaUpdates deltaUpdate,
			CaslFixturesFactory p_CaslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		this.cmc = cmc;
		this.deltaUpdate = deltaUpdate;
		f_CaslFixtures = p_CaslFixtures;
	}

	/**A variable to hold the catalog entry identifier created**/
	private String productId;
	
	/**A variable to hold the master catalog**/
	private String masterCatalogId;

	/**A variable to hold delta update**/
	private DeltaUpdates deltaUpdate;

	/**
	 * A test to login to CMC and create a product.
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_2501() throws Exception
	{	
		//login to CMC
		dsm.setDataLocation("testFV2STOREB2C_2501", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore();
		cmc.selectCatalog();
		String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
		
		//create a new product
		dsm.setDataLocation("testFV2STOREB2C_2501", "createProduct");
		productId = cmc.createProduct(dsm.getInputParameter("parentCategory"),dsm.getInputParameter("partNumber"),dsm.getInputParameter("productName"),dsm.getInputParameter("published"));

 		cmc.deleteCatalogEntry("");
		//set first offer price and list price
		dsm.setDataLocation("testFV2STOREB2C_2501", "offerPrice1");
		cmc.createOfferPriceForCatalogEntry(productId);
		cmc.createListPriceForCatalogEntry(productId);
		
		//set second offer price
		dsm.setDataLocation("testFV2STOREB2C_2501", "offerPrice2");
		cmc.createOfferPriceForCatalogEntry(productId);
		
		//set third offer price
		dsm.setDataLocation("testFV2STOREB2C_2501", "offerPrice3");
		cmc.createOfferPriceForCatalogEntry(productId);
		
		//create attributes for the created product
		dsm.setDataLocation("testFV2STOREB2C_2501", "createProductAttribute");
		String attributeId = cmc.createProductDefiningAttribute(productId);
		
		//create attributes values for the product
		cmc.createProductDefiningAttributeValues(productId, attributeId, dsm.getInputParameter("attributeValue1"),"1");
		cmc.createProductDefiningAttributeValues(productId, attributeId, dsm.getInputParameter("attributeValue2"),"2");
		
		//create an SKU for the product
		dsm.setDataLocation("testFV2STOREB2C_2501", "createSKU");
		String itemId = cmc.createSKU(productId,dsm.getInputParameter("partNumber"),dsm.getInputParameter("skuName"),dsm.getInputParameter("published"));
		cmc.addSKUDefiningAttributeValue(productId,itemId,attributeId,dsm.getInputParameter("attributeValue"),dsm.getInputParameter("languageId"));
		
		
		//set first offer price and list price for the SKU
		dsm.setDataLocation("testFV2STOREB2C_2501", "offerPrice1");
		cmc.createOfferPriceForCatalogEntry(itemId);
		cmc.createListPriceForCatalogEntry(itemId);
		
		//set second offer price
		dsm.setDataLocation("testFV2STOREB2C_2501", "offerPrice2");
		cmc.createOfferPriceForCatalogEntry(itemId);
		
		//set third offer price
		dsm.setDataLocation("testFV2STOREB2C_2501", "offerPrice3");
		cmc.createOfferPriceForCatalogEntry(itemId);
		
		masterCatalogId = cmc.selectCatalog();

		// Trigger a delta update to ensure the product will be visible.
		deltaUpdate.beforeWaitForDelta(masterCatalogId);
		getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
		deltaUpdate.waitForDelta(masterCatalogId, 60);
		
		//log off from CMC
		cmc.logoff();
		
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		dsm.setDataLocation("testFV2STOREB2C_2501", "testFV2STOREB2C_2501");
		//Login with valid user id and password
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"));
		
		//Wait for the logon to succeed before proceeding.
		HeaderWidget header = signIn.signIn().closeSignOutDropDownWidget();
		
		//Click top category in department drop down
		CategoryPage subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CATEGORY_NAME"),dsm.getInputParameter("SUB_CATEGORY_NAME"));
		subCat.getCatalogEntryListWidget().verifyProductIsPresent(productId);
	    
	    ProductDisplayPage productPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("productName"));
	    
	    // Verify the Price Range is displayed with correct quantities and prices
	    // Range 1 has a minimum and a maximum
	    ItemPriceRange range1 = new ItemPriceRange(Integer.parseInt (dsm.getInputParameter("RANGE_1_MIN")),
	    		Integer.parseInt (dsm.getInputParameter("RANGE_1_MAX")),
	    		new BigDecimal(dsm.getInputParameter("PRICE_1")));
	    // Range 2 has a minimum and a maximum
	    ItemPriceRange range2 = new ItemPriceRange(Integer.parseInt (dsm.getInputParameter("RANGE_2_MIN")),
	    		Integer.parseInt (dsm.getInputParameter("RANGE_2_MAX")),
	    		new BigDecimal(dsm.getInputParameter("PRICE_2")));
	    // Range 3 has a minimum but no maximum
	    ItemPriceRange range3 = new ItemPriceRange(Integer.parseInt (dsm.getInputParameter("RANGE_3_MIN")),
	    		new BigDecimal(dsm.getInputParameter("PRICE_3")));
	    
	    // Verify the Price Ranges on the PDP against the objects created above
		productPage.verifyPriceRanges(productId, Arrays.asList(range1, range2, range3));

	    //add the created product to cart
	    productPage.addToCartRequest();
		
		//click on Shopping Cart link from the header
		ShopCartPage cart = subCat.getHeaderWidget().goToShoppingCartPage();
		
		// Verify the unit price is correct for the default quantity
		cart.verifyProductPriceUpdated("1", dsm.getInputParameter("PRICE_1"));
		
		//change the quantity
		cart.changeQuantity("1", dsm.getInputParameter("NEW_QUANTITY_1"));
		
		// Verify the unit price is correct for the new quantity
		cart.verifyProductPriceUpdated("1", dsm.getInputParameter("PRICE_2"));
		
		//change the quantity
		cart.changeQuantity("1", dsm.getInputParameter("NEW_QUANTITY_2"));

		// Verify the unit price is correct for the new quantity
		cart.verifyProductPriceUpdated("1", dsm.getInputParameter("PRICE_3"));

		//remove all items from shop cart
		cart.removeAllItems();
		
		//Click on log out link
		cart.getHeaderWidget().openSignOutDropDownWidget().signOut();

	}
	
	
	
	/**
	 * Perform teardown operations after the test is done, whether it is successful or not.
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception
	{
		try
		{
			//Open the store in the browser
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
			
			//Login with valid user id and password
			SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
			signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"));
			
			//click on Shopping Cart link from the header
			ShopCartPage cart = signIn.signIn().closeSignOutDropDownWidget().goToShoppingCartPage();
			//cart = cart.removeAllItems();
			
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
			orders.removeAllItemsFromCart();
			orders.deletePaymentMethod();
			
			//login to CMC
			cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
			
			//select store and catalog
			cmc.setLocale(dsm.getInputParameter("locale"));
			cmc.selectStore();
			cmc.selectCatalog();
			String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
			
			//delete the product
			dsm.setDataLocation("testFV2STOREB2C_2501", "createProduct");
			cmc.deleteCatalogEntry(productId);
			
			masterCatalogId = cmc.selectCatalog();
			//Wait until delta update completes before timeout
			//deltaUpdate.waitForDeltaWithinApproximateTimeout(masterCatalogId, 600);
			// Trigger a delta update to ensure the product will be visible.
			deltaUpdate.beforeWaitForDelta(masterCatalogId);
			getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
			deltaUpdate.waitForDelta(masterCatalogId, 60);
			//log off from CMC
			cmc.logoff();		
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}
	}
	
	
	
}