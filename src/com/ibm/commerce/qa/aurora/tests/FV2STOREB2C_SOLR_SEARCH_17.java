package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2013
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.IBMCopyright;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AdvancedSearchPage;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.page.SubCategoryPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/** Test scenario to filter search results based on selected facets
 *  Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_SOLR_SEARCH_17 extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = IBMCopyright.SHORT_COPYRIGHT;

	//A variable to hold the name of the data file where input parameters can be found.
	//$ANALYSIS-IGNORE
	protected final String dataFileName = "src/data/FV2STOREB2C_SOLR_SEARCH_17_Data.xml";

	@DataProvider
	private final TestDataProvider dsm;
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dsm
	 */
	@Inject
	public FV2STOREB2C_SOLR_SEARCH_17(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dsm)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dsm;
	}
	
	/** Test Case to verify that store front search box suggests category 
	 * that match the search term being typed by the user 
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1701() throws Exception 
	{
		dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1701", "testFV2STOREB2C_SOLR_SEARCH_1701_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Type search term
		HeaderWidget headerSection = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
		
		//verify highlighted string
		headerSection.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_1"));
		
		//check for keywords in search pop up
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
	}
	
	/** Test Case to verify that clicking on one of the suggested category directs 
	 * the user to the selected category page 
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1703() throws Exception 
	{
		dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1703", "testFV2STOREB2C_SOLR_SEARCH_1703_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Type search term
		HeaderWidget headerSection = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
		
		//verify highlighted string
		headerSection.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_1"));
		
		//check for keywords in search pop up
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		
		//perform search by clicking on category
		SubCategoryPage subCat = headerSection.selectSubCategoryKeywordInSearchSuggestion(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		
		//verify search results
		subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_PRODUCT_1"));
		subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_PRODUCT_2"));	
	}
	
	/** Test Case to verify that the number of suggested brands get downsized 
	 * while the user is typing the keyword
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1706() throws Exception 
	{
		dsm.setDataFile(dataFileName);
		
		//Identify which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1706", "testFV2STOREB2C_SOLR_SEARCH_1706_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Type search term - brand (tab)
		HeaderWidget headerSection = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
	
		//verify highlighted string
		headerSection.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_1"));
		
		//check for category keywords in search pop up
		headerSection.verifyCategoryInAutoSuggest(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		headerSection.verifyCategoryInAutoSuggest(dsm.getInputParameter("EXPECTED_KEYWORD_2"));
		headerSection.verifyCategoryInAutoSuggest(dsm.getInputParameter("EXPECTED_KEYWORD_3"));
		
		//continue typing search term - brand (let)
		headerSection = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_2"), false, true);
		
		//verify highlighted string
		headerSection.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_2"));
		
		//check for keywords in search pop up
		headerSection.verifyCategoryInAutoSuggest(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		
		//check for brands that shouldn't be present
		headerSection.verifyKeywordNotInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_2"));
		headerSection.verifyKeywordNotInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_3"));	
	}
	

	/**
	 * Test Case to verify the category auto suggest feature works from other
	 * pages other than the home page
	 * 
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1707() throws Exception 
	{
		dsm.setDataFile(dataFileName);
		
		//Identify which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1707", "testFV2STOREB2C_SOLR_SEARCH_1707_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Type search term
		HeaderWidget headerWidget = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
		
		//verify highlighted string
		headerWidget.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_1"));
		
		//check for keywords in search pop up
		headerWidget.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		
		//perform search
		SubCategoryPage subCat = headerWidget.selectSubCategoryKeywordInSearchSuggestion(dsm.getInputParameter("EXPECTED_KEYWORD_2"));
		
		//verify results
		subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_PRODUCT_1"));
		subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_PRODUCT_2"));
	
		
		//check category auto suggest from department page
		//Type search term
		headerWidget = subCat.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
		
		//verify highlighted string
		headerWidget.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_1"));
		
		//check for keywords in search pop up
		headerWidget.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		
		//perform search
		subCat = headerWidget.selectSubCategoryKeywordInSearchSuggestion(dsm.getInputParameter("EXPECTED_KEYWORD_2"));
		
		//verify results
		subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_PRODUCT_1"));
		subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_PRODUCT_2"));
	
		//select a product and goto product display page
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("EXPECTED_PRODUCT_1"));
		
		//Perform search from product display page
		//Type search term
		headerWidget = productDisplayPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
		
		//verify highlighted string
		headerWidget.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_1"));
		
		//check for keywords in search pop up
		headerWidget.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		
		//perform search
		subCat = headerWidget.selectSubCategoryKeywordInSearchSuggestion(dsm.getInputParameter("EXPECTED_KEYWORD_2"));
		
		//verify results
		subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_PRODUCT_1"));
		subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_PRODUCT_2"));
	}
	

	/**
	 * Test Case To verify that category suggestion navigation and advanced search for the same category all return the same result
	 *  
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1713()  
	{
		dsm.setDataFile(dataFileName);
		
		//Identify which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1713", "testFV2STOREB2C_SOLR_SEARCH_1713_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Type search term
		HeaderWidget headerSection = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
		
		//verify highlighted string
		headerSection.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_1"));
		
		//check for keywords in search pop up
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		
		//perform search
		SubCategoryPage subCat = headerSection.selectSubCategoryKeywordInSearchSuggestion(dsm.getInputParameter("EXPECTED_KEYWORD_2"));
		

		//verify results
		subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_PRODUCT_1"));
		subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_PRODUCT_2"));
		
		//Go to advanced search page
		AdvancedSearchPage advancedsearchpage=subCat.getFooterWidget().goToAdvancedSearchPage();
		
		//Enter search term
		 advancedsearchpage.typeSearchFor(dsm.getInputParameter("SEARCHTERM_2"));
		 
		//Select Department
		//Choose Category
		advancedsearchpage.selectSearchIn(dsm.getInputParameter("CATAGORY"));
		
		//Click search button
		 advancedsearchpage.submitSearchButton().verifyTextPresent(dsm.getInputParameter("EXPECT_RESULT"));
		 
		//Verify results
		 subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_PRODUCT_1"));
		 subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_PRODUCT_2"));
		
	}
	
	
	
	/**
	 * Test Case To verify that storefront can auto suggest categories with multiple words
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1715()  
	{
		dsm.setDataFile(dataFileName);
		
		//Identify which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1715", "testFV2STOREB2C_SOLR_SEARCH_1715_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Type search term
		HeaderWidget headerSection = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
		
		//verify highlighted string
		headerSection.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_1"));
		
		//check for keywords in search pop up
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		
		//perform search
		SubCategoryPage subCat = headerSection.selectSubCategoryKeywordInSearchSuggestion(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		

		//verify results
		subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_PRODUCT_1"));
		subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_PRODUCT_2"));
		
		subCat.getBreadCrumbWidget().goToDepartmentPage(dsm.getInputParameter("DEPTNAME"));
		
	}
	
	/**
	 * Test Case to verify that parent categories can not be displayed in the suggested categories list
	 * 
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1717()  
	{
		dsm.setDataFile(dataFileName);
		
		//Identify which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1717", "testFV2STOREB2C_SOLR_SEARCH_1717_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Type search term
		HeaderWidget headerSection = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
		
		//verify highlighted string
		headerSection.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_1"));
		
		//check for keywords in search pop up
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		
	}
	
	/**
	 * Test Case To verify that performing search with a search term that matches something in the suggested Categories, would execute search with the entered search term
	 * 
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1720()  
	{
		dsm.setDataFile(dataFileName);
		
		//Identify which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1720", "testFV2STOREB2C_SOLR_SEARCH_1720_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Type search term
		HeaderWidget headerSection = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
		
		//verify highlighted string
		headerSection.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_1"));
		
		//check for keywords in search pop up
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		
		//perform search
		SearchResultsPage srp=headerSection.performSearch(dsm.getInputParameter("SEARCHTERM_1"));
		
		//verify the results
		srp.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		
	}
	
	
	/**
	 * Test Case To verify that result from auto-suggest category(top and sub category) is same as category navigation
	 * 
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1725()  
	{
		dsm.setDataFile(dataFileName);
		
		//Identify which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1725", "testFV2STOREB2C_SOLR_SEARCH_1725_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Type search term
		HeaderWidget headerSection = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
		
		//verify highlighted string
		headerSection.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_1"));
		
		//check for keywords in search pop up
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		
		//Select the department name
		SubCategoryPage subCat =headerSection.selectSubCategoryKeywordInSearchSuggestion(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		
		//Verify the product
		subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_2"));
		subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));		
		subCat.getBreadCrumbWidget().goToDepartmentPage(dsm.getInputParameter("DEPARTNAME"));
	
		//GO to Health->Medicine department.
		aurorafrontPage.getHeaderWidget().goToSubCategoryPageByHierarchy(dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME")
				,dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_2"));
		subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		
		
		//Type another search term
		aurorafrontPage=subCat.getHeaderWidget().goToFrontPage();
		aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_2"), true, true);
		
		//check for keywords in search pop up
		headerSection=headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_2"));
		
		//perform search
		subCat = headerSection.selectSubCategoryKeywordInSearchSuggestion(dsm.getInputParameter("EXPECTED_KEYWORD_2"));
		  
		 //Verify the result
		subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_3"));		 
		subCat.getBreadCrumbWidget().goToDepartmentPage(dsm.getInputParameter("DEPARTNAME1"));
		 
		//GO to Home Frunishing->Kitchenware department.
		aurorafrontPage.getHeaderWidget().goToSubCategoryPageByHierarchy(dsm.getInputParameter("TOP_CAT1"), dsm.getInputParameter("SUB_CAT1"));
		subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_3"));
		subCat.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_4"));
		
	}
	
	
}
	