package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2013
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.IBMCopyright;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/** Test scenario to test search history
 *  Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_SOLR_SEARCH_18 extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = IBMCopyright.SHORT_COPYRIGHT;

	//A variable to hold the name of the data file where input parameters can be found.
	//$ANALYSIS-IGNORE
	protected final String dataFileName = "src/data/FV2STOREB2C_SOLR_SEARCH_18_Data.xml";

	@DataProvider
	private final TestDataProvider dsm;
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dsm
	 */
	@Inject
	public FV2STOREB2C_SOLR_SEARCH_18(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dsm)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dsm;
	}
	
	/**
	 * Test Case to test the auto-suggest menu in header shows a history of
	 * typed search terms entered by the user in the auto-suggest window
	 * 
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1801() throws Exception 
	{
		dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1801", "testFV2STOREB2C_SOLR_SEARCH_1801_DATABLOCK");
		
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Type search term
		HeaderWidget headerSection = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
		
		//check for keywords in search pop up
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_2"));
	
		//perform search
		headerSection.performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCHTERM_1"));
		
		//Type search term again
		headerSection.typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
		
		//check for search term in the search history
		headerSection.verifyKeywordInSearchHistory(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
	}
	
	/** Test Case to test that search history is cleared after session terminated
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1804() throws Exception 
	{
		dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1804", "testFV2STOREB2C_SOLR_SEARCH_1804_DATABLOCK");

		//Open store front
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
		//Type search term
		HeaderWidget headerSection = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
		
		//check for keywords in search pop up
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_2"));
	
		//perform search
		@SuppressWarnings("unused")
		SearchResultsPage searchResultsPage = headerSection.performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCHTERM_1"));
		
		AuroraFrontPage aurorafrontPage2 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
	
		//Type search term again
		HeaderWidget headerSection2 = aurorafrontPage2.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
		
		//check for search term in the search history
		headerSection2.verifyHistoryNotPresentInSearchPopup();	
	}
	
	/** Test Case to test that the search history suggestions are ordered by the most recent history
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1806() throws Exception 
	{
		dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1806", "testFV2STOREB2C_SOLR_SEARCH_1806_DATABLOCK");

		//Open store front
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
	
		//Search for a string
		HeaderWidget headerSection = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, false);
		headerSection.executeSearch(SearchResultsPage.class);
		
		//Search for a string
		headerSection.typeSearchTerm(dsm.getInputParameter("SEARCHTERM_2"), true, false);
		headerSection.executeSearch(SearchResultsPage.class);		
		
		//Search for a string
		headerSection.typeSearchTerm(dsm.getInputParameter("SEARCHTERM_3"), true, false);
		headerSection.executeSearch(SearchResultsPage.class);
		
		//Search for a string
		headerSection.typeSearchTerm(dsm.getInputParameter("SEARCHTERM_5"), true, true);

		//Verify if keywords are in search history
		headerSection.verifyKeywordInSearchHistory(dsm.getInputParameter("EXPECTED_HISTORY_KEYWORD_3"));
		headerSection.verifyKeywordInSearchHistory(dsm.getInputParameter("EXPECTED_HISTORY_KEYWORD_2"));
		
		//verify keyword not in search history
		headerSection.verifyKeywordNotInSearchHistory(dsm.getInputParameter("EXPECTED_HISTORY_KEYWORD_1"));
		
		//Search for a string
		headerSection.typeSearchTerm(dsm.getInputParameter("SEARCHTERM_4"), true, false);
		headerSection.executeSearch(SearchResultsPage.class);
		
		//Search for a string
		headerSection.typeSearchTerm(dsm.getInputParameter("SEARCHTERM_5"), true, true);
		
		//Verify if keywords are in search history
		headerSection.verifyKeywordInSearchHistory(dsm.getInputParameter("EXPECTED_HISTORY_KEYWORD_4"));
		headerSection.verifyKeywordInSearchHistory(dsm.getInputParameter("EXPECTED_HISTORY_KEYWORD_3"));
		
		//verify keyword not in search history
		headerSection.verifyKeywordNotInSearchHistory(dsm.getInputParameter("EXPECTED_HISTORY_KEYWORD_2"));
	}
	
	/** Test Case to test that search history can suggest special search terms (i.e. phrases, numeric, etc.)
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1807() throws Exception 
	{
		dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1807", "testFV2STOREB2C_SOLR_SEARCH_1807_DATABLOCK");

		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Search for a string
		HeaderWidget headerSection = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, false);
		headerSection.executeSearch(SearchResultsPage.class);
		
		//Verify if keyword is in search history
		headerSection.typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
		headerSection.verifyKeywordInSearchHistory(dsm.getInputParameter("EXPECTED_HISTORY_KEYWORD_1"));
		headerSection.executeSearch(SearchResultsPage.class);
		//Search for a string
		headerSection.typeSearchTerm(dsm.getInputParameter("SEARCHTERM_2"), true, false);
		headerSection.executeSearch(SearchResultsPage.class);
		
		//Verify if keyword is in search history
		headerSection.typeSearchTerm(dsm.getInputParameter("SEARCHTERM_2"), true, true);
		headerSection.verifyKeywordInSearchHistory(dsm.getInputParameter("EXPECTED_HISTORY_KEYWORD_2"));
		headerSection.executeSearch(SearchResultsPage.class);

		//Search for a string
		headerSection.typeSearchTerm(dsm.getInputParameter("SEARCHTERM_3"), true, true);
		headerSection.executeSearch(SearchResultsPage.class);
		
		//Verify if keyword is in search history
		headerSection.typeSearchTerm(dsm.getInputParameter("SEARCHTERM_3"), true, true);
		headerSection.verifyKeywordInSearchHistory(dsm.getInputParameter("EXPECTED_HISTORY_KEYWORD_3"));
		headerSection.executeSearch(SearchResultsPage.class);
		
		//Search for a string
		headerSection.performSearch(SearchResultsPage.class, ""); //Blank search inserted
		headerSection.typeSearchTerm(dsm.getInputParameter("SEARCHTERM_4"), true, false);
		headerSection.executeSearch(SearchResultsPage.class);

		//Verify if keyword is in search history
		headerSection.typeSearchTerm(dsm.getInputParameter("SEARCHTERM_4_1"), true, true);
		headerSection.verifyKeywordInSearchHistory(dsm.getInputParameter("EXPECTED_HISTORY_KEYWORD_4"));
	}
	
	/**
	 * Test Case to test that search history suggestions works from all pages in
	 * store front
	 * 
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1810() throws Exception 
	{
		dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1810", "testFV2STOREB2C_SOLR_SEARCH_1810_DATABLOCK");

		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Search for a string
		HeaderWidget headerSection = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, true);
		@SuppressWarnings("unused")
		SearchResultsPage searchResultsPage = headerSection.executeSearch(SearchResultsPage.class);
		
		//Search for a string
		headerSection.typeSearchTerm(dsm.getInputParameter("SEARCHTERM_2"), true, true);
		searchResultsPage = headerSection.executeSearch(SearchResultsPage.class);
		
		//Type a search term in search term input field
		headerSection.typeSearchTerm(dsm.getInputParameter("SEARCHTERM_3"), true, true);
		
		//Verify if previous keyword is in search history
		headerSection.verifyKeywordInSearchHistory(dsm.getInputParameter("SEARCHTERM_2"));
		
		//search a string to bring us to a product details page
		headerSection.performSearch(SearchResultsPage.class, dsm.getInputParameter("KEYWORD_1"));
		
		//Type a search term in search term input field
		headerSection.typeSearchTerm(dsm.getInputParameter("SEARCHTERM_3"), true, true);
		
		//Verify if previous keyword is in search history
		headerSection.verifyKeywordInSearchHistory(dsm.getInputParameter("SEARCHTERM_1"));
		
		//Verify if previous keyword is in search history
		headerSection.verifyKeywordInSearchHistory(dsm.getInputParameter("SEARCHTERM_2"));
		
		//Select a keyword from the search pop up window
		headerSection.selectProductKeywordInSearchSuggestion(dsm.getInputParameter("KEYWORD_2"));
		
		//click shopping cart
		headerSection.goToShoppingCartPage();
		
		//Type a search term in search term input field
		headerSection.typeSearchTerm(dsm.getInputParameter("SEARCHTERM_3"), true, true);

	
		//Verify if previous keyword is in search history
		headerSection.verifyKeywordInSearchHistory(dsm.getInputParameter("SEARCHTERM_2"));
		
		//Verify if previous keyword is in search history
		headerSection.verifyKeywordInSearchHistory(dsm.getInputParameter("SEARCHTERM_3"));	
	}
	
	/** Test Case to test long strings can be displayed properly in search history
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1812() throws Exception 
	{
		dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1812", "testFV2STOREB2C_SOLR_SEARCH_1812_DATABLOCK");

		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Search for a string
		HeaderWidget headerSection = aurorafrontPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_1"), true, false);
		@SuppressWarnings("unused")
		SearchResultsPage searchResultsPage = headerSection.executeSearch(SearchResultsPage.class);
		
		//Search for a string
		headerSection.typeSearchTerm(dsm.getInputParameter("SEARCHTERM_2"), true, true);
		
		//Verify if previous keyword is in search history
		headerSection.verifyKeywordInSearchHistory(dsm.getInputParameter("EXPECTED_HISTORY_KEYWORD"));
		
	}
		
	
}