package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 * Scenario: FV2STOREB2C_59
 * Details: BazaarVoice Detailed Reviews
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_59 extends AbstractAuroraSingleSessionTests
{
	static String productName = null;
		
	@DataProvider
	private final TestDataProvider dsm;
	private final CMC cmc;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param cmc CMC object
	 */
	@Inject
	public FV2STOREB2C_59(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,CMC cmc)
	{
		super(log, wcWebTestRule, caslTestRule);
		
		this.dsm = dataSetManager;
		this.cmc = cmc;
	}
	
	 /**
	 * Set the environment up ensuring Rating & Reviews feature is enabled. 
	 * 
	 * @throws Exception 
	 */
	@Before
	 public void setUp() throws Exception{		
		productName = dsm.getInputParameter("PRODUCTNAME");
		String cmcUserID = dsm.getInputParameter("CMCLOGONID");
		String cmcPassword = dsm.getInputParameter("CMCPASSWORD");
		bazaarvoiceFlexFlowOption(cmcUserID,cmcPassword,true);
	}
	
	 /**
	 *  Clean up. Example disable Rating & Reviews feature.
	 * 
	 * @throws Exception 
	 */
	@After
	 public void tearDown() throws Exception{
		String cmcUserID = dsm.getInputParameter("CMCLOGONID");
		String cmcPassword = dsm.getInputParameter("CMCPASSWORD");
		bazaarvoiceFlexFlowOption(cmcUserID,cmcPassword,false);
	 }
	
		
	/**
	 * To verify that the reviews list renders properly on a product page.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_5901()
	{
		//wait for 20 minutes before check for new review as it takes that much for Bazaarvoice to consume.
		//Can skip this wait if you know the review already exists on page.
		try {
			Thread.sleep(1200000);
		} catch (InterruptedException e) {
			getLog().log(Level.WARNING, "Got interrupted while waiting for review to be available from BazaarVoice.", e);		
			e.printStackTrace();
		}	
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		SearchResultsPage searchResults = frontPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));			
		try {
			searchResults.getCatalogEntryListWidget().goToProductPageByName(productName).verifyReviewPresent(dsm.getInputParameter("REVIEW_TITLE"));
		} catch (Exception e) {
			searchResults.getCatalogEntryListWidget().goToProductPageByName(productName).verifyReviewPresent(dsm.getInputParameter("REVIEW_TITLE"));;
		}
	}

	/**
	 * Enable or disable Bazaarvoice flexflow option.
	 * 
	 * @param cmcUserid CMC Administrator userid.
	 * @param cmcPasswqord CMC Administrator password 
	 * @param enable whether to enable or disable the feature 
	 * @throws Exception 
	 */
	public void bazaarvoiceFlexFlowOption(String cmcUserid, String cmcPasswqord, Boolean enable) throws Exception
	{
		String[] featureName = {"RatingReviewIntegration"};
				
		cmc.logon(cmcUserid, cmcPasswqord);
		if(enable){
			cmc.modifyChangeFlowOptions(featureName, null);	
		}else{
			cmc.modifyChangeFlowOptions(null,featureName);
		}
		
		cmc.logoff();
			
	}

}
