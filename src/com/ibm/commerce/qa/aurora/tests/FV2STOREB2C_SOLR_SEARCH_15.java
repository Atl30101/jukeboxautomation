package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2013
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.IBMCopyright;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/**
 * Test scenario to test shopper can use keyword search in the articles to help
 * making a shopping decision for a specific product or specific category in
 * keyword. Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_SOLR_SEARCH_15 extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = IBMCopyright.SHORT_COPYRIGHT;

	//A variable to hold the name of the data file where input parameters can be found.
	//$ANALYSIS-IGNORE
	protected final String dataFileName = "src/data/FV2STOREB2C_SOLR_SEARCH_15_Data.xml";

	@DataProvider
	private final TestDataProvider dsm;
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dsm
	 */
	@Inject
	public FV2STOREB2C_SOLR_SEARCH_15(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dsm)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dsm;
	}
	
	/**
	 * Test case to verify the Articles auto suggest feature works from other
	 * pages other than the home page
	 * 
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_1503() throws Exception 
	{
		dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1503", "testFV2STOREB2C_SOLR_SEARCH_1503_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Perform search to navigate to a page other than homepage
		SearchResultsPage searchResultsPage = aurorafrontPage.getHeaderWidget().performSearch(SearchResultsPage.class,dsm.getInputParameter("SEARCHTERM_1"));
		
		//Type 2nd search term
		HeaderWidget headerSection = searchResultsPage.getHeaderWidget().typeSearchTerm(dsm.getInputParameter("SEARCHTERM_2"), true, true);

		//Verify some keywords are in search pup up window
		headerSection.verifyKeywordInSearchPopup(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		
		//Verify the search term entered is highlighted in search pop up window.
		headerSection.verifyHighlightedKeyword(dsm.getInputParameter("EXPECTED_HIGHLIGHTED_STRING_1"));
		
		//Perform 2nd search
		searchResultsPage = headerSection.selectProductKeywordInSearchSuggestion(dsm.getInputParameter("EXPECTED_KEYWORD_1"));
		
		//check for results
		searchResultsPage.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		
		
	}
	
	/**
	 * Perform tear down operations such as stopping the test harness.
	 * @throws Exception
	 *//*
	public void tearDown() throws Exception
	{
		//Stops the test server.
		super.tearDown();
	}*/
}