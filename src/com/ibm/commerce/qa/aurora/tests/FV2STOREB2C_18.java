package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

//Import the task libraries for use in this test script.
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.foundation.common.datatypes.AddressUsageEnumerationType;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.MemberFixture;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.casl.fixtures.databuilders.IAddressBuilder;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/** 
 * Scenario FV2STOREB2C_18
 * This objective of this test scenario is to test shopper updating information from Shipping and Billing
 * Display page (checkout function).
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_18 extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

	@DataProvider(apolloDSM=true)
	
	//A Variable to retrieve data from the data file.
	private final TestDataProvider dsm;
	
	private final CMC cmc;
	
	//A Variable to determine if the address book needs to be removed in tear down.
	private boolean addressBookCleanup = false;
	
	//A Variable to determine if the promotion needs to be removed in tear down.
	private boolean promotionCleanup = false;
	
	//The promotion Id of the created promotion.
	private String promotionId;

	private CaslFixturesFactory f_CaslFixtures;

	private IAddressBuilder f_addressBuilder;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param cmc 
	 * 			   object to perform CMC tasks
	 * @param p_CaslFixtures 
	 * @param p_addressBuilder 
	 */
	@Inject
	public FV2STOREB2C_18(Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CMC cmc,
			CaslFixturesFactory p_CaslFixtures,
			IAddressBuilder p_addressBuilder){
		
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		this.cmc = cmc;
		f_CaslFixtures = p_CaslFixtures;
		f_addressBuilder = p_addressBuilder;
	}
	
	/**
	 * Helper method to create address book entry.
	 * 
	 */
	public void createAddressBookEntrySetup()
	{
		MemberFixture member = f_CaslFixtures.createMemberFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		//Enter new address information
		f_addressBuilder.selectAddressType(AddressUsageEnumerationType.SHIPPING_AND_BILLING_LITERAL)
			.withFirstName(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
			.withLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
			.withStreetAddress(dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
			.withCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
			.withCountry(dsm.getInputParameter("ADDRESS_BOOK_COUNTRY"))
			.withState(dsm.getInputParameter("ADDRESS_BOOK_STATE"))
			.withZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"));
		
		member.createAddress(dsm.getInputParameter("ADDRESS_BOOK_RECIPIENT"), f_addressBuilder.build(), dsm.getInputParameter("ADDRESS_BOOK_EMAIL"));
		
		//Indicate that address must be removed at tear down
		addressBookCleanup = true;
	}
	
	/**
	 * Helper method to execute the sign in action for each test case.
	 * 
	 * @return a new my account page object.
	 */
	public MyAccountMainPage executeSignIn()
	{
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Open Sign In/Register page
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter account User Name/Password and sign in
		MyAccountMainPage accountPage = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD")).signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		return accountPage;
	}
	
	/** 
	 * Test case to Add an item to the cart from the Shipping and Billing Display page and complete
	 * the check out flow.
	 * 
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_1801() {
		//add product to cart
		OrdersFixture order = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		
		order.removeAllItemsFromCart();
		
		order.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY", Double.class));
		
		//Execute user login
		MyAccountMainPage accountPage = executeSignIn();
		
		
		//Get the header section object.
		HeaderWidget headerSection = accountPage.getHeaderWidget();
		
		//Go to Shopping Cart page from Header.
		ShopCartPage shopCartPage = headerSection.goToShoppingCartPage();
		
		//Verify shopping cart subtotal.
		headerSection.verifyMiniCartTotalPrice(dsm.getInputParameter("MINI_CART_TOTAL"));
		
		//Clicks on 'Checkout' button on Shop Cart Page.
		ShippingAndBillingPage shippingAndBillingPage = shopCartPage.continueToNextStep();
		
		//Verify the default shipping method is selected.
		shippingAndBillingPage.verifyShippingMethod(dsm.getInputParameter("EXPECTED_SHIPPING_METHOD"))
		
		//Verify the ship as complete option is selected.
		.verifyShipAsCompleteIsChecked()
		
		//Verify the previously added item is in the shop cart.
		.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"))
		
		//Verify the default number of payment methods is selected.
		.verifyNoOfPaymentMethods(dsm.getInputParameter("DEFAULT_NUMBER_OF_PAYMENT_METHODS"))
		
		//Selects Billing Method from drop down list on shipping billing page e.g 'Pay Later'.
		.selectPaymentMethod(dsm.getInputParameter("BILLING_METHOD"));
		
		//Clicks on 'Next' button link on Shipping and Billing Page.
		OrderSummarySingleShipPage orderSummaryPage = shippingAndBillingPage.singleShipNext();
		
		//Verify the item quantity on quick order page.
		orderSummaryPage.verifyItemQty(dsm.getInputParameter("PRODUCT_ITEM_POSITION"), dsm.getInputParameter("PRODUCT_QUANTITY"))
		
		//Verify the order total.
		.verifyOrderTotal(dsm.getInputParameter("SUBTOTAL"))
		
		//Verifies Billing Method on order summary page.
		.verifyBillingMethod(dsm.getInputParameter("EXPECTED_BILLING_METHOD"), dsm.getInputParameter("PAYMENT_NUMBER"));
	
		//Clicks on 'Order' button on Order Summary Page.
		OrderConfirmationPage orderConfirmationPage = orderSummaryPage.completeOrder();
		
		//Verifies the order Confirmation.
		orderConfirmationPage.verifyOrderSuccessful(dsm.getInputParameter("ORDER_SUCCESSFUL_MESSAGE"));
	}
	
	/** 
	 * Test case to remove an item from the shipping and billing page and complete the checkout flow.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_1802() {
		
		//add product to cart
		OrdersFixture order = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		order.removeAllItemsFromCart();
		
		order.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY", Double.class));
		
		//Execute user login.
		MyAccountMainPage accountPage = executeSignIn();
		
		//Get the header section object
		HeaderWidget headerSection = accountPage.getHeaderWidget();
		
		//Clicks on 'Shopping Cart' link on header
		ShopCartPage shopCartPage = headerSection.goToShoppingCartPage();
		
		//Clicks on 'Checkout' button on Shop Cart Page
		ShippingAndBillingPage shippingAndBilling =  shopCartPage.continueToNextStep();
		
	    //Clicks on product image on Shipping Billing Page
		ProductDisplayPage productDisplayPage = shippingAndBilling.goToProductPageByImagePosition(Integer.valueOf(dsm.getInputParameter("PRODUCT_IMAGE_POSITION_NUMBER_IN_CART")));
	    
	    //Clicks on add to cart button on product display page
		productDisplayPage.addToCart();

		//Get the header section object
		headerSection = productDisplayPage.getHeaderWidget();
		
		//Clicks on 'Shopping Cart' link on header.
		shopCartPage = headerSection.goToShoppingCartPage();
		
		//Verify shopping cart subtotal
		headerSection.verifyMiniCartTotalPrice(dsm.getInputParameter("SECOND_SUBTOTAL"));
		
		//Clicks on 'Checkout' button on Shop Cart Page.
		shippingAndBilling = shopCartPage.continueToNextStep();
		
		//Clicks on product image on Shipping Billing Page.
		shippingAndBilling.removeItem(dsm.getInputParameter("REMOVE_PRODUCT_POSITION_NUMBER_IN_CART"));
		
		//verify if item is removed from shopping cart.
		shippingAndBilling.getHeaderWidget().verifyMessage(dsm.getInputParameter("MESSAGE_ITEM_REMOVED_FROM_SHOPCART"));
		
		//Verify the default shipping method is selected.
		shippingAndBilling.verifyShippingMethod(dsm.getInputParameter("EXPECTED_SHIPPING_METHOD"))
			
		//Verify the ship as complete option is selected.
		.verifyShipAsCompleteIsChecked()
				
		//Verify the previously added item is in the shop cart.
		.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"))
				
		//Verify the default number of payment methods is selected.
		.verifyNoOfPaymentMethods(dsm.getInputParameter("DEFAULT_NUMBER_OF_PAYMENT_METHODS"))
				
		//Selects Billing Method from drop down list on shipping billing page e.g 'Pay Later'.
		.selectPaymentMethod(dsm.getInputParameter("BILLING_METHOD"));
		
		//Clicks on 'Next' button link on Shipping and Billing Page.
		OrderSummarySingleShipPage orderSummaryPage = shippingAndBilling.singleShipNext();
		
		//Verify the item quantity on quick order page.
		orderSummaryPage.verifyItemQty(dsm.getInputParameter("PRODUCT_IMAGE_POSITION_NUMBER_IN_CART"), dsm.getInputParameter("PRODUCT_QUANTITY"))
		
		//Verify the order total.
		.verifyOrderTotal(dsm.getInputParameter("FIRST_SUBTOTAL"))
		
		//Verifies Billing Method on order summary page.
		.verifyBillingMethod(dsm.getInputParameter("EXPECTED_BILLING_METHOD"), dsm.getInputParameter("PAYMENT_NUMBER"));
		
		//Clicks on 'Order' button on Order Summary Page.
		OrderConfirmationPage orderConfirmationPage = orderSummaryPage.completeOrder();
		
		//Verifies the order Confirmation.
		orderConfirmationPage.verifyOrderSuccessful(dsm.getInputParameter("ORDER_SUCCESSFUL_MESSAGE"));
	}
	
	/**
	 * Test case to Remove all items from the shipping and billing page.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_1803() {
		
		//add product to cart
		OrdersFixture order = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		order.removeAllItemsFromCart();
		
		order.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY", Double.class));
		
		//Execute user login.
		MyAccountMainPage accountPage = executeSignIn();

		
		//Get the header section object
		HeaderWidget headerSection = accountPage.getHeaderWidget();
		
		//Clicks on 'Shopping Cart' link on header.
		ShopCartPage shopCartPage = headerSection.goToShoppingCartPage();
		
		//Clicks on 'Checkout' button on Shop Cart Page.
		ShippingAndBillingPage shippingAndBillingPage = shopCartPage.continueToNextStep();
		
	    //Clicks on product image on Shipping Billing Page.
		ProductDisplayPage productDisplayPage = shippingAndBillingPage.goToProductPageByImagePosition(Integer.valueOf(dsm.getInputParameter("PRODUCT_IMAGE_POSITION_NUMBER_IN_CART")));
	    
	    //Clicks on add to cart button on product display page. 
		productDisplayPage.addToCart();

		//Get the header section object
		headerSection = productDisplayPage.getHeaderWidget();
		
		//Clicks on 'Shopping Cart' link on header.
		shopCartPage = headerSection.goToShoppingCartPage();
		
		//Verify shopping cart subtotal.
		headerSection.verifyMiniCartTotalPrice(dsm.getInputParameter("SECOND_SUBTOTAL"));
		
		//Clicks on 'Checkout' button on Shop Cart Page.
		shippingAndBillingPage = shopCartPage.continueToNextStep();
		
		//Removes all items from cart in Shipping Billing Page.
		shippingAndBillingPage.removeAllItems();
		
		//CLick the shopping cart link in the header
		shopCartPage = headerSection.goToShoppingCartPage();
		
		//verify shopping cart is empty
		shopCartPage.verifyShoppingCartIsEmpty();
	}
	
	/**
	 * Test case to ensure Shopper can update the shipping address from the Shipping and Billing Display page
	 * and complete checkout flow.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_1804() {
		
		
		createAddressBookEntrySetup();
		
		//add product to cart
		OrdersFixture order = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		order.removeAllItemsFromCart();
		
		order.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY", Double.class));
		
		//Open the store in the browser

		//Execute user login.
		MyAccountMainPage accountPage = executeSignIn();
		
		//Get the header section object
		HeaderWidget headerSection = accountPage.getHeaderWidget();
		
		//Clicks on 'Shopping Cart' link on header
		ShopCartPage shopCartPage = headerSection.goToShoppingCartPage();
		
		//Clicks on 'Checkout' button on Shop Cart Page.
		ShippingAndBillingPage shippingAndBillingPage = shopCartPage.continueToNextStep();
		
	    //Select shipping address from drop down list in Shipping Billing Page.
	    shippingAndBillingPage.selectShippingAddress(dsm.getInputParameter("SHIPPING_ADDRESS_LABEL"))
	    
	    //Select billing address from drop down list in Shipping Billing Page.
	    .selectBillingAddressByName(dsm.getInputParameter("BILLING_ADDRESS_LABEL"))
	    
	    //clicks on edit address link in Shipping Billing Page.
	    .editShippingAddress()
	  //Confirm ship and bill info page is loaded completely
		.verifyIsNotOnShipBillPage()
		.typePhone(dsm.getInputParameter("UPDATE_PHONE")).submitSuccessfulForm()
	    
    	//select billing method from drop down list in Shipping Billing Page.
	    .selectPaymentMethod(dsm.getInputParameter("BILLING_METHOD"))
	    
	    //Verify the item exist on shipping and billing page.
	    .verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"))
	    
	    //Verify the billing address profile detail.
		.verifyBillingAddressDetails(dsm.getInputParameter("FIRST_LAST_NAME_POSITION"), dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME") + " " + dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
		.verifyBillingAddressDetails(dsm.getInputParameter("STREET_ADDRESS_POSITION"), dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
		.verifyBillingAddressDetails(dsm.getInputParameter("CITY_STATE_POSITION"), dsm.getInputParameter("ADDRESS_BOOK_CITY") + " " + dsm.getInputParameter("ADDRESS_BOOK_STATE"))
		.verifyBillingAddressDetails(dsm.getInputParameter("COUNTRY_ZIPCODE_POSITION"), dsm.getInputParameter("ADDRESS_BOOK_COUNTRY") + " " + dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
		.verifyBillingAddressDetails(dsm.getInputParameter("PHONE_POSITION"), dsm.getInputParameter("UPDATE_PHONE"))
		.verifyBillingAddressDetails(dsm.getInputParameter("EMAIL_POSITION"), dsm.getInputParameter("ADDRESS_BOOK_EMAIL"))
		
		//Verify the shipping address profile details.
		.verifyShippingAddressDetails(dsm.getInputParameter("FIRST_LAST_NAME_POSITION"), dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME") + " " + dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
		.verifyShippingAddressDetails(dsm.getInputParameter("STREET_ADDRESS_POSITION"), dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
		.verifyShippingAddressDetails(dsm.getInputParameter("CITY_STATE_POSITION"), dsm.getInputParameter("ADDRESS_BOOK_CITY") + " " + dsm.getInputParameter("ADDRESS_BOOK_STATE"))
		.verifyShippingAddressDetails(dsm.getInputParameter("COUNTRY_ZIPCODE_POSITION"), dsm.getInputParameter("ADDRESS_BOOK_COUNTRY") + " " + dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
		.verifyShippingAddressDetails(dsm.getInputParameter("PHONE_POSITION"), dsm.getInputParameter("UPDATE_PHONE"))
		.verifyShippingAddressDetails(dsm.getInputParameter("EMAIL_POSITION"), dsm.getInputParameter("ADDRESS_BOOK_EMAIL"));
	    
	    //Clicks on 'Next' button link on Shipping and Billing Page.
	    OrderSummarySingleShipPage orderSummaryPage = shippingAndBillingPage.singleShipNext();
		
		//Verifies Billing Method on order summary page.
	    orderSummaryPage.verifyBillingMethod(dsm.getInputParameter("EXPECTED_BILLING_METHOD"), dsm.getInputParameter("PAYMENT_NUMBER"));
		
		//Clicks on 'Order' button on Order Summary Page.
	    OrderConfirmationPage orderConfirmationPage = orderSummaryPage.completeOrder();
		
		//Verifies the order Confirmation.
	    orderConfirmationPage.verifyOrderSuccessful(dsm.getInputParameter("ORDER_SUCCESSFUL_MESSAGE"));
		
	}
	
	/** 
	 * Test case to ensure Shopper can update the shipping method from the Shipping and Billing Display page.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_1805() {
		//add product to cart
		OrdersFixture order = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		
		order.removeAllItemsFromCart();
		
		order.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY", Double.class));
		
		
		//Execute user login.
		MyAccountMainPage accountPage = executeSignIn();
				
		
		//Get the header section object.
		HeaderWidget headerSection = accountPage.getHeaderWidget();
		
		//Clicks on 'Shopping Cart' link on header.
		ShopCartPage shopCartPage = headerSection.goToShoppingCartPage();
		
		//Clicks on 'Checkout' button on Shop Cart Page.
		ShippingAndBillingPage shippingAndBillingPage = shopCartPage.continueToNextStep();
		
		//Verify the default shipping method is selected.
		shippingAndBillingPage.verifyShippingMethod(dsm.getInputParameter("INITIAL_EXPECTED_SHIPPING_METHOD"))
		
		//Verify the ship as complete option is selected.
		.verifyShipAsCompleteIsChecked()
		
		//Verify the previously added item is in the shop cart.
		.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"))
		
		//Verify the default number of payment methods is selected.
		.verifyNoOfPaymentMethods(dsm.getInputParameter("DEFAULT_NUMBER_OF_PAYMENT_METHODS"))
		
	    //Select shipping method from drop down list in Shipping Billing Page.
	    .selectShippingMethod(dsm.getInputParameter("SHIPPING_METHOD"))
	    
    	//select billing method from drop down list in Shipping Billing Page.
	    .selectPaymentMethod(dsm.getInputParameter("BILLING_METHOD"));
	    
	    //Clicks on 'Next' button link on Shipping and Billing Page.
		OrderSummarySingleShipPage orderSummaryPage = shippingAndBillingPage.singleShipNext();
		
		//Verifies Shipping method on order summary page.
		orderSummaryPage.verifyShippingMethod(dsm.getInputParameter("EXPECTED_SHIPPING_METHOD"))
		
		//Verifies Billing Method on order summary page.
		.verifyBillingMethod(dsm.getInputParameter("EXPECTED_BILLING_METHOD"), dsm.getInputParameter("PAYMENT_NUMBER"))
		
		//Verify the item quantity on quick order page.
		.verifyItemQty(dsm.getInputParameter("PRODUCT_ITEM_POSITION"), dsm.getInputParameter("PRODUCT_QUANTITY"))
		
		//Verify the order total.
		.verifyOrderTotal(dsm.getInputParameter("AFTER_SHIPPING_METHOD_CHANGE_SUBTOTAL"));
		
		//Clicks on 'Order' button on Order Summary Page.
		OrderConfirmationPage orderConfirmationPage = orderSummaryPage.completeOrder();
		
		//Verifies the order Confirmation.
		orderConfirmationPage.verifyOrderSuccessful(dsm.getInputParameter("ORDER_SUCCESSFUL_MESSAGE"));
	}
	
	
	/**
	 * Tear down ran after every test case
	 */
	@After
	public void tearDown()
	{
		try
		{
			OrdersFixture order = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
			order.removeAllItemsFromCart();
			order.deletePaymentMethod();
			
			if(addressBookCleanup == true){
				
				MemberFixture member = f_CaslFixtures.createMemberFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
				
				member.deleteAddress(dsm.getInputParameter("ADDRESS_BOOK_RECIPIENT"));
				
				
			}
			
			if(promotionCleanup == true)
			{

				//Log into cmc
				dsm.setDataBlock("CMClogon");
				cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
				cmc.setLocale(dsm.getInputParameter("locale"));
				cmc.setTimeZone("America/Montreal");
				
				//Select the store to work on
				cmc.selectStore();

				//Deactivate and delete promotion
				cmc.deactivatePromotion(promotionId);
				cmc.deletePromotion(promotionId);
			}
			promotionCleanup = false;
			addressBookCleanup = false;
					}
		catch(Exception e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

	}
	
	
}
