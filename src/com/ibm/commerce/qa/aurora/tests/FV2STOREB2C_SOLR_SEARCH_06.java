package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.IBMCopyright;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;





//Declares a new test scenario called FV2STOREB2C_SOLR_SEARCH_01 which contains one or more test case methods.
//All test scenario classes must extend StoreModelTestCase.

/**
 * Scenario: FV2STOREB2C_SOLR_SEARCH_06 Objective: To specify a search term in the search box
 * Pre-requisites: Tester knows the Store URL
 * (http://<hostname>/webapp/wcs/stores/servlet/en/auroraesite) 
 * Search based navigation is enabled
 *  Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_SOLR_SEARCH_06 extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = IBMCopyright.SHORT_COPYRIGHT;

	//A variable to hold the name of the data file where input parameters can be found.
	//$ANALYSIS-IGNORE
	protected final String dataFileName = "data/FV2STOREB2C_SOLR_SEARCH_06_Data.xml";

	@DataProvider
	private final TestDataProvider dsm;
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dsm
	 */
	@Inject
	public FV2STOREB2C_SOLR_SEARCH_06(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dsm)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dsm;
	}
	
///////////////////////////////////////////////////////
	/** Test case to test Customer sorts search results based on Name
	 * Post conditions: The search is re_initiated using the new sorting rule, products are sorted alphabetically by name, and displays page 1 of the results.
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_0604() throws Exception {
		dsm.setDataFile(dataFileName);
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_0604", "testFV2STOREB2C_SOLR_SEARCH_0604_DATABLOCK");
		
		// Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		// search for the word you typed
		SearchResultsPage searchResults = frontPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCHTERM_1"));
				
		//verify the results are present
		searchResults.verifyTextPresent(dsm.getInputParameter("SEARCHTERM_1"));		
		//specify sorting method
		searchResults.getCatalogEntryListWidget().sortBy(dsm.getInputParameter("SORTMETHOD"));
		//verify the results are arranged correctly
		searchResults.getCatalogEntryListWidget().verifyProductPresentInPosition(dsm.getInputParameter("prod0"), 0);
		searchResults.getCatalogEntryListWidget().verifyProductPresentInPosition(dsm.getInputParameter("prod1"), 1);
		searchResults.getCatalogEntryListWidget().verifyProductPresentInPosition(dsm.getInputParameter("prod2"), 2);
		searchResults.getCatalogEntryListWidget().verifyProductPresentInPosition(dsm.getInputParameter("prod3"), 3);
		searchResults.getCatalogEntryListWidget().verifyProductPresentInPosition(dsm.getInputParameter("prod4"), 4);
		searchResults.getCatalogEntryListWidget().verifyProductPresentInPosition(dsm.getInputParameter("prod5"), 5);
		searchResults.getCatalogEntryListWidget().verifyProductPresentInPosition(dsm.getInputParameter("prod6"), 6);
	}	
	
}	