package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2013
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.DepartmentPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.FacetNavigationWidget;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.constants.ChangeFlowOptions;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.StoreManagement;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/** Test scenario to Deep/shallow Search
 *  Refer to each test case for a detailed use case description
 */

@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_73 extends AbstractAuroraSingleSessionTests 
{		
	//A Variable to retrieve data from the data file.
		@DataProvider
		private final TestDataProvider dsm;
	
		private final String ESITE_STORE_NAME = getConfig().getCustomProperty("EXTENDED_SITES_STORE_NAME");
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dataSetManager 
	 * @param storeManagement 
	 * @param changeFlowOptions 
	 */
	@Inject
	public FV2STOREB2C_73(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			StoreManagement storeManagement,
			ChangeFlowOptions changeFlowOptions
			)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
	}
	
	/**
	 * setup method to enable Expanded Category Navigation
	 * @throws Exception 
	 */
	@Before
	public void enableExpandedCategoryNavigation() throws Exception {
		
		
//		storeManagement.modifyChangeFlowOptions(null,new String[] { changeFlowOptions.getExpandedCategoryNavigation()},  ESITE_STORE_NAME);
		
	}
	/** Test case to test Customer chooses Featured facets section for Deep-search navigation
		 * Post conditions: The clearance product is displayed   
		 * @throws Exception
	 */
	@Test
	public void testFV2STORE_B2C_73_1() throws Exception 
	{	
		//Open store
		AuroraFrontPage auroraFrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
		//Go to Category page		
		DepartmentPage dp=auroraFrontPage.getHeaderWidget().goToDepartmentByName(DepartmentPage.class, dsm.getInputParameter("DEPARTMENTNAME"));
			
		//click on clearance
		dp.getFacetNavigationWidget().chooseFacet(dsm.getInputParameter("CLEARANCE"));
		
		//Verify clearance product
		dp.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		dp.getCatalogEntryListWidget().verifyProductIsNotPresent(dsm.getInputParameter("EXPECTED_RESULT_2"));
		
	}
	/** Test case to test Customer chooses Featured facets section for Shallow-search navigation
	 * Post conditions: The clearance product is displayed   
	 * @throws Exception
	*/
	@Test
	public void testFV2STORE_B2C_73_2() throws Exception 
	
	{
		//Open store
		AuroraFrontPage auroraFrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
		//Go to department category	
		CategoryPage subCatPage=	auroraFrontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"),dsm.getInputParameter("SUB_CAT1"))
		.getCategoryNavigationWidget().goToCategoryLink(dsm.getInputParameter("SUB_CAT2"), CategoryPage.class );
			
		//Click on clearance product
		subCatPage.getFacetNavigationWidget().addNewFilter(dsm.getInputParameter("CLEARANCE"));
		 
		//Verify clearance product
		subCatPage.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		subCatPage.getCatalogEntryListWidget().verifyProductIsNotPresent(dsm.getInputParameter("EXPECTED_RESULT_2"));
				
	}

	/** Test case to test Breadcrumb tail for Deep-search navigation
	 * Post conditions: The Breadcrumb  is displayed   
	 * @throws Exception
	*/
	@Test
	public void testFV2STORE_B2C_73_3() throws Exception 
	
	{
	
		//Open store
		AuroraFrontPage auroraFrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
		//Go to department category	 and validate breadcrumb
		DepartmentPage departmentpage=auroraFrontPage.getHeaderWidget().goToDepartmentByName(DepartmentPage.class, dsm.getInputParameter("TOP_CAT")).getFacetNavigationWidget().chooseFacet(dsm.getInputParameter("CLEARANCE")).getParentPage(DepartmentPage.class)
		.getCategoryNavigationWidget().goToCategory(dsm.getInputParameter("SUB_CAT1")).getCategoryNavigationWidget().goToCategory(dsm.getInputParameter("SUB_CAT2"));
		departmentpage.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		
		//Verify the BreadCrumb
		String pos=dsm.getInputParameter("POS");
		int position=Integer.parseInt(pos);
		departmentpage.getBreadCrumbWidget().goToDepartmentPageByPosition(position);
		departmentpage.verifyCategoryName(dsm.getInputParameter("EXPECTED_RESULT_2"));
		
		
		//Click sub category link from breadcrumb
		departmentpage=departmentpage.getHeaderWidget().goToDepartmentByName(DepartmentPage.class, dsm.getInputParameter("TOP_CAT")).getFacetNavigationWidget().chooseFacet(dsm.getInputParameter("CLEARANCE")).getParentPage(DepartmentPage.class)
		.getCategoryNavigationWidget().goToCategory(dsm.getInputParameter("SUB_CAT1")).getCategoryNavigationWidget().goToCategory(dsm.getInputParameter("SUB_CAT2"));
		departmentpage.getBreadCrumbWidget().goToDepartmentPage(dsm.getInputParameter("CAT_NAME"));
		departmentpage.verifyCategoryName(dsm.getInputParameter("CAT_NAME"));
		
	
			
	}


	/** Test case to test Customer chooses  featured facets to filter by
	 * Post conditions: The clearance/Exclusive product is displayed  based on filter type  
	 * @throws Exception
	*/
	@Test
	public void testFV2STORE_B2C_73_4() throws Exception 
	
	{
	
		//Open store
		AuroraFrontPage auroraFrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
		
		//search for a keyword
		SearchResultsPage srp=auroraFrontPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		
		//Select clearance
		FacetNavigationWidget fc=srp.getFacetWidget().addNewFilter(dsm.getInputParameter("CLEARANCE"));
			
		//Verify product in clearance
		srp.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		
		
		//select Exclusive 
		srp.getFacetWidget().addNewFilter(dsm.getInputParameter("EXCLUSIVE"));
		
		//Verify product in Exclusive
		srp.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_2"));
		srp.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_3"));
		
		//Remove Exclusive filter
		fc.cancelLastFilter();
		
		//Verify clearance product is displayed
		srp.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_4"));
		srp.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_5"));
		
		//Remove all filters
		fc.cancelAllFilters();
	
	
	
	}
	
	
}