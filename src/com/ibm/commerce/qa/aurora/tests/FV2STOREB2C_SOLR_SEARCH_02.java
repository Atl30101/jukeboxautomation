package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.IBMCopyright;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.ComparePage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;




//Declares a new test scenario called FV2STOREB2C_SOLR_SEARCH_01 which contains one or more test case methods.
//All test scenario classes must extend StoreModelTestCase.

/**
 * Scenario: FV2STOREB2C_SOLR_SEARCH_02 Objective: To specify a search term in the search box
 * Pre-requisites: Tester knows the Store URL
 * (http://<hostname>/webapp/wcs/stores/servlet/en/auroraesite) 
 * Search based navigation is enabled
 *  Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_SOLR_SEARCH_02 extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = IBMCopyright.SHORT_COPYRIGHT;

	//A variable to hold the name of the data file where input parameters can be found.
	//$ANALYSIS-IGNORE
	protected final String dataFileName = "data/FV2STOREB2C_SOLR_SEARCH_02_Data.xml";
	
	private CaslFixturesFactory f_CaslFactory;

	@DataProvider
	private final TestDataProvider dsm;

	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dsm
	 * @param p_CaslFactory 
	 */
	@Inject
	public FV2STOREB2C_SOLR_SEARCH_02(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dsm,
			CaslFixturesFactory p_CaslFactory)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dsm;
		f_CaslFactory = p_CaslFactory;
	}
//////////////////////////////////////////////////////////
	/**
	 * Test case to test Shopper search results in one product match found
	 * Pre-conditions: none
	 * Post conditions: Search results page displays the product match for the
	 * keyword search.
	 * 
	 * @throws Exception
	 */
	@Category(Sanity.class)
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_0201() throws Exception {
		dsm.setDataFile(dataFileName);
		//Tell which data block to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_0201", "testFV2STOREB2C_SOLR_SEARCH_0201_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		signIn.typeUsername(dsm.getInputParameter("logonId")).typePassword(dsm.getInputParameter("logonPassword")).signIn().closeSignOutDropDownWidget();
		
		//search for a product with multiple SKUs
		SearchResultsPage searchResults = signIn.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM_1"));
		//click on a product identified by its name
		ProductDisplayPage product = searchResults.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME_1"));
		//choose color
		product.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("color"));
		//choose size
		product.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("size"));
		//click on add to cart button from the product display page.
		product.addToCart();
	
		//complete order using WC commerce service layer
		OrdersFixture orders = f_CaslFactory.createOrdersFixture(dsm.getInputParameter("logonId"), dsm.getInputParameter("logonPassword"),getConfig().getStoreName());

		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();
		
	}

	/**
	 * Test case to shopper search results in no product matches
	 * Pre-conditions: none
	 * Post conditions: Search results page displays with no results found.
	 * 
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_0203() throws Exception {
		dsm.setDataFile(dataFileName);
		//Tell which data block to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_0203", "testFV2STOREB2C_SOLR_SEARCH_0203_DATABLOCK");
		
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Perform a Search
		SearchResultsPage searchResults = frontPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCHTERM_1"));

		//verify the search result summary is present.
		searchResults.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT"));
	}

	/**
	 * Test case to test Multiple keyword search results in one product match found
	 * Post conditions: Search results page displays the product
	 * match for the multiple keyword search.
	 * 
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_0206() throws Exception {
		dsm.setDataFile(dataFileName);
		//Tell which data block to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_0206", "testFV2STOREB2C_SOLR_SEARCH_0206_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Perfrom a search
		SearchResultsPage searchResults = frontPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCHTERM_1"));

		//verify the expected results are present
		searchResults.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
	}

	/**
	 * Test the product compare functionality by clicking the compare checkbox for two 
	 * products and click on the compare button  Post conditions: Search is successfully initiated
	 * and customer is able to add their drag-and-drop multiple products into
	 * the product comparison box successfully.
	 * 
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_0209() throws Exception {
		dsm.setDataFile(dataFileName);
		//Tell which data block to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_0209", "testFV2STOREB2C_SOLR_SEARCH_0209_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Perform a search
		SearchResultsPage searchResults = frontPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCHTERM_1"));

		//verify the expected results are present
		searchResults.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		searchResults.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_2"));
		
		//check comparison box for the products 
		searchResults.getCatalogEntryListWidget().allowCompareProduct(dsm.getInputParameter("EXPECTED_RESULT_1"));
		searchResults.getCatalogEntryListWidget().allowCompareProduct(dsm.getInputParameter("EXPECTED_RESULT_2"));
		
		//CategoryDisplayPage.clickCompareSelectedButton();
		ComparePage comp = searchResults.getCatalogEntryListWidget().compare(dsm.getInputParameter("EXPECTED_RESULT_1"));
				
		//check if the products are present on the compare page
		comp.verifyItemIsPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		comp.verifyItemIsPresent(dsm.getInputParameter("EXPECTED_RESULT_2"));
	}

	/**
	 * Test case to test QuickInfo popup appears to allow user select attributes
	 * then add item to cart in search Post conditions:QuickAdd popup appears,
	 * user selects attributes and adds to order
	 * 
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_0210() throws Exception {
		dsm.setDataFile(dataFileName);
		//Tell which data block to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_0210", "testFV2STOREB2C_SOLR_SEARCH_0210_DATABLOCK");
	
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
	
		//Perform a search
		ProductDisplayPage prod = frontPage.getHeaderWidget().performSearch(ProductDisplayPage.class, dsm.getInputParameter("SEARCHTERM_1"));
		
		//verify the expected results are present
		prod.getNamePartNumberAndPriceWidget().verifyCatEntryName(dsm.getInputParameter("EXPECTED_RESULT_1"));		//choose color
		prod.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("color"));
		//choose size
		prod.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("size"));
		//Click add to cart 
		prod.addToCart();
	
		//Verify the add to cart success message appears.
		prod.getHeaderWidget().verifyProductNameInMiniCart(dsm.getInputParameter("EXPECTED_RESULT_1"));
	}
	/**
	 * Test case to test QuickInfo popup appears to allow user select attributes
	 * then add item to cart in search Post conditions:QuickAdd popup appears,
	 * user selects attributes and adds to order
	 * 
	 * @throws Exception
	 */
	
	/**
	 * Test case to test Search works properly under HTTPS protocol 
	 * Post conditions: Search results page displays the product match for the keyword search.
	 * 
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFV2STOREB2C_SOLR_SEARCH_0213() throws Exception {
		dsm.setDataFile(dataFileName);
		//Tell which data block to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_0213", "testFV2STOREB2C_SOLR_SEARCH_0213_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//click on log in to get to an HTTPS page
		SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn()
				.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"));
		HeaderWidget header = signIn.signIn().closeSignOutDropDownWidget();
		
		// Do a Search
		SearchResultsPage searchResults = header.performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCHTERM_1"));
		//verify the expected results are present
		searchResults.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));
		searchResults.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_2"));
	}
	/**
	 * tear down method, runs after every test case
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception{

		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().continueToPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
	
		//Remove all items from the shopping cart
		frontPage.getHeaderWidget().goToShoppingCartPage().removeAllItems();
	}
	
	
	
	
}	
	
