package com.ibm.commerce.qa.aurora.tests;


	/*
	 *-----------------------------------------------------------------
	 * Licensed Materials - Property of IBM
	 *
	 * 
	 *
	 * WebSphere Commerce
	 *
	 * (C) Copyright IBM Corp. 2012
	 *
	 * US Government Users Restricted Rights - Use, duplication or
	 * disclosure restricted by GSA ADP Schedule Contract with
	 * IBM Corp.
	 *-----------------------------------------------------------------
	 */

	import java.util.logging.Logger;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.casl.keys.CaslKeysFactory;
import com.ibm.commerce.casl.keys.ToolingKeys;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.CustomerServiceFindCustomerWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.OrgAdminConsole;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.wte.util.WcConfigManager;


	/** 
	 * Test scenario to test various use cases associated with Dropdown menu context
	 * Refer to each test case for a detailed use case description
	 */
	@RunWith(GuiceTestRunner.class)
	@TestModules(AuroraModule.class)
	public class FSTOREB2CCSR_03 extends AbstractAuroraSingleSessionTests
	{
		/**
		 * The internal copyright field.
		 */
		public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

		//A Variable to retrieve data from the data file. 
		@DataProvider
		private final TestDataProvider dsm;
		private CaslKeysFactory f_caslKeysFactory;
		
		/**
		 * Test Class object constructor.
		 * 
		 * @param log
		 * 			   logging object 
		 * @param config
		 * 			   object to work with getConfig().properties file
		 * @param session
		 * 			   factory to create browser sessions
		 * @param dataSetManager
		 * 			   object to work with data files
		 * @param p_caslFixtures 
		 */		
		@Inject
		public FSTOREB2CCSR_03(
				Logger log, 
				WcConfigManager config,
				WcWteTestRule wcWebTestRule,
				CaslFoundationTestRule caslTestRule,
				TestDataProvider dataSetManager,
				OrgAdminConsole oac,
				CaslFixturesFactory p_caslFixtures, CaslKeysFactory p_caslKeysFactory)
		{
			super(log, wcWebTestRule, caslTestRule);
			this.dsm = dataSetManager;
			f_caslKeysFactory = p_caslKeysFactory;
		}


		/** Test case to Shop on behalf of a shopper as a CSR
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0301()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//Get LOGONID of a shopper that we are trying to locate
			String ShopperLogonId = dsm.getInputParameter("SHOPPER_LOGONID");
			
			CustomerServiceFindCustomerWidget FindCustomerWidget = frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
			.typeLogonId(ShopperLogonId).submitSearch();
			
			//Query to return unique user id (USER_ID) 
			ToolingKeys p = f_caslKeysFactory.createToolingKeys(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"));
			final String USER_ID = p.findPersonIdByLogonId(ShopperLogonId);
			
			//Access Customer's account and shop on behalf of a shopper as a CSR 
			FindCustomerWidget.AccessCustomerAccount(USER_ID).getHeaderWidget().goToFrontPage();
		}
		

		/** Test case to search for a field that would return zero results
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0302()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//Get LOGONID of a shopper that we are trying to locate
			String logonId = dsm.getInputParameter("ZERO_RESULT_LOGINID");

			
			frontPage.getHeaderWidget().goToCustomerService()
			.getFindCustomerWidget()
			.typeLogonId(logonId).submitSearch()
			
			//Verify that search did not return any result
			.verifySearchErrorMessageIsDisplayed();
			
		}
		
		

		/** Test case to enter invalid firstname and last name that would return zero results
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0303()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//Get LOGONID of a shopper that we are trying to locate
			String firstName = dsm.getInputParameter("INVALID_FIRSTNAME");
			String lastName = dsm.getInputParameter("INVALID_LASTNAME");

			
			frontPage.getHeaderWidget().goToCustomerService()
			.getFindCustomerWidget()
			.typeFirstName(firstName)
			.typeLastName(lastName).submitSearch()
			
			//Verify that search did not return any result
			.verifySearchErrorMessageIsDisplayed();
			
		}
		
		/** Test case to Shop on behalf of a shopper as a CSR
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0304()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//Get LOGONID of a shopper that we are trying to locate
			String ShopperLogonId = dsm.getInputParameter("SHOPPER_LOGONID");

			
			CustomerServiceFindCustomerWidget FindCustomerWidget = frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
			
			.typeLogonId(ShopperLogonId)
			.typeFirstName(dsm.getInputParameter("SHOPPER_FIRST_NAME"))
			.typeLastName(dsm.getInputParameter("SHOPPER_LAST_NAME"))
			.typeStreetAddressLine1(dsm.getInputParameter("SHOPPER_ADDRESS"))
			.typePhoneNumber(dsm.getInputParameter("SHOPPER_PHONE_NUMBER"))
			.selectCountryOrRegion(dsm.getInputParameter("SHOPPER_COUNTRY"))
			.typeZipCode(dsm.getInputParameter("SHOPPER_ZIPCODE"))
			.typeEmail(dsm.getInputParameter("SHOPPER_EMAIL"))
			.selectStateOrProvince(dsm.getInputParameter("SHOPPER_STATE"))
			
			.submitSearch();
			
			//Query to return unique user id (USER_ID) 
			ToolingKeys p = f_caslKeysFactory.createToolingKeys(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"));
			final String USER_ID = p.findPersonIdByLogonId(ShopperLogonId);
			
			//Locate and verify that customer appears in serach result
			FindCustomerWidget.verifyShopperIsDisplayedInSearchResult(USER_ID);
		}
		
		
		/** Test case to Enter all search fields then click Clear Filter
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0306()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//Get LOGONID of a shopper that we are trying to locate
			String ShopperLogonId = dsm.getInputParameter("SHOPPER_LOGONID");

			
			frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
			
			.typeLogonId(ShopperLogonId)
			.typeFirstName(dsm.getInputParameter("SHOPPER_FIRST_NAME"))
			.typeLastName(dsm.getInputParameter("SHOPPER_LAST_NAME"))
			.typeStreetAddressLine1(dsm.getInputParameter("SHOPPER_ADDRESS"))
			.typePhoneNumber(dsm.getInputParameter("SHOPPER_PHONE_NUMBER"))
			.selectCountryOrRegion(dsm.getInputParameter("SHOPPER_COUNTRY"))
			.typeZipCode(dsm.getInputParameter("SHOPPER_ZIPCODE"))
			.typeEmail(dsm.getInputParameter("SHOPPER_EMAIL"))
			.selectStateOrProvince(dsm.getInputParameter("SHOPPER_STATE"))
			
			.clearFilter();
			
			
		}
		
		/** Test case to  Search for a shopper then collapse and expand the search box
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0307()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//Get LOGONID of a shopper that we are trying to locate
			String ShopperLogonId = dsm.getInputParameter("SHOPPER_LOGONID");
			
			frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
			
			.typeLogonId(ShopperLogonId)
			.submitSearch()
			
			.clickExpandCollapseToggleButton();
				
		}
		
		/** Test case to  Click on a search result to see more information
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0310()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//Get LOGONID of a shopper that we are trying to locate
			String ShopperLogonId = dsm.getInputParameter("SHOPPER_LOGONID");

			
			CustomerServiceFindCustomerWidget FindCustomerWidget = frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
			.typeLogonId(ShopperLogonId)
			.submitSearch();
			
			//Query to return unique user id (USER_ID) 
			ToolingKeys p = f_caslKeysFactory.createToolingKeys(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"));
			final String USER_ID = p.findPersonIdByLogonId(ShopperLogonId);
			
			//Locate and verify that customer appears in serach result
			FindCustomerWidget.clickExpandSearchResultButton(USER_ID).verifyMemberDetailsExpanded(USER_ID);
		}
		
		/** Test case to  Access shopper's account
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0311()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//Get LOGONID of a shopper that we are trying to locate
			String ShopperLogonId = dsm.getInputParameter("SHOPPER_LOGONID");
			
			CustomerServiceFindCustomerWidget FindCustomerWidget = frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
			.typeLogonId(ShopperLogonId).submitSearch();
			
			//Query to return unique user id (USER_ID) 
			ToolingKeys p = f_caslKeysFactory.createToolingKeys(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"));
			final String USER_ID = p.findPersonIdByLogonId(ShopperLogonId);
			
			//Access Customer's account and shop on behalf of a shopper as a CSR 
			FindCustomerWidget.AccessCustomerAccount(USER_ID).getHeaderWidget().goToFrontPage();
		}
		
		/** Test case to  Disable shopper's account
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0312()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//Get LOGONID of a shopper that we are trying to locate
			String ShopperLogonId = dsm.getInputParameter("SHOPPER_LOGONID");

			
			CustomerServiceFindCustomerWidget FindCustomerWidget = frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
			.typeLogonId(ShopperLogonId)
			.submitSearch();
			
			//Query to return unique user id (USER_ID) 
			ToolingKeys p = f_caslKeysFactory.createToolingKeys(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"));
			final String USER_ID = p.findPersonIdByLogonId(ShopperLogonId);
			
			FindCustomerWidget.clickActionButton(USER_ID).clickDisableCustomerAccount(USER_ID);
			
		}
		
		/** Test case to enable shopper's account
		 */
		@Category(Sanity.class)
		@Test
		public void testFSTOREB2CCSR_0313()
		{
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

			//Log in as CSR
			frontPage.signIn(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"));
			
			//Get LOGONID of a shopper that we are trying to locate
			String ShopperLogonId = dsm.getInputParameter("SHOPPER_LOGONID");

			
			CustomerServiceFindCustomerWidget FindCustomerWidget = frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
			.typeLogonId(ShopperLogonId)
			.submitSearch();
			
			//Query to return unique user id (USER_ID) 
			ToolingKeys p = f_caslKeysFactory.createToolingKeys(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"));
			final String USER_ID = p.findPersonIdByLogonId(ShopperLogonId);
			
			FindCustomerWidget.clickActionButton(USER_ID).clickEnableCustomerAccount(USER_ID);
		}
		
	
	}
