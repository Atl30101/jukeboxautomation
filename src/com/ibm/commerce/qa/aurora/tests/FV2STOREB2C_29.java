package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.ApparelSubCategoryPage;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/**			
* Scenario: FV2STOREB2C_29
* Details: This scenario will test Marketing Management.
*
* 
*/
@RunWith(GuiceTestRunner.class) 
@TestModules(AuroraModule.class)
public class FV2STOREB2C_29  extends AbstractAuroraSingleSessionTests
{
	@DataProvider
	private final TestDataProvider dsm;
	
	private CMC cmc;
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dataSetManager
	 * @param cmc
	 */
	@Inject
	public FV2STOREB2C_29(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CMC cmc)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		this.cmc = cmc;
	}	
	
	/**A variable to hold the web activity name**/
	private String uniqueActivityName = null;
	
	/**A variable to hold the web activity identifier created**/
	private String activityId; 
	
	/**A variable to hold the promotion identifier created**/
	private String promotionId;
	
	/**
	 * A test to Create a Web Activity from CMC and view from store front.
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_2901() throws Exception
	{	
		//login to CMC
		dsm.setDataLocation("testFV2STOREB2C_2901", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore();
		
		//Create a coupon promotion
		dsm.setDataLocation("testFV2STOREB2C_2901", "Create_Promotion");
		promotionId = cmc.getPromotionId(dsm.getInputParameter("PROMOTION_NAME"));
		if (promotionId == null) {
			promotionId = cmc.createPromotion(dsm.getInputParameter("PROMOTION_NAME"), dsm.getInputParameter("PROMOTION_TYPE"));
			cmc.createPromotionElementforOrderLevelPromotion(dsm.getInputParameter("minimunOrderAmount"), dsm.getInputParameter("discountAmount"), dsm.getInputParameter("elementSubType1"), dsm.getInputParameter("elementSubType2"));
		}
		if (!cmc.isPromotionActivated(promotionId)) {
			cmc.activatePromotion(promotionId);
		}
		//Create a content
		dsm.setDataLocation("testFV2STOREB2C_2901", "Create_Content");
		String contentId = cmc.getContentId(dsm.getInputParameter("CONTENT_NAME"));
		if (contentId == null) {
			contentId = cmc.createMarketingContent(dsm.getInputParameter("CONTENT_NAME"), dsm.getInputParameter("CONTENT_TYPE"), "",dsm.getInputParameter("text"), "-1", dsm.getInputParameter("url"));
		}
		
		//create web activity
		dsm.setDataLocation("testFV2STOREB2C_2901", "webActivity");
		cmc.selectCatalog();
		String emsId = cmc.getESpotId(dsm.getInputParameter("ESpot"));
		if(emsId == null){
			emsId = cmc.createESpot(dsm.getInputParameter("ESpot"), dsm.getInputParameter("ESpot_Description"));
		}
		uniqueActivityName = dsm.getInputParameter("ACTIVITY_NAME");
		activityId = cmc.createPromotionRecommendations(uniqueActivityName, emsId, promotionId, contentId);
		
		//activate web activity
		cmc.activateActivity(uniqueActivityName); 

		//log off from CMC
		cmc.logoff();
	
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_2901","testFV2STOREB2C_2901");
		
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Login with valid user id and password
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"));
		HeaderWidget header = signIn.signIn().closeSignOutDropDownWidget();
		
		//click on Home link
		ApparelSubCategoryPage dept = header.goToCategoryPageByHierarchy(ApparelSubCategoryPage.class, dsm.getInputParameter("TOP_CAT"), dsm.getInputParameter("SUB_CAT"));
		
		
		//Verify the coupon promotion is displayed at home page.
		dept.verifyTextPresent(dsm.getInputParameter("COUPON_TEXT"));
		
		//Click on log out link
		frontPage.getHeaderWidget().openSignOutDropDownWidget().signOut();
	}
	
	/**
	 * Perform tear down operations such as stopping the test harness.
	 * This method will also delete the web activity created in this scenario.
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception
	{	
	try
	{	//login to CMC
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.selectStore();
		cmc.selectCatalog();
		
		//deactivate and delete web activity
		cmc.deactivateActivity(uniqueActivityName);
		cmc.deleteWebActivity(activityId);
		
		//deactivate and delete Promotion
		cmc.deactivatePromotion(promotionId);
		cmc.deletePromotion(promotionId);
		
		//log off from CMC
		cmc.logoff(); 
	}
	catch(RuntimeException e)
	{
		getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
	}
	}
}
