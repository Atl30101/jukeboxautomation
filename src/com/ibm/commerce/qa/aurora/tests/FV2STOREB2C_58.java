package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/**
 * Scenario: FV2STOREB2C_58
 * Details: BazaarVoice Summary Reviews
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_58 extends AbstractAuroraSingleSessionTests
{
	@DataProvider
	private final TestDataProvider dsm;
	private final CMC cmc;
		
	/**
	 * @param logger
	 * @param config
	 * @param session
	 * @param dataSetManager
	 * @param cmc
	 */
	@Inject
	public FV2STOREB2C_58(Logger logger, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CMC cmc) {
		
	    super(logger, wcWebTestRule, caslTestRule);
		this.dsm=dataSetManager;
		this.cmc=cmc;
	}
	
	/**
	 * enable/disable flexflow option
	 * @param optNameToenable 
	 * @param optNametoDisable
	 * @throws Exception 
	 */
	public void enableOrdisableFlexFlowOption(String[] optNameToenable, String[] optNametoDisable) throws Exception
	{
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("logonPassword"));
		cmc.modifyChangeFlowOptions(optNameToenable, optNametoDisable);
		cmc.logoff();
			
	}
	
	/**
	 * goto ProductDisplay page
	 * @return ProductDisplayPage
	 */
	public ProductDisplayPage productPageTest()
	{
		
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		SearchResultsPage searchResults = frontPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		ProductDisplayPage productDisplay = searchResults.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));	
		return productDisplay;	
		
	}
	
	
	/**
	 * Enable, disable and again enable BazaarVoice RatingReview Flex Flow and verify on storefront 
	 * @throws Exception 
	 */
	@Test
	public void testFV2STOREB2C_5804() throws Exception {
		String opt[] = dsm.getInputParameterAsArray("flowOptionName");
		
		//enable flex flow option
		enableOrdisableFlexFlowOption(opt,null);
		
		//verify on storefront
		ProductDisplayPage productDisplay = productPageTest();
		productDisplay.verifyBazaarVoiceState(true);
		
		//disable flex flow option
		enableOrdisableFlexFlowOption(null,opt);
		
		//verify on storefront
		productDisplay = productPageTest();
		productDisplay.verifyBazaarVoiceState(false);
		
		//enable flex flow option
		enableOrdisableFlexFlowOption(opt,null);
		
		//verify on storefront
		productDisplay = productPageTest();
		productDisplay.verifyBazaarVoiceState(true);
		
	}


}
