package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */
//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/** Test scenario to test various use cases associated with viewing items in shopping cart.
 *  Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_08 extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	//private boolean cleanUpNeeded = true;
	@DataProvider(apolloDSM=true)
	
	//A Variable to retrieve data from the data file.
	private final TestDataProvider dsm;

	private CaslFixturesFactory f_CaslFixtures;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log logging object 
	 * @param dataSetManager  object to work with data files
	 * @param config object to work with config.properties file
	 * @param session factory to create browser sessions
	 * @param p_CaslFixtures 
	 */
	@Inject
	public FV2STOREB2C_08(Logger log,
			TestDataProvider dataSetManager,
			CaslFoundationTestRule caslTestRule, 
			WcWteTestRule wcWebTestRule,
			CaslFixturesFactory p_CaslFixtures){
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		f_CaslFixtures = p_CaslFixtures;
	}
	
	
	/**
	 * Helper method to execute the sign in action for each test case.
	 * 
	 * @return a new my account page object.
	 */
	public MyAccountMainPage executeSignIn()
	{
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Open Sign In/Register page
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter account User Name/Password and sign in
		MyAccountMainPage accountPage = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD")).signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		return accountPage;
	}
	
	/** 
	 * Test case to View items in the mini shopping cart.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_0801() {
		
		//Execute user login
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open Sub category Apparel->Dresses.
		CategoryPage subCat = myAccountPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), 
				dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//Open dress product detail page.
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		for(int i = 0; i < Integer.valueOf(dsm.getInputParameter("NUMBER_OF_ITEMS")); i++)
		{
			subCat = productDisplay.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), 
					dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
			
			//Re-navigate to the product page
			productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));

			//Add an item to the cart
			productDisplay = productDisplay.addToCart();
		}
		
		//Get the header section page
		HeaderWidget header = productDisplay.getHeaderWidget();
		
		productDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//Verify the item count in the mini shop cart is correct.
		header.verifyMiniCartItemCount(dsm.getInputParameter("NUMBER_OF_ITEMS"));

		//Select/Verify the 'view more items' link on mini shopping cart.
		ShopCartPage shopCart = header.viewMoreItemsInMiniShopCart();
				
		//Verify the price total in the mini shop cart.
		shopCart.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL_PRICE"));
		
		//Remove all the items from the shopping cart.
		shopCart.removeAllItems();
		
		//cleanUpNeeded = false;
	
  }
	
	
	/** 
	 * Test case to View items in the detailed shopping cart.
	 * 
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_0802() {
		
		//Execute user login
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open Sub category Apparel->Dresses.
		CategoryPage subCat = myAccountPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class,dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//Open dress product detail page.
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select product attribute value
		productDisplayPage
			.getDefiningAttributesWidget()
			.selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE_VALUE"));
		
		//Add product to Shopping Cart. 
		productDisplayPage.addToCart();
		
		//Go to Shop Cart detail page.
		ShopCartPage shopCart = productDisplayPage.getHeaderWidget().goToShoppingCartPage();
		
		//Verify item in shop cart list.
		shopCart.verifyItemInShopCart(dsm.getInputParameter("VERIFY_ITEM_NAME"));
		
		shopCart.verifyDiscounts(dsm.getInputParameter("VERIFY_DISCOUNT"))
		
		//Remove all the items from the shopping cart.
		.removeAllItems();
		
		//cleanUpNeeded = false;
		
  }
	
	/** 
	 * Test case to View an item in the shopping cart that is not in stock and back ordered.
	 * 
	 */
	@Test
	public void testFV2STOREB2C_0803() {
		
		//Execute user login
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open Sub category Apparel->Dresses.
		CategoryPage subCat = myAccountPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class,dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//Click on product image whose inventory status is out of stock or back ordered.
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByImagePosition(Integer.valueOf(dsm.getInputParameter("POSITION_OF_OUTOFSTOCK_PRODUCT")));
		
		//Update product quantity
		productDisplayPage.getShopperActionsWidget().updateQuantity(dsm.getInputParameter("UPDATE_QUANTITY"));

		//Select product size attribute value
		productDisplayPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE_VALUE"));
		
		//Add product to shopping cart.
		productDisplayPage.addToCart();
		
		//Go to shopping cart page
		ShopCartPage shopCartPage = productDisplayPage.getHeaderWidget().goToShoppingCartPage();
		
		//Go to shipping and billing page.
		ShippingAndBillingPage shippingAndBilling = shopCartPage.continueToNextStep();
		
		//Complete a quick checkout order so product becomes out of stock
		OrderConfirmationPage orderConfirmationPage = shippingAndBilling.selectPaymentMethod("Pay later").singleShipNext().completeOrder();
		
		//Open Sub category Apparel->Dresses.
		subCat = orderConfirmationPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class,dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), 
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//Click on product image whose inventory status is out of stock or back ordered.
		productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByImagePosition(Integer.valueOf(dsm.getInputParameter("POSITION_OF_OUTOFSTOCK_PRODUCT")));
		
		//Select product size attribute value
		productDisplayPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE_VALUE"));
		
		//Add product to shopping cart. 
		productDisplayPage.addToCart();
		
		//Go to Shop Cart detail page.
		shopCartPage = productDisplayPage.getHeaderWidget().goToShoppingCartPage();
		
		//Verifies item status on Shop Cart Page.
		shopCartPage.verifyItemStatusInShopCart(dsm.getInputParameter("SHOP_CART_ITEM_STATUS"),
				dsm.getInputParameter("ITEM_NUMBER_IN_SHOP_CART"))
		
		//Remove all the items from the shopping cart.
		.removeAllItems();
		
		//cleanUpNeeded = false;
		
	}
     
    /** 
     * Test case to View an empty Mini shopping cart.
     * 
	 */
	@Test
	public void testFV2STOREB2C_0804() {
 		
		//Execute user login
		MyAccountMainPage myAccountPage = executeSignIn();
 		
 		//Verify that mini Shop cart is empty.
		myAccountPage.getHeaderWidget().verifyEmptyMiniShopCart();
		
		//cleanUpNeeded = false;
		 		
 	}
      
    /** 
     * Test case to View an empty detailed shopping cart.
     * 
	 */
	@Test
    public void testFV2STOREB2C_0805() {
   		
		//Execute user login
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Remove all items from the shopping cart
		myAccountPage.getHeaderWidget().goToShoppingCartPage().removeAllItems();
   		
    	//Clicks on 'Shopping Cart' link on Header.
		ShopCartPage shopCartPage = myAccountPage.getHeaderWidget().goToShoppingCartPage();
		
		//Verifies that shop cart is empty.
		shopCartPage.verifyShoppingCartIsEmpty()
   		
   		//Verify Shopping cart empty message.
   		.verifyEmptyShoppingCartMessage(dsm.getInputParameter("SHOP_CART_EMPTY_MESSAGE"));
		
		//cleanUpNeeded = false;
   		
 }
	
    /**
     * Test case to View a shopping cart that has more items than the shopping cart paging limit.
     * 
	 */
	@Test
   	public void testFV2STOREB2C_0806() {
    		
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		
		for(int a=0;a<=Integer.valueOf(dsm.getInputParameter("MAX_NUMBER"));a++)
		{
			orders.addItem(dsm.getInputParameter("PRODUCT_SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
		}
		//Execute user login
		MyAccountMainPage myAccountPage = executeSignIn();
		
		
		//Clicks on 'Shopping Cart' link on Header.
		ShopCartPage shoppingCart = myAccountPage.getHeaderWidget().goToShoppingCartPage();
		
		//Clicks on next page link on shopping cart page.
		shoppingCart.selectNextPage()
		
		//Verifies number of items in the second product list page
		.verifyNumberOfItemsOnCurrentPage(Integer.valueOf(dsm.getInputParameter("VERIFY_NUMBER_OF_ITEMS_ON_SECOND_PAGE")))
		
		//Clicks on next page link on shopping cart page.
		.selectPreviousPage()
		
		//Verifies number of items in the first product list page
		.verifyNumberOfItemsOnCurrentPage(Integer.valueOf(dsm.getInputParameter("VERIFY_NUMBER_OF_ITEMS_ON_FIRST_PAGE")));	
      
		//cleanUpNeeded = true;
	}

	
	/**
	 * Tear down ran after every test case
	 */
	@After
	public void tearDown(){
		try
		{
			//if (cleanUpNeeded) {
			//Continue browser session starting at the home page.
				//HeaderWidget headerSection = getSession().continueToPage(getConfig().getStoreUrl(), HeaderWidget.class);
			
				//Remove all items from the shopping cart
				OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
				
				orders.removeAllItemsFromCart();		
				orders.deletePaymentMethod();
			//}
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

	}
 }

