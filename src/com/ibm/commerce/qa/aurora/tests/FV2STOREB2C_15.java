package com.ibm.commerce.qa.aurora.tests;


/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.NonRegisteredShippingBillingInfoPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.wte.framework.page.BrowserType;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;



/**
 * Scenario: FV2STOREB2C_15
 * This scenario tests adding or removing promotions to/from an order	
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_15 extends AbstractAuroraSingleSessionTests
{
	
	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	private final CaslFixturesFactory f_CaslFixtures;
	
	private CMC cmc;
	
	/**A variable to hold the promotion identifier created**/
	private String promotionId;
		
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 				object to work with data files
	 * @param cmc
	 * 				object to work with cmc settings
	 */
	@Inject
	public FV2STOREB2C_15(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CMC cmc,
			CaslFixturesFactory p_CaslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		this.cmc = cmc;
		f_CaslFixtures = p_CaslFixtures;
	}
	
	/**
	 * As a registered shopper, apply a valid promotion from the shopping cart page
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_1501() throws Exception
	{		
		//login to CMC
		dsm.setDataLocation("testFV2STOREB2C_1501", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore();
		
		dsm.setDataLocation("testFV2STOREB2C_1501", "Create_Promotion");
		promotionId = cmc.getPromotionId(dsm.getInputParameter("PROMOTION_NAME"));
		if (promotionId == null) {
			promotionId = cmc.createPromotion(dsm.getInputParameter("PROMOTION_NAME"), dsm.getInputParameter("PROMOTION_TYPE"));
			cmc.createPromotionElementforOrderLevelPromotion(dsm.getInputParameter("minimunOrderAmount"), dsm.getInputParameter("discountAmount"));
		}
		if (!cmc.isPromotionActivated(promotionId)) {
			cmc.activatePromotion(promotionId);
		}
		
		dsm.setDataLocation("testFV2STOREB2C_1501", "testFV2STOREB2C_1501");
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log on store
		HeaderWidget header = signIn.typeUsername(dsm.getInputParameter("LOGONID"))
		.typePassword(dsm.getInputParameter("PASSWORD"))
		.signIn().closeSignOutDropDownWidget();
		
		//cleanup
		CartCleanup(header);
		
		//Open Sub category Apparel->Dresses.
		CategoryPage subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//click on product image on Category Display page.
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select the product swatches
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR")).selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));
		
		//Change the quantity of the product
		productDisplay.getShopperActionsWidget().updateQuantity(dsm.getInputParameter("ITEM_QTY"));
		
		//click Add to Cart from Product Display page.
		productDisplay.addToCart();
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();

		//Confirm product is available
		shopCart.verifyItemInShopCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Enter promotion code
		shopCart.typePromoCode(dsm.getInputParameter("PROMO_CODE"));
		
		//Confirm promotion code
		shopCart.verifyPromoCodeIsOnPage(dsm.getInputParameter("PROMO_CODE_POSITION"), dsm.getInputParameter("PROMO_CODE"));
		
		//Confirm product discount
		shopCart.verifyDiscounts(dsm.getInputParameter("DISCOUNT"));
		
		//Confirm order sub total
		shopCart.verifyOrderSubTotal(dsm.getInputParameter("SUBTOTAL"));
		
		//Remove promotion code
		shopCart.removePromoCode(dsm.getInputParameter("PROMO_CODE_POSITION"));
	}
	
	/**
	 * As a registered shopper, apply a valid catalog entry level promotion from the shipping and billing page
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_1502() throws Exception
	{
		dsm.setDataLocation("testFV2STOREB2C_1502", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore();
		
		dsm.setDataLocation("testFV2STOREB2C_1502", "Create_Promotion");
		promotionId = cmc.getPromotionId(dsm.getInputParameter("PROMOTION_NAME"));
		if (promotionId == null) {
			promotionId = cmc.createPromotion(dsm.getInputParameter("PROMOTION_NAME"), dsm.getInputParameter("PROMOTION_TYPE"));
			cmc.createPromotionElementforOrderLevelPromotion(dsm.getInputParameter("minimunOrderAmount"), dsm.getInputParameter("discountAmount"),dsm.getInputParameter("elementSubType1"),dsm.getInputParameter("elementSubType2"));
		}
		if (!cmc.isPromotionActivated(promotionId)) {
			cmc.activatePromotion(promotionId);
		}
		
		dsm.setDataLocation("testFV2STOREB2C_1502", "testFV2STOREB2C_1502");
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log on store
		HeaderWidget header = signIn.typeUsername(dsm.getInputParameter("LOGONID"))
		.typePassword(dsm.getInputParameter("PASSWORD"))
		.signIn().closeSignOutDropDownWidget();
		
		//cleanup
		CartCleanup(header);
		
		//Go to department page				
		CategoryPage subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//click on product image on Category Display page.
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select the product swatches
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR")).selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));
		
		//Change the quantity of the product
		productDisplay.getShopperActionsWidget().updateQuantity(dsm.getInputParameter("ITEM_QTY"));
		
		//click Add to Cart from Product Display page.
		productDisplay.addToCart();
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//Confirm the price on minishopping cart
		productDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
		
		//Confirm product is available
		shopCart.verifyItemInShopCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Enter promotion code
		shopCart.typePromoCode(dsm.getInputParameter("PROMO_CODE"));
		
		//Confirm promotion code
		shopCart.verifyPromoCodeIsOnPage(dsm.getInputParameter("PROMO_CODE_POSITION"), dsm.getInputParameter("PROMO_CODE"));
		
		//Confirm product discount
		shopCart.verifyDiscounts(dsm.getInputParameter("DISCOUNT"));
		
		//Confirm order total
		shopCart.verifyOrderSubTotal(dsm.getInputParameter("SUBTOTAL"));
		
		//Confirm order total
		shopCart.verifyOrderTotal(dsm.getInputParameter("PROMOTION_TOTAL"));
		
		//Remove promotions
		shopCart.removePromoCode(dsm.getInputParameter("PROMO_CODE_POSITION"));
	}
	
	/**
	 * As a registered shopper, remove a promotion code from the checkout page
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_1503() throws Exception
	{
		dsm.setDataLocation("testFV2STOREB2C_1503", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore();
		
		dsm.setDataLocation("testFV2STOREB2C_1503", "Create_Promotion");
		promotionId = cmc.getPromotionId(dsm.getInputParameter("PROMOTION_NAME"));
		if (promotionId == null) {
			promotionId = cmc.createPromotion(dsm.getInputParameter("PROMOTION_NAME"), dsm.getInputParameter("PROMOTION_TYPE"));
			cmc.createPromotionElementforOrderLevelPromotion(dsm.getInputParameter("minimunOrderAmount"), dsm.getInputParameter("discountAmount"),dsm.getInputParameter("elementSubType1"),dsm.getInputParameter("elementSubType2"));
		}
		if (!cmc.isPromotionActivated(promotionId)) {
			cmc.activatePromotion(promotionId);
		}
		
		dsm.setDataLocation("testFV2STOREB2C_1503", "testFV2STOREB2C_1503");
		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log on store
		HeaderWidget header = signIn.typeUsername(dsm.getInputParameter("LOGONID"))
		.typePassword(dsm.getInputParameter("PASSWORD"))
		.signIn().closeSignOutDropDownWidget();

		//cleanup
		CartCleanup(header);
		
		//Go to department page				
		CategoryPage subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//click on product image on Category Display page.
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select the product swatches
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR")).selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));
		
		//Change the quantity of the product
		productDisplay.getShopperActionsWidget().updateQuantity(dsm.getInputParameter("ITEM_QTY"));
		
		//click Add to Cart from Product Display page.
		productDisplay.addToCart();
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//Confirm the price on minishopping cart
		productDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
		
		//Confirm product is available
		shopCart.verifyItemInShopCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Enter promotion code
		shopCart.typePromoCode(dsm.getInputParameter("PROMO_CODE"));
		
		//Confirm promotion code
		shopCart.verifyPromoCodeIsOnPage(dsm.getInputParameter("PROMO_CODE_POSITION"), dsm.getInputParameter("PROMO_CODE"));
		
		//Confirm product discount
		shopCart.verifyDiscounts(dsm.getInputParameter("DISCOUNT"));
		
		//Confirm order total
		shopCart.verifyOrderSubTotal(dsm.getInputParameter("SUBTOTAL"));
		
		ShippingAndBillingPage shippingPage = shopCart.continueToNextStep();
		
		shippingPage.removePromoCode("1");
	}			
	
	/**
	 * As a registered shopper, apply an invalid promotion from the shipping and billing page
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_1504() throws Exception
	{		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log on store
		HeaderWidget header = signIn.typeUsername(dsm.getInputParameter("LOGONID"))
		.typePassword(dsm.getInputParameter("PASSWORD"))
		.signIn().closeSignOutDropDownWidget();
		
		if (getConfig().getBrowserType() == BrowserType.INTERNET_EXPLORER)
		{
			header.goToShoppingCartPage().removeAllItems();
		}
		
		//Go to department page				
		CategoryPage subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//cleanup
		CartCleanup(header);
		
		//click on product image on Category Display page.
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select the product swatches
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR")).selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));
		
		//Change the quantity of the product
		productDisplay.getShopperActionsWidget().updateQuantity(dsm.getInputParameter("ITEM_QTY"));
		
		//click Add to Cart from Product Display page.
		productDisplay.addToCart();
	
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();

		//Confirm product is available
		shopCart.verifyItemInShopCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Enter promotion code
		shopCart.typePromoCode(dsm.getInputParameter("PROMO_CODE"));
		
		//Confirm header message
		shopCart.getHeaderWidget().verifyErrorMessage(dsm.getInputParameter("ERROR_MESSAGE"));
	}
	
	/**
	 * As a registered shopper, apply an invalid promotion from the shipping and billing page
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_1505() throws Exception
	{
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log on store
		HeaderWidget header = signIn.typeUsername(dsm.getInputParameter("LOGONID"))
		.typePassword(dsm.getInputParameter("PASSWORD"))
		.signIn().closeSignOutDropDownWidget();
		
		//cleanup
		CartCleanup(header);
		
		//Go to department page				
		CategoryPage subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//click on product image on Category Display page.
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select the product swatches
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR")).selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));
		
		//Change the quantity of the product
		productDisplay.getShopperActionsWidget().updateQuantity(dsm.getInputParameter("ITEM_QTY"));
		
		//click Add to Cart from Product Display page.
		productDisplay.addToCart();
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//Confirm the price on minishopping cart
		productDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));

		//Confirm product is available
		shopCart.verifyItemInShopCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		
		ShippingAndBillingPage shippingPage = shopCart.continueToNextStep();
		
		//Enter promotion code
		shippingPage.typeInvalidPromoCode(dsm.getInputParameter("PROMO_CODE"));
		
		//Confirm header message
		shippingPage.getHeaderWidget().verifyErrorMessage(dsm.getInputParameter("ERROR_MESSAGE"));
	}
	
	/**
	 *As a guest shopper, apply a valid promotion from the shopping cart page and complete the order
	 *
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_1506() throws Exception
	{		
		
		//login to CMC
		dsm.setDataLocation("testFV2STOREB2C_1506", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore();
		
		dsm.setDataLocation("testFV2STOREB2C_1506", "Create_Promotion");
		promotionId = cmc.getPromotionId(dsm.getInputParameter("PROMOTION_NAME"));
		if (promotionId == null) {
			promotionId = cmc.createPromotion(dsm.getInputParameter("PROMOTION_NAME"), dsm.getInputParameter("PROMOTION_TYPE"));
			cmc.createPromotionElementforOrderLevelPromotion(dsm.getInputParameter("minimunOrderAmount"), dsm.getInputParameter("discountAmount"));
		}
		if (!cmc.isPromotionActivated(promotionId)) {
			cmc.activatePromotion(promotionId);
		}
		
		dsm.setDataLocation("testFV2STOREB2C_1506", "testFV2STOREB2C_1506");
		
		cmc.logoff();
		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Go to department page				
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//click on product image on Category Display page.
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select the product swatches
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));
		
		//Update quantity
		productDisplay.getShopperActionsWidget().updateQuantity(dsm.getInputParameter("QTY"));
		
		//click Add to Cart from Product Display page.
		productDisplay.addToCart();
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//Confirm the price on minishopping cart
		productDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
		
		//Enter promotion code
		shopCart.typePromoCode(dsm.getInputParameter("PROMO_CODE"));
		
		//Confirm product discount
		shopCart.verifyDiscounts(dsm.getInputParameter("DISCOUNT"));

		String receipientNickName1 = dsm.getInputParameter("RECIPIENT_1");			
		
		//Click on edit link on billing section
		NonRegisteredShippingBillingInfoPage guestUserShipBillInfo = shopCart.continueAsGuestUser();
				
		ShippingAndBillingPage shippingAndBilling = guestUserShipBillInfo.typeBillRecipient(receipientNickName1)
		 .typeBillLastName(dsm.getInputParameter("LAST_NAME"))
		 .typeBillFirstName(dsm.getInputParameter("FIRST_NAME"))
		 .typeBillAddress(dsm.getInputParameter("STREET_ADDRESS"))
		 .typeBillCity(dsm.getInputParameter("CITY"))
		 .selectBillCountry(dsm.getInputParameter("COUNTRY"))
		 .selectBillState(dsm.getInputParameter("PROVINCE"))
		 .typeBillZipCode(dsm.getInputParameter("POSTAL_CODE"))
		 .typeBillPhone(dsm.getInputParameter("PHONE_NUMBER"))
		 .typeBillEmail(dsm.getInputParameter("EMAIL"))
		 .applyToBothShippingBilling()
		 .submitSuccessfulForm();
		
		//Wait for billing address to display
		shippingAndBilling.selectShippingAddress(receipientNickName1);
		shippingAndBilling.waitForShippingAddress(receipientNickName1);

		//Select payment method for first number of payment
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));

		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	
		
		//Confirm payment page on order summary page
		singleOrderSummary.verifyBillingMethod(dsm.getInputParameter("PAY_METHOD"),dsm.getInputParameter("PAYMENT_NUMBER"));
		
		//Confirguesm billing address for first payment		
		singleOrderSummary.verifyBillingAddressByPaymentNumber(receipientNickName1, dsm.getInputParameter("ITEM_POSITION"));
		
		//Confirm product name
		singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm item quantity
		singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("QTY"));
		
		//Confirm total price of the item.
		singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_TOTAL"));
		
		//Confirm the total price of the order
		singleOrderSummary.verifyOrderTotal(dsm.getInputParameter("ORDER_TOTAL"));						
		
		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
		
		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();
	}
	
	//cleanup method to remove all items from the cart before proceeding.
	
	private void CartCleanup(HeaderWidget header)
	{
			try {
				header.verifyMiniCartItemCount("0");
			} catch (Exception e) {
				//cart isn't 0, cleaning up
				header.goToShoppingCartPage().removeAllItems();
			}

		
	}
	
	/**
	 * Perform teardown operations after the test is done, whether it is successful or not.
	 * @throws Exception
	 */
	@After
	public void testScenarioTearDown() throws Exception
	{
		
		try
		{
			getLog().info("testScenarioTearDown running");
			
			dsm.setDataLocation("testScenarioTearDown", "CMClogon");
			cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
			cmc.setLocale(dsm.getInputParameter("locale"));
			cmc.setTimeZone("America/Montreal");
			
			//Select the store to work on
			cmc.selectStore();

			//Deactivate and delete promotion
			cmc.deactivatePromotion(promotionId);
			cmc.deletePromotion(promotionId);
			
			dsm.setDataLocation("testScenarioTearDown", "testScenarioTearDown");
			
			cmc.logoff();
			
			//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//Opens the Sign In page in browser.
			SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
			
			//Log on store
			HeaderWidget header = signIn.typeUsername(dsm.getInputParameter("LOGONID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
			
			//cleanup
			//CartCleanup(header);
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
			
			orders.removeAllItemsFromCart();
			orders.deletePaymentMethod();
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

				
	}
		
}
