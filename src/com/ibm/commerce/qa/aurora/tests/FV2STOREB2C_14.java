package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2013
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp..
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import static org.junit.Assert.fail;

import java.util.logging.Logger;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.MyWishListPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;




/** 		
 * Scenario: FV2STOREB2C_14
 * Details: The objective of this test scenario is testing Shopper adds, removes and shares items to/from wish list
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_14 extends  AbstractAuroraSingleSessionTests {

	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	
	
	
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dataSetManager
	 */
	@Inject
	public FV2STOREB2C_14(Logger log, CaslFoundationTestRule caslTestRule, WcWteTestRule wcWebTestRule, TestDataProvider dataSetManager)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
	}
	

	/**
	 * Guest user cannot access wish list feature.
	 * * Pre-conditions: <ul>
	 * 					<li>Flex flow options: Wish List is enabled.				
	 *   </ul>	
	 */
	@Test
	public void FV2STOREB2C_1401(){
		
		
		boolean passStatus = false;
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Go to the Sing In page	
		try
		{
			// goToMyWishList method should throw exception for guest user.
			frontPage.getHeaderWidget().goToMyWishList();
		}catch(RuntimeException E)
		{
			passStatus =  true;
		}
		if(!passStatus)
			fail();
		
	}
	

	
	/**
	 * Guest user tries to add items to Wish List through product details page	 
	  	
	 */
	@Category(Sanity.class)
	@Test
	public void FV2STOREB2C_1402(){

		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//performing a search result for a category
		//Go to department page				
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//Clicking on first product
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Following action will through exception because guest user is not allowed to add product in wish list
		productDisplay.getShopperActionsWidget().addToWishlistAsGuestShopper();
		
	}
	
	/**
	* Testcase to Create a wish list	
	 */
	@Test
	public void FV2STOREB2C_1403(){
		
				
		String wishList = String.format("%s_%s", RandomStringUtils.randomAlphanumeric(8), "wishList");
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//cleanup
		myAccount.getSidebar().goToWishListPag().deleteAllWishLists();
		
		//Clicking on Personal wish list link
		MyWishListPage wishListPage = myAccount.getSidebar().goToWishListPag();
		
		//Creating new wish list and verifying whether it got created successfully				
		wishListPage.createNewWishList(wishList)
		.verifyWishListExist(wishList)
		.deleteWishList(wishList);
		
							
	}
	
	/**
	 * Testcase to rename a wish list	 
	*/
	@Test
	public void FV2STOREB2C_1404(){
		
				
		String wishList = String.format("%s_%s", RandomStringUtils.randomAlphanumeric(8), "wishList");
		String wishList2 = wishList + "2";
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//cleanup
		myAccount.getSidebar().goToWishListPag().deleteAllWishLists();	
		
		//Clicking on Personal wish list link
		MyWishListPage wishListPage = myAccount.getSidebar().goToWishListPag();
		
		//Creating new wish list and renaming it .
		wishListPage.createNewWishList(wishList)
		.selectWishList(wishList)
		.goToChangeWishListName()
		.typeEditWishListNameField(wishList2)
		.saveInEditWishListDialogue()
		.verifyWishListExist(wishList2);//verifying 							
	}
	
	/**
	 * Delete existing non-default wish list		
	 */
	
	@Test
	public void FV2STOREB2C_1405(){
		
				
		String wishList = String.format("%s_%s", RandomStringUtils.randomAlphanumeric(8), "wishList");
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//cleanup
		myAccount.getSidebar().goToWishListPag().deleteAllWishLists();	
		
		//Clicking on Personal wish list link
		MyWishListPage wishListPage = myAccount.getSidebar().goToWishListPag();
		
		//Creating new wish list and verifying whether it got created successfully
		wishListPage.createNewWishList(wishList)
		.verifyWishListExist(wishList)
		.deleteWishList(wishList); //deleting the newly created wish list							
	}
	
	/**
	 * Shopper cannot rename or delete the pre-set default wish list "Wish List"	
	 */
	@Test
	public void FV2STOREB2C_1406()
	{
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//Clicking on Personal wish list link
		MyWishListPage wishListPage = myAccount.getSidebar().goToWishListPag();
		
		//Verify that there is no "change your wish list name" link visible
		wishListPage.selectWishList("Wish List").verifyChangeWishListIsNotVisible();
		
		//Verify there is no delete option
		wishListPage.selectWishList("Wish List").verifyDeleteThisWishListIsNotVisible();
	}	
	
	/**
	 * Shopper cannot create a wish list with the name "Wish List" which is reserved for the default wish list	
	 */
	@Test
	public void FV2STOREB2C_1407()
	{
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//Clicking on Personal wish list link
		MyWishListPage wishListPage = myAccount.getSidebar().goToWishListPag();
		
		//selecting the default wish list and try to delete it. it will throw an exception
		
		wishListPage.createNewWishListRequest(dsm.getInputParameter("WISTLISTNAME"));	
		
		wishListPage.verifycreateWishListErrorMessage(dsm.getInputParameter("ERRORMESSAGE"));
	}
	
	/**
	 * Set a wish list as default wish list for Content Area	
	 */
	@Test
	public void FV2STOREB2C_1408(){
		
				
		//Creating two unique wish list name
		String wishList1 = RandomStringUtils.randomAlphanumeric(8);
		String wishList2 = RandomStringUtils.randomAlphanumeric(8);
		
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//cleanup
		myAccount.getSidebar().goToWishListPag().deleteAllWishLists();	
		
		//Creating two wish list
		MyWishListPage wishListPage = myAccount
									.getSidebar()
									.goToWishListPag()
									.createNewWishList(wishList1)
									.createNewWishList(wishList2);
		
		//performing search for getting the product listing
		SearchResultsPage searchResultPage = myAccount.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		
		//adding two product in wishList1
		//adding two product to default wish list
		ProductDisplayPage pdp = searchResultPage.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT1"));
				pdp.getShopperActionsWidget().addToExistingWishList(wishList1);
		
		searchResultPage = myAccount.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		
		pdp = searchResultPage.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT2"));
		pdp.getShopperActionsWidget().addToExistingWishList(wishList1);
		
		//adding two product in wishList2
		searchResultPage = myAccount.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		
		pdp = searchResultPage.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT3"));
		pdp.getShopperActionsWidget().addToExistingWishList(wishList2);

		searchResultPage = myAccount.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		
		pdp = searchResultPage.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT4"));
		pdp.getShopperActionsWidget().addToExistingWishList(wishList2);
		
		//Go to the My wish list page
		wishListPage = searchResultPage.getHeaderWidget().goToMyWishList();
		
		//verifying the products in wishList1 in content area
		wishListPage.selectWishList(wishList1).verifyProductInWishList(dsm.getInputParameter("PRODUCT1"))
					.verifyProductInWishList(dsm.getInputParameter("PRODUCT2"));
		
		//verifying the products in wishList2 in content area
		wishListPage.selectWishList(wishList2).verifyProductInWishList(dsm.getInputParameter("PRODUCT3"))
		.verifyProductInWishList(dsm.getInputParameter("PRODUCT4"));
		
		//setting the wishList1 as default wish list for content area
		wishListPage.selectWishList(wishList1);
		
		//moving away from the wish list page.
		searchResultPage = wishListPage.getHeaderWidget().performSearch(SearchResultsPage.class, "dress");
		
		//Going to wish list page and verifying the default wish list
		
		wishListPage = searchResultPage.getHeaderWidget().goToMyWishList();
		wishListPage.verifyProductInWishList(dsm.getInputParameter("PRODUCT1")).verifyProductInWishList(dsm.getInputParameter("PRODUCT2"));
		
		//Clean up by deleting all wish list
		wishListPage.deleteAllWishLists();
								
	}
	
	/**
	 * Add item from product detail page to default wish list (Wish List)		
	 */
	@Category(Sanity.class)
	@Test
	public void FV2STOREB2C_1409(){
		
				
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//performing search for getting the product listing
		SearchResultsPage searchResultPage = myAccount.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		
		//adding two product to default wish list
		ProductDisplayPage pdp = searchResultPage.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT1"));
		
		pdp.getShopperActionsWidget().addToWishlistLoggedIn();
		
		
		//Go to the My wish list page
		MyWishListPage wishListPage = pdp.getHeaderWidget().goToMyWishList();
		
		//verifying the products is showing under default wish list
		wishListPage.verifyProductInWishList(dsm.getInputParameter("PRODUCT1"));
		
		// clean up ( removing all item from this wish list
		wishListPage.removeAllItems();
					
	}
	
	/**
	 * Add item from product detail page to a new wish list		
	 */
	@Test
	public void FV2STOREB2C_1410(){
		
		//Getting some unique name for wish list
		String wishList  =  RandomStringUtils.randomAlphanumeric(8);
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = frontPage.getHeaderWidget().signIn().typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//performing a search result for a category
		SearchResultsPage searchResults = myAccount.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		
		//Clicking on first product
		ProductDisplayPage productDisplay = searchResults.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Adding this product to newly created wish list
		productDisplay.getShopperActionsWidget().createNewWishList(wishList);
		productDisplay.getShopperActionsWidget().addToExistingWishList(wishList);
		
		
		//Go to the wish list page
		MyWishListPage wishListPage = productDisplay.getHeaderWidget().goToMyWishList();
		
		//selecting the newly created wish list
		wishListPage.selectWishList(wishList);
		
		//verifying the product availability
		wishListPage.verifyProductInWishList(dsm.getInputParameter("PRODUCT_NAME"));
		
		
		//clean up task
		wishListPage.deleteAllWishLists();
		
			
		
		
	
	}
	
	/**
	 * Add item from product detail page to an existing non-default wish list		
	 */
	@Test
	public void FV2STOREB2C_1411(){
		
				
		//Creating two unique wish list name
		String wishList1 = RandomStringUtils.randomAlphanumeric(8);
		
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		
		
		//Creating one wish list
		MyWishListPage wishListPage = myAccount
									.getSidebar()
									.goToWishListPag()
									.deleteAllWishLists()
									.createNewWishList(wishList1);
									
		
		//performing search for getting the product listing
		SearchResultsPage searchResultPage = wishListPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		
		//Go to the product display page
		ProductDisplayPage productDisplayPage = searchResultPage.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT1"));
		
		//adding product to the existing wish list
		productDisplayPage.getShopperActionsWidget().addToExistingWishList(wishList1);
		
		//Go to the My wish list page
		wishListPage = productDisplayPage.getHeaderWidget().goToMyWishList();
		
		//verifying the products in wishList1 in content area
		wishListPage.selectWishList(wishList1).verifyProductInWishList(dsm.getInputParameter("PRODUCT1"));
				
		//Clean up
		wishListPage.deleteAllWishLists();
		
						
	}
	
	/**
	 *Shopper logs on and view his wish lists in Aurora store		
	 */
	@Test
	public void FV2STOREB2C_1412(){
		
				
		//Getting unique name for wish list
		String wishList = RandomStringUtils.randomAlphanumeric(8);
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//Go to the My Wish list page
		MyWishListPage myWishListPage = myAccount.getHeaderWidget().goToMyWishList();
		
		//Create a new wish list
		myWishListPage.createNewWishList(wishList);
		
		//performing search for getting the product listing
		SearchResultsPage searchResultPage = myWishListPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		
		//adding two item in newly created wish list
		ProductDisplayPage pdp = searchResultPage.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT1"));
		
		pdp.getShopperActionsWidget().addToExistingWishList(wishList);
		
		//add second item
		searchResultPage = myWishListPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));

		pdp = searchResultPage.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT2"));
		
		pdp.getShopperActionsWidget().addToExistingWishList(wishList);
		
		//log out from store
		signInPage = pdp.getHeaderWidget().openSignOutDropDownWidget().signOut().signIn();
		
	
		//Entering the Login Name and password
		myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
						
		
		
		//Go to the My wish list page
		MyWishListPage wishListPage = myAccount.getSidebar().goToWishListPag();
		
		//verifying the correct product have shown in wish list
		wishListPage.selectWishList(wishList).verifyProductInWishList(dsm.getInputParameter("PRODUCT1"));
		wishListPage.selectWishList(wishList).verifyProductInWishList(dsm.getInputParameter("PRODUCT2"));
		wishListPage.verifyNumberOfItems("2");
				
		//Clean up
		wishListPage.deleteAllWishLists();
		wishListPage.removeAllItems();
		
						
	}
	/**
	 *Delete item from a wish list	
	 */
	@Test
	public void FV2STOREB2C_1413(){
		
				
		//Getting unique name for wish list
		String wishList = RandomStringUtils.randomAlphanumeric(8);
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//Go to the My Wish list page
		MyWishListPage myWishListPage = myAccount.getHeaderWidget().goToMyWishList();
		
		//Create a new wish list
		myWishListPage.createNewWishList(wishList);
		
		//performing search for getting the product listing
		SearchResultsPage searchResultPage = myWishListPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		
		//adding two item in newly created wish list
		ProductDisplayPage pdp = searchResultPage.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT1"));
		
		pdp.getShopperActionsWidget().addToExistingWishList(wishList);
		
		//add second item
		searchResultPage = myWishListPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));

		pdp = searchResultPage.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT2"));
		
		pdp.getShopperActionsWidget().addToExistingWishList(wishList);
		
		
		//Go to the My wish list page
		MyWishListPage wishListPage = pdp.getHeaderWidget().goToMyWishList();
		
		//verifying the correct product have shown in wish list
		wishListPage.selectWishList(wishList).verifyProductInWishList(dsm.getInputParameter("PRODUCT1"));
		wishListPage.selectWishList(wishList).verifyProductInWishList(dsm.getInputParameter("PRODUCT2"));
		wishListPage = wishListPage.verifyNumberOfItems("2");
				
		//Deleting the product from wish list
		wishListPage = wishListPage.selectWishList(wishList).removeItem(dsm.getInputParameter("PRODUCT1")); //removing the first product
		wishListPage =	wishListPage.selectWishList(wishList).removeItem(dsm.getInputParameter("PRODUCT2")); //removing second product
		
		
		//clean up
		wishListPage.selectWishList(wishList).deleteThisWishList();
		
						
	}
	/**
	 * Add an item from wish list to shopcart	
	 */
	@Category(Sanity.class)
	@Test
	public void FV2STOREB2C_1414(){
		
				
		//Getting unique name for wish list
		String wishList = RandomStringUtils.randomAlphanumeric(8);
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//cleanup
		myAccount.getHeaderWidget().goToShoppingCartPage().removeAllItems();	
		
		//Go to the My Wish list page
		MyWishListPage myWishListPage = myAccount.getHeaderWidget().goToMyWishList();
		
		//first delete all the existing wish list
		myWishListPage = myWishListPage.deleteAllWishLists();
		
		//Create a new wish list
		myWishListPage.createNewWishList(wishList);
		
		//performing search for getting the product listing
		SearchResultsPage searchResultPage = myWishListPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		
		//adding two item in newly created wish list
		//adding two item in newly created wish list
		ProductDisplayPage pdp = searchResultPage.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT1"));
		
		pdp.getShopperActionsWidget().addToExistingWishList(wishList);
		
		//add second item
		searchResultPage = myWishListPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));

		pdp = searchResultPage.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT2"));
		
		pdp.getShopperActionsWidget().addToExistingWishList(wishList);
		
		
		//Go to the My wish list page
		MyWishListPage wishListPage = pdp.getHeaderWidget().goToMyWishList();
		
		//verifying the correct product have shown in wish list
		wishListPage.selectWishList(wishList).verifyProductInWishList(dsm.getInputParameter("PRODUCT1"));
		wishListPage.selectWishList(wishList).verifyProductInWishList(dsm.getInputParameter("PRODUCT2"));
		wishListPage = wishListPage.verifyNumberOfItems("2");
				
		//Adding one item from wish list to shop cart
		wishListPage = wishListPage.selectWishList(wishList).addToCart(dsm.getInputParameter("PRODUCT1"));
		
		//Go to the shop cart page and verify item got item to shop cart 
		ShopCartPage shopCartPage = wishListPage.getHeaderWidget().goToShoppingCartPage();
		shopCartPage.verifyItemInShopCart(dsm.getInputParameter("PRODUCT1"));						
		
		//clean up
		shopCartPage.getHeaderWidget().goToMyWishList().deleteAllWishLists();				
						
	}
}
