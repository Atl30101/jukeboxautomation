package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 *  Scenario: FV2STOREB2C_39
 * Details: Product attachments
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_39 extends AbstractAuroraSingleSessionTests
{	
	/**A variable to hold CMC**/
	private CMC cmc;
	
	@DataProvider
	private final TestDataProvider dsm;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * @param cmc
	 */
	@Inject
	public FV2STOREB2C_39(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CMC cmc)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		this.cmc = cmc;
	}
	
	
	/**
	 * A test to login to CMC and create an attachment for product.
	 * @throws Exception
	 */
	@Test
	public void FV2STOREB2C_3901() throws Exception
	{	

		//login to CMC
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//Select the store.
		cmc.selectStore(getConfig().getStoreName());
		
		//Select the locale.
		cmc.setLocale(dsm.getInputParameter("locale"));
		
		//select catalog.
		cmc.selectCatalog();
		
		cmc.deactivateActivity(dsm.getInputParameter("DEACTIVATE_ACTIVITY"));
		
		//Open the store in the browser.
		try {
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			frontPage.getSummerArrivalsAdWidget();
			throw new IllegalStateException("Expecting Aurora Front page to fail. Widget should be missing");
		} catch (IllegalStateException e) {
			//exepcting to fail, do nothing 
		}
	
		//Activate the newly created Activity.
		cmc.activateActivity(dsm.getInputParameter("DEACTIVATE_ACTIVITY"));
		
		//log off from cmc
		cmc.logoff();

		//Open the store in the browser.
		getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
	}
	
	/**
	 * Perform teardown operations after the test is done, whether it is successful or not.
	 * @throws Exception
	 */
	@After
	public void testScenarioTearDown() throws Exception
	{
	try
	{
		getLog().info("testScenarioTearDown running");
		
		//login to CMC
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//Select the store.
		cmc.selectStore(getConfig().getStoreName());
		
		//Select the locale.
		cmc.setLocale(dsm.getInputParameter("locale"));
		
		//select catalog.
		cmc.selectCatalog();
		
		//Activate the newly created Activity.
		cmc.activateActivity(dsm.getInputParameter("DEACTIVATE_ACTIVITY"));
		
		//log off from cmc
		cmc.logoff();
	}
	catch(RuntimeException e)
	{
		getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
	}
	}
}


