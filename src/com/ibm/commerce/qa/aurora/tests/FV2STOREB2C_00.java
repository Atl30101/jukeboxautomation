package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 * 
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CustomerRegisterPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.casl.util.CaslModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.OrgAdminConsole;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/** Scenario: PreSetup
 *  Details: This test should be run before any other tests in the bucket to ensure that the users common to many test
 *  cases are registered in the store and tooling as appropriate.
 */
@Category(Sanity.class)
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class, CaslModule.class})
public class FV2STOREB2C_00 extends AbstractAuroraSingleSessionTests
{
	   /**
	    * The internal copyright field.
     	*/
		public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

		//A variable to send requests to OrgadminConsole
		private OrgAdminConsole oac;
		private final CaslFixturesFactory f_CaslFixtures;
		
		@DataProvider
		private final TestDataProvider dsm;

		/**
		 * Test Class object constructor.
		 * 
		 * @param log
		 * @param config
		 * @param session
		 * @param oac
		 * @param dataSetManager
		 */
		@Inject
		public FV2STOREB2C_00(
				Logger log, 
				CaslFoundationTestRule caslTestRule,
				WcWteTestRule wcWteTestRule,
				OrgAdminConsole oac,
				TestDataProvider dataSetManager,
				CaslFixturesFactory p_CaslFixtures)
		{
			super(log, wcWteTestRule, caslTestRule);
			this.oac = oac;
			this.dsm = dataSetManager;
			f_CaslFixtures = p_CaslFixtures;
		}

	  /**
		* Creates administrative users that will be utilized in other scenarios thru out this bucket.
		* Pre-conditions: <ul>
		*					<li>Tester knows the Store URL (http://<hostname>/webapp/wcs/stores/servlet/en/auroraesite)</li>
		* 				</ul>
		* Post conditions: Registration Successful. The user is taken to My Account page.
		* @throws Exception
		*/
	@Test
	public void testCreateAdminUsers() throws Exception
	{
		//Logon to orgadminconsole using the admin account
		oac.logon(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"));
		
		//Create a Marketing Manager account 
		oac.createNewUser(dsm.getInputParameter("MKR_USERID"), 
				dsm.getInputParameter("MKR_FIRST_NAME") , 
				dsm.getInputParameter("MKR_LAST_NAME"), 
				dsm.getInputParameter("MKR_USERPASSWORD"), 
				dsm.getInputParameter("MKR_PASSWORD_VERIFY"), 
				dsm.getInputParameter("MKR_POLICYID"), 
				dsm.getInputParameter("MKR_STATUS"), 
				dsm.getInputParameter("MKR_LANGUAGE"), 
				dsm.getInputParameter("MKR_PARENTORG"), 
				dsm.getInputParameter("MKR_ADDRESS"), 
				dsm.getInputParameter("MKR_CITY"), 
				dsm.getInputParameter("MKR_STATE"), 
				dsm.getInputParameter("MKR_COUNTRY"), 
				dsm.getInputParameter("MKR_ZIPCODE"), 
				dsm.getInputParameter("MKR_EMAIL"), 
				dsm.getInputParameter("MKR_PASSWORD_EXPIRED"));
		
		
		//Assign role as a Marketing Manager
		oac.assignRoleToUser(dsm.getInputParameter("MKR_USERID"),
				dsm.getInputParameter("MKR_PARENTORG"), dsm.getInputParameter("MKR_ROLE"));

		//Create a Product Manager account 
		oac.createNewUser(dsm.getInputParameter("P_USERID"), 
				dsm.getInputParameter("P_FIRST_NAME") , 
				dsm.getInputParameter("P_LAST_NAME"), 
				dsm.getInputParameter("P_USERPASSWORD"), 
				dsm.getInputParameter("P_PASSWORD_VERIFY"), 
				dsm.getInputParameter("P_POLICYID"), 
				dsm.getInputParameter("P_STATUS"), 
				dsm.getInputParameter("P_LANGUAGE"), 
				dsm.getInputParameter("P_PARENTORG"), 
				dsm.getInputParameter("P_ADDRESS"), 
				dsm.getInputParameter("P_CITY"), 
				dsm.getInputParameter("P_STATE"), 
				dsm.getInputParameter("P_COUNTRY"), 
				dsm.getInputParameter("P_ZIPCODE"), 
				dsm.getInputParameter("P_EMAIL"), 
				dsm.getInputParameter("P_PASSWORD_EXPIRED"));
		
		//Assign role as a Product Manager
		oac.assignRoleToUser(dsm.getInputParameter("P_USERID"),dsm.getInputParameter("P_PARENTORG"), dsm.getInputParameter("P_ROLE"));
		
		//Create a Category Manager 
		oac.createNewUser(dsm.getInputParameter("C_USERID"), 
				dsm.getInputParameter("C_FIRST_NAME") , 
				dsm.getInputParameter("C_LAST_NAME"), 
				dsm.getInputParameter("C_USERPASSWORD"), 
				dsm.getInputParameter("C_PASSWORD_VERIFY"), 
				dsm.getInputParameter("C_POLICYID"), 
				dsm.getInputParameter("C_STATUS"), 
				dsm.getInputParameter("C_LANGUAGE"), 
				dsm.getInputParameter("C_PARENTORG"), 
				dsm.getInputParameter("C_ADDRESS"), 
				dsm.getInputParameter("C_CITY"), 
				dsm.getInputParameter("C_STATE"), 
				dsm.getInputParameter("C_COUNTRY"), 
				dsm.getInputParameter("C_ZIPCODE"), 
				dsm.getInputParameter("C_EMAIL"), 
				dsm.getInputParameter("C_PASSWORD_EXPIRED"));
		
		//Assign role as a Category Manager
		oac.assignRoleToUser(dsm.getInputParameter("C_USERID"),dsm.getInputParameter("C_PARENTORG"), dsm.getInputParameter("C_ROLE"));
		
		//Create a Seller account 
		oac.createNewUser(dsm.getInputParameter("S_USERID"), 
				dsm.getInputParameter("S_FIRST_NAME") , 
				dsm.getInputParameter("S_LAST_NAME"), 
				dsm.getInputParameter("S_USERPASSWORD"), 
				dsm.getInputParameter("S_PASSWORD_VERIFY"), 
				dsm.getInputParameter("S_POLICYID"), 
				dsm.getInputParameter("S_STATUS"), 
				dsm.getInputParameter("S_LANGUAGE"), 
				dsm.getInputParameter("S_PARENTORG"), 
				dsm.getInputParameter("S_ADDRESS"), 
				dsm.getInputParameter("S_CITY"), 
				dsm.getInputParameter("S_STATE"), 
				dsm.getInputParameter("S_COUNTRY"), 
				dsm.getInputParameter("S_ZIPCODE"), 
				dsm.getInputParameter("S_EMAIL"), 
				dsm.getInputParameter("S_PASSWORD_EXPIRED"));
		
		//Assign role as a Seller Manager
		oac.assignRoleToUser(dsm.getInputParameter("S_USERID"),dsm.getInputParameter("S_PARENTORG"), dsm.getInputParameter("S_ROLE"));
		
		//Log off from orgadminconsole
		oac.logoff();
					
	}
	
	
	/**
	 * Test case to register a Shopper from the store home page with valid inputs. This shopper can be used across multiple test cases in this bucket.
	 * @throws Exception
	 */
	@Test
	public void setupUser() throws Exception{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//attempt to sign in:
		try {
			signInPage.typeUsername(dsm.getInputParameter("LOGONID"))
				.typePassword(dsm.getInputParameter("PASSWORD"))
				.signIn();
		} catch (Exception e1) {
			//still on the sign in page, user isn't registered
			//Click on the registration button on the SignIn page
			CustomerRegisterPage crp= signInPage.registerCustomer();

			crp
			//type username
			.typeLogonId(dsm.getInputParameter("LOGONID"))
			
			//type first name
			.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
			
			//type last name
			.typeLastName(dsm.getInputParameter("LAST_NAME"))
			
			//type password
			.typePassword(dsm.getInputParameter("PASSWORD"))
			
			//Verify password
			.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
			
			//type street address
			.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
			
			//Select country
			.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
			
			//type or select state
			.selectStateOrProvince(dsm.getInputParameter("STATE"))
			
			//type city
			.typeCity(dsm.getInputParameter("CITY"))
			
			//type zipcode
			.typeZipCode(dsm.getInputParameter("ZIPCODE"))
			
			//type E-mail
			.typeEmail(dsm.getInputParameter("EMAIL"))
			
			//type home phone number
			.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
			
			//Select gender
			.selectGender(dsm.getInputParameter("GENDER"))
			
			//type mobile phone number
			.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
			
			//Update Allow Me Option check box
//			.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
			
//			//Check if preferred language drop down is visible
//			.verifyPreferredLanguageDropDownListPresent()	
			
			//Select preferred language
//			.selectPreferedCurrency(dsm.getInputParameter("PREFERRED_CURRENCY"))
			
			//Check if preferred currency drop down is visible
//			.verifyPreferredCurrencyDropDownListPresent()
			
//			//Selected preferred language
//			.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
			
			//Check if birthday selection drop down is visible
			.verifyBirthdaySelectionPresent()
			
			//Select  birth year
			.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
			
			//Select birth month
			.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
			
			//Select birth day
			.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
			//submit user registration
			.submit();
			
			// Complete two orders
			
			//Place order 1
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY",Double.class));
			orders.deletePaymentMethod();
			orders.addPayLaterPaymentMethod();
			orders.completeOrder();
			
			//Place order 2
			orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU2"), dsm.getInputParameterAsNumber("QTY",Double.class));
			orders.deletePaymentMethod();
			orders.addPayLaterPaymentMethod();
			orders.completeOrder();
		}
	}	
	/*
	@Test
	public void setupUserOrder() throws Exception{
		//Place order 1
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY",Double.class));
		orders.deletePaymentMethod();
		orders.addPayLaterPaymentMethod();
		orders.completeOrder();
		
		
		//Place order 2
		orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID2"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		orders.addItem(dsm.getInputParameter("SKU2"), dsm.getInputParameterAsNumber("QTY",Double.class));
		orders.deletePaymentMethod();
		orders.addPayLaterPaymentMethod();
		orders.completeOrder();
	}*/
}

