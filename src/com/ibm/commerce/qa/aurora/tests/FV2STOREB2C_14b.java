package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2013
 *
 *  US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.casl.keys.CaslKeysFactory;
import com.ibm.commerce.casl.keys.StorefrontKeys;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.MyWishListPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.WishListFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;




/** 		
 * Scenario: FV2STOREB2C_14
 * Details: The objective of this test scenario is testing Shopper adds, removes and shares items to/from wish list
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class})
public class FV2STOREB2C_14b extends  AbstractAuroraSingleSessionTests {

	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;


	private CaslFixturesFactory f_caslFixtures;


	private CaslKeysFactory f_caslKeys;
	
	
	
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dataSetManager
	 * @param p_caslFixtures 
	 * @param p_caslKeys 
	 */
	@Inject
	public FV2STOREB2C_14b(Logger log, CaslFoundationTestRule caslTestRule, WcWteTestRule wcWebTestRule, 
			TestDataProvider dataSetManager, CaslFixturesFactory p_caslFixtures, CaslKeysFactory p_caslKeys)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		f_caslFixtures = p_caslFixtures;
		f_caslKeys = p_caslKeys;
	}
	

	/**
	 * Try creating a wish list with duplicated name or rename a wish list with name already exist	
	 */
	@Test
	public void FV2STOREB2C_1417(){
		
				
		//Getting unique name for wish list
		String wishList1 = RandomStringUtils.randomAlphanumeric(8);
		String wishList2 = RandomStringUtils.randomAlphanumeric(8);
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//Go to the My Wish list page
		MyWishListPage myWishListPage = myAccount.getHeaderWidget().goToMyWishList();
		
		//first delete all the existing wish list
		myWishListPage = myWishListPage.deleteAllWishLists();
		
		//Create two wish list
		myWishListPage = myWishListPage.createNewWishList(wishList1).createNewWishList(wishList2);
		
		
		boolean passStatus = false;
		//try to create wish list with duplicate name
		try
		{
			myWishListPage = myWishListPage.createNewWishList(wishList1).createNewWishList(wishList2);
		}catch(RuntimeException e)
		{
			passStatus = true;
			myWishListPage.cancelInCreateWishListDialogue();

		}
		
		if(!passStatus)
			throw new IllegalStateException("Wish List should not get created with duplicate name");
		passStatus = false;
		
		//Try to rename a wish list with existing name
		try
		{
		myWishListPage = myWishListPage.updateWishListName(wishList1, wishList2);
		}
		catch(RuntimeException e)
		{
			passStatus = true;
			myWishListPage.cancelInEditWishListDialogue();
		}
		
		if(!passStatus)
			throw new IllegalStateException("Renaming of wish list name with already exist should not happen");
		
		//clean up
		myWishListPage.deleteAllWishLists();
							
	}
	
	/**
	 * Ensure that wish list UI can properly handle non-alphanumeric input	
	 */
	@Test
	public void FV2STOREB2C_1418(){
		
				
		//Getting unique name for wish list from the character set !, @, #, $, %, ^, &, *, (, ), ', ., , /, ?, :, ;, ", {, }, [, ], |, \,   
		char[] specialCharacterSet = {'!','@', '#', '$', '%', '^', '&', '*', '(', ')', '.', '/', '?', ':', ';', '"', '{', '}', '[', ']', '|', '\\'};  

		String wishList1 = RandomStringUtils.random(9,specialCharacterSet);
		String wishList2 = RandomStringUtils.random(9,specialCharacterSet);
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//Go to the My Wish list page
		MyWishListPage myWishListPage = myAccount.getHeaderWidget().goToMyWishList();
		
		//first delete all the existing wish list
		myWishListPage = myWishListPage.deleteAllWishListsWithoutVerify();
		
		//Create two wish list
		myWishListPage = myWishListPage.createNewWishListWithoutVerify(wishList1);
		
							
	}
	
	/**
	 * Wish list page pagination works properly	
	 */
	@Test
	public void FV2STOREB2C_1419(){
		
		//use service layer to populate wishlists
		int storeId = f_caslKeys.createToolingKeys().getStoreIdByName(getConfig().getStoreName());
		WishListFixture wishList = f_caslFixtures.createWishListFixture(dsm.getInputParameter("LOGINID"), dsm.getInputParameter("PASSWORD"), storeId); 
		
		String wishListId = wishList.createWishList(dsm.getInputParameter("WISHLIST_NAME"));
		
		StorefrontKeys keys = f_caslKeys.createStorefrontKeys(storeId + "");
		
		String catEntry = keys.getCatEntryIdByPartNumber( dsm.getInputParameter("STARTING_SKU"));
		
	
		String  totalNoOfItem = dsm.getInputParameter("TOTAL_ITEM");
		int limit = Integer.parseInt(totalNoOfItem);
		String pageSize = dsm.getInputParameter("TOTAL_NO_OF_ITEM_PER_PAGE");
		
		for (int i = 0; i < limit ; i++) {
			Long catEntryId =  Long.parseLong(catEntry);
			
			wishList.addItem( wishListId, catEntryId + i, dsm.getInputParameterAsNumber("QTY", Double.class));
		}
		
		
		
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//Go to the default wish list and remove all item under default wish list
		MyWishListPage myWishListPage = myAccount.getSidebar().goToWishListPag();
		
		myWishListPage =  myWishListPage.selectWishList(dsm.getInputParameter("WISHLIST_NAME")).verifyNumberOfItems(totalNoOfItem);
		
		Double noOfPage = Math.ceil(Double.parseDouble(totalNoOfItem)/Double.parseDouble(pageSize));
	
		//validate no page in wish list
		myWishListPage = myWishListPage.verifyNumberOfPageInWishList(Integer.toString(noOfPage.intValue()));
		
		//removing some item such that total no of pagination decreased
		myWishListPage =  myWishListPage.removeItemByPosition(1)
										.removeItemByPosition(2)
										.removeItemByPosition(3);
		
		//after removing three item verifying the pagination
		myWishListPage = myWishListPage.verifyNumberOfPageInWishList(Integer.toString(noOfPage.intValue()-1));
		
		//clean up
		myWishListPage.deleteThisWishList();
		
						
	}
	
	/**
	 * Wish list return to 1st page when items in second page removed
	 */
	@Test
	public void FV2STOREB2C_1420(){
		int storeId = f_caslKeys.createToolingKeys().getStoreIdByName(getConfig().getStoreName());
		WishListFixture wishList = f_caslFixtures.createWishListFixture(dsm.getInputParameter("LOGINID"), dsm.getInputParameter("PASSWORD"), storeId); 
		
		String wishListId = wishList.createWishList(dsm.getInputParameter("WISHLIST_NAME"));
		
		StorefrontKeys keys = f_caslKeys.createStorefrontKeys(storeId + "");
		
		String catEntry = keys.getCatEntryIdByPartNumber( dsm.getInputParameter("STARTING_SKU"));
		
	
		String  totalNoOfItem = dsm.getInputParameter("TOTAL_ITEM");
		int limit = Integer.parseInt(totalNoOfItem);
		String pageSize = dsm.getInputParameter("TOTAL_NO_OF_ITEM_PER_PAGE");
		
		for (int i = 0; i < limit ; i++) {
			Long catEntryId =  Long.parseLong(catEntry);
			
			wishList.addItem( wishListId, catEntryId + i, dsm.getInputParameterAsNumber("QTY", Double.class));
		}
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//Go to the default wish list and remove all item under default wish list
		MyWishListPage myWishListPage = myAccount.getSidebar().goToWishListPag();
	
		
		//validate no of item in wish list 
		myWishListPage =  myWishListPage.selectWishList(dsm.getInputParameter("WISHLIST_NAME")).verifyNumberOfItems(totalNoOfItem);
		
		Double noOfPage = Math.ceil(Double.parseDouble(totalNoOfItem)/Double.parseDouble(pageSize));
	
		//validate number page in wish list
		myWishListPage = myWishListPage.verifyNumberOfPageInWishList(Integer.toString(noOfPage.intValue()));
		
		//removing some item such that total no of pagination decreased
		myWishListPage =  myWishListPage.removeItemByPosition(1)
										.removeItemByPosition(2);
										
		
		//after removing three item verifying the pagination
		myWishListPage = myWishListPage.verifyNumberOfPageInWishList(Integer.toString(noOfPage.intValue()-1));
		
		myWishListPage =  myWishListPage.verifyNumberOfItems(String.valueOf(Integer.parseInt(totalNoOfItem) -2));
		
		//clean up
		myWishListPage.deleteThisWishList();
		
						
	}
}
