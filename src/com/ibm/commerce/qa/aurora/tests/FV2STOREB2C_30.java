package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.ErrorPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.MyPersonalInfoPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/**
 * Scenario: FV2STOREB2C_30
 * Details: Session timeout 
 *
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_30 extends AbstractAuroraSingleSessionTests
{
	@DataProvider
	private final TestDataProvider dsm;
	private final CaslFixturesFactory f_CaslFixtures;
	
		/**
		 * @param log
		 * @param config
		 * @param session
		 * @param dataSetManager
		 */
		@Inject
		public FV2STOREB2C_30(
				Logger log, 
				CaslFoundationTestRule caslTestRule,
				WcWteTestRule wcWebTestRule,
				TestDataProvider dataSetManager,
				CaslFixturesFactory p_CaslFixtures
				)
		{
			super(log, wcWebTestRule, caslTestRule);
			this.dsm = dataSetManager;
			f_CaslFixtures = p_CaslFixtures;
		}
	/**
	 * Test case to test store display page times out due to extended inactivity.
	 * Pre-conditions: <ul>
	 * 					<li>Tester knows the Store URL (http://<hostname>/webapp/wcs/stores/servlet/en/auroraesite)</li>
	 * 					<li>Product Quick Info, AJAX add to shopping cart, AJAX checkout, AJAX My Account</li>				
	 *					<li>Registered shopper TestUser with password Test1User exists</li>
	 * 				   </ul>

	 * Post conditions: Session time out message is displayed: "Your session has timed out and you have been successfully logged off. Click Log in to access the store again."
	 * @throws Exception
	 */	
	@Test
	public void testFV2STOREB2C_3001() throws Exception
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter valid username
		HeaderWidget header= signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				//Enter valid password
				.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
				//click Sign In button
				.signIn().closeSignOutDropDownWidget();
		//Go to category page Apparel -> Dresses
		CategoryPage subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		//Go to the product page of an item
		ProductDisplayPage product = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("ITEM_NAME"));
		//Select the appropriate attributes
		product.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE"));
		//Wait until session timeout occurs
		Thread.sleep(2100000);
	
		//Click "Add to Cart" button. Timeout error page is displayed
		ErrorPage error = product.addToCartTimeout();
		//Go to Sign In page from the header
		signInPage = error.getHeaderWidget().signIn();
		//Enter a valid username
		header = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		//Enter a valid password
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
		//Click Sign In button
		.signIn().closeSignOutDropDownWidget();

		//Go to category page Apparel -> Dresses
		subCat = header.goToCategoryPageByHierarchy(CategoryPage.class,dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		//Go to the product page of an item
		product = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("ITEM_NAME"));
		//Select the appropriate attributes
		product.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE"));
		//Add the product to the shopping cart
		product.addToCart();
		//Go to the shopping cart from the mini shop cart in the header
		ShopCartPage shopCart = product.getHeaderWidget().goToShoppingCartPage();
		//Click on the checkout button
		ShippingAndBillingPage shipping = shopCart.continueToNextStep();
		//Select Billing method
		OrderSummarySingleShipPage orderSummary = shipping.selectPaymentMethod(dsm.getInputParameter("PAYMENT_METHOD"))
		//Continue as single shipment
		.singleShipNext();
		//Complete the order
		orderSummary.completeOrder();		
	}
	/**
	 * Test case to test shop cart page times out due to extended inactivity.
	 * Pre-requisites: <ul>
	 * 					<li>Tester knows the Store URL (http://<hostname>/webapp/wcs/stores/servlet/en/auroraesite)</li>
	 * 					<li>Product Quick Info, AJAX add to shopping cart, AJAX checkout, AJAX My Account</li>	
	 * 					<li>Registered shopper TestUser with password Test1User exists</li>
	 * 				</ul>
	 * Post conditions: Session time out message is displayed: "Your session has timed out and you have been successfully logged off. Click Log in to access the store again."
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_3002() throws Exception
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter valid username
		HeaderWidget header= signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				//Enter valid password
				.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
				//Click Sign In button
				.signIn().closeSignOutDropDownWidget();
		//Go to category page Apparel -> Dresses
		CategoryPage subCat = header.goToCategoryPageByHierarchy(CategoryPage.class,dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		//Go to the product page of an item
		ProductDisplayPage product=  subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("ITEM_NAME"));
		//Select the appropriate attributes
		product.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE"));
		
		product.addToCart();
		//Add the product to the shopping cart
		ShopCartPage shopCart = product.getHeaderWidget().goToShoppingCartPage();
		//Wait until session timeout occurs
		Thread.sleep(2100000);
		
		//Click on Checkout button
		ErrorPage error = shopCart.continueToNextStepTimeout();
		//Click on the Sign In link from the header
		signInPage  = error.getHeaderWidget().signIn();
		//Enter a valid username
		ShippingAndBillingPage shipping = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		//Enter a valid password
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
		//Click on the Sign In page and continue with the checkout
		.signInToShippingAndBillingPage();
		//Go to the shopping cart page from the header
		shipping.getHeaderWidget().goToShoppingCartPage()
		//Verify the product still in the shopping cart
		.verifyItemInShopCart(dsm.getInputParameter("ITEM_NAME"));
		
	}
	/**
	 * Test case to test store display page times out due to extended inactivity.
	 * Pre-requisites: <ul>
	 * 					<li>Tester knows the Store URL (http://<hostname>/webapp/wcs/stores/servlet/en/auroraesite)</li>
	 * 					<li>Product Quick Info, AJAX add to shopping cart, AJAX checkout, AJAX My Account</li>	
	 * 					<li>Registered shopper TestUser with password Test1User exists</li>
	 * 				</ul>
	 * Post conditions: Session time out message is displayed: "Your session has timed out and you have been successfully logged off. Click Log in to access the store again."
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_3003() throws Exception
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter a valid username
		HeaderWidget header = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				//Enter a valid password
				.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
				//Click Sign In button
				.signIn().closeSignOutDropDownWidget();
		//Go to category page Apparel -> Dresses
		CategoryPage subCat  = header.goToCategoryPageByHierarchy(CategoryPage.class,dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
			//Go to product Page of an item
			ProductDisplayPage product = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("ITEM_NAME"));
			//Select Appropriate attributes
			product.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE"));
			//Add the item to the shopping cart
			product.addToCart();
		//Go to shopping cart page from the header
		ShopCartPage shopCart = product.getHeaderWidget().goToShoppingCartPage();
		//Click the Checkout button
		ShippingAndBillingPage shipping = shopCart.continueToNextStep()
		//Select billing method
		.selectPaymentMethod(dsm.getInputParameter("PAYMENT_METHOD"));
		//Wait until session timeout occurs
			Thread.sleep(2100000);
		
		//Click on the Next button, timeout error page is displayed
		ErrorPage error  = shipping.singleShipNextTimeout();
		//Go to Sign In page from the header
		signInPage = error.getHeaderWidget().signIn();
		//Enter a valid username
		header= signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				//Enter a valid password
				.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
				//Click Sign In button
				.signIn().closeSignOutDropDownWidget();
		//Go to category page Apparel -> Dresses
		subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		//Go to the product page of an item
		product = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("ITEM_NAME"));
		//Select appropriate attributes
		product.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE"));
		product.addToCart();
		//Go to the shopping cart page from the mini shop cart in the header
		shopCart = product.getHeaderWidget().goToShoppingCartPage();
		//Click the Checkout button
		shipping = shopCart.continueToNextStep()
		//Select the billing method
		.selectPaymentMethod(dsm.getInputParameter("PAYMENT_METHOD"));
		//Click Next button
		OrderSummarySingleShipPage ordersummary =shipping.singleShipNext();
		//Complete the order
		ordersummary.completeOrder();		
	}
	/**
	 * Test case to test my account page times out due to extended inactivity.
	 * Pre-requisites: <ul>
	 *					<li>Tester knows the Store URL (http://<hostname>/webapp/wcs/stores/servlet/en/auroraesite)</li>
	 * 					<li>Product Quick Info, AJAX add to shopping cart, AJAX checkout, AJAX My Account</li>	
	 * 					<li>Registered shopper TestUser with password Test1User exists</li>
	 * 				</ul>
	 * Post conditions: Session time out message is displayed: "Your session has timed out and you have been successfully logged off. Click Log in to access the store again."
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_3004() throws Exception
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		//Enter a  valid username
		MyAccountMainPage myAccount= signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				//Enter a valid password
				.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
				//Click the Sign In button
				.signIn().closeSignOutDropDownWidget().goToMyAccount();
		//Click on the My Personal Information link on the left sidebar
		MyPersonalInfoPage personalInfoPage = myAccount.goToPersonalInformationPageForEditing()	
		//Update the E-mail input field
		.typeEMail(dsm.getInputParameter("USER_EMAIL"));
		//Wait until session Timeout occurs
		Thread.sleep(2100000);
		
		//Click Submit button, and timeout error page is displayed
		ErrorPage error = personalInfoPage.updateTimeout();
		//Click on the SignIn page link on the header
		myAccount = error.getHeaderWidget().signIn()
		//Enter a valid username
		.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				//Enter a valid password
				.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
				//Click Sign In button
				.signIn().closeSignOutDropDownWidget().goToMyAccount();
		//Click on the My Personal Information link on the left sidebar
		personalInfoPage = myAccount.goToPersonalInformationPageForEditing();
		//Verify that the E-mail field is not updated
		personalInfoPage.verifyIncorrectEmailAddress(dsm.getInputParameter("USER_EMAIL"));
	
	}

	/**
	 * Tear down ran after every test case
	 * @throws Exception 
	 */
	@After
	public void tearDown() throws Exception{
		try
		{
			//Open Auroraesite store
			AuroraFrontPage frontPage = getSession().continueToPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
			//Remove all items from the shopping cart
			//frontPage.getHeaderWidget().goToShoppingCartPage().removeAllItems();	
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
			orders.removeAllItemsFromCart();
			orders.deletePaymentMethod();
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}
	}
}
