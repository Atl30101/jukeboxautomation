package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.CustomerRegisterPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 * Scenario: FV2STOREB2C_02
 * Shopper Signs in to the Store				
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_02 extends AbstractAuroraSingleSessionTests
{
	
	/**
	 * The internal copyright field.
	 */
	 public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	 
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	private final CaslFixturesFactory f_CaslFixtures;

	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 */
		@Inject
		public FV2STOREB2C_02(
				Logger log, 
				WcWteTestRule wcWebTestRule,
				CaslFoundationTestRule caslTestRule,
				TestDataProvider dataSetManager,
				CaslFixturesFactory p_CaslFixtures)
		{
			super(log, wcWebTestRule, caslTestRule);
			this.dsm = dataSetManager;
			f_CaslFixtures = p_CaslFixtures;
		}
		
		
	/**
	 * Test case to login to AuroraEsite with valid user id and password.
	 */	
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_0201()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter a valid username
		signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		
		//Enter a valid password
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
		
		//Click Sign In
		.signIn();					
	}
	
	/**
	 * Test case to login to AuroraEsite with valid user id, but invalid password.
	 */	
	@Test
	public void testFV2STOREB2C_0202()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter a valid username
		signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		
		//Enter an invalid password
				.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
				
		//Click Sign In, sign in not successful
				.unsuccessfulSignIn();
		
		//Verify the longin error message
		signInPage.verifyIncorrectLogonMessage()
		
		//Enter a valid password
				.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD_RETRY"))
				
				//Click Sign In
				.signIn();

	}
	/**
	 * Test case to login to AuroraEsite with valid user id, but empty password.
	 */
	@Test
	public void testFV2STOREB2C_0203()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter a valid username
		signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		
		//Click Sign In, sign in not successful
		.unsuccessfulSignInPassword();
		
		//Verify the error message
		signInPage.verifyEmptyPasswordMessage()
		
		//Enter a valid password
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
		
		//Click Sign In
		.signIn();

	}	
	/**
	 * Test case to login to Auroraesite with invalid user id and invalid password.
	 */
	@Test
	public void testFV2STOREB2C_0204()
	{
		//Open Auroraesite store		
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter an invalid username
		signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		
		//Enter an invalid password
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
		
		//Click Sign In, sign in not successful
		.unsuccessfulSignIn()
		
		//Verify the error message
		.verifyIncorrectLogonMessage();

	}
	/**
	 * Test case to login to AuroraEsite with invalid user id and empty password.
	 */
	@Test
	public void testFV2STOREB2C_0205()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter a invalid username
		signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		
		//Click Sign In, sign in not successful
		.unsuccessfulSignInPassword()
		
		//Verify the error message
		.verifyEmptyPasswordMessage();

	}
	/**
	 * Test case to login to Auroraesite with empty user id and empty password.
	 */
	@Test
	public void testFV2STOREB2C_0206()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Click Sign In, sign in not successful
		signInPage.unsuccessfulSignInLogonId()
		
		//Verify the empty error message
		.verifyEmptyUsernamePasswordMessage();

	}	
	/**
	 * Test case to login to AuroraEsite with valid user id and invalid password twice.
	 */
	@Test
	public void testFV2STOREB2C_0207()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter a valid username
		signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		
		//Enter an invalid password
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
		
		//Click Sign In, sign in not successful
		.unsuccessfulSignIn();
		
		//Verify the error message
		signInPage.verifyIncorrectLogonMessage()
		
		//Enter an invalid password
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
		
		//Click Sign In, sign in not successful
		.unsuccessfulSignIn().unsuccessfulSignIn()
		
		//Verify the error message
		.verifyShortWaitMessage();
		
	}
	
	
	/**
	 * Test case to login to AuroraEsite with valid user id and invalid password 6 times so that user account is disabled.
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_0208() throws Exception
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Click on the registration button on the SignIn page
		CustomerRegisterPage crp = signInPage.registerCustomer();
		
		String userID = crp.getUniqueID();
		
		//type username
		MyAccountMainPage myAccount = crp.typeLogonId(userID)
				
				//type first name
				.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
				
				//type last name
				.typeLastName(dsm.getInputParameter("LAST_NAME"))
				
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				
				//Verify password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
				
				//type street address
				.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
				
				//Select country
				.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
				
				//type or select state
				.selectStateOrProvince(dsm.getInputParameter("STATE"))
				
				//type city
				.typeCity(dsm.getInputParameter("CITY"))
				
				//type zipcode
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				
				//type E-mail
				.typeEmail(dsm.getInputParameter("EMAIL"))
				
				//type home phone number
				.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
				
				//Select gender
				.selectGender(dsm.getInputParameter("GENDER"))
				
				//type mobile phone number
				.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
				
				//Update Allow Me Option check box
				.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
				
				//Check if preferred language drop down is visible
				.verifyPreferredLanguageDropDownListPresent()			
				
				//Select preferred language
				.selectPreferedCurrency(dsm.getInputParameter("PREFERRED_CURRENCY"))
				
				//Check if preferred currency drop down is visible
				.verifyPreferredCurrencyDropDownListPresent()
				
				//Selected preferred language
				.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
				
				//Check if birthday selection drop down is visible
				.verifyBirthdaySelectionPresent()
				
				//Select  birth year
				.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
				
				//Select birth month
				.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
				
				//Select birth day
				.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
				
				//Submit registration, the user is taken to the MyAccounts page
				.submit();			
		
		//Sign out from the store
		myAccount.getHeaderWidget().getSignOutDropDownWidget().signOut();
		
		//Wait
		//Thread.sleep(180000);
		
		signInPage = myAccount.getHeaderWidget().signIn();
		//Enter a valid username
		signInPage.typeUsername(userID);

	for(int i =0 ; i< 6; i++ )
	{
		getLog().info("Sign In Attempt: " + i);
		
		//Enter an invalid password
		signInPage.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
		
		//Click Sign In, sign in not successful
		.unsuccessfulSignIn();
		
		//If attempted less than 4 times
			try {
				//Verify the error message
				signInPage.verifyIncorrectLogonMessage();
			} catch (Exception e) {
				signInPage.verifyTimeOutLogonMessage();
				//logged in too many times in a short period of time, introduce a wait
				getLog().info("Store is forcing a wait before logging again, waiting for a minute");
				i--;
				//Wait
				Thread.sleep(30000);
			}
		
		
			
	}
		//Wait
		Thread.sleep(60000);
		
		//On the 7th attempt, enter a valid password
		signInPage.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD_RETRY"))
		
		//Click Sign In, sign in not successful
		.unsuccessfulSignIn()
		
		//Verify user id is locked
		.verifyAccountLockMessage();
	}
	
	
	
	/**
	 * Test case to test registered shopper adding an item to cart with remember me checked when logging on.
	 */
	@Test
	public void testFV2STOREB2C_0210()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();	
		
		//Enter a valid username
		signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				
		//Enter a valid password
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
		
		//Click Sign In
		.signIn()
		
		//Close the sign out dropdown widget
		.closeSignOutDropDownWidget();
		
		//Go to category page from the Department Dropdown 
		CategoryPage department =signInPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, 
				dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//Go to the product page of an item
		ProductDisplayPage productDisplay = department.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("ITEM_NAME"));
		
		//Select the appropriate attribute
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE"));
		
		//Add the item to the cart
		productDisplay.addToCart();

		//Confirm that the itme is in the mini shopping cart
		productDisplay.getHeaderWidget().verifyProductNameInMiniCart(dsm.getInputParameter("ITEM_NAME"));
		
		//close the browser
		getSession().state().resetWithCurrentState();
		
		//Open another browser and go to Auroraesite
		AuroraFrontPage frontPage2 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Verify that the item is no longer in the mini shopping cart
		frontPage2.getHeaderWidget().verifyEmptyMiniShopCart();
		
	}
	
	
	/**
	 * Test case to login to AuroraEsite with valid user id and password.
	 */	
	@Test
	public void testFV2STOREB2C_0211()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signInViaQuickLinks();
		
		//Enter a valid username
		signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		
		//Enter a valid password
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
		
		//Click Sign In
		.signIn();					
	}
	
	/**
	 * Tear down ran after every test case
	 */
	@After
	public void tearDown(){
	
		try
		{
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
			orders.deletePaymentMethod();
			orders.removeAllItemsFromCart();
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

		
	}
}
