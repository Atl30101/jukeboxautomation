package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2010
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.RegisteredShippingBillingInfoPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.MemberFixture;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;



/** 		
 * Scenario: FV2STOREB2C_13
 * Details: The objective of this test scenario is testing different cases of a shopper checking out an order with different billing methods and addresses.
 */
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class})
public class FV2STOREB2C_13 extends AbstractAuroraSingleSessionTests
{

	
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	private CaslFixturesFactory f_CaslFixtures;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * @param p_CaslFixtures 
	 */
	@Inject
	public FV2STOREB2C_13(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_CaslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		f_CaslFixtures = p_CaslFixtures;
	}
	
	/**
	 * Shopper checks out by specifying billing information
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_1301()
	{		
		
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY",Double.class));
		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
		
		//click on the checkout button
		ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
		
		//Confirm ship as complete option is selected.
		shippingAndBilling.verifyShipAsCompleteIsChecked();
		
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm the correct shipping address is selected
		shippingAndBilling.verifyShippingAddressSelected(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
		
		//select Visa as the payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
		
		//Enter credit card number
		shippingAndBilling.typeCreditCardNumber(dsm.getInputParameter("CARD_NUM"));
		
		//Select a valid expiry Year
		shippingAndBilling.selectExpirationYear(dsm.getInputParameter("CARD_YEAR"));
		
		//Select a valid expiry Month
		shippingAndBilling.selectExpirationMonth(dsm.getInputParameter("CARD_MONTH"));
		
		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	
		
		//Confirm payment page on order summary page
		singleOrderSummary.verifyBillingMethod(dsm.getInputParameter("PAY_METHOD"),dsm.getInputParameter("PAYMENT_NUMBER"));
		
		//Confirm product name
		singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm item quantity
		singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_QTY"));
		
		//Confirm total price of the item.
		singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_TOTAL"));
		
		//Confirm the total price of the order
		singleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL"));						
		
		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
		
		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();
	}
	
	/**
	 * Test case to Checkout with single payment method using a check while editing an existing billing address
	 */
	@Test
	public void testFV2STOREB2C_1302() 
	{
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY",Double.class));
		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
		
		//click on the checkout button
		ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
		
		//Confirm ship as complete option is selected.
		shippingAndBilling.verifyShipAsCompleteIsChecked();
		
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm the correct shipping address is selected
		shippingAndBilling.verifyShippingAddressSelected(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
		
		//Select billing address
		shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
		
		//Click on edit link on billing section
		RegisteredShippingBillingInfoPage billingInfoPage =	shippingAndBilling.editBillingAddress();
		
		//Edit street address
		billingInfoPage.typeAddress(dsm.getInputParameter("STREET_ADDRESS"));
		
		//Click on submit button
		billingInfoPage.submitSuccessfulForm();
		
		//verify billing address has been updated
		shippingAndBilling.verifyBillingAddressDetails("2", dsm.getInputParameter("STREET_ADDRESS"));
		
		//Select number of payment
		shippingAndBilling.selectNoOfPaymentMethods(dsm.getInputParameter("NUMBER_OF_PAYMENTS_LABEL"));
		
		//select check as the payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
		
		//Enter information
		shippingAndBilling.typeBankInformation((dsm.getInputParameter("ROUTING_NO")), (dsm.getInputParameter("ACCOUNT_NO")));
		
		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	
		
		//Confirm payment page on order summary page
		singleOrderSummary.verifyBillingMethod(dsm.getInputParameter("PAY_METHOD"),dsm.getInputParameter("PAYMENT_NUMBER"));
		
		//Confirm product name
		singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm item quantity
		singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_QTY"));
		
		//Confirm total price of the item.
		singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_TOTAL"));
		
		//Confirm the total price of the order
		singleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL"));						
		
		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
		
		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();
	}
	
	/**
	 * Test case to checkout with single payment method using a check by creating a new billing address
	 */
	@Test
	public void testFV2STOREB2C_1303() 
	{
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY",Double.class));
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
		
		//click on the checkout button
		ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
		
		//Confirm ship as complete option is selected.
		shippingAndBilling.verifyShipAsCompleteIsChecked();
		
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm the correct shipping address is selected
		shippingAndBilling.verifyShippingAddressSelected(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
		
		//Click on edit link on billing section
		RegisteredShippingBillingInfoPage shippingAndBillingInfo =	shippingAndBilling.createBillingAddress();
			
		//Enter info and submit the new address creating request
		shippingAndBillingInfo.typeRecipient(dsm.getInputParameter("RECIPIENT"))
		 .typeLastName(dsm.getInputParameter("LAST_NAME"))		 
		 .typeAddress(dsm.getInputParameter("STREET_ADDRESS"))
		 .typeCity(dsm.getInputParameter("CITY"))
		 .selectCountry(dsm.getInputParameter("COUNTRY"))
		 .selectStateOrProvince(dsm.getInputParameter("PROVINCE"))
		 .typeZipCode(dsm.getInputParameter("ZIPCODE"))
		 .typePhone(dsm.getInputParameter("PHONE_NUMBER"))
		 .typeEmail(dsm.getInputParameter("EMAIL"))
		 .applyToBothShippingBilling()
		 .submitSuccessfulForm();
		
		//Wait for billing address to display
		shippingAndBilling.waitForBillingAddress(dsm.getInputParameter("RECIPIENT"));
		
		//Select number of payment
		shippingAndBilling.selectNoOfPaymentMethods(dsm.getInputParameter("NUMBER_OF_PAYMENTS_LABEL"));
		
		//select Cash on delivery as the payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
		
		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	
		
		//Confirm payment page on order summary page
		singleOrderSummary.verifyBillingMethod(dsm.getInputParameter("PAY_METHOD"),dsm.getInputParameter("PAYMENT_NUMBER"));
		
		//Confirm billing address		
		//singleOrderSummary.verifyBillingAddress(dsm.getInputParameter("RECIPIENT"));
		
		//Confirm product name
		singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm item quantity
		singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_QTY"));
		
		//Confirm total price of the item.
		singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_TOTAL"));
		
		//Confirm the total price of the order
		singleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL"));						
		
		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
		
		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();
	}			
	
	/**
	 * Test case to checkout with multiple payment methods while using the same method and billing address for all payment methods
	 */
	@Test
	public void testFV2STOREB2C_1304()
	{		
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY",Double.class));
		
		orders.addItem(dsm.getInputParameter("SKU_CODE2"), dsm.getInputParameterAsNumber("ITEM_QTY",Double.class));
		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
		
		//click on the checkout button
		ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
		
		//Confirm ship as complete option is selected.
		shippingAndBilling.verifyShipAsCompleteIsChecked();
		
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm the correct shipping address is selected
		shippingAndBilling.verifyShippingAddressSelected(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
		
		//Click on edit link on billing section
		RegisteredShippingBillingInfoPage shippingAndBillingInfo =	shippingAndBilling.createShippingAddress();
		
		//Enter info and submit the new address creating request
		shippingAndBillingInfo.typeRecipient(dsm.getInputParameter("RECIPIENT_1"))
		 .typeLastName(dsm.getInputParameter("LAST_NAME"))		 
		 .typeAddress(dsm.getInputParameter("STREET_ADDRESS"))
		 .typeCity(dsm.getInputParameter("CITY"))
		 .selectCountry(dsm.getInputParameter("COUNTRY"))
		 .selectStateOrProvince(dsm.getInputParameter("PROVINCE"))
		 .typeZipCode(dsm.getInputParameter("ZIPCODE"))
		 .typePhone(dsm.getInputParameter("PHONE_NUMBER"))
		 .typeEmail(dsm.getInputParameter("EMAIL"))
		 .applyToBothShippingBilling()
		 .submitSuccessfulForm();
		
		shippingAndBilling.selectShippingAddress(dsm.getInputParameter("RECIPIENT_1"));
		//Wait for billing address to display
		shippingAndBilling.waitForShippingAddress(dsm.getInputParameter("RECIPIENT_1"));
				
		//Click on edit link on billing section
		RegisteredShippingBillingInfoPage billingInfo = shippingAndBilling.createBillingAddress();
		
		//Confirm ship and bill info page is completely loaded
		billingInfo.verifyIsNotOnShipBillPage();	
		
		//Enter info and submit the new address creating request
		billingInfo
		 .typeLastName(dsm.getInputParameter("LAST_NAME"))
		 .typeRecipient(dsm.getInputParameter("RECIPIENT_2"))
		 .typeAddress(dsm.getInputParameter("STREET_ADDRESS"))
		 .typeCity(dsm.getInputParameter("CITY"))
		 .selectCountry(dsm.getInputParameter("COUNTRY"))
		 .selectStateOrProvince(dsm.getInputParameter("PROVINCE"))
		 .typeZipCode(dsm.getInputParameter("ZIPCODE"))
		 .typePhone(dsm.getInputParameter("PHONE_NUMBER"))
		 .typeEmail(dsm.getInputParameter("EMAIL"))
		 .applyToBothShippingBilling()
		 .submitSuccessfulForm();
		
		//Wait for billing address to display
		shippingAndBilling.waitForBillingAddress(dsm.getInputParameter("RECIPIENT_2"));

		//Select number of payment
		shippingAndBilling.selectNoOfPaymentMethods(dsm.getInputParameter("NUMBER_OF_PAYMENTS_LABEL"));
		
		//Select billing address for first number of payment
		shippingAndBilling.getMultiplepayment().selectBillingAddressByPosition(dsm.getInputParameter("RECIPIENT_1"), dsm.getInputParameter("ITEM_POSITION1"));
		
		//update new amount
		shippingAndBilling.getMultiplepayment().typeAmountByPaymentsPosition(dsm.getInputParameter("ITEM1_AMOUNT"), dsm.getInputParameter("ITEM_POSITION1"));		
		
		//Select billing method
		shippingAndBilling.getMultiplepayment().selectBillingMethodByPaymentsPosition(dsm.getInputParameter("PAY_METHOD"), dsm.getInputParameter("ITEM_POSITION1"));
				
		//Select billing address for first number of payment
		shippingAndBilling.getMultiplepayment().selectBillingAddressByPosition(dsm.getInputParameter("RECIPIENT_1"), dsm.getInputParameter("ITEM_POSITION2"));		

		//Select billing method
		shippingAndBilling.getMultiplepayment().selectBillingMethodByPaymentsPosition(dsm.getInputParameter("PAY_METHOD"), dsm.getInputParameter("ITEM_POSITION2"));				
			
		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	
		
		//Confirm payment page on order summary page
		singleOrderSummary.verifyBillingMethod(dsm.getInputParameter("PAY_METHOD"),dsm.getInputParameter("PAYMENT_NUMBER"));
		
		//Confirm billing address for first payment		
		singleOrderSummary.verifyBillingAddressByPaymentNumber(dsm.getInputParameter("RECIPIENT_1"), dsm.getInputParameter("ITEM_POSITION1"));
		
		//Confirm billing address for second payment	
		singleOrderSummary.verifyBillingAddressByPaymentNumber(dsm.getInputParameter("RECIPIENT_1"), dsm.getInputParameter("ITEM_POSITION2"));
		
		//Confirm product name
		singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION1"), dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm item quantity
		singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION1"), dsm.getInputParameter("ITEM_QTY"));
		
		//Confirm total price of the item.
		singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION1"),dsm.getInputParameter("ITEM_TOTAL"));
		
		//Confirm the total price of the order
		singleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL_ORDER_SUMMARY"));						
		
		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
		
		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();
	}
	
	/**
	 * Test case to checkout with multiple payment methods while using the same method and billing address for all payment methods
	 */
	@Test
	public void testFV2STOREB2C_1305() 
	{
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.addItem(dsm.getInputParameter("SKU_CODE"), dsm.getInputParameterAsNumber("ITEM_QTY",Double.class));
		
		orders.addItem(dsm.getInputParameter("SKU_CODE2"), dsm.getInputParameterAsNumber("ITEM_QTY",Double.class));
		
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = signIn.getHeaderWidget().goToShoppingCartPage();
		
		//Verify Order total
		shopCart.verifyOrderTotal(dsm.getInputParameter("TOTAL_ORDER_SUMMARY"));
		
		//click on the checkout button
		ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
		
		//Confirm ship as complete option is selected.
		shippingAndBilling.verifyShipAsCompleteIsChecked();
		
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm the correct shipping address is selected
		shippingAndBilling.verifyShippingAddressSelected(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
		
		//Click on edit link on billing section
		RegisteredShippingBillingInfoPage shippingAndBillingInfo =	shippingAndBilling.createShippingAddress();
				
		//Enter info and submit the new address creating request
		shippingAndBillingInfo.typeRecipient(dsm.getInputParameter("RECIPIENT_1"))
		 .typeLastName(dsm.getInputParameter("LAST_NAME"))		 
		 .typeAddress(dsm.getInputParameter("STREET_ADDRESS"))
		 .typeCity(dsm.getInputParameter("CITY"))
		 .selectCountry(dsm.getInputParameter("COUNTRY"))
		 .selectStateOrProvince(dsm.getInputParameter("PROVINCE"))
		 .typeZipCode(dsm.getInputParameter("ZIPCODE"))
		 .typePhone(dsm.getInputParameter("PHONE_NUMBER"))
		 .typeEmail(dsm.getInputParameter("EMAIL"))
		 .applyToBothShippingBilling()
		 .submitSuccessfulForm();
		
		//Wait for billing address to display
		shippingAndBilling.waitForShippingAddress(dsm.getInputParameter("RECIPIENT_1"));
		
		//Click on edit link on billing section
		RegisteredShippingBillingInfoPage billingInfo = shippingAndBilling.createBillingAddress();
		
		//Confirm ship and bill info page is completely loaded
		billingInfo.verifyIsNotOnShipBillPage();	
		
		//Enter info and submit the new address creating request
		billingInfo.typeLastName(dsm.getInputParameter("LAST_NAME"))
 		 .typeRecipient(dsm.getInputParameter("RECIPIENT_2"))
		 .typeAddress(dsm.getInputParameter("STREET_ADDRESS"))
		 .typeCity(dsm.getInputParameter("CITY"))
		 .selectCountry(dsm.getInputParameter("COUNTRY"))
		 .selectStateOrProvince(dsm.getInputParameter("PROVINCE"))
		 .typeZipCode(dsm.getInputParameter("ZIPCODE"))
		 .typePhone(dsm.getInputParameter("PHONE_NUMBER"))
		 .typeEmail(dsm.getInputParameter("EMAIL"))
		 .applyToBothShippingBilling()
		 .submitSuccessfulForm();
		
		//Wait for billing address to display
		shippingAndBilling.waitForBillingAddress(dsm.getInputParameter("RECIPIENT_2"));

		//Select number of payment
		shippingAndBilling.selectNoOfPaymentMethods(dsm.getInputParameter("NUMBER_OF_PAYMENTS_LABEL"));
		
		//Select billing address for first number of payment
		shippingAndBilling.getMultiplepayment().selectBillingAddressByPosition(dsm.getInputParameter("RECIPIENT_1"), dsm.getInputParameter("ITEM_POSITION1"));
		
		//Select billing method
		shippingAndBilling.getMultiplepayment().selectBillingMethodByPaymentsPosition(dsm.getInputParameter("PAY_METHOD"), dsm.getInputParameter("ITEM_POSITION1"));
		
		//update new amount
		shippingAndBilling.getMultiplepayment().typeAmountByPaymentsPosition(dsm.getInputParameter("ITEM1_AMOUNT"), dsm.getInputParameter("ITEM_POSITION1"));
		
		//Select billing address for first number of payment
		shippingAndBilling.getMultiplepayment().selectBillingAddressByPosition(dsm.getInputParameter("RECIPIENT_2"), dsm.getInputParameter("ITEM_POSITION2"));		

		//Select billing method
		shippingAndBilling.getMultiplepayment().selectBillingMethodByPaymentsPosition(dsm.getInputParameter("PAY_METHOD2"), dsm.getInputParameter("ITEM_POSITION2"));				
			
		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	
		
		//Confirm payment page on order summary page
		singleOrderSummary.verifyBillingMethod(dsm.getInputParameter("PAY_METHOD"),dsm.getInputParameter("PAYMENT_NUMBER"));
		
		//Confirm billing address for first payment		
		singleOrderSummary.verifyBillingAddressByPaymentNumber(dsm.getInputParameter("RECIPIENT_2"), dsm.getInputParameter("ITEM_POSITION1"));
		
		//Confirm billing address for second payment	
		singleOrderSummary.verifyBillingAddressByPaymentNumber(dsm.getInputParameter("RECIPIENT_1"), dsm.getInputParameter("ITEM_POSITION2"));
		
		//Confirm product name
		singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION1"), dsm.getInputParameter("PRODUCT_NAME"));
		
		//Confirm item quantity
		singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION1"), dsm.getInputParameter("ITEM_QTY"));
		
		//Confirm total price of the item.
		singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION1"),dsm.getInputParameter("ITEM_TOTAL"));
		
		//Confirm the total price of the order
		singleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL_ORDER_SUMMARY"));						
		
		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
		
		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();
	}
	
	/**
	 * Perform teardown operations after the test is done, whether it is successful or not.
	 */
	@After
	public void testScenarioTearDown()
	{
		
		try
		{
			getLog().info("testScenarioTearDown running");
			
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
			
			orders.removeAllItemsFromCart();
			orders.deletePaymentMethod();
			
			MemberFixture member = f_CaslFixtures.createMemberFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
			
			member.deleteAddress(dsm.getInputParameter("RECIPIENT_1"));
			member.deleteAddress(dsm.getInputParameter("RECIPIENT_2"));
			
			
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

	}
		
}
