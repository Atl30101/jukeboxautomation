package com.ibm.commerce.qa.aurora.tests;
/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.IBMCopyright;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

//Declares a new test scenario called FV2STOREB2C_SOLR_SEARCH_12_WTE which contains one or more test case methods.
//All test scenario classes must extend AbstractAuroraSingleSessionTests.

/** Test scenario to test Classic attribute and Attribute Dictionary attribute search
 *  Pre-requisites: Tester knows the Store URL 
 * (http://<hostname>/webapp/wcs/stores/servlet/en/auroraesite)
 *  Search in store is enabled
 * and tester enables facet following the guide 
 * http://publib.boulder.ibm.com/infocenter/wchelp/v7r0m0/index.jsp?topic=%2Fcom.ibm.commerce.developer.doc%2Ftasks%2Ftsdsearchenableattr.htm&resultof=%22enable%22%20%22enabl%22%20%22facets%22%20%22facet%22
 * <br/>
 * Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_SOLR_SEARCH_12 extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = IBMCopyright.SHORT_COPYRIGHT;

	//A variable to hold the name of the data file where input parameters can be found.
	//$ANALYSIS-IGNORE
	protected final String dataFileName = "src/data/FV2STOREB2C_SOLR_SEARCH_12_Data.xml";

	@DataProvider
	private final TestDataProvider dsm;
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dsm
	 */
	@Inject
	public FV2STOREB2C_SOLR_SEARCH_12(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dsm)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dsm;
	}

	/** Test case to test Search products with classic attribute using string value
	 * @throws Exception
	 */
	@Category(Sanity.class)
	@DataProvider(auto=false)
	@Test
	public void testFSRC_STOREINT_1201() throws Exception 
	{
		dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1201", "testFV2STOREB2C_SOLR_SEARCH_1201_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//search for a product with multiple SKUs
		SearchResultsPage searchResultsPage = aurorafrontPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCHTERM_1"));
		
		//check for results
		searchResultsPage.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));	
	}
	
	/** Test case to test Search products with classic attribute using float value
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFSRC_STOREINT_1203() throws Exception 
	{
		dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1203", "testFV2STOREB2C_SOLR_SEARCH_1203_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//search for a product with multiple SKUs
		SearchResultsPage searchResultsPage = aurorafrontPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCHTERM_1"));
		
		//check for results
		searchResultsPage.verifyTextPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));		
	}
	
	/**
	 * Test case to test Search products with attribute dictionary attribute
	 * using string value
	 * 
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFSRC_STOREINT_1204() throws Exception 
	{
		dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1204", "testFV2STOREB2C_SOLR_SEARCH_1204_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//search for a product with multiple SKUs
		SearchResultsPage searchResultsPage = aurorafrontPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCHTERM_1"));
		
		//check for results
		searchResultsPage.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("EXPECTED_RESULT_1"));			
	}


	/** Test case to test Search products with attribute dictionary attribute using integer value
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFSRC_STOREINT_1205() throws Exception 
	{

		dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1205", "testFV2STOREB2C_SOLR_SEARCH_1205_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//search for a product with multiple SKUs
		ProductDisplayPage productDisplayPage = aurorafrontPage.getHeaderWidget().performSearch(ProductDisplayPage.class, dsm.getInputParameter("SEARCHTERM_1"));
		
		//check for results
		productDisplayPage.getNamePartNumberAndPriceWidget().verifyCatEntryName(dsm.getInputParameter("EXPECTED_RESULT_1"));						
	}
	
	/** Test case to test Search products with attribute dictionary attribute using float value
	 * @throws Exception
	 */
	@DataProvider(auto=false)
	@Test
	public void testFSRC_STOREINT_1206() throws Exception 
	{
		
		dsm.setDataFile(dataFileName);
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_SOLR_SEARCH_1205", "testFV2STOREB2C_SOLR_SEARCH_1205_DATABLOCK");

		//Open the store in the browser
		AuroraFrontPage aurorafrontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//search for a product with multiple SKUs
		ProductDisplayPage productDisplayPage = aurorafrontPage.getHeaderWidget().performSearch(ProductDisplayPage.class, dsm.getInputParameter("SEARCHTERM_1"));
		
		//check for results
		productDisplayPage.getNamePartNumberAndPriceWidget().verifyCatEntryName(dsm.getInputParameter("EXPECTED_RESULT_1"));						
	}
}