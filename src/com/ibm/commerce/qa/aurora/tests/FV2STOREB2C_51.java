package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2010
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.url.DeltaUpdates;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
/**			
* Scenario: FV2STOREB2C_51
* Details: Setup product attribute swatch and view them on quick info popup.
*
*/
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_51 extends AbstractAuroraSingleSessionTests
{
	@DataProvider
	private final TestDataProvider dsm;
	
	private CMC cmc;
	
	/** Used to verify if a product is present **/
	public static final String CAT_PRODUCT_IMAGE_LINK = "CatalogEntryProdImg_";
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dataSetManager
	 * @param cmc
	 * @param deltaUpdate
	 */
	@Inject
	public FV2STOREB2C_51(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CMC cmc,
			DeltaUpdates deltaUpdate)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		this.cmc = cmc;
		this.deltaUpdate = deltaUpdate;
	}
	
	/**A variable to hold the catalog entry identifier created**/
	private String productId;
	
	private String sku;
	
	/**A variable to hold the master catalog**/
	private String masterCatalogId;

	/**A variable to hold delta update**/
	private DeltaUpdates deltaUpdate;	
	
	private AuroraFrontPage 	auroraFront;

	private boolean 			test5103 = false;
	private boolean 			test5105 = false;
	
	/**
	 * Test cases to verify the product image change based on the selected swatch attribute
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_5103() throws Exception
	{
		test5103 = true;
		
		dsm.setDataLocation("testFV2STOREB2C_5103", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//generate preview url
		cmc.selectStore();
		cmc.selectCatalog();
		cmc.storePreview("yyyy/MM/dd", "12HR", "true");
		
		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore(dsm.getInputParameter("storename"));
		cmc.selectCatalog();
		
		
		//Delete Black 2XL SKU
		dsm.setDataLocation("testFV2STOREB2C_5103", "deleteSKU");
		sku = cmc.getCatalogEntryId(dsm.getInputParameter("partNumber"));
		cmc.deleteCatalogEntry(sku);
		
		masterCatalogId = cmc.selectCatalog();
		//Wait until delta update completes before timeout
	
		//Confrim SKU items no longer able to be selected 
		dsm.setDataLocation("testFV2STOREB2C_5103", "testFV2STOREB2C_5103");	
		
		auroraFront = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Search for an item
		auroraFront.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("PROD_NAME"));
		
	}
	/**
	 * Test cases to verify the product image change based on the selected swatch attribute
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_5105() throws Exception
	{
		test5105 = true;
		
		dsm.setDataLocation("testFV2STOREB2C_5105", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//generate preview url
		cmc.selectStore();
		cmc.selectCatalog();
		String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
		
		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore(dsm.getInputParameter("storename"));
		cmc.selectCatalog();
		
		
		dsm.setDataLocation("testFV2STOREB2C_5105", "testFV2STOREB2C_5105");
		productId = cmc.getCatalogEntryId(dsm.getInputParameter("ProdPartNum"));

		//create an SKU for the product
		dsm.setDataLocation("testFV2STOREB2C_5105", "createSKU");
		String tempSku = cmc.createSKU(productId,dsm.getInputParameter("partNumber"),dsm.getInputParameter("skuName"),dsm.getInputParameter("published"));

		cmc.createSKUDefiningAttrDictAttributeValue(tempSku, productId, dsm.getInputParameter("attributeValColorSeq"), dsm.getInputParameter("attributeValColor"));
		cmc.createSKUDefiningAttrDictAttributeValue(tempSku, productId, dsm.getInputParameter("attributeValSizeSeq"), dsm.getInputParameter("attributeValSize"));
		
		dsm.setDataLocation("testFV2STOREB2C_5105", "updateSKU");
		cmc.updateProduct(tempSku);
		dsm.setDataLocation("testFV2STOREB2C_5105", "updateSKUImage");
		cmc.updateProduct(tempSku);
		
		//set first offer price and list price
		dsm.setDataLocation("testFV2STOREB2C_5105", "offerPrice");
		cmc.createOfferPriceForCatalogEntry(tempSku);
		cmc.createListPriceForCatalogEntry(tempSku);
		
		masterCatalogId = cmc.selectCatalog();
		//Wait until delta update completes before timeout
		deltaUpdate.beforeWaitForDelta(masterCatalogId);
		getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
		deltaUpdate.waitForDelta(masterCatalogId, 60);

		cmc.logoff();
		
		dsm.setDataLocation("testFV2STOREB2C_5105", "testFV2STOREB2C_5105");
		auroraFront = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Search for an item
		auroraFront.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("PROD_NAME"));
		
	}	

	/**
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception
	{
	try
	{
		if(test5103)
		{
			dsm.setDataLocation("tearDown", "CMClogon");
			cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
			//generate preview url
			cmc.selectStore();
			cmc.selectCatalog();
			String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
			
			//select store and catalog
			cmc.setLocale(dsm.getInputParameter("locale"));
			cmc.selectStore(dsm.getInputParameter("storename"));
			cmc.selectCatalog();
			
			dsm.setDataLocation("tearDown", "tearDown");
			productId = cmc.getCatalogEntryId(dsm.getInputParameter("ProdPartNum"));

			//create an SKU for the product
			dsm.setDataLocation("tearDown", "createSKU");
			String tempSku = cmc.createSKU(productId,dsm.getInputParameter("partNumber"),dsm.getInputParameter("skuName"),dsm.getInputParameter("published"));

			cmc.createSKUDefiningAttrDictAttributeValue(tempSku, productId, dsm.getInputParameter("attributeValColorSeq"), dsm.getInputParameter("attributeValColor"));
			cmc.createSKUDefiningAttrDictAttributeValue(tempSku, productId, dsm.getInputParameter("attributeValSizeSeq"), dsm.getInputParameter("attributeValSize"));
		
			dsm.setDataLocation("tearDown", "updateSKU");
			cmc.updateProduct(tempSku);
			dsm.setDataLocation("tearDown", "updateSKUImage");
			cmc.updateProduct(tempSku);
		
			//set first offer price and list price
			dsm.setDataLocation("tearDown", "offerPrice");
			cmc.createOfferPriceForCatalogEntry(tempSku);
			cmc.createListPriceForCatalogEntry(tempSku);
		
			masterCatalogId = cmc.selectCatalog();
			//Wait until delta update completes before timeout
			deltaUpdate.beforeWaitForDelta(masterCatalogId);
			getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
			deltaUpdate.waitForDelta(masterCatalogId, 60);
		
			cmc.logoff();	
		
			test5103 = false;
		}
		
		if(test5105)
		{
			dsm.setDataLocation("tearDown", "CMClogon");
			cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
			//select store and catalog
			cmc.setLocale(dsm.getInputParameter("locale"));
			cmc.selectStore(dsm.getInputParameter("storename"));
			cmc.selectCatalog();
			
			//get the SKU we created, and delete it
			dsm.setDataLocation("testFV2STOREB2C_5105", "createSKU");
			sku = cmc.getCatalogEntryId(dsm.getInputParameter("partNumber"));
			cmc.deleteCatalogEntry(sku);
			
			cmc.logoff();
			
		}
	}
	catch(RuntimeException e)
	{
		getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
	}
	}
	
	
}