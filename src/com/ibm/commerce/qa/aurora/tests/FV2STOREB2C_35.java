package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2010
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.StoreManagement;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/** 		
 * Scenario FV2STOREB2C_35
 * The objective of this test scenario is testing flex flow options 'Enable/Disable Search'
 * and 'Display inventory availability' in store front.
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_35 extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	@DataProvider(apolloDSM=true)
	
	//A Variable to retrieve data from the data file.
	private final TestDataProvider dsm;
	
	//A Variable to enable/disable flex flow options from CMC.
	private final StoreManagement storeManagement;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param storeManagement
	 * 			   object to work with CMC flex flow options
	 */
	@Inject
	public FV2STOREB2C_35(Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			StoreManagement storeManagement){
		
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		this.storeManagement = storeManagement;
	}
	
	/**
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		//Setup appropriate change flow options
		storeManagement.modifyChangeFlowOptions(null, new String[] {dsm.getInputParameter("SEARCH_FLOW"), 
				dsm.getInputParameter("PRODUCT_COMPARE_FLOW"), dsm.getInputParameter("INVENTORY_AVAILABILITY_FLOW")});
	}
	
	/**
	 * Test case to Enable/Disable Search in Store (enabled by default).
	 * @throws Exception 
	 * 
	 */
	@Test
	public void testFV2STOREB2C_3501() throws Exception 
	{
		
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Open Sign In/Register page
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
				
		//Enter account User Name/Password and sign in
		HeaderWidget header = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD")).signIn().closeSignOutDropDownWidget();
				
		//Clicks on the home link
		frontPage = header.goToFrontPage();
		
		frontPage.getFooterWidget().verifyAdvancedSearchNotPresent();
		//Get the header section object
		HeaderWidget headerSection = frontPage.getHeaderWidget();
		
		//Verify that the search field is not present on the header section
		headerSection.verifySearchFieldIsNotPresent()
		
		//Click on sign out link on header
		.openSignOutDropDownWidget().signOut();
		
		//Re-enable search store catalog		
		storeManagement.enableChangeFlowOption(dsm.getInputParameter("SEARCH_FLOW"));

		//open store named Madisons using firefox as the browser
		frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Open Sign In/Register page
		signInPage = frontPage.getHeaderWidget().signIn();
				
		//Enter account User Name/Password and sign in
		header = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD")).signIn().closeSignOutDropDownWidget();
				
		//Clicks on the home link.
		frontPage = header.goToFrontPage();
		
		//Get the header section object
		headerSection = frontPage.getHeaderWidget();
		
		//Verify that the search field is present on the header section
		headerSection.verifySearchFieldIsPresent();
		
		//Perform simple search
		SearchResultsPage searchResultsPage = headerSection.performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		
		//Verify product is present in search result
		searchResultsPage.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("SEARCH_PRODUCT_NAME"));
		
		//Verify search result text present
		searchResultsPage.verifyTextPresent(dsm.getInputParameter("SEARCH_CONTAINED_TEXT"));

	}
	
	/**
	 * Test case to Display inventory availability (enabled by default).
	 * @throws Exception 
	 * 
	 */
	@Test
	public void testFV2STOREB2C_3502() throws Exception {
	
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Open Sign In/Register page
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
				
		//Enter account User Name/Password and sign in
		HeaderWidget header = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD")).signIn().closeSignOutDropDownWidget();
		
		//Clicks on the home link
		frontPage =header.goToFrontPage();
			
		//Click on Sub category on Header.e.g 'Home &amp; Furnishing->Furniture->Student Desk'
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), 
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//click on product image on category page
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
	
		//Select product to see availability
		productDisplayPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("PRODUCT_ATTRIBUTE"));
		
		//verify that Store Availability is displayed on product display Page
		productDisplayPage.verifyShowAvailabilityIsNotPresent();
		
		//Click on sign out link on header
		HeaderWidget headerSection = productDisplayPage.getHeaderWidget();
		
		//Click Sign out from account
		headerSection.openSignOutDropDownWidget().signOut();
		
		//Re-enable display inventory availability
		storeManagement.enableChangeFlowOption(dsm.getInputParameter("INVENTORY_AVAILABILITY_FLOW"));
	
		//Open the store in the browser
		frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Open Sign In/Register page
		signInPage = frontPage.getHeaderWidget().signIn();
				
		//Enter account User Name/Password and sign in
		header = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD")).signIn().closeSignOutDropDownWidget();
		
		//Clicks on the home link.
		frontPage = header.goToFrontPage();
		
		//Click on Sub category on Header.e.g 'Home &amp; Furnishing->Furniture->Student Desk'.
		subCat  = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), 
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//click on product image on category page.
		productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
	
		//Select product to see availability
		productDisplayPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("PRODUCT_ATTRIBUTE"));
		
		//Verify availability section is visible
		productDisplayPage.verifyShowAvailabilityIsPresent()
		
		//Verify product online availability is correct
		.verifyOnlineAvailability(dsm.getInputParameter("PRODUCT_ONLINE_AVAILABILITY"));
   }
	
	/**
	 * Tear down ran after every test case
	 * @throws Exception 
	 */
	@After
	public void tearDown() throws Exception{
	try
	{	//Setup appropriate change flow options
		storeManagement.modifyChangeFlowOptions(new String[] {dsm.getInputParameter("SEARCH_FLOW"), 
				dsm.getInputParameter("PRODUCT_COMPARE_FLOW"), dsm.getInputParameter("INVENTORY_AVAILABILITY_FLOW")}, null);
	
	}
	catch(RuntimeException e)
	{
		getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
	}	
	}
 }
