package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.GenericCategoryPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.url.DeltaUpdates;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/**			
* Scenario: FV2STOREB2C_31
* Details: This scenario will test Store Pagination Support.
*
* 
*/
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_31 extends AbstractAuroraSingleSessionTests
{
	@DataProvider
	private final TestDataProvider dsm;
	
	private CMC cmc;

	//A Variable to perform delta update wait.
	private final DeltaUpdates deltaUpdate;

	//The catalog Id of the store
	private String masterCatalogId;

	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dataSetManager
	 * @param cmc
	 * @param deltaUpdate 
	 * 				object to perform delta update wait operations
	 */
	@Inject
	public FV2STOREB2C_31(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CMC cmc,
			DeltaUpdates deltaUpdate)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		this.cmc = cmc;
		this.deltaUpdate = deltaUpdate;
	}	

	/**A variable to hold the catalog group identifier created**/
	private String catgroupId;
	private String[] productId = new String[150];
	private String[] subcatgroupId = new String[150];
	private List<String> expectedTextList = new ArrayList<String>();
		
	/**
	 * A test to check Store pagination support for main categories.
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_3101() throws Exception
	{	
		/*
		 *  Note that counters used in this test case start at 101, not 1.
		 *  This is so that all counters are 3 digits in length, and no padding is required
		 */
		
		//login to CMC
		dsm.setDataLocation("testFV2STOREB2C_3101", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));

		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore(getConfig().getStoreName());
		masterCatalogId = cmc.selectCatalog();
		String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
		
		String defaultCat = cmc.getDefaultCatalog();
		cmc.removeDefaultSalesCatalog(defaultCat);
		
		//Create a new top-level Category
		dsm.setDataLocation("testFV2STOREB2C_3101", "createCatalogGroup");
		catgroupId = cmc.createCatalogGroup("0",dsm.getInputParameter("identifierId"),dsm.getInputParameter("CATALOG_NAME"),dsm.getInputParameter("published"));
		//getLog().info("catgroupId is '" + catgroupId + "'");
		
		for (int ctr = 101; ctr <= Integer.parseInt(dsm.getInputParameter("NUM_SUBCATEGORIES_TO_CREATE")); ctr++) {
			String SUBCAT_ID = "Book Sub-category " + Integer.toString(ctr);
			String PRODUCT_ID = "Book Product " + Integer.toString(ctr);
			
			// create Sub-category "BookSub-Category {ctr}" under the new Top-level Category
			subcatgroupId[ctr] = cmc.createCatalogGroup(catgroupId, SUBCAT_ID, SUBCAT_ID, dsm.getInputParameter("published"));
			if (ctr <= (Integer.parseInt(dsm.getInputParameter("PAGINATION_LIMIT")) + 100)) {
				expectedTextList.add(SUBCAT_ID);
			}
			//getLog().info("subcatgroupId[" + Integer.toString(ctr) + "] is '" + subcatgroupId[ctr] + "'");

			// create Product "Book Product {ctr}" in Sub-category "Book Sub-Category {ctr}"
			productId[ctr] = cmc.createProduct(subcatgroupId[ctr], PRODUCT_ID, PRODUCT_ID, dsm.getInputParameter("published"));
			//getLog().info("productId[" + Integer.toString(ctr) + "] is '" + productId[ctr] + "'");
		}
		// Wait for Delta Update
		deltaUpdate.beforeWaitForDelta(masterCatalogId);
		getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
		deltaUpdate.waitForDelta(masterCatalogId, 120);
		
		//log off from CMC
		cmc.logoff();
		
		//Tell which test case to use for input parameters
		dsm.setDataLocation("testFV2STOREB2C_3101","testFV2STOREB2C_3101");
		
		//Open the store in the browser
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Login with valid user id and password
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn().typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"));
		HeaderWidget header = signIn.signIn().closeSignOutDropDownWidget();
		
		//Go to homepage
		frontPage = header.goToFrontPage();

		// Verify new top-level Category is there by just going to it
		GenericCategoryPage department = frontPage.getHeaderWidget().goToDepartmentByName(GenericCategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"));

		// Go back to homepage
		frontPage = department.getHeaderWidget().goToFrontPage();

		// Add the "more..." link to the expected List
		expectedTextList.add("more...");

		// Hover over the new top-level Category, verify that the correct number of sub-categories are displayed
		frontPage.getHeaderWidget().loadDepartmentsMenuAndVerifyCategorySequence("Books001", expectedTextList);

		// Click on the "more..." link/button, verify you are taken to top-level Category page
		frontPage.getHeaderWidget().goToDeptPageByMoreLink(dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"));


		//Click on log out link
		department.getHeaderWidget().openSignOutDropDownWidget().signOut();

	}
	
	/**
	 * Perform tear down operations such as stopping the test harness.
	 * This method will also delete the catalog group created in this scenario.
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception
	{	

		//login to CMC
		dsm.setDataLocation("testFV2STOREB2C_3101", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.selectStore(getConfig().getStoreName());
		cmc.setLocale(dsm.getInputParameter("locale"));
		masterCatalogId = cmc.selectCatalog();
		String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
		
		dsm.setDataLocation("testFV2STOREB2C_3101", "createCatalogGroup");

		// Delete the Products and Sub-categories we created
		for (int ctr = 101; ctr <= Integer.parseInt(dsm.getInputParameter("NUM_SUBCATEGORIES_TO_CREATE")); ctr++) {
			cmc.deleteCatalogEntry(productId[ctr]);
			cmc.deleteCatalogGroup(subcatgroupId[ctr]);
		}

		//Delete the top-level Category we created
		cmc.deleteCatalogGroup(catgroupId);
		
		// Wait for Delta Update
		deltaUpdate.beforeWaitForDelta(masterCatalogId);
		getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
		deltaUpdate.waitForDelta(masterCatalogId, 60);
 
		//log off from CMC
		cmc.logoff();

	}
}
