package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.MyRecuringOrdersPage;
import com.ibm.commerce.qa.aurora.page.MyRecuringOrdersPage.orderType;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.RecurringChildOrderPage;
import com.ibm.commerce.qa.aurora.page.RecurringOrderDetailPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.url.StoreManagement;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/** 		
 * Scenario: FV2STOREB2C_53
 * Details: View My Orders
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_53 extends AbstractAuroraSingleSessionTests 
{	
	/**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	@DataProvider
	private final TestDataProvider dsm;
	
	//A Variable to perform CMC tasks.
	private CMC cmc;
	
	//A Variable to enable/disable flex flow options from CMC.
	private final StoreManagement storeManagement;
	
	String loginId = RandomStringUtils.randomAlphanumeric(8);
	String password = "wcs1admin";

	private CaslFixturesFactory f_caslFixtures;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 				object to work with data files
	 * @param cmc
	 * 				object to work with CMC flex flow options
	 * @param storeManagement
	 * @param p_caslFixtures 
	 */
	@Inject
	public FV2STOREB2C_53
	(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CMC cmc,
			StoreManagement storeManagement,
			CaslFixturesFactory p_caslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		this.cmc = cmc;
		this.storeManagement = storeManagement;
		f_caslFixtures = p_caslFixtures;
	}


	// In this pre set up step , creating a register user that will be use in further test cases.
	/**
	 * @throws Exception
	 */
	@Before
	public void customerRegistration() throws Exception
	{
		storeManagement.enableChangeFlowOption("DateOfBirth");
		
		loginId=RandomStringUtils.randomAlphanumeric(8);
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);


		frontPage.getHeaderWidget().signIn().registerCustomer()
		//type username
		.typeLogonId(loginId)
		//type first name
		.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
		//type last name
		.typeLastName(dsm.getInputParameter("LAST_NAME"))
		//type password
		.typePassword(password)
		//Verify password
		.typeVerifyPassword(password)
		//type street address
		.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
		//Select country
		.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
		//type or select state
		.selectStateOrProvince(dsm.getInputParameter("STATE"))
		//type city
		.typeCity(dsm.getInputParameter("CITY"))
		//type zipcode
		.typeZipCode(dsm.getInputParameter("ZIPCODE"))
		//type E-mail
		.typeEmail(dsm.getInputParameter("EMAIL"))
		//type home phone number
		.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
		//Select gender
		.selectGender(dsm.getInputParameter("GENDER"))
		//type mobile phone number
		.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
		//Update Allow Me Option check box
		.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
		//Check if preferred language drop down is visible
		.verifyPreferredLanguageDropDownListPresent()	
		//Select preferred language
		//.selectPreferedCurrency(dsm.getInputParameter("PREFERRED_CURRENCY"))
		//Check if preferred currency drop down is visible
		.verifyPreferredCurrencyDropDownListPresent()
		//Selected preferred language
		//.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
		//Check if birthday selection drop down is visible
		.verifyBirthdaySelectionPresent()
		//Select  birth year
		.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
		//Select birth month
		.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
		//Select birth day
		.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
		//Submit registration, the uesr is taken to the MyAccounts page
		.submit();
	}

	/**
	 * Views My recurring Orders in Order Status Page.
	 * Pre-conditions: <ul>
	 * 					<li>Tester knows the Store URL (http://<hostname>/webapp/wcs/stores/servlet/en/aurora)</li>
	 * 					<li>User is guest user</li>				
	 * 				   </ul>
	 * Flex flow options: none
	 * Post conditions: Registered user should be able to successfully place the recurring order
	 * 
	 * @throws Exception
	 */
	@Test
	public void FV2STOREB2C_5301() throws Exception{


		//opening the store front page
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Sign In  into the store
		MyAccountMainPage myAccountPage =  frontPage.getHeaderWidget().signIn().typeUsername(loginId).typePassword(password).signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//Go to department page
		CategoryPage subCat = myAccountPage.getFooterWidget().goToSiteMapPage().goToCategoryPageByName(CategoryPage.class, dsm.getInputParameter("CATEGORY_NAME"));
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCTNAME")).addToCart();


		//Go to the shopCart page and schedule this order as recurring order
		ShopCartPage shopCartPage = productDisplayPage.getHeaderWidget().goToShoppingCartPage().checkScheduleThisOrderCheckBox();

		//Go to the shipping billing page
		ShippingAndBillingPage shippingBillingPage = shopCartPage.continueToNextStep();

		//Getting the tomorrow date
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String dateNow = formatter.format(currentDate.getTime());

		//typeing the order frequency and order start date.
		shippingBillingPage = shippingBillingPage.selectOrderFrequency(dsm.getInputParameter("ORDER_FREQUENCY")).typeStartDate(dateNow);

		//typeing the payment method and credit card details
		shippingBillingPage.selectPaymentMethod(dsm.getInputParameter("PAYMENTMETHOD")).typeCreditCardNumber(dsm.getInputParameter("CREDIT_CARD_NO"));

		//go the order summary page
		OrderSummarySingleShipPage orderSummaryPage = shippingBillingPage.singleShipNext();

		//Go to order confirmation page . verifying the recurring order got placed successfully
		OrderConfirmationPage orderConformationPage = orderSummaryPage.completeOrder().verifyScheduledOrderSuccessful();

		//Get order no from order confirmation page.
		String orderNumber = orderConformationPage.getScheduledOrderNumber();	
		
		//create child order
		OrdersFixture orders =f_caslFixtures.createOrdersFixture(loginId, password, getConfig().getStoreName());
		
		orders.createChildOrder(Integer.parseInt(orderNumber), "Y", "N");

		//Go to recurring order page
		MyRecuringOrdersPage recurringOrderPage = orderConformationPage.getHeaderWidget().goToMyAccount().getSidebar().goToRecurringOrdersPage();


		//verifying order no present in recurring order page.
		recurringOrderPage = recurringOrderPage.verifyOrdreNumberIsPresent(orderNumber);

		//verifying the recurring order status
		recurringOrderPage = recurringOrderPage.verifyOrderStatus("Active" , orderNumber);



		//Go to the recurring order details page
		RecurringOrderDetailPage recurringOrderDetailPage = recurringOrderPage.goToRecurringOrderDetailPage(orderNumber);

		//Go to child order page
		RecurringChildOrderPage recurringChildOrdrePage = recurringOrderDetailPage.goToChildOrderPage();



		//verifying atleast one child order is present
		recurringChildOrdrePage.verifyAtLeatOneChildOrderPresent();


	}


	/**
	 * Test pagination for recurring orders in my account
	 * 
	 * @throws Exception
	 */
	
	@Test
	public void FV2STOREB2C_5302() throws Exception
	{
			
			String orderNumber = "";
			dsm.setDataBlock("cmcData");
			
			//Log into cmc
			cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
			
			//select store and catalog
			cmc.setLocale(dsm.getInputParameter("locale"));
			
			//Select the store to work on
			cmc.selectStore();
			
			//Select the catalog to work on
			cmc.selectCatalog();
			
			//Get the catalog entry ID of the item by search
			String catalogEntryId = cmc.getCatalogEntryId(dsm.getInputParameter("searchText1"));
			
			dsm.setDataBlock("allowRecurringOrder");
			
			//Enable recurring order for the item.
			cmc.updateProduct(catalogEntryId);
			
			cmc.logoff();
	
			dsm.setDataBlock("FV2STOREB2C_5302");
			
			//Open the store in the browser
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
			
			//Open Sign In/Register page
			SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
			
			//type account User Name/Password and sign in
			MyAccountMainPage myAccount = signIn.typeUsername(loginId).typePassword(password)
				.signIn().closeSignOutDropDownWidget().goToMyAccount();
			
			OrdersFixture orders = f_caslFixtures.createOrdersFixture(loginId, password, getConfig().getStoreName());
			
			int numberOfOrder = dsm.getInputParameterAsNumber("MAXNUMBEROFORDERS", Integer.class);
			
			for(int i = 0; i<=numberOfOrder; i++){
				
				orders.addItem(dsm.getInputParameter("PRODUCT_SKU"));				
				orders.addPayLaterPaymentMethod();
				orderNumber = orders.completeRecurringOrder(dsm.getInputParameterAsNumber("FREQUENCY", Double.class), dsm.getInputParameter("FREQ_UNITS"));			
			}
	
			//Open Recurring Orders from left side bar.
			MyRecuringOrdersPage myRecurringOrder = myAccount.getSidebar().goToRecurringOrdersPage();
	
			//Verify the order number for the placed order is visible
			myRecurringOrder.verifyOrder(orderNumber)
			
			.verifyOrderCount(orderType.RECURRINGORDER, numberOfOrder)
			
			//Verify the pagination selection widget is visible and select the next arrow from widget.
			.verifyPaginationSectionVisible().selectNextPage();
	}
	
	/**
	 * To verify if shopper can cancel a recurring order which has no child order created yet
	 * Pre-conditions: <ul>
	 * 					<li>Tester knows the Store URL of Aurora store						
	 * 				   </ul>
	 * Post conditions: Recurring order is cancelled.
	 * @throws Exception
	 */
	@Test
	public void FV2STOREB2C_5303() throws Exception {




		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();

		//Log in to the store.
		signIn.typeUsername(loginId).typePassword(password).signIn().closeSignOutDropDownWidget();		

		//Go to department page				
		CategoryPage subCat = signIn.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"),dsm.getInputParameter("SUB_CAT"));

		//click on product image on Category Display page.
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		

		//click Add to Cart from Product Display page.
		productDisplay.addToCart();

	
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();

		shopCart=shopCart.checkScheduleThisOrderCheckBox();

		//click on the checkout button
		ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();


		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String dateNow = formatter.format(currentDate.getTime());

		shippingAndBilling = shippingAndBilling.selectOrderFrequency(
				dsm.getInputParameter("ORDER_FREQUENCY")).typeStartDate(
						dateNow);

		shippingAndBilling.selectPaymentMethod(
				dsm.getInputParameter("PAYMENTMETHOD")).typeCreditCardNumber(
						dsm.getInputParameter("CREDIT_CARD_NO"));

		//Confirm ship as complete option is selected.
		shippingAndBilling=shippingAndBilling.verifyShipAsCompleteIsChecked();

		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling=shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));

		//Confirm the correct shipping address is selected
		shippingAndBilling=shippingAndBilling.verifyShippingAddressSelected(loginId);


		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	

		//Confirm product name
		singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("PRODUCT_NAME"));

		//Confirm item quantity
		singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_QTY"));

		//Confirm total price of the item.
		singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_TOTAL"));

		//Confirm the total price of the order
		singleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL"));						

		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();

		//verify that the order has been placed
		
		String orderNumber = orderConfirmation.getScheduledOrderNumber();

		//Go to recurring order page
		MyRecuringOrdersPage recurringOrderPage = orderConfirmation.getHeaderWidget().goToMyAccount().getSidebar().goToRecurringOrdersPage();


		//verifying order no present in recurring order page.
		recurringOrderPage = recurringOrderPage.verifyOrdreNumberIsPresent(orderNumber);

		//verifying the recurring order status before cancellation
		recurringOrderPage = recurringOrderPage.verifyOrderStatus("Active" , orderNumber);


		
        //Cancel the recurring order
		recurringOrderPage=recurringOrderPage.selectRecurringActionButtonByPosition(1);
		recurringOrderPage=recurringOrderPage.cancelRecurringOrderByPosition(1);
		recurringOrderPage=recurringOrderPage.cancelRecurringOrderConfirmationYes();	
		recurringOrderPage.getHeaderWidget().closeMessageArea();
						
		//verifying order no present in recurring order page which is cancelled
		recurringOrderPage = recurringOrderPage.verifyOrdreNumberIsPresent(orderNumber);		


		//verifying the recurring order status before cancellation
		recurringOrderPage = recurringOrderPage.verifyUpdatedOrderStatus("Cancel" , orderNumber);

	}


	/**
	 * Shopper reorders the recurring Order to complete a normal flow 
	 * Pre-conditions: <ul>
	 * 					<li>Tester knows the Store URL of Aurora store						
	 * 				   </ul>
	 * Post conditions: Recurring order is created.
	 * @throws Exception
	 */
	@Test
	public void FV2STOREB2C_5308() throws Exception {

		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();

		//Log in to the store.
		MyAccountMainPage myAccount = signIn.typeUsername(loginId).typePassword(password)
				.signIn().closeSignOutDropDownWidget().goToMyAccount();


		//Go to department page				
		CategoryPage subCat = myAccount.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"),dsm.getInputParameter("SUB_CAT"));

		//click on product image on Category Display page.
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));				

		//click Add to Cart from Product Display page.
		productDisplay.addToCart();				

		//click on Shopping Cart link from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();

		//click on the checkout button
		ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();

		//Confirm ship as complete option is selected.
		shippingAndBilling=shippingAndBilling.verifyShipAsCompleteIsChecked();

		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling=shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));

		//Confirm the correct shipping address is selected
		shippingAndBilling=shippingAndBilling.verifyShippingAddressSelected(loginId);

		//select  the payment method				
		shippingAndBilling.selectPaymentMethod(
				dsm.getInputParameter("PAY_METHOD"));

		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	

		//Confirm product name
		singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("PRODUCT_NAME"));

		//Confirm item quantity
		singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_QTY"));

		//Confirm total price of the item.
		singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_TOTAL"));

		//Confirm the total price of the order
		singleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL"));						

		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();

		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();


		//Go to MyAccount page
		myAccount=orderConfirmation.getHeaderWidget().goToMyAccount();

		//Reorder the completed order
		ShopCartPage shopcartpage= myAccount.getSidebar().goToOrderHistoryPage().reOrder("1");

		//Schedule as Recurring order
		shopcartpage.checkScheduleThisOrderCheckBox();

		//Go to shipping and Billing page
		ShippingAndBillingPage shippingBillingPage = shopcartpage.continueToNextStep();

		//Schedule the Frequency and select payment methond
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String dateNow = formatter.format(currentDate.getTime());
		shippingBillingPage = shippingBillingPage.selectOrderFrequency(
				dsm.getInputParameter("ORDER_FREQUENCY")).typeStartDate(
						dateNow);

		shippingBillingPage.selectPaymentMethod(
				dsm.getInputParameter("PAYMENTMETHOD")).typeCreditCardNumber(
						dsm.getInputParameter("CREDIT_CARD_NO"));

		//Go To order summary page
		OrderSummarySingleShipPage orderSummaryPage = shippingBillingPage
				.singleShipNext();

		//Go to order confirmation page
		OrderConfirmationPage orderConformationPage = orderSummaryPage
				.completeOrder().verifyScheduledOrderSuccessful();

		//Get order no from order confirmation page.
		String orderNumber = orderConformationPage.getScheduledOrderNumber();

		//create child order
		OrdersFixture orders =f_caslFixtures.createOrdersFixture(loginId, password, getConfig().getStoreName());
		
		orders.createChildOrder(Integer.parseInt(orderNumber));

		//Go to recurring order page
		MyRecuringOrdersPage recurringOrderPage = orderConformationPage.getHeaderWidget().goToMyAccount().getSidebar().goToRecurringOrdersPage();


		//verifying order no present in recurring order page.
		recurringOrderPage = recurringOrderPage.verifyOrdreNumberIsPresent(orderNumber);

		//verifying the recurring order status
		recurringOrderPage = recurringOrderPage.verifyOrderStatus("Active" , orderNumber);



		//Go to the recurring order details page
		RecurringOrderDetailPage recurringOrderDetailPage = recurringOrderPage.goToRecurringOrderDetailPage(orderNumber);

		//Go to child order page
		RecurringChildOrderPage recurringChildOrdrePage = recurringOrderDetailPage.goToChildOrderPage();



		//verifying at least one child order is present
		recurringChildOrdrePage.verifyAtLeatOneChildOrderPresent();

	}
	
	/**
	 * Tear down ran after every test case
	 */
	@After
	public void tearDown(){
		
		try
		{	
			OrdersFixture orders = f_caslFixtures.createOrdersFixture(loginId, password, getConfig().getStoreName());
			
			orders.removeAllItemsFromCart();
			orders.deletePaymentMethod();
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}
		
	}
	
}


