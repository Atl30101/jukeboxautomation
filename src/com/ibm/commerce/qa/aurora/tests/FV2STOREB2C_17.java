package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.OrderDetailPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/** 
 * Scenario FV2STOREB2C_17
 * Shopper view order status and manages orders
 * 
 */

@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_17 extends AbstractAuroraSingleSessionTests
{
	
	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	
	private CaslFixturesFactory f_CaslFactory;

	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_CaslFixtures 
	 */

		@Inject
		public FV2STOREB2C_17(
				Logger log, 
				CaslFoundationTestRule caslTestRule,
				WcWteTestRule wcWebTestRule,
				TestDataProvider dataSetManager,
				CaslFixturesFactory p_CaslFixtures
				)
		{
			super(log, wcWebTestRule, caslTestRule);
			this.dsm = dataSetManager;
			f_CaslFactory = p_CaslFixtures;
			
		}
		

	/**
	 * Shopper views order status summary
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_1701()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter a valid username
		 HeaderWidget header= signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				
			//Enter a valid password
			.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
			
			//Click Sign In
			.signIn().closeSignOutDropDownWidget();
		
		//complete order using WC commerce service layer
		OrdersFixture orders = f_CaslFactory.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"),getConfig().getStoreName());

		orders.addItem(dsm.getInputParameter("ITEM_SKU"), dsm.getInputParameterAsNumber("QUANTITY",Double.class));
		
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();
		
		MyAccountMainPage myAccount = header.goToMyAccount();
		
		//Verify the expected total in MyAccountPage
		myAccount.verifyOrderTotal(dsm.getInputParameter("EXPECTED_TOTAL"))
		
		//Check that date is present
		.verifyDatePresent();
		
	}
	
	/**
	 * Shopper views order status details
	 */
	@Test
	public void testFV2STOREB2C_1702()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter a valid username
		 HeaderWidget header = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				
			//Enter a valid password
			.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
			
			//Click Sign In
			.signIn().closeSignOutDropDownWidget();
		
		//complete order using WC commerce service layer
		OrdersFixture orders = f_CaslFactory.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"),getConfig().getStoreName());

		orders.addItem(dsm.getInputParameter("ITEM_SKU"), dsm.getInputParameterAsNumber("QUANTITY",Double.class));
		
		orders.addPayLaterPaymentMethod();
		
		long orderid = orders.completeOrder();
		
		MyAccountMainPage myAccount = header.goToMyAccount();
		//Go to Order detail page of the order
		OrderDetailPage orderDetails = myAccount.goToOrderDetailPageByPosition(Integer.valueOf(dsm.getInputParameter("ORDER_ROW")));
		
		orderDetails.verifyOrderNumberPresent(orderid + "");
	}
	
	/**
	 * Shopper re-orders a previously placed order without making any changes after viewing the order details
	 */
	@Test
	public void testFV2STOREB2C_1704()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter a valid username
		HeaderWidget header  = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				
			//Enter a valid password
			.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
			
			//Click Sign In
			.signIn().closeSignOutDropDownWidget();
		
		//complete order using WC commerce service layer
		OrdersFixture orders = f_CaslFactory.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());

		orders.addItem(dsm.getInputParameter("ITEM_SKU"), dsm.getInputParameterAsNumber("QUANTITY",Double.class));
		
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();
		
		MyAccountMainPage myAccount = header.goToMyAccount();
		//Go to Order detail page of the order
		myAccount.goToOrderDetailPageByPosition(Integer.valueOf(dsm.getInputParameter("ORDER_ROW")));
		
		//Browse back to the previous page
		myAccount =getSession().goBack(MyAccountMainPage.class);
		
		//Click on the "ReOrder" link of the first order 
		myAccount.reorderByPosition(Integer.valueOf(dsm.getInputParameter("ORDER_ROW")));
		
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();
	}
	
	/**
	 * Shopper re-orders a previously placed order with making some changes after viewing the order details
	 */
	@Test
	public void testFV2STOREB2C_1705()
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter a valid username
		HeaderWidget header = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				
			//Enter a valid password
			.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
			
			//Click Sign In
			.signIn().closeSignOutDropDownWidget();
		
		//complete order using WC commerce service layer
		OrdersFixture orders = f_CaslFactory.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"),getConfig().getStoreName());

		orders.addItem(dsm.getInputParameter("ITEM_SKU"), dsm.getInputParameterAsNumber("QUANTITY",Double.class));
		
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();
		
		//Go to Order detail page of the order
		MyAccountMainPage myAccount = header.goToMyAccount();
		myAccount.goToOrderDetailPageByPosition(Integer.valueOf(dsm.getInputParameter("ORDER_ROW")));
		
		//Browse back to the previous page
		myAccount =getSession().goBack(MyAccountMainPage.class);
		
		//Click on the "ReOrder" link of the first order 
		ShopCartPage shopCartPage = myAccount.reorderByPosition(Integer.valueOf(dsm.getInputParameter("ORDER_ROW")));
		
		//Change quantity of the the item in the shopping cart
		shopCartPage.changeQuantity(dsm.getInputParameter("ITEM_ROW"), dsm.getInputParameter("NEW_QUANTITY"));
		
		//Click the checkout button
		ShippingAndBillingPage shippingAndBillingPage = shopCartPage.continueToNextStep()
					
			//Select billing method
			.selectPaymentMethod(dsm.getInputParameter("BILLING_METHOD"));
		
		//Continue as single shipment
		OrderSummarySingleShipPage orderSummary = shippingAndBillingPage.singleShipNext();
						
		//Complete order
		orderSummary.completeOrder();
	}
	
	/**
	 * Tear down ran after every test case
	 * @throws Exception 
	 */
	@After
	public void tearDown() throws Exception{
		try
		{
			OrdersFixture orders = f_CaslFactory.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
	
			orders.removeAllItemsFromCart();
			orders.deletePaymentMethod();
			}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}
	}
}
