package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2013
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script



import java.util.logging.Logger;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.CustomerRegisterPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.MyWishListPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.SearchResultsPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;




/** 		
 * Scenario: FV2STOREB2C_14
 * Details: The objective of this test scenario is testing Shopper adds, removes and shares items to/from wish list
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class})
public class FV2STOREB2C_14c extends  AbstractAuroraSingleSessionTests {

	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;


	private CaslFixturesFactory f_caslFixtures;
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dataSetManager
	 */
	@Inject
	public FV2STOREB2C_14c(Logger log, CaslFoundationTestRule caslTestRule, WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager, CaslFixturesFactory p_caslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		f_caslFixtures = p_caslFixtures;
	}
	

	/**
	 * Shopper moves an item from Shopping cart to default wish list (i.e. Wish List)
	 */
	@Test
	public void FV2STOREB2C_1421(){
		
				
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//delete all item from shop cart page.
		ShopCartPage shopCartPage = myAccount.getHeaderWidget().goToShoppingCartPage().removeAllItems();
		
		//Go to the wish list and remove all item from default wish list
		MyWishListPage wishListPage = shopCartPage.getHeaderWidget().goToMyWishList().selectWishList("Wish List").removeAllItems();
		
		
					
		//performing search for getting the product listing
		SearchResultsPage searchResultPage = wishListPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		
		
		
		//adding item to the shop cart
		ProductDisplayPage productDisplayPage = searchResultPage.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT1")).addToCart();
		
		
		
		//Go to the shopping cart page.
		shopCartPage = productDisplayPage.getHeaderWidget().goToShoppingCartPage();
		
		//Add item to default wish list from shopCart page.
		shopCartPage = shopCartPage.moveToWishList("1");
						
		//Go to the My wish list page
		wishListPage = shopCartPage.getHeaderWidget().goToMyWishList();
		
		//verifying the correct product have shown in wish list
		wishListPage = wishListPage.selectWishList("Wish List").verifyProductInWishList(dsm.getInputParameter("PRODUCT1"));
		wishListPage = wishListPage.verifyNumberOfItems("1").removeAllItems();
				
		//clean up
		
		wishListPage.removeAllItems().deleteAllWishLists();
				
	}
	
	/**
	 * Shopper moves an item from Shopping cart to an existing wish list that is not the default wish list
	 */
	@Test
	public void FV2STOREB2C_1422(){
		
				
		//Generating a unique name to wish list
		String wishList = RandomStringUtils.randomAlphanumeric(8);
		
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		
		//delete all item from shop cart page.
		ShopCartPage shopCartPage = myAccount.getHeaderWidget().goToShoppingCartPage().removeAllItems();
		
		//Go to the wish list page and create a new wish list
		MyWishListPage wishListPage = shopCartPage.getHeaderWidget().goToMyWishList().createNewWishList(wishList);
		
		
					
		//performing search for getting the product listing
		SearchResultsPage searchResultPage = wishListPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
		
		
		
		//adding item to the shop cart
		ProductDisplayPage productDisplayPage = searchResultPage.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT1")).addToCart();
		
		
		
		//Go to the shopping cart page.
		shopCartPage = productDisplayPage.getHeaderWidget().goToShoppingCartPage();
		
		//Add item to default wish list from shopCart page.
		shopCartPage = shopCartPage.moveToNonDefaultWishList(dsm.getInputParameter("PRODUCT1"), wishList);
						
		//Go to the My wish list page
		wishListPage = shopCartPage.getHeaderWidget().goToMyWishList();
		
		//verifying the correct product have shown in wish list
		wishListPage = wishListPage.selectWishList(wishList).verifyProductInWishList(dsm.getInputParameter("PRODUCT1"));
		wishListPage = wishListPage.verifyNumberOfItems("1").removeAllItems();
				
		//clean up
		wishListPage.removeAllItems().deleteAllWishLists();
				
	}
	
	/**
	 * Shopper moves an item from Shopping cart to new Wish list when no wish lists have been previoiusly created by user	
	 */
	@Category(Sanity.class)
	@Test
	public void FV2STOREB2C_1423(){
		
				
		//Generating a unique name to wish list
		String wishList = RandomStringUtils.randomAlphanumeric(8);
		
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		
		//delete all item from shop cart page.
		ShopCartPage shopCartPage = myAccount.getHeaderWidget().goToShoppingCartPage().removeAllItems();
		
		//performing search for getting the product listing
		SearchResultsPage searchResultPage = shopCartPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
						
		//adding item to the shop cart
		ProductDisplayPage productDisplayPage = searchResultPage.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT1")).addToCart();
						
		//Go to the shopping cart page.
		shopCartPage = productDisplayPage.getHeaderWidget().goToShoppingCartPage();
		
		//Create wish list from drop down list
		shopCartPage =  shopCartPage.createNewWishListFromDropDown(wishList, dsm.getInputParameter("PRODUCT1"));
		
		//Add item to default wish list from shopCart page.
		shopCartPage = shopCartPage.moveToNonDefaultWishList(dsm.getInputParameter("PRODUCT1"), wishList);
						
		//Go to the My wish list page
		MyWishListPage wishListPage = shopCartPage.getHeaderWidget().goToMyWishList();
		
		//verifying the correct product have shown in wish list
		wishListPage = wishListPage.selectWishList(wishList).verifyProductInWishList(dsm.getInputParameter("PRODUCT1"));
		wishListPage = wishListPage.verifyNumberOfItems("1").removeAllItems();
				
		//clean up
		wishListPage.removeAllItems().deleteAllWishLists();
				
	}
	
	/**
	 * Registered shopper moves items from Shipping and Billing page to default Wish List, a new wish list and an existing non-default wish list
	 */
	@Test
	public void FV2STOREB2C_1424(){
		
				
		//Generating a unique name to wish list
		String existingWishList = RandomStringUtils.randomAlphanumeric(8);
		String newWishList = RandomStringUtils.randomAlphanumeric(8);
		
		OrdersFixture orders = f_caslFixtures.createOrdersFixture(dsm.getInputParameter("LOGINID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//Go to the My Wish List page and delete all non default wish list
		MyWishListPage myWishListPage = myAccount.getSidebar().goToWishListPag().deleteAllWishLists();
		
		//remove all item from default wish list
		myWishListPage = myWishListPage.selectWishList("Wish List").removeAllItems();
		
		//Create two new wish list
		myWishListPage = myWishListPage.createNewWishList(existingWishList);
		
		
		
		//delete all item from shop cart page.
//		ShopCartPage shopCartPage = myWishListPage.getHeaderWidget().goToShoppingCartPage().removeAllItems();
		
		orders.removeAllItemsFromCart();
		
		//performing search for getting the product listing
//		SearchResultsPage searchResultPage = myWishListPage.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
//		
//		
//		
//															
//		ProductDisplayPage pdp = searchResultPage.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT1")).addToCart();
//		
//		searchResultPage = pdp.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
//		
//		pdp = searchResultPage.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT2")).addToCart();
//		
//		searchResultPage = pdp.getHeaderWidget().performSearch(SearchResultsPage.class, dsm.getInputParameter("SEARCH_TERM"));
//		
//		pdp = searchResultPage.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT3"));
//		
//		pdp.addToCart();
		
		orders.addItem(dsm.getInputParameter("PRODUCT1_SKU"), 1);
		orders.addItem(dsm.getInputParameter("PRODUCT2_SKU"), 1);
		orders.addItem(dsm.getInputParameter("PRODUCT3_SKU"), 1);
		
		
		//Go to the shopping cart page.
		ShopCartPage shopCartPage = myWishListPage.getHeaderWidget().goToShoppingCartPage();
		
		//Go to the Shipping & Billing Page
		ShippingAndBillingPage shippingAndBillingPage =  shopCartPage.continueToNextStep();
		
		//Add PRODUCT1 to default wish list
		shippingAndBillingPage = shippingAndBillingPage.moveToDefaultWishListByProductNameWhenMoreThanOneItemPresent(dsm.getInputParameter("PRODUCT1"));
		
		//Add  PRODUCT2 to existing wish list
		shippingAndBillingPage = shippingAndBillingPage.moveToNonDefaultWishListWhenMoreThanOneItemPresent(existingWishList, dsm.getInputParameter("PRODUCT2"));
		
		//Add PRODUCT3 to newly created wish list
		shippingAndBillingPage = shippingAndBillingPage.createNewWishListFromDropDown(dsm.getInputParameter("PRODUCT3"), newWishList);
		shippingAndBillingPage.moveToNonDefaultWishListWhenOnlyOneItemPresent(newWishList, dsm.getInputParameter("PRODUCT3"));
		
		//Go to the My wish list page
		MyWishListPage wishListPage = shippingAndBillingPage.getHeaderWidget().goToMyWishList();
		
		//verifying the correct product have shown in default wish list
		wishListPage = wishListPage.selectWishList("Wish List").verifyProductInWishList(dsm.getInputParameter("PRODUCT1"));
		wishListPage = wishListPage.verifyNumberOfItems("1").removeAllItems();
		
		//verifying the correct product have shown in pre existing wish list
		wishListPage = wishListPage.selectWishList(existingWishList).verifyProductInWishList(dsm.getInputParameter("PRODUCT2"));
		wishListPage = wishListPage.verifyNumberOfItems("1").removeAllItems();
		
		//verifying the correct product have shown in newly created wish list
		wishListPage = wishListPage.selectWishList(newWishList).verifyProductInWishList(dsm.getInputParameter("PRODUCT3"));
		wishListPage = wishListPage.verifyNumberOfItems("1").removeAllItems();
				
		//clean up
		wishListPage.removeAllItems().deleteAllWishLists();
							
	}
	
	
	
	/**
	 * Guest shopper adds an item to default Wish List from shopping cart page by converting to a new registered shopper
	 */
	
	@Test
	public void FV2STOREB2C_1425()
	{
		
		//Unique name for User Loging id
		String loginId = RandomStringUtils.randomAlphanumeric(8);
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		
		//opening sub category page 
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		
		//clicking on product image
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//selecting the swatch attribute
		productDisplayPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE1"))
												.selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE2"));
		
		//Adding product to shop cart
		ShopCartPage shopCartPage = productDisplayPage.addToCart().getHeaderWidget().goToShoppingCartPage();
		
		
		//try to add this product in default wish list.
		SignInDropdownWidget  signInPage =  shopCartPage.moveToWishListAsGuestUser(dsm.getInputParameter("PRODUCT_NAME"));
		
		CustomerRegisterPage crp = signInPage.registerCustomer();
		MyAccountMainPage myAccount = crp.typeLogonId(loginId)
		.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
		.typeLastName(dsm.getInputParameter("LAST_NAME"))
		.typePassword(dsm.getInputParameter("PASSWORD"))
		.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
		.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
		.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
		.selectStateOrProvince(dsm.getInputParameter("STATE"))
		.typeCity(dsm.getInputParameter("CITY"))
		.typeZipCode(dsm.getInputParameter("ZIPCODE"))
		.typeEmail(dsm.getInputParameter("EMAIL"))
		.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
		.selectGender(dsm.getInputParameter("GENDER"))
		.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
		.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
		.verifyPreferredLanguageDropDownListPresent()			
		.selectPreferedCurrency(dsm.getInputParameter("PREFERRED_CURRENCY"))
		.verifyPreferredCurrencyDropDownListPresent()
		.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
		.verifyBirthdaySelectionPresent()
		.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
		.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
		.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
		.submit();
		
		shopCartPage = myAccount.getHeaderWidget().goToShoppingCartPage();

		shopCartPage.moveToWishListByProductName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Go to wish list page and verify the product got added to wish list.
		MyWishListPage myWishListPage = shopCartPage.getHeaderWidget().goToMyWishList().selectWishList("Wish List").verifyProductInWishList(dsm.getInputParameter("PRODUCT_NAME"));
		
		HeaderWidget header = myWishListPage.getHeaderWidget().openSignOutDropDownWidget().signOut();
		
		header.verifyEmptyMiniShopCart();
		
		//clicking on product image
		subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		
		
		
		productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//selecting the swatch attribute
		productDisplayPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE1a"))
												.selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE2a"));
												
		productDisplayPage.addToCart();
		
		productDisplayPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE1b"))
		.selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE2b"));
		
		productDisplayPage.addToCart();
		
		
		shopCartPage = productDisplayPage.getHeaderWidget().goToShoppingCartPage();
		
		ShippingAndBillingPage shipAndBill = shopCartPage.signInAndContinue(loginId, dsm.getInputParameter("PASSWORD"));
		
		shipAndBill.moveToDefaultWishListByProductPosition(1);
		
		shipAndBill.getHeaderWidget().goToMyAccount();
		
		
		//Go to wish list page and verify the product got added to wish list.
		myWishListPage = shopCartPage.getHeaderWidget().goToMyWishList().selectWishList("Wish List").verifyProductInWishList(dsm.getInputParameter("PRODUCT_NAME"));
		
		//clean up
		myWishListPage.removeAllItems().deleteAllWishLists();
		
		
		
							
	}
	
	/**
	 * Guest shopper adds an item to default Wish List from shopping cart page by converting to a new registered shopper 
	 */
	@Test
	public void FV2STOREB2C_1426()
	{
		
		
		//Unique name for User Loging id
		String loginId = RandomStringUtils.randomAlphanumeric(8);
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//opening sub category page 
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		//clicking on product image
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//selecting the swatch attribute
		productDisplayPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE1"))
												.selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE2"));
		
		//Adding product to shop cart
		ShopCartPage shopCartPage = productDisplayPage.addToCart().getHeaderWidget().goToShoppingCartPage();
		
		
		//try to add this product in default wish list.
		SignInDropdownWidget signInPage =  shopCartPage.moveToWishListAsGuestUser(dsm.getInputParameter("PRODUCT_NAME"));
		
		CustomerRegisterPage crp = signInPage.registerCustomer();
		MyAccountMainPage myAccount = crp.typeLogonId(loginId)
		.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
		.typeLastName(dsm.getInputParameter("LAST_NAME"))
		.typePassword(dsm.getInputParameter("PASSWORD"))
		.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
		.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
		.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
		.selectStateOrProvince(dsm.getInputParameter("STATE"))
		.typeCity(dsm.getInputParameter("CITY"))
		.typeZipCode(dsm.getInputParameter("ZIPCODE"))
		.typeEmail(dsm.getInputParameter("EMAIL"))
		.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
		.selectGender(dsm.getInputParameter("GENDER"))
		.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
		.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
		.verifyPreferredLanguageDropDownListPresent()			
		.selectPreferedCurrency(dsm.getInputParameter("PREFERRED_CURRENCY"))
		.verifyPreferredCurrencyDropDownListPresent()
		.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
		.verifyBirthdaySelectionPresent()
		.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
		.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
		.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
		.submit();
		shopCartPage = myAccount.getHeaderWidget().goToShoppingCartPage();
		shopCartPage.moveToWishListByProductName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Go to wish list page and verify the product got added to wish list.
		MyWishListPage myWishListPage = shopCartPage.getHeaderWidget().goToMyWishList().selectWishList("Wish List").verifyProductInWishList(dsm.getInputParameter("PRODUCT_NAME"));
		
		//clean up
		myWishListPage.removeAllItems().deleteAllWishLists();
									
	}
	
	/**
	 * Guest shopper adds an item to an existing non-default wish list from shopping cart page by converting to a new registered shopper 
	 */
	@Test
	public void FV2STOREB2C_1427()
	{
		
		
		//Unique wish list name
		String wishListName = RandomStringUtils.randomAlphanumeric(8);
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//SingIn into the store.
		MyAccountMainPage myAccountPage = frontPage.getHeaderWidget()
										 .signIn()
										 .typeUsername(dsm.getInputParameter("LOGINID"))
										 .typePassword(dsm.getInputParameter("PASSWORD"))
										 .signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//Going to wish list page and create a wish list
		MyWishListPage myWishListPage = myAccountPage.getHeaderWidget().goToMyWishList();
		myWishListPage = myWishListPage.deleteAllWishLists().createNewWishList(wishListName);
		
		//Login out from store
		HeaderWidget header= myWishListPage.getHeaderWidget().openSignOutDropDownWidget().signOut();

		//opening sub category page 
		CategoryPage subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//clicking on product image
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//selecting the swatch attribute
		productDisplayPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE1"))
												.selectAttributeSwatch(dsm.getInputParameter("ATTRIBUTE2"));
		
		//Adding product to shop cart
		ShopCartPage shopCartPage = productDisplayPage.addToCart().getHeaderWidget().goToShoppingCartPage();
		
		
		//try to add this product in default wish list.
		SignInDropdownWidget signInPage =  shopCartPage.moveToWishListAsGuestUser(dsm.getInputParameter("PRODUCT_NAME"));
		
		signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
								 .typePassword(dsm.getInputParameter("PASSWORD"))
								 .signIn().closeSignOutDropDownWidget();
		
		shopCartPage = signInPage.getHeaderWidget().goToShoppingCartPage();

		shopCartPage.moveToNonDefaultWishList(dsm.getInputParameter("PRODUCT_NAME"),wishListName);
		
		//Go to wish list page and verify the product got added to wish list.
		myWishListPage = shopCartPage.getHeaderWidget().goToMyWishList().selectWishList(wishListName).verifyProductInWishList(dsm.getInputParameter("PRODUCT_NAME"));
		
		//clean up
		myWishListPage.removeAllItems().deleteAllWishLists();
	}
	/**
	 * Guest shopper adds an item to the default Wish List, 
	 * a newly created wish list and an existing non-default wish list from shopping cart page by converting to a returning registered shopper 	
	 */
	@Test
	public void FV2STOREB2C_1428()
	{
		
		
		//Unique wish list names
		String existingWishList = RandomStringUtils.randomAlphanumeric(8);
		String newlyCreatedWishList = RandomStringUtils.randomAlphanumeric(8);
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//SingIn into the store.
		MyAccountMainPage myAccountPage = frontPage.getHeaderWidget()
										 .signIn()
										 .typeUsername(dsm.getInputParameter("LOGINID"))
										 .typePassword(dsm.getInputParameter("PASSWORD"))
										 .signIn().closeSignOutDropDownWidget().goToMyAccount();
		
		//Going to wish list page and create a wish list
		MyWishListPage myWishListPage = myAccountPage.getHeaderWidget().goToMyWishList();
		myWishListPage = myWishListPage.deleteAllWishLists().createNewWishList(existingWishList);
		
		//Login out from store
		HeaderWidget  header = myWishListPage.getHeaderWidget().openSignOutDropDownWidget().signOut();

		//opening sub category page 
		CategoryPage subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//clicking on product image
		ProductDisplayPage productPage = subCat.getCatalogEntryListWidget().goToProductPagebyIdentifier(dsm.getInputParameter("PRODUCT_NAME1"));
		
		productPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("PRO1_ATTR1"))
									.selectAttributeSwatch(dsm.getInputParameter("PRO1_ATTR2"));
		
		productPage.addToCart();
		
		subCat = productPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		productPage = subCat.getCatalogEntryListWidget().goToProductPagebyIdentifier(dsm.getInputParameter("PRODUCT_NAME2"));
				productPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("PRO2_ATTR1"))
		  .selectAttributeSwatch(dsm.getInputParameter("PRO2_ATTR2"));
				productPage.addToCart();
		
		subCat = productPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		productPage = subCat.getCatalogEntryListWidget().goToProductPagebyIdentifier(dsm.getInputParameter("PRODUCT_NAME3"));
		productPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("PRO3_ATTR1"))
		  .selectAttributeSwatch(dsm.getInputParameter("PRO3_ATTR2"));
		productPage.addToCart();
		
		ShopCartPage shopCartPage = productPage.getHeaderWidget().goToShoppingCartPage();
					 
			
		//try to add this product in default wish list.
		SignInDropdownWidget signInPage =  shopCartPage.moveToWishListAsGuestUser(dsm.getInputParameter("PRODUCT_NAME1"));
		
		signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
								 .typePassword(dsm.getInputParameter("PASSWORD"))
								 .signIn().closeSignOutDropDownWidget();
		
		
		shopCartPage = signInPage.getHeaderWidget().goToShoppingCartPage();
		//moving an item into default wish list
		shopCartPage = shopCartPage.moveToWishListByProductName(dsm.getInputParameter("PRODUCT_NAME1"));
		
		//moving an item to existing wish list
		shopCartPage = shopCartPage.moveToNonDefaultWishList(dsm.getInputParameter("PRODUCT_NAME2"), existingWishList);
		
		//Create a new wish list from wish list drop down and moving item to that wish list
		
		shopCartPage = shopCartPage.createNewWishListFromDropDown(newlyCreatedWishList, dsm.getInputParameter("PRODUCT_NAME3"))
									.moveToNonDefaultWishList(dsm.getInputParameter("PRODUCT_NAME3"), newlyCreatedWishList);
		
		
		
		//Go to wish list page and verify the product got added to wish list.
		myWishListPage = shopCartPage.getHeaderWidget().goToMyWishList()
									.selectWishList("Wish List").verifyProductInWishList(dsm.getInputParameter("PRODUCT_NAME1"))
									.selectWishList(existingWishList).verifyProductInWishList(dsm.getInputParameter("PRODUCT_NAME2"))
									.selectWishList(newlyCreatedWishList).verifyProductInWishList(dsm.getInputParameter("PRODUCT_NAME3"));
									
									
		
		//clean up
		myWishListPage.removeAllItems().deleteAllWishLists();
	}
}
