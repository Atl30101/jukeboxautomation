package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.TimeoutException;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderDetailPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;


/**
 * Scenario: FV2STOREB2C_14
 * Details: Test add to shopping cart flows.
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FSTOREB2CCSR_14 extends AbstractAuroraSingleSessionTests
{

    /**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	private boolean doTearDown = true;
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	private final CaslFixturesFactory f_CaslFixtures;
	

	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_CaslFixtures 
	 */	@Inject
	public FSTOREB2CCSR_14(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_CaslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		f_CaslFixtures = p_CaslFixtures;
		
	}
	 /** 
	  * Shopper can not see the Order Summary Page. 
	  */
	 @Test
	 public void testFSTOREB2CCSR_1410()
	 {
		 
		    doTearDown = false;
		 	//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			 
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = frontPage.getHeaderWidget().goToShoppingCartPage();
			
			//Go to shipping and billing page
			ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
			//Confirm the correct Shipping Address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Confirm the correct Billing address is selected
			shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
				.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
							
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
							
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();
			
			MyAccountMainPage accountMainPage = frontPage.getHeaderWidget().goToMyAccountQuickLink();
			OrderDetailPage orderDetailPage = accountMainPage.goToOrderDetailPageByOrderId(orderId);
			orderDetailPage.verifyOrderNumberPresent(orderId);
			
		
			
	 }
	 /** 
	  * shopper's can not see the order comments slider on the order details page or shop cart page
	  */
	 @Test
	 public void testFSTOREB2CCSR_1411()
	 {
		    doTearDown = false;
		 	//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("SHOPPER_LOGONID"), dsm.getInputParameter("SHOPPER_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			 
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = frontPage.getHeaderWidget().goToShoppingCartPage();
			
			//Go to shipping and billing page
			ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
			//Confirm the correct Shipping Address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Confirm the correct Billing address is selected
			shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
				.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
							
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
							
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();
			
			MyAccountMainPage accountMainPage = frontPage.getHeaderWidget().goToMyAccountQuickLink();
			OrderDetailPage orderDetailPage = accountMainPage.goToOrderDetailPageByOrderId(orderId);
			orderDetailPage.verifyOrderNumberPresent(orderId);
		
			
			//order comments section should NOT be visible 
			try{
			orderDetailPage.getCommentsBar().slideOrderComments();
			}catch(TimeoutException e)	{
				System.out.println("OrderCommentsSlideBarWidget should not be available.");
			}
			
			
			try{
				
			orderDetailPage.getHeaderWidget().goToShoppingCartPage()
				.getCommentsBar().slideOrderComments();
				
			}catch(TimeoutException e){
				System.out.println("OrderCommentsSlideBarWidget should not be available.");
			}
								
	 }
	 /** 
	  *  Customer Service Supervisor can access shopper's account
	  */
	 @Test
	 public void testFSTOREB2CCSR_1413()
	 {
		    doTearDown = false;
		 	//Open the store in the browser.
			AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
			//Log in as a registered shopper 
			frontPage.signIn(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"));
			
			//Complete a full shopping flow 
			//add product to cart
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"), getConfig().getStoreName());
			orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			
			//Get the order number of the shopping flow just completed 
			String orderId = orders.getCurrentOrderId();
			 
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = frontPage.getHeaderWidget().goToShoppingCartPage();
			
			//Go to shipping and billing page
			ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
			
			//Confirm product is available on the Shipping and Billing Page
			shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
						
			//Confirm the correct Shipping Address is selected
			shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
				.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
			
			//Confirm the correct Billing address is selected
			shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
				.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
						
			shippingAndBilling.editShippingAddress().selectCountry(dsm.getInputParameter("COUNTRY"))
			.selectStateOrProvince(dsm.getInputParameter("STATE")).typeCity(dsm.getInputParameter("CITY"))
			.typeAddress(dsm.getInputParameter("STREET_ADDRESS")).typeZipCode(dsm.getInputParameter("ZIPCODE"))
			.typeEmail(dsm.getInputParameter("EMAIL")).submitSuccessfulForm();
			
			
			//select Pay later as the payment method
			shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
							
			//click next button
			OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
			
			//click Order button
			OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
							
			//verify that the order has been placed
			orderConfirmation.verifyOrderSuccessful();
			
			MyAccountMainPage accountMainPage = frontPage.getHeaderWidget().goToMyAccountQuickLink();
			OrderDetailPage orderDetailPage = accountMainPage.goToOrderDetailPageByOrderId(orderId);
			orderDetailPage.verifyOrderNumberPresent(orderId);
		
			
			//order comments section should NOT be visible 
			try{
			orderDetailPage.getCommentsBar().slideOrderComments();
			}catch(TimeoutException e)	{
				System.out.println("OrderCommentsSlideBarWidget should not be available.");
			}
			
			
			try{
				
			orderDetailPage.getHeaderWidget().goToShoppingCartPage()
				.getCommentsBar().slideOrderComments();
				
			}catch(TimeoutException e){
				System.out.println("OrderCommentsSlideBarWidget should not be available.");
			}
						
	 }
	 
		
		/**
		 * Tear down ran after every test case
		 */
		@After
		public void tearDown(){
			try
			{
			if (doTearDown) 
			{
				//Continue browser session starting at the home page.
				HeaderWidget headerSection = getSession().continueToPage(getConfig().getStoreUrl(), HeaderWidget.class);

				//Remove all items from the shopping cart
				headerSection.goToShoppingCartPage().clickLockOrder().removeAllItems().clickUnlockOrder()
				.getHeaderWidget().openSignOutDropDownWidget().signOut();
			
			}
			}
			catch(RuntimeException e)
			{
				getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
			}

		}
}