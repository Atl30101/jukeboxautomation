package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2010
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.ApparelSubCategoryPage;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.MyCouponsPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
/**			
* Scenario: FV2STOREB2C_25
* Details: This scenario will compare attributes of selected items.
*
*/
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_16 extends AbstractAuroraSingleSessionTests
{
	@DataProvider
	private final TestDataProvider dsm;
	
	private CMC cmc;
	private final CaslFixturesFactory f_CaslFixtures;
	
	/** Used to verify if a product is present **/
	public static final String CAT_PRODUCT_IMAGE_LINK = "CatalogEntryProdImg_";
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dataSetManager
	 * @param cmc
	 */
	@Inject
	public FV2STOREB2C_16(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CMC cmc,
			CaslFixturesFactory p_CaslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		this.cmc = cmc;
		f_CaslFixtures = p_CaslFixtures;
	}
	
	/**A variable to hold the web activity name**/
	private String uniqueActivityName = null;
	
	/**A variable to hold the web activity identifier created**/
	private String activityId; 
	
	/**A variable to hold the promotion identifier created**/
	private String promotionId;	
	
	private AuroraFrontPage 	auroraFront;
	private ProductDisplayPage 	productDisplay;
	private SignInDropdownWidget			signIn;
	private ShopCartPage		shopCart;

	private HeaderWidget header;
	/**
	 * Shopper applies a coupon with order level discount on the shopping cart page
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_1601() throws Exception
	{
		//login to CMC
		dsm.setDataLocation("testFV2STOREB2C_1601", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore();
		
		//Create a coupon promotion
		dsm.setDataLocation("testFV2STOREB2C_1601", "Create_Promotion");
		promotionId = cmc.getPromotionId(dsm.getInputParameter("PROMOTION_NAME"));
		if (promotionId == null) {
			promotionId = cmc.createPromotion(dsm.getInputParameter("PROMOTION_NAME"), dsm.getInputParameter("PROMOTION_TYPE"));
			cmc.createPromotionElementforOrderLevelPromotion(dsm.getInputParameter("minimunOrderAmount"), dsm.getInputParameter("discountAmount"), dsm.getInputParameter("elementSubType1"), dsm.getInputParameter("elementSubType2"));
		}
		if (!cmc.isPromotionActivated(promotionId)) {
			cmc.activatePromotion(promotionId);
		}
		//Create a content
		dsm.setDataLocation("testFV2STOREB2C_1601", "Create_Content");
		String contentId = cmc.getContentId(dsm.getInputParameter("CONTENT_NAME"));
		if (contentId == null) {
			String paddedText="<div style=\"padding-top:100px;\">" + dsm.getInputParameter("text") + "</div>";
			contentId = cmc.createMarketingContent(dsm.getInputParameter("CONTENT_NAME"), dsm.getInputParameter("CONTENT_TYPE"), "",paddedText, "-1", dsm.getInputParameter("url"));
		}
		
		//create web activity
		dsm.setDataLocation("testFV2STOREB2C_1601", "webActivity");
		cmc.selectCatalog();
		String emsId = cmc.getESpotId(dsm.getInputParameter("ESpot"));
		if(emsId == null){
			emsId = cmc.createESpot(dsm.getInputParameter("ESpot"), dsm.getInputParameter("ESpot_Description"));
		}
		uniqueActivityName = dsm.getInputParameter("ACTIVITY_NAME");
		activityId = cmc.createPromotionRecommendations(uniqueActivityName, emsId, promotionId, contentId);
		
		//activate web activity
		cmc.activateActivity(uniqueActivityName); 

		//log off from CMC
		cmc.logoff();

		dsm.setDataLocation("testFV2STOREB2C_1601", "testFV2STOREB2C_1601");
		
		//Open the store
		auroraFront = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		
		//Go to Sign In Page
		signIn = auroraFront.getHeaderWidget().signIn();
		header = signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD")).signIn().closeSignOutDropDownWidget();
		
		
		//Search for an item
		CategoryPage subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"), dsm.getInputParameter("SUB_CAT"), dsm.getInputParameter("CATEGORY"));
		
		//Go to the product page
		productDisplay = subCat.getCatalogEntryListWidget().goToProductPagebyIdentifier(dsm.getInputParameter("PRODUCTID"));
		
		//Select Swatches
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SwatchColor")).selectAttributeSwatch(dsm.getInputParameter("SwatchSize"));
		
		//Add to cart and go department page
		ApparelSubCategoryPage dept = productDisplay.addToCart().getHeaderWidget().goToCategoryPageByHierarchy(ApparelSubCategoryPage.class, dsm.getInputParameter("TOP_CAT2"), dsm.getInputParameter("SUB_CAT2"));
		
		//click on promotion that adds coupon to coupon wallet
		shopCart = dept.verifyAndClickTextPresent(dsm.getInputParameter("COUPON_TEXT"), ShopCartPage.class);
		
		//verify coupon shows up in my coupon page
		MyCouponsPage couponPage  = shopCart.getHeaderWidget().goToMyAccount().getSidebar().goToMyCouponsPage();
		
		couponPage.verifyCouponIsPresent(dsm.getInputParameter("PROMOTION_NAME"));
		
		shopCart = couponPage.getHeaderWidget().goToShoppingCartPage();
		
		shopCart.changeQuantity(dsm.getInputParameter("shopCartItem"), dsm.getInputParameter("quantity"));
		
		shopCart.applyCouponByName(dsm.getInputParameter("PROMOTION_NAME"));
		
		auroraFront = shopCart.getHeaderWidget().goToFrontPage();
		
		
	}

	/**
	 * Perform tear down operations such as stopping the test harness.
	 * This method will also delete the web activity created in this scenario.
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception
	{	
		try
		{
			//Open the store
			auroraFront = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
			
			//Go to Sign In Page
			signIn = auroraFront.getHeaderWidget().signIn();
			signIn = signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"));
			
			//Click Sign in and go to ShopCartPage
			shopCart = signIn.signIn().closeSignOutDropDownWidget().goToShoppingCartPage();
			
			//Clear ShopCart and SignOut
			//signIn = shopCart.removeAllItems().getHeaderWidget().openSignOutDropDownWidget().signOut().signIn().closeSignOutDropDownWidget();
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
			orders.removeAllItemsFromCart();
			orders.deletePaymentMethod();
			shopCart.getHeaderWidget().openSignOutDropDownWidget().signOut();
			
			//login to CMC
			cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
			
			//select store and catalog
			cmc.selectStore();
			cmc.selectCatalog();
			
			//deactivate and delete web activity
			cmc.deactivateActivity(uniqueActivityName);
			cmc.deleteWebActivity(activityId);
			
			//deactivate and delete Promotion
			cmc.deactivatePromotion(promotionId);
			cmc.deletePromotion(promotionId);
			
			//log off from CMC
			cmc.logoff(); 		
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

	}
}