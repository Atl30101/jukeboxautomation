package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.StoreSelectionPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/**
 * This Scenario is related to physical store. 
 * 
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_41 extends  AbstractAuroraSingleSessionTests {

	@DataProvider
	private final TestDataProvider dsm;
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dataSetManager
	 */
	@Inject
	public FV2STOREB2C_41(Logger log, CaslFoundationTestRule caslTestRule, WcWteTestRule wcWebTestRule, TestDataProvider dataSetManager)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		
	}
	
	/** Shopper searches for physical stores via drop-down menu that finds some stores
	 * Post Condition : 
	 * Stores that match the search criteria are returned with store address, phone number and business hours. 
	 * At least the following results are returned: 
	 * Found Stores = Alton Towers Plaza, Bathurst Plaza, Bay Plaza"
	 * @throws Exception
	 */
	@Test
	public void FV2STOREB2C_4101() throws Exception
	{
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		HeaderWidget header = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		StoreSelectionPage storeSelectionPage = header.goToStoreLocator();
		
		storeSelectionPage = storeSelectionPage.selectCountry(dsm.getInputParameter("COUNTRY"))
												.selectState(dsm.getInputParameter("STATE"))
												.selectCity(dsm.getInputParameter("CITY"))
												.submitSearch();
		
		//validating the result has returned as expected 
		storeSelectionPage = storeSelectionPage.verifyStoreExistsInSearchList(dsm.getInputParameter("STORE1"))
												.verifyStoreExistsInSearchList(dsm.getInputParameter("STORE2"))
												.verifyStoreExistsInSearchList(dsm.getInputParameter("STORE3"));
		
		
	}
	
	
}
