package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.ApparelSubCategoryPage;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.CustomerRegisterPage;
import com.ibm.commerce.qa.aurora.page.DepartmentPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.MySubscriptionsPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderDetailPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.page.SubscriptionChildOrderPage;
import com.ibm.commerce.qa.aurora.page.SubscriptionOrderDetailPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
/**
 * Scenario: FV2STOREB2C_55
 * Details: Create a Subscription Product in  CMC
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_55 extends AbstractAuroraSingleSessionTests
{

	@DataProvider
	private final TestDataProvider dsm;
	private CaslFixturesFactory f_caslFixtures;

	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dataSetManager
	 * @param p_caslFixtures 
	 */
	@Inject
	public FV2STOREB2C_55(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_caslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		f_caslFixtures = p_caslFixtures;
	}

	
	
	/**
	 * Test to quick order an item with valid SKU and valid quantity, as a registered shopper.
	 * Pre-conditions: <ul>
	 * 					<li>Tester knows the Store URL (http://<hostname>/webapp/wcs/stores/servlet/en/auroraesite)</li>
	 * 					<li>Product Quick Info, AJAX add to shopping cart, AJAX checkout, AJAX My Account</li>				
	 * 				   </ul>
	 * Flex flow options: Quick Order is enabled.
	 * Post conditions: Items are added to shopping cart
	 * 
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_5501() throws Exception
	{	
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);	
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		//Enter a valid username
		HeaderWidget header = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				//Enter a valid password
				.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
				//Click Sign In
				.signIn().closeSignOutDropDownWidget();

		//Go to category page from the Department Dropdown 
		CategoryPage subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"), dsm.getInputParameter("SUB_CAT"));
		//Go to the product page of an item
		ProductDisplayPage productPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("ITEM_NAME"));
		//Select the appropriate attribute
		productPage.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("ATTRIBUTE_NAME"), dsm.getInputParameter("ATTRIBUTE_VALUE"));
		//Add the product into the cart
		productPage.addToCart();
		//Go to the shopping cart page from the header
		ShopCartPage shopCart = productPage.getHeaderWidget().goToShoppingCartPage();
		//Click the Checkout button
		ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
		//Select the billing method
		OrderSummarySingleShipPage orderSummary = shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAYMENT_METHOD"))
				//Continue as single shipment
				.singleShipNext();
		//Complete the order
		OrderConfirmationPage confirmation = orderSummary.completeOrder();
		//get Order number
		String ordernumber = confirmation.getOrderNumber();
		//Go to My Accounts page from the header
		MyAccountMainPage myAccount = confirmation.getHeaderWidget().goToMyAccount();
		//Go to the order detail page of the latest order
		OrderDetailPage orderDetail = myAccount.goToOrderDetailPageByOrderId(ordernumber);
		//Verify the total order amount 
		orderDetail.verifyTotalAmount(dsm.getInputParameter("ORDER_AMOUNT"));
//		//Sign out
//		signInPage = orderDetail.getHeaderWidget().openSignOutDropDownWidget().signOut();
		//Wait until the child order for the subscription has been processed
		//create child order
		OrdersFixture orders =f_caslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		
		orders.createChildOrderForSubscription(Integer.parseInt(ordernumber), "N", "N");
		
//		//Enter a valid username
//		myAccount  = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
//				//Enter a valid password
//				.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
//				//Click Sign In
//				.signIn().closeSignOutDropDownWidget();
		
		myAccount = orderDetail.getHeaderWidget().goToMyAccount();
		//Go to the order detail page of the child order
		myAccount.getSidebar().goToSubscriptionsPage().goToOrderDetailbyPosition("1");
		//Verify that the total order amount was divided by the number of issues
		orderDetail.verifyTotalAmount(dsm.getInputParameter("CHILD_ORDER_AMOUNT"));

	}

	/**
	 * Guest user  books a subscription Order after he registers in the store.
	 * in shipping and billing page .
	 * Pre-conditions: <ul>
	 * 					<li>Tester knows the Store URL of Aurora store						
	 * 				   </ul>
	 * Post conditions: Subscription Order is created successfully.
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_5503() throws Exception
	{	
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);			

		//Go to category page from the Department Dropdown 
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"), dsm.getInputParameter("SUB_CAT"));

		//Go to the product page of an item	
		ProductDisplayPage productPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("ITEM_NAME"));

		//Select the appropriate attribute
		productPage.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("ATTRIBUTE_NAME"), dsm.getInputParameter("ATTRIBUTE_VALUE"));

		//Add the product into the cart
		productPage= productPage.addToCart();

		//Register Guest user .
		CustomerRegisterPage registrationPage = productPage.getHeaderWidget().signIn().registerCustomer();				

		//Registering the customer
		String loginId = RandomStringUtils.randomAlphanumeric(8);
		
		MyAccountMainPage myAccountpPage = registrationPage
				//type username
				.typeLogonId(loginId)
				.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
				//type last name
				.typeLastName(dsm.getInputParameter("LAST_NAME"))
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				//Verify password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
				//type street address
				.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
				//Select country
				.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
				//type or select state
				.selectStateOrProvince(dsm.getInputParameter("STATE"))
				//type city
				.typeCity(dsm.getInputParameter("CITY"))
				//type zipcode
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				//type E-mail
				.typeEmail(dsm.getInputParameter("EMAIL"))
				//type home phone number
				.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
				//Select gender
				.selectGender(dsm.getInputParameter("GENDER"))
				//type mobile phone number
				.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
				//Update Allow Me Option check box
				.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
				//Check if preferred language drop down is visible
				.verifyPreferredLanguageDropDownListPresent()			
				//Select preferred language
				//.selectPreferedCurrency(dsm.getInputParameter("PREFERRED_CURRENCY"))
				//Check if preferred currency drop down is visible
				.verifyPreferredCurrencyDropDownListPresent()
				//Selected preferred language
				//.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
				//Check if birthday selection drop down is visible
				.verifyBirthdaySelectionPresent()
				//Select  birth year
				.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
				//Select birth month
				.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
				//Select birth day
				.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
				//Submit registration, the uesr is taken to the MyAccounts page
				.submit();					

		//Go to shopcart page.				
		ShopCartPage shopCartPage = myAccountpPage.getHeaderWidget().goToShoppingCartPage();


		//Click the Checkout button
		ShippingAndBillingPage shippingAndBilling = shopCartPage.continueToNextStep();

		//Select the billing method
		OrderSummarySingleShipPage orderSummary = shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAYMENT_METHOD"))
				//Continue as single shipment
				.singleShipNext();

		//Complete the order
		OrderConfirmationPage confirmation = orderSummary.completeOrder().verifyOrderSuccessful();		
		String ordernumber=confirmation.getOrderNumber();

		//Go to My Accounts page from the header
		MyAccountMainPage myAccount = confirmation.getHeaderWidget().goToMyAccount();


		//Go to the order detail page of the latest order
		OrderDetailPage orderDetail = myAccount.goToOrderDetailPageByOrderId(ordernumber);		
		orderDetail.verifyOrderNumberPresent(ordernumber);
		
		//Verify the total order amount 
		orderDetail.verifyTotalAmount(dsm.getInputParameter("ORDER_AMOUNT"));
		//Sign out
		SignInDropdownWidget  signInPage  = orderDetail.getHeaderWidget().openSignOutDropDownWidget().signOut().signIn();
		//Wait until the child order for the subscription has been processed
		//create child order
		OrdersFixture orders =f_caslFixtures.createOrdersFixture(loginId, dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		
		orders.createChildOrderForSubscription(Integer.parseInt(ordernumber), "N", "N");
		
		//Enter a valid username
		myAccount  = signInPage.typeUsername(loginId)
				//Enter a valid password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				//Click Sign In
				.signIn().closeSignOutDropDownWidget().goToMyAccount();
		//Go to the order detail page of the child order
		myAccount.goToFirstOrderDetailPage();
		//Verify that the total order amount was divided by the number of issues
		orderDetail.verifyTotalAmount(dsm.getInputParameter("CHILD_ORDER_AMOUNT"));

		MySubscriptionsPage subscriptionpage=myAccount.getSidebar().goToSubscriptionsPage();
		
        //Verify the status of the subscription
		subscriptionpage=subscriptionpage.verifyStatus("Active", dsm.getInputParameter("POSITION"));
        
		//Verify at least one child order is present
		SubscriptionOrderDetailPage subscriptionorderpage=subscriptionpage.goToOrderDetailbyPosition(dsm.getInputParameter("POSITION"));
		SubscriptionChildOrderPage subscriptionchildorer=subscriptionorderpage.goToChildOrderPage().verifyAtLeatOneChildOrderPresent();
		subscriptionchildorer.verifySubscriptionitem(dsm.getInputParameter("POSITION"),dsm.getInputParameter("SUBSCRIPTION_ITEM"));

	}
	
	

	/**
	 * To verify more than 2 subscription item can be subscribed in an Order
	 * 
	 * Pre-conditions: <ul>
	 * 					<li>Tester knows the Store URL of Aurora store						
	 * 				   </ul>
	 * Post conditions: Subscription Order is created successfully.
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_5507() throws Exception
	{	
		
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);			

		//Go to category page from the Department Dropdown 
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"), dsm.getInputParameter("SUB_CAT"));

		//Go to the product page of an item	
		ProductDisplayPage productPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("ITEM_NAME"));

		//Select the appropriate attribute
		productPage.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("ATTRIBUTE_NAME"), dsm.getInputParameter("ATTRIBUTE_VALUE"));

		//Add the product into the cart
		productPage= productPage.addToCart();
		
		//Add the second subscription item
		subCat=productPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"), dsm.getInputParameter("SUB_CAT"));
		
		//Go to the product page of an item2	
        productPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("ITEM_NAME2"));
        productPage.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("ATTRIBUTE_NAME"), dsm.getInputParameter("ATTRIBUTE_VALUE"));
        productPage= productPage.addToCart();
		


		//Register Guest user .
		CustomerRegisterPage registrationPage = productPage.getHeaderWidget().signIn().registerCustomer();				

		//Registering the customer
		String loginId = RandomStringUtils.randomAlphanumeric(8);
		MyAccountMainPage myAccountpPage = registrationPage
				//Enter username
				.typeLogonId(loginId)
				.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
				//type last name
				.typeLastName(dsm.getInputParameter("LAST_NAME"))
				//type password
				.typePassword(dsm.getInputParameter("PASSWORD"))
				//Verify password
				.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
				//type street address
				.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
				//Select country
				.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
				//type or select state
				.selectStateOrProvince(dsm.getInputParameter("STATE"))
				//type city
				.typeCity(dsm.getInputParameter("CITY"))
				//type zipcode
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				//type E-mail
				.typeEmail(dsm.getInputParameter("EMAIL"))
				//type home phone number
				.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
				//Select gender
				.selectGender(dsm.getInputParameter("GENDER"))
				//type mobile phone number
				.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
				//Update Allow Me Option check box
				.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
				//Check if preferred language drop down is visible
				.verifyPreferredLanguageDropDownListPresent()			
				//Select preferred language
				//.selectPreferedCurrency(dsm.getInputParameter("PREFERRED_CURRENCY"))
				//Check if preferred currency drop down is visible
				.verifyPreferredCurrencyDropDownListPresent()
				//Selected preferred language
				//.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
				//Check if birthday selection drop down is visible
				.verifyBirthdaySelectionPresent()
				//Select  birth year
				.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
				//Select birth month
				.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
				//Select birth day
				.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
				//Submit registration, the uesr is taken to the MyAccounts page
				.submit();					
		//Go to shopcart page.				
		ShopCartPage shopCartPage = myAccountpPage.getHeaderWidget().goToShoppingCartPage();

				
				//Validate the cart
		shopCartPage.verifyNumberOfItemsOnCurrentPage(Integer.parseInt(dsm.getInputParameter("NUMBER_OF_ITEMS")));
				
				//validate the items in cart.
		shopCartPage.verifyItemInShopCart(dsm.getInputParameter("ITEM_NAME"));
		shopCartPage.verifyItemInShopCart(dsm.getInputParameter("ITEM_NAME2"));

		//Click the Checkout button
		ShippingAndBillingPage shippingAndBilling = shopCartPage.continueToNextStep();

		//Select the billing method
		OrderSummarySingleShipPage orderSummary = shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAYMENT_METHOD"))
				//Continue as single shipment
				.singleShipNext();

		//Complete the order
		OrderConfirmationPage confirmation = orderSummary.completeOrder().verifyOrderSuccessful();
		String ordernumber=confirmation.getOrderNumber();
		HeaderWidget header =confirmation.getHeaderWidget().openSignOutDropDownWidget().signOut();


		Thread.sleep(120000);
				
		//GO to myaccount page	
		MyAccountMainPage myAccount=header.signIn().typeUsername(loginId).typePassword(dsm.getInputParameter("PASSWORD"))
				.signIn().closeSignOutDropDownWidget().goToMyAccount();
				
	
		//Go to the order detail page of the latest order
		SubscriptionOrderDetailPage subnOrderDetail = myAccount.goToSubscriptionOrderDetailPageByPosition(Integer.parseInt((dsm.getInputParameter("POSITION"))));		
		subnOrderDetail.verifyOrderNumberPresent(ordernumber);
		
		//Verify the total order amount 
		subnOrderDetail.verifyTotalAmount(dsm.getInputParameter("ORDER_AMOUNT"));
		
		//Go to the order detail page of the child order
		 myAccount.getHeaderWidget().goToMyAccount().goToFirstOrderDetailPage();	
		
        //Verify subscription item
		MySubscriptionsPage subscriptionpage=myAccount.getSidebar().goToSubscriptionsPage();
		
        //Verify the status of the subscription
		subscriptionpage=subscriptionpage.verifyStatus("Active", dsm.getInputParameter("POSITION"));
		subscriptionpage=subscriptionpage.verifyStatus("Active", dsm.getInputParameter("POSITION2"));
        
		//Verify atleast one child order is present
		subscriptionpage.goToOrderDetailbyPosition(dsm.getInputParameter("POSITION"));	

	}

	/**
	 * Test to verify "PickUp in Store" option is not selectable when only a subscription item
	 * is in shopping cart
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_5508() throws Exception
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter a valid username
		signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				
		//Enter a valid password
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
		
		//Click Sign In
		.signIn().closeSignOutDropDownWidget();

		//Go to category page from the Department Dropdown 
		CategoryPage subCat = signInPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"), dsm.getInputParameter("SUB_CAT"));
		
		//Go to the product page of a subscription item
		ProductDisplayPage productPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("ITEM_NAME"));
		
		//Select the appropriate attribute
		productPage.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("ATTRIBUTE_NAME"), dsm.getInputParameter("ATTRIBUTE_VALUE"));
		
		//Add the product into the cart
		productPage.addToCart();
		
		//Go to the shopping cart page from the header
		ShopCartPage shopCart = productPage.getHeaderWidget().goToShoppingCartPage();
		
		//Verify that the "PickUp in Store" option is not selectable
		shopCart.verifyPickUpAtStoreIsDisabled();
	}


	/**
	 * Test to verify "PickUp in Store" option is not selectable when a subscription item
	 * and a non-subscription item are in shopping cart
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_5509() throws Exception
	{
		OrdersFixture orders =f_caslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
		//add item to cart
		orders.addItem(dsm.getInputParameter("SKU1"), 1.0);
		//add item to cart
		orders.addItem(dsm.getInputParameter("SKU2"), 1.0);
		
		
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter a valid username
		HeaderWidget header = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				
		//Enter a valid password
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
		
		//Click Sign In
		.signIn().closeSignOutDropDownWidget();

		

		//Go to the shopping cart page from the header
		ShopCartPage shopCart = header.goToShoppingCartPage();
		
		//Verify that the "PickUp in Store" option is not selectable
		shopCart.verifyPickUpAtStoreIsDisabled();
	}


	/**
	 * Test to verify that the "Pick Up at Store" button is deselected and disabled
	 * when a subscription item is added to the card, even if it was selected previously.
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_5510() throws Exception
	{
		//Open Auroraesite store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter a valid username
		signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				
		//Enter a valid password
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
		
		//Click Sign In
		.signIn().closeSignOutDropDownWidget();

		//Go to category page from the Department Dropdown 
		DepartmentPage department = signInPage.getHeaderWidget().goToDepartmentByName(DepartmentPage.class, dsm.getInputParameter("TOP_CAT_2"));
		
		//Click category link from side bar
		ApparelSubCategoryPage apparelSubCat = department.getCategoryNavigationWidget().goToCategoryLink(dsm.getInputParameter("SUB_CAT_2_1"), ApparelSubCategoryPage.class);
		
		//Click category link from side bar
		CategoryPage subCat = apparelSubCat.getCategoryNavigationWidget().goToCategoryLink(dsm.getInputParameter("SUB_CAT_2_2"), CategoryPage.class);
		
		//Click product image
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("ITEM_NAME_2"));
		
		//Select swatches from product display page
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR")).selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));

		//Click add to cart
		productDisplay.addToCart();

		//Go to the shopping cart page from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();

		//Select the "Pick Up at Store" button
		shopCart.selectPickUpAtStore();

		//Click category link
		department.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class,dsm.getInputParameter("TOP_CAT"), dsm.getInputParameter("SUB_CAT"));
		
		//Go to the product page of a subscription item
		ProductDisplayPage productPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("ITEM_NAME"));
		
		//Select the appropriate attribute
		productPage.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("ATTRIBUTE_NAME"), dsm.getInputParameter("ATTRIBUTE_VALUE"));
		
		//Add the product into the cart
		productPage.addToCart();
		
		//Go to the shopping cart page from the header
		productPage.getHeaderWidget().goToShoppingCartPage();
		
		//Verify that the "PickUp in Store" option is not selectable
		shopCart.verifyPickUpAtStoreIsDisabled();
	}


	/**
	 * Tear down ran after every test case
	 * @throws Exception 
	 */
	@After
	public void tearDown() throws Exception{
		try
		{
			OrdersFixture orders =f_caslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
			
			orders.removeAllItemsFromCart();
			orders.deletePaymentMethod();
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}
	}
}