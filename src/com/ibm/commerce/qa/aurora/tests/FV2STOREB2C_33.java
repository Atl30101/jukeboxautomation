package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.MyPersonalInfoPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.constants.ChangeFlowOptions;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.url.StoreManagement;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/**
 * This Scenario is related to enable/disable preferred currency from CMC tool and verify in store front
 * 
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_33 extends  AbstractAuroraSingleSessionTests {

	@DataProvider
	private final TestDataProvider dsm;
	private final CMC cmc;
	private ChangeFlowOptions changeFlowOptions;
	private final StoreManagement storeManagement;
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dataSetManager
	 * @param cmc 
	 * @param storeManagement 
	 * @param changeFlowOptions 
	 */
	@Inject
	public FV2STOREB2C_33(Logger log,
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CMC cmc,
			StoreManagement storeManagement,
			ChangeFlowOptions changeFlowOptions)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		this.cmc= cmc;
		this.storeManagement = storeManagement;
		this.changeFlowOptions = changeFlowOptions;
	}
	

	/**
	 * Enable/Disable preferred currency from CMC tool and verify in store front
	 * @throws Exception
	 */
	@Category(Sanity.class)
	@Test
	public void FV2STOREB2C_3301() throws Exception
	{
		
		//Disable the preferred currency option from CM
		cmc.logon("wcsadmin", "wcs1admin");
		
		
		//Enable quick order if not already enabled.
		storeManagement.disableChangeFlowOption(changeFlowOptions.getPreferredCurrency());
		cmc.logoff();
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().goToMyAccount();
		
		//Go to the personal information page.
		MyPersonalInfoPage myPersonalInformationPage = myAccount.getSidebar().goToMyPersonalInfoPage();
		
		//verifying preferred currency option should not be present
		myPersonalInformationPage.verifyPreferredCurrencyDropDownListNotPresent();
		
		//Enabling the preferred currency from CMC and verifying in store front
		cmc.logon("wcsadmin", "wcs1admin");
		
		//Enable quick order if not already enabled.
		storeManagement.enableChangeFlowOption(changeFlowOptions.getPreferredCurrency());
		cmc.logoff();
		
		myAccount = myPersonalInformationPage.getHeaderWidget()
											 .openSignOutDropDownWidget().signOut().signIn()
											 .typeUsername(dsm.getInputParameter("LOGINID"))
											 .typePassword(dsm.getInputParameter("PASSWORD"))
											 .signIn().goToMyAccount();
		
		//Go to the personal information page.
		myPersonalInformationPage = myAccount.getSidebar().goToMyPersonalInfoPage();
		
		//verifying preferred currency option should  be present
		myPersonalInformationPage.verifyPreferredCurrencyDropDownListPresent();
	}
	
	/**
	 * Enable/Disable preferred language(enabled by default)
	 * @throws Exception
	 */
	@Test
	public void FV2STOREB2C_3302() throws Exception
	{
		
		//Disable the preferred currency option from CM
		cmc.logon("wcsadmin", "wcs1admin");
		
		
		//Enable quick order if not already enabled.
		storeManagement.disableChangeFlowOption(changeFlowOptions.getPreferredLanguage());
		cmc.logoff();
		
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Clicking on Sign In link
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Entering the Login Name and password
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("LOGINID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().goToMyAccount();
		
		//Go to the personal information page.
		MyPersonalInfoPage myPersonalInformationPage = myAccount.getSidebar().goToMyPersonalInfoPage();
		
		//verifying preferred currency option should not be present
		myPersonalInformationPage.verifyPreferredLanguageDropDownListNotPresent();
		
		//Enabling the preferred currency from CMC and verifying in store front
		cmc.logon("wcsadmin", "wcs1admin");
		
		//Enable quick order if not already enabled.
		storeManagement.enableChangeFlowOption(changeFlowOptions.getPreferredLanguage());
		cmc.logoff();
		
		myAccount = myPersonalInformationPage.getHeaderWidget()
											 .openSignOutDropDownWidget().signOut().signIn()
											 .typeUsername(dsm.getInputParameter("LOGINID"))
											 .typePassword(dsm.getInputParameter("PASSWORD"))
											 .signIn().goToMyAccount();
		
		//Go to the personal information page.
		myPersonalInformationPage = myAccount.getSidebar().goToMyPersonalInfoPage();
		
		//verifying preferred currency option should  be present
		myPersonalInformationPage.verifyPreferredLanguageDropDownListPresent();
		
		
	}
	
	/*
	 * Need to implement TearDown to make sure language is reset
	 * 
	 * NOTE: be sure to put tearDown in try catch block
	 */
	
}
