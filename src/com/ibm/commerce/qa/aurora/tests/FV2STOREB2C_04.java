package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.ApparelSubCategoryPage;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.DepartmentPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;



/**
 * Scenario: FV2STOREB2C_04
 * Details: Scenario to view items by navigating to them via different menus and viewing them in different formats.
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_04 extends AbstractAuroraSingleSessionTests
{

	/**
	 * The internal copyright field.
	 */
	 public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	//A Variable to retrieve data from the data file. 
	@DataProvider
	private final TestDataProvider dsm;
	private final CaslFixturesFactory f_CaslFixtures;
	
	//boolean doTearDown = false;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 */	@Inject
	public FV2STOREB2C_04(
			Logger log, 
			WcWteTestRule wcWebTestRule,
			CaslFoundationTestRule caslTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_CaslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		f_CaslFixtures = p_CaslFixtures;
	}
	
	
	/**
	 * Test to Browse and Navigate to categories and items using the top menu
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_0401()
	{		
		//Go to Front page
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Click sign in link
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Logon store
		signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		//Click category link from department drop down
		DepartmentPage department = signIn.getHeaderWidget().goToDepartmentByName(DepartmentPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"));
		
		//Click category link from department drop down
		CategoryPage subCat = department.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), 
				dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//Click on product image
		subCat.getCatalogEntryListWidget().goToProductPageByImagePosition(Integer.valueOf(dsm.getInputParameter("PRODUCT_POSITION")));
		
		//Move back to previous page
		getSession().goBack(CategoryPage.class);
		
		//Move forward to next page
		getSession().goForward(ProductDisplayPage.class);				
	}
	
	/**
	 * Test to Browse and Navigate categories and items using side menu
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_0402() throws Exception
	{
		//Go to front page
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Click sign in link
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Logon store
		signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		//Click category link
		CategoryPage subCat= signIn.getHeaderWidget()
				.goToCategoryPageByHierarchy(CategoryPage.class, 
					 dsm.getInputParameter("TOP_CAT"), 
					 dsm.getInputParameter("SUB_CAT"), 
					 dsm.getInputParameter("CATEGORY_ITEM"));
		
		//CLick category link
		 DepartmentPage department =  subCat.getHeaderWidget().goToDepartmentByName(DepartmentPage.class, dsm.getInputParameter("TOP_CAT"));
		
		//Click category link from side bar
		 ApparelSubCategoryPage dept = department.getCategoryNavigationWidget().goToCategoryLink( dsm.getInputParameter("SUB_CAT_1"), ApparelSubCategoryPage.class);
		
		//Click category link from side bar
		 subCat = dept.getCategoryNavigationWidget().goToCategoryLink(dsm.getInputParameter("SUB_CAT_2"), CategoryPage.class);
		
		//Click product image
		subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
	}
	
	/**
	 * Test to View items in Image View
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_0403() throws Exception
	{
		//Go to front page
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Click Sign In Link
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in store
		signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		//Click Sub Category link
		CategoryPage subCat = signIn.getHeaderWidget()
				.goToCategoryPageByHierarchy(CategoryPage.class,
						dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), 
						dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//Confirm category is listed as grid view
		subCat.getCatalogEntryListWidget().verifyIsGridView();
		
	}
	
	/**
	 * Test to View items in Detailed View
	 * 
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_0404() throws Exception
	{		
		//Go To fron page
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Click Sign In Link
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in store
		signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		//Click sub category link
		CategoryPage subCat = signIn.getHeaderWidget()
				.goToCategoryPageByHierarchy(CategoryPage.class, 
						dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), 
						dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//Confirm category is in detailed view
		subCat.getCatalogEntryListWidget().selectDetailedView();
	}
	
	/**
	 * Test to Navigate pages using the Category Bread Crumb Trail
	 * @throws Exception
	 */

	@Test
	public void testFV2STOREB2C_0405() throws Exception
	{
		//Go to front page
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Click Sign In Link
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in store
		signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
			.signIn().closeSignOutDropDownWidget();
		
		//Click sub category link
		CategoryPage subCat = signIn.getHeaderWidget()
				.goToCategoryPageByHierarchy(CategoryPage.class, 
						dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
						dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), 
						dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));
		
		//Click category from bread crumb
		DepartmentPage department = subCat.getBreadcrumbTrailWidget()
				.goToDepartmentPage(dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"));
		
		//Verify that the selected department page name is correct		
		department.verifyCategoryName(dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"));
	}
	
	/**
	 * Test to Navigate pages using the Checkout Flow Bread Crumb Trail
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2C_0406() throws Exception
	{
		//doTearDown = true;
		
		//Go to front page
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Go to Sign in Page
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
				
		//Go to My Account Page
		signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
			.signIn().closeSignOutDropDownWidget();				
		
		//Click category link from department drop down
		CategoryPage subCat = signIn.getHeaderWidget()
				.goToCategoryPageByHierarchy(CategoryPage.class, 
						dsm.getInputParameter("TOP_CAT"), 
						dsm.getInputParameter("SUB_CAT"), 
						dsm.getInputParameter("CATEGORY_ITEM"));
		
		//Click product name from category page 
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select swatches from product display page
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_COLOR"));
		productDisplay.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_SIZE"));
		
		//Click add to cart
		productDisplay.addToCart();
		
		//Go to shop cart page
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//Click on check out button
		ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();
		
		//Select payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("BILLING_METHOD"));
		
		//Click on Next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
		
		//Click on Shipping and Billing link from breadcrumb
		shippingAndBilling = singleOrderSummary.getShipmentBreadCrumbWidget().goToShippingAndBilling();
		
		//Click on Shop cart Link from breadcrumb
		shopCart = shippingAndBilling.getShipmentBreadCrumbWidget().goToShoppingCart();
		
		//doTearDown = false;
	}
	
	/**
	 * Test to Access all links in Footer
	 * @throws Exception
	 */
	@Test
	@DataProvider(auto=false)
	public void testFV2STOREB2C_0407() throws Exception
	{
		
		//Go to front page
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		frontPage.getFooterWidget().goToAboutUsPage()
		
		.getFooterWidget().goToContactUsCorporatePage()
		
		.getFooterWidget().goToContactUsPage()
		
		.getFooterWidget().goToHelpPage()
		
		.getFooterWidget().goToReturnPolicyPage()
		
		.getFooterWidget().goToPrivacyPolicyPage()
		
		.getFooterWidget().goToStoreLocatorPage()
		
		.getFooterWidget().goToSiteMapPage()
		
		.getFooterWidget().goToAdvancedSearchPage();
		
	}
		
	
	
	/**
	 * Tear down ran after every test case
	 */
	@After
	public void tearDown(){
		try
		{	/* The old tear down method
			if (doTearDown) 
			{
				//Continue browser session starting at the home page.
				HeaderWidget headerSection = getSession().continueToPage(getConfig().getStoreUrl(), HeaderWidget.class);
				
				//Remove all items from the shopping cart
				headerSection.goToShoppingCartPage().removeAllItems().getHeaderWidget().openSignOutDropDownWidget().signOut();
				}
			*/
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
			orders.deletePaymentMethod();
			orders.removeAllItemsFromCart();
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

	}
	
}
