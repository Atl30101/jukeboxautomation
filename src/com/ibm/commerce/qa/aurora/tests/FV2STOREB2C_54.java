package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.BundleDisplayPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.KitDisplayPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.MyRecuringOrdersPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderDetailPage;
import com.ibm.commerce.qa.aurora.page.OrderSummaryMultipleShipPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.RecurringChildOrderPage;
import com.ibm.commerce.qa.aurora.page.RecurringOrderDetailPage;
import com.ibm.commerce.qa.aurora.page.RegisteredShippingBillingInfoPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.Accelerator;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.url.DeltaUpdates;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 *  Scenario: FV2STOREB2C_54
 * Details: Complete Recurring orders
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2C_54 extends AbstractAuroraSingleSessionTests 
{	

	@DataProvider
	private final TestDataProvider dsm;

	/** A variable to hold Accelerator**/
	private Accelerator accelerator;

	/**A variable to hold CMC**/
	private CMC cmc;

	/**A variable to hold delta update**/
	private DeltaUpdates deltaUpdate;

	/**catID of SKU**/
	String catID;

	/**A variable to hold the catalog entry identifier created**/
	private String productId;

	/**A variable to hold the catalog entry identifier created**/
	private String productIdSecond;

	/**A variable to hold the catalog entry identifier created**/
	private String bundleId;

	/**A variable to hold the catalog entry identifier created**/
	private String kitId;

	/**A variable to hold the master catalog**/
	private String masterCatalogId;

	/** Registered User details **/
	String loginId, password="wcs1admin";

	private CaslFixturesFactory f_caslFixtures;

	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * @param cmc 
	 * @param accelerator 
	 * @param deltaUpdate 
	 * @param p_caslFixtures 
	 */
	@Inject
	public FV2STOREB2C_54
	(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CMC cmc,
			Accelerator accelerator,
			DeltaUpdates deltaUpdate,
			CaslFixturesFactory p_caslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		this.cmc = cmc;
		this.accelerator = accelerator;
		this.deltaUpdate = deltaUpdate;
		f_caslFixtures = p_caslFixtures;
	}

	// In this pre set up step , creating a register user that will be use in further test cases.
	/**
	 * @throws Exception
	 */
	@Before
	public void customerRegistration() throws Exception
	{

		loginId = RandomStringUtils.randomAlphanumeric(8);
		//opening the Aurora home page.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);


		frontPage.getHeaderWidget().signIn().registerCustomer()
		//Enter user name
		.typeLogonId(loginId)
		//Enter first name
		.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
		//type last name
		.typeLastName(dsm.getInputParameter("LAST_NAME"))
		//type password
		.typePassword(password)
		//Verify password
		.typeVerifyPassword(password)
		//type street address
		.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
		//Select country
		.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
		//type or select state
		.selectStateOrProvince(dsm.getInputParameter("STATE"))
		//type city
		.typeCity(dsm.getInputParameter("CITY"))
		//type zipcode
		.typeZipCode(dsm.getInputParameter("ZIPCODE"))
		//type E-mail
		.typeEmail(dsm.getInputParameter("EMAIL"))
		//type home phone number
		.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
		//Select gender
		.selectGender(dsm.getInputParameter("GENDER"))
		//type mobile phone number
		.typeMobilePhoneNumber(dsm.getInputParameter("MOBILE_PHONE"))
		//Update Allow Me Option check box
		.allowRememberMeOption(Boolean.valueOf(dsm.getInputParameter("REMEMBER_ME")))
		//Check if preferred language drop down is visible
		.verifyPreferredLanguageDropDownListPresent()	
		//Select preferred language
		.selectPreferedCurrency(dsm.getInputParameter("PREFERRED_CURRENCY"))
		//Check if preferred currency drop down is visible
		.verifyPreferredCurrencyDropDownListPresent()
		//Selected preferred language
		.selectPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))
		//Check if birthday selection drop down is visible
		.verifyBirthdaySelectionPresent()
		//Select  birth year
		.selectBirthYear(dsm.getInputParameter("BIRTH_YEAR"))
		//Select birth month
		.selectBirthMonth(dsm.getInputParameter("BIRTH_MONTH"))
		//Select birth day
		.selectBirthDay(dsm.getInputParameter("BIRTH_DATE"))
		//Submit registration, the u is taken to the MyAccounts page
		.submit();
	}


	/**
	 * A test to login to CMC and create an attachment for product.
	 * @throws Exception
	 */
	@Test
	public void FV2STOREB2C_5402() throws Exception
	{	
		dsm.setDataLocation("FV2STOREB2C_5402", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));


		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore();
		cmc.selectCatalog();

		//Enable recurring order option
		dsm.setDataLocation("FV2STOREB2C_5402", "UPDATESKU");
		catID = cmc.getCatalogEntryId(dsm.getInputParameter("searchText"));	
		getLog().fine("catID" + catID);
		dsm.setDataLocation("FV2STOREB2C_5402", "CHANGESKU");
		cmc.updateProduct(catID);

		//Log out from CMC
		cmc.logoff();

		dsm.setDataLocation("FV2STOREB2C_5402","FV2STOREB2C_5402");
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();

		//Log in to the store.
		signIn.typeUsername(loginId).typePassword(password)
				.signIn().closeSignOutDropDownWidget();

		//Go to Home page
		frontPage = signIn.getHeaderWidget().goToFrontPage();

		//Go to the Dairy category page
		CategoryPage subCat = signIn.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"), dsm.getInputParameter("SUB_CAT")); 
		
		//Go to the Product Details Page for PRODUCT_NAME_1
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME_1"));
		
		//Click on Add to cart
		productDisplay.addToCart();

		//Go back to the Dairy category page
		subCat = productDisplay.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"), dsm.getInputParameter("SUB_CAT")); 
		
		//Go to the Product Details Page for PRODUCT_NAME_2
		productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME_2"));
		
		//Click on Add to cart
		productDisplay.addToCart();

		//click on Shopping Cart link from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();

		//Confirm the SUB TOTAL
		shopCart.verifyOrderSubTotal(dsm.getInputParameter("SUBTOTAL"));

		//Confirm the ORDER TOTAL
		shopCart.verifyOrderTotal(dsm.getInputParameter("ORDERTOTAL"));

		//Select as recurring order
		shopCart.checkScheduleThisOrderCheckBox();

		//click on the checkout button
		ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();

		//Select recurring order frequency
		shippingAndBilling.selectOrderFrequency(dsm.getInputParameter("ORDER_FREQUENCY"));
		
		//Getting the tomorrow date
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String dateNow = formatter.format(currentDate.getTime());
		
		//Enter start date
		shippingAndBilling.typeStartDate(dateNow);

		//Confirm ship as complete option is selected.
		shippingAndBilling.verifyShipAsCompleteIsChecked();

		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME_1"));

		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME_2"));

		//Confirm the correct shipping address is selected
		shippingAndBilling.verifyShippingAddressSelected(loginId);

		//select Cash on delivery as the payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAY_METHOD"));
		
		shippingAndBilling.verifyShoppingCartIsNOTEmpty();

		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();	

		//Confirm product name
		singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("PRODUCT_NAME_2"));

		//Confirm item quantity
		singleOrderSummary.verifyItemQty(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_QTY"));

		//Confirm item each price
		singleOrderSummary.verifyItemEachPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_EACH_TOTAL"));

		//Confirm total price of the item.
		singleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("ITEM_POSITION"),dsm.getInputParameter("ITEM_TOTAL"));

		//Confirm the total price of the order
		singleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL"));						

		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();

		//verify that the order has been placed
		orderConfirmation.verifyScheduledOrderSuccessful()	;

		//Get recurring order number
		String orderNumber = orderConfirmation.getScheduledOrderNumber();

		//Go to my account page
		MyAccountMainPage myAccount = orderConfirmation.getHeaderWidget().goToMyAccount();

		//Go to my recurring order page
		MyRecuringOrdersPage myRecurringOrder = myAccount.getSidebar().goToRecurringOrdersPage();

		//Confirm recurring order present on recurring order history page
		myRecurringOrder.verifyOrder(orderNumber);	

		frontPage.getHeaderWidget().openSignOutDropDownWidget().signOut();
	}

	/**
	 * @throws Exception
	 */
	@Test
	public void FV2STOREB2C_5403() throws Exception
	{	

		//login to CMC
		dsm.setDataLocation("FV2STOREB2C_5403", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));

		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore(getConfig().getStoreName());
		masterCatalogId = cmc.selectCatalog();
		String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
		
		//create a new product
		dsm.setDataLocation("FV2STOREB2C_5403", "createProduct_1");
		String partNumber = dsm.getInputParameter("partNumber");
		productId = cmc.createProduct(dsm.getInputParameter("parentCategory"),partNumber,dsm.getInputParameter("productName"),dsm.getInputParameter("published"));

		//set first offer price
		dsm.setDataLocation("FV2STOREB2C_5403", "offerPrice1");
		cmc.createOfferPriceForCatalogEntry(productId);
		cmc.createListPriceForCatalogEntry(productId);

		//set second offer price
		dsm.setDataLocation("FV2STOREB2C_5403", "offerPrice2");
		cmc.createOfferPriceForCatalogEntry(productId);

		//set third offer price
		dsm.setDataLocation("FV2STOREB2C_5403", "offerPrice3");
		cmc.createOfferPriceForCatalogEntry(productId);

		//create attributes for the created product
		dsm.setDataLocation("FV2STOREB2C_5403", "createProductAttribute");
		String attributeId = cmc.createProductDefiningAttribute(productId);

		//create attributes values for the product
		cmc.createProductDefiningAttributeValues(productId, attributeId, dsm.getInputParameter("attributeValue1"),"1");
		cmc.createProductDefiningAttributeValues(productId, attributeId, dsm.getInputParameter("attributeValue2"),"2");

		//create an SKU for the product
		dsm.setDataLocation("FV2STOREB2C_5403", "createSKU_1");
		String itemId = cmc.createSKU(productId,dsm.getInputParameter("partNumber"),dsm.getInputParameter("skuName"),dsm.getInputParameter("published"));
		cmc.addSKUDefiningAttributeValue(productId,itemId,attributeId,dsm.getInputParameter("attributeValue"),dsm.getInputParameter("languageId"));

		//set first offer price and list price for the SKU
		dsm.setDataLocation("FV2STOREB2C_5403", "offerPrice1");
		cmc.createOfferPriceForCatalogEntry(itemId);
		cmc.createListPriceForCatalogEntry(itemId);

		//set second offer price
		dsm.setDataLocation("FV2STOREB2C_5403", "offerPrice2");
		cmc.createOfferPriceForCatalogEntry(itemId);

		//set third offer price
		dsm.setDataLocation("FV2STOREB2C_5403", "offerPrice3");
		cmc.createOfferPriceForCatalogEntry(itemId);

		//enable recurring order option
		dsm.setDataLocation("FV2STOREB2C_5403", "CHANGESKU");
		cmc.updateProduct(itemId);


		//Create another product
		dsm.setDataLocation("FV2STOREB2C_5403", "createProduct_2");
		String partNumberSecond = dsm.getInputParameter("partNumber");
		productIdSecond = cmc.createProduct(dsm.getInputParameter("parentCategory"),partNumberSecond,dsm.getInputParameter("productName"),dsm.getInputParameter("published"));

		//set first offer price and list price for the SKU
		dsm.setDataLocation("FV2STOREB2C_5403", "offerPrice1");
		cmc.createOfferPriceForCatalogEntry(productIdSecond);
		cmc.createListPriceForCatalogEntry(productIdSecond);

		//set second offer price
		dsm.setDataLocation("FV2STOREB2C_5403", "offerPrice2");
		cmc.createOfferPriceForCatalogEntry(productIdSecond);

		//set third offer price
		dsm.setDataLocation("FV2STOREB2C_5403", "offerPrice3");
		cmc.createOfferPriceForCatalogEntry(productIdSecond);

		//create attributes values for the product
		dsm.setDataLocation("FV2STOREB2C_5403", "createProductAttribute");
		String attributeIdSecond = cmc.createProductDefiningAttribute(productIdSecond);
		cmc.createProductDefiningAttributeValues(productIdSecond, attributeIdSecond, dsm.getInputParameter("attributeValue1"),"1");
		cmc.createProductDefiningAttributeValues(productIdSecond, attributeIdSecond, dsm.getInputParameter("attributeValue2"),"2");

		//create an SKU for the second product
		dsm.setDataLocation("FV2STOREB2C_5403", "createSKU_2");
		String itemIdSecond = cmc.createSKU(productIdSecond,dsm.getInputParameter("partNumber"),dsm.getInputParameter("skuName"),dsm.getInputParameter("published"));
		cmc.addSKUDefiningAttributeValue(productIdSecond,itemIdSecond,attributeIdSecond,dsm.getInputParameter("attributeValue"),dsm.getInputParameter("languageId"));

		//set first offer price and list price for the SKU
		dsm.setDataLocation("FV2STOREB2C_5403", "offerPrice1");
		cmc.createOfferPriceForCatalogEntry(itemIdSecond);
		cmc.createListPriceForCatalogEntry(itemIdSecond);

		//set second offer price
		dsm.setDataLocation("FV2STOREB2C_5403", "offerPrice2");
		cmc.createOfferPriceForCatalogEntry(itemIdSecond);

		//set third offer price
		dsm.setDataLocation("FV2STOREB2C_5403", "offerPrice3");
		cmc.createOfferPriceForCatalogEntry(itemIdSecond);

		//enable recurring order option
		dsm.setDataLocation("FV2STOREB2C_5403", "CHANGESKU");
		cmc.updateProduct(itemIdSecond);
		
		//Wait until delta update completes before timeout
		deltaUpdate.beforeWaitForDelta(masterCatalogId);
		getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
		deltaUpdate.waitForDelta(masterCatalogId, 60);
		
		//log off from CMC
		cmc.logoff();	
		
		dsm.setDataLocation("FV2STOREB2C_5403", "FV2STOREB2C_5403");
		
		OrdersFixture orders =f_caslFixtures.createOrdersFixture(loginId, password, getConfig().getStoreName());
		orders.removeAllItemsFromCart();
		orders.addItem(dsm.getInputParameter("PRODUCT_SKU_1"),1.0);
		orders.addItem(dsm.getInputParameter("PRODUCT_SKU_2"),1.0);
		orders.addItem(dsm.getInputParameter("PRODUCT_SKU_3"),1.0);
		
		//Open store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		HeaderWidget header =frontPage.getHeaderWidget().signIn().typeUsername(loginId).typePassword(password).signIn().closeSignOutDropDownWidget();

		//Go to shopping cart page
		ShopCartPage shopCart = header.goToShoppingCartPage();

		//Go to guest user shipping and billing information page
		ShippingAndBillingPage shippingAndBilling = shopCart.continueToNextStep();

		//Click on edit link on billing section
		RegisteredShippingBillingInfoPage shippingAndBillingInfo =	shippingAndBilling.createShippingAddress();

		//Enter info and submit the new address creating request
		shippingAndBillingInfo.typeRecipient(dsm.getInputParameter("RECIPIENT_1"))
		.typeLastName(dsm.getInputParameter("LAST_NAME"))		 
		.typeAddress(dsm.getInputParameter("STREET_ADDRESS"))
		.typeCity(dsm.getInputParameter("CITY"))
		.selectCountry(dsm.getInputParameter("COUNTRY"))
		.selectStateOrProvince(dsm.getInputParameter("PROVINCE"))
		.typeZipCode(dsm.getInputParameter("ZIPCODE"))
		.typePhone(dsm.getInputParameter("PHONE_NUMBER"))
		.typeEmail(dsm.getInputParameter("EMAIL"))
		.applyToBothShippingBilling()
		.submitSuccessfulForm();

		//Wait for billing address to display
		shippingAndBilling.waitForShippingAddress(dsm.getInputParameter("RECIPIENT_1"));

		//Click on edit link on billing section
		RegisteredShippingBillingInfoPage billingInfo = shippingAndBilling.createBillingAddress();

		billingInfo.verifyIsNotOnShipBillPage();	
		
		//Enter info and submit the new address creating request
		billingInfo.typeRecipient(dsm.getInputParameter("RECIPIENT_2"))
		.typeLastName(dsm.getInputParameter("LAST_NAME"))		 
		.typeAddress(dsm.getInputParameter("STREET_ADDRESS"))
		.typeCity(dsm.getInputParameter("CITY"))
		.selectCountry(dsm.getInputParameter("COUNTRY"))
		.selectStateOrProvince(dsm.getInputParameter("PROVINCE"))
		.typeZipCode(dsm.getInputParameter("ZIPCODE"))
		.typePhone(dsm.getInputParameter("PHONE_NUMBER"))
		.typeEmail(dsm.getInputParameter("EMAIL"))
		.applyToBothShippingBilling()
		.submitSuccessfulForm();

		//Wait for billing address to display
		shippingAndBilling.waitForBillingAddress(dsm.getInputParameter("RECIPIENT_2"));

		//Choose multiple shipment
		shippingAndBilling.chooseMultipleShipping();

		//Select number of payment
		shippingAndBilling.selectNoOfPaymentMethods(dsm.getInputParameter("NUMBER_OF_PAYMENTS_LABEL"));

		//Select billing method
		shippingAndBilling.getMultiplepayment().selectBillingMethodByPaymentsPosition(dsm.getInputParameter("PAY_METHOD"), dsm.getInputParameter("POSITION_1"));

		//update new amount
		shippingAndBilling.getMultiplepayment().typeAmountByPaymentsPosition(dsm.getInputParameter("ITEM1_AMOUNT"), dsm.getInputParameter("POSITION_1"));

		//Select billing method
		shippingAndBilling.getMultiplepayment().selectBillingMethodByPaymentsPosition(dsm.getInputParameter("PAY_METHOD"), dsm.getInputParameter("POSITION_2"));

		//update new amount
		shippingAndBilling.getMultiplepayment().typeAmountByPaymentsPosition(dsm.getInputParameter("ITEM2_AMOUNT"), dsm.getInputParameter("POSITION_2"));

		//Select billing method
		shippingAndBilling.getMultiplepayment().selectBillingMethodByPaymentsPosition(dsm.getInputParameter("PAY_METHOD"), dsm.getInputParameter("POSITION_3"));

		//click next button
		OrderSummaryMultipleShipPage multipleOrderSummary = shippingAndBilling.multipleShipNext();	

		//Confirm the first item's product name
		multipleOrderSummary.verifyItemDetails(dsm.getInputParameter("POSITION_1"), dsm.getInputParameter("PRODUCT_NAME_1"));

		//Confirm item quantity
		multipleOrderSummary.verifyItemQTY(dsm.getInputParameter("POSITION_1"), dsm.getInputParameter("ITEM_QTY"));

		//Confirm item each price
		multipleOrderSummary.verifyItemEachPrice(dsm.getInputParameter("POSITION_1"),dsm.getInputParameter("ITEM_EACH_TOTAL_1"));

		//Confirm total price of the item.
		multipleOrderSummary.verifyItemTotalPrice(dsm.getInputParameter("POSITION_1"),dsm.getInputParameter("ITEM_TOTAL_1"));

		//Confirm the total price of the order
		multipleOrderSummary.verifyOrderTotal(dsm.getInputParameter("TOTAL"));	

		//Confirm the total price of the order
		multipleOrderSummary.verifyOrderSubtotal(dsm.getInputParameter("ORDER_SUBTOTAL"));	

		//Verifies Billing Method on order summary page.
		multipleOrderSummary.verifyBillingMethod(dsm.getInputParameter("PAY_METHOD"));

		//click Order button
		OrderConfirmationPage orderConfirmation = multipleOrderSummary.completeOrder();

		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();
	}


	/**
	 * Schedule a recurring Order with new recurring bundle of recurring SKUs. 
	 * @throws Exception
	 */
	@Test
	public void FV2STOREB2C_5404() throws Exception
	{
		getLog().info("Test case FV2STOREB2C_5404 is running");

		//login in cmc with user having the category manager role
		dsm.setDataLocation("FV2STOREB2C_5404", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));

		//selecting the locale
		cmc.setLocale(dsm.getInputParameter("locale"));


		//selecting store
		cmc.selectStore();
		masterCatalogId = cmc.selectCatalog();


		//opening catalog management tool in cmc
		String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
		//creating a bundle
		dsm.setDataLocation("FV2STOREB2C_5404", "CMCCreateBundle");
		try
		{
			bundleId = cmc.createProduct(dsm.getInputParameter("parentCategory"), dsm.getInputParameter("partNumber"), dsm.getInputParameter("productName"), dsm.getInputParameter("published"));
		}catch(IllegalStateException e)
		{
			bundleId = cmc.getCatalogEntryId(dsm.getInputParameter("partNumber"));
		}
		//creating first component
		dsm.setDataLocation("FV2STOREB2C_5404", "CMCcreateComponen1");
		cmc.createComponent(dsm.getInputParameter("parentCatentryPartNumber"), dsm.getInputParameter("componentPartNumber"), dsm.getInputParameter("quantity"), dsm.getInputParameter("sequence"));

		//creating second component
		dsm.setDataLocation("FV2STOREB2C_5404", "CMCcreateComponen2");
		cmc.createComponent(dsm.getInputParameter("parentCatentryPartNumber"), dsm.getInputParameter("componentPartNumber"), dsm.getInputParameter("quantity"), dsm.getInputParameter("sequence"));
	
		//Wait until delta update completes before timeout
		deltaUpdate.beforeWaitForDelta(masterCatalogId);
		getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
		deltaUpdate.waitForDelta(masterCatalogId, 60);
		
		//log off from CMC
		cmc.logoff();	

		


		dsm.setDataLocation("FV2STOREB2C_5404", "FV2STOREB2C_5404");

		//Open store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Sign In to the store
		MyAccountMainPage myAccountPage = frontPage.getHeaderWidget().signIn().typeUsername(loginId).typePassword(password).signIn().closeSignOutDropDownWidget().goToMyAccount();
		

		//Go to department page
		CategoryPage department = myAccountPage.getFooterWidget().goToSiteMapPage().goToCategoryPageByName(CategoryPage.class, dsm.getInputParameter("TOP_CATEGORY_NAME")); 

		//Verify whether product appears on department
		department.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("BUNDLE_NAME"));

		//Go to the bundle display page by clicking on bundle image
		BundleDisplayPage bundleDisplayPage = department.getCatalogEntryListWidget().goToBundlePagebyIdentifier(dsm.getInputParameter("BUNDLE_NAME"));

		//Add to cart
		bundleDisplayPage = bundleDisplayPage.addToCart();

		//Go to shopping cart page
		ShopCartPage shopCartPage = bundleDisplayPage.getHeaderWidget().goToShoppingCartPage().checkScheduleThisOrderCheckBox();


		//Go to the shipping billing page
		ShippingAndBillingPage shippingBillingPage = shopCartPage.continueToNextStep();

		//Getting the tomorrow date
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String dateNow = formatter.format(currentDate.getTime());

		//Entering the order frequency and order start date.
		shippingBillingPage = shippingBillingPage.selectOrderFrequency(dsm.getInputParameter("ORDER_FREQUENCY")).typeStartDate(dateNow);

		//Entering the payment method and credit card details
		shippingBillingPage.selectPaymentMethod(dsm.getInputParameter("PAYMENTMETHOD")).typeCreditCardNumber(dsm.getInputParameter("CREDIT_CARD_NO"));

		//go the order summary page
		OrderSummarySingleShipPage orderSummaryPage = shippingBillingPage.singleShipNext();

		//Go to order confirmation page . verifying the recurring order got placed successfully
		OrderConfirmationPage orderConformationPage = orderSummaryPage.completeOrder().verifyScheduledOrderSuccessful();

		//Get order no from order confirmation page.
		String orderNumber = orderConformationPage.getScheduledOrderNumber();

		//wait for 2 min to subscription scheduler to run
		//create child order
		OrdersFixture orders =f_caslFixtures.createOrdersFixture(loginId, password, getConfig().getStoreName());
		
		orders.createChildOrder(Integer.parseInt(orderNumber),"Y","N");

		//Go to recurring order page
		MyRecuringOrdersPage recurringOrderPage = orderConformationPage.getHeaderWidget().goToMyAccount().getSidebar().goToRecurringOrdersPage();


		//verifying order no present in recurring order page.
		recurringOrderPage = recurringOrderPage.verifyOrdreNumberIsPresent(orderNumber);

		//verifying the recurring order status
		recurringOrderPage = recurringOrderPage.verifyOrderStatus("Active" , orderNumber);



		//Go to the recurring order details page
		RecurringOrderDetailPage recurringOrderDetailPage = recurringOrderPage.goToRecurringOrderDetailPage(orderNumber);

		//Go to child order page
		RecurringChildOrderPage recurringChildOrdrePage = recurringOrderDetailPage.goToChildOrderPage();



		//verifying at least one child order is present
		recurringChildOrdrePage.verifyAtLeatOneChildOrderPresent();

	}

	/**
	 * To check if Order is scheduled with   a new recurring Package containing existing  recurring items.
	 * @throws Exception
	 */
	@Test
	public void FV2STOREB2C_5406() throws Exception
	{
		getLog().info("Test case FV2STOREB2C_5406 is running");

		//login in cmc with user having the category manager role
		dsm.setDataLocation("FV2STOREB2C_5406", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));

		//selecting the locale
		cmc.setLocale(dsm.getInputParameter("locale"));


		//selecting store
		cmc.selectStore();
		masterCatalogId = cmc.selectCatalog();


		//opening catalog management tool in cmc
		String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
		//creating a bundle
		dsm.setDataLocation("FV2STOREB2C_5406", "CMCCreateKit");
		try
		{
			kitId = cmc.createProduct(dsm.getInputParameter("parentCategory"), dsm.getInputParameter("partNumber"), dsm.getInputParameter("productName"), dsm.getInputParameter("published"));
		}catch (IllegalStateException e)
		{
			kitId = cmc.getCatalogEntryId(dsm.getInputParameter("partNumber"));
		}
		//Creating list price for this kit
		dsm.setDataLocation("FV2STOREB2C_5406", "CMCListPrice");
		cmc.createListPriceForCatalogEntry(kitId);

		//Creating offer price for this kit
		dsm.setDataLocation("FV2STOREB2C_5406", "CMCOfferPrice");
		cmc.createOfferPriceForCatalogEntry(kitId);


		//creating first component
		dsm.setDataLocation("FV2STOREB2C_5406", "CMCcreateComponen1");
		cmc.createComponent(dsm.getInputParameter("parentCatentryPartNumber"), dsm.getInputParameter("componentPartNumber"), dsm.getInputParameter("quantity"), dsm.getInputParameter("sequence"));

		//creating second component
		dsm.setDataLocation("FV2STOREB2C_5406", "CMCcreateComponen2");
		cmc.createComponent(dsm.getInputParameter("parentCatentryPartNumber"), dsm.getInputParameter("componentPartNumber"), dsm.getInputParameter("quantity"), dsm.getInputParameter("sequence"));
		
		//Wait until delta update completes before timeout
		deltaUpdate.beforeWaitForDelta(masterCatalogId);
		getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
		deltaUpdate.waitForDelta(masterCatalogId, 60);
		
		//log off from CMC
		cmc.logoff();

		//log in to Accelerator
		dsm.setDataLocation("FV2STOREB2C_5406", "AcceleratorLogOn");
		accelerator.setFulfillmentCenterName(getConfig().getStoreName());
		accelerator.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"),dsm.getInputParameter("storeName"),dsm.getInputParameter("langId"));
		
		//creating inventory for newly created kit
		dsm.setDataLocation("FV2STOREB2C_5406", "CMCCreateKit");
		accelerator.addAdHocReceiptForSKU(dsm.getInputParameter("partNumber"), "10", "50", "USD");

		//log off from accelerator
		accelerator.logoff();


		dsm.setDataLocation("FV2STOREB2C_5406", "FV2STOREB2C_5406");

		//Open store
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Sign In to the store
		MyAccountMainPage myAccountPage = frontPage.getHeaderWidget().signIn().typeUsername(loginId).typePassword(password).signIn().closeSignOutDropDownWidget().goToMyAccount();

		//Go to department page
		CategoryPage department = myAccountPage.getFooterWidget().goToSiteMapPage().goToCategoryPageByName(CategoryPage.class, dsm.getInputParameter("TOP_CATEGORY_NAME")); 

		//Verify whether product appears on department
		department.getCatalogEntryListWidget().verifyProductIsPresent(dsm.getInputParameter("KIT_NAME"));

		//Go to the bundle display page by clicking on bundle image
		KitDisplayPage kitDisplayPage = department.getCatalogEntryListWidget().goToKitPagebyIdentifier(dsm.getInputParameter("KIT_NAME"));

		//Add to cart
		kitDisplayPage = kitDisplayPage.addToCart();

		//Go to shopping cart page
		ShopCartPage shopCartPage = kitDisplayPage.getHeaderWidget().goToShoppingCartPage().checkScheduleThisOrderCheckBox();


		//Go to the shipping billing page
		ShippingAndBillingPage shippingBillingPage = shopCartPage.continueToNextStep();

		//Getting the today date
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String dateNow = formatter.format(currentDate.getTime());

		//Entering the order frequency and order start date.
		shippingBillingPage = shippingBillingPage.selectOrderFrequency(dsm.getInputParameter("ORDER_FREQUENCY")).typeStartDate(dateNow);

		//Entering the payment method and credit card details
		shippingBillingPage.selectPaymentMethod(dsm.getInputParameter("PAYMENTMETHOD")).typeCreditCardNumber(dsm.getInputParameter("CREDIT_CARD_NO"));

		//go the order summary page
		OrderSummarySingleShipPage orderSummaryPage = shippingBillingPage.singleShipNext();

		//Go to order confirmation page . verifying the recurring order got placed successfully
		OrderConfirmationPage orderConformationPage = orderSummaryPage.completeOrder().verifyScheduledOrderSuccessful();

		//Get order no from order confirmation page.
		String orderNumber = orderConformationPage.getScheduledOrderNumber();

		//wait for 2 min to subscription scheduler to run
		//create child order
		OrdersFixture orders =f_caslFixtures.createOrdersFixture(loginId, password, getConfig().getStoreName());
		
		orders.createChildOrder(Integer.parseInt(orderNumber),"Y","N");

		//Go to recurring order page
		MyRecuringOrdersPage recurringOrderPage = orderConformationPage.getHeaderWidget().goToMyAccount().getSidebar().goToRecurringOrdersPage();


		//verifying order no present in recurring order page.
		recurringOrderPage = recurringOrderPage.verifyOrdreNumberIsPresent(orderNumber);

		//verifying the recurring order status
		recurringOrderPage = recurringOrderPage.verifyOrderStatus("Active" , orderNumber);



		//Go to the recurring order details page
		RecurringOrderDetailPage recurringOrderDetailPage = recurringOrderPage.goToRecurringOrderDetailPage(orderNumber);

		//Go to child order page
		RecurringChildOrderPage recurringChildOrdrePage = recurringOrderDetailPage.goToChildOrderPage();



		//verifying atleast one child order is present
		recurringChildOrdrePage.verifyAtLeatOneChildOrderPresent();

	}

	/**
	 * Mark an existing Product as non recurring and complete a normal order.
	 * Pre-conditions: <ul>
	 * 					<li>Tester knows the Store URL of Aurora store						
	 * 				   </ul>
	 * Post conditions: Order is created successfully.
	 * @throws Exception
	 */
	@Test
	public void FV2STOREB2C_5408() throws Exception
	{
		getLog().info("Test case FV2STOREB2C_5408 is running");

		//login in cmc with user having the category manager role
		dsm.setDataLocation("FV2STOREB2C_5408", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));

		//selecting the locale
		cmc.setLocale(dsm.getInputParameter("locale"));

		//selecting store
		cmc.selectStore();

		//opening catalog management tool in cmc
		masterCatalogId = cmc.selectCatalog();
		String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
		
		//Disable the  recurring order option
		dsm.setDataLocation("FV2STOREB2C_5408", "UPDATESKU");
		catID = cmc.getCatalogEntryId(dsm.getInputParameter("searchText"));	
		getLog().fine("catID" + catID);
		dsm.setDataLocation("FV2STOREB2C_5408", "CHANGESKU");
		cmc.updateProduct(catID);

		//Wait until delta update completes before timeout
		deltaUpdate.beforeWaitForDelta(masterCatalogId);
		getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
		deltaUpdate.waitForDelta(masterCatalogId, 60);

		//log off from CMC
		cmc.logoff();	

		dsm.setDataLocation("FV2STOREB2C_5408","FV2STOREB2C_5408");
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Go to Cateogory page.
		CategoryPage subCat=frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"), dsm.getInputParameter("SUB_CAT"));

		//click on product image on Category Display page.
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));

		//click Add to Cart from Product Display page.
		productDisplay=productDisplay.addToCart();

		//Go to shopcart page
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();

		//Verify recurring order checkbox is not present
		shopCart.verifyScheduleThisOrderCheckBoxIsNotPresent();	

		//Convert guest shopper as registered user
		ShippingAndBillingPage shippingpage=shopCart.signInAndContinue(loginId, password);

		shippingpage=shippingpage.selectPaymentMethod(
				dsm.getInputParameter("PAY_METHOD"));

		//Confirm ship as complete option is selected.
		shippingpage=shippingpage.verifyShipAsCompleteIsChecked();

		//Confirm product is available on the Shipping and Billing Page
		shippingpage=shippingpage.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));

		//Confirm the correct shipping address is selected
		shippingpage=shippingpage.verifyShippingAddressSelected(loginId);

		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingpage.singleShipNext();	

		//Confirm product name
		singleOrderSummary.verifyItemDetails(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("PRODUCT_NAME"));					

		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();		
		String ordernumber=orderConfirmation.getOrderNumber();

		//Go to orderdetail page and verify the order
		OrderDetailPage orderdetailpage= orderConfirmation.getHeaderWidget().goToMyAccount().goToFirstOrderDetailPage();		
		orderdetailpage.verifyOrderNumberPresent(ordernumber);

	}

	/**
	 * To verify that modifying attributes of a product   schedules an Order only if the SKU is recurring.
	 * in shipping and billing page .
	 * Pre-conditions: <ul>
	 * 					<li>Tester knows the Store URL of Aurora store						
	 * 				   </ul>
	 * Post conditions: Order is created successfully.
	 * @throws Exception
	 */

	@Test
	public void FV2STOREB2C_5409() throws Exception
	{
		getLog().info("Test case FV2STOREB2C_5409 is running");

		//login in cmc with user having the category manager role
		dsm.setDataLocation("FV2STOREB2C_5409", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));

		//selecting the locale
		cmc.setLocale(dsm.getInputParameter("locale"));

		//selecting store
		cmc.selectStore();

		//opening catalog management tool in cmc
		masterCatalogId = cmc.selectCatalog();
		String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
		
		//Enable recurring order option of an Item that has Attributes
		dsm.setDataLocation("FV2STOREB2C_5409", "UPDATEITEM");
		catID = cmc.getCatalogEntryId(dsm.getInputParameter("searchText"));	
		getLog().fine("catID" + catID);
		cmc.updateProduct(catID);
		
		//Set one of the SKUs of that Item to be recurring
		dsm.setDataLocation("FV2STOREB2C_5409", "CHANGESKU");
		catID = cmc.getCatalogEntryId(dsm.getInputParameter("searchText"));	
		getLog().fine("catID" + catID);
		cmc.updateProduct(catID);

		//Wait until delta update completes before timeout
		deltaUpdate.beforeWaitForDelta(masterCatalogId);
		getSession().startAtStorePreviewPage(previewURL, AuroraFrontPage.class);
		deltaUpdate.waitForDelta(masterCatalogId, 60);				

		//log off from CMC
		cmc.logoff();	

		dsm.setDataLocation("FV2STOREB2C_5409","FV2STOREB2C_5409");
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);

		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();

		//Log in to the store.
		MyAccountMainPage myAccount = signIn.typeUsername(loginId).typePassword(password)
				.signIn().closeSignOutDropDownWidget().goToMyAccount();

		CategoryPage subCat = myAccount.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), 
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"), dsm.getInputParameter("SELECT_CATEGORY_ITEM_NAME"));

		//click on product image on Category Display page.
		ProductDisplayPage productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));

		//Select the attribute value
		productDisplay.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("ATTR1"), dsm.getInputParameter("VAL1"));
		productDisplay.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("ATTR2"), dsm.getInputParameter("VAL2"));

		//Add product to cart
		productDisplay.addToCart();

		//click on Shopping Cart link from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();	
		
        //Verify that the recurring order checkbox is visible
		shopCart.verifyScheduleThisOrderCheckBoxIsPresent();
        
		//Go to Shipping and Billing page
		ShippingAndBillingPage shippingpage=shopCart.continueToNextStep();

	    //Update attributes in Shipping and Billing page
		shippingpage=shippingpage.changeAttributes("1").updateValueForAttribute(dsm.getInputParameter("ATTR3"), dsm.getInputParameter("VAL3")).updateCartItem(ShippingAndBillingPage.class);
  
        //Verify that the recurring order checkbox is not visible
		shopCart=shippingpage.getHeaderWidget().goToShoppingCartPage();
		shopCart=shopCart.verifyScheduleThisOrderCheckBoxIsNotPresent();		
		
		//Verify that the new SKU is still in the cart
		shopCart.verifySKUNotRemoved(dsm.getInputParameter("SKU_NAME"));

		shippingpage=shopCart.continueToNextStep();

		shippingpage=shippingpage.selectPaymentMethod(
				dsm.getInputParameter("PAY_METHOD"));

		//Confirm product is available on the Shipping and Billing Page
		shippingpage=shippingpage.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));

		//Confirm the correct shipping address is selected
		shippingpage=shippingpage.verifyShippingAddressSelected(loginId);

		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingpage.singleShipNext();	


		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();		
		String ordernumber=orderConfirmation.getOrderNumber();

		//Go to orderdetail page and verify the order
		OrderDetailPage orderdetailpage= orderConfirmation.getHeaderWidget().goToMyAccount().goToFirstOrderDetailPage();		
		orderdetailpage.verifyOrderNumberPresent(ordernumber);


	}


	/**
	 * Perform teardown operations after the test is done, whether it is successful or not.
	 * @throws Exception
	 */
	@After
	public void testScenarioTearDown() throws Exception
	{
		try
		{
		
		getLog().info("testScenarioTearDown running");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));		
		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore();
		cmc.selectCatalog();

		//delete the new product created 
		dsm.setDataLocation("FV2STOREB2C_5403", "createProduct_1");
		try{
			cmc.deleteCatalogEntry(productId);
		}catch(RuntimeException e){}
		dsm.setDataLocation("FV2STOREB2C_5403", "createProduct_2");
		try{cmc.deleteCatalogEntry(productIdSecond);}catch(RuntimeException e){}	

		//delete the newly created bundle
		try{cmc.deleteCatalogEntry(bundleId);}catch(RuntimeException e){}

		//delete the newly created kit
		try{cmc.deleteCatalogEntry(kitId);}catch(RuntimeException e){}	

		//Disable recurring order option of the Item with Attributes
		try{
			dsm.setDataLocation("FV2STOREB2C_5409", "UPDATEITEM");
			catID = cmc.getCatalogEntryId(dsm.getInputParameter("searchText"));	
			getLog().fine("catID" + catID);
			dsm.setDataLocation("FV2STOREB2C_5409", "RESET");
			cmc.updateProduct(catID);
		}catch(RuntimeException e){}
		
		//Set the updated SKU of that Item to be non-recurring
		try{
			dsm.setDataLocation("FV2STOREB2C_5409", "CHANGESKU");
			catID = cmc.getCatalogEntryId(dsm.getInputParameter("searchText"));	
			getLog().fine("catID" + catID);
			dsm.setDataLocation("FV2STOREB2C_5409", "RESET");
			cmc.updateProduct(catID);
		}catch(RuntimeException e){}

		
	}
	catch(RuntimeException e)
	{
		getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
	}
	}

}


