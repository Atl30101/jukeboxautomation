package com.ibm.commerce.qa.aurora.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.AuroraFrontPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.ExternalSitePage;
import com.ibm.commerce.qa.aurora.page.ForgotPasswordPage;
import com.ibm.commerce.qa.aurora.page.NonRegisteredShippingBillingInfoPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.wte.framework.page.BrowserType;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 * Scenario FSTORE.B2C.11
 * Test cases to checkout as a guest shopper.
 */
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class})
public class FV2STOREB2C_11 extends AbstractAuroraSingleSessionTests {

	 /**
     * The internal copyright field.
     */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

	
	@DataProvider(apolloDSM=true)
	
	//A Variable to retrieve data from the data file.
	private final TestDataProvider dsm;
	private final CaslFixturesFactory f_CaslFixtures;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   factory to work with pages and elements
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 */
	@Inject
	public FV2STOREB2C_11(Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_CaslFixtures){
		
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		f_CaslFixtures = p_CaslFixtures;
	}	
	
	/**
	 * Test case to add a single item to shop cart and checkout as a guest shopper
	 * 
	 */
	@Test
	public void testFV2STOREB2C_1101(){
	
		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Select a category and sub category to view the list of products within
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CATEGORY_NAME"), 
				 dsm.getInputParameter("SUB_CATEGORY_NAME") , dsm.getInputParameter("CATEGORY_ITEM_NAME"));

		//Select a product from the list of visible products under the chosen sub-category
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().
				goToProductPageByImagePosition(Integer.parseInt(dsm.getInputParameter("PRODUCT_NUMBER")));
	
		//Select swatch values
		productDisplayPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_ATTRIBUTE_1"))
		
		//Select swatch values
		.selectAttributeSwatch(dsm.getInputParameter("SWATCH_ATTRIBUTE_2"));
		
		//Add the item to the cart
		productDisplayPage.addToCart();
		
		//Opens the shopping cart page
		ShopCartPage shopCartPage = productDisplayPage.getHeaderWidget().goToShoppingCartPage();
		
		//Clicks the guest user checkout button
		NonRegisteredShippingBillingInfoPage nonRegisteredShippingBillingPage = shopCartPage.continueAsGuestUser();
		
		//Enter recipient information
		nonRegisteredShippingBillingPage.typeBillRecipient(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
		
		//Enter last name
		.typeBillLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
		
		//Enter billing address
		.typeBillAddress(dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
		
		//Enter billing city
		.typeBillCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
		
		//Enter billing country
		.selectBillCountry(dsm.getInputParameter("ADDRESS_BOOK_COUNTRY"))
		
		//Enter billing province
		.selectBillState(dsm.getInputParameter("ADDRESS_BOOK_STATE"))
		
		//Enter billing phone number
		.typeBillPhone(dsm.getInputParameter("ADDRESS_PHONE_NUMBER"))
		
		//Enter billing zip code
		.typeBillZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
		
		//Enter e-mail address
		.typeBillEmail(dsm.getInputParameter("ADDRESS_BOOK_EMAIL"))
		
		//Click the check box to make the shipping information the same as the billing information
		.applyToBothShippingBilling();
		
		//Opens the shipping and billing method page
		ShippingAndBillingPage shippingAndBillingPage = nonRegisteredShippingBillingPage.submitSuccessfulForm();
		
		//Selects a user-defined billing method
		shippingAndBillingPage.selectPaymentMethod(dsm.getInputParameter("BILLLING_METHOD"));
		
		//Opens the order summary page
		OrderSummarySingleShipPage orderSummaryPage = shippingAndBillingPage.singleShipNext();
		
		//Submits the order
		OrderConfirmationPage orderConfirmationPage = orderSummaryPage.completeOrder();

		//Confirm that the order was successfully submitted
		orderConfirmationPage.verifyOrderSuccessful(dsm.getInputParameter("ORDER_SUCCESSFUL_MESSAGE"));
	
	}
	
	/**
	 * Test case to add multiple items to shop cart and checkout as a guest shopper
	 * 
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2C_1102() {

		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);		
		
		//Select a category and sub category to view the list of products within
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class,dsm.getInputParameter("TOP_CATEGORY_NAME"), 
				dsm.getInputParameter("CATEGORY_ITEM_NAME"));
	
		//Select a product from the list of visible products under the chosen sub-category
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget()
				.goToProductPageByImagePosition(Integer.parseInt(dsm.getInputParameter("STUDENT_DESK")));
		
		//Add the item to the cart
		productDisplayPage.addToCart();
		
		//Click home link
		frontPage = productDisplayPage.getHeaderWidget().goToFrontPage();
		
		//Select a category and sub category to view the list of products within
		subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class,dsm.getInputParameter("TOP_CATEGORY_NAME2"), 
				dsm.getInputParameter("SUB_CATEGORY_NAME2") , dsm.getInputParameter("CATEGORY_ITEM_NAME2") );
		
		//Select a product from the list of visible products under the chosen sub-category
		productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));

		//Select swatch values
		productDisplayPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_ATTRIBUTE"));
		
		//Add the item to the cart
		productDisplayPage.addToCart();
		
		//Click home link
		frontPage = productDisplayPage.getHeaderWidget().goToFrontPage();
		
		//Select a category and sub category to view the list of products within
		subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CATEGORY_NAME2"), 
				 dsm.getInputParameter("SUB_CATEGORY_NAME2"), dsm.getInputParameter("CATEGORY_ITEM_NAME2"));
		
		//Select a product from the list of visible products under the chosen sub-category
		productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("DRESS"));
		
		//Select swatch values
		productDisplayPage.getDefiningAttributesWidget().selectAttributeSwatch(dsm.getInputParameter("SWATCH_ATTRIBUTE"));
		
		//Add the item to the cart
		productDisplayPage.addToCart();

		//Get the header section page object
		HeaderWidget headerSection = productDisplayPage.getHeaderWidget();
		
		//Confirm the number of items in cart is correct.
		headerSection.verifyMiniCartItemCount(dsm.getInputParameter("HEADER_NO_OF_ITEM_IN_CART"));
		
		//Opens the shopping cart page
		ShopCartPage shopCartPage = headerSection.goToShoppingCartPage();
		
		//Clicks the guest user checkout button
		NonRegisteredShippingBillingInfoPage nonRegisteredShippingBillingPage = shopCartPage.continueAsGuestUser();
		
		//Enter billing recipient
		nonRegisteredShippingBillingPage.typeBillRecipient(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
		
		//Enter last name
		.typeBillLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
		
		//Enter billing address
		.typeBillAddress(dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
		
		//Enter billing city
		.typeBillCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
		
		//Enter billing country
		.selectBillCountry(dsm.getInputParameter("ADDRESS_BOOK_COUNTRY"))
		
		//Enter billing province
		.selectBillState(dsm.getInputParameter("ADDRESS_BOOK_STATE"))
		
		//Enter billing phone number
		.typeBillPhone(dsm.getInputParameter("ADDRESS_PHONE_NUMBER"))
		
		//Enter billing zip code
		.typeBillZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
		
		//Enter e-mail address
		.typeBillEmail(dsm.getInputParameter("ADDRESS_BOOK_EMAIL"))
		
		//Click the check box to make the shipping information the same as the billing information
		.applyToBothShippingBilling();
		
		//Opens the shipping and billing method page
		ShippingAndBillingPage shippingAndBillingPage = nonRegisteredShippingBillingPage.submitSuccessfulForm();
		
		//Selects a user-specified billing method
		shippingAndBillingPage.selectPaymentMethod(dsm.getInputParameter("BILLLING_METHOD"));
		
		//Opens the order summary page
		OrderSummarySingleShipPage orderSummaryPage = shippingAndBillingPage.singleShipNext();
		
		//Submits the order
		OrderConfirmationPage orderConfirmationPage = orderSummaryPage.completeOrder();
		
		//Confirm that the order was successfully submitted
		orderConfirmationPage.verifyOrderSuccessful(dsm.getInputParameter("ORDER_SUCCESSFUL_MESSAGE"));
	
	}
	
	/**
	 * Test case to add items to cart and continue as a guest shopper when sign in fails
	 * 
	 */
	@Test
	public void testFV2STOREB2C_1103() {

		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Select a category and sub category to view the list of products within
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CATEGORY_NAME"), 
				dsm.getInputParameter("CATEGORY_ITEM_NAME"));
		
		//Select a product from the list of visible products under the chosen sub-category
		ProductDisplayPage productPage = subCat.getCatalogEntryListWidget().goToProductPageByImagePosition(Integer.parseInt(dsm.getInputParameter("STUDENT_DESK")))
		
		//Add the item to the cart
		.addToCart();

		//Verify the add to cart success message appears.
		HeaderWidget headerSection = productPage.getHeaderWidget();
	
		//Opens the shopping cart page
		ShopCartPage shopCart = headerSection.goToShoppingCartPage();
		
		//Enter login user name
		shopCart.signInAndContinueFail(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"));
		
		//Get the header section object
		headerSection = shopCart.getHeaderWidget();
		
		//Verifies that the login information was invalid
		headerSection.verifyErrorMessage(dsm.getInputParameter("MESSAGE_TEXT"));
		
		//Continue the checkout process as a guest user
		NonRegisteredShippingBillingInfoPage nonRegisteredShippingBillingPage = shopCart.continueAsGuestUser();
		
		//Enter billing recipient
		nonRegisteredShippingBillingPage.typeBillRecipient(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
		
		//Enter last name
		.typeBillLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
		
		//Enter billing address
		.typeBillAddress(dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
		
		//Enter billing city
		.typeBillCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
		
		//Enter billing country
		.selectBillCountry(dsm.getInputParameter("ADDRESS_BOOK_COUNTRY"))
		
		//Enter billing province
		.selectBillState(dsm.getInputParameter("ADDRESS_BOOK_STATE"))
	
		//Enter billing phone number
		.typeBillPhone(dsm.getInputParameter("ADDRESS_PHONE_NUMBER"))
		
		//Enter billing zip code
		.typeBillZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
		
		//Enter e-mail address
		.typeBillEmail(dsm.getInputParameter("ADDRESS_BOOK_EMAIL"))
		
		//Click the check box to make the shipping information the same as the billing information
		.applyToBothShippingBilling();
		
		//Opens the shipping and billing method page
		ShippingAndBillingPage shippingAndBillingPage = nonRegisteredShippingBillingPage.submitSuccessfulForm();
		
		//Selects a user-specified billing method
		shippingAndBillingPage.selectPaymentMethod(dsm.getInputParameter("BILLLING_METHOD"));
		
		//Opens the order summary page
		OrderSummarySingleShipPage orderSummarySinglePage = shippingAndBillingPage.singleShipNext();
		
		//Submits an order
		OrderConfirmationPage orderConfirmationPage = orderSummarySinglePage.completeOrder();

		//Confirm that the order was successfully submitted
		orderConfirmationPage.verifyOrderSuccessful(dsm.getInputParameter("ORDER_SUCCESSFUL_MESSAGE"));

	}

	/**
	 * Test case to add items to cart and continue as a guest shopper when password reset fails
	 * 
	 */
	@Test
	public void testFV2STOREB2C_1104() {

		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		//Select a category and sub category to view the list of products within
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CATEGORY_NAME"), 
				dsm.getInputParameter("CATEGORY_ITEM_NAME"));

		//Select a product from the list of visible products under the chosen sub-category
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget()
				.goToProductPageByImagePosition(Integer.parseInt(dsm.getInputParameter("STUDENT_DESK")));
		
		//Add the item to the cart
		productDisplayPage.addToCart();

		//Opens the shopping cart page
		ShopCartPage shopCartPage = productDisplayPage.getHeaderWidget().goToShoppingCartPage();
		
		//Clicks the forgot my password link
		ForgotPasswordPage forgotPasswordPage = shopCartPage.forgotPassword();
		
		//Enter user information to retrieve the account password
		forgotPasswordPage.retrieveForgottenPasswordFail(dsm.getInputParameter("STORE_USER_INVALID_NAME"))
		
		//Verify that the user name or password was incorrect
		.verifyErrorMessage(dsm.getInputParameter("MESSAGE_TEXT"));
		
		//Return to the previous page
		getSession().goBack(ForgotPasswordPage.class);
		
		//Return to the shopping cart page
		if(getConfig().getBrowserType() == BrowserType.INTERNET_EXPLORER)
		{
			try
			{
				shopCartPage = getSession().goBack(ShopCartPage.class);
			}catch (Exception e) {
				shopCartPage = getSession().goBack(ShopCartPage.class);
			}
		}else{
			shopCartPage = getSession().goBack(ShopCartPage.class);
		}
		//Continue the guest checkout
		NonRegisteredShippingBillingInfoPage nonRegisteredShippingBillingPage = shopCartPage.continueAsGuestUser();
		
		//Enter billing recipient
		nonRegisteredShippingBillingPage.typeBillRecipient(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
		
		//Enter last name
		.typeBillLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
		
		//Enter billing address
		.typeBillAddress(dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
		
		//Enter billing city
		.typeBillCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
		
		//Enter billing country
		.selectBillCountry(dsm.getInputParameter("ADDRESS_BOOK_COUNTRY"))
		
		//Enter billing province
		.selectBillState(dsm.getInputParameter("ADDRESS_BOOK_STATE"))

		//Enter billing phone number
		.typeBillPhone(dsm.getInputParameter("ADDRESS_PHONE_NUMBER"))		
		
		//Enter billing zip code
		.typeBillZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
		
		//Enter e-mail address
		.typeBillEmail(dsm.getInputParameter("ADDRESS_BOOK_EMAIL"))
		
		//Click the check box to make the shipping information the same as the billing information
		.applyToBothShippingBilling();
		
		//Opens the shipping and billing method page
		ShippingAndBillingPage shippingAndBillingPage = nonRegisteredShippingBillingPage.submitSuccessfulForm();
		
		//Selects a user-specified billing method
		shippingAndBillingPage.selectPaymentMethod(dsm.getInputParameter("BILLLING_METHOD"));
		
		//Opens the order summary page
		OrderSummarySingleShipPage orderSummaryPage = shippingAndBillingPage.singleShipNext();
		
		//Submits an order
		OrderConfirmationPage orderConfirmationPage = orderSummaryPage.completeOrder();
		
		//Confirm that the order was successfully submitted
		orderConfirmationPage.verifyOrderSuccessful(dsm.getInputParameter("ORDER_SUCCESSFUL_MESSAGE"));
	}
	
	/**
	 * Test case to add items to cart and navigate to another web site and come back to store and
	 * continue with the checkout
	 * 
	 */
	@Test
	public void testFV2STOREB2C_1106() {

		//Open the store in the browser.
		AuroraFrontPage frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPage.class);
		
		if (getConfig().getBrowserType() == BrowserType.INTERNET_EXPLORER)
		{
			frontPage.getHeaderWidget().goToShoppingCartPage().removeAllItems();
		}
		
		//Select a category and sub category to view the list of products within
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CATEGORY_NAME"), 
				dsm.getInputParameter("CATEGORY_ITEM_NAME"));

		//Select a product from the list of visible products under the chosen sub-category
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByImagePosition(Integer.parseInt(dsm.getInputParameter("STUDENT_DESK")));
	
		//Add the item to the cart
		productDisplayPage.addToCart();
		
		//Opens the shopping cart page
		ShopCartPage shopCartPage = productDisplayPage.getHeaderWidget().goToShoppingCartPage();
		
		//Goes to a new web site
		getSession().continueToPage(dsm.getInputParameter("WEBSITE_URL"), ExternalSitePage.class);
		
		//Returns to the shopping cart page
		shopCartPage = getSession().goBack(ShopCartPage.class);
		
		//Validate the number of entries in the cart.
		shopCartPage.verifyNumberOfItemsOnCurrentPage(Integer.parseInt(dsm.getInputParameter("NUMBER_OF_ITEMS")));
		
		//Clicks the guest user checkout button
		NonRegisteredShippingBillingInfoPage nonRegisteredShippingBillingPage = shopCartPage.continueAsGuestUser();
		
		//Enter billing recipient
		nonRegisteredShippingBillingPage.typeBillRecipient(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
		
		//Enter last name
		.typeBillLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
		
		//Enter billing address
		.typeBillAddress(dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
		
		//Enter billing city
		.typeBillCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
		
		//Enter billing country
		.selectBillCountry(dsm.getInputParameter("ADDRESS_BOOK_COUNTRY"))
		
		//Enter billing province
		.selectBillState(dsm.getInputParameter("ADDRESS_BOOK_STATE"))

		//Enter billing phone number
		.typeBillPhone(dsm.getInputParameter("ADDRESS_PHONE_NUMBER"))		
		
		//Enter billing zip code
		.typeBillZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
		
		//Enter e-mail address
		.typeBillEmail(dsm.getInputParameter("ADDRESS_BOOK_EMAIL"))
		
		//Click the check box to make the shipping information the same as the billing information
		.applyToBothShippingBilling();
		
		//Opens the shipping and billing method page
		ShippingAndBillingPage shippingAndBillingPage = nonRegisteredShippingBillingPage.submitSuccessfulForm();
		
		//Selects a user-specified billing method
		shippingAndBillingPage.selectPaymentMethod(dsm.getInputParameter("BILLLING_METHOD"));
		
		//Opens the order summary page
		OrderSummarySingleShipPage orderSummaryPage = shippingAndBillingPage.singleShipNext();
		
		//Submits an order
		OrderConfirmationPage orderConfirmationPage = orderSummaryPage.completeOrder();
		
		//Confirm that the order was successfully submitted
		orderConfirmationPage.verifyOrderSuccessful(dsm.getInputParameter("ORDER_SUCCESSFUL_MESSAGE"));
	
	}
	
	/**
	 * Tear down ran after every test case
	 */
	@After
	public void tearDown(){
		
		
		try
		{			
			
			//Continue browser session starting at the home page.
			//HeaderWidget headerSection = getSession().continueToPage(getConfig().getStoreUrl(), HeaderWidget.class);
			
			//Remove all items from the shopping cart
			// headerSection.goToShoppingCartPage().removeAllItems();
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
			
			orders.removeAllItemsFromCart();
			orders.deletePaymentMethod();
			
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

	}
	
}