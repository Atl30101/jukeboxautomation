package com.ibm.commerce.qa.aurorab2b.tests;


/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 * 
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Logger;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.CustomerRegisterationPageB2B;
import com.ibm.commerce.qa.casl.util.CaslModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.OrgAdminConsole;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/** Scenario: PreSetup
 *  Details: This test should be run before any other tests in the bucket to ensure that the users common to many test
 *  cases are registered in the store and tooling as appropriate.
 */
@Category(Sanity.class)
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class, CaslModule.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FSTOREB2BCSR_00 extends AbstractAuroraSingleSessionTests
{
	   /**
	    * The internal copyright field.
     	*/
		public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

		//A variable to send requests to OrgadminConsole
		private OrgAdminConsole oac;
		
		@DataProvider
		private final TestDataProvider dsm;

		/**
		 * Test Class object constructor.
		 * 
		 * @param log
		 * @param config
		 * @param session
		 * @param oac
		 * @param dataSetManager
		 */
		@Inject
		public FSTOREB2BCSR_00(
				Logger log, 
				CaslFoundationTestRule caslTestRule,
				WcWteTestRule wcWteTestRule,
				OrgAdminConsole oac,
				TestDataProvider dataSetManager)
		{
			super(log, wcWteTestRule, caslTestRule);
			this.oac = oac;
			this.dsm = dataSetManager;
		}

	  /**
		* Creates administrative users that will be utilized in other scenarios thru out this bucket.
		* Pre-conditions: <ul>
		*					<li>Tester knows the Store URL (http://<hostname>/webapp/wcs/stores/servlet/en/auroraesite)</li>
		* 				</ul>
		* Post conditions: Registration Successful. The user is taken to My Account page.
		* @throws Exception
		*/
	@Test
	public void setupB2BCSRUsers() throws Exception
	{
		//Logon to orgadminconsole using the admin account
		oac.logon(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"));
		
		//Create a CSR account 
		oac.createNewUser(dsm.getInputParameter("CSR_USERID"), 
				dsm.getInputParameter("CSR_FIRST_NAME") , 
				dsm.getInputParameter("CSR_LAST_NAME"), 
				dsm.getInputParameter("CSR_USERPASSWORD"), 
				dsm.getInputParameter("CSR_PASSWORD_VERIFY"), 
				dsm.getInputParameter("CSR_POLICYID"), 
				dsm.getInputParameter("CSR_STATUS"), 
				dsm.getInputParameter("CSR_LANGUAGE"), 
				dsm.getInputParameter("CSR_PARENTORG"), 
				dsm.getInputParameter("CSR_ADDRESS"), 
				dsm.getInputParameter("CSR_CITY"), 
				dsm.getInputParameter("CSR_STATE"), 
				dsm.getInputParameter("CSR_COUNTRY"), 
				dsm.getInputParameter("CSR_ZIPCODE"), 
				dsm.getInputParameter("CSR_EMAIL"), 
				dsm.getInputParameter("CSR_PASSWORD_EXPIRED"));
		
		
		//Assign role as a Customer Service Rep
		oac.assignRoleToUser(dsm.getInputParameter("CSR_USERID"),
		dsm.getInputParameter("CSR_PARENTORG"), dsm.getInputParameter("CSR_ROLE"));		
		
		//Create a CSR account 2
		oac.createNewUser(dsm.getInputParameter("CSR_USERID_2"), 
				dsm.getInputParameter("CSR_FIRST_NAME") , 
				dsm.getInputParameter("CSR_LAST_NAME"), 
				dsm.getInputParameter("CSR_USERPASSWORD"), 
				dsm.getInputParameter("CSR_PASSWORD_VERIFY"), 
				dsm.getInputParameter("CSR_POLICYID"), 
				dsm.getInputParameter("CSR_STATUS"), 
				dsm.getInputParameter("CSR_LANGUAGE"), 
				dsm.getInputParameter("CSR_PARENTORG"), 
				dsm.getInputParameter("CSR_ADDRESS"), 
				dsm.getInputParameter("CSR_CITY"), 
				dsm.getInputParameter("CSR_STATE"), 
				dsm.getInputParameter("CSR_COUNTRY"), 
				dsm.getInputParameter("CSR_ZIPCODE"), 
				dsm.getInputParameter("CSR_EMAIL"), 
				dsm.getInputParameter("CSR_PASSWORD_EXPIRED"));
		
		
		//Assign role as a Customer Service Rep 2
		oac.assignRoleToUser(dsm.getInputParameter("CSR_USERID_2"),
		dsm.getInputParameter("CSR_PARENTORG"), dsm.getInputParameter("CSR_ROLE"));		

		//Create a CSS account
		oac.createNewUser(dsm.getInputParameter("CSS_USERID"), 
				dsm.getInputParameter("CSS_FIRST_NAME") , 
				dsm.getInputParameter("CSS_LAST_NAME"), 
				dsm.getInputParameter("CSS_USERPASSWORD"), 
				dsm.getInputParameter("CSS_PASSWORD_VERIFY"), 
				dsm.getInputParameter("CSS_POLICYID"), 
				dsm.getInputParameter("CSS_STATUS"), 
				dsm.getInputParameter("CSS_LANGUAGE"), 
				dsm.getInputParameter("CSS_PARENTORG"), 
				dsm.getInputParameter("CSS_ADDRESS"), 
				dsm.getInputParameter("CSS_CITY"), 
				dsm.getInputParameter("CSS_STATE"), 
				dsm.getInputParameter("CSS_COUNTRY"), 
				dsm.getInputParameter("CSS_ZIPCODE"), 
				dsm.getInputParameter("CSS_EMAIL"), 
				dsm.getInputParameter("CSS_PASSWORD_EXPIRED"));
		
		
		//Assign role as a Customer Service Rep
		 	oac.assignRoleToUser(dsm.getInputParameter("CSS_USERID"), dsm.getInputParameter("CSS_PARENTORG"), dsm.getInputParameter("CSS_ROLE"));	
		
		//Update Password for buyerAadmin
			oac.updateUserPassword(dsm.getInputParameter("B_USERID"),dsm.getInputParameter("B_USERPASSWORD"), dsm.getInputParameter("B_PASSWORD_VERIFY"));
		//Logoff
			oac.logoff();
	}
	
	
	/**
	 * Test case to register a Shopper from the store home page with valid inputs. This shopper can be used across multiple test cases in this bucket.
	 * @throws Exception
	 */
	@Test
	public void setupBuyerOrganization() throws Exception{
		//Open Auroraesite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();
		
		//attempt to sign in:
		try {
			signInPage.typeUsername(dsm.getInputParameter("BUYER_ADMIN_LOGONID"))
				.typePassword(dsm.getInputParameter("PASSWORD"))
				.signIn();
			
			//Open Auroraesite store
			frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

			
		} catch (Exception e1) {
			//Open Auroraesite store
			

			//Click on the registration button on the SignIn page
			CustomerRegisterationPageB2B rp = signInPage.registerB2B();

			rp.selectOrganizationRegister()
			
			//type organization name
			.typeOrganizationName(dsm.getInputParameter("ORGANIZATION_NAME"))
			
			//type organization address
			.typeOrganizationStreetAddressLine1(dsm.getInputParameter("ORGANIZATION_ADDRESS"))
			
			//select organization country
			.selectOrganizationCountryOrRegion(dsm.getInputParameter("ORGANIZATION_COUNTRY"))
			
			//select organization province
			.selectOrganizationStateOrProvince(dsm.getInputParameter("ORGANIZATION_STATE"))
			
			//type organization city
			.typeOrganizationCity(dsm.getInputParameter("ORGANIZATION_CITY"))
			
			//type organization zipcode
			.typeOrganizationZipCode(dsm.getInputParameter("ORGANIZATION_ZIPCODE"))
			
			//type organization email
			.typeOrganizationEmail(dsm.getInputParameter("ORGANIZATION_EMAIL"))
			
			//type organization phone number
			.typeOrganizationPhoneNumber(dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))

			//type buyer username
			.typeOrganizationBuyerLogonId(dsm.getInputParameter("BUYER_ADMIN_LOGONID"))

			//type buyer password
			.typeOrganizationBuyerPassword(dsm.getInputParameter("PASSWORD"))

			//Verify buyer password verify
			.typeOrganizationBuyerVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))

			//type buyer first name
			.typeOrganizationBuyerFirstName(dsm.getInputParameter("FIRST_NAME"))

			//type buyer last name
			.typeOrganizationBuyerLastName(dsm.getInputParameter("LAST_NAME"))

			//type buyer street address
			.typeOrganizationBuyerStreetAddressLine1(dsm.getInputParameter("ADDRESS"))

			//Select buyer country
			.selectOrganizationBuyerCountryOrRegion(dsm.getInputParameter("COUNTRY"))

			//type or select state for buyer
			.selectOrganizationBuyerStateOrProvince(dsm.getInputParameter("STATE"))

			//type buyer city
			.typeOrganizationBuyerCity(dsm.getInputParameter("CITY"))

			//type buyer zipcode
			.typeOrganizationBuyerZipCode(dsm.getInputParameter("ZIPCODE"))

			//type buyer E-mail
			.typeOrganizationBuyerEmail(dsm.getInputParameter("EMAIL"))

			//type buyer home phone number
			.typeOrganizationBuyerPhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))

			//Check if preferred language drop down is visible
			.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			

			//Select buyer preferred language
			.selectOrganizationBuyerPreferedCurrency(dsm.getInputParameter("PREFERRED_CURRENCY"))

			//Check if preferred currency drop down is visible
			.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()

			//Select buyer preferred language
			.selectOrganizationBuyerPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))

			//Submit registration
			.submitOrganizationBuyerRegistration()
			
			//verify registration confirmation message
			.verifyOrganizationBuyerRegistrationConfirmationMessage();
			
			// Approve Organization
				oac.logon(dsm.getInputParameter("ACCELERATOR_LOGON_ID"), dsm.getInputParameter("ACCELERATOR_PASSWORD"));
				oac.approveAllApprovals();

		}
		
		
	}	
	
	

	/**
	 * Test case to register a Shopper from the store home page with valid inputs. This shopper can be used across multiple test cases in this bucket.
	 * @throws Exception
	 */
	@Test
	public void setupBuyers() throws Exception{
		//Open Auroraesite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();
		
		//attempt to sign in:
		try {
			signInPage.typeUsername(dsm.getInputParameter("BUYER_LOGONID"))
				.typePassword(dsm.getInputParameter("PASSWORD"))
				.signIn();
			
			//Open Auroraesite store
			frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

			
		} catch (Exception e1) {
			//Open Auroraesite store
			

			//Click on the registration button on the SignIn page
			CustomerRegisterationPageB2B rp = signInPage.registerB2B();
			
				
			//Click on the SignIn page link on the header
			signInPage = frontPage.getHeaderWidget().signIn();
			rp = signInPage.registerB2B().selectBuyerRegister();
			
			rp
			//type username
			.typeOrganization(dsm.getInputParameter("ORGANIZATION_NAME"))
			
			.typeLogonId(dsm.getInputParameter("BUYER_LOGONID"))
			
			//type first name
			.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
			
			//type last name
			.typeLastName(dsm.getInputParameter("LAST_NAME"))
			
			//type password
			.typePassword(dsm.getInputParameter("PASSWORD"))
			
			//Verify password
			.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
			
			//type street address
			.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
			
			//Select country
			.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
			
			//type or select state
			.selectStateOrProvince(dsm.getInputParameter("STATE"))
			
			//type city
			.typeCity(dsm.getInputParameter("CITY"))
			
			//type zipcode
			.typeZipCode(dsm.getInputParameter("ZIPCODE"))
			
			//type E-mail
			.typeEmail(dsm.getInputParameter("EMAIL"))
			
			//type home phone number
			.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
			
			//submit user registration
			.submit();
			
			// Approve Buyer
			oac.logon(dsm.getInputParameter("BUYER_ADMIN_LOGONID"), dsm.getInputParameter("BUYER_ADMIN_PASSWORD"));
			oac.approveAllApprovals();	
		}
		
		// Setup Second Buyer under Buyer A Organization
		
		//Click on the SignIn page link on the header
				frontPage.getHeaderWidget().signIn();
				
				//attempt to sign in:
				try {
					signInPage.typeUsername(dsm.getInputParameter("BUYER2_LOGONID"))
						.typePassword(dsm.getInputParameter("PASSWORD"))
						.signIn();
					
					//Open Auroraesite store
					frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

					
				} catch (Exception e1) {
					//Open Auroraesite store
					

					//Click on the registration button on the SignIn page
					CustomerRegisterationPageB2B rp = signInPage.registerB2B();
					
						
					//Click on the SignIn page link on the header
					signInPage = frontPage.getHeaderWidget().signIn();
					rp = signInPage.registerB2B().selectBuyerRegister();
					
					rp
					//type username
					.typeOrganization(dsm.getInputParameter("ORGANIZATION_NAME2"))
					
					.typeLogonId(dsm.getInputParameter("BUYER2_LOGONID"))
					
					//type first name
					.typeFirstName(dsm.getInputParameter("FIRST_NAME2"))
					
					//type last name
					.typeLastName(dsm.getInputParameter("LAST_NAME2"))
					
					//type password
					.typePassword(dsm.getInputParameter("PASSWORD"))
					
					//Verify password
					.typeVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))
					
					//type street address
					.typeStreetAddressLine1(dsm.getInputParameter("ADDRESS"))
					
					//Select country
					.selectCountryOrRegion(dsm.getInputParameter("COUNTRY"))
					
					//type or select state
					.selectStateOrProvince(dsm.getInputParameter("STATE"))
					
					//type city
					.typeCity(dsm.getInputParameter("CITY"))
					
					//type zipcode
					.typeZipCode(dsm.getInputParameter("ZIPCODE"))
					
					//type E-mail
					.typeEmail(dsm.getInputParameter("EMAIL"))
					
					//type home phone number
					.typePhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))
					
					//submit user registration
					.submit();
					
					frontPage.getHeaderWidget().signIn().typeUsername(dsm.getInputParameter("BUYERADMIN2_LOGONID")).typePassword(dsm.getInputParameter("BUYERADMIN2_PASSWORD")).signIn();
					MyAccountMainPage myAccount = frontPage.getHeaderWidget().goToMyAccount();
					myAccount.getSidebar().goToBuyerApprovalPage().getBuyersApprovalWidget().approveBuyer(dsm.getInputParameter("BUYER_NAME"));
					myAccount.getHeaderWidget().openSignOutDropDownWidget().signOutB2B();						
				}
				
				
	}	
}

