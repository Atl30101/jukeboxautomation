package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.TimeoutException;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.casl.keys.CaslKeysFactory;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderDetailPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.CustomerServiceFindOrderWidget;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;


/** 
 * Test scenario to test various use cases associated with Dropdown menu context
 * Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FSTOREB2BCSR_14 extends AbstractAuroraSingleSessionTests
{
	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	private boolean doTearDown = true;
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	private final CaslFixturesFactory f_CaslFixtures;

	

	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_CaslFixtures 
	 */	@Inject
	public FSTOREB2BCSR_14(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_CaslFixtures, CaslKeysFactory p_caslKeysFactory)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		f_CaslFixtures = p_CaslFixtures;
		
	}

	

	 
	/** 
	 * Buyer admin can not see the order comments slider
	 */
	@Test
	public void testFSTOREB2BCSR_1409()
	{

		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
	
		//Log in as a registered shopper 
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		signInPage.typeUsername(dsm.getInputParameter("BUYER_ADMIN_LOGONID"))
		.typePassword(dsm.getInputParameter("BUYER_ADMIN_PASSWORD")).signIn();
		
		//Complete a full shopping flow 
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("BUYER_ADMIN_LOGONID"), dsm.getInputParameter("BUYER_ADMIN_PASSWORD"), getConfig().getStoreName());
		orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
		
		//Get the order number of the shopping flow just completed 
		String orderId = orders.getCurrentOrderId();
		 
		//click on Shopping Cart link from the header
		
		ShopCartPage shopCart = frontPage.getHeaderWidget().goToShoppingCartPage();
		
		//Go to shipping and billing page
		ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
		
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT"));
					
		//Confirm the correct Shipping Address is selected
		shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
			.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
		
		//Confirm the correct Billing address is selected
		shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
			.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
					
		//select Pay later as the payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAYMENT_METHOD"));
						
		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
		
		//click Order button and verify Order is successful
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
		
		orderConfirmation.verifyOrderSuccessful();
						
		//shoppers accesses my account page
		MyAccountMainPage accountMainPage = frontPage.getHeaderWidget().goToMyAccountQuickLink();
		OrderDetailPage orderDetailPage = accountMainPage.goToOrderDetailPageByOrderId(orderId);
		orderDetailPage.verifyOrderNumberPresent(orderId);
			
		//order comments section should NOT be visible 
		try{
		orderDetailPage.getCommentsBar().slideOrderComments();
		}catch(TimeoutException e)	{
			System.out.println("OrderCommentsSlideBarWidget should not be available.");
		}
		
		
		try{
		orderDetailPage.getHeaderWidget().goToShoppingCartPage()
			.getCommentsBar().slideOrderComments();
			
		}catch(TimeoutException e){
			System.out.println("OrderCommentsSlideBarWidget should not be available.");
		}
		
	}
	
	/** 
	 * CSR can not see the order summary page while on behalf of shopper
	 */
	
	@Test
	public void testFSTOREB2BCSR_1414()
	{
		
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
	
		//Log in as a registered shopper 
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		signInPage.typeUsername(dsm.getInputParameter("BUYER_ADMIN_LOGONID"))
		.typePassword(dsm.getInputParameter("BUYER_ADMIN_PASSWORD")).signIn();
		
		//Complete a full shopping flow 
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("BUYER_ADMIN_LOGONID"), dsm.getInputParameter("BUYER_ADMIN_PASSWORD"), getConfig().getStoreName());
		orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
		
		//Get the order number of the shopping flow just completed 
		String orderId = orders.getCurrentOrderId();
		 
		//click on Shopping Cart link from the header
		
		ShopCartPage shopCart = frontPage.getHeaderWidget().goToShoppingCartPage();
		
		//Go to shipping and billing page
		ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
		
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT"));
					
		//Confirm the correct Shipping Address is selected
		shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
			.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
		
		//Confirm the correct Billing address is selected
		shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
			.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
					
		//select Pay later as the payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAYMENT_METHOD"));
						
		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
		
		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
		orderConfirmation.verifyOrderSuccessful();
		
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage2 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
				
		//Log in as a Customer Service Representative
		SignInDropdownWidget  signInPage2 = frontPage2.getHeaderWidget().signIn();
		signInPage2.typeUsername(dsm.getInputParameter("CSR_LOGONID"))
		.typePassword(dsm.getInputParameter("CSR_PASSWORD")).signIn();
					
		//use find order widget to search for order and access customer account 
		CustomerServiceFindOrderWidget findOrderWidget = frontPage2.getHeaderWidget().goToCustomerService().getFindOrderWidget();
		MyAccountMainPage myAccountMainPage = findOrderWidget.typeOrderNumber(orderId)
			.submitSearch().clickActionButton(orderId).clickAccessCustomerAccount();
					
		//go to order details page while accessing customer's account via order history
		OrderDetailPage orderDetailPage = myAccountMainPage.goToOrderHistoryToViewAllOrders()
				.goToOrderDetailPageWithOrderId(orderId);
		
		//check if order comments slider is visible on the order details page
		orderDetailPage.getCommentsBar().slideOrderComments();
		orderDetailPage.isSliderCommentTextAreaVisible();
		
	
					
	}
	 /**
	 * Tear down ran after every test case
	 */
	@After
	public void tearDown(){
		try
		{
			if (doTearDown) 
			{
				//Continue browser session starting at the home page.
				HeaderWidget headerSection = getSession().continueToPage(getConfig().getStoreUrl(), HeaderWidget.class);
	
				//Remove all items from the shopping cart
				headerSection.goToShoppingCartPage().clickLockOrder().removeAllItems().clickUnlockOrder()
				.getHeaderWidget().openSignOutDropDownWidget().signOut();
			
			
			}
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

	}
			 
	
}
