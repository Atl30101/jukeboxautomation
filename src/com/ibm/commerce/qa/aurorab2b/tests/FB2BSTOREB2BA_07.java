package com.ibm.commerce.qa.aurorab2b.tests;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.OrderSummaryMultipleShipPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.MyAccountSidebarWidget;
import com.ibm.commerce.qa.aurora.widget.OrganizationBuyerListWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AddBuyerPage;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.OrderApprovalDetailsPage;
import com.ibm.commerce.qa.aurorab2b.page.OrderApprovalPage;
import com.ibm.commerce.qa.aurorab2b.page.OrganizationsAndBuyersPage;
import com.ibm.commerce.qa.aurorab2b.page.ProductDisplayPageB2B;
import com.ibm.commerce.qa.casl.util.CaslModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.OrgAdminConsole;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
/**
 * Scenario: FSSC-92726-07
 * Details: Manage order approvals as a buyer admin user
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class, CaslModule.class})
public class FB2BSTOREB2BA_07 extends AbstractAuroraSingleSessionTests {


	//TODO: it logs in as buyerAadmin, but this user needs to be reset password as pre-req. 
	// @Shakira, better automate this pre-req
	
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider f_dsm;

	/**
	 * Test Class object constructor.
	 * 
	 */
	@Inject
	public FB2BSTOREB2BA_07(
			Logger log, 
			WcWteTestRule wcWteTestRule,
			CaslFoundationTestRule caslTestRule,
			TestDataProvider p_dsm,
			OrgAdminConsole p_oac) throws Exception
	{
		super(log, wcWteTestRule, caslTestRule);

		f_dsm = p_dsm;
	}

	/**
	 * Helper method to create new buyer with buyer approver role.
	 * 
	 * @param logonid
	 * 			username of the new buyer
	 * @throws Exception
	 */
	public void addBuyerApprover(String logonid) throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel
				
				.typeUsername(f_dsm.getInputParameter("ADMIN_USERNAME"))

				//Enter a valid password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				//Click Sign In
				.signInWithoutVerify(HeaderWidget.class);

		//go to My Account page
		MyAccountMainPage myAccountPage = header.goToMyAccount();

		MyAccountSidebarWidget myAccountSidebar = myAccountPage.getSidebar();

		//Go to organization and buyer page from my account side bar.
		OrganizationsAndBuyersPage orgAndBuyersPage = myAccountSidebar.goToOrganizationsAndBuyersPage();

		OrganizationBuyerListWidget orgBuyerListWidget = orgAndBuyersPage.getOrganizationBuyerListWidget();

		//Click add buyer from organization and buyer page 
		AddBuyerPage addBuyerPage = orgBuyerListWidget.goToAddBuyerPage();

		//Enter buyer details
		addBuyerPage.getBuyerDetailsWidget().typeLogonId(logonid)

				.typeFirstName(f_dsm.getInputParameter("FIRST_NAME"))
		
				.typeLastName(f_dsm.getInputParameter("LAST_NAME"))
		
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
		
				.typeVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))
		
				.typeEmail(f_dsm.getInputParameter("EMAIL"));

		//Assign role to buyer
		addBuyerPage.getUserRoleManagementWidget()
		
				.selectExpandUserRoleMenuToggle()
				
				.selectRole(f_dsm.getInputParameter("ROLE"));

		//Click submit button
		addBuyerPage.submit();
	}
	
	/**
	 * Helper method to get shipping date value
	 * 
	 * @param daysFromToday
	 * 		number of days to add to the current date
	 * @return
	 * 		shipping date
	 */
	public String getShippingDateFromToday(int daysFromToday)
	{
		Calendar calender = Calendar.getInstance();
		calender.add(Calendar.DAY_OF_WEEK, daysFromToday);
		
		String timeStamp = new SimpleDateFormat("MM/dd/yy").format(calender.getTime());
		
		return timeStamp;
	}

	/**
	 * Buyer admin approves a pending order approval request.
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_07_07() throws Exception 
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel.typeUsername(f_dsm.getInputParameter("ADMIN_USERNAME"))

				//Enter a valid password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				//Click Sign In
				.signInWithoutVerify(HeaderWidget.class);

		//Select contract from signIn drop down widget
		signInPanel.selectContractByName(f_dsm.getInputParameter("CONTRACTNAME"));

		//Go to my account page from header
		MyAccountMainPage myAccountPage	= header.goToMyAccount();

		//Go to department page	
		CategoryPage subCat = myAccountPage.getHeaderWidget()
				.goToCategoryPageByHierarchy(CategoryPage.class, f_dsm.getInputParameter("DEPARTMENT"), f_dsm.getInputParameter("CATEGORY"));
		
		//click on product image on Category Display page.
		ProductDisplayPageB2B productDisplay = subCat.getCatalogEntryListWidget()
				.goToProductPageByNameB2B(f_dsm.getInputParameter("PRODUCT_NAME"));

		//Select the product attributes			
		productDisplay.getDefiningAttributesWidget()
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR1_NAME"), f_dsm.getInputParameter("ATTR1_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR2_NAME"), f_dsm.getInputParameter("ATTR2_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR3_NAME"), f_dsm.getInputParameter("ATTR3_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR4_NAME"), f_dsm.getInputParameter("ATTR4_VALUE"));
		
		//Change quantity of sku
		productDisplay.getSkuListWidget()
		
				.updateQuantity(f_dsm.getInputParameter("PRODUCT_SKU"), f_dsm.getInputParameter("ITEM_QTY"));
		
		//click Add to Current Order from Product Display page.
		productDisplay.addToCurrentOrder(f_dsm.getInputParameter("PRODUCT_SKU"));

		//Go to shopping cart page and continue to shipping and billing page.
		ShippingAndBillingPage shippingAndBilling = header.openShopCartWidget()
				
				.goToShoppingCartPage()
				
				.continueToNextStep();

		//Select the order payment method type and enter the purchase order number.
		OrderSummarySingleShipPage orderSummary = shippingAndBilling
				
				.selectPaymentMethod(f_dsm.getInputParameter("BILLING_METHOD"))

				.enterPoNumber(String.valueOf(System.currentTimeMillis()))

				.singleShipNext();

		String orderId = orderSummary.completeOrder().getOrderNumber();

		//go to My Account page.
		header.goToMyAccount();

		//Go to order approval page from my account side bar.
		OrderApprovalPage orderApprovalPage = myAccountPage.getSidebar()
				
				.goToOrderApprovalPage();

		//Approve the order.
		orderApprovalPage.getOrderApprovalWidget()
		
				.openActionListByOrderId(orderId)
				
				.approveOrderByOrderId(orderId);
		
		//Verify the order no longer exists on the order approval page.
		orderApprovalPage.getOrderApprovalWidget().verifyOrderIdNotPresent(orderId);
	}

	/**
	 * A buyer with the role of Buyer Approver can approve order approvals
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_07_11() throws Exception
	{
		String buyerApprover = f_dsm.getInputParameter("APPROVER_USERNAME") + System.currentTimeMillis();

		addBuyerApprover(buyerApprover);

		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel.typeUsername(buyerApprover)

				//Enter a valid password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				//Click Sign In
				.signInWithoutVerify(HeaderWidget.class);

		signInPanel.selectContractByName(f_dsm.getInputParameter("CONTRACTNAME"));

		//Go to my account page.
		MyAccountMainPage myAccountPage	= header.goToMyAccount();

		//Go to department page	
		CategoryPage subCat = myAccountPage.getHeaderWidget()
				.goToCategoryPageByHierarchy(CategoryPage.class, f_dsm.getInputParameter("DEPARTMENT"),
						f_dsm.getInputParameter("CATEGORY"));
		
		//click on product image on Category Display page.
		ProductDisplayPageB2B productDisplay = subCat.getCatalogEntryListWidget()
				.goToProductPageByNameB2B(f_dsm.getInputParameter("PRODUCT_NAME"));

		//Select the product attributes			
		productDisplay.getDefiningAttributesWidget()
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR1_NAME"), f_dsm.getInputParameter("ATTR1_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR2_NAME"), f_dsm.getInputParameter("ATTR2_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR3_NAME"), f_dsm.getInputParameter("ATTR3_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR4_NAME"), f_dsm.getInputParameter("ATTR4_VALUE"));
		
		//Change quantity of sku
		productDisplay.getSkuListWidget()
		
				.updateQuantity(f_dsm.getInputParameter("PRODUCT_SKU"), f_dsm.getInputParameter("ITEM_QTY"));
		
		//click Add to Current Order from Product Display page.
		productDisplay.addToCurrentOrder(f_dsm.getInputParameter("PRODUCT_SKU"));

		//Go to shopping cart page and continue to shipping and billing page.
		ShippingAndBillingPage shippingAndBilling = header.openShopCartWidget()
				
				.goToShoppingCartPage()
				
				.continueToNextStep();

		//Select the order payment method type and enter the purchase order number.
		OrderSummarySingleShipPage orderSummary = shippingAndBilling.selectPaymentMethod(f_dsm.getInputParameter("BILLING_METHOD"))

				.enterPoNumber(String.valueOf(System.currentTimeMillis()))

				.singleShipNext();

		String orderId = orderSummary.completeOrder().getOrderNumber();

		//go to My Account page
		header.goToMyAccount();

		//Go to order approval page from my account side bar.
		OrderApprovalPage orderApprovalPage = myAccountPage.getSidebar()
				
				.goToOrderApprovalPage();

		//Approve the order
		orderApprovalPage.getOrderApprovalWidget()
		
				.openActionListByOrderId(orderId)
				
				.approveOrderByOrderId(orderId);
		
		//Verify the order no longer exists on the order approval page.
		orderApprovalPage.getOrderApprovalWidget().verifyOrderIdNotPresent(orderId);
	}

	/**
	 * A buyer with the role of Buyer Approver can reject order approvals.
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_07_12() throws Exception
	{
		String buyerApprover = f_dsm.getInputParameter("APPROVER_USERNAME") + System.currentTimeMillis();

		addBuyerApprover(buyerApprover);

		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel.typeUsername(buyerApprover)

				//Enter a valid password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				//Click Sign In
				.signInWithoutVerify(HeaderWidget.class);

		//Select contract from signIn drop down widget
		signInPanel.selectContractByName(f_dsm.getInputParameter("CONTRACTNAME"));

		//Go to my account page.
		MyAccountMainPage myAccountPage	= header.goToMyAccount();

		//Go to department page	
		CategoryPage subCat = myAccountPage.getHeaderWidget()
				.goToCategoryPageByHierarchy(CategoryPage.class, f_dsm.getInputParameter("DEPARTMENT"), f_dsm.getInputParameter("CATEGORY"));
		
		//click on product image on Category Display page.
		ProductDisplayPageB2B productDisplay = subCat.getCatalogEntryListWidget()
				.goToProductPageByNameB2B(f_dsm.getInputParameter("PRODUCT_NAME"));

		//Select the product attributes			
		productDisplay.getDefiningAttributesWidget()
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR1_NAME"), f_dsm.getInputParameter("ATTR1_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR2_NAME"), f_dsm.getInputParameter("ATTR2_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR3_NAME"), f_dsm.getInputParameter("ATTR3_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR4_NAME"), f_dsm.getInputParameter("ATTR4_VALUE"));
		
		//Change quantity of sku
		productDisplay.getSkuListWidget()
		
				.updateQuantity(f_dsm.getInputParameter("PRODUCT_SKU"), f_dsm.getInputParameter("ITEM_QTY"));
		
		//click Add to Current Order from Product Display page.
		productDisplay.addToCurrentOrder(f_dsm.getInputParameter("PRODUCT_SKU"));

		//Go to shopping cart page and continue to shipping and billing page.
		ShippingAndBillingPage shippingAndBilling = header.openShopCartWidget()
				
				.goToShoppingCartPage()
				
				.continueToNextStep();

		//Select the order payment method type and enter the purchase order number.
		OrderSummarySingleShipPage orderSummary = shippingAndBilling.selectPaymentMethod(f_dsm.getInputParameter("BILLING_METHOD"))

				.enterPoNumber(String.valueOf(System.currentTimeMillis()))

				.singleShipNext();

		String orderId = orderSummary.completeOrder().getOrderNumber();

		//go to My Account page
		header.goToMyAccount();

		//Go to order approval page from my account side bar
		OrderApprovalPage orderApprovalPage = myAccountPage.getSidebar()
				
				.goToOrderApprovalPage();

		//Reject the order
		orderApprovalPage.getOrderApprovalWidget()
		
				.openActionListByOrderId(orderId)
				
				.rejectOrderByOrderId(orderId)
				
				//Verify record reject message
				.verifyErrorMessage(f_dsm.getInputParameter("MESSAGE"));
				
		//Verify the order no longer exists on the order approval page.
		orderApprovalPage.getOrderApprovalWidget()
		
				.verifyOrderIdNotPresent(orderId);
	}
	
	/**
	 * View order approval details when the order has multiple shipments.
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_07_17() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel
				
				.typeUsername(f_dsm.getInputParameter("ADMIN_USERNAME"))

				//Enter a valid password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				//Click Sign In
				.signInWithoutVerify(HeaderWidget.class);

		//Select contract from signIn drop down widget
		signInPanel.selectContractByName(f_dsm.getInputParameter("CONTRACTNAME"));

		//Go to my account page.
		MyAccountMainPage myAccountPage	= header.goToMyAccount();

		//Go to department page	
		CategoryPage subCat = myAccountPage.getHeaderWidget()
				.goToCategoryPageByHierarchy(CategoryPage.class, f_dsm.getInputParameter("DEPARTMENT"), f_dsm.getInputParameter("CATEGORY"));
		
		//click on product image on Category Display page.
		ProductDisplayPageB2B productDisplay = subCat.getCatalogEntryListWidget()
				.goToProductPageByNameB2B(f_dsm.getInputParameter("PRODUCT_NAME"));

		//Select the product attributes			
		productDisplay.getDefiningAttributesWidget()
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR1_NAME"), f_dsm.getInputParameter("ATTR1_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR2_NAME"), f_dsm.getInputParameter("ATTR2_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR3_NAME"), f_dsm.getInputParameter("ATTR3_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR4_NAME"), f_dsm.getInputParameter("ATTR4_VALUE"));
		
		//Change quantity of sku
		productDisplay.getSkuListWidget()
		
				.updateQuantity(f_dsm.getInputParameter("PRODUCT_SKU"), f_dsm.getInputParameter("ITEM_QTY"));
		
		//click Add to Current Order from Product Display page.
		productDisplay.addToCurrentOrder(f_dsm.getInputParameter("PRODUCT_SKU"));

		//Change quantity of sku
		productDisplay.getSkuListWidget()
		
				.updateQuantity(f_dsm.getInputParameter("PRODUCT_SKU"), f_dsm.getInputParameter("ITEM_QTY"));
		
		//click Add to Current Order from Product Display page.
		productDisplay.addToCurrentOrder(f_dsm.getInputParameter("PRODUCT_SKU"));		
		
		//Go to shopping cart page and continue to shipping and billing page.
		ShippingAndBillingPage shippingAndBilling = header.openShopCartWidget()
				
				.goToShoppingCartPage()
				
				.continueToNextStep();

		String shippingDate1 = getShippingDateFromToday(5);
		String shippingDate2 = getShippingDateFromToday(10);
		
		//Enter multiple shipping page details
		shippingAndBilling.chooseMultipleShipping()
		
				.openOrderItemSection()
				
				.getMultipleshipment()
				
				//Enter the shipping date for the first item
				.addShippingDateByPosition(shippingDate1, "1")
				
				.getMultipleshipment()
				
				//Enter the shipping date for the second item
				.addShippingDateByPosition(shippingDate2, "2")
				
				.selectPaymentMethod(f_dsm.getInputParameter("BILLING_METHOD"))
				
				//Enter the purchase order number
				.enterPoNumber(String.valueOf(System.currentTimeMillis()));
		
		//Continue to order summary page
		OrderSummaryMultipleShipPage orderSummary = shippingAndBilling.multipleShipNext();

		String orderId = orderSummary.completeOrder().getOrderNumber();

		//go to My Account page
		header.goToMyAccount();

		//Go to order approval page from my account side bar
		OrderApprovalPage orderApprovalPage = myAccountPage.getSidebar()
				
				.goToOrderApprovalPage();

		//Go to order approval details page
		OrderApprovalDetailsPage orderApprovalDetailsPage = orderApprovalPage.getOrderApprovalWidget()
		
				.openActionListByOrderId(orderId)
				
				.goToOrderDetailPage(orderId);
		
		//Verify the order details on the order approval details page
		orderApprovalDetailsPage
				
				.getOrderApprovalItemTableWidget()
				
				//Verify the first items shipping date
				.verifyShippingDate("1", shippingDate1)
				
				//Verify the second items shipping date
				.verifyShippingDate("2", shippingDate2);
	}
	
	/**
	 * View order details when the order has multiple payments.
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_07_19() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel.typeUsername(f_dsm.getInputParameter("ADMIN_USERNAME"))

				//Enter a valid password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				//Click Sign In
				.signInWithoutVerify(HeaderWidget.class);

		signInPanel.selectContractByName(f_dsm.getInputParameter("CONTRACTNAME"));

		//Go to my account page.
		MyAccountMainPage myAccountPage	= header.goToMyAccount();

		//Go to department page	
		CategoryPage subCat = myAccountPage.getHeaderWidget()
				.goToCategoryPageByHierarchy(CategoryPage.class, f_dsm.getInputParameter("DEPARTMENT"), f_dsm.getInputParameter("CATEGORY"));
		
		//click on product image on Category Display page.
		ProductDisplayPageB2B productDisplay = subCat.getCatalogEntryListWidget()
				.goToProductPageByNameB2B(f_dsm.getInputParameter("PRODUCT_NAME"));

		//Select the product attributes			
		productDisplay.getDefiningAttributesWidget()
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR1_NAME"), f_dsm.getInputParameter("ATTR1_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR2_NAME"), f_dsm.getInputParameter("ATTR2_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR3_NAME"), f_dsm.getInputParameter("ATTR3_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR4_NAME"), f_dsm.getInputParameter("ATTR4_VALUE"));
		
		//Change quantity of sku
		productDisplay.getSkuListWidget()
		
				.updateQuantity(f_dsm.getInputParameter("PRODUCT_SKU"), f_dsm.getInputParameter("ITEM_QTY"));
		
		//click Add to Current Order from Product Display page.
		productDisplay.addToCurrentOrder(f_dsm.getInputParameter("PRODUCT_SKU"));
		
		//Go to shopping cart page and continue to shipping and billing page.
		ShippingAndBillingPage shippingAndBilling = header.openShopCartWidget()
				
				.goToShoppingCartPage()
				
				.continueToNextStep();

		//Enter order shipping details for multiple payments
		OrderSummarySingleShipPage orderSummary = shippingAndBilling
				
				//Select multiple payments from drop down
				.selectNoOfPaymentMethods(f_dsm.getInputParameter("NUMBER_OF_PAYMENT_METHODS"))

				.getMultiplepayment()

				//Enter first billing method
				.selectBillingMethodByPaymentsPosition(f_dsm.getInputParameter("BILLING_METHOD1"), "1")
				
				.getMultiplepayment()
				
				//Enter first billing method
				.selectBillingMethodByPaymentsPosition(f_dsm.getInputParameter("BILLING_METHOD2"), "2")
				
				//Enter purchase order number
				.enterPoNumber(String.valueOf(System.currentTimeMillis()))

				//Continue to order summary page
				.singleShipNext();

		String orderId = orderSummary.completeOrder().getOrderNumber();

		//go to My Account page
		header.goToMyAccount();

		//Go to order approval page from my account side bar
		OrderApprovalPage orderApprovalPage = myAccountPage.getSidebar()
				
				.goToOrderApprovalPage();

		//Go to order approval detail page
		OrderApprovalDetailsPage orderApprovalDetailsPage = orderApprovalPage.getOrderApprovalWidget()
		
				.openActionListByOrderId(orderId)
				
				.goToOrderDetailPage(orderId);
				
		//Verify the number of payment methods is correct from order approval details page
		orderApprovalDetailsPage.getOrderApprovalBillingWidget()
				
				.verifyNumberOfPaymentMethods("2");
				
	}

	@After
	public void tearDown() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel.typeUsername(f_dsm.getInputParameter("ADMIN_USERNAME"))

				//Enter a valid password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				//Click Sign In
				.signInWithoutVerify(HeaderWidget.class);
		
		header.openShopCartWidget()
		
				.goToShoppingCartPage()
		
				.removeAllItems();
	}

}