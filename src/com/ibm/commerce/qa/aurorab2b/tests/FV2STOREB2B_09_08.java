package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.CustomerRegisterationPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.ProductDisplayPageB2B;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.Accelerator;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.url.OrgAdminConsole;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 * Scenario: FV2STOREB2B_11
 * Details: Test View Order Status
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2B_09_08 extends AbstractAuroraSingleSessionTests
{

    /**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	/**A variable to hold the name of the data file where input parameters can be found.**/
	//protected final String dataFileName = "data/FSTOREELITE_09_Data.xml";
	private String [] shippingModes = {};
	private String [] shippingCharges = {};
	private String [] contract1 = {};
	private String [] contract2 = {};
	private String accountId;
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	
	private OrgAdminConsole oac;
	
	private Accelerator accelerator;
	
	private CMC cmc;
	
	public String catFilterId;
	
	public boolean merchAssocCleanup = false;
	
	/**A variable to hold merchandise association identifier created**/
	private String associationId;
	
	/**A variable to hold the catalog entry identifier**/
	private String catalogEntryId;
	
	/**A variable to hold the catalog entry identifier**/
	private String assocCatalogEntryId;
	
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_CaslFixtures 
	 */	@Inject
	public FV2STOREB2B_09_08(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,	
			Accelerator accelerator,
			OrgAdminConsole oac,
			CMC cmc)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		this.accelerator = accelerator;
		this.oac = oac;
		this.cmc = cmc;
		
	}
	 private void registerUserAndOrg(){ 
			//Open Auroraesite store
			AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

			//Click on the SignIn page link on the header
			SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();
			
			//attempt to sign in:
			try {
				signInPage.typeUsername(dsm.getInputParameter("LOGONID"))
					.typePassword(dsm.getInputParameter("PASSWORD"))
					.signIn();
				
				//Open Auroraesite store
				frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

				
			} catch (Exception e1) {
				//Open Auroraesite store
				

				//Click on the registration button on the SignIn page
				CustomerRegisterationPageB2B rp = signInPage.registerB2B();

				rp.selectOrganizationRegister()
				
				//type organization name
				.typeOrganizationName(dsm.getInputParameter("ORGANIZATION_NAME"))
				
				//type organization address
				.typeOrganizationStreetAddressLine1(dsm.getInputParameter("ORGANIZATION_ADDRESS"))
				
				//select organization country
				.selectOrganizationCountryOrRegion(dsm.getInputParameter("ORGANIZATION_COUNTRY"))
				
				//select organization province
				.selectOrganizationStateOrProvince(dsm.getInputParameter("ORGANIZATION_STATE"))
				
				//type organization city
				.typeOrganizationCity(dsm.getInputParameter("ORGANIZATION_CITY"))
				
				//type organization zipcode
				.typeOrganizationZipCode(dsm.getInputParameter("ORGANIZATION_ZIPCODE"))
				
				//type organization email
				.typeOrganizationEmail(dsm.getInputParameter("ORGANIZATION_EMAIL"))
				
				//type organization phone number
				.typeOrganizationPhoneNumber(dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))

				//type buyer username
				.typeOrganizationBuyerLogonId(dsm.getInputParameter("LOGONID"))

				//type buyer password
				.typeOrganizationBuyerPassword(dsm.getInputParameter("PASSWORD"))

				//Verify buyer password verify
				.typeOrganizationBuyerVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))

				//type buyer first name
				.typeOrganizationBuyerFirstName(dsm.getInputParameter("FIRST_NAME"))

				//type buyer last name
				.typeOrganizationBuyerLastName(dsm.getInputParameter("LAST_NAME"))

				//type buyer street address
				.typeOrganizationBuyerStreetAddressLine1(dsm.getInputParameter("ADDRESS"))

				//Select buyer country
				.selectOrganizationBuyerCountryOrRegion(dsm.getInputParameter("COUNTRY"))

				//type or select state for buyer
				.selectOrganizationBuyerStateOrProvince(dsm.getInputParameter("STATE"))

				//type buyer city
				.typeOrganizationBuyerCity(dsm.getInputParameter("CITY"))

				//type buyer zipcode
				.typeOrganizationBuyerZipCode(dsm.getInputParameter("ZIPCODE"))

				//type buyer E-mail
				.typeOrganizationBuyerEmail(dsm.getInputParameter("EMAIL"))

				//type buyer home phone number
				.typeOrganizationBuyerPhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))

				//Check if preferred language drop down is visible
				.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			

				//Select buyer preferred language
				.selectOrganizationBuyerPreferedCurrency(dsm.getInputParameter("PREFERRED_CURRENCY"))

				//Check if preferred currency drop down is visible
				.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()

				//Select buyer preferred language
				.selectOrganizationBuyerPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))

				//Submit registration
				.submitOrganizationBuyerRegistration()
				
				//verify registration confirmation message
				.verifyOrganizationBuyerRegistrationConfirmationMessage();
				
				// Approve Organization
				oac.logon(dsm.getInputParameter("ADMIN_LOGON_ID"), dsm.getInputParameter("ADMIN_PASSWORD"));
				oac.approveAllApprovals();
				
			}
		}
	 @Before
	 public void setup() throws Exception {
		 
		registerUserAndOrg();
		 
		accelerator.logon(dsm.getInputParameter("ACCELERATOR_LOGON_ID"),dsm.getInputParameter("ACCELERATOR_PASSWORD"));
		accountId = accelerator.createNewAccount(dsm.getInputParameter("ORG"), null, null, false);
		contract1 = createContract(dsm.getInputParameter("ORG"));
		accelerator.logoff();
		
		//create catalog filter
		dsm.setDataLocation("setup", "setup");
		cmc.logon(dsm.getInputParameter("CMC_LOGON_ID"),dsm.getInputParameter("CMC_PASSWORD"));
		cmc.selectStore();
		cmc.selectCatalog();
		
		try {
			catFilterId = cmc.getCatalogFilterId(dsm.getInputParameter("CAT_FILTER_NAME"));
		} catch (Exception e) {
			catFilterId = cmc.createCatalogFilter(dsm.getInputParameter("CAT_FILTER_NAME"));
			cmc.createCatalogFilterCategoryElement(catFilterId, dsm.getInputParameter("CAT_FILTER_EXCULDE_CATEGORY"), dsm.getInputParameter("CAT_FILTER_SELECTION"));
			
		}
		
		cmc.logoff();
//		
		//create account and contract
		accelerator.logon(dsm.getInputParameter("ACCELERATOR_LOGON_ID"),dsm.getInputParameter("ACCELERATOR_PASSWORD"));
		contract2 = createContract(dsm.getInputParameter("ORG"));
		accelerator.logoff();	
		
	 }
		 
	 /** 
	 * A merchandising association is not shown if the products are excluded in the contract
	 * @throws Exception 
	 */
		
		@Test
		public void testFV2STOREB2B_0908() throws Exception
		{	
		
			//Open the store in the browser
			AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	
			
			//login to CMC
			dsm.setDataLocation("testFV2STOREB2B_0908", "CMClogon");
			cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
			
			//select store and catalog
			cmc.setLocale(dsm.getInputParameter("locale"));
			cmc.selectStore();
			cmc.selectCatalog();
			
			//get the catentryId of the product
			dsm.setDataLocation("testFV2STOREB2B_0908", "createMerchandiseAssociate");
			catalogEntryId = cmc.getCatalogEntryId(dsm.getInputParameter("PRODUCT_1"));
			assocCatalogEntryId = cmc.getCatalogEntryId(dsm.getInputParameter("PRODUCT_2"));

			//create Merchandising Association for the given product 
			associationId = cmc.createMerchandisingAssociation(catalogEntryId, assocCatalogEntryId,dsm.getInputParameter("name"),dsm.getInputParameter("quantity"),null);
			
			//log off from CMC
			cmc.logoff();
			
			merchAssocCleanup = true;
				
			//Tell which test case to use for input parameters
			dsm.setDataLocation("testFV2STOREB2B_0908","testFV2STOREB2B_0908");
			
			HeaderWidget header = frontPage.getHeaderWidget();
			//Login with valid user id and password
			SignInDropdownWidget  signIn = header.signIn();
			signIn.typeUsername(dsm.getInputParameter("USER_NAME")).typePassword(dsm.getInputParameter("USER_PASSWORD")).signInWithoutVerify(SignInDropdownWidget.class);
			
			// Verify Merchandising Association is not Present
			signIn.selectContractByName(contract2[1]);
		
			//Click the Top category in menu
			CategoryPage subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CATEGORY_NAME"),dsm.getInputParameter("SUB_CATEGORY_NAME"));
			
			//click on the product image
			ProductDisplayPageB2B prodPage = subCat.getCatalogEntryListWidget().goToProductPageByNameB2B(dsm.getInputParameter("PRODUCT_1"));
		
			prodPage.verifyMerchandisingAssociationWidgetNotPresent();
			
			// Verify Merchandising Association is not Present
			header.openSignOutDropDownWidget().selectContractByName(contract1[1]);
			
			//Click the Top category in menu
			subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CATEGORY_NAME"),dsm.getInputParameter("SUB_CATEGORY_NAME"));
						
			//click on the product image
			prodPage = subCat.getCatalogEntryListWidget().goToProductPageByNameB2B(dsm.getInputParameter("PRODUCT_1"));
					
			prodPage.verifyMerchandisingAssociationWidgetPresent();
			
			prodPage.getMerchandisingAssociationsWidget().verifyCatentryNamesShown(dsm.getInputParameter("PRODUCT_2"));
			
			prodPage.getHeaderWidget().openSignOutDropDownWidget().signOutB2B();
		}
		
		
			private String [] createContract(String org) throws Exception
			{
				String [] tempcontract =  new String[2];
				
				//Tell which test case to use for input parameters
				dsm.setDataLocation("setup", "createContract");
				
				String [] percentOff = {dsm.getInputParameter("percentOff1_1"),dsm.getInputParameter("percentOff1_2")};
				String [] requiredCatalogs = {dsm.getInputParameter("requiredCatalogs1_1"),dsm.getInputParameter("requiredCatalogs1_2")};
				
				//gets a new unique contract name.
				String contractName = RandomStringUtils.randomAlphanumeric(8);
				
				//gets all available shipping modes.
				shippingModes = accelerator.getShippingModes();
				
				//gets all available shipping charges.
				shippingCharges = accelerator.getShippingCharges();
				
				//creates new contract for the account created in previous steps.
				String tempContractId = accelerator.createNewContract(contractName, org,dsm.getInputParameter("INCLUDE_ENTIRE_CATALOG"),dsm.getInputParameter("PERCENTAGE_OFF_ON_ENTIRE_CATALOG"), requiredCatalogs, percentOff, shippingModes, shippingCharges);				
			    
				//submit the contract;
				accelerator.submitContract(accountId, tempContractId);
				
				tempcontract[0] = tempContractId;
				tempcontract[1] = contractName;
				
				return tempcontract;
						
			}
			
			
		@After
		public void tearDown() throws Exception {
			accelerator.logon(dsm.getInputParameter("ACCELERATOR_LOGON_ID"),dsm.getInputParameter("ACCELERATOR_PASSWORD"));
			
			
			accelerator.cancelContract(accountId, contract1[0]);
			accelerator.deleteContract(accountId, contract1[0]);
			accelerator.cancelContract(accountId, contract2[0]);
			accelerator.deleteContract(accountId, contract2[0]);
			
			accelerator.deleteAccount(accountId);
			
			cmc.logon(dsm.getInputParameter("CMC_LOGON_ID"),dsm.getInputParameter("CMC_PASSWORD"));
			cmc.selectStore();
			cmc.selectCatalog();
			cmc.deleteCatalogFilter(catFilterId);
			cmc.logoff();
			
			if(merchAssocCleanup){
				//login to CMC
				dsm.setDataLocation("testFV2STOREB2B_0908", "CMClogon");
				cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
				
				//select store and catalog
				cmc.selectStore();
				cmc.selectCatalog();
				
				cmc.deleteMerchandisingAssociation(associationId,catalogEntryId,assocCatalogEntryId);
				cmc.removeParameterInGlobalMap("catentryId");
				cmc.removeParameterInGlobalMap("assocCatentryId");
				//log off from CMC
				cmc.logoff();
				
				merchAssocCleanup=false;
				
			}
			
		}
			
			



	
			
	
	
}
