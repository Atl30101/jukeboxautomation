package com.ibm.commerce.qa.aurorab2b.tests;

import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AddBuyerPage;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.BuyerDetailsPage;
import com.ibm.commerce.qa.aurorab2b.page.CustomerRegisterationPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.OrganizationsAndBuyersPage;
import com.ibm.commerce.qa.aurorab2b.page.SignInPageB2B;
import com.ibm.commerce.qa.casl.util.CaslModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.OrgAdminConsole;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.wte.framework.test.WebSession;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
/**
 * Scenario: FSSC-92726-05
 * Details: Manage buyers as a buyer admin user
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class, CaslModule.class})
public class FB2BSTOREB2BA_05 extends AbstractAuroraSingleSessionTests {


	private final OrgAdminConsole oac;
	
	
	
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider f_dsm;

	private static String organizationName;

	private static String adminUsername;
	
	private static String buyerUsername;
	
	private static boolean setupCompleted = false;
	
	/**
	 * Test Class object constructor.
	 * 
	 */
	@Inject
	public FB2BSTOREB2BA_05(
			Logger log, 
			WcWteTestRule wcWteTestRule,
			CaslFoundationTestRule caslTestRule,
			TestDataProvider p_dsm,
			OrgAdminConsole p_oac,
			WebSession p_webSession) throws Exception
	{
		super(log, wcWteTestRule, caslTestRule);
		
		f_dsm = p_dsm;
		oac = p_oac;
	}
	
	@Before
	public void setup() throws Exception
	{
		if(!setupCompleted)
		{
			//Open AuroraB2BEsite store
			AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	
	
			//Click on the SignIn page link on the header
			SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();
	
			//Click on the registration button on the SignIn page
			CustomerRegisterationPageB2B rp = signInPanel.registerB2B();
	
			adminUsername = f_dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID") + System.currentTimeMillis();
	
			organizationName = f_dsm.getInputParameter("ORGANIZATION_NAME") + System.currentTimeMillis();
	
			buyerUsername = f_dsm.getInputParameter("BUYER_LOGONID") + System.currentTimeMillis();
			
			SignInPageB2B singInPage = rp.selectOrganizationRegister()
	
					//type organization name
					.typeOrganizationName(organizationName)
			
					//type organization address
					.typeOrganizationStreetAddressLine1(f_dsm.getInputParameter("ORGANIZATION_ADDRESS"))
			
					//select organization country
					.selectOrganizationCountryOrRegion(f_dsm.getInputParameter("ORGANIZATION_COUNTRY"))
			
					//select organization province
					.selectOrganizationStateOrProvince(f_dsm.getInputParameter("ORGANIZATION_STATE"))
			
					//type organization city
					.typeOrganizationCity(f_dsm.getInputParameter("ORGANIZATION_CITY"))
			
					//type organization zipcode
					.typeOrganizationZipCode(f_dsm.getInputParameter("ORGANIZATION_ZIPCODE"))
			
					//type organization email
					.typeOrganizationEmail(f_dsm.getInputParameter("ORGANIZATION_EMAIL"))
			
					//type organization phone number
					.typeOrganizationPhoneNumber(f_dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))
			
					//type buyer username
					.typeOrganizationBuyerLogonId(adminUsername)
			
					//type buyer password
					.typeOrganizationBuyerPassword(f_dsm.getInputParameter("PASSWORD"))
			
					//Verify buyer password verify
					.typeOrganizationBuyerVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))
			
					//type buyer first name
					.typeOrganizationBuyerFirstName(f_dsm.getInputParameter("FIRST_NAME"))
			
					//type buyer last name
					.typeOrganizationBuyerLastName(f_dsm.getInputParameter("LAST_NAME"))
			
					//type buyer street address
					.typeOrganizationBuyerStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))
			
					//Select buyer country
					.selectOrganizationBuyerCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))
			
					//type or select state for buyer
					.selectOrganizationBuyerStateOrProvince(f_dsm.getInputParameter("STATE"))
			
					//type buyer city
					.typeOrganizationBuyerCity(f_dsm.getInputParameter("CITY"))
			
					//type buyer zipcode
					.typeOrganizationBuyerZipCode(f_dsm.getInputParameter("ZIPCODE"))
			
					//type buyer E-mail
					.typeOrganizationBuyerEmail(f_dsm.getInputParameter("EMAIL"))
			
					//type buyer home phone number
					.typeOrganizationBuyerPhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))
			
					//Check if preferred language drop down is visible
					.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			
			
					//Select buyer preferred language
					.selectOrganizationBuyerPreferedCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))
			
					//Check if preferred currency drop down is visible
					.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()
			
					//Select buyer preferred language
					.selectOrganizationBuyerPreferedLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))
			
					//Submit registration
					.submitOrganizationBuyerRegistration()
			
					//verify registration confirmation message
					.verifyOrganizationBuyerRegistrationConfirmationMessage();
	
			rp = singInPage.register();
			
			rp.selectBuyerRegister()
			
					//type buyer username
					.typeLogonId(buyerUsername)
					
					//type organization name
					.typeOrganization(organizationName)
					
					//type buyer password
					.typePassword(f_dsm.getInputParameter("PASSWORD"))
					
					//type buyer verify password
					.typeVerifyPassword(f_dsm.getInputParameter("PASSWORD"))
					
					//type buyer first name
					.typeFirstName(f_dsm.getInputParameter("FIRST_NAME"))
					
					//type buyer last name
					.typeLastName(f_dsm.getInputParameter("LAST_NAME"))
					
					//type organization address
					.typeStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))
					
					//type organization city
					.typeCity(f_dsm.getInputParameter("CITY"))
					
					//select country for buyer
					.selectCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))
					
					//select state or province for buyer
					.selectStateOrProvince(f_dsm.getInputParameter("STATE"))
					
					//type zipcode for buyer
					.typeZipCode(f_dsm.getInputParameter("ZIPCODE"))
					
					//type email
					.typeEmail(f_dsm.getInputParameter("EMAIL"))
					
					.submit();
			
			// Approve Organization
			oac.logon(f_dsm.getInputParameter("ACCELERATOR_LOGON_ID"), f_dsm.getInputParameter("ACCELERATOR_PASSWORD"));
			oac.approveAllApprovals();
			
			setupCompleted = true;
		}
	}
	
	/**
	 *  View the list of buyers the buyer admin can manage
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_05_01() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel

				.typeUsername(adminUsername)
				
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				//Click Sign In
				.signIn();
		
		//Go to organizations and buyers page from my account side bar
		OrganizationsAndBuyersPage orgAndBuyersPage = header.goToMyAccount()
		
				.getSidebar()
				
				.goToOrganizationsAndBuyersPage();
		
		//Verify buyers account status
		orgAndBuyersPage.getOrganizationBuyerListWidget()
		
				.verifyBuyerAccountStatus(f_dsm.getInputParameter("STATUS"), buyerUsername)
				
				.verifyBuyerAccountStatus(f_dsm.getInputParameter("STATUS"), adminUsername);
	}

	/**
	 *  Buyer admin uses the search function to filter the buyers
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_05_03() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel

				.typeUsername(adminUsername)
				
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				//Click Sign In
				.signIn();
		
		//Go to organizations and buyers page from my account side bar
		OrganizationsAndBuyersPage orgAndBuyersPage = header.goToMyAccount()
		
				.getSidebar()
				
				.goToOrganizationsAndBuyersPage();
		
		//Search for buyer by username
		orgAndBuyersPage.getOrganizationBuyerListWidget()
		
				.selectSearchOptions()
				
				.typeLogonId(buyerUsername)
				
				.searchBuyerList();
		
		//Verify buyer status
		orgAndBuyersPage.getOrganizationBuyerListWidget()
		
				.verifyBuyerAccountStatus(f_dsm.getInputParameter("STATUS"), buyerUsername);
	}
	
	/**
	 *  Update the profile of a buyer by clicking the buyer's last name
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_05_06() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel

				.typeUsername(adminUsername)
				
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				//Click Sign In
				.signIn();
		
		//Go to organizations and buyers page from my account side bar
		OrganizationsAndBuyersPage orgAndBuyersPage = header.goToMyAccount()
		
				.getSidebar()
				
				.goToOrganizationsAndBuyersPage();
		
		//Go to buyer detail page
		BuyerDetailsPage buyerDetailsPage = orgAndBuyersPage.getOrganizationBuyerListWidget()
		
				.goToOrganizationBuyerDetailPage(buyerUsername);
		
		//Edit buyers detail info
		buyerDetailsPage.getBuyerDetailsWidget()
		
				.getOrganizationBuyerDetails()
				
				.selectEditBuyerDetails()
				
				.typeLastName(f_dsm.getInputParameter("LAST_NAME_UPDATE"))
				
				.saveBuyerDetail();
		
		//Verify buyers detail
		buyerDetailsPage.getBuyerDetailsWidget()
		
				.getOrganizationBuyerDetails()
				
				.verifyFullName(f_dsm.getInputParameter("FULL_NAME"));
	}
	
	/**
	 *  Updates a buyer by assigning a new role
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_05_08() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel

				.typeUsername(adminUsername)
				
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				//Click Sign In
				.signIn();
		
		//Go to organizations and buyers page from my account side bar
		OrganizationsAndBuyersPage orgAndBuyersPage = header.goToMyAccount()
		
				.getSidebar()
				
				.goToOrganizationsAndBuyersPage();
		
		//Go to buyer detail page
		BuyerDetailsPage buyerDetailsPage = orgAndBuyersPage.getOrganizationBuyerListWidget()
		
				.goToOrganizationBuyerDetailPage(buyerUsername);
		
		//Edit buyers roles
		buyerDetailsPage.getBuyerDetailsWidget()
		
				.getUserRoleManagement()
				
				.selectEditUserRole()
				
				.selectRole(f_dsm.getInputParameter("NEW_ROLE"))
				
				.saveRoles();
		
		//Verify buyers role changes
		buyerDetailsPage.getBuyerDetailsWidget()
		
				.getUserRoleManagement()
				
				.verifyUserRole(f_dsm.getInputParameter("NEW_ROLE"));
	}
	
	/**
	 * Create a new registered buyer
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_05_14() throws Exception
	{
		String newBuyerLogin = f_dsm.getInputParameter("BUYER_LOGONID") + System.currentTimeMillis();
		
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel

				.typeUsername(adminUsername)
				
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				//Click Sign In
				.signIn();
		
		//Go to organizations and buyers page from my account side bar
		OrganizationsAndBuyersPage orgAndBuyersPage = header.goToMyAccount()
		
				.getSidebar()
				
				.goToOrganizationsAndBuyersPage();

		//Click add buyer button. 
		AddBuyerPage addBuyerPage = orgAndBuyersPage.getOrganizationBuyerListWidget()
		
				.goToAddBuyerPage();
		
		//Enter buyers detail info
		addBuyerPage
			
				.getBuyerDetailsWidget()
				
				.typeLogonId(newBuyerLogin)
				
				.typeFirstName(f_dsm.getInputParameter("FIRST_NAME"))
				
				.typeLastName(f_dsm.getInputParameter("LAST_NAME"))
				
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				.typeVerifyPassword(f_dsm.getInputParameter("PASSWORD"))
				
				.typeEmail(f_dsm.getInputParameter("EMAIL"));
		
		//Enter buyers address info
		addBuyerPage
		
				.getBuyerAddressWidget()
				
				.typeStreetAddress(f_dsm.getInputParameter("ADDRESS"))
				
				.typeZipCode(f_dsm.getInputParameter("ZIPCODE"))
				
				.typeCity(f_dsm.getInputParameter("CITY"));
				
		//Click submit buttom to create new buyer
		addBuyerPage.submit();
		
		//Verify buyers account status
		orgAndBuyersPage.getOrganizationBuyerListWidget()
		
				.verifyBuyerAccountStatus(f_dsm.getInputParameter("STATUS"), newBuyerLogin);
	}
	
	/**
	 * Buyer admin locks a registered buyer account
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_05_19() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel

				.typeUsername(adminUsername)
				
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				//Click Sign In
				.signIn();
		
		//Go to organizations and buyers page from my account side bar
		OrganizationsAndBuyersPage orgAndBuyersPage = header.goToMyAccount()
		
				.getSidebar()
				
				.goToOrganizationsAndBuyersPage();
		
		//Disable buyers account
		orgAndBuyersPage
			
				.getOrganizationBuyerListWidget()
				
				.selectDisableUserAccount(buyerUsername)
				
				//Verify buyers account status is updated
				.verifyBuyerAccountStatus(f_dsm.getInputParameter("STATUS"), buyerUsername);
	}
	
	/**
	 * Update the member group and role of a buyer
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_05_22() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel

				.typeUsername(adminUsername)
				
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				//Click Sign In
				.signIn();

		//Go to organizations and buyers page from my account side bar
		OrganizationsAndBuyersPage orgAndBuyersPage = header.goToMyAccount()
		
				.getSidebar()
				
				.goToOrganizationsAndBuyersPage();
		
		//Go to buyer detail page from action dropdown menu
		BuyerDetailsPage buyerDetailsPage = orgAndBuyersPage
			
				.getOrganizationBuyerListWidget()
				
				.goToBuyerDetailsFromActionDropDown(buyerUsername);
		
		//Edit buyers member group info
		buyerDetailsPage.getBuyerDetailsWidget()
				
				.getBuyerMemberGroup()
				
				.selectEditMemberGroup()
				
				.selectMemberGroupToInclude(organizationName + "/OrderApprovalGroup")
				
				.saveMemberGroupUpdate();
		
		//Verify buyers member group info
		buyerDetailsPage
		
				.getBuyerDetailsWidget()
				
				.getBuyerMemberGroup()
				
				.verifyMemberGroups("1", organizationName + "/OrderApprovalGroup");
				
	}
	
}