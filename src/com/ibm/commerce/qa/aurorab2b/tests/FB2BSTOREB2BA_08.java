package com.ibm.commerce.qa.aurorab2b.tests;

import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.CustomerRegisterationPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.ProductDisplayPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.RequisitionListsPage;
import com.ibm.commerce.qa.aurorab2b.page.SavedOrdersPage;
import com.ibm.commerce.qa.aurorab2b.page.SignInPageB2B;
import com.ibm.commerce.qa.casl.util.CaslModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.OrgAdminConsole;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.wte.framework.test.WebSession;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
/**
 * Scenario: FSSC-92726-08
 * Details: Buyer Admin uses the on-behalf feature
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class, CaslModule.class})
public class FB2BSTOREB2BA_08 extends AbstractAuroraSingleSessionTests {


	private final OrgAdminConsole oac;
	
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider f_dsm;

	private String adminUsername;
	
	private String buyerUsername;
	
	private String lastName;
	
	/**
	 * Test Class object constructor.
	 * 
	 */
	@Inject
	public FB2BSTOREB2BA_08(
			Logger log, 
			WcWteTestRule wcWteTestRule,
			CaslFoundationTestRule caslTestRule,
			TestDataProvider p_dsm,
			OrgAdminConsole p_oac,
			WebSession p_webSession) throws Exception
	{
		super(log, wcWteTestRule, caslTestRule);
		
		f_dsm = p_dsm;
		oac = p_oac;
	}
	
	/**
	 * Setup to create buyer
	 * 
	 * @throws Exception
	 */
	@Before
	public void setup() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPanel.registerB2B();

		adminUsername = f_dsm.getInputParameter("ORGANIZATION_ADMIN_LOGONID") + System.currentTimeMillis();
		buyerUsername = f_dsm.getInputParameter("BUYER_LOGONID") + System.currentTimeMillis();
		lastName = f_dsm.getInputParameter("LAST_NAME") + System.currentTimeMillis();
		String organizationName = f_dsm.getInputParameter("ORGANIZATION_NAME") + System.currentTimeMillis();

		SignInPageB2B singInPage = rp.selectOrganizationRegister()

				//type organization name
				.typeOrganizationName(organizationName)
		
				//type organization address
				.typeOrganizationStreetAddressLine1(f_dsm.getInputParameter("ORGANIZATION_ADDRESS"))
		
				//select organization country
				.selectOrganizationCountryOrRegion(f_dsm.getInputParameter("ORGANIZATION_COUNTRY"))
		
				//select organization province
				.selectOrganizationStateOrProvince(f_dsm.getInputParameter("ORGANIZATION_STATE"))
		
				//type organization city
				.typeOrganizationCity(f_dsm.getInputParameter("ORGANIZATION_CITY"))
		
				//type organization zipcode
				.typeOrganizationZipCode(f_dsm.getInputParameter("ORGANIZATION_ZIPCODE"))
		
				//type organization email
				.typeOrganizationEmail(f_dsm.getInputParameter("ORGANIZATION_EMAIL"))
		
				//type organization phone number
				.typeOrganizationPhoneNumber(f_dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))
		
				//type buyer username
				.typeOrganizationBuyerLogonId(adminUsername)
		
				//type buyer password
				.typeOrganizationBuyerPassword(f_dsm.getInputParameter("PASSWORD"))
		
				//Verify buyer password verify
				.typeOrganizationBuyerVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))
		
				//type buyer first name
				.typeOrganizationBuyerFirstName(f_dsm.getInputParameter("FIRST_NAME"))
		
				//type buyer last name
				.typeOrganizationBuyerLastName(f_dsm.getInputParameter("LAST_NAME"))
		
				//type buyer street address
				.typeOrganizationBuyerStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))
		
				//Select buyer country
				.selectOrganizationBuyerCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))
		
				//type or select state for buyer
				.selectOrganizationBuyerStateOrProvince(f_dsm.getInputParameter("STATE"))
		
				//type buyer city
				.typeOrganizationBuyerCity(f_dsm.getInputParameter("CITY"))
		
				//type buyer zipcode
				.typeOrganizationBuyerZipCode(f_dsm.getInputParameter("ZIPCODE"))
		
				//type buyer E-mail
				.typeOrganizationBuyerEmail(f_dsm.getInputParameter("EMAIL"))
		
				//type buyer home phone number
				.typeOrganizationBuyerPhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))
		
				//Check if preferred language drop down is visible
				.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			
		
				//Select buyer preferred language
				.selectOrganizationBuyerPreferedCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))
		
				//Check if preferred currency drop down is visible
				.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()
		
				//Select buyer preferred language
				.selectOrganizationBuyerPreferedLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))
		
				//Submit registration
				.submitOrganizationBuyerRegistration()
		
				//Verify registration confirmation message
				.verifyOrganizationBuyerRegistrationConfirmationMessage();

		rp = singInPage.register();
		
		rp.selectBuyerRegister()
		
				.typeLogonId(buyerUsername)
				
				.typeOrganization(organizationName)
				
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				.typeVerifyPassword(f_dsm.getInputParameter("PASSWORD"))
				
				.typeFirstName(f_dsm.getInputParameter("FIRST_NAME"))
				
				.typeLastName(lastName)
				
				.typeStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))
				
				.typeCity(f_dsm.getInputParameter("CITY"))
				
				.selectCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))
				
				.selectStateOrProvince(f_dsm.getInputParameter("STATE"))
				
				.typeZipCode(f_dsm.getInputParameter("ZIPCODE"))
				
				.typeEmail(f_dsm.getInputParameter("EMAIL"))
				
				.submit();
		
		// Approve Organization
		oac.logon(f_dsm.getInputParameter("ACCELERATOR_LOGON_ID"), f_dsm.getInputParameter("ACCELERATOR_PASSWORD"));
		oac.approveAllApprovals();
	}

	/**
	 * Buyer Admin completes an order on behalf of buyer
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_08_02() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel

				.typeUsername(adminUsername)
				
				.typePassword(f_dsm.getInputParameter("ADMIN_PASSWORD"))
				
				//Click Sign In
				.signIn();

		//Select Buy on behalf off and enter user last name 
		header.getSignOutDropDownWidget()
			
				//Select buy on behalf of check box 
				.selectBuyOnBehalf()
				
				//Enter buyer last name
				.typeBuyForUser(lastName);
		
		//Go to requisition list page from my account side bar
		RequisitionListsPage requisitionListsPage = header.goToMyAccount()
		
				.getSidebar()
		
				.goToRequisitionListPage();
		
		//Go to product display page from catalog entry recommendation section
		ProductDisplayPageB2B pdp = requisitionListsPage
				
				.getCatalogEntryRecommendationWidget()
				
				.viewCatentryNumber(ProductDisplayPageB2B.class, 3);
	
		//Change quantity of sku
		pdp.getSkuListWidget()
				
				.updateQuantity(f_dsm.getInputParameter("SKU"), f_dsm.getInputParameter("QUANTITY"));
			
		//Click the add to current order button
		pdp.addToCurrentOrder(f_dsm.getInputParameter("SKU"));
		
		//Go to shopping cart page
		ShopCartPage shopCartPage = pdp.getHeaderWidget().goToShoppingCartPage();
	
		//Complete order checkout 
		shopCartPage.continueToNextStep()
		
				.selectPaymentMethod(f_dsm.getInputParameter("PAYMENT_TYPE"))
				
				.singleShipNext()
				
				.completeOrder()
				
				.verifyOrderSuccessful();
	}
	
	/**
	 * Buyer can't place an order when Buyer Admin has taken over cart
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_08_05() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel

				.typeUsername(adminUsername)
				
				.typePassword(f_dsm.getInputParameter("ADMIN_PASSWORD"))
				
				//Click Sign In
				.signIn(HeaderWidget.class);

		header.getSignOutDropDownWidget()
			
				.selectBuyOnBehalf()
			
				.typeBuyForUser(lastName);
		
		frontPage = getSession().continueToPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
		
		//Go to department page	
		CategoryPage subCat = header.goToCategoryPageByHierarchy(CategoryPage.class,
				f_dsm.getInputParameter("DEPARTMENT"), f_dsm.getInputParameter("CATEGORY"));
		
		//click on product image on Category Display page.
		ProductDisplayPageB2B productDisplay = subCat.getCatalogEntryListWidget()
				.goToProductPageByNameB2B(f_dsm.getInputParameter("PRODUCT_NAME"));

		//Select the product attributes			
		productDisplay.getDefiningAttributesWidget()
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR1_NAME"), f_dsm.getInputParameter("ATTR1_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR3_NAME"), f_dsm.getInputParameter("ATTR3_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR4_NAME"), f_dsm.getInputParameter("ATTR4_VALUE"));
	
		//Change quantity of sku
		productDisplay.getSkuListWidget()
		
				.updateQuantity(f_dsm.getInputParameter("PRODUCT_SKU"), f_dsm.getInputParameter("QUANTITY"));
			
		//click Add to Current Order from Product Display page.
		productDisplay.addToCurrentOrder(f_dsm.getInputParameter("PRODUCT_SKU"));
		
		subCat = header.goToCategoryPageByHierarchy(CategoryPage.class,
				f_dsm.getInputParameter("DEPARTMENT"), f_dsm.getInputParameter("CATEGORY"));
		
		productDisplay = subCat.getCatalogEntryListWidget()
				.goToProductPageByNameB2B(f_dsm.getInputParameter("PRODUCT_NAME2"));
		
		//Select the product attributes			
		productDisplay.getDefiningAttributesWidget()
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR1_NAME"), f_dsm.getInputParameter("ATTR1_VALUE2"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR2_NAME"), f_dsm.getInputParameter("ATTR2_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR3_NAME"), f_dsm.getInputParameter("ATTR3_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR4_NAME"), f_dsm.getInputParameter("ATTR4_VALUE"));
	
		//Change quantity of sku
		productDisplay.getSkuListWidget()
		
				.updateQuantity(f_dsm.getInputParameter("PRODUCT_SKU2"), f_dsm.getInputParameter("QUANTITY"));
		
		//click Add to Current Order from Product Display page.
		productDisplay.addToCurrentOrder(f_dsm.getInputParameter("PRODUCT_SKU2"));
		
		ShopCartPage shopCart =  productDisplay.getHeaderWidget().goToShoppingCartPage();
		
				shopCart.verifyItemInShopCart(f_dsm.getInputParameter("PRODUCT_SKU"))
				
				.verifyItemInShopCart(f_dsm.getInputParameter("PRODUCT_SKU2"))
				
				.removeFromCart("1")
				
				.verifyItemNotInShopCart(f_dsm.getInputParameter("PRODUCT_SKU"));
				
		header = shopCart.getHeaderWidget();
		shopCart = header.closeMessageArea().goToShoppingCartPage();
		shopCart.getHeaderWidget().openSignOutDropDownWidget()
				
				.signOutB2B()
				
				.signIn();

		//Enter a valid username
		header = signInPanel

				.typeUsername(buyerUsername)
				
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				//Click Sign In
				.signIn(HeaderWidget.class);
		
		//Go to department page	
		subCat = header.goToCategoryPageByHierarchy(CategoryPage.class,
				f_dsm.getInputParameter("DEPARTMENT"), f_dsm.getInputParameter("CATEGORY"));
		
		//click on product image on Category Display page.
		productDisplay = subCat.getCatalogEntryListWidget()
				.goToProductPageByNameB2B(f_dsm.getInputParameter("PRODUCT_NAME"));

		//Select the product attributes			
		productDisplay.getDefiningAttributesWidget()
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR1_NAME"), f_dsm.getInputParameter("ATTR1_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR3_NAME"), f_dsm.getInputParameter("ATTR3_VALUE"))
				
				.selectAttributeFromDropdown(f_dsm.getInputParameter("ATTR4_NAME"), f_dsm.getInputParameter("ATTR4_VALUE"));
	
		//Change quantity of sku
		productDisplay.getSkuListWidget()
		
				.updateQuantity(f_dsm.getInputParameter("PRODUCT_SKU"), f_dsm.getInputParameter("QUANTITY"));
			
		//click Add to Current Order from Product Display page.
		productDisplay.getSkuListWidget().clickAddToCurrentOrder();
		
		productDisplay.getHeaderWidget().verifyMessage("locked");
	}

	/**
	 * Locking an order when buyer admin uses the on-behalf feature
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_08_06() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel

				.typeUsername(buyerUsername)
				
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				//Click Sign In
				.signIn();
		
		//Go to saved orders page from my account page side bar
		SavedOrdersPage savedOrdersPage = header.goToMyAccount()
		
				.getSidebar()
				
				.goToSavedOrdersPage();
		
		//Create a new saved order
		savedOrdersPage.getSavedOrdersWidget()
		
				.createNewSavedOrder(f_dsm.getInputParameter("ORDER_NAME"))
				
				.verifySavedOrderByName(f_dsm.getInputParameter("ORDER_NAME"));
		
		//Open AuroraB2BEsite store
		frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
		
		header = frontPage.getHeaderWidget();
		
		//Sign in as admin user
		header.signIn()
				
				.typeUsername(adminUsername)
				
				.typePassword(f_dsm.getInputParameter("ADMIN_PASSWORD"))
				
				//Click Sign In
				.signIn();

		//Select buy on behalf off
		header.getSignOutDropDownWidget()
		
				//Select buy on behalf of check box
				.selectBuyOnBehalf()
			
				//Enter buyers last name
				.typeBuyForUser(lastName);
		
		//Go to saved orders page from my account page side bar
		savedOrdersPage = header.goToMyAccount().getSidebar()
				
				.goToSavedOrdersPage();
			
		//Lock the order and verify order is locked
		savedOrdersPage.getSavedOrdersWidget()
				
				.lockOrderByPosition(f_dsm.getInputParameter("ORDER_POSITION"))
				
				.verifyOrderIsLocked(f_dsm.getInputParameter("ORDER_POSITION"));
	}
}

