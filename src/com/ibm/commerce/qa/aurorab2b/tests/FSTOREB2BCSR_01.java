package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.casl.keys.CaslKeysFactory;
import com.ibm.commerce.casl.keys.ToolingKeys;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.CustomerServiceFindCustomerWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.wte.util.WcConfigManager;


/** 
 * Test scenario to test various use cases associated with Dropdown menu context
 * Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FSTOREB2BCSR_01 extends AbstractAuroraSingleSessionTests
{
	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

	//A Variable to retrieve data from the data file. 
	@DataProvider
	private final TestDataProvider dsm;
	private CaslKeysFactory f_caslKeysFactory;

	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with getConfig().properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_caslFixtures 
	 */		
	@Inject
	public FSTOREB2BCSR_01(
			Logger log, 
			WcConfigManager config,
			WcWteTestRule wcWebTestRule,
			CaslFoundationTestRule caslTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_caslFixtures, CaslKeysFactory p_caslKeysFactory)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		f_caslKeysFactory = p_caslKeysFactory;
	}

				
		
	/** Test case to Log on to B2B storefront as a CSR
	 */
	@Category(Sanity.class)
	@Test
	public void testFSTOREB2BCSR_0107()
	{
		
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);

		//Open Sign In/Register page.
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();

		//Enter account User Name/Password and sign in.
		signInPage.typeUsername(dsm.getInputParameter("LOGONID"))
		.typePassword(dsm.getInputParameter("PASSWORD")).signIn();	
	}
	
	@Test
	public void testFSTOREB2BCSR_0108()
	{
		//Open the store in the browser.
				AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);

				//Open Sign In/Register page.
				SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();

				//Enter account User Name/Password and sign in.
				signInPage.typeUsername(dsm.getInputParameter("CSR_LOGONID"))
				.typePassword(dsm.getInputParameter("CSR_PASSWORD")).signIn();	
				
				
				//Get LOGONID of a shopper that we are trying to locate
				String ShopperLogonId = dsm.getInputParameter("BUYER_LOGONID");
				CustomerServiceFindCustomerWidget FindCustomerWidget = frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
				.typeLogonId(ShopperLogonId).submitSearch();
				
				//Query to return unique user id (USER_ID) 
				ToolingKeys p = f_caslKeysFactory.createToolingKeys(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"));
				final String USER_ID = p.findPersonIdByLogonId(ShopperLogonId);
				
				//Access Customer's account and shop on behalf of a shopper as a CSR 
				FindCustomerWidget.AccessCustomerAccount(USER_ID).getHeaderWidget().goToFrontPageB2B();
	}
	
	
	@Test
	public void testFSTOREB2BCSR_0110()
	{
		//Open the store in the browser.
				AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);

				//Open Sign In/Register page.
				SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();

				//Enter account User Name/Password and sign in.
				signInPage.typeUsername(dsm.getInputParameter("CSR_LOGONID"))
				.typePassword(dsm.getInputParameter("CSR_PASSWORD")).signIn();	
				
				
				//Get LOGONID of a shopper that we are trying to locate
				String ShopperLogonId = dsm.getInputParameter("BUYER_LOGONID");
				CustomerServiceFindCustomerWidget FindCustomerWidget = frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
				.typeLogonId(ShopperLogonId).submitSearch();
				
				//Query to return unique user id (USER_ID) 
				ToolingKeys p = f_caslKeysFactory.createToolingKeys(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"));
				final String USER_ID = p.findPersonIdByLogonId(ShopperLogonId);
				
				//Access Customer's account and terminate on behalf of session 
				MyAccountMainPage CustomerMyAccountPage = FindCustomerWidget.AccessCustomerAccount(USER_ID);
				
				//One way of terminating session by clicking Singin as Yourself link in Signout widget
				CustomerMyAccountPage.getHeaderWidget().openSignOutDropDownWidget().signInAsYourself();
				
				//Access Customer's account
				CustomerMyAccountPage = FindCustomerWidget.typeLogonId(ShopperLogonId).submitSearch()
						                .AccessCustomerAccount(USER_ID);

				//Second way of terminating session by clicking Customer Service Link from Header
				CustomerMyAccountPage.getHeaderWidget().goToFrontPageB2B()
				.getHeaderWidget().gotoCustomerServiceWithTerminateModal().clickYes();
	}
	
	@Test
	public void testFSTOREB2BCSR_0111()
	{
		//Open the store in the browser.
				AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);

				//Open Sign In/Register page.
				SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();

				//Enter account User Name/Password and sign in.
				signInPage.typeUsername(dsm.getInputParameter("CSR_LOGONID"))
				.typePassword(dsm.getInputParameter("CSR_PASSWORD")).signIn();	
				
				
				//Get LOGONID of a shopper that we are trying to locate
				String ShopperLogonId = dsm.getInputParameter("BUYER_ADMIN_LOGONID");
				CustomerServiceFindCustomerWidget FindCustomerWidget = frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
				.typeLogonId(ShopperLogonId).submitSearch();
				
				//Query to return unique user id (USER_ID) 
				ToolingKeys p = f_caslKeysFactory.createToolingKeys(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"));
				final String USER_ID = p.findPersonIdByLogonId(ShopperLogonId);
				
				//Access Customer's account and shop on behalf of a shopper as a CSR 
				FindCustomerWidget.AccessCustomerAccount(USER_ID).getHeaderWidget().goToFrontPageB2B();
	}
	
}
