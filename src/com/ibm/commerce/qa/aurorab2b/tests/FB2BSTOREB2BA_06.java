package com.ibm.commerce.qa.aurorab2b.tests;

import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.BuyersApprovalDetailsPage;
import com.ibm.commerce.qa.aurorab2b.page.BuyersApprovalPage;
import com.ibm.commerce.qa.aurorab2b.page.CustomerRegisterationPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.SignInPageB2B;
import com.ibm.commerce.qa.casl.util.CaslModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.OrgAdminConsole;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.wte.framework.test.WebSession;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
/**
 * Scenario: FSSC-92726-06
 * Details: Manage buyer approvals as a buyer admin user
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class, CaslModule.class})
public class FB2BSTOREB2BA_06 extends AbstractAuroraSingleSessionTests {


	private OrgAdminConsole oac;
	
	
	
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider f_dsm;

	private static String adminUsername;
	
	private static String buyerUsername;
	
	private static Boolean setupCompleted = false;
	
	private static String buyerFullName;
	
	
	
	/**
	 * Test Class object constructor.
	 * 
	 */
	@Inject
	public FB2BSTOREB2BA_06(
			Logger log, 
			WcWteTestRule wcWteTestRule,
			CaslFoundationTestRule caslTestRule,
			TestDataProvider p_dsm,
			OrgAdminConsole p_oac,
			WebSession p_webSession) throws Exception
	{
		super(log, wcWteTestRule, caslTestRule);
		
		f_dsm = p_dsm;
		oac = p_oac;
	}
	
	@Before
	public void setup() throws Exception
	{
		adminUsername = f_dsm.getInputParameter("ORGANIZATION_ADMIN_LOGONID");
		
		if(!setupCompleted)
		{
			//Open AuroraB2BEsite store
			AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	
	
			//Click on the SignIn page link on the header
			SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();
	
			//Click on the registration button on the SignIn page
			CustomerRegisterationPageB2B rp = signInPanel.registerB2B();
	
			buyerUsername = f_dsm.getInputParameter("BUYER_LOGONID") + System.currentTimeMillis();
			
			buyerFullName = f_dsm.getInputParameter("FIRST_NAME") + System.currentTimeMillis();
			
			rp.selectBuyerRegister()
			
				//type buyer username
				.typeLogonId(buyerUsername)
				
				//type organization name
				.typeOrganization(f_dsm.getInputParameter("ORGANIZATION_NAME"))
				
				//type buyer password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				//type buyer verify password
				.typeVerifyPassword(f_dsm.getInputParameter("PASSWORD"))
				
				//type buyer first name
				.typeFirstName(buyerFullName)
				
				//type buyer last name
				.typeLastName(f_dsm.getInputParameter("LAST_NAME"))
				
				//type organization address
				.typeStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))
				
				//type organization city
				.typeCity(f_dsm.getInputParameter("CITY"))
				
				//select country for buyer
				.selectCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))
				
				//select state or province for buyer
				.selectStateOrProvince(f_dsm.getInputParameter("STATE"))
				
				//type zipcode for buyer
				.typeZipCode(f_dsm.getInputParameter("ZIPCODE"))
				
				//type email
				.typeEmail(f_dsm.getInputParameter("EMAIL"))
				
				.submit();

			buyerFullName = buyerFullName + " " + f_dsm.getInputParameter("LAST_NAME");
		}
		setupCompleted = true;
	}
	
	/**
	 *  View the details of a buyer approval request by clicking the buyer's approval id
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_06_04() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel

				.typeUsername(adminUsername)
				
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				//Click Sign In
				.signIn();
		
		//Go to buyers approval page
		BuyersApprovalPage buyerApprovalPage = header.goToMyAccount()
		
				.getSidebar()
				
				.goToBuyerApprovalPage();
		
		//Go to buyers approval detail page
		BuyersApprovalDetailsPage buyerApprovalDetailPage = buyerApprovalPage.getBuyersApprovalWidget()
		
				.goToApprovalDetailsPage(buyerFullName);
				
		//Verify buyers detail information from buyers detail page
		buyerApprovalDetailPage.getBuyersApprovalDetailsWidget()
				
				.verifyBuyerDetails(f_dsm.getInputParameter("BUYER_DETAILS_LOGONID_POS"), buyerUsername);
	}
	
	
	/**
	 *  Buyer admin approves a pending buyer approval request and leaves a comment
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_06_08() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel

				.typeUsername(adminUsername)
				
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				//Click Sign In
				.signIn();
		
		//Go to buyers approval page
		BuyersApprovalPage buyerApprovalPage = header.goToMyAccount()
		
				.getSidebar()
				
				.goToBuyerApprovalPage();
		
		//Approve the buyers approval request and verify the message pop
		buyerApprovalPage.getBuyersApprovalWidget()
		
				.approveBuyer(buyerFullName)
				
				.verifyMessagePopup(f_dsm.getInputParameter("APPROVED_MESSAGE"));
	}
	
	/**
	 * Buyer admin can only see the buyer approval requests under their administration
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_06_11() throws Exception
	{
		String adminUsername2 = f_dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID") + System.currentTimeMillis();

		String organizationName2 = f_dsm.getInputParameter("ORGANIZATION_2_NAME") + System.currentTimeMillis();
		
		String buyerUsername2 = f_dsm.getInputParameter("BUYER_LOGONID") + System.currentTimeMillis();
		
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPanel.registerB2B();

		SignInPageB2B singInPage = rp.selectOrganizationRegister()

				//type organization name
				.typeOrganizationName(organizationName2)
		
				//type organization address
				.typeOrganizationStreetAddressLine1(f_dsm.getInputParameter("ORGANIZATION_ADDRESS"))
		
				//select organization country
				.selectOrganizationCountryOrRegion(f_dsm.getInputParameter("ORGANIZATION_COUNTRY"))
		
				//select organization province
				.selectOrganizationStateOrProvince(f_dsm.getInputParameter("ORGANIZATION_STATE"))
		
				//type organization city
				.typeOrganizationCity(f_dsm.getInputParameter("ORGANIZATION_CITY"))
		
				//type organization zipcode
				.typeOrganizationZipCode(f_dsm.getInputParameter("ORGANIZATION_ZIPCODE"))
		
				//type organization email
				.typeOrganizationEmail(f_dsm.getInputParameter("ORGANIZATION_EMAIL"))
		
				//type organization phone number
				.typeOrganizationPhoneNumber(f_dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))
		
				//type buyer username
				.typeOrganizationBuyerLogonId(adminUsername2)
		
				//type buyer password
				.typeOrganizationBuyerPassword(f_dsm.getInputParameter("PASSWORD"))
		
				//Verify buyer password verify
				.typeOrganizationBuyerVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))
		
				//type buyer first name
				.typeOrganizationBuyerFirstName(f_dsm.getInputParameter("FIRST_NAME"))
		
				//type buyer last name
				.typeOrganizationBuyerLastName(f_dsm.getInputParameter("LAST_NAME"))
		
				//type buyer street address
				.typeOrganizationBuyerStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))
		
				//Select buyer country
				.selectOrganizationBuyerCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))
		
				//type or select state for buyer
				.selectOrganizationBuyerStateOrProvince(f_dsm.getInputParameter("STATE"))
		
				//type buyer city
				.typeOrganizationBuyerCity(f_dsm.getInputParameter("CITY"))
		
				//type buyer zipcode
				.typeOrganizationBuyerZipCode(f_dsm.getInputParameter("ZIPCODE"))
		
				//type buyer E-mail
				.typeOrganizationBuyerEmail(f_dsm.getInputParameter("EMAIL"))
		
				//type buyer home phone number
				.typeOrganizationBuyerPhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))
		
				//Check if preferred language drop down is visible
				.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			
		
				//Select buyer preferred language
				.selectOrganizationBuyerPreferedCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))
		
				//Check if preferred currency drop down is visible
				.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()
		
				//Select buyer preferred language
				.selectOrganizationBuyerPreferedLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))
		
				//Submit registration
				.submitOrganizationBuyerRegistration()
		
				//verify registration confirmation message
				.verifyOrganizationBuyerRegistrationConfirmationMessage();

		// Approve Organization
		oac.logon(f_dsm.getInputParameter("ACCELERATOR_LOGON_ID"), f_dsm.getInputParameter("ACCELERATOR_PASSWORD"));
		oac.approveAllApprovals();
		
		rp = singInPage.register();
		
		rp.selectBuyerRegister()
		
				//type buyer username
				.typeLogonId(buyerUsername2)
				
				//type organization name
				.typeOrganization(f_dsm.getInputParameter("ORGANIZATION_1_NAME"))
				
				//type buyer password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				//type buyer verify password
				.typeVerifyPassword(f_dsm.getInputParameter("PASSWORD"))
				
				//type buyer first name
				.typeFirstName(f_dsm.getInputParameter("FIRST_NAME_BUYER"))
				
				//type buyer last name
				.typeLastName(f_dsm.getInputParameter("LAST_NAME_BUYER"))
				
				//type organization address
				.typeStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))
				
				//type organization city
				.typeCity(f_dsm.getInputParameter("CITY"))
				
				//select country for buyer
				.selectCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))
				
				//select state or province for buyer
				.selectStateOrProvince(f_dsm.getInputParameter("STATE"))
				
				//type zipcode for buyer
				.typeZipCode(f_dsm.getInputParameter("ZIPCODE"))
				
				//type email
				.typeEmail(f_dsm.getInputParameter("EMAIL"))
				
				.submit();
		
		//Open AuroraB2BEsite store
		frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel

				.typeUsername(adminUsername2)
				
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				//Click Sign In
				.signIn();
		
		//Go to buyers approval page from My Account page side bar
		BuyersApprovalPage buyerApprovalPage = header.goToMyAccount()
		
				.getSidebar()
				
				.goToBuyerApprovalPage();
		
		//Verify buyers approval request not visible for this user
		buyerApprovalPage.getBuyersApprovalWidget()
		
				.verifyApprovalDoesNotExist(f_dsm.getInputParameter("BUYER_FULL_NAME"));
		
		//Open AuroraB2BEsite store
		frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		signInPanel = frontPage.getHeaderWidget().signIn();
		
		//Enter a valid username
		header = signInPanel

				.typeUsername(adminUsername)
				
				.typePassword(f_dsm.getInputParameter("ADMIN_PASSWORD"))
				
				//Click Sign In
				.signIn();
		
		//Go to buyers approval page from My Account page side bar
		buyerApprovalPage = header.goToMyAccount()
				
				.getSidebar()
				
				.goToBuyerApprovalPage();
		
		//Verify buyer approval is visible from buyers approval page
		buyerApprovalPage.getBuyersApprovalWidget()
		
				.verifyApprovalExists(f_dsm.getInputParameter("BUYER_FULL_NAME"));
		
		//Go to buyers approval detail page
		BuyersApprovalDetailsPage approvalDetailsPage = buyerApprovalPage.getBuyersApprovalWidget()
		
				.goToApprovalDetailsPage(f_dsm.getInputParameter("BUYER_FULL_NAME"));
		
		//Enter commment and approve buyer approval request
		approvalDetailsPage.getBuyersApprovalCommentWidget()
		
				.typeComment(f_dsm.getInputParameter("COMMENT"))
				
				.approveBuyer();
		
	}
}