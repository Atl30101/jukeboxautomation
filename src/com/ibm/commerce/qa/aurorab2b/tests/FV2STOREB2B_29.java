package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.casl.keys.util.CaslKeysModule;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.LanguageCurrencyPopUp;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.MyPersonalInfoPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.ProductDisplayPageB2B;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.casl.util.CaslModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 * Scenario: FV2STOREB2C_06
 * Test scenario to test various use cases associated with user preferred language and currency.
 * Refer to each test case for a detailed use case description 
 */
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class, CaslModule.class, CaslKeysModule.class})
public class FV2STOREB2B_29 extends AbstractAuroraSingleSessionTests
{

	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	 
	 
	@DataProvider
	private final TestDataProvider dsm;
	
	private final CaslFixturesFactory f_CaslFixtures;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 */
	@Inject
	public FV2STOREB2B_29(
			Logger log, 
			WcWteTestRule wcWebTestRule,
			CaslFoundationTestRule caslTestRule,
			CaslFixturesFactory f_CaslFixtures,
			TestDataProvider dataSetManager)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.f_CaslFixtures = f_CaslFixtures;
		this.dsm = dataSetManager;
	}
	
	/** Test case to Change the currency of a registered shopper from the My Account page.
	 * @throws Exception
	 */

	@Test
	public void testFV2STOREB2B_2901() throws Exception
	{		
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		MyAccountMainPage myAccount = signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
			.signIn().goToMyAccount();
		
		cartCleanup(myAccount);
		
		//Click Home link on the Header.
		MyPersonalInfoPage personalInfo = myAccount.getSidebar().goToMyPersonalInfoPage();
		
		//Select preferred Currency from drop down list on Personal Information Page.
		personalInfo.typePreferredCurrency(dsm.getInputParameter("SELECTED_CURRENCY"));
		
		//Update the changes		
		myAccount = personalInfo.update();
		
		//Go to home page
		myAccount.getHeaderWidget().goToFrontPageB2B();
		
		//Navigate to category page
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, 
				dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), 
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"));
		
		//Verify the currency displayed on mini shop cart
		// (If cart is empty, it does not display the totals or currency.  Need to add an item to the cart.)
		// Click product name from category page 
		ProductDisplayPageB2B productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByNameB2B(dsm.getInputParameter("PRODUCT_NAME"));
		
		
		
		// Select swatches from product display page
		productDisplay.getDefiningAttributesWidget()
					.selectAttributeFromDropdown(dsm.getInputParameter("ATTR_NAME1"), dsm.getInputParameter("ATTR_VAL1"))
					.selectAttributeFromDropdown(dsm.getInputParameter("ATTR_NAME2"), dsm.getInputParameter("ATTR_VAL2"))
					.selectAttributeFromDropdown(dsm.getInputParameter("ATTR_NAME3"), dsm.getInputParameter("ATTR_VAL3"));					


		//change quantity of sku
		productDisplay.getSkuListWidget().updateQuantity(dsm.getInputParameter("SKU"), dsm.getInputParameter("QTY"));
		
		
		// Click add to cart
		productDisplay.addToCurrentOrder(dsm.getInputParameter("SKU"));
		
		subCat.getHeaderWidget().verifyMiniCartCurrency(dsm.getInputParameter("EXPECTED_CURRENCY"));
		
	}
	
	/** Test case to Change the currency of a registered shopper from the Home page.
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2B_2902() throws Exception
	{
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		MyAccountMainPage myAccount = signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME")).typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
			.signIn().goToMyAccount();
		
		//Click Home link on the Header.
		frontPage = signIn.getHeaderWidget().goToFrontPageB2B();
		
		//Open Language/Currency pop-up
		LanguageCurrencyPopUp languageCurrencyPopUp = frontPage.getHeaderWidget().goToLanguageCurrencyPopUp();
		
		//Select preferred Currency from drop down list
		languageCurrencyPopUp.selectCurrency(dsm.getInputParameter("SELECTED_CURRENCY"));
		
		//Apply the changes
		languageCurrencyPopUp.apply(AuroraFrontPageB2B.class);		
		
		//Navigate to category page
		CategoryPage subCat = frontPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, 
				dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"), 
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"));
		
		//Verify the currency displayed on mini shop cart
		// (If cart is empty, it does not display the totals or currency.  Need to add an item to the cart.)
		// Click product name from category page 
		ProductDisplayPageB2B productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByNameB2B(dsm.getInputParameter("PRODUCT_NAME"));
		
		
		
		// Select swatches from product display page
		productDisplay.getDefiningAttributesWidget()
					.selectAttributeFromDropdown(dsm.getInputParameter("ATTR_NAME1"), dsm.getInputParameter("ATTR_VAL1"))
					.selectAttributeFromDropdown(dsm.getInputParameter("ATTR_NAME2"), dsm.getInputParameter("ATTR_VAL2"))
					.selectAttributeFromDropdown(dsm.getInputParameter("ATTR_NAME3"), dsm.getInputParameter("ATTR_VAL3"));					


		//change quantity of sku
		productDisplay.getSkuListWidget().updateQuantity(dsm.getInputParameter("SKU"), dsm.getInputParameter("QTY"));
		
		
		// Click add to cart
		productDisplay.addToCurrentOrder(dsm.getInputParameter("SKU"));

		//Verify the currency displayed on mini shop cart
		productDisplay.getHeaderWidget().verifyMiniCartCurrency(dsm.getInputParameter("EXPECTED_CURRENCY"));
		
		//Sign out the store
		signIn = subCat.getHeaderWidget().openSignOutDropDownWidget().signOutB2B().signIn();
		
		//Sign back in
		signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
				.signIn();
		
		//Verify the currency displayed on mini shop cart
		signIn.getHeaderWidget().verifyMiniCartCurrency(dsm.getInputParameter("EXPECTED_CURRENCY"));
		
		
		myAccount = signIn.getHeaderWidget().goToMyAccount();
		
		//Click my personal information link
		MyPersonalInfoPage personalInfo = myAccount.getSidebar().goToMyPersonalInfoPage();
		
		//Verify preferred currency in Personal Information page.
		personalInfo.verifyPreferredCurrency(dsm.getInputParameter("PREFERRED_CURRENCY"));
	}
	
	/** 
	 * Test case to Change the preferred language of a registered shopper from the My Account page.
	 * @throws Exception
	 */
	public void testFV2STOREB2B_2903() throws Exception
	{
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		MyAccountMainPage myAccount = signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
											.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
											.signIn()
											.goToMyAccount();

		//Click my personal information link
		MyPersonalInfoPage personalInfo = myAccount.getSidebar().goToMyPersonalInfoPage();
		
		//Select preferred Language from drop down list on Personal Information Page.
		personalInfo.typePreferredLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"));
		
		//Click update button
		personalInfo.update();
		
		//Log Out of the store.
		HeaderWidget headerWidget = myAccount.getHeaderWidget().openSignOutDropDownWidget().signOutB2B();
		
		//Log in to the store.
		myAccount = headerWidget.signIn().typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
							.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
							.signIn()
							.goToMyAccount();
		
		//Click my personal information link
		personalInfo = myAccount.getSidebar().goToMyPersonalInfoPage();
		
		//Verify preferred currency in Personal Information page.
		personalInfo.verifyPerferredLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"));
		
		//Open language and currency pop-up
		LanguageCurrencyPopUp languageCurrencyPopUp = personalInfo.getHeaderWidget().goToLanguageCurrencyPopUp();
		
		//Verify the language
		languageCurrencyPopUp.verifyLanguage(dsm.getInputParameter("EXPECTED_LANGUAGE"));	
	}
	
	
	
	
		
	
	/**
	 * Perform teardown operations after the test is done, whether it is successful or not.
	 * @throws Exception
	 */
	@After
	public void testScenarioTearDown() throws Exception
	{
		try
		{
			getLog().info("testScenarioTearDown running");
			
			//Open the store in the browser.
			AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
			
			//Opens the Sign In page in browser.
			SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
			
			//Log on store
			signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
			.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
			.signIn().goToMyAccount()
			.getSidebar().goToMyPersonalInfoPage()		
			//Reset Shopper personal preferred language and currency 
			.typePreferredLanguage(dsm.getInputParameter("ORIGINAL_LANGUAGE"))
			.typePreferredCurrency(dsm.getInputParameter("ORIGINAL_CURRENCY"))
			.update()
			
					
			
			// Empty the cart
			// Remove all items from the shopping cart
			.getHeaderWidget().goToShoppingCartPage().removeAllItems().getHeaderWidget().openSignOutDropDownWidget().signOutB2B();
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

		
	}
	
	
	
	private void cartCleanup(MyAccountMainPage page)
	{		
			try {
				page.getHeaderWidget().verifyMiniCartItemCount("0");
			} catch (Exception e) {
				//cart isn't 0, cleaning up
				OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
				orders.removeAllItemsFromCart();
			}		
	}
}
