package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.MyRecuringOrdersPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.ProductDisplayPageB2B;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
/** 		
 * Scenario: FV2STOREB2C_52
 * Details: Shopper Completes a recurring Order 
 * Pre-requisites: 
 * 1. subscription feature should be enabled
 * 2. SubscriptionSchedulerCmc scheduler should be running. and interval should be less than one minute.
 * 3. Recurring item inventory should be available for this store.
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2B_18 extends  AbstractAuroraSingleSessionTests {

	@DataProvider
	private final TestDataProvider dsm;
	
		
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dataSetManager
	 * @param p_caslFixtures 
	 */
	@Inject
	public FV2STOREB2B_18(Logger log, CaslFoundationTestRule caslTestRule, WcWteTestRule wcWebTestRule, TestDataProvider dataSetManager)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
	}
	
	
	
	/**
	 * Registered user schedules a recurring Order 
	 * @throws Exception
	 */
	@Test
	public void FV2STOREB2B_1801() throws Exception{
		
		
		//opening the store front page
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
		
		//Sign In  into the store
		MyAccountMainPage myAccountPage =  frontPage.getHeaderWidget()
													.signIn()
													.typeUsername(dsm.getInputParameter("LOGON_USER"))	
													.typePassword(dsm.getInputParameter("LOGON_PASS"))
													.signIn()
													.goToMyAccount();
					
		
		//Go to department page
		CategoryPage subCat = myAccountPage.getFooterWidget().goToSiteMapPage()
															.goToCategoryPageByName(CategoryPage.class, dsm.getInputParameter("CATEGORY_NAME"));
		
		//Go to product display page
		ProductDisplayPageB2B productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByNameB2B(dsm.getInputParameter("PRODUCTNAME"));
		
		//update quantity
		productDisplayPage.getSkuListWidget().updateQuantity(dsm.getInputParameter("PRODUCTSKU"), dsm.getInputParameter("QTY"));
		
		//add to cart / current order
		productDisplayPage.addToCurrentOrder(dsm.getInputParameter("PRODUCTSKU"));
		
			
		//Go to the shopCart page and schedule this order as recurring order
		ShopCartPage shopCartPage = productDisplayPage.getHeaderWidget().goToShoppingCartPage().checkScheduleThisOrderCheckBox();
		
		//Go to the shipping billing page
		ShippingAndBillingPage shippingBillingPage = shopCartPage.continueToNextStep();
		
		//Getting the today date
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String dateNow = formatter.format(currentDate.getTime());
		  
		//Entering the order frequency and order start date.
		shippingBillingPage = shippingBillingPage.selectOrderFrequency(dsm.getInputParameter("ORDER_FREQUENCY")).typeStartDate(dateNow);
		
		//Entering the payment method and credit card details
		shippingBillingPage.selectPaymentMethod(dsm.getInputParameter("PAYMENTMETHOD")).typeCreditCardNumber(dsm.getInputParameter("CREDIT_CARD_NO"));
		
		//go the order summary page
		OrderSummarySingleShipPage orderSummaryPage = shippingBillingPage.singleShipNext();
		
		//Go to order confirmation page . verifying the recurring order got placed successfully
		OrderConfirmationPage orderConformationPage = orderSummaryPage.completeOrder().verifyScheduledOrderSuccessful();
		
		//Get order no from order confirmation page.
		String orderNumber = orderConformationPage.getScheduledOrderNumber();
		
		
		//Go to recurring order page
		MyRecuringOrdersPage recurringOrderPage = orderConformationPage.getHeaderWidget().goToMyAccount().getSidebar().goToRecurringOrdersPage();
		
		
		//verifying order no present in recurring order page.
		recurringOrderPage = recurringOrderPage.verifyOrdreNumberIsPresent(orderNumber);
		
		//verifying the recurring order status
		recurringOrderPage = recurringOrderPage.verifyOrderStatus("Active" , orderNumber);
			
		
		//Go to the recurring order details page
		recurringOrderPage.goToRecurringOrderDetailPage(orderNumber);
		
		recurringOrderPage.getHeaderWidget().openSignOutDropDownWidget().signOutB2B();
		
		
	}
		
	

	
		
	
}
