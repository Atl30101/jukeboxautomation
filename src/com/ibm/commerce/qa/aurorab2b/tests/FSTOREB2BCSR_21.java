package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.casl.rest.fixtures.utils.CaslRestFixtureModule;
import com.ibm.commerce.qa.aurora.page.CustomerServiceOrderSummaryPage;
import com.ibm.commerce.qa.aurora.page.MyOrderHistoryPage;
import com.ibm.commerce.qa.aurora.page.OrderDetailPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.CustomerServiceFindOrderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.OrderApprovalPage;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 * Scenario: FV2.STOREB2C.CSR_21
 * Details: Access the Customer Service Order Summary Page
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class, CaslRestFixtureModule.class})
public class FSTOREB2BCSR_21 extends AbstractAuroraSingleSessionTests
{

    /**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	private final CaslFixturesFactory f_CaslFixtures;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_CaslFixtures 
	 */	@Inject
	public FSTOREB2BCSR_21(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_CaslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		f_CaslFixtures = p_CaslFixtures;
		
	}
	 
	 /** 
	  * Customer Service Representative can access Order Summary Page for a completed order on B2Bstorefront 
	  *  which requires order approval 
	  */
	 @Test
	 public void testFSTOREB2BCSR_2108()
	 {
				//Place an Order as a buyer
				OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("BUYER_LOGONID"), dsm.getInputParameter("BUYER_PASSWORD"), getConfig().getStoreName());
				//add item to cart
				orders.addItem(dsm.getInputParameter("SKU1"), dsm.getInputParameterAsNumber("QTY", Double.class));
				orders.addItem(dsm.getInputParameter("SKU2"), dsm.getInputParameterAsNumber("QTY", Double.class));
				orders.addItem(dsm.getInputParameter("SKU3"), dsm.getInputParameterAsNumber("QTY", Double.class));
				//get OrderId
				String orderId = orders.getCurrentOrderId();
				
				//Add payment and complete Order
				orders.addPayLaterPaymentMethod();
				//Unique PO Number
				 long random = Math.round(Math.random());
			     
			    //Complete 
				orders.completeOrderWithPurchaseOrderNumber(Long.toString(random));

				//Open the store in the browser
				AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
						
				//Opens the Sign In page in browser.
				SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
				
				//Login as CSR
				signIn.typeUsername(dsm.getInputParameter("CSS_LOGONID")).typePassword(dsm.getInputParameter("CSS_PASSWORD"))
					.signIn();
				
				//Find Completed Order
				CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
				findOrderWidget.typeOrderNumber(orderId).submitSearch();
				findOrderWidget.verifyOrderSearchResultIsDisplayed();
				
				CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget.clickActionButton(orderId).clickOrderSummary();
				//Verify Order is correct
				orderSummaryPage.verifyOrderId(orderId);
				orderSummaryPage.getOrderSummary().verifyOrderedBy(dsm.getInputParameter("BUYER_NAME"));	
				orderSummaryPage.getOrderSummary().verifyStatus(dsm.getInputParameter("ORDER_STATUS"));
				orderSummaryPage.getOrderTable().toggleOrderSummary().verifyItemPresent(dsm.getInputParameter("SKU1"));
			    
				orderSummaryPage.getComments().toggleOrderComments().getWriteAndDisplayCommentsWidgets().addNewComments(dsm.getInputParameter("COMMENT"));
				Assert.assertTrue("Cancel Order button is not visible on page !", orderSummaryPage.isCancelButtonVisibleOnPage());
				
				//go to Order Detail Page and view comments slider
				findOrderWidget = orderSummaryPage.getSidebarWidget().gotoFindOrderPage().getFindOrder();
				findOrderWidget.typeOrderNumber(orderId).submitSearch();
				OrderDetailPage orderDetailPg = findOrderWidget.clickOrderID(orderId);
				orderDetailPg.getCommentsBar().slideOrderComments().verifyCommentPresent(dsm.getInputParameter("COMMENT"));
				MyOrderHistoryPage ordersPg = orderDetailPg.getHeaderWidget().goToMyAccount().getSidebar().goToOrderHistoryPage();
				ordersPg.clickWaitingForApprovalTab();
				orderDetailPg = ordersPg.goToOrderDetailPageWithOrderId(orderId);
				orderDetailPg.getHeaderWidget().openSignOutDropDownWidget().signInAsYourself();
	 }
	/** 
	  * Customer Service Representative can access Order Summary Page for a completed order on B2Bstorefront 
	  *  which was rejected during order approval 
	  */
	 @Test
	 public void testFSTOREB2BCSR_2109()
	 {
				//Place a Subscription Order as a shopper
				OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("BUYER_LOGONID"), dsm.getInputParameter("BUYER_PASSWORD"), getConfig().getStoreName());
				//add item to cart
				orders.addItem(dsm.getInputParameter("SKU1"), dsm.getInputParameterAsNumber("QTY", Double.class));
				orders.addItem(dsm.getInputParameter("SKU2"), dsm.getInputParameterAsNumber("QTY", Double.class));
				orders.addItem(dsm.getInputParameter("SKU3"), dsm.getInputParameterAsNumber("QTY", Double.class));
				//get OrderId
				String orderId = orders.getCurrentOrderId();
				
				//Add payment and complete Order
				orders.addPayLaterPaymentMethod();
				//Unique PO Number
				 long random = Math.round(Math.random());
			     
			    //Complete 
				orders.completeOrderWithPurchaseOrderNumber(Long.toString(random));
	
				//Open the store in the browser
				AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
						
				//Opens the Sign In page in browser.
				SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
				
				//Login as buyerAdmin to reject Order
				signIn.typeUsername(dsm.getInputParameter("BUYERADMIN_LOGONID")).typePassword(dsm.getInputParameter("BUYERADMIN_PASSWORD"))
				.signIn();
				OrderApprovalPage orderApprovalPg = frontPage.getHeaderWidget().goToMyAccount().getSidebar().goToOrderApprovalPage();
				orderApprovalPg.getOrderApprovalWidget().rejectOrderByOrderId(orderId);
				signIn = orderApprovalPg.getHeaderWidget().openSignOutDropDownWidget().signOutB2B().signIn();
				
				//Login as CSR
				signIn.typeUsername(dsm.getInputParameter("CSR_LOGONID")).typePassword(dsm.getInputParameter("CSR_PASSWORD"))
					.signIn();
				
				//Find Completed Order
				CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
				findOrderWidget.typeOrderNumber(orderId).submitSearch();
				findOrderWidget.verifyOrderSearchResultIsDisplayed();
				
				CustomerServiceOrderSummaryPage orderSummaryPage = findOrderWidget.clickActionButton(orderId).clickOrderSummary();
				//Verify Order is correct
				orderSummaryPage.verifyOrderId(orderId);
				orderSummaryPage.getOrderSummary().verifyOrderedBy(dsm.getInputParameter("BUYER_NAME"));	
				orderSummaryPage.getOrderSummary().verifyStatus(dsm.getInputParameter("ORDER_STATUS"));
				orderSummaryPage.getOrderTable().toggleOrderSummary().verifyItemPresent(dsm.getInputParameter("SKU1"));
			    
				orderSummaryPage.getComments().toggleOrderComments().getWriteAndDisplayCommentsWidgets().addNewComments(dsm.getInputParameter("COMMENT"));
				Assert.assertTrue("Cancel Order button is not visible on page !", orderSummaryPage.isCancelButtonVisibleOnPage());
				
				//go to Order Detail Page and view comments slider
				findOrderWidget = orderSummaryPage.getSidebarWidget().gotoFindOrderPage().getFindOrder();
				findOrderWidget.typeOrderNumber(orderId).submitSearch();
				OrderDetailPage orderDetailPg = findOrderWidget.clickOrderID(orderId);
				orderDetailPg.getCommentsBar().slideOrderComments().verifyCommentPresent(dsm.getInputParameter("COMMENT"));
				MyOrderHistoryPage ordersPg = orderDetailPg.getHeaderWidget().goToMyAccount().getSidebar().goToOrderHistoryPage();
				String order[] = new String [1];
				order[0] = orderId;
				String orderStatus [] = new String [1];
				orderStatus[0] = dsm.getInputParameter("ORDER_STATUS");
				
				ordersPg.verifyOrderNumberSequence(order, orderStatus);
				ordersPg.getHeaderWidget().openSignOutDropDownWidget().signInAsYourself();
	 }

	/**
		 * Tear down ran after every test case
		 */
		@After
		public void tearDown(){
			
			 // do nothing

		}
}