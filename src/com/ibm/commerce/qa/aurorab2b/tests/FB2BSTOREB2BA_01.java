package com.ibm.commerce.qa.aurorab2b.tests;

import java.util.logging.Logger;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.BundleDisplayPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.KitDisplayPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.MyAccountSidebarWidget;
import com.ibm.commerce.qa.aurora.widget.RequisitionListAddItemSelectionWidget;
import com.ibm.commerce.qa.aurora.widget.RequisitionListsWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurora.widget.impl.RealRequisitionListItemsWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.CustomerRegisterationPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.ProductDisplayPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.RequisitionListDetailsPage;
import com.ibm.commerce.qa.aurorab2b.page.RequisitionListsPage;
import com.ibm.commerce.qa.aurorab2b.page.SignInPageB2B;
import com.ibm.commerce.qa.casl.util.CaslModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.Accelerator;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.url.DeltaUpdates;
import com.ibm.commerce.qa.url.OrgAdminConsole;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;

/**
 * Scenario: FSSC-92726-01
 * Details: Requisition list test scenario
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class, CaslModule.class})
public class FB2BSTOREB2BA_01 extends AbstractAuroraSingleSessionTests {

	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	

	private final OrgAdminConsole oac;
	
	private final Accelerator accelerator;
	
	private final CMC cmc;

	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider f_dsm;

	private String organizationName;

	private String buyerUsername;
	
	private String contractId;
	
	private String accountId;
	
	private DeltaUpdates deltaUpdate;
	
	/**
	 * Test Class object constructor.
	 * 
	 */
	@Inject
	public FB2BSTOREB2BA_01(
			Logger log, 
			WcWteTestRule wcWteTestRule,
			CaslFoundationTestRule caslTestRule,
			TestDataProvider p_dsm,
			OrgAdminConsole p_oac,
			Accelerator accelerator,
			CMC cmc,
			DeltaUpdates deltaUpdate) throws Exception
	{
		super(log, wcWteTestRule, caslTestRule);
		
		f_dsm = p_dsm;
		oac = p_oac;
		this.accelerator = accelerator;
		this.cmc = cmc;
		this.deltaUpdate = deltaUpdate;
	}
	
	/**
	 * Helper method to create a new contract
	 * 
	 * @param organization
	 * @return the name of the new contract
	 * @throws Exception
	 */
	private String createContract(String organization) throws Exception
	{
		f_dsm.setDataLocation("FB2BSTOREB2BA_01_02", "FB2BSTOREB2BA_01_02");
				
		String [] percentOff = {f_dsm.getInputParameter("percentOff1_1"), 
				f_dsm.getInputParameter("percentOff1_2")};
		
		String [] requiredCatalogs = {f_dsm.getInputParameter("requiredCatalogs1_1"), 
				f_dsm.getInputParameter("requiredCatalogs1_2")};
		
		//Tell which test case to use for input parameters
		f_dsm.setDataLocation("FB2BSTOREB2BA_01_02", "Accelerator");
		
		//logs on to the accelerator.
		accelerator.logon(f_dsm.getInputParameter("ADMIN_LOGON_ID"),f_dsm.getInputParameter("ADMIN_PASSWORD"));
		
		//Tell which test case to use for input parameters
		f_dsm.setDataLocation("FB2BSTOREB2BA_01_02", "createAccount");
		
		//Create a new Account for Organization.
		accountId = accelerator.getOrganizationAccountNo(organization);
		
		//gets a new unique contract name.
		String contractName = RandomStringUtils.randomAlphanumeric(8);
		
		//Tell which test case to use for input parameters
		f_dsm.setDataLocation("FB2BSTOREB2BA_01_02", "createContract");
		
		//gets all available shipping modes.
		String[] shippingModes = accelerator.getShippingModes();
		
		//gets all available shipping charges.
		String[] shippingCharges = accelerator.getShippingCharges();
		
		//creates new contract for the account created in previous steps.
		contractId = accelerator.createNewContract(contractName, organization, 
				f_dsm.getInputParameter("INCLUDE_ENTIRE_CATALOG"),
				f_dsm.getInputParameter("PERCENTAGE_OFF_ON_ENTIRE_CATALOG"), 
				requiredCatalogs, percentOff, shippingModes, shippingCharges);
		
	
		//submit the contract;
		accelerator.submitContract(accountId, contractId);
		
		//log out from accelerator.
		accelerator.logoff();
		
		return contractName;
				
	}
	
	/**
	 * Helper method to create a requisition list containing a single item from
	 * product display page.
	 * 
	 * @param header
	 * @return a new RequisitionListsPage object.
	 */
	private RequisitionListsPage createRequisitionListWithSingleItem(HeaderWidget header)
	{
		//Go to product display page
		CategoryPage departmentPage = header.goToDepartmentByName(CategoryPage.class, 
				f_dsm.getInputParameter("DEPARTMENT"));

		//click on product image on Category Display page.
		ProductDisplayPageB2B pdp = departmentPage.getCatalogEntryListWidget()
				
					.goToProductPageByNameB2B(f_dsm.getInputParameter("PRODUCT"));
		
		pdp.getSkuListWidget().updateQuantity(f_dsm.getInputParameter("SKU"), f_dsm.getInputParameter("QUANTITY"));
		
		//click on add to requisition list button.
		RequisitionListAddItemSelectionWidget reqSelection = pdp.addToRequisitionList();
		
		//enter requisition list data.
		reqSelection.selectCreateNewReqList()
		
				.typeReqListName(f_dsm.getInputParameter("REQUISITION_LIST_NAME"))
				
				.selectReqType(f_dsm.getInputParameter("REQUISITION_LIST_TYPE"))
				
				.addToReqList()
				
				.selectContinueShopping();
		
		//Go to my account page.
		MyAccountMainPage myAccountPage = pdp.getHeaderWidget().goToMyAccount();
		
		//Go to requisition list page from my account side bar.
		RequisitionListsPage reqListPage = myAccountPage.getSidebar()
				
				.goToRequisitionListPage();
		
		return reqListPage;
	}

	/**
	 * Setup script to create an organization and buyer
	 * 
	 * @throws Exception
	 */
	@Before
	public void setup() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPanel.registerB2B();

		String adminUsername = f_dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID") + System.currentTimeMillis();

		organizationName = f_dsm.getInputParameter("ORGANIZATION_NAME") + System.currentTimeMillis();

		buyerUsername = f_dsm.getInputParameter("BUYER_LOGONID") + System.currentTimeMillis();
		
		SignInPageB2B singInPage = rp.selectOrganizationRegister()

				//type organization name
				.typeOrganizationName(organizationName)
		
				//type organization address
				.typeOrganizationStreetAddressLine1(f_dsm.getInputParameter("ORGANIZATION_ADDRESS"))
		
				//select organization country
				.selectOrganizationCountryOrRegion(f_dsm.getInputParameter("ORGANIZATION_COUNTRY"))
		
				//select organization province
				.selectOrganizationStateOrProvince(f_dsm.getInputParameter("ORGANIZATION_STATE"))
		
				//type organization city
				.typeOrganizationCity(f_dsm.getInputParameter("ORGANIZATION_CITY"))
		
				//type organization zipcode
				.typeOrganizationZipCode(f_dsm.getInputParameter("ORGANIZATION_ZIPCODE"))
		
				//type organization email
				.typeOrganizationEmail(f_dsm.getInputParameter("ORGANIZATION_EMAIL"))
		
				//type organization phone number
				.typeOrganizationPhoneNumber(f_dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))
		
				//type buyer username
				.typeOrganizationBuyerLogonId(adminUsername)
		
				//type buyer password
				.typeOrganizationBuyerPassword(f_dsm.getInputParameter("PASSWORD"))
		
				//Verify buyer password verify
				.typeOrganizationBuyerVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))
		
				//type buyer first name
				.typeOrganizationBuyerFirstName(f_dsm.getInputParameter("FIRST_NAME"))
		
				//type buyer last name
				.typeOrganizationBuyerLastName(f_dsm.getInputParameter("LAST_NAME"))
		
				//type buyer street address
				.typeOrganizationBuyerStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))
		
				//Select buyer country
				.selectOrganizationBuyerCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))
		
				//type or select state for buyer
				.selectOrganizationBuyerStateOrProvince(f_dsm.getInputParameter("STATE"))
		
				//type buyer city
				.typeOrganizationBuyerCity(f_dsm.getInputParameter("CITY"))
		
				//type buyer zipcode
				.typeOrganizationBuyerZipCode(f_dsm.getInputParameter("ZIPCODE"))
		
				//type buyer E-mail
				.typeOrganizationBuyerEmail(f_dsm.getInputParameter("EMAIL"))
		
				//type buyer home phone number
				.typeOrganizationBuyerPhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))
		
				//Check if preferred language drop down is visible
				.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			
		
				//Select buyer preferred language
				.selectOrganizationBuyerPreferedCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))
		
				//Check if preferred currency drop down is visible
				.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()
		
				//Select buyer preferred language
				.selectOrganizationBuyerPreferedLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))
		
				//Submit registration
				.submitOrganizationBuyerRegistration()
		
				//Verify registration confirmation message
				.verifyOrganizationBuyerRegistrationConfirmationMessage();

		rp = singInPage.register();
		
		rp.selectBuyerRegister()
		
				//type buyer username
				.typeLogonId(buyerUsername)
				
				//type organization name
				.typeOrganization(organizationName)
				
				//type buyer password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				//type buyer verify password
				.typeVerifyPassword(f_dsm.getInputParameter("PASSWORD"))
				
				//type buyer first name
				.typeFirstName(f_dsm.getInputParameter("FIRST_NAME"))
				
				//type buyer last name
				.typeLastName(f_dsm.getInputParameter("LAST_NAME"))
				
				//type organization address
				.typeStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))
				
				//type organization city
				.typeCity(f_dsm.getInputParameter("CITY"))
				
				//select country for buyer
				.selectCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))
				
				//select state or province for buyer
				.selectStateOrProvince(f_dsm.getInputParameter("STATE"))
				
				//type zipcode for buyer
				.typeZipCode(f_dsm.getInputParameter("ZIPCODE"))
				
				//type email
				.typeEmail(f_dsm.getInputParameter("EMAIL"))
				
				.submit();
		
		// Approve Organization
		oac.logon(f_dsm.getInputParameter("ACCELERATOR_LOGON_ID"), f_dsm.getInputParameter("ACCELERATOR_PASSWORD"));
		oac.approveAllApprovals();
	}

	/**
	 * Test case create a shared requisition list for a buyer who belongs to multiple
	 * contracts.
	 */
	@Test
	public void FB2BSTOREB2BA_01_02() throws Exception
	{
		accelerator.logon(f_dsm.getInputParameter("ADMIN_LOGONID"), f_dsm.getInputParameter("ADMIN_PASSWORD"));
		accelerator.createNewAccount(organizationName, null, null, false);
		
		//Create two new contracts
		createContract(organizationName);
		createContract(organizationName);
		
		f_dsm.setDataLocation("FB2BSTOREB2BA_01_02", "FB2BSTOREB2BA_01_02");
		
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel

				.typeUsername(buyerUsername)
				
				.typePassword(f_dsm.getInputParameter("BUYER_PASSWORD"))
				
				//Click Sign In
				.signInWithoutVerify(HeaderWidget.class);

		//go to My Account page
		MyAccountMainPage myAccountPage = header.goToMyAccount();

		MyAccountSidebarWidget myAccountSidebar = myAccountPage.getSidebar();

		//Go to requisition list page from my account side bar.
		RequisitionListsPage reqListPage = myAccountSidebar.goToRequisitionListPage();

		RequisitionListsWidget reqListWidget = reqListPage.getRequistionListsWidget();

		//enter requisition list data.
		reqListWidget.goToNewRequistionListForm()
				.selectRequistionType(f_dsm.getInputParameter("REQUISITION_LIST_TYPE"))

				.typeNewRequistionListName(f_dsm.getInputParameter("REQUISITION_LIST_NAME"))
		
				.saveNewRequistionList();
		
		//Verify new requisition list exists on requisition list page
		reqListWidget.verifyRequistionList(f_dsm.getInputParameter("REQUISITION_LIST_NAME"));

	}
	
	/**
	 * Test case create new private requisition list.
	 */
	@Test
	public void FB2BSTOREB2BA_01_03() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel

				//Enter a valid password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				.typeUsername(buyerUsername)
				
				//Click Sign In
				.signIn(HeaderWidget.class);

		//go to My Account page
		MyAccountMainPage myAccountPage = header.goToMyAccount();

		MyAccountSidebarWidget myAccountSidebar = myAccountPage.getSidebar();

		//Go to requisition list page from my account side bar.
		RequisitionListsPage reqListPage = myAccountSidebar.goToRequisitionListPage();

		RequisitionListsWidget reqListWidget = reqListPage.getRequistionListsWidget();

		//enter requisition list data.
		reqListWidget.goToNewRequistionListForm()

				.typeNewRequistionListName(f_dsm.getInputParameter("REQUISITION_LIST_NAME"))
		
				.selectRequistionType(f_dsm.getInputParameter("REQUISITION_LIST_TYPE"))
		
				.saveNewRequistionList();

		//Verify new requisition list exists on requisition list page
		reqListWidget.verifyRequistionList(f_dsm.getInputParameter("REQUISITION_LIST_NAME"));
	}

	/**
	 * Test case delete requisition list.
	 */
	@Test
	public void FB2BSTOREB2BA_01_06() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel.typeUsername(buyerUsername)

				//Enter a valid password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				//Click Sign In
				.signIn(HeaderWidget.class);

		//Go to My Account page
		MyAccountMainPage myAccountPage = header.goToMyAccount();

		MyAccountSidebarWidget myAccountSidebar = myAccountPage.getSidebar();

		//Go to requisition list page from my account side bar.
		RequisitionListsPage reqListPage = myAccountSidebar.goToRequisitionListPage();

		RequisitionListsWidget reqListWidget = reqListPage.getRequistionListsWidget();

		//Enter requisition list data.
		reqListWidget.goToNewRequistionListForm()

				.typeNewRequistionListName(f_dsm.getInputParameter("REQUISITION_LIST_NAME"))
		
				.selectRequistionType(f_dsm.getInputParameter("REQUISITION_LIST_TYPE"))
		
				.saveNewRequistionList();
		
		//Verify new requisition list exists on requisition list page
		reqListWidget.verifyRequistionList(f_dsm.getInputParameter("REQUISITION_LIST_NAME"));

		//Delete new requisition list
		reqListWidget.deleteRequistionList(f_dsm.getInputParameter("REQUISITION_LIST_POS"));
		
		//Verify new requisition list does not exist
		reqListWidget.verifyRequistionListNotPresent(f_dsm.getInputParameter("REQUISITION_LIST_NAME"));
	}

	/**
	 * Buyer updates a requisition list by removing an item from the requisition list.
	 */
	@Test
	public void FB2BSTOREB2BA_01_07() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel.typeUsername(buyerUsername)

				//Enter a valid password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				//Click Sign In
				.signIn(HeaderWidget.class);

		RequisitionListsPage reqListPage = createRequisitionListWithSingleItem(header);

		//Go to requisition list details page 
		RequisitionListDetailsPage reqListDetailsPage = reqListPage.getRequistionListsWidget()
			
				.goToRequisitionListDetails(f_dsm.getInputParameter("REQUISITION_LIST_POS"));
		
		//Delete the requisition list and verify requisition page is empty
		reqListDetailsPage
			
				.getRequisitionListItemsWidget()
			
				.deleteItemByPos(f_dsm.getInputParameter("REQUISITION_LIST_ITEM_POS"))
				
				.verifyReqListIsEmpty();
	}
	
	/**
	 * Test case create duplicate requisition list.
	 */
	@Test
	public void FB2BSTOREB2BA_01_10() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel.typeUsername(buyerUsername)

				//Enter a valid password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				//Click Sign In
				.signIn(HeaderWidget.class);

		//go to My Account page
		MyAccountMainPage myAccountPage = header.goToMyAccount();

		MyAccountSidebarWidget myAccountSidebar = myAccountPage.getSidebar();

		//Go to requisition list page from my account side bar.
		RequisitionListsPage reqListPage = myAccountSidebar.goToRequisitionListPage();

		RequisitionListsWidget reqListWidget = reqListPage.getRequistionListsWidget();

		//Enter requisition list data.
		reqListWidget.goToNewRequistionListForm()

				.typeNewRequistionListName(f_dsm.getInputParameter("REQUISITION_LIST_NAME"))
		
				.selectRequistionType(f_dsm.getInputParameter("REQUISITION_LIST_TYPE"))
		
				.saveNewRequistionList();
		
		//Verify new requisition list exists on requisition list page.
		reqListWidget.verifyRequistionList(f_dsm.getInputParameter("REQUISITION_LIST_NAME"));

		//Click add duplicate list.
		reqListWidget.addDuplicateList(f_dsm.getInputParameter("REQUISITION_LIST_POS"));
	}
	
	/**
	 * Create a new requisition list from bundle details page.
	 * 
	 * @throws Exception
	 */
	@Test
	public void FB2BSTOREB2BA_01_13() throws Exception
	{
		TeardownTask task = new TeardownTask() {
			
			String bundleId = null;
			
			@Override
			void runPreSetup() throws Exception {
			
				//login to CMC as admin
				cmc.logon(f_dsm.getInputParameter("ADMIN_LOGONID"), f_dsm.getInputParameter("ADMIN_PASSWORD"));
				cmc.setLocale(f_dsm.getInputParameter("LOCALE"));
				
				//select store and 
				cmc.selectStore();
				String masterCatalogId = cmc.selectCatalog();
				
				//opening catalog management tool in cmc
				String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
				
				//creating a bundle
				f_dsm.setDataLocation("FB2BSTOREB2BA_01_13", "CMCCreateBundle");
				
				try
				{
					bundleId = cmc.createProduct(f_dsm.getInputParameter("PARENT_CATEGORY"), f_dsm.getInputParameter("PART_NUMBER"), 
							f_dsm.getInputParameter("PRODUCT_NAME"), f_dsm.getInputParameter("PUBLISHED"));
				}catch (IllegalStateException e)
				{
					bundleId = cmc.getCatalogEntryId(f_dsm.getInputParameter("PART_NUMBER"));
					cmc.deleteCatalogEntry(bundleId);
					bundleId = cmc.createProduct(f_dsm.getInputParameter("PARENT_CATEGORY"), f_dsm.getInputParameter("PART_NUMBER"), 
							f_dsm.getInputParameter("PRODUCT_NAME"), f_dsm.getInputParameter("PUBLISHED"));
				}
				System.out.println("Bundle ID: " + bundleId);
				
				//creating first component
				f_dsm.setDataLocation("FB2BSTOREB2BA_01_13", "CMCcreateComponent");
				cmc.createComponent(f_dsm.getInputParameter("PARENT_CATENTRY_PART_NUMBER"), f_dsm.getInputParameter("COMPONENT_PART_NUMBER"), 
						f_dsm.getInputParameter("QUANTITY"), f_dsm.getInputParameter("SEQUENCE"));
		
				//Wait until delta update completes before timeout
				deltaUpdate.beforeWaitForDelta(masterCatalogId);
				getSession().startAtStorePreviewPage(previewURL, AuroraFrontPageB2B.class);
				deltaUpdate.waitForDelta(masterCatalogId, 120);
			}
		
			@Override
			void runTaskProcedure() throws Exception {
			
				f_dsm.setDataLocation("FB2BSTOREB2BA_01_13", "FB2BSTOREB2BA_01_13");
		
				//Open AuroraB2BEsite store
				AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
				
				//Click on the SignIn page link on the header
				SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();
		
				//Enter a valid username
				HeaderWidget header = signInPanel.typeUsername(buyerUsername)
		
						//Enter a valid password
						.typePassword(f_dsm.getInputParameter("PASSWORD"))
		
						//Click Sign In
						.signIn(HeaderWidget.class);
				
				//Go to product display page
				CategoryPage departmentPage = header.goToDepartmentByName(CategoryPage.class, f_dsm.getInputParameter("DEPARTMENT"));
		
				//click on product image on Category Display page.
				BundleDisplayPage bundleDisplayPage = departmentPage.getCatalogEntryListWidget()
						
						.goToBundleDisplayPageByName(f_dsm.getInputParameter("PRODUCT"));

				//Select the product attributes	for both items in bundle
				bundleDisplayPage.getComponentsWidget()
						
						.selectAttributeFromDropdown(f_dsm.getInputParameter("ITEM_BUNDLE"), 
								f_dsm.getInputParameter("ITEM_ATTR_NAME_1"), f_dsm.getInputParameter("ITEM_ATTR_VALUE_1"))
						
						.selectAttributeFromDropdown(f_dsm.getInputParameter("ITEM_BUNDLE"), 
								f_dsm.getInputParameter("ITEM_ATTR_NAME_2"), f_dsm.getInputParameter("ITEM_ATTR_VALUE_2"))
						
						.selectAttributeFromDropdown(f_dsm.getInputParameter("ITEM_BUNDLE"), 
								f_dsm.getInputParameter("ITEM_ATTR_NAME_3"), f_dsm.getInputParameter("ITEM_ATTR_VALUE_3"));
				
				//Click on add to requisition list button.
				RequisitionListAddItemSelectionWidget reqSelection = bundleDisplayPage.getShopperActionsWidget()
							
						.clickAddToRequisitionList();
				
				//Enter requisition list data and continue shopping.
				reqSelection.selectCreateNewReqList()
				
					.typeReqListName(f_dsm.getInputParameter("REQUISITION_LIST_NAME"))
					
					.selectReqType(f_dsm.getInputParameter("REQUISITION_LIST_TYPE"))
					
					.addToReqList()
					
					.selectContinueShopping();
			}

			@Override
			void runTeardown() throws Exception
			{
				if(bundleId!=null)
				{
					cmc.deleteCatalogEntry(bundleId);
				}
				cmc.logoff();
			}
		};
		task.start();
	}
	
	/**
	 * Create a new requisition list from kit details page.
	 * 
	 * @throws Exception
	 */
	@Test
	public void FB2BSTOREB2BA_01_14() throws Exception
	{
		
		TeardownTask task = new TeardownTask() {
			
			boolean loggedIntoCMC = false;
			String kitId = null;
			
			@Override
			void runPreSetup() throws Exception
			{
				//login to CMC as admin
				cmc.logon(f_dsm.getInputParameter("ADMIN_LOGONID"), f_dsm.getInputParameter("ADMIN_PASSWORD"));
				loggedIntoCMC = true;
				cmc.setLocale(f_dsm.getInputParameter("LOCALE"));
				
				//select store and 
				cmc.selectStore();
				String masterCatalogId = cmc.selectCatalog();
				
				//creating a kit
				f_dsm.setDataLocation("FB2BSTOREB2BA_01_14", "CMCCreateKit");
				kitId = cmc.createProduct(f_dsm.getInputParameter("PARENT_CATEGORY"), f_dsm.getInputParameter("PART_NUMBER"), 
						f_dsm.getInputParameter("PRODUCT_NAME"), f_dsm.getInputParameter("PUBLISH"));
				
				//creating first component
				f_dsm.setDataLocation("FB2BSTOREB2BA_01_14", "CMCcreateComponent");
				cmc.createComponent(f_dsm.getInputParameter("PARENT_CATENTRY_PART_NUMBER"), f_dsm.getInputParameter("COMPONENT_PART_NUMBER"), 
						f_dsm.getInputParameter("QUANTITY"), f_dsm.getInputParameter("SEQUENCE"));
				
				//Creating list price for this kit
				f_dsm.setDataLocation("FB2BSTOREB2BA_01_14", "CMCListPrice");
				cmc.createListPriceForCatalogEntry(kitId);
		
				//Creating offer price for this kit
				f_dsm.setDataLocation("FB2BSTOREB2BA_01_14", "CMCOfferPrice");
				cmc.createOfferPriceForCatalogEntry(kitId);	
				
				//temporarily log off for accelerator
				cmc.logoff();
				loggedIntoCMC = false;
		
				//Login to Accelerator, create inventory for kit, then log off
				f_dsm.setDataLocation("FB2BSTOREB2BA_01_14", "FB2BSTOREB2BA_01_14");
				accelerator.logon(f_dsm.getInputParameter("ADMIN_LOGONID"), f_dsm.getInputParameter("ADMIN_PASSWORD"));
				
				f_dsm.setDataLocation("FB2BSTOREB2BA_01_14", "CMCCreateKit");
				accelerator.addAdHocReceiptForSKU(f_dsm.getInputParameter("PART_NUMBER"), 
						f_dsm.getInputParameter("INVENTORY_QTY"), 
						f_dsm.getInputParameter("COST"), 
						f_dsm.getInputParameter("CURRENCY"));
				
				accelerator.logoff();
				
				//log back into CMC
				f_dsm.setDataLocation("FB2BSTOREB2BA_01_14", "FB2BSTOREB2BA_01_14");
				cmc.logon(f_dsm.getInputParameter("ADMIN_LOGONID"), f_dsm.getInputParameter("ADMIN_PASSWORD"));
				cmc.setLocale(f_dsm.getInputParameter("LOCALE"));	
				cmc.selectStore();
				cmc.selectCatalog();
				loggedIntoCMC = true; //flag for teardown
				
				//opening catalog management tool in cmc
				String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
				
				//Use store prview to build index. Wait until delta update completes before timeout
				deltaUpdate.beforeWaitForDelta(masterCatalogId);
				getSession().startAtStorePreviewPage(previewURL, AuroraFrontPageB2B.class);
				deltaUpdate.waitForDelta(masterCatalogId, 120);
			}
			
			@Override
			void runTaskProcedure() throws Exception
			{
				f_dsm.setDataLocation("FB2BSTOREB2BA_01_14", "FB2BSTOREB2BA_01_14");
				
				//Open AuroraB2BEsite store
				AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
				
				//Click on the SignIn page link on the header
				SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();
		
				//Enter a valid username
				HeaderWidget header = signInPanel.typeUsername(buyerUsername)
		
						//Enter a valid password
						.typePassword(f_dsm.getInputParameter("PASSWORD"))
		
						//Click Sign In
						.signIn(HeaderWidget.class);
				
				//Go to product display page
				CategoryPage departmentPage = header.goToCategoryPageByHierarchy(CategoryPage.class, 
						f_dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
						f_dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"));
		
				//Go to product display page
				KitDisplayPage kitDisplayPage = departmentPage.getCatalogEntryListWidget()
						
						.goToKitPagebyIdentifier(f_dsm.getInputParameter("PRODUCT"));
				
				//Click on add to requisition list button.
				RequisitionListAddItemSelectionWidget reqSelection = kitDisplayPage.getShopperActionsWidget()
							
						.clickAddToRequisitionList();
				
				//Enter requisition list data and continue shopping.
				reqSelection.selectCreateNewReqList()
				
						.typeReqListName(f_dsm.getInputParameter("REQUISITION_LIST_NAME"))
						
						.selectReqType(f_dsm.getInputParameter("REQUISITION_LIST_TYPE"))
						
						.addToReqList()
						
						.selectContinueShopping();
			}
			
			@Override
			void runTeardown() throws Exception
			{
				//An error occured during accelerator. Need to re-login to CMC
				if(!loggedIntoCMC)
				{
					cmc.logon(f_dsm.getInputParameter("ADMIN_LOGONID"), f_dsm.getInputParameter("ADMIN_PASSWORD"));
					cmc.setLocale(f_dsm.getInputParameter("LOCALE"));	
					cmc.selectStore();	
					cmc.selectCatalog();
				}
				
				//delete test kit
				if(kitId!=null)
				{
					cmc.deleteCatalogEntry(kitId);
				}				
				cmc.logoff();
			}
		};
		task.start();
	}

	/**
	 * Test case checking pagination of requisition lists.
	 */
	@Test
	public void FB2BSTOREB2BA_01_19() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel.typeUsername(buyerUsername)

				//Enter a valid password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				//Click Sign In
				.signInWithoutVerify(HeaderWidget.class);

		//go to My Account page
		MyAccountMainPage myAccountPage = header.goToMyAccount();

		MyAccountSidebarWidget myAccountSidebar = myAccountPage.getSidebar();

		//Go to requisition list page from my account side bar.
		RequisitionListsPage reqListPage = myAccountSidebar.goToRequisitionListPage();

		RequisitionListsWidget reqListWidget = reqListPage.getRequistionListsWidget();

		//create 14 requisition lists
		for (int i = 0; i < 14; i++) {

			String reqlist = f_dsm.getInputParameter("REQUISITION_LIST_NAME") + System.currentTimeMillis();

			//enter requisition list data.
			reqListWidget.goToNewRequistionListForm()

					.typeNewRequistionListName(reqlist)
		
					.selectRequistionType(f_dsm.getInputParameter("REQUISITION_LIST_TYPE"))
		
					.saveNewRequistionList();
			
			//verify new requisition list exists on requisition list page
			reqListWidget.verifyRequistionList(reqlist);
		}

		//Click next page button.
		reqListWidget.goToNextPage();

		//Click previouse page button.
		reqListWidget.goToPreviousPage();
	}

	/**
	 * Test case checking pagination of requisition lists.
	 */
	@Test
	public void FB2BSTOREB2BA_01_20() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel.typeUsername(buyerUsername)

				//Enter a valid password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				//Click Sign In
				.signIn(HeaderWidget.class);

		//Go to My Account page
		MyAccountMainPage myAccountPage = header.goToMyAccount();

		MyAccountSidebarWidget myAccountSidebar = myAccountPage.getSidebar();

		//Go to requisition list page from my account side bar.
		RequisitionListsPage reqListPage = myAccountSidebar.goToRequisitionListPage();

		RequisitionListsWidget reqListWidget = reqListPage.getRequistionListsWidget();
		
		//Enter requisition list data.
		reqListWidget.goToNewRequistionListForm()
		
				.typeNewRequistionListName(f_dsm.getInputParameter("REQUISITION_LIST_NAME"))
				
				.selectRequistionType(f_dsm.getInputParameter("REQUISITION_LIST_TYPE"))
				
				.saveNewRequistionList();
		
		//Go to requisition list details page
		RequisitionListDetailsPage reqListDetails = reqListWidget
				
				.goToRequisitionListDetails(f_dsm.getInputParameter("REQUISITION_LIST_POS"));

		RealRequisitionListItemsWidget reqListItemsWidget = reqListDetails.getRequisitionListItemsWidget();

		//create 14 requisition lists
		for (int i = 0; i < 14; i++)
		{

			//Add new item to requisition list and verify popup.
			reqListItemsWidget.typeSkuValue(f_dsm.getInputParameter("SKU_NUMBER"))
			
					.typeQuantity(f_dsm.getInputParameter("QUANTITY"))
				
					.addItemToList()
					
					.verifyItemAddedSuccessPopup();
		}

		//Click next page button.
		reqListItemsWidget.goToNextpage();

		//Click previouse page button.
		reqListItemsWidget.goToPreviouspage();
	}
	
	/**
	 * Buyer adds a shared requisition list created by another buyer to current
	 * order from requisition list page.
	 */
	@Test
	public void FB2BSTOREB2BA_01_21() throws Exception
	{
		String buyerUsername2 = f_dsm.getInputParameter("BUYER_LOGONID") + System.currentTimeMillis();
		
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPanel.registerB2B();
		
		//Register new buyer user
		rp.selectBuyerRegister()
		
				.typeLogonId(buyerUsername2)
				
				.typeOrganization(organizationName)
				
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				.typeVerifyPassword(f_dsm.getInputParameter("PASSWORD"))
				
				.typeFirstName(f_dsm.getInputParameter("FIRST_NAME"))
				
				.typeLastName(f_dsm.getInputParameter("LAST_NAME"))
				
				.typeStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))
				
				.typeCity(f_dsm.getInputParameter("CITY"))
				
				.selectCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))
				
				.selectStateOrProvince(f_dsm.getInputParameter("STATE"))
				
				.typeZipCode(f_dsm.getInputParameter("ZIPCODE"))
				
				.typeEmail(f_dsm.getInputParameter("EMAIL"))
				
				.submit();
		
		// Approve Organization
		oac.logon(f_dsm.getInputParameter("ACCELERATOR_LOGON_ID"), f_dsm.getInputParameter("ACCELERATOR_PASSWORD"));
		oac.approveAllApprovals();
		
		//Open AuroraB2BEsite store
		frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
		
		//Click on the SignIn page link on the header
		signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel.typeUsername(buyerUsername2)

				//Enter a valid password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				//Click Sign In
				.signIn(HeaderWidget.class);

		//Go to product display page
		RequisitionListsPage reqListPage = createRequisitionListWithSingleItem(header);
		
		//Sign back into store as different buyer
		header = reqListPage
					
				.getHeaderWidget()
				
				.openSignOutDropDownWidget()
				
				.signOut()
		
				.signIn()
		
				.typeUsername(buyerUsername)

				//Enter a valid password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				//Click Sign In
				.signIn(HeaderWidget.class);
		
		//go to My Account page
		MyAccountMainPage myAccountPage = header.goToMyAccount();

		MyAccountSidebarWidget myAccountSidebar = myAccountPage.getSidebar();
		
		//Go to requisition list page from my account side bar.
		reqListPage = myAccountSidebar.goToRequisitionListPage();

		RequisitionListsWidget reqListWidget = reqListPage.getRequistionListsWidget();
		
		//Click add to current order for this requisition list
		ShopCartPage shopCartPage = reqListWidget.addToCurrentOrder(f_dsm.getInputParameter("REQUISITION_LIST_POS"));
		
		//Complete order checkout
		shopCartPage.continueToNextStep()
		
				.selectPaymentMethod(f_dsm.getInputParameter("PAYMENT_TYPE"))
				
				.singleShipNext()
				
				.completeOrder()
				
				.verifyOrderSuccessful();
	}
	
	/**
	 * Add an item to a requisition list from requisition list details page
	 * 
	 * @throws Exception
	 */
	@Test
	public void FB2BSTOREB2BA_01_27() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel.typeUsername(buyerUsername)

				//Enter a valid password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				//Click Sign In
				.signIn(HeaderWidget.class);

		RequisitionListsPage reqListPage = createRequisitionListWithSingleItem(header);

		RequisitionListsWidget reqListWidget = reqListPage.getRequistionListsWidget();
		
		//Go to requisition list details page
		RequisitionListDetailsPage reqListDetailsPage = reqListWidget.goToRequisitionListDetails(
				f_dsm.getInputParameter("REQUISITION_LIST_POS"));
		
		//Add item to requisition list from requisition list detail page
		reqListDetailsPage.getRequisitionListItemsWidget()
				
				.typeSkuValue(f_dsm.getInputParameter("SKU_2"))
				
				.typeQuantity(f_dsm.getInputParameter("QUANTITY"))
				
				.addItemToList()
				
				.verifyItemByName(f_dsm.getInputParameter("SKU_2"));
	}
	
	/**
	 * A simple template class. Calling {@link #start()} will run 
	 * {@link #runTaskProcedure()}. {@link #runTeardown()} will run 
	 * irregardless of any exceptions in runTaskProcedure. 
	 * Any exception will be re-thrown after running teardown. 
	 * 
	 */
	abstract class TeardownTask{
		
		/**
		 * Run any pre-setup code. This is a convenience method and 
		 * will run before {@link #runTaskProcedure()}
		 * 
		 * @throws Exception
		 */
		void runPreSetup() throws Exception { }
		
		/**
		 * Run the main code for test case execution. Any exceptions 
		 * will be caught, {@link #runTeardown(boolean)} will be 
		 * called, then caught exception will be re-thrown 
		 * 
		 * @throws Exception
		 */
		abstract void runTaskProcedure() throws Exception;
		
		/**
		 * Method for teardown. Will run regardless of thrown exceptions. 
		 * @param errorCaught true if an exception was caught in 
		 * {@link #runPreSetup()} or {@link #runTaskProcedure()} 
		 * 
		 * @throws Exception
		 */
		abstract void runTeardown() throws Exception;
				
		final void start() throws Exception{
			
			Exception e = null;
			try{ 
				runPreSetup();
				runTaskProcedure();			
			}
			catch(Exception caught) { e = caught; }
			finally{
				runTeardown();
				
				if(e!=null) throw e;
			}			
		}	
	}

}