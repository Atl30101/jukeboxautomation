package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Logger;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.CustomerRegisterationPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.SignInPageB2B;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.OrgAdminConsole;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.wte.util.WcConfigManager;


/** 
 * Test scenario to test various use cases associated with user registration
 * Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2B_04 extends AbstractAuroraSingleSessionTests
{
	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

	//A Variable to retrieve data from the data file. 
	@DataProvider
	private final TestDataProvider dsm;

	private OrgAdminConsole oac;

	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with getConfig().properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_caslFixtures 
	 */		
	@Inject
	public FV2STOREB2B_04(
			Logger log, 
			WcConfigManager config,
			WcWteTestRule wcWebTestRule,
			CaslFoundationTestRule caslTestRule,
			OrgAdminConsole oac,
			TestDataProvider dataSetManager)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.oac = oac;
		this.dsm = dataSetManager;
	}

	/** 
	 * Register an Organization
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2B_0413()
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPage.registerB2B();

		rp.selectOrganizationRegister()

		//type organization name
		.typeOrganizationName(dsm.getInputParameter("ORGANIZATION_NAME") + RandomStringUtils.randomAlphanumeric(4))

		//type organization address
		.typeOrganizationStreetAddressLine1(dsm.getInputParameter("ORGANIZATION_ADDRESS"))

		//select organization country
		.selectOrganizationCountryOrRegion(dsm.getInputParameter("ORGANIZATION_COUNTRY"))

		//select organization province
		.selectOrganizationStateOrProvince(dsm.getInputParameter("ORGANIZATION_STATE"))

		//type organization city
		.typeOrganizationCity(dsm.getInputParameter("ORGANIZATION_CITY"))

		//type organization zipcode
		.typeOrganizationZipCode(dsm.getInputParameter("ORGANIZATION_ZIPCODE"))

		//type organization email
		.typeOrganizationEmail(dsm.getInputParameter("ORGANIZATION_EMAIL"))

		//type organization phone number
		.typeOrganizationPhoneNumber(dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))

		//type buyer username
		.typeOrganizationBuyerLogonId(dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID") + RandomStringUtils.randomAlphanumeric(4))

		//type buyer password
		.typeOrganizationBuyerPassword(dsm.getInputParameter("PASSWORD"))

		//Verify buyer password verify
		.typeOrganizationBuyerVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))

		//type buyer first name
		.typeOrganizationBuyerFirstName(dsm.getInputParameter("FIRST_NAME"))

		//type buyer last name
		.typeOrganizationBuyerLastName(dsm.getInputParameter("LAST_NAME"))

		//type buyer street address
		.typeOrganizationBuyerStreetAddressLine1(dsm.getInputParameter("ADDRESS"))

		//Select buyer country
		.selectOrganizationBuyerCountryOrRegion(dsm.getInputParameter("COUNTRY"))

		//type or select state for buyer
		.selectOrganizationBuyerStateOrProvince(dsm.getInputParameter("STATE"))

		//type buyer city
		.typeOrganizationBuyerCity(dsm.getInputParameter("CITY"))

		//type buyer zipcode
		.typeOrganizationBuyerZipCode(dsm.getInputParameter("ZIPCODE"))

		//type buyer E-mail
		.typeOrganizationBuyerEmail(dsm.getInputParameter("EMAIL"))

		//type buyer home phone number
		.typeOrganizationBuyerPhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))

		//Check if preferred language drop down is visible
		.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			

		//Select buyer preferred language
		.selectOrganizationBuyerPreferedCurrency(dsm.getInputParameter("PREFERRED_CURRENCY"))

		//Check if preferred currency drop down is visible
		.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()

		//Select buyer preferred language
		.selectOrganizationBuyerPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))

		//Submit registration
		.submitOrganizationBuyerRegistration()

		//verify registration confirmation message
		.verifyOrganizationBuyerRegistrationConfirmationMessage();

		// Approve Organization
		oac.logon(dsm.getInputParameter("ACCELERATOR_LOGON_ID"), dsm.getInputParameter("ACCELERATOR_PASSWORD"));
		oac.approveAllApprovals();
	}	

	/** 
	 * Sign in to the store with valid logon ID and valid password
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2B_0401()
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		signInPage.typeUsername(dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID"))

		//Enter a valid password
		.typePassword(dsm.getInputParameter("PASSWORD"))

		//Click Sign In
		.signIn();
	}

	/**
	 * Test case to login to AuroraB2BESite with valid user id, but invalid password.
	 */	
	@Test
	public void testFV2STOREB2B_0402()
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		signInPage.typeUsername(dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID"))

		//Enter an invalid password
		.typePassword(dsm.getInputParameter("PASSWORD_INVALID"))

		//Click Sign In, sign in not successful
		.unsuccessfulSignIn()

		//Verify the login error message
		.verifyIncorrectLogonMessage();

		//Enter a valid password
		signInPage.typePassword(dsm.getInputParameter("PASSWORD"))

		//Click Sign In
		.signIn();
	}

	/**
	 * Test case to login to AuroraB2BESite with valid user id, but empty password.
	 */
	@Test
	public void testFV2STOREB2B_0403()
	{
		//Open AuroraB2BESite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		signInPage.typeUsername(dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID"))

		//Click Sign In, sign in not successful
		.unsuccessfulSignInPassword();

		//Verify the error message
		signInPage.verifyEmptyPasswordMessage()

		//Enter a valid password
		.typePassword(dsm.getInputParameter("PASSWORD"))

		//Click Sign In
		.signIn();
	}	

	/**
	 * Test case to login to AuroraB2BEsite with invalid user id and invalid password.
	 */
	@Test
	public void testFV2STOREB2B_0404()
	{
		//Open AuroraB2BEsite store		
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();

		//Enter an invalid username
		signInPage.typeUsername(dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID_INVALID"))

		//Enter an invalid password
		.typePassword(dsm.getInputParameter("PASSWORD_INVALID"))

		//Click Sign In, sign in not successful
		.unsuccessfulSignIn()

		//Verify the error message
		.verifyIncorrectLogonMessage();
	}

	/**
	 * Test case to login to AuroraB2BESite with invalid user id and empty password.
	 */
	@Test
	public void testFV2STOREB2B_0405()
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();

		//Enter a invalid username
		signInPage.typeUsername(dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID_INVALID"))

		//Click Sign In, sign in not successful
		.unsuccessfulSignInPassword()

		//Verify the error message
		.verifyEmptyPasswordMessage();

	}

	/**
	 * Test case to login to AuroraB2BESite with empty user id and empty password.
	 */
	@Test
	public void testFV2STOREB2B_0406()
	{
		//Open AuroraB2BESite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();

		//Click Sign In, sign in not successful
		signInPage.unsuccessfulSignInLogonId()

		//Verify the empty error message
		.verifyEmptyUsernameMessage();
	}	

	/**
	 * Test case to login to AuroraB2BESite with valid user id and invalid password twice.
	 */
	@Test
	public void testFV2STOREB2B_0407()
	{
		//Open AuroraB2BESite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		signInPage.typeUsername(dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID"))

		//Enter an invalid password
		.typePassword(dsm.getInputParameter("PASSWORD_INVALID"))

		//Click Sign In, sign in not successful
		.unsuccessfulSignIn();

		//Verify the error message
		signInPage.verifyIncorrectLogonMessage()

		//Enter an invalid password
		.typePassword(dsm.getInputParameter("PASSWORD_INVALID"))

		//Click Sign In, sign in not successful
		.unsuccessfulSignIn()

		//Verify the error message
		.verifyIncorrectLogonMessage()

		//Enter an invalid password
		.typePassword(dsm.getInputParameter("PASSWORD_INVALID"))

		//Click Sign In, sign in not successful		
		.unsuccessfulSignIn()

		//Verify wait message due to multiple unsuccessful attempts
		.verifyShortWaitMessage();
	}

	/**
	 * Test case to login to AuroraB2BESite with valid user id and invalid password 6 times so that user account is disabled.
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2B_0408() throws Exception
	{
		//Open AuroraB2BEsite store
				AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

				//Click on the SignIn page link on the header
				SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();

				//Click on the registration button on the SignIn page
				CustomerRegisterationPageB2B rp = signInPage.registerB2B();
				String userId = dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID") + RandomStringUtils.randomAlphanumeric(4);
				SignInPageB2B signInPageB2B = 	rp.selectOrganizationRegister()

				//type organization name
				.typeOrganizationName(dsm.getInputParameter("ORGANIZATION_NAME") + RandomStringUtils.randomAlphanumeric(4))

				//type organization address
				.typeOrganizationStreetAddressLine1(dsm.getInputParameter("ORGANIZATION_ADDRESS"))

				//select organization country
				.selectOrganizationCountryOrRegion(dsm.getInputParameter("ORGANIZATION_COUNTRY"))

				//select organization province
				.selectOrganizationStateOrProvince(dsm.getInputParameter("ORGANIZATION_STATE"))

				//type organization city
				.typeOrganizationCity(dsm.getInputParameter("ORGANIZATION_CITY"))

				//type organization zipcode
				.typeOrganizationZipCode(dsm.getInputParameter("ORGANIZATION_ZIPCODE"))

				//type organization email
				.typeOrganizationEmail(dsm.getInputParameter("ORGANIZATION_EMAIL"))

				//type organization phone number
				.typeOrganizationPhoneNumber(dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))

				//type buyer username
				.typeOrganizationBuyerLogonId(userId)

				//type buyer password
				.typeOrganizationBuyerPassword(dsm.getInputParameter("PASSWORD"))

				//Verify buyer password verify
				.typeOrganizationBuyerVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))

				//type buyer first name
				.typeOrganizationBuyerFirstName(dsm.getInputParameter("FIRST_NAME"))

				//type buyer last name
				.typeOrganizationBuyerLastName(dsm.getInputParameter("LAST_NAME"))

				//type buyer street address
				.typeOrganizationBuyerStreetAddressLine1(dsm.getInputParameter("ADDRESS"))

				//Select buyer country
				.selectOrganizationBuyerCountryOrRegion(dsm.getInputParameter("COUNTRY"))

				//type or select state for buyer
				.selectOrganizationBuyerStateOrProvince(dsm.getInputParameter("STATE"))

				//type buyer city
				.typeOrganizationBuyerCity(dsm.getInputParameter("CITY"))

				//type buyer zipcode
				.typeOrganizationBuyerZipCode(dsm.getInputParameter("ZIPCODE"))

				//type buyer E-mail
				.typeOrganizationBuyerEmail(dsm.getInputParameter("EMAIL"))

				//type buyer home phone number
				.typeOrganizationBuyerPhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))

				//Check if preferred language drop down is visible
				.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			

				//Select buyer preferred language
				.selectOrganizationBuyerPreferedCurrency(dsm.getInputParameter("PREFERRED_CURRENCY"))

				//Check if preferred currency drop down is visible
				.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()

				//Select buyer preferred language
				.selectOrganizationBuyerPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))

				//Submit registration
				.submitOrganizationBuyerRegistration()

				//verify registration confirmation message
				.verifyOrganizationBuyerRegistrationConfirmationMessage();

				// Approve Organization
				oac.logon(dsm.getInputParameter("ACCELERATOR_LOGON_ID"), dsm.getInputParameter("ACCELERATOR_PASSWORD"));
				oac.approveAllApprovals();
				
				frontPage = signInPageB2B.getHeaderWidget().goToFrontPageB2B();
				
				signInPage = frontPage.getHeaderWidget().signIn();
				
		signInPage.typeUsername(userId);
		
		for(int i = 0; i < 6; i++ ) 
		{
			getLog().info("Sign In Attempt: " + i);

			//Enter an invalid password
			signInPage.typePassword(dsm.getInputParameter("PASSWORD_INVALID"))

			//Click Sign In, sign in not successful
			.unsuccessfulSignIn();

			//1 min timeout when enetering wrong information
			try {
				//Verify the error message
				signInPage.verifyIncorrectLogonMessage();
			} catch (IllegalArgumentException e) {
				signInPage.verifyTimeOutLogonMessage();
				//logged in too many times in a short period of time, introduce a wait
				getLog().info("Store is forcing a wait before logging again, waiting for a minute");
				i--;
				//Wait
				Thread.sleep(61000);
			}
		}
		//Wait
		Thread.sleep(60000);

		//On the 7th attempt, enter a valid password
		signInPage.typePassword(dsm.getInputParameter("PASSWORD"))

		//Click Sign In, sign in not successful
		.unsuccessfulSignIn()

		//Verify user id is locked
		.verifyAccountLockMessageB2B();
	}
	
	
}
