package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.CustomerRegisterationPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.OrderApprovalPage;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.Accelerator;
import com.ibm.commerce.qa.url.OrgAdminConsole;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 * Scenario: FV2STOREB2B_11
 * Details: Test View Order Status
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2B_10 extends AbstractAuroraSingleSessionTests
{

    /**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	/**A variable to hold the name of the data file where input parameters can be found.**/
	//protected final String dataFileName = "data/FSTOREELITE_09_Data.xml";
	private String [] shippingModes = {};
	private String [] shippingCharges = {};
	private String contractId;
	private String accountId;
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	
	private final CaslFixturesFactory f_CaslFixtures;
	
	private OrgAdminConsole oac;
	
	private Accelerator accelerator;
	
	
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_CaslFixtures 
	 */	@Inject
	public FV2STOREB2B_10(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,	
			Accelerator accelerator,
			CaslFixturesFactory p_CaslFixtures,
			OrgAdminConsole oac)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		this.accelerator = accelerator;
		this.oac = oac;
		f_CaslFixtures = p_CaslFixtures;
		
	}
	
	 @Before
	 public void setup() throws Exception {
		 registerUserAndOrg();
		 accelerator.logon(dsm.getInputParameter("ACCELERATOR_LOGON_ID"),dsm.getInputParameter("ACCELERATOR_PASSWORD"));
		 accountId = accelerator.createNewAccount(dsm.getInputParameter("ORG"), null, null, false);
		 createContract(dsm.getInputParameter("ORG"));
		 accelerator.logoff();
	 }
	 private void registerUserAndOrg(){ 
			//Open Auroraesite store
			AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

			//Click on the SignIn page link on the header
			SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();
			
			//attempt to sign in:
			try {
				signInPage.typeUsername(dsm.getInputParameter("LOGONID"))
					.typePassword(dsm.getInputParameter("PASSWORD"))
					.signIn();
				
				//Open Auroraesite store
				frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

				
			} catch (Exception e1) {
				//Open Auroraesite store
				

				//Click on the registration button on the SignIn page
				CustomerRegisterationPageB2B rp = signInPage.registerB2B();

				rp.selectOrganizationRegister()
				
				//type organization name
				.typeOrganizationName(dsm.getInputParameter("ORGANIZATION_NAME"))
				
				//type organization address
				.typeOrganizationStreetAddressLine1(dsm.getInputParameter("ORGANIZATION_ADDRESS"))
				
				//select organization country
				.selectOrganizationCountryOrRegion(dsm.getInputParameter("ORGANIZATION_COUNTRY"))
				
				//select organization province
				.selectOrganizationStateOrProvince(dsm.getInputParameter("ORGANIZATION_STATE"))
				
				//type organization city
				.typeOrganizationCity(dsm.getInputParameter("ORGANIZATION_CITY"))
				
				//type organization zipcode
				.typeOrganizationZipCode(dsm.getInputParameter("ORGANIZATION_ZIPCODE"))
				
				//type organization email
				.typeOrganizationEmail(dsm.getInputParameter("ORGANIZATION_EMAIL"))
				
				//type organization phone number
				.typeOrganizationPhoneNumber(dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))

				//type buyer username
				.typeOrganizationBuyerLogonId(dsm.getInputParameter("LOGONID"))

				//type buyer password
				.typeOrganizationBuyerPassword(dsm.getInputParameter("PASSWORD"))

				//Verify buyer password verify
				.typeOrganizationBuyerVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))

				//type buyer first name
				.typeOrganizationBuyerFirstName(dsm.getInputParameter("FIRST_NAME"))

				//type buyer last name
				.typeOrganizationBuyerLastName(dsm.getInputParameter("LAST_NAME"))

				//type buyer street address
				.typeOrganizationBuyerStreetAddressLine1(dsm.getInputParameter("ADDRESS"))

				//Select buyer country
				.selectOrganizationBuyerCountryOrRegion(dsm.getInputParameter("COUNTRY"))

				//type or select state for buyer
				.selectOrganizationBuyerStateOrProvince(dsm.getInputParameter("STATE"))

				//type buyer city
				.typeOrganizationBuyerCity(dsm.getInputParameter("CITY"))

				//type buyer zipcode
				.typeOrganizationBuyerZipCode(dsm.getInputParameter("ZIPCODE"))

				//type buyer E-mail
				.typeOrganizationBuyerEmail(dsm.getInputParameter("EMAIL"))

				//type buyer home phone number
				.typeOrganizationBuyerPhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))

				//Check if preferred language drop down is visible
				.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			

				//Select buyer preferred language
				.selectOrganizationBuyerPreferedCurrency(dsm.getInputParameter("PREFERRED_CURRENCY"))

				//Check if preferred currency drop down is visible
				.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()

				//Select buyer preferred language
				.selectOrganizationBuyerPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))

				//Submit registration
				.submitOrganizationBuyerRegistration()
				
				//verify registration confirmation message
				.verifyOrganizationBuyerRegistrationConfirmationMessage();
				
				// Approve Organization
				oac.logon(dsm.getInputParameter("ADMIN_LOGON_ID"), dsm.getInputParameter("ADMIN_PASSWORD"));
				oac.approveAllApprovals();
				
			}
		} 	 
	 /** 
	 * Buyer views pending order approval list
	 * @throws Exception 
		 */
		@Category(Sanity.class)
		@Test
		public void testFV2STOREB2B_1001() throws Exception
		{
			//verify in storefront
			AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
			
			//Opens the Sign In page in browser.
			SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
			
			//Log in to the store.
			HeaderWidget header = signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
												.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
												.signIn();
												
			//use Service layer call to create the order waiting for Approval
			OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"), getConfig().getStoreName());
			orders.removeAllItemsFromCart();
			orders.addItem(dsm.getInputParameter("PRODUCT_SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
			orders.addPayLaterPaymentMethod();
			Long orderId = orders.completeOrder();
			
			
			MyAccountMainPage myAccount	= header.goToMyAccount();
			
			OrderApprovalPage orderApproval = myAccount.getSidebar().goToOrderApprovalPage();
			
			orderApproval.getOrderApprovalWidget().verifyOrderIdPresent(orderId+"");
			
			orderApproval.getOrderApprovalWidget().openActionListByRow("1").rejectOrderByRow("1");
			
			
			
		}	
	
			private String createContract(String org) throws Exception
			{
				dsm.setDataLocation("setup", "createContract");
						
				String [] percentOff = {dsm.getInputParameter("percentOff1_1"),dsm.getInputParameter("percentOff1_2")};
				String [] requiredCatalogs = {dsm.getInputParameter("requiredCatalogs1_1"),dsm.getInputParameter("requiredCatalogs1_2")};
				
				//gets a new unique contract name.
				String contractName = RandomStringUtils.randomAlphanumeric(8);
				
				//gets all available shipping modes.
				shippingModes = accelerator.getShippingModes();
				
				//gets all available shipping charges.
				shippingCharges = accelerator.getShippingCharges();
				
				//creates new contract for the account created in previous steps.
				contractId = accelerator.createNewContract(contractName, org,dsm.getInputParameter("INCLUDE_ENTIRE_CATALOG"),dsm.getInputParameter("PERCENTAGE_OFF_ON_ENTIRE_CATALOG"), requiredCatalogs, percentOff, shippingModes, shippingCharges);
							
				//submit the contract;
				accelerator.submitContract(accountId, contractId);
				
				while(!(accelerator.isContractActive(org, contractName)))
				{
					if(accelerator.isContractActive(org, contractName))
					{
						break;
					}
				}
						
				return contractName;
						
			}
			
			
		@After
		public void tearDown() throws Exception {
			
			//cancel and delete contract created
			accelerator.logon(dsm.getInputParameter("ACCELERATOR_LOGON_ID"),dsm.getInputParameter("ACCELERATOR_PASSWORD"));
			
			accelerator.cancelContract(accountId, contractId);
			accelerator.deleteContract(accountId, contractId);
			
			accelerator.deleteAccount(accountId);					
			
		}
			
			



	
			
	
	
}
