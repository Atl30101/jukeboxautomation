package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.MyAddressBookPage;
import com.ibm.commerce.qa.aurora.page.MyPersonalInfoPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.wte.util.WcConfigManager;


/** 
 * Test scenario to Update personal information
 * Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2B_06 extends AbstractAuroraSingleSessionTests
{
	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

	//A Variable to retrieve data from the data file. 
	@DataProvider
	private final TestDataProvider dsm;
	
	//A Variable to determine if the address book needs to be removed in tear down.
		private boolean addressBookCleanup = false;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with getConfig().properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_caslFixtures 
	 */		
	@Inject
	public FV2STOREB2B_06(
			Logger log, 
			WcConfigManager config,
			WcWteTestRule wcWebTestRule,
			CaslFoundationTestRule caslTestRule,
			TestDataProvider dataSetManager)
	{
		super(log, wcWebTestRule, caslTestRule);
		
		this.dsm = dataSetManager;
	}

	/**
	 * Helper method to execute the sign in action for each test case.
	 * 
	 * @return a new my account page object.
	 */
	public MyAccountMainPage executeSignIn()
	{
		//Open the store in the browser
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);

		//Open Sign In/Register page
		HeaderWidget headerWidget = frontPage.getHeaderWidget().signIn()
		
		//Enter account User Name/Password and sign in
		.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
		.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD")).signIn();
		
		MyAccountMainPage accountPage = headerWidget.goToMyAccount();
		return accountPage;
	}
	/** Test case to Add a new shipping address to the buyer's address book
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2B_0602()
	{

		//Execute sign in operation
				MyAccountMainPage myAccountPage = executeSignIn();
				
				//Open my address book page
				MyAddressBookPage addressBook = myAccountPage.getSidebar().goToMyAddressBook();
				
				//Add a new address to my address book
				addressBook.addNewAddress();
				
				//Enter new address data
				addressBook.selectAddressType(dsm.getInputParameter("ADDRESS_BOOK_ADDRESS_TYPE"))
				.typeRecipient(dsm.getInputParameter("ADDRESS_BOOK_RECIPIENT"))
				.typeFirstName(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
				.typeLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
				.typeStreetAddress(dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
				.typeCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
				.typeZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
				.typePhoneNumber(dsm.getInputParameter("ADDRESS_BOOK_PHONE_NUMBER"))
				.typeEMail(dsm.getInputParameter("ADDRESS_BOOK_EMAIL"));
				
				//Submit personal information update
				addressBook.submitSuccessfulNewAddress();
				
				//Verify address added successfully
				addressBook.getHeaderWidget().verifyMessage(dsm.getInputParameter("CONFIRMATION_MESSAGE"));

				//Indicate that address must be removed at tear down
				addressBookCleanup = true;
	}	
	/**
	 * Test case to add a new billing address in my address book page.
	 * 
	 */
	@Test
	public void testFV2STOREB2B_0603() {
		
		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open my address book page
		MyAddressBookPage addressBook = myAccountPage.getSidebar().goToMyAddressBook();
		
		//Add a new address to my address book
		addressBook.addNewAddress();
		
		//Enter new address data
		addressBook.selectAddressType(dsm.getInputParameter("ADDRESS_BOOK_ADDRESS_TYPE"))
		.typeRecipient(dsm.getInputParameter("ADDRESS_BOOK_RECIPIENT"))
		.typeFirstName(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
		.typeLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
		.typeStreetAddress(dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
		.typeCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
		.typeZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
		.typePhoneNumber(dsm.getInputParameter("ADDRESS_BOOK_PHONE_NUMBER"))
		.typeEMail(dsm.getInputParameter("ADDRESS_BOOK_EMAIL"));
		
		//Submit personal information update
		addressBook.submitSuccessfulNewAddress();
		
		//Verify address added successfully
		addressBook.getHeaderWidget().verifyMessage(dsm.getInputParameter("CONFIRMATION_MESSAGE"));
		
		//Indicate that address must be removed at tear down
		addressBookCleanup = true;
	}
	
	/**
	 * Test case to add a new Shipping and Billing address in my address book page.
	 * 
	 */
	@Test
	public void testFV2STOREB2B_0604() {

		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open my address book page
		MyAddressBookPage addressBook = myAccountPage.getSidebar().goToMyAddressBook();
		
		//Add a new address to my address book
		addressBook.addNewAddress();
		
		//Enter new address data
		addressBook.selectAddressType(dsm.getInputParameter("ADDRESS_BOOK_ADDRESS_TYPE"))
		.typeRecipient(dsm.getInputParameter("ADDRESS_BOOK_RECIPIENT"))
		.typeFirstName(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
		.typeLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
		.typeStreetAddress(dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
		.typeCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
		.typeZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
		.typePhoneNumber(dsm.getInputParameter("ADDRESS_BOOK_PHONE_NUMBER"))
		.typeEMail(dsm.getInputParameter("ADDRESS_BOOK_EMAIL"));
		
		//Submit personal information update
		addressBook.submitSuccessfulNewAddress();
		
		//Verify address added successfully
		addressBook.getHeaderWidget().verifyMessage(dsm.getInputParameter("CONFIRMATION_MESSAGE"));
		
		//Indicate that address must be removed at tear down
		addressBookCleanup = true;
	}
	
	/**
	 * Test case to add a new shipping address to the address book with an invalid email value
	 * 
	 */
	@Test
	public void testFV2STOREB2B_0605() {

		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open my address book page
		MyAddressBookPage addressBook = myAccountPage.getSidebar().goToMyAddressBook();
		
		//Add a new address to my address book
		addressBook.addNewAddress();

		//Enter new address data
		addressBook.selectAddressType(dsm.getInputParameter("ADDRESS_BOOK_ADDRESS_TYPE"))
		.typeRecipient(dsm.getInputParameter("ADDRESS_BOOK_RECIPIENT"))
		.typeFirstName(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
		.typeLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
		.typeStreetAddress(dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
		.typeCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
		.typeZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
		.typePhoneNumber(dsm.getInputParameter("ADDRESS_BOOK_PHONE_NUMBER"))
		.typeEMail(dsm.getInputParameter("ADDRESS_BOOK_EMAIL"));
		
		//Submit personal information update
		addressBook.submitUnsuccessfulNewAddress();
		
		//Verify error message pop up on address book Page
		addressBook.verifyErrorTooltipPresent(dsm.getInputParameter("TOOLTIP_ERROR_MESSAGE"));
	}
	
	/**
	 * Test case to update the street address on the personal information page with a blank value.
	 * 
	 */
	@Test
	public void testFV2STOREB2B_0606() {

		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open personal information page
		MyPersonalInfoPage personalInfoPage = myAccountPage.getSidebar().goToMyPersonalInfoPage();
		
		//Update street address 
		personalInfoPage.typeStreetAddress(dsm.getInputParameter("PERSONAL_INFORMATION_PAGE_STREET_ADDRESS"));
		
		//Submit personal information update
		personalInfoPage.updateFail();
		
		//Verify error message pop up on personal information page
		personalInfoPage.verifyErrorTooltipPresent(dsm.getInputParameter("TOOLTIP_ERROR_MESSAGE"));
	}
	
	/**
	 * Test case to add a new shipping address to the address book with an empty
	 * value for last name.
	 * 
	 */
	@Test
	public void testFV2STOREB2B_0607() {

		//Execute sign in operation
		MyAccountMainPage myAccountPage = executeSignIn();
		
		//Open my address book page
		MyAddressBookPage addressBook = myAccountPage.getSidebar().goToMyAddressBook();
		
		//Add a new address to my address book
		addressBook.addNewAddress();

		//Enter new address data
		addressBook.selectAddressType(dsm.getInputParameter("ADDRESS_BOOK_ADDRESS_TYPE"))
		.typeRecipient(dsm.getInputParameter("ADDRESS_BOOK_RECIPIENT"))
		.typeFirstName(dsm.getInputParameter("ADDRESS_BOOK_FIRST_NAME"))
		.typeLastName(dsm.getInputParameter("ADDRESS_BOOK_LAST_NAME"))
		.typeStreetAddress(dsm.getInputParameter("ADDRESS_BOOK_STREET_ADDRESS"))
		.typeCity(dsm.getInputParameter("ADDRESS_BOOK_CITY"))
		.typeZipCode(dsm.getInputParameter("ADDRESS_BOOK_ZIPCODE"))
		.typePhoneNumber(dsm.getInputParameter("ADDRESS_BOOK_PHONE_NUMBER"))
		.typeEMail(dsm.getInputParameter("ADDRESS_BOOK_EMAIL"));
		
		//Submit personal information update
		addressBook.submitUnsuccessfulNewAddress();
		
		//Verify error message pop up on address book Page
		addressBook.verifyErrorTooltipPresent(dsm.getInputParameter("TOOLTIP_ERROR_MESSAGE"));
	}
	/**
	 * Test case to update the shipping address with a valid phone number.
	 * 
	 */
	@Test
	public void testFV2STOREB2B_0612() {
			
			//Execute sign in operation
			MyAccountMainPage myAccountPage = executeSignIn();
			
			//Open my quick checkout profile page
			MyAddressBookPage addressBook = myAccountPage.getSidebar().goToMyAddressBook();
			
			//Updates the address information
			addressBook.selectAddress(dsm.getInputParameter("CHANGE_ADDRESS_BOOK_ADDRESS_ID"))
			.selectAddressType(dsm.getInputParameter("ADDRESS_BOOK_ADDRESS_TYPE"))
			.typePhoneNumber(dsm.getInputParameter("ADDRESS_BOOK_PHONE_NUMBER"));
			
			//Submit address book information update
			addressBook.updateSuccessfulAddress();
			
			//Verify address added successfully
			addressBook.getHeaderWidget().verifyMessage(dsm.getInputParameter("CONFIRMATION_MESSAGE")).closeMessageArea();
		
			//Set default address
			addressBook.selectAddress(dsm.getInputParameter("CHANGE_ADDRESS_BOOK_ADDRESS_ID"))
			.selectAddressType(dsm.getInputParameter("ADDRESS_BOOK_ADDRESS_TYPE")).updateSuccessfulAddress();
			
	}
	/**
	 * Tear down ran after every test case
	 */
	@After
	public void tearDown() {
		
		
		try
		{
			//Open up browser session continuing at the header section
			HeaderWidget headerSection = getSession().continueToPage(getConfig().getStoreUrl(), HeaderWidget.class);
			
			if(addressBookCleanup == true){
				
				//Go to my account page
				MyAccountMainPage myAccountPage = headerSection.goToMyAccount();
				
				//Open address book page from left side bar on my account page
				MyAddressBookPage addressBook = myAccountPage.getSidebar().goToMyAddressBook();
				
				//Remove address from address book
				addressBook.removeAddress(dsm.getInputParameter("ADDRESS_BOOK_RECIPIENT"));
				
			}
			
			addressBookCleanup = false;
					}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

		
	}
}
