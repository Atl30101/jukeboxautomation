package com.ibm.commerce.qa.aurorab2b.tests;

import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.CreateOrganizationPage;
import com.ibm.commerce.qa.aurorab2b.page.CustomerRegisterationPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.EditOrganizationPage;
import com.ibm.commerce.qa.aurorab2b.page.OrganizationsAndBuyersPage;
import com.ibm.commerce.qa.aurorab2b.page.SignInPageB2B;
import com.ibm.commerce.qa.casl.util.CaslModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.OrgAdminConsole;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.wte.framework.test.WebSession;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
/**
 * Scenario: FSSC-92726-04
 * Details: Manage organizations as a buyer admin user
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class, CaslModule.class})
public class FB2BSTOREB2BA_04 extends AbstractAuroraSingleSessionTests {

	private final OrgAdminConsole oac;
	
	
	
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider f_dsm;

	private static String organizationName;

	private static String adminUsername;
	
	private static String lastCreatedOrg = "";
	
	private static boolean setupCompleted = false;
	
	/**
	 * Test Class object constructor.
	 * 
	 */
	@Inject
	public FB2BSTOREB2BA_04(
			Logger log, 
			WcWteTestRule wcWteTestRule,
			CaslFoundationTestRule caslTestRule,
			TestDataProvider p_dsm,
			OrgAdminConsole p_oac,
			WebSession p_webSession) throws Exception
	{
		super(log, wcWteTestRule, caslTestRule);
		
		f_dsm = p_dsm;
		oac = p_oac;
	}
	
	@Before
	public void setup() throws Exception
	{
		if(!setupCompleted)
		{
			//Open AuroraB2BEsite store
			AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	
	
			//Click on the SignIn page link on the header
			SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();
	
			//Click on the registration button on the SignIn page
			CustomerRegisterationPageB2B rp = signInPanel.registerB2B();
	
			adminUsername = f_dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID") + System.currentTimeMillis();
	
			organizationName = f_dsm.getInputParameter("ORGANIZATION_NAME") + System.currentTimeMillis();
	
			SignInPageB2B singInPage = rp.selectOrganizationRegister()
	
					//type organization name
					.typeOrganizationName(organizationName)
			
					//type organization address
					.typeOrganizationStreetAddressLine1(f_dsm.getInputParameter("ORGANIZATION_ADDRESS"))
			
					//select organization country
					.selectOrganizationCountryOrRegion(f_dsm.getInputParameter("ORGANIZATION_COUNTRY"))
			
					//select organization province
					.selectOrganizationStateOrProvince(f_dsm.getInputParameter("ORGANIZATION_STATE"))
			
					//type organization city
					.typeOrganizationCity(f_dsm.getInputParameter("ORGANIZATION_CITY"))
			
					//type organization zipcode
					.typeOrganizationZipCode(f_dsm.getInputParameter("ORGANIZATION_ZIPCODE"))
			
					//type organization email
					.typeOrganizationEmail(f_dsm.getInputParameter("ORGANIZATION_EMAIL"))
			
					//type organization phone number
					.typeOrganizationPhoneNumber(f_dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))
			
					//type buyer username
					.typeOrganizationBuyerLogonId(adminUsername)
			
					//type buyer password
					.typeOrganizationBuyerPassword(f_dsm.getInputParameter("PASSWORD"))
			
					//Verify buyer password verify
					.typeOrganizationBuyerVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))
			
					//type buyer first name
					.typeOrganizationBuyerFirstName(f_dsm.getInputParameter("FIRST_NAME"))
			
					//type buyer last name
					.typeOrganizationBuyerLastName(f_dsm.getInputParameter("LAST_NAME"))
			
					//type buyer street address
					.typeOrganizationBuyerStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))
			
					//Select buyer country
					.selectOrganizationBuyerCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))
			
					//type or select state for buyer
					.selectOrganizationBuyerStateOrProvince(f_dsm.getInputParameter("STATE"))
			
					//type buyer city
					.typeOrganizationBuyerCity(f_dsm.getInputParameter("CITY"))
			
					//type buyer zipcode
					.typeOrganizationBuyerZipCode(f_dsm.getInputParameter("ZIPCODE"))
			
					//type buyer E-mail
					.typeOrganizationBuyerEmail(f_dsm.getInputParameter("EMAIL"))
			
					//type buyer home phone number
					.typeOrganizationBuyerPhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))
			
					//Check if preferred language drop down is visible
					.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			
			
					//Select buyer preferred language
					.selectOrganizationBuyerPreferedCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))
			
					//Check if preferred currency drop down is visible
					.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()
			
					//Select buyer preferred language
					.selectOrganizationBuyerPreferedLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))
			
					//Submit registration
					.submitOrganizationBuyerRegistration()
			
					//verify registration confirmation message
					.verifyOrganizationBuyerRegistrationConfirmationMessage();
	
			// Approve Organization
			oac.logon(f_dsm.getInputParameter("ACCELERATOR_LOGON_ID"), f_dsm.getInputParameter("ACCELERATOR_PASSWORD"));
			oac.approveAllApprovals();
			
			f_dsm.setDataBlock("createOrganization");
			
			singInPage.typeUsername(adminUsername)
					
					.typePassword(f_dsm.getInputParameter("PASSWORD"))
					
					//Click Sign In
					.signIn();
			
			//Go to organizations and buyers page from my account side bar
			OrganizationsAndBuyersPage orgAndBuyersPage = singInPage.getHeaderWidget().goToMyAccount()
					
					.getSidebar()
					
					.goToOrganizationsAndBuyersPage();
			
			//Create 20 new organizations that buyer admin can manage.
			for (int i = 0; i < 20; i++)
			{
				lastCreatedOrg = f_dsm.getInputParameter("ORGANIZATION_NAME") + System.currentTimeMillis();
				
				//Go to create organization page.
				CreateOrganizationPage createOrgPage = orgAndBuyersPage.getOrganizationListWidget()
				
						.goToCreateOrganizationPage();
				
				//Enter new organization details.
				createOrgPage.getCreateOrganizationWidget()
						
						.getOrganizationDetailsWidget()
						
						.getOrganizationListWidget()
						
						.typeOrganizationName(lastCreatedOrg);
				
				createOrgPage.getCreateOrganizationWidget()
				
						.getOrganizationAddressWidget()
						
						.typeStreetAddress(f_dsm.getInputParameter("ADDRESS"))
						
						.typeCity(f_dsm.getInputParameter("CITY"))
						
						.typeZipCode(f_dsm.getInputParameter("ZIPCODE"));
						
				createOrgPage.getCreateOrganizationWidget()
				
						.getOrganizationContactInfoWidget()
						
						.typeEmail(f_dsm.getInputParameter("EMAIL"));
				
				//Select assigned roles for this organization
				createOrgPage.getCreateOrganizationWidget()
				
						.getOrganizationRolesWidget()
						
						.allowRoleAssignment(f_dsm.getInputParameter("ROLE_1"))
						
						.allowRoleAssignment(f_dsm.getInputParameter("ROLE_2"))
						
						.allowRoleAssignment(f_dsm.getInputParameter("ROLE_3"));
				
				//Click submit button to create organization
				EditOrganizationPage editOrgPage = createOrgPage.getCreateOrganizationWidget()
				
						.submit();
				
				//Go to organizations and buyers page from my account side bar
				orgAndBuyersPage = editOrgPage.getSidebar().goToOrganizationsAndBuyersPage();
			}
		}
		setupCompleted = true;
	}
	
	/**
	 * Update the details of the organization the buyer admin can manage
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_04_04() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel

				.typeUsername(f_dsm.getInputParameter("BUYER_ADMIN_USER"))
				
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				//Click Sign In
				.signInWithoutVerify(); 
		
		//Go to organizations and buyers page from my account side bar
		OrganizationsAndBuyersPage orgAndBuyersPage = header.goToMyAccount()
		
				.getSidebar()
				
				.goToOrganizationsAndBuyersPage();
		
		//Click edit organization button
		EditOrganizationPage editPage = orgAndBuyersPage.getOrganizationListWidget()
				
				.verifyOrgListIsDropDown()
		
				.goToEditOrganizationPage();
		
		//Edit the address info for this organization
		editPage.getEditOrganizationWidget()
			
				.getOrganizationAddressWidget()
				
				.selectEditAddress()
				
				.typeStreetAddress(f_dsm.getInputParameter("STREET_UPDATE"))
				
				.saveAddress()
				
				.verifyAddressDetail(f_dsm.getInputParameter("STREET_LINE_NUMBER"), 
						f_dsm.getInputParameter("STREET_UPDATE"));
		
		//Edit the contact info for this organization
		editPage.getEditOrganizationWidget()
		
				.getOrganizationContactInfoWidget()
				
				.selectEditContact()
				
				.typeEmail(f_dsm.getInputParameter("EMAIL_UPDATE"))
				
				.saveContactInfo()
				
				.verifyEmailInfo(f_dsm.getInputParameter("EMAIL_UPDATE"));
	}
	
	/**
	 * Search for an organization, then clear the results
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_04_09() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel

				.typeUsername(adminUsername)
				
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				//Click Sign In
				.signIn();
		
		//Go to organizations and buyers page from my account side bar
		OrganizationsAndBuyersPage orgAndBuyersPage = header.goToMyAccount()
				
				.getSidebar()
				
				.goToOrganizationsAndBuyersPage();
		
		//Search for organization name
		orgAndBuyersPage.getOrganizationListWidget()
		
				.verifyOrgListIsTableFormat()
				
				.searchForOrganization(lastCreatedOrg)
		
				.verifyOrganizationInTable(lastCreatedOrg)
				
				.verifyTotalNumberOfOrgs("1")
				
				.searchForOrganization("")
				
				.verifyTotalNumberOfOrgs(f_dsm.getInputParameter("SEARCH_TOTAL"));
	}
	
	/**
	 * Ensure only parent organizations roles are listed in the current
	 * organization set of roles
	 * 
	 */
	@Test
	public void FB2BSTOREB2BA_04_20() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel

				.typeUsername(adminUsername)
				
				.typePassword(f_dsm.getInputParameter("PASSWORD"))
				
				//Click Sign In
				.signIn();
		
		//Go to organizations and buyers page from my account side bar
		OrganizationsAndBuyersPage orgAndBuyersPage = header.goToMyAccount()
		
				.getSidebar()
				
				.goToOrganizationsAndBuyersPage();
		
		String orgName = f_dsm.getInputParameter("ORGANIZATION_NAME") + System.currentTimeMillis();
		
		//Click on create organization buttom
		CreateOrganizationPage createOrgPage = orgAndBuyersPage.getOrganizationListWidget()
		
				.goToCreateOrganizationPageFromTableView();
		
		//Enter new organization data
		createOrgPage.getCreateOrganizationWidget()
				
				.getOrganizationDetailsWidget()
				
				.getOrganizationListWidget()
				
				.typeOrganizationName(orgName)
				
				//Select the parent organization
				.searchForOrganization(lastCreatedOrg)
				
				.selectOrganizationInTable(lastCreatedOrg);
		
		//Enter new organization address info.
		createOrgPage.getCreateOrganizationWidget()
		
				.getOrganizationAddressWidget()
				
				.typeStreetAddress(f_dsm.getInputParameter("ADDRESS"))
				
				.typeCity(f_dsm.getInputParameter("CITY"))
				
				.typeZipCode(f_dsm.getInputParameter("ZIPCODE"));
		
		//Enter new organization contact info.
		createOrgPage.getCreateOrganizationWidget()
		
				.getOrganizationContactInfoWidget()
				
				.typeEmail(f_dsm.getInputParameter("EMAIL"));
		
		//Verify the correct role assignment exist.
		createOrgPage.getCreateOrganizationWidget()
		
				.getOrganizationRolesWidget()
				
				.verifyRoleAssignmentIsPresent(f_dsm.getInputParameter("ROLE_1"))
				
				.verifyRoleAssignmentIsPresent(f_dsm.getInputParameter("ROLE_2"))
				
				.verifyRoleAssignmentIsPresent(f_dsm.getInputParameter("ROLE_3"))
				
				.verifyRoleAssignmentIsPresent(f_dsm.getInputParameter("ROLE_NOT_PRESENT_1"))
				
				.verifyRoleAssignmentIsPresent(f_dsm.getInputParameter("ROLE_NOT_PRESENT_2"))
				
				.allowRoleAssignment(f_dsm.getInputParameter("ROLE_3"));
		
		//Click submit button to create the organization
		createOrgPage.getCreateOrganizationWidget()
		
				.submit();
	}
}