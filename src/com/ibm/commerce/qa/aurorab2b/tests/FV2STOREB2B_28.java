package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.ProductDisplayPageB2B;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 * Scenario: FV2STOREB2B_28
 * Details: Test add to shopping cart flows.
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2B_28 extends AbstractAuroraSingleSessionTests
{

    /**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	
	private final CaslFixturesFactory f_CaslFixtures;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_CaslFixtures 
	 */	@Inject
	public FV2STOREB2B_28(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,	
			CaslFixturesFactory p_CaslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		
		f_CaslFixtures = p_CaslFixtures;
		
	}
	
	 	
		 
	/**
	 * Test case to Order an item that has complete inventory available
	 *
	 * @throws Exception
	 */	
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2B_2801() throws Exception
	{		
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		MyAccountMainPage myAccount = signIn.typeUsername(dsm.getInputParameter("LOGONID"))
											.typePassword(dsm.getInputParameter("PASSWORD"))
											.signIn()
											.goToMyAccount();
		
		//cleanup
		cartCleanup(myAccount);
		
		//Go to department page				
		CategoryPage subCat= myAccount.getHeaderWidget()
				.goToCategoryPageByHierarchy(CategoryPage.class, 
						dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
						dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"));
						
		
		//click on product image on Category Display page.
		ProductDisplayPageB2B productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByNameB2B(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select the product attributes			
		productDisplay.getDefiningAttributesWidget()
			.selectAttributeFromDropdown(dsm.getInputParameter("ATTR1_NAME"), dsm.getInputParameter("ATTR1_VALUE"))
			.selectAttributeFromDropdown(dsm.getInputParameter("ATTR2_NAME"), dsm.getInputParameter("ATTR2_VALUE"))
			.selectAttributeFromDropdown(dsm.getInputParameter("ATTR3_NAME"), dsm.getInputParameter("ATTR3_VALUE"))
			.selectAttributeFromDropdown(dsm.getInputParameter("ATTR4_NAME"), dsm.getInputParameter("ATTR4_VALUE"));
		
		//change quantity of sku
		productDisplay.getSkuListWidget().updateQuantity(dsm.getInputParameter("PRODUCT_SKU"), dsm.getInputParameter("ITEM_QTY"));
		
		productDisplay.getSkuListWidget().verifySkuInStock(dsm.getInputParameter("PRODUCT_SKU"));
		//click Add to Current Order from Product Display page.
		productDisplay.addToCurrentOrder(dsm.getInputParameter("PRODUCT_SKU"));
		
		//verify quantity in cart
		productDisplay.getHeaderWidget().verifyMiniCartItemCount(dsm.getInputParameter("ITEM_QTY"));
		
		//Confirm the price on minishopping cart
		productDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
		
		//complete order
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
	
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();
		
		productDisplay.getHeaderWidget().openSignOutDropDownWidget().signOutB2B();
	
	}
	

	//cleanup method to remove all items from the cart before proceeding.
	
	private void cartCleanup(MyAccountMainPage page)
	{		
			
		//cart isn't 0, cleaning up
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		orders.removeAllItemsFromCart();		
	}
	
			
	
	
}
