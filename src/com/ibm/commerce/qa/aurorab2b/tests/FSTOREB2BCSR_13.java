package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.casl.keys.CaslKeysFactory;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.OrderConfirmationPage;
import com.ibm.commerce.qa.aurora.page.OrderDetailPage;
import com.ibm.commerce.qa.aurora.page.OrderSummarySingleShipPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.CustomerServiceFindOrderWidget;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.ProductDisplayPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.RequisitionListsPage;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;


/** 
 * Test scenario to test various use cases associated with Dropdown menu context
 * Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FSTOREB2BCSR_13 extends AbstractAuroraSingleSessionTests
{
	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	private boolean doTearDown = true;
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	private final CaslFixturesFactory f_CaslFixtures;

	

	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_CaslFixtures 
	 */	@Inject
	public FSTOREB2BCSR_13(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_CaslFixtures, CaslKeysFactory p_caslKeysFactory)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		f_CaslFixtures = p_CaslFixtures;
		
	}

	
	/** 
	 * Test case for  Customer Service Representative can shop on behalf of a Buyer in B2B context
	 */
	@Category(Sanity.class)
	@Test
	public void testFSTOREB2BCSR_1313()
	{

		 doTearDown = true;
		//Open the store in the browser.
			AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);

			//Open Sign In/Register page.
			SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();

			//Enter account User Name/Password and sign in.
			signInPage.typeUsername(dsm.getInputParameter("CSR_LOGONID"))
			.typePassword(dsm.getInputParameter("CSR_PASSWORD")).signIn();	
			
			String buyerAdmin = dsm.getInputParameter("BUYER_ADMIN_LOGONID");

			MyAccountMainPage buyerAdminAccountPage =  frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
					.typeLogonId(buyerAdmin).submitSearch()
					.clickActionButtonByPosition(1).clickAccessCustomerAccount();

			//Go to requisition list page from my account side bar
			RequisitionListsPage requisitionListsPage = buyerAdminAccountPage
			
					.getSidebar()
			
					.goToRequisitionListPage();
			
			//Go to product display page from catalog entry recommendation section
			ProductDisplayPageB2B pdp = requisitionListsPage
					
					.getCatalogEntryRecommendationWidget()
					
					.viewCatentryNumber(ProductDisplayPageB2B.class, 3);
		
			//Change quantity of sku
			pdp.getSkuListWidget()
					
					.updateQuantity(dsm.getInputParameter("SKU"), dsm.getInputParameter("QUANTITY"));
				
			//Click the add to current order button
			pdp.addToCurrentOrder(dsm.getInputParameter("SKU"));
			
			//Go to shopping cart page
			ShopCartPage shopCartPage = pdp.getHeaderWidget().goToShoppingCartPage();
		
			//Complete order checkout 
			shopCartPage.continueToNextStep()
			
					.selectPaymentMethod(dsm.getInputParameter("PAYMENT_TYPE"))
					
					.singleShipNext()
					
					.completeOrder()
					
					.verifyOrderSuccessful();
		
	}
	
	/** 
	 * Test case for  Customer Service Representative can shop on behalf of a buyerAdmin in B2B context
	 */
	@Category(Sanity.class)
	@Test
	public void testFSTOREB2BCSR_1314()
	{


		 doTearDown = true;
		//Open the store in the browser.
			AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);

			//Open Sign In/Register page.
			SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();

			//Enter account User Name/Password and sign in.
			signInPage.typeUsername(dsm.getInputParameter("CSR_LOGONID"))
			.typePassword(dsm.getInputParameter("CSR_PASSWORD")).signIn();	
			
			String buyer = dsm.getInputParameter("BUYER_LOGONID");

			MyAccountMainPage buyerAdminAccountPage =  frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
					.typeLogonId(buyer).submitSearch()
					.clickActionButtonByPosition(1).clickAccessCustomerAccount();

			//Go to requisition list page from my account side bar
			RequisitionListsPage requisitionListsPage = buyerAdminAccountPage
			
					.getSidebar()
			
					.goToRequisitionListPage();
			
			//Go to product display page from catalog entry recommendation section
			ProductDisplayPageB2B pdp = requisitionListsPage
					
					.getCatalogEntryRecommendationWidget()
					
					.viewCatentryNumber(ProductDisplayPageB2B.class, 3);
		
			//Change quantity of sku
			pdp.getSkuListWidget()
					
					.updateQuantity(dsm.getInputParameter("SKU"), dsm.getInputParameter("QUANTITY"));
				
			//Click the add to current order button
			pdp.addToCurrentOrder(dsm.getInputParameter("SKU"));
			
			//Go to shopping cart page
			ShopCartPage shopCartPage = pdp.getHeaderWidget().goToShoppingCartPage();
		
			//Complete order checkout 
			shopCartPage.continueToNextStep()
			
					.selectPaymentMethod(dsm.getInputParameter("PAYMENT_TYPE"))
					
					.singleShipNext()
					
					.completeOrder()
					
					.verifyOrderSuccessful();
		
		
	}
	
	/** 
	 * Test case for Customer Service Representative can see buyer admin links when shopping on behalf of a buyer admin
	 */
	@Category(Sanity.class)
	@Test
	public void testFSTOREB2BCSR_1315()
	{
		
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);

		//Open Sign In/Register page.
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();

		//Enter account User Name/Password and sign in.
		signInPage.typeUsername(dsm.getInputParameter("CSR_LOGONID"))
		.typePassword(dsm.getInputParameter("CSR_PASSWORD")).signIn();	

		String buyerAdmin = dsm.getInputParameter("BUYER_ADMIN_LOGONID");

		MyAccountMainPage buyerAdminAccountPage =  frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
				.typeLogonId(buyerAdmin).submitSearch()
				.clickActionButtonByPosition(1).clickAccessCustomerAccount();

		buyerAdminAccountPage.getSidebar().verifyOrgsAndBuyersLinkVisbility(true);
	}
	 
	/** 
	 * Test case for  Customer Service Supervisor can see order comments slider while on behalf of shopper
	 */
	@Test
	public void testFSTOREB2BCSR_1322()
	{

		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
	
		//Log in as a registered shopper 
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		signInPage.typeUsername(dsm.getInputParameter("BUYER_LOGONID"))
		.typePassword(dsm.getInputParameter("BUYER_PASSWORD")).signIn();
		
		//Complete a full shopping flow 
		//add product to cart
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("BUYER_LOGONID"), dsm.getInputParameter("BUYER_PASSWORD"), getConfig().getStoreName());
		orders.addItem(dsm.getInputParameter("SKU"), dsm.getInputParameterAsNumber("QTY", Double.class));
		
		//Get the order number of the shopping flow just completed 
		String orderId = orders.getCurrentOrderId();
		 
		//click on Shopping Cart link from the header
		
		ShopCartPage shopCart = frontPage.getHeaderWidget().goToShoppingCartPage();
		
		//Go to shipping and billing page
		ShippingAndBillingPage shippingAndBilling = shopCart.clickLockOrder().continueToNextStep();
		
		//Confirm product is available on the Shipping and Billing Page
		shippingAndBilling.verifyItemIsInCart(dsm.getInputParameter("PRODUCT"));
					
		//Confirm the correct Shipping Address is selected
		shippingAndBilling.selectShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"))
			.verifyShippingAddress(dsm.getInputParameter("DEFAULT_SHIPPING_ADDRESS"));
		
		//Confirm the correct Billing address is selected
		shippingAndBilling.selectBillingAddressByName(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"))
			.verifyBillingAddress(dsm.getInputParameter("DEFAULT_BILLING_ADDRESS"));
					
		//select Pay later as the payment method
		shippingAndBilling.selectPaymentMethod(dsm.getInputParameter("PAYMENT_METHOD"));
						
		//click next button
		OrderSummarySingleShipPage singleOrderSummary = shippingAndBilling.singleShipNext();
		
		//click Order button
		OrderConfirmationPage orderConfirmation = singleOrderSummary.completeOrder();
						
		//verify that the order has been placed
		orderConfirmation.verifyOrderSuccessful();
		
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage2 = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
	
		//Log in as a Customer Service Supervisor 
		SignInDropdownWidget  signInPage2 = frontPage2.getHeaderWidget().signIn();
		signInPage2.typeUsername(dsm.getInputParameter("CSS_LOGONID"))
		.typePassword(dsm.getInputParameter("CSS_PASSWORD")).signIn();
			
		//use find order widget to search for order 
		CustomerServiceFindOrderWidget findOrderWidget = frontPage.getHeaderWidget().goToCustomerService().getFindOrderWidget();
		findOrderWidget.typeOrderNumber(orderId)
			.submitSearch();
		
		//verify if search results are displayed for order ID 
		findOrderWidget.verifyOrderSearchResultIsDisplayed();
		
		//go to Order Details page
		OrderDetailPage orderDetailPage = findOrderWidget.clickOrderID(orderId);
		orderDetailPage.getCommentsBar().slideOrderComments();
		
		orderDetailPage.isSliderCommentTextAreaVisible();
			
		
		
	}
	 /**
	 * Tear down ran after every test case
	 */
	@After
	public void tearDown(){
		try
		{
			if (doTearDown) 
			{
				//Continue browser session starting at the home page.
				HeaderWidget headerSection = getSession().continueToPage(getConfig().getStoreUrl(), HeaderWidget.class);
	
				//Remove all items from the shopping cart
				headerSection.goToShoppingCartPage().clickLockOrder().removeAllItems().clickUnlockOrder()
				.getHeaderWidget().openSignOutDropDownWidget().signOut();
			
			
			}
		}
		catch(RuntimeException e)
		{
			getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
		}

	}
			 
	
}
