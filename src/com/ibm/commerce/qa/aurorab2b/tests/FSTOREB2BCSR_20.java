package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.casl.keys.CaslKeysFactory;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.OrganizationBuyerListWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AddBuyerPage;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.BuyerDetailsPage;
import com.ibm.commerce.qa.aurorab2b.page.CreateOrganizationPage;
import com.ibm.commerce.qa.aurorab2b.page.EditOrganizationPage;
import com.ibm.commerce.qa.aurorab2b.page.OrganizationsAndBuyersPage;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.wte.util.WcConfigManager;


/** 
 * Test scenario to test various use cases associated with Dropdown menu context
 * Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FSTOREB2BCSR_20 extends AbstractAuroraSingleSessionTests
{
	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	

	//A Variable to retrieve data from the data file. 
	@DataProvider
	private final TestDataProvider dsm;
	private static String lastCreatedOrg = "";
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with getConfig().properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_caslFixtures 
	 */		
	@Inject
	public FSTOREB2BCSR_20(
			Logger log, 
			WcConfigManager config,
			WcWteTestRule wcWebTestRule,
			CaslFoundationTestRule caslTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_caslFixtures, CaslKeysFactory p_caslKeysFactory)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
	}


	/** Test case to Add Organization
	 */
	@Category(Sanity.class)
	@Test
	public void testFSTOREB2BCSR_2001()
	{
		
		
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);

		//Open Sign In/Register page.
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();

		//Enter account User Name/Password and sign in.
		signInPage.typeUsername(dsm.getInputParameter("CSR_LOGONID"))
		.typePassword(dsm.getInputParameter("CSR_PASSWORD")).signIn();	
		
		String buyerAdmin = dsm.getInputParameter("BUYER_ADMIN_LOGONID");
		
		MyAccountMainPage buyerAdminAccountPage =  frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
		.typeLogonId(buyerAdmin).submitSearch()
		.clickActionButtonByPosition(dsm.getInputParameterAsNumber("INDEX", Integer.class)).clickAccessCustomerAccount();
		
		//Go to organizations and buyers page from my account side bar
		OrganizationsAndBuyersPage orgAndBuyersPage = buyerAdminAccountPage.getSidebar()

		.goToOrganizationsAndBuyersPage();
		
		lastCreatedOrg = dsm.getInputParameter("ORGANIZATION_NAME") + System.currentTimeMillis();
		
		//Go to create organization page.
		CreateOrganizationPage createOrgPage = orgAndBuyersPage.getOrganizationListWidget()
		
				.goToCreateOrganizationPage();
		
		//Enter new organization details.
		createOrgPage.getCreateOrganizationWidget()
				
				.getOrganizationDetailsWidget()
				
				.getOrganizationListWidget()
				
				.typeOrganizationName(lastCreatedOrg);
		
		createOrgPage.getCreateOrganizationWidget()
		
				.getOrganizationAddressWidget()
				
				.typeStreetAddress(dsm.getInputParameter("ADDRESS"))
				
				.typeCity(dsm.getInputParameter("CITY"))
				
				.typeZipCode(dsm.getInputParameter("ZIPCODE"));
				
		createOrgPage.getCreateOrganizationWidget()
		
				.getOrganizationContactInfoWidget()
				
				.typeEmail(dsm.getInputParameter("EMAIL"));
		
	
		//Click submit button to create organization
		EditOrganizationPage editOrgPage = createOrgPage.getCreateOrganizationWidget()
		
				.submit();
		
		//Go to organizations and buyers page from my account side bar
		orgAndBuyersPage = editOrgPage.getSidebar().goToOrganizationsAndBuyersPage();
		
	
	}
	
		
	/** Test case to Edit Organization
	 */
	@Category(Sanity.class)
	@Test
	public void testFSTOREB2BCSR_2002()
	{
		
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);

		//Open Sign In/Register page.
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();

		//Enter account User Name/Password and sign in.
		signInPage.typeUsername(dsm.getInputParameter("CSR_LOGONID"))
		.typePassword(dsm.getInputParameter("CSR_PASSWORD")).signIn();	
		
		String buyerAdmin = dsm.getInputParameter("BUYER_ADMIN_LOGONID");
		
		MyAccountMainPage buyerAdminAccountPage =  frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
				.typeLogonId(buyerAdmin).submitSearch()
				.clickActionButtonByPosition(dsm.getInputParameterAsNumber("INDEX", Integer.class)).clickAccessCustomerAccount();
				
		
		//Go to organizations and buyers page from my account side bar
		OrganizationsAndBuyersPage orgAndBuyersPage = buyerAdminAccountPage.getSidebar()

		.goToOrganizationsAndBuyersPage();

		//Click edit organization button
		EditOrganizationPage editPage = orgAndBuyersPage.getOrganizationListWidget()
			.verifyOrgListIsDropDown()
			.goToEditOrganizationPage();

		//Edit the address info for this organization
		editPage.getEditOrganizationWidget()
			.getOrganizationAddressWidget()
			.selectEditAddress()
			.typeStreetAddress(dsm.getInputParameter("STREET_UPDATE"))	
			.saveAddress()
			.verifyAddressDetail(dsm.getInputParameter("STREET_LINE_NUMBER"), 
				dsm.getInputParameter("STREET_UPDATE"));

		//Edit the contact info for this organization
		editPage.getEditOrganizationWidget()
			.getOrganizationContactInfoWidget()
			.selectEditContact()
			.typeEmail(dsm.getInputParameter("EMAIL_UPDATE"))
			.saveContactInfo()	
			.verifyEmailInfo(dsm.getInputParameter("EMAIL_UPDATE"));
	
	}

	
	
	/** Test case to  Add Buyer
	 */
	@Category(Sanity.class)
	@Test
	public void testFSTOREB2BCSR_2005()
	{
		
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);

		//Open Sign In/Register page.
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();

		//Enter account User Name/Password and sign in.
		signInPage.typeUsername(dsm.getInputParameter("CSR_LOGONID"))
		.typePassword(dsm.getInputParameter("CSR_PASSWORD")).signIn();	
		
		String buyerAdmin = dsm.getInputParameter("BUYER_ADMIN_LOGONID");
		String newBuyerLogin = dsm.getInputParameter("NEW_BUYER_LOGONID") + System.currentTimeMillis();
		MyAccountMainPage buyerAdminAccountPage =  frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
				.typeLogonId(buyerAdmin).submitSearch()
				.clickActionButtonByPosition(dsm.getInputParameterAsNumber("INDEX", Integer.class)).clickAccessCustomerAccount();
				
		
		//Go to organizations and buyers page from my account side bar
		OrganizationsAndBuyersPage orgAndBuyersPage = buyerAdminAccountPage.getSidebar()

		.goToOrganizationsAndBuyersPage();
		
		OrganizationBuyerListWidget orgBuyerListWidget = orgAndBuyersPage.getOrganizationBuyerListWidget();

		//Click add buyer from organization and buyer page 
		AddBuyerPage addBuyerPage = orgBuyerListWidget.goToAddBuyerPage();

			
		//Enter buyers detail info
		addBuyerPage
			
				.getBuyerDetailsWidget()
				
				.typeLogonId(newBuyerLogin)
				
				.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
				
				.typeLastName(dsm.getInputParameter("LAST_NAME"))
				
				.typeEmail(dsm.getInputParameter("EMAIL"));
		
		//Enter buyers address info
		addBuyerPage
		
				.getBuyerAddressWidget()
				
				.typeStreetAddress(dsm.getInputParameter("ADDRESS"))
				
				.typeZipCode(dsm.getInputParameter("ZIPCODE"))
				
				.typeCity(dsm.getInputParameter("CITY"));
				
		//Click submit buttom to create new buyer
		orgAndBuyersPage = addBuyerPage
		
				.submit();
		
		//Verify buyers account status
		orgAndBuyersPage.getOrganizationBuyerListWidget()
		
				.verifyBuyerAccountStatus(dsm.getInputParameter("STATUS"), newBuyerLogin);
		
	}
	
	/** 
	 * Test case to  Edit Buyer
	 */
	@Category(Sanity.class)
	@Test
	public void testFSTOREB2BCSR_2006()
	{
		
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);

		//Open Sign In/Register page.
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();

		//Enter account User Name/Password and sign in.
		signInPage.typeUsername(dsm.getInputParameter("CSR_LOGONID"))
		.typePassword(dsm.getInputParameter("CSR_PASSWORD")).signIn();	
		
		String buyerAdmin = dsm.getInputParameter("BUYER_ADMIN_LOGONID");

		MyAccountMainPage buyerAdminAccountPage =  frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
				.typeLogonId(buyerAdmin).submitSearch()
				.clickActionButtonByPosition(dsm.getInputParameterAsNumber("INDEX", Integer.class)).clickAccessCustomerAccount();

				
		//Go to organizations and buyers page from my account side bar
		OrganizationsAndBuyersPage orgAndBuyersPage = buyerAdminAccountPage.getSidebar()

		.goToOrganizationsAndBuyersPage();
	//Go to buyer detail page
			BuyerDetailsPage buyerDetailsPage = orgAndBuyersPage.getOrganizationBuyerListWidget()
			
					.goToOrganizationBuyerDetailPage(dsm.getInputParameter("BUYER_LOGONID"));
			
			//Edit buyers detail info
			buyerDetailsPage.getBuyerDetailsWidget()
			
					.getOrganizationBuyerDetails()
					
					.selectEditBuyerDetails()
					
					.typeFirstName(dsm.getInputParameter("FIRST_NAME_UPDATE"))
					
					.typeLastName(dsm.getInputParameter("LAST_NAME_UPDATE"))
					
					.saveBuyerDetail();
			
			//Verify buyers detail
			buyerDetailsPage.getBuyerDetailsWidget()
			
					.getOrganizationBuyerDetails()
					
					.verifyFullName(dsm.getInputParameter("FULL_NAME_UPDATE"));
			
			//Edit buyers detail info
			buyerDetailsPage.getBuyerDetailsWidget()
			
					.getOrganizationBuyerDetails()
					
					.selectEditBuyerDetails()
					
					.typeFirstName(dsm.getInputParameter("FIRST_NAME"))
					
					.typeLastName(dsm.getInputParameter("LAST_NAME"))
					
					.saveBuyerDetail();
			
			//Verify buyers detail
			buyerDetailsPage.getBuyerDetailsWidget()
			
					.getOrganizationBuyerDetails()
					
					.verifyFullName(dsm.getInputParameter("FULL_NAME"));
	}
	
	/** 
	 * Test case to  Edit Buyer
	 */
	@Category(Sanity.class)
	@Test
	public void testFSTOREB2BCSR_2009()
	{
		
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);

		//Open Sign In/Register page.
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();

		//Enter account User Name/Password and sign in.
		signInPage.typeUsername(dsm.getInputParameter("CSR_LOGONID"))
		.typePassword(dsm.getInputParameter("CSR_PASSWORD")).signIn();	
		
		String buyerUsername = dsm.getInputParameter("BUYER_LOGONID");
		String buyerAdmin = dsm.getInputParameter("BUYER_ADMIN_LOGONID");

		MyAccountMainPage buyerAdminAccountPage =  frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
				.typeLogonId(buyerAdmin).submitSearch()
				.clickActionButtonByPosition(dsm.getInputParameterAsNumber("INDEX", Integer.class)).clickAccessCustomerAccount();
				
		
		//Go to organizations and buyers page from my account side bar
		OrganizationsAndBuyersPage orgAndBuyersPage = buyerAdminAccountPage.getSidebar()

		.goToOrganizationsAndBuyersPage();
		
		//Disable buyers account
		orgAndBuyersPage
			
				.getOrganizationBuyerListWidget()
				
				.selectDisableUserAccount(buyerUsername)
				
				//Verify buyers account status is updated
				.verifyBuyerAccountStatus(dsm.getInputParameter("STATUS2"), buyerUsername);
		
		//Enable buyers account
				orgAndBuyersPage
					
						.getOrganizationBuyerListWidget()
						
						.selectEnableUserAccount(buyerUsername)
						
						//Verify buyers account status is updated
						.verifyBuyerAccountStatus(dsm.getInputParameter("STATUS1"), buyerUsername);
		
		
	}
	
	
	
}
