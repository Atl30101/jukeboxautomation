package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 * 
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.CustomerRegisterationPageB2B;
import com.ibm.commerce.qa.casl.util.CaslModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.OrgAdminConsole;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/** Scenario: PreSetup
 *  Details: This test should be run before any other tests in the bucket to ensure that the users common to many test
 *  cases are registered in the store and tooling as appropriate.
 */
@Category(Sanity.class)
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class, CaslModule.class})
public class FV2STOREB2B_00 extends AbstractAuroraSingleSessionTests
{
	   /**
	    * The internal copyright field.
     	*/
		public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

		//A variable to send requests to OrgadminConsole
		private OrgAdminConsole oac;
		
		@DataProvider
		private final TestDataProvider dsm;

		/**
		 * Test Class object constructor.
		 * 
		 * @param log
		 * @param config
		 * @param session
		 * @param oac
		 * @param dataSetManager
		 */
		@Inject
		public FV2STOREB2B_00(
				Logger log, 
				CaslFoundationTestRule caslTestRule,
				WcWteTestRule wcWteTestRule,
				OrgAdminConsole oac,
				TestDataProvider dataSetManager)
		{
			super(log, wcWteTestRule, caslTestRule);
			this.oac = oac;
			this.dsm = dataSetManager;
		}

	  /**
		* Creates administrative users that will be utilized in other scenarios thru out this bucket.
		* Pre-conditions: <ul>
		*					<li>Tester knows the Store URL (http://<hostname>/webapp/wcs/stores/servlet/en/auroraesite)</li>
		* 				</ul>
		* Post conditions: Registration Successful. The user is taken to My Account page.
		* @throws Exception
		*/
	@Test
	public void testCreateAdminUsers() throws Exception
	{
		//Logon to orgadminconsole using the admin account
		oac.logon(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"));
		
		//Create a Marketing Manager account 
		oac.createNewUser(dsm.getInputParameter("MKR_USERID"), 
				dsm.getInputParameter("MKR_FIRST_NAME") , 
				dsm.getInputParameter("MKR_LAST_NAME"), 
				dsm.getInputParameter("MKR_USERPASSWORD"), 
				dsm.getInputParameter("MKR_PASSWORD_VERIFY"), 
				dsm.getInputParameter("MKR_POLICYID"), 
				dsm.getInputParameter("MKR_STATUS"), 
				dsm.getInputParameter("MKR_LANGUAGE"), 
				dsm.getInputParameter("MKR_PARENTORG"), 
				dsm.getInputParameter("MKR_ADDRESS"), 
				dsm.getInputParameter("MKR_CITY"), 
				dsm.getInputParameter("MKR_STATE"), 
				dsm.getInputParameter("MKR_COUNTRY"), 
				dsm.getInputParameter("MKR_ZIPCODE"), 
				dsm.getInputParameter("MKR_EMAIL"), 
				dsm.getInputParameter("MKR_PASSWORD_EXPIRED"));
		
		
		//Assign role as a Marketing Manager
		oac.assignRoleToUser(dsm.getInputParameter("MKR_USERID"),
				dsm.getInputParameter("MKR_PARENTORG"), dsm.getInputParameter("MKR_ROLE"));

		//Create a Product Manager account 
		oac.createNewUser(dsm.getInputParameter("P_USERID"), 
				dsm.getInputParameter("P_FIRST_NAME") , 
				dsm.getInputParameter("P_LAST_NAME"), 
				dsm.getInputParameter("P_USERPASSWORD"), 
				dsm.getInputParameter("P_PASSWORD_VERIFY"), 
				dsm.getInputParameter("P_POLICYID"), 
				dsm.getInputParameter("P_STATUS"), 
				dsm.getInputParameter("P_LANGUAGE"), 
				dsm.getInputParameter("P_PARENTORG"), 
				dsm.getInputParameter("P_ADDRESS"), 
				dsm.getInputParameter("P_CITY"), 
				dsm.getInputParameter("P_STATE"), 
				dsm.getInputParameter("P_COUNTRY"), 
				dsm.getInputParameter("P_ZIPCODE"), 
				dsm.getInputParameter("P_EMAIL"), 
				dsm.getInputParameter("P_PASSWORD_EXPIRED"));
		
		//Assign role as a Product Manager
		oac.assignRoleToUser(dsm.getInputParameter("P_USERID"),dsm.getInputParameter("P_PARENTORG"), dsm.getInputParameter("P_ROLE"));
		
		//Create a Category Manager 
		oac.createNewUser(dsm.getInputParameter("C_USERID"), 
				dsm.getInputParameter("C_FIRST_NAME") , 
				dsm.getInputParameter("C_LAST_NAME"), 
				dsm.getInputParameter("C_USERPASSWORD"), 
				dsm.getInputParameter("C_PASSWORD_VERIFY"), 
				dsm.getInputParameter("C_POLICYID"), 
				dsm.getInputParameter("C_STATUS"), 
				dsm.getInputParameter("C_LANGUAGE"), 
				dsm.getInputParameter("C_PARENTORG"), 
				dsm.getInputParameter("C_ADDRESS"), 
				dsm.getInputParameter("C_CITY"), 
				dsm.getInputParameter("C_STATE"), 
				dsm.getInputParameter("C_COUNTRY"), 
				dsm.getInputParameter("C_ZIPCODE"), 
				dsm.getInputParameter("C_EMAIL"), 
				dsm.getInputParameter("C_PASSWORD_EXPIRED"));
		
		//Assign role as a Category Manager
		oac.assignRoleToUser(dsm.getInputParameter("C_USERID"),dsm.getInputParameter("C_PARENTORG"), dsm.getInputParameter("C_ROLE"));
		
		//Create a Seller account 
		oac.createNewUser(dsm.getInputParameter("S_USERID"), 
				dsm.getInputParameter("S_FIRST_NAME") , 
				dsm.getInputParameter("S_LAST_NAME"), 
				dsm.getInputParameter("S_USERPASSWORD"), 
				dsm.getInputParameter("S_PASSWORD_VERIFY"), 
				dsm.getInputParameter("S_POLICYID"), 
				dsm.getInputParameter("S_STATUS"), 
				dsm.getInputParameter("S_LANGUAGE"), 
				dsm.getInputParameter("S_PARENTORG"), 
				dsm.getInputParameter("S_ADDRESS"), 
				dsm.getInputParameter("S_CITY"), 
				dsm.getInputParameter("S_STATE"), 
				dsm.getInputParameter("S_COUNTRY"), 
				dsm.getInputParameter("S_ZIPCODE"), 
				dsm.getInputParameter("S_EMAIL"), 
				dsm.getInputParameter("S_PASSWORD_EXPIRED"));
		
		//Assign role as a Seller Manager
		oac.assignRoleToUser(dsm.getInputParameter("S_USERID"),dsm.getInputParameter("S_PARENTORG"), dsm.getInputParameter("S_ROLE"));
		
		//Update Password for buyerAadmin
		oac.updateUserPassword(dsm.getInputParameter("B_USERID"),dsm.getInputParameter("B_USERPASSWORD"), dsm.getInputParameter("B_PASSWORD_VERIFY"));
		
		//Log off from orgadminconsole
		oac.logoff();
					
	}
	
	
	/**
	 * Test case to register a Shopper from the store home page with valid inputs. This shopper can be used across multiple test cases in this bucket.
	 * @throws Exception
	 */
	@Test
	public void setupUser() throws Exception{
		//Open Auroraesite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();
		
		//attempt to sign in:
		try {
			signInPage.typeUsername(dsm.getInputParameter("LOGONID"))
				.typePassword(dsm.getInputParameter("PASSWORD"))
				.signIn();
			
			//Open Auroraesite store
			frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

			
		} catch (Exception e1) {
			//Open Auroraesite store
			

			//Click on the registration button on the SignIn page
			CustomerRegisterationPageB2B rp = signInPage.registerB2B();

			rp.selectOrganizationRegister()
			
			//type organization name
			.typeOrganizationName(dsm.getInputParameter("ORGANIZATION_NAME"))
			
			//type organization address
			.typeOrganizationStreetAddressLine1(dsm.getInputParameter("ORGANIZATION_ADDRESS"))
			
			//select organization country
			.selectOrganizationCountryOrRegion(dsm.getInputParameter("ORGANIZATION_COUNTRY"))
			
			//select organization province
			.selectOrganizationStateOrProvince(dsm.getInputParameter("ORGANIZATION_STATE"))
			
			//type organization city
			.typeOrganizationCity(dsm.getInputParameter("ORGANIZATION_CITY"))
			
			//type organization zipcode
			.typeOrganizationZipCode(dsm.getInputParameter("ORGANIZATION_ZIPCODE"))
			
			//type organization email
			.typeOrganizationEmail(dsm.getInputParameter("ORGANIZATION_EMAIL"))
			
			//type organization phone number
			.typeOrganizationPhoneNumber(dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))

			//type buyer username
			.typeOrganizationBuyerLogonId(dsm.getInputParameter("LOGONID"))

			//type buyer password
			.typeOrganizationBuyerPassword(dsm.getInputParameter("PASSWORD"))

			//Verify buyer password verify
			.typeOrganizationBuyerVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))

			//type buyer first name
			.typeOrganizationBuyerFirstName(dsm.getInputParameter("FIRST_NAME"))

			//type buyer last name
			.typeOrganizationBuyerLastName(dsm.getInputParameter("LAST_NAME"))

			//type buyer street address
			.typeOrganizationBuyerStreetAddressLine1(dsm.getInputParameter("ADDRESS"))

			//Select buyer country
			.selectOrganizationBuyerCountryOrRegion(dsm.getInputParameter("COUNTRY"))

			//type or select state for buyer
			.selectOrganizationBuyerStateOrProvince(dsm.getInputParameter("STATE"))

			//type buyer city
			.typeOrganizationBuyerCity(dsm.getInputParameter("CITY"))

			//type buyer zipcode
			.typeOrganizationBuyerZipCode(dsm.getInputParameter("ZIPCODE"))

			//type buyer E-mail
			.typeOrganizationBuyerEmail(dsm.getInputParameter("EMAIL"))

			//type buyer home phone number
			.typeOrganizationBuyerPhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))

			//Check if preferred language drop down is visible
			.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			

			//Select buyer preferred language
			.selectOrganizationBuyerPreferedCurrency(dsm.getInputParameter("PREFERRED_CURRENCY"))

			//Check if preferred currency drop down is visible
			.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()

			//Select buyer preferred language
			.selectOrganizationBuyerPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))

			//Submit registration
			.submitOrganizationBuyerRegistration()
			
			//verify registration confirmation message
			.verifyOrganizationBuyerRegistrationConfirmationMessage();
			
			// Approve Organization
				oac.logon(dsm.getInputParameter("ACCELERATOR_LOGON_ID"), dsm.getInputParameter("ACCELERATOR_PASSWORD"));
				oac.approveAllApprovals();
			
		}
		
		
	}	
}

