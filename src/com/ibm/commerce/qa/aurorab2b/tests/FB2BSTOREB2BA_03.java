package com.ibm.commerce.qa.aurorab2b.tests;

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.CustomerRegisterationPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.SignInPageB2B;
import com.ibm.commerce.qa.casl.util.CaslModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.OrgAdminConsole;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
/**
 * Scenario: FSSC-92726-03
 * Details: Organization registration test scenario
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class, CaslModule.class})
public class FB2BSTOREB2BA_03 extends AbstractAuroraSingleSessionTests {

	private final OrgAdminConsole oac;

	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider f_dsm;

	/**
	 * Test Class object constructor.
	 * 
	 */
	@Inject
	public FB2BSTOREB2BA_03(
			Logger log, 
			WcWteTestRule wcWteTestRule,
			CaslFoundationTestRule caslTestRule,
			TestDataProvider p_dsm,
			OrgAdminConsole p_oac
			) throws Exception
	{
		super(log, wcWteTestRule, caslTestRule);

		f_dsm = p_dsm;

		oac = p_oac;
	}

	public void turnOffApprovals(String organization, String approval) throws Exception
	{
		// Approve Organization
		oac.logon(f_dsm.getInputParameter("ACCELERATOR_LOGON_ID"), f_dsm.getInputParameter("ACCELERATOR_PASSWORD"));
		oac.approveAllApprovals();

		//Disable buyer approval inherited attributes for this organization
		oac.addApprovalsToOrganization(organization, approval);
	}
	
	/**
	 * Test case register organization.
	 */
	@Test
	public void FB2BSTOREB2BA_03_01() throws Exception
	{
		//erase things
		//f_dbi.executeUpdate("delete from member where member_id = (select users_id from users where dn like '%admin01%') or member_id = (select orgentity_id from orgentity where orgentityname = 'testOrg01')");

		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPanel.registerB2B();

		String username = f_dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID") + System.currentTimeMillis();

		SignInPageB2B signInPage = rp.selectOrganizationRegister()

				//type organization name
				.typeOrganizationName(f_dsm.getInputParameter("ORGANIZATION_NAME") + System.currentTimeMillis())

				//type organization address
				.typeOrganizationStreetAddressLine1(f_dsm.getInputParameter("ORGANIZATION_ADDRESS"))

				//select organization country
				.selectOrganizationCountryOrRegion(f_dsm.getInputParameter("ORGANIZATION_COUNTRY"))

				//select organization province
				.selectOrganizationStateOrProvince(f_dsm.getInputParameter("ORGANIZATION_STATE"))

				//type organization city
				.typeOrganizationCity(f_dsm.getInputParameter("ORGANIZATION_CITY"))

				//type organization zipcode
				.typeOrganizationZipCode(f_dsm.getInputParameter("ORGANIZATION_ZIPCODE"))

				//type organization email
				.typeOrganizationEmail(f_dsm.getInputParameter("ORGANIZATION_EMAIL"))

				//type organization phone number
				.typeOrganizationPhoneNumber(f_dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))

				//type buyer username
				.typeOrganizationBuyerLogonId(username)

				//type buyer password
				.typeOrganizationBuyerPassword(f_dsm.getInputParameter("PASSWORD"))

				//Verify buyer password verify
				.typeOrganizationBuyerVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))

				//type buyer first name
				.typeOrganizationBuyerFirstName(f_dsm.getInputParameter("FIRST_NAME"))

				//type buyer last name
				.typeOrganizationBuyerLastName(f_dsm.getInputParameter("LAST_NAME"))

				//type buyer street address
				.typeOrganizationBuyerStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))

				//Select buyer country
				.selectOrganizationBuyerCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))

				//type or select state for buyer
				.selectOrganizationBuyerStateOrProvince(f_dsm.getInputParameter("STATE"))

				//type buyer city
				.typeOrganizationBuyerCity(f_dsm.getInputParameter("CITY"))

				//type buyer zipcode
				.typeOrganizationBuyerZipCode(f_dsm.getInputParameter("ZIPCODE"))

				//type buyer E-mail
				.typeOrganizationBuyerEmail(f_dsm.getInputParameter("EMAIL"))

				//type buyer home phone number
				.typeOrganizationBuyerPhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))

				//Check if preferred language drop down is visible
				.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			

				//Select buyer preferred language
				.selectOrganizationBuyerPreferedCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))

				//Check if preferred currency drop down is visible
				.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()

				//Select buyer preferred language
				.selectOrganizationBuyerPreferedLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))

				//Submit registration
				.submitOrganizationBuyerRegistration();

		//verify registration confirmation message
		signInPage.verifyOrganizationBuyerRegistrationConfirmationMessage();

		// Approve Organization
		oac.logon(f_dsm.getInputParameter("ACCELERATOR_LOGON_ID"), f_dsm.getInputParameter("ACCELERATOR_PASSWORD"));
		oac.approveAllApprovals();

		//Enter a valid username
		signInPage.typeUsername(username)

		//Enter a valid password
		.typePassword(f_dsm.getInputParameter("PASSWORD"))

		//Click Sign In
		.signIn();
	}

	/**
	 * Test case register organization and sign in before approved.
	 */
	@Test
	public void FB2BSTOREB2BA_03_02() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPanel.registerB2B();

		String username = f_dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID") + System.currentTimeMillis();

		SignInPageB2B signInPage = rp.selectOrganizationRegister()

				//type organization name
				.typeOrganizationName(f_dsm.getInputParameter("ORGANIZATION_NAME") + System.currentTimeMillis())

				//type organization address
				.typeOrganizationStreetAddressLine1(f_dsm.getInputParameter("ORGANIZATION_ADDRESS"))

				//select organization country
				.selectOrganizationCountryOrRegion(f_dsm.getInputParameter("ORGANIZATION_COUNTRY"))

				//select organization province
				.selectOrganizationStateOrProvince(f_dsm.getInputParameter("ORGANIZATION_STATE"))

				//type organization city
				.typeOrganizationCity(f_dsm.getInputParameter("ORGANIZATION_CITY"))

				//type organization zipcode
				.typeOrganizationZipCode(f_dsm.getInputParameter("ORGANIZATION_ZIPCODE"))

				//type organization email
				.typeOrganizationEmail(f_dsm.getInputParameter("ORGANIZATION_EMAIL"))

				//type organization phone number
				.typeOrganizationPhoneNumber(f_dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))

				//type buyer username
				.typeOrganizationBuyerLogonId(username)

				//type buyer password
				.typeOrganizationBuyerPassword(f_dsm.getInputParameter("PASSWORD"))

				//Verify buyer password verify
				.typeOrganizationBuyerVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))

				//type buyer first name
				.typeOrganizationBuyerFirstName(f_dsm.getInputParameter("FIRST_NAME"))

				//type buyer last name
				.typeOrganizationBuyerLastName(f_dsm.getInputParameter("LAST_NAME"))

				//type buyer street address
				.typeOrganizationBuyerStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))

				//Select buyer country
				.selectOrganizationBuyerCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))

				//type or select state for buyer
				.selectOrganizationBuyerStateOrProvince(f_dsm.getInputParameter("STATE"))

				//type buyer city
				.typeOrganizationBuyerCity(f_dsm.getInputParameter("CITY"))

				//type buyer zipcode
				.typeOrganizationBuyerZipCode(f_dsm.getInputParameter("ZIPCODE"))

				//type buyer E-mail
				.typeOrganizationBuyerEmail(f_dsm.getInputParameter("EMAIL"))

				//type buyer home phone number
				.typeOrganizationBuyerPhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))

				//Check if preferred language drop down is visible
				.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			

				//Select buyer preferred language
				.selectOrganizationBuyerPreferedCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))

				//Check if preferred currency drop down is visible
				.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()

				//Select buyer preferred language
				.selectOrganizationBuyerPreferedLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))

				//Submit registration
				.submitOrganizationBuyerRegistration();

		//verify registration confirmation message
		signInPage.verifyOrganizationBuyerRegistrationConfirmationMessage()

		//Enter a valid username
		.typeUsername(username)

		//Enter a valid password
		.typePassword(f_dsm.getInputParameter("PASSWORD"))

		//Click Sign In
		.signIn();

		//Confirm not approved message
		signInPage.verifyNotApprovedMessage();
	}


	/**
	 * Test case register organization with one mandatory field left blank.
	 */
	@Test
	public void FB2BSTOREB2BA_03_04() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPage.registerB2B();

		CustomerRegisterationPageB2B regpage = rp.selectOrganizationRegister()

				//type organization name
				.typeOrganizationName(f_dsm.getInputParameter("ORGANIZATION_NAME") + System.currentTimeMillis())

				//type organization address
				.typeOrganizationStreetAddressLine1(f_dsm.getInputParameter("ORGANIZATION_ADDRESS"))

				//select organization country
				.selectOrganizationCountryOrRegion(f_dsm.getInputParameter("ORGANIZATION_COUNTRY"))

				//select organization province
				.selectOrganizationStateOrProvince(f_dsm.getInputParameter("ORGANIZATION_STATE"))

				//type organization city
				.typeOrganizationCity(f_dsm.getInputParameter("ORGANIZATION_CITY"))

				//type organization zipcode
				.typeOrganizationZipCode(f_dsm.getInputParameter("EMPTY_ZIPCODE"))

				//type organization email
				.typeOrganizationEmail(f_dsm.getInputParameter("ORGANIZATION_EMAIL"))

				//type organization phone number
				.typeOrganizationPhoneNumber(f_dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))

				//type buyer username
				.typeOrganizationBuyerLogonId(f_dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID") + System.currentTimeMillis())

				//type buyer password
				.typeOrganizationBuyerPassword(f_dsm.getInputParameter("PASSWORD"))

				//Verify buyer password verify
				.typeOrganizationBuyerVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))

				//type buyer first name
				.typeOrganizationBuyerFirstName(f_dsm.getInputParameter("FIRST_NAME"))

				//type buyer last name
				.typeOrganizationBuyerLastName(f_dsm.getInputParameter("LAST_NAME"))

				//type buyer street address
				.typeOrganizationBuyerStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))

				//Select buyer country
				.selectOrganizationBuyerCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))

				//type or select state for buyer
				.selectOrganizationBuyerStateOrProvince(f_dsm.getInputParameter("STATE"))

				//type buyer city
				.typeOrganizationBuyerCity(f_dsm.getInputParameter("CITY"))

				//type buyer zipcode
				.typeOrganizationBuyerZipCode(f_dsm.getInputParameter("ZIPCODE"))

				//type buyer E-mail
				.typeOrganizationBuyerEmail(f_dsm.getInputParameter("EMAIL"))

				//type buyer home phone number
				.typeOrganizationBuyerPhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))

				//Check if preferred language drop down is visible
				.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			

				//Select buyer preferred language
				.selectOrganizationBuyerPreferedCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))

				//Check if preferred currency drop down is visible
				.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()

				//Select buyer preferred language
				.selectOrganizationBuyerPreferedLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))

				//Submit registration
				.submitUnsuccessfulOrganizationRegistration()

				//verify registration message appears for empty field
				.verifyEmptyTooltipPresent();

		//type organization zipcode
		regpage.typeOrganizationZipCode(f_dsm.getInputParameter("ORGANIZATION_ZIPCODE"))

		//Resubmit registration
		.submitOrganizationBuyerRegistration()

		//Confirmation
		.verifyOrganizationBuyerRegistrationConfirmationMessage();
	}

	/**
	 * Test case register organization with two or more mandatory fields left blank.
	 */
	@Test
	public void FB2BSTOREB2BA_03_05() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPage.registerB2B();

		CustomerRegisterationPageB2B regpage = rp.selectOrganizationRegister()

				//type organization name
				.typeOrganizationName(f_dsm.getInputParameter("ORGANIZATION_NAME") + System.currentTimeMillis())

				//type organization address
				.typeOrganizationStreetAddressLine1(f_dsm.getInputParameter("ORGANIZATION_ADDRESS"))

				//select organization country
				.selectOrganizationCountryOrRegion(f_dsm.getInputParameter("ORGANIZATION_COUNTRY"))

				//select organization province
				.selectOrganizationStateOrProvince(f_dsm.getInputParameter("ORGANIZATION_STATE"))

				//type organization city
				.typeOrganizationCity(f_dsm.getInputParameter("INVALID_ORGANIZATION_CITY"))

				//type organization zipcode
				.typeOrganizationZipCode(f_dsm.getInputParameter("INVALID_ORGANIZATION_ZIPCODE"))

				//type organization email
				.typeOrganizationEmail(f_dsm.getInputParameter("ORGANIZATION_EMAIL"))

				//type organization phone number
				.typeOrganizationPhoneNumber(f_dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))

				//type buyer username
				.typeOrganizationBuyerLogonId(f_dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID") + System.currentTimeMillis())

				//type buyer password
				.typeOrganizationBuyerPassword(f_dsm.getInputParameter("PASSWORD"))

				//Verify buyer password verify
				.typeOrganizationBuyerVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))

				//type buyer first name
				.typeOrganizationBuyerFirstName(f_dsm.getInputParameter("FIRST_NAME"))

				//type buyer last name
				.typeOrganizationBuyerLastName(f_dsm.getInputParameter("LAST_NAME"))

				//type buyer street address
				.typeOrganizationBuyerStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))

				//Select buyer country
				.selectOrganizationBuyerCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))

				//type or select state for buyer
				.selectOrganizationBuyerStateOrProvince(f_dsm.getInputParameter("STATE"))

				//type buyer city
				.typeOrganizationBuyerCity(f_dsm.getInputParameter("CITY"))

				//type buyer zipcode
				.typeOrganizationBuyerZipCode(f_dsm.getInputParameter("ZIPCODE"))

				//type buyer E-mail
				.typeOrganizationBuyerEmail(f_dsm.getInputParameter("EMAIL"))

				//type buyer home phone number
				.typeOrganizationBuyerPhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))

				//Check if preferred language drop down is visible
				.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			

				//Select buyer preferred language
				.selectOrganizationBuyerPreferedCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))

				//Check if preferred currency drop down is visible
				.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()

				//Select buyer preferred language
				.selectOrganizationBuyerPreferedLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))

				//Submit registration
				.submitUnsuccessfulOrganizationRegistration()

				//verify registration message appears for empty field
				.verifyEmptyTooltipPresent();

		regpage.typeOrganizationCity(f_dsm.getInputParameter("ORGANIZATION_CITY"))

		.typeOrganizationZipCode(f_dsm.getInputParameter("ORGANIZATION_ZIPCODE"))

		//Resubmit registration
		.submitOrganizationBuyerRegistration()

		//Confirmation
		.verifyOrganizationBuyerRegistrationConfirmationMessage();
	}

	/**
	 * Test case register organization with invalid entries.
	 */
	@Test
	public void FB2BSTOREB2BA_03_06() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPage.registerB2B();

		CustomerRegisterationPageB2B regpage = rp.selectOrganizationRegister()

				//type organization name
				.typeOrganizationName(f_dsm.getInputParameter("ORGANIZATION_NAME") + System.currentTimeMillis())

				//type organization address
				.typeOrganizationStreetAddressLine1(f_dsm.getInputParameter("ORGANIZATION_ADDRESS"))

				//select organization country
				.selectOrganizationCountryOrRegion(f_dsm.getInputParameter("ORGANIZATION_COUNTRY"))

				//select organization province
				.selectOrganizationStateOrProvince(f_dsm.getInputParameter("ORGANIZATION_STATE"))

				//type organization city
				.typeOrganizationCity(f_dsm.getInputParameter("ORGANIZATION_CITY"))

				//type organization zipcode
				.typeOrganizationZipCode(f_dsm.getInputParameter("ORGANIZATION_ZIPCODE"))

				//type organization email
				.typeOrganizationEmail(f_dsm.getInputParameter("INVALID_ORGANIZATION_EMAIL"))

				//type organization phone number
				.typeOrganizationPhoneNumber(f_dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))

				//type buyer username
				.typeOrganizationBuyerLogonId(f_dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID") + System.currentTimeMillis())

				//type buyer password
				.typeOrganizationBuyerPassword(f_dsm.getInputParameter("PASSWORD"))

				//Verify buyer password verify
				.typeOrganizationBuyerVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))

				//type buyer first name
				.typeOrganizationBuyerFirstName(f_dsm.getInputParameter("FIRST_NAME"))

				//type buyer last name
				.typeOrganizationBuyerLastName(f_dsm.getInputParameter("LAST_NAME"))

				//type buyer street address
				.typeOrganizationBuyerStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))

				//Select buyer country
				.selectOrganizationBuyerCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))

				//type or select state for buyer
				.selectOrganizationBuyerStateOrProvince(f_dsm.getInputParameter("STATE"))

				//type buyer city
				.typeOrganizationBuyerCity(f_dsm.getInputParameter("CITY"))

				//type buyer zipcode
				.typeOrganizationBuyerZipCode(f_dsm.getInputParameter("ZIPCODE"))

				//type buyer E-mail
				.typeOrganizationBuyerEmail(f_dsm.getInputParameter("EMAIL"))

				//type buyer home phone number
				.typeOrganizationBuyerPhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))

				//Check if preferred language drop down is visible
				.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			

				//Select buyer preferred language
				.selectOrganizationBuyerPreferedCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))

				//Check if preferred currency drop down is visible
				.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()

				//Select buyer preferred language
				.selectOrganizationBuyerPreferedLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))

				//Submit registration
				.submitUnsuccessfulOrganizationRegistration()

				//verify registration message appears for invalid field
				.verifyInvalidTooltipPresent();

		regpage.typeOrganizationEmail(f_dsm.getInputParameter("ORGANIZATION_EMAIL"))

		//Resubmit registration
		.submitOrganizationBuyerRegistration()

		//Confirmation
		.verifyOrganizationBuyerRegistrationConfirmationMessage();
	}

	/**
	 * Test case register organization with password mismatch for the administrative user.
	 */
	@Test
	public void FB2BSTOREB2BA_03_08() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPage.registerB2B();

		CustomerRegisterationPageB2B regpage = rp.selectOrganizationRegister()

				//type organization name
				.typeOrganizationName(f_dsm.getInputParameter("ORGANIZATION_NAME") + System.currentTimeMillis())

				//type organization address
				.typeOrganizationStreetAddressLine1(f_dsm.getInputParameter("ORGANIZATION_ADDRESS"))

				//select organization country
				.selectOrganizationCountryOrRegion(f_dsm.getInputParameter("ORGANIZATION_COUNTRY"))

				//select organization province
				.selectOrganizationStateOrProvince(f_dsm.getInputParameter("ORGANIZATION_STATE"))

				//type organization city
				.typeOrganizationCity(f_dsm.getInputParameter("ORGANIZATION_CITY"))

				//type organization zipcode
				.typeOrganizationZipCode(f_dsm.getInputParameter("ORGANIZATION_ZIPCODE"))

				//type organization email
				.typeOrganizationEmail(f_dsm.getInputParameter("ORGANIZATION_EMAIL"))

				//type organization phone number
				.typeOrganizationPhoneNumber(f_dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))

				//type buyer username
				.typeOrganizationBuyerLogonId(f_dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID") + System.currentTimeMillis())

				//type buyer password
				.typeOrganizationBuyerPassword(f_dsm.getInputParameter("INVALID_PASSWORD"))

				//Verify buyer password verify
				.typeOrganizationBuyerVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))

				//type buyer first name
				.typeOrganizationBuyerFirstName(f_dsm.getInputParameter("FIRST_NAME"))

				//type buyer last name
				.typeOrganizationBuyerLastName(f_dsm.getInputParameter("LAST_NAME"))

				//type buyer street address
				.typeOrganizationBuyerStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))

				//Select buyer country
				.selectOrganizationBuyerCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))

				//type or select state for buyer
				.selectOrganizationBuyerStateOrProvince(f_dsm.getInputParameter("STATE"))

				//type buyer city
				.typeOrganizationBuyerCity(f_dsm.getInputParameter("CITY"))

				//type buyer zipcode
				.typeOrganizationBuyerZipCode(f_dsm.getInputParameter("ZIPCODE"))

				//type buyer E-mail
				.typeOrganizationBuyerEmail(f_dsm.getInputParameter("EMAIL"))

				//type buyer home phone number
				.typeOrganizationBuyerPhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))

				//Check if preferred language drop down is visible
				.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			

				//Select buyer preferred language
				.selectOrganizationBuyerPreferedCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))

				//Check if preferred currency drop down is visible
				.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()

				//Select buyer preferred language
				.selectOrganizationBuyerPreferedLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))

				//Submit registration
				.submitUnsuccessfulOrganizationRegistration()

				//verify registration message appears for mismatched passwords
				.verifyMismatchTooltipPresent();

		regpage.typeOrganizationBuyerPassword(f_dsm.getInputParameter("PASSWORD"))

		//Resubmit registration
		.submitOrganizationBuyerRegistration()

		//Confirmation
		.verifyOrganizationBuyerRegistrationConfirmationMessage();
	}

	/**
	 * Test case register organization with an invalid password.
	 */
	@Test
	public void FB2BSTOREB2BA_03_09() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPage.registerB2B();

		rp.selectOrganizationRegister()

		//type organization name
		.typeOrganizationName(f_dsm.getInputParameter("ORGANIZATION_NAME") + System.currentTimeMillis())

		//type organization address
		.typeOrganizationStreetAddressLine1(f_dsm.getInputParameter("ORGANIZATION_ADDRESS"))

		//select organization country
		.selectOrganizationCountryOrRegion(f_dsm.getInputParameter("ORGANIZATION_COUNTRY"))

		//select organization province
		.selectOrganizationStateOrProvince(f_dsm.getInputParameter("ORGANIZATION_STATE"))

		//type organization city
		.typeOrganizationCity(f_dsm.getInputParameter("ORGANIZATION_CITY"))

		//type organization zipcode
		.typeOrganizationZipCode(f_dsm.getInputParameter("ORGANIZATION_ZIPCODE"))

		//type organization email
		.typeOrganizationEmail(f_dsm.getInputParameter("ORGANIZATION_EMAIL"))

		//type organization phone number
		.typeOrganizationPhoneNumber(f_dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))

		//type buyer username
		.typeOrganizationBuyerLogonId(f_dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID") + System.currentTimeMillis())

		//type buyer password
		.typeOrganizationBuyerPassword(f_dsm.getInputParameter("INVALID_PASSWORD"))

		//Verify buyer password verify
		.typeOrganizationBuyerVerifyPassword(f_dsm.getInputParameter("INVALID_PASSWORD_VERIFY"))

		//type buyer first name
		.typeOrganizationBuyerFirstName(f_dsm.getInputParameter("FIRST_NAME"))

		//type buyer last name
		.typeOrganizationBuyerLastName(f_dsm.getInputParameter("LAST_NAME"))

		//type buyer street address
		.typeOrganizationBuyerStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))

		//Select buyer country
		.selectOrganizationBuyerCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))

		//type or select state for buyer
		.selectOrganizationBuyerStateOrProvince(f_dsm.getInputParameter("STATE"))

		//type buyer city
		.typeOrganizationBuyerCity(f_dsm.getInputParameter("CITY"))

		//type buyer zipcode
		.typeOrganizationBuyerZipCode(f_dsm.getInputParameter("ZIPCODE"))

		//type buyer E-mail
		.typeOrganizationBuyerEmail(f_dsm.getInputParameter("EMAIL"))

		//type buyer home phone number
		.typeOrganizationBuyerPhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))

		//Check if preferred language drop down is visible
		.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			

		//Select buyer preferred language
		.selectOrganizationBuyerPreferedCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))

		//Check if preferred currency drop down is visible
		.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()

		//Select buyer preferred language
		.selectOrganizationBuyerPreferedLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))

		//Submit registration
		.submitUnsuccessfulOrganizationRegistration()

		//verify registration message appears for invalid password
		.verifyPasswordErrorMessage()

		.typeOrganizationBuyerPassword(f_dsm.getInputParameter("PASSWORD"))

		.typeOrganizationBuyerVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))

		//Resubmit registration
		.submitOrganizationBuyerRegistration()

		//Confirmation
		.verifyOrganizationBuyerRegistrationConfirmationMessage();
	}

	/**
	 * Test case register organization but cancel before submitting for approval.
	 */
	@Test
	public void FB2BSTOREB2BA_03_10() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPage.registerB2B();

		rp.selectOrganizationRegister()

		//type organization name
		.typeOrganizationName(f_dsm.getInputParameter("ORGANIZATION_NAME"))

		//type organization address
		.typeOrganizationStreetAddressLine1(f_dsm.getInputParameter("ORGANIZATION_ADDRESS"))

		//select organization country
		.selectOrganizationCountryOrRegion(f_dsm.getInputParameter("ORGANIZATION_COUNTRY"))

		//select organization province
		.selectOrganizationStateOrProvince(f_dsm.getInputParameter("ORGANIZATION_STATE"))

		//type organization city
		.typeOrganizationCity(f_dsm.getInputParameter("ORGANIZATION_CITY"))

		//type organization zipcode
		.typeOrganizationZipCode(f_dsm.getInputParameter("ORGANIZATION_ZIPCODE"))

		//type organization email
		.typeOrganizationEmail(f_dsm.getInputParameter("ORGANIZATION_EMAIL"))

		//type organization phone number
		.typeOrganizationPhoneNumber(f_dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))

		//type buyer username
		.typeOrganizationBuyerLogonId(f_dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID"))

		//type buyer password
		.typeOrganizationBuyerPassword(f_dsm.getInputParameter("PASSWORD"))

		//Verify buyer password verify
		.typeOrganizationBuyerVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))

		//type buyer first name
		.typeOrganizationBuyerFirstName(f_dsm.getInputParameter("FIRST_NAME"))

		//type buyer last name
		.typeOrganizationBuyerLastName(f_dsm.getInputParameter("LAST_NAME"))

		//type buyer street address
		.typeOrganizationBuyerStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))

		//Select buyer country
		.selectOrganizationBuyerCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))

		//type or select state for buyer
		.selectOrganizationBuyerStateOrProvince(f_dsm.getInputParameter("STATE"))

		//type buyer city
		.typeOrganizationBuyerCity(f_dsm.getInputParameter("CITY"))

		//type buyer zipcode
		.typeOrganizationBuyerZipCode(f_dsm.getInputParameter("ZIPCODE"))

		//type buyer E-mail
		.typeOrganizationBuyerEmail(f_dsm.getInputParameter("EMAIL"))

		//type buyer home phone number
		.typeOrganizationBuyerPhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))

		//Check if preferred language drop down is visible
		.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			

		//Select buyer preferred language
		.selectOrganizationBuyerPreferedCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))

		//Check if preferred currency drop down is visible
		.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()

		//Select buyer preferred language
		.selectOrganizationBuyerPreferedLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))

		//Cancel registration
		.cancelOrganizationBuyer();
	}

	/**
	 * Test case register organization with the user's address the same as the organizations'.
	 * 
	 * 
	 * 
	 * Issues when run with everything else. something may not be torn down correctly from previous TC.
	 * runs fine when run alone though
	 */
	@Test
	public void FB2BSTOREB2BA_03_11() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPanel.registerB2B();

		String username = f_dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID") + System.currentTimeMillis();

		SignInPageB2B signInPage = rp.selectOrganizationRegister()

				//type organization name
				.typeOrganizationName(f_dsm.getInputParameter("ORGANIZATION_NAME") + System.currentTimeMillis())

				//type organization address
				.typeOrganizationStreetAddressLine1(f_dsm.getInputParameter("ORGANIZATION_ADDRESS"))

				//select organization country
				.selectOrganizationCountryOrRegion(f_dsm.getInputParameter("ORGANIZATION_COUNTRY"))

				//select organization province
				.selectOrganizationStateOrProvince(f_dsm.getInputParameter("ORGANIZATION_STATE"))

				//type organization city
				.typeOrganizationCity(f_dsm.getInputParameter("ORGANIZATION_CITY"))

				//type organization zipcode
				.typeOrganizationZipCode(f_dsm.getInputParameter("ORGANIZATION_ZIPCODE"))

				//type organization email
				.typeOrganizationEmail(f_dsm.getInputParameter("ORGANIZATION_EMAIL"))

				//type organization phone number
				.typeOrganizationPhoneNumber(f_dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))

				//type buyer username
				.typeOrganizationBuyerLogonId(username)

				//type buyer password
				.typeOrganizationBuyerPassword(f_dsm.getInputParameter("PASSWORD"))

				//Verify buyer password verify
				.typeOrganizationBuyerVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))

				//type buyer first name
				.typeOrganizationBuyerFirstName(f_dsm.getInputParameter("FIRST_NAME"))

				//type buyer last name
				.typeOrganizationBuyerLastName(f_dsm.getInputParameter("LAST_NAME"))

				//use the same address as the organization
				.useSameAddressAsMyOrgOption(true)

				//type buyer E-mail
				.typeOrganizationBuyerEmail(f_dsm.getInputParameter("EMAIL"))

				//type buyer home phone number
				.typeOrganizationBuyerPhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))

				//Check if preferred language drop down is visible
				.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			

				//Select buyer preferred language
				.selectOrganizationBuyerPreferedCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))

				//Check if preferred currency drop down is visible
				.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()

				//Select buyer preferred language
				.selectOrganizationBuyerPreferedLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))

				//Submit registration
				.submitOrganizationBuyerRegistration();

		//verify registration confirmation message
		signInPage.verifyOrganizationBuyerRegistrationConfirmationMessage();

		// Approve Organization
		oac.logon(f_dsm.getInputParameter("ACCELERATOR_LOGON_ID"), f_dsm.getInputParameter("ACCELERATOR_PASSWORD"));
		oac.approveAllApprovals();

		//Enter a valid username
		signInPage.typeUsername(username)

		//Enter a valid password
		.typePassword(f_dsm.getInputParameter("PASSWORD"))

		//Click Sign In
		.signIn();
	}
}