package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2013
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp..
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.MyWishListPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.StoreManagement;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;




/** 		
 * Scenario: FV2STOREB2B_41
 * Details: The objective of this test scenario is testing Shopper adds, removes and shares items to/from wish list
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2B_41 extends  AbstractAuroraSingleSessionTests {

	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;


	private StoreManagement storeManagement;
	
	
	
	
	/**
	 * @param log
	 * @param config
	 * @param session
	 * @param dataSetManager
	 */
	@Inject
	public FV2STOREB2B_41(Logger log, CaslFoundationTestRule caslTestRule, 
			WcWteTestRule wcWebTestRule, TestDataProvider dataSetManager,
			StoreManagement storeManagement)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		this.storeManagement = storeManagement;
	}

	
	@Before
	public void pre_Setup() throws Exception{
		storeManagement.enableChangeFlowOption(dsm.getInputParameter("WISHLIST"));
	}
	/**
	 * Guest user tries to add items to Wish List through product details page	 
	 * @throws Exception 
	  	
	 */
	@Test
	public void FV2STOREB2B_4102() {

		//opening the store front page
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
				
		//Log on store
		HeaderWidget header = signIn.typeUsername(dsm.getInputParameter("LOGONID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn();
				
		//Go to department page
		CategoryPage subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"));
		//Go to product display page
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
		
		productDisplayPage.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("ATTRIBUTE_NAME1"),dsm.getInputParameter("ATTRIBUTE_VALUE1"))
		.selectAttributeFromDropdown(dsm.getInputParameter("ATTRIBUTE_NAME2"),dsm.getInputParameter("ATTRIBUTE_VALUE2"))
		.selectAttributeFromDropdown(dsm.getInputParameter("ATTRIBUTE_NAME3"),dsm.getInputParameter("ATTRIBUTE_VALUE3"));
	
		//Following action will through exception because guest user is not allowed to add product in wish list
		productDisplayPage.getShopperActionsWidget().addToWishlistLoggedIn();
		
		//Go to the My wish list page
		MyWishListPage wishListPage = productDisplayPage.getHeaderWidget().goToMyWishList();
				
		//verifying the products is showing under default wish list
		wishListPage.verifyProductInWishList(dsm.getInputParameter("PRODUCT_NAME"));
				
		// clean up ( removing all item from this wish list)
		wishListPage.removeAllItems();
		
	}
	
	/**
	 * Guest shopper adds an item to an existing non-default wish list from shopping cart page by converting to a new registered shopper 
	 */
	@Test
	public void FV2STOREB2B_4103()
	{
		//Unique wish list name
		String wishListName = RandomStringUtils.randomAlphanumeric(8);
		
		//opening the Aurora home page.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
		
		//SingIn into the store.
		MyAccountMainPage myAccountPage = frontPage.getHeaderWidget()
										 .signIn()
										 .typeUsername(dsm.getInputParameter("LOGINID"))
										 .typePassword(dsm.getInputParameter("PASSWORD"))
										 .signIn().goToMyAccount();
		
		//Going to wish list page and create a wish list
		MyWishListPage myWishListPage = myAccountPage.getHeaderWidget().goToMyWishList();
		myWishListPage = myWishListPage.deleteAllWishLists().createNewWishList(wishListName);
		

		//Go to department page
		CategoryPage subCat = myWishListPage.getHeaderWidget().goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"));
				//Go to product display page
		ProductDisplayPage productDisplayPage = subCat.getCatalogEntryListWidget().goToProductPageByName(dsm.getInputParameter("PRODUCT_NAME"));
				
		productDisplayPage.getDefiningAttributesWidget().selectAttributeFromDropdown(dsm.getInputParameter("ATTRIBUTE_NAME1"),dsm.getInputParameter("ATTRIBUTE_VALUE1"))
			.selectAttributeFromDropdown(dsm.getInputParameter("ATTRIBUTE_NAME2"),dsm.getInputParameter("ATTRIBUTE_VALUE2"))
			.selectAttributeFromDropdown(dsm.getInputParameter("ATTRIBUTE_NAME3"),dsm.getInputParameter("ATTRIBUTE_VALUE3"));
			
		//Adding product to shop cart
		ShopCartPage shopCartPage = productDisplayPage.addToCart().getHeaderWidget().goToShoppingCartPage();
		
		shopCartPage.moveToNonDefaultWishList(dsm.getInputParameter("PRODUCT_NAME"),wishListName);
		
		//Go to wish list page and verify the product got added to wish list.
		myWishListPage = shopCartPage.getHeaderWidget().goToMyWishList().selectWishList(wishListName).verifyProductInWishList(dsm.getInputParameter("PRODUCT_NAME"));
		
		//clean up
		myWishListPage.removeAllItems().deleteAllWishLists();
		
		myWishListPage.getHeaderWidget().openSignOutDropDownWidget().signOutB2B();
	}
	
	@After
	public void tearDown() throws Exception{
		storeManagement.disableChangeFlowOption(dsm.getInputParameter("WISHLIST"));
	}
	
}
