package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.ProductDisplayPageB2B;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.wte.util.WcConfigManager;


/** 
 * Test cases to update items in the shopping cart.
 * Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2B_31 extends AbstractAuroraSingleSessionTests
{
	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

	//A Variable to retrieve data from the data file. 
	@DataProvider
	private final TestDataProvider dsm;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with getConfig().properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_caslFixtures 
	 */		
	@Inject
	public FV2STOREB2B_31(
			Logger log, 
			WcConfigManager config,
			WcWteTestRule wcWebTestRule,
			CaslFoundationTestRule caslTestRule,
			TestDataProvider dataSetManager)
	{
		super(log, wcWebTestRule, caslTestRule);
		
		this.dsm = dataSetManager;
	}

	/**
	 * Test to update the quantity of items in the shopping cart with valid values
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2B_3101()
	{		
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		HeaderWidget headerWidget = signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn();
		
		//cleanup
		CartCleanup(headerWidget);
		
		//Open Sub category Apparel->Dresses.
		CategoryPage subCat = headerWidget.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
						dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"));
				
		//click on product image on Category Display page.
		ProductDisplayPageB2B productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByNameB2B(dsm.getInputParameter("PRODUCT_NAME"));
				
		//Change the quantity of the product
		productDisplay.getSkuListWidget().updateQuantity(dsm.getInputParameter("SKU_NAME"),dsm.getInputParameter("ITEM_QTY"));
		
		//click Add to Cart from Product Display page.
		productDisplay.addToCurrentOrder(dsm.getInputParameter("SKU_NAME"));
		
		//Confirm the price on minishopping cart
		productDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//Change the quantity of the item
		shopCart.changeQuantity(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_NEW_QTY"));
		
		shopCart.verifyOrderTotal(dsm.getInputParameter("NEW_TOTAL"));
	}
	
	/**
	 * Test to update the quantity of items in the shopping cart with invalid values 
	 */
	@Test
	public void testFV2STOREB2B_3102() 
	{
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget  signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		HeaderWidget headerWidget = signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn();
		
		//cleanup
		CartCleanup(headerWidget);
		
		headerWidget.goToFrontPageB2B();
		
		//Open Sub category Apparel->Dresses.
		CategoryPage subCat = headerWidget.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
						dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"));
						
		//click on product image on Category Display page.
		ProductDisplayPageB2B productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByNameB2B(dsm.getInputParameter("PRODUCT_NAME"));
						
		//Change the quantity of the product
		productDisplay.getSkuListWidget().updateQuantity(dsm.getInputParameter("SKU_NAME"),dsm.getInputParameter("ITEM_QTY"));
				
		//click Add to Cart from Product Display page.
		productDisplay.addToCurrentOrder(dsm.getInputParameter("SKU_NAME"));
		
		//Confirm the price on minishopping cart
		productDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();
		
		//Change the quantity of the item
		shopCart.changeQuantityRequest(dsm.getInputParameter("ITEM_POSITION"), dsm.getInputParameter("ITEM_NEW_QTY"));
		
		//Verify tool tip present
		shopCart.verifyErrorTooltipPresent(dsm.getInputParameter("ERROR"));
		
		HeaderWidget header = shopCart.removeAllItems().getHeaderWidget();
		
		header.openSignOutDropDownWidget().signOutB2B();
	}
	
	
	
	
	
	/**
	 * cleanup method to remove all items from the cart before proceeding.
	 * @param header
	 */
		private void CartCleanup(HeaderWidget header)
		{
			
				try {
					header.verifyMiniCartItemCount("0");
				} catch (Exception e) {
					//cart isn't 0, cleaning up
					header.goToShoppingCartPage().removeAllItems();
				}
			
		}
		
		
		
		
			
			
}
