package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.casl.keys.CaslKeysFactory;
import com.ibm.commerce.casl.keys.ToolingKeys;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.CustomerServiceFindCustomerWidget;
import com.ibm.commerce.qa.aurora.widget.MyAccountSidebarWidget;
import com.ibm.commerce.qa.aurora.widget.RequisitionListsWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurora.widget.impl.RealRequisitionListItemsWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.RequisitionListDetailsPage;
import com.ibm.commerce.qa.aurorab2b.page.RequisitionListsPage;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.wte.util.WcConfigManager;


/** 
 * Test scenario to test various use cases associated with Dropdown menu context
 * Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FSTOREB2BCSR_04 extends AbstractAuroraSingleSessionTests
{
	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

	//A Variable to retrieve data from the data file. 
	@DataProvider
	private final TestDataProvider dsm;
	private CaslKeysFactory f_caslKeysFactory;

	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with getConfig().properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_caslFixtures 
	 */		
	@Inject
	public FSTOREB2BCSR_04(
			Logger log, 
			WcConfigManager config,
			WcWteTestRule wcWebTestRule,
			CaslFoundationTestRule caslTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_caslFixtures, CaslKeysFactory p_caslKeysFactory)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.dsm = dataSetManager;
		f_caslKeysFactory = p_caslKeysFactory;
	}

	// Test to  Add item to B2B requisition list
	@Test
	public void testFSTOREB2BCSR_0405()
	{
		//Open the store in the browser.
				AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);

				//Open Sign In/Register page.
				SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();

				//Enter account User Name/Password and sign in.
				signInPage.typeUsername(dsm.getInputParameter("CSR_LOGONID"))
				.typePassword(dsm.getInputParameter("CSR_PASSWORD")).signIn();	
				
				
				//Get LOGONID of a shopper that we are trying to locate
				String BuyerLogonId = dsm.getInputParameter("BUYER_ADMIN_LOGONID");
				CustomerServiceFindCustomerWidget FindCustomerWidget = frontPage.getHeaderWidget().goToCustomerService().getFindCustomerWidget()
				.typeLogonId(BuyerLogonId).submitSearch();
				
				//Query to return unique user id (USER_ID) 
				ToolingKeys p = f_caslKeysFactory.createToolingKeys(dsm.getInputParameter("ADMIN_USERID"), dsm.getInputParameter("ADMIN_PASSWORD"));
				final String USER_ID = p.findPersonIdByLogonId(BuyerLogonId);
				
				//Access Customer's account and shop on behalf of a shopper as a CSR 
				MyAccountMainPage myAccountPage = FindCustomerWidget.AccessCustomerAccount(USER_ID);

				MyAccountSidebarWidget myAccountSidebar = myAccountPage.getSidebar();

				//Go to requisition list page from my account side bar.
				RequisitionListsPage reqListPage = myAccountSidebar.goToRequisitionListPage();

				RequisitionListsWidget reqListWidget = reqListPage.getRequistionListsWidget();
				
				//Enter requisition list data.
				reqListWidget.goToNewRequistionListForm()
				
						.typeNewRequistionListName(dsm.getInputParameter("REQUISITION_LIST_NAME"))
						
						.selectRequistionType(dsm.getInputParameter("REQUISITION_LIST_TYPE"))
						
						.saveNewRequistionList();
				
				//Go to requisition list details page
				RequisitionListDetailsPage reqListDetails = reqListWidget
						
						.goToRequisitionListDetails(dsm.getInputParameter("REQUISITION_LIST_POS"));

				RealRequisitionListItemsWidget reqListItemsWidget = reqListDetails.getRequisitionListItemsWidget();

				//create 14 requisition lists
				for (int i = 0; i < 2; i++)
				{

					//Add new item to requisition list and verify popup.
					reqListItemsWidget.typeSkuValue(dsm.getInputParameter("SKU_NUMBER"))
					
							.typeQuantity(dsm.getInputParameter("QUANTITY"))
						
							.addItemToList()
							
							.verifyItemAddedSuccessPopup();
				}

				//Click next page button.
				reqListItemsWidget.goToNextpage();

				//Click previouse page button.
				reqListItemsWidget.goToPreviouspage();
				
				
	}
	
}
