package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.CustomerRegisterationPageB2B;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.Accelerator;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.url.OrgAdminConsole;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 * Scenario: FV2STOREB2B_11
 * Details: Test View Order Status
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2B_09_01 extends AbstractAuroraSingleSessionTests
{

    /**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	/**A variable to hold the name of the data file where input parameters can be found.**/
	private String [] shippingModes = {};
	private String [] shippingCharges = {};
	private String [] contract1 = {};
	private String [] contract2= {};
	private String contractId;
	private String contractId2;

	private String accountId;
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	
	private OrgAdminConsole oac;
	
	private Accelerator accelerator;
	
	private CMC cmc;
	
	public String catFilterId;
	
	public boolean merchAssocCleanup = false;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_CaslFixtures 
	 */	@Inject
	public FV2STOREB2B_09_01(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,	
			Accelerator accelerator,
			OrgAdminConsole oac,
			CMC cmc)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		this.accelerator = accelerator;
		this.oac = oac;
		this.cmc = cmc;
		
	}
	private void registerUserAndOrg(){ 
		//Open Auroraesite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPage = frontPage.getHeaderWidget().signIn();
		
		//attempt to sign in:
		try {
			signInPage.typeUsername(dsm.getInputParameter("LOGONID"))
				.typePassword(dsm.getInputParameter("PASSWORD"))
				.signIn();
			
			//Open Auroraesite store
			frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

			
		} catch (Exception e1) {
			//Open Auroraesite store
			

			//Click on the registration button on the SignIn page
			CustomerRegisterationPageB2B rp = signInPage.registerB2B();

			rp.selectOrganizationRegister()
			
			//type organization name
			.typeOrganizationName(dsm.getInputParameter("ORGANIZATION_NAME"))
			
			//type organization address
			.typeOrganizationStreetAddressLine1(dsm.getInputParameter("ORGANIZATION_ADDRESS"))
			
			//select organization country
			.selectOrganizationCountryOrRegion(dsm.getInputParameter("ORGANIZATION_COUNTRY"))
			
			//select organization province
			.selectOrganizationStateOrProvince(dsm.getInputParameter("ORGANIZATION_STATE"))
			
			//type organization city
			.typeOrganizationCity(dsm.getInputParameter("ORGANIZATION_CITY"))
			
			//type organization zipcode
			.typeOrganizationZipCode(dsm.getInputParameter("ORGANIZATION_ZIPCODE"))
			
			//type organization email
			.typeOrganizationEmail(dsm.getInputParameter("ORGANIZATION_EMAIL"))
			
			//type organization phone number
			.typeOrganizationPhoneNumber(dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))

			//type buyer username
			.typeOrganizationBuyerLogonId(dsm.getInputParameter("LOGONID"))

			//type buyer password
			.typeOrganizationBuyerPassword(dsm.getInputParameter("PASSWORD"))

			//Verify buyer password verify
			.typeOrganizationBuyerVerifyPassword(dsm.getInputParameter("PASSWORD_VERIFY"))

			//type buyer first name
			.typeOrganizationBuyerFirstName(dsm.getInputParameter("FIRST_NAME"))

			//type buyer last name
			.typeOrganizationBuyerLastName(dsm.getInputParameter("LAST_NAME"))

			//type buyer street address
			.typeOrganizationBuyerStreetAddressLine1(dsm.getInputParameter("ADDRESS"))

			//Select buyer country
			.selectOrganizationBuyerCountryOrRegion(dsm.getInputParameter("COUNTRY"))

			//type or select state for buyer
			.selectOrganizationBuyerStateOrProvince(dsm.getInputParameter("STATE"))

			//type buyer city
			.typeOrganizationBuyerCity(dsm.getInputParameter("CITY"))

			//type buyer zipcode
			.typeOrganizationBuyerZipCode(dsm.getInputParameter("ZIPCODE"))

			//type buyer E-mail
			.typeOrganizationBuyerEmail(dsm.getInputParameter("EMAIL"))

			//type buyer home phone number
			.typeOrganizationBuyerPhoneNumber(dsm.getInputParameter("PHONE_NUMBER"))

			//Check if preferred language drop down is visible
			.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			

			//Select buyer preferred language
			.selectOrganizationBuyerPreferedCurrency(dsm.getInputParameter("PREFERRED_CURRENCY"))

			//Check if preferred currency drop down is visible
			.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()

			//Select buyer preferred language
			.selectOrganizationBuyerPreferedLanguage(dsm.getInputParameter("PREFERRED_LANGUAGE"))

			//Submit registration
			.submitOrganizationBuyerRegistration()
			
			//verify registration confirmation message
			.verifyOrganizationBuyerRegistrationConfirmationMessage();
			
			// Approve Organization
			oac.logon(dsm.getInputParameter("ADMIN_LOGON_ID"), dsm.getInputParameter("ADMIN_PASSWORD"));
			oac.approveAllApprovals();
			
		}
	}
	 @Before
	 public void setup() throws Exception {
		
		//create a user and org
		registerUserAndOrg();
		
		accelerator.logon(dsm.getInputParameter("ACCELERATOR_LOGON_ID"),dsm.getInputParameter("ACCELERATOR_PASSWORD"));
		accelerator.createNewAccount(dsm.getInputParameter("ORG"), null, null,false);
		contract1 = createContract(dsm.getInputParameter("ORG"));
		contractId = contract1[0];
		accelerator.logoff();
		
		//create catalog filter
		dsm.setDataLocation("setup", "setup");
		cmc.logon(dsm.getInputParameter("CMC_LOGON_ID"),dsm.getInputParameter("CMC_PASSWORD"));
		cmc.selectStore();
		cmc.selectCatalog();
		
		try {
			catFilterId = cmc.getCatalogFilterId(dsm.getInputParameter("CAT_FILTER_NAME"));
		} catch (Exception e) {
			catFilterId = cmc.createCatalogFilter(dsm.getInputParameter("CAT_FILTER_NAME"));
			cmc.createCatalogFilterCategoryElement(catFilterId, dsm.getInputParameter("CAT_FILTER_EXCULDE_CATEGORY"), dsm.getInputParameter("CAT_FILTER_SELECTION"));
			
		}
		
		cmc.logoff();
		
		//create account and contract
		accelerator.logon(dsm.getInputParameter("ACCELERATOR_LOGON_ID"),dsm.getInputParameter("ACCELERATOR_PASSWORD"));
		
		contract2 = createContract(dsm.getInputParameter("ORG"));
		contractId2 = contract2[0];
		accelerator.logoff();
			
	 }
		 
	 /** 
	 * Single catalog filter contract filters the product display in the store front
	 * @throws Exception 
		 */
		@Category(Sanity.class)
		@Test
		public void testFV2STOREB2B_0901() throws Exception
		{
			//verify in storefront
			AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
			
			//Opens the Sign In page in browser.
			SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
			
			//Log in to the store.
			signIn = signIn.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
												.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
												.signInWithoutVerify(SignInDropdownWidget.class);
			
			HeaderWidget header = signIn.selectContractByName(contract2[1]);
		
			CategoryPage category = header.goToDepartmentByName(CategoryPage.class,dsm.getInputParameter("DEPARTMENT") );
			
			category.getCategoryNavigationWidget().verifyCategoryIsNotPresent(dsm.getInputParameter("CATEGORY"));
			
			category.getHeaderWidget().openSignOutDropDownWidget().signOutB2B();
			
		}	
	
	
			private String [] createContract(String org) throws Exception
			{
					String [] tempcontract =  new String[2];
														
					//NOte register organization and approve it
																	
					//Tell which test case to use for input parameters
					dsm.setDataLocation("setup", "Accelerator");
					
					//logs on to the accelerator.
					accelerator.logon(dsm.getInputParameter("ADMIN_LOGON_ID"),dsm.getInputParameter("ADMIN_PASSWORD"));
					
					//Tell which test case to use for input parameters
					dsm.setDataLocation("setup", "createAccount");
					
					//Create a new Account for Organization.
					
					accountId = accelerator.getOrganizationAccountNo(org);
					
					dsm.setDataLocation("setup", "createContract");
					
					String [] percentOff = {dsm.getInputParameter("percentOff1_1"),dsm.getInputParameter("percentOff1_2")};
					String [] requiredCatalogs = {dsm.getInputParameter("requiredCatalogs1_1"),dsm.getInputParameter("requiredCatalogs1_2")};
					
					//gets a new unique contract name.
					String contractName = RandomStringUtils.randomAlphanumeric(8);

					//gets all available shipping modes.
					shippingModes = accelerator.getShippingModes();
					
					//gets all available shipping charges.
					shippingCharges = accelerator.getShippingCharges();
					
					//creates new contract for the account created in previous steps.
					String tempContractId = accelerator.createNewContract(contractName, org,dsm.getInputParameter("INCLUDE_ENTIRE_CATALOG"),dsm.getInputParameter("PERCENTAGE_OFF_ON_ENTIRE_CATALOG"), requiredCatalogs, percentOff, shippingModes, shippingCharges);
									
					//submit the contract;
					accelerator.submitContract(accountId, tempContractId);
					
					
					while(!accelerator.isContractActive(org, contractName))
					{
						if(accelerator.isContractActive(org, contractName))
						{
							break;
						}
					}

					//log out from accelerator.
					accelerator.logoff();
									
					tempcontract[0] = tempContractId;
					tempcontract[1] = contractName;
					return tempcontract;
						
			}
			
			
		@After
		public void tearDown() throws Exception {
			accelerator.logon(dsm.getInputParameter("ADMIN_LOGON_ID"),dsm.getInputParameter("ADMIN_PASSWORD"));
			
			accelerator.cancelContract(accountId, contractId);
			accelerator.deleteContract(accountId, contractId);
			accelerator.cancelContract(accountId, contractId2);
			accelerator.deleteContract(accountId, contractId2);
			
			accelerator.deleteAccount(accountId);
			accelerator.removeParameterInGlobalMap("catalogFilterId");
			
			cmc.logon(dsm.getInputParameter("CMC_LOGON_ID"),dsm.getInputParameter("CMC_PASSWORD"));
			cmc.selectStore();
			cmc.selectCatalog();
			cmc.deleteCatalogFilter(catFilterId);
			cmc.logoff();
			
		}
			
			



	
			
	
	
}
