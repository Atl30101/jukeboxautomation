package com.ibm.commerce.qa.aurorab2b.tests;

import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.CreateOrganizationWidget;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.MyAccountSidebarWidget;
import com.ibm.commerce.qa.aurora.widget.OrganizationAddressWidget;
import com.ibm.commerce.qa.aurora.widget.OrganizationContactInfoWidget;
import com.ibm.commerce.qa.aurora.widget.OrganizationDetailsWidget;
import com.ibm.commerce.qa.aurora.widget.OrganizationListWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.BuyersApprovalPage;
import com.ibm.commerce.qa.aurorab2b.page.CreateOrganizationPage;
import com.ibm.commerce.qa.aurorab2b.page.CustomerRegisterationPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.EditOrganizationPage;
import com.ibm.commerce.qa.aurorab2b.page.OrganizationsAndBuyersPage;
import com.ibm.commerce.qa.aurorab2b.page.SignInPageB2B;
import com.ibm.commerce.qa.casl.util.CaslModule;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.OrgAdminConsole;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
/**
 * Scenario: FSSC-92726-10
 * Details: Buyer registration test scenario
 * 
 */
@RunWith(GuiceTestRunner.class)
@TestModules({AuroraModule.class, CaslModule.class})
public class FB2BSTOREB2BA_10 extends AbstractAuroraSingleSessionTests {

	private final OrgAdminConsole oac;

	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider f_dsm;

	private String organizationName;

	private String username;

	private String subOrganizationName;

	private boolean setupCompleted = false;

	/**
	 * Test Class object constructor.
	 * 
	 */
	@Inject
	public FB2BSTOREB2BA_10(
			Logger log, 
			WcWteTestRule wcWteTestRule,
			CaslFoundationTestRule caslTestRule,
			TestDataProvider p_dsm,
			OrgAdminConsole p_oac) throws Exception
	{
		super(log, wcWteTestRule, caslTestRule);

		f_dsm = p_dsm;
		oac = p_oac;
	}

	@Before
	public void setUp() throws Exception
	{
		if (!setupCompleted) {
			createOrganization();
			createSubOrganization();
		}

		setupCompleted = true;
	}

	public void createOrganization() throws Exception
	{
		f_dsm.setDataLocation("createOrganization", "createOrganization");
		
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPanel.registerB2B();

		username = f_dsm.getInputParameter("ORGANIZATION_BUYER_LOGONID") + System.currentTimeMillis();

		organizationName = f_dsm.getInputParameter("ORGANIZATION_NAME") + System.currentTimeMillis();

		rp.selectOrganizationRegister()

		//type organization name
		.typeOrganizationName(organizationName)

		//type organization address
		.typeOrganizationStreetAddressLine1(f_dsm.getInputParameter("ORGANIZATION_ADDRESS"))

		//select organization country
		.selectOrganizationCountryOrRegion(f_dsm.getInputParameter("ORGANIZATION_COUNTRY"))

		//select organization province
		.selectOrganizationStateOrProvince(f_dsm.getInputParameter("ORGANIZATION_STATE"))

		//type organization city
		.typeOrganizationCity(f_dsm.getInputParameter("ORGANIZATION_CITY"))

		//type organization zipcode
		.typeOrganizationZipCode(f_dsm.getInputParameter("ORGANIZATION_ZIPCODE"))

		//type organization email
		.typeOrganizationEmail(f_dsm.getInputParameter("ORGANIZATION_EMAIL"))

		//type organization phone number
		.typeOrganizationPhoneNumber(f_dsm.getInputParameter("ORGANIZATION_PHONE_NUMBER"))

		//type buyer username
		.typeOrganizationBuyerLogonId(username)

		//type buyer password
		.typeOrganizationBuyerPassword(f_dsm.getInputParameter("PASSWORD"))

		//Verify buyer password verify
		.typeOrganizationBuyerVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))

		//type buyer first name
		.typeOrganizationBuyerFirstName(f_dsm.getInputParameter("FIRST_NAME"))

		//type buyer last name
		.typeOrganizationBuyerLastName(f_dsm.getInputParameter("LAST_NAME"))

		//type buyer street address
		.typeOrganizationBuyerStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))

		//Select buyer country
		.selectOrganizationBuyerCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))

		//type or select state for buyer
		.selectOrganizationBuyerStateOrProvince(f_dsm.getInputParameter("STATE"))

		//type buyer city
		.typeOrganizationBuyerCity(f_dsm.getInputParameter("CITY"))

		//type buyer zipcode
		.typeOrganizationBuyerZipCode(f_dsm.getInputParameter("ZIPCODE"))

		//type buyer E-mail
		.typeOrganizationBuyerEmail(f_dsm.getInputParameter("EMAIL"))

		//type buyer home phone number
		.typeOrganizationBuyerPhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))

		//Check if preferred language drop down is visible
		.verifyOrganizationBuyerPreferredLanguageDropDownListPresent()			

		//Select buyer preferred language
		.selectOrganizationBuyerPreferedCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))

		//Check if preferred currency drop down is visible
		.verifyOrganizationBuyerPreferredCurrencyDropDownListPresent()

		//Select buyer preferred language
		.selectOrganizationBuyerPreferedLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))

		//Submit registration
		.submitOrganizationBuyerRegistration()

		//verify registration confirmation message
		.verifyOrganizationBuyerRegistrationConfirmationMessage();

		// Approve Organization
		oac.logon(f_dsm.getInputParameter("ACCELERATOR_LOGON_ID"), f_dsm.getInputParameter("ACCELERATOR_PASSWORD"));
		oac.approveAllApprovals();
	}

	public void createSubOrganization() throws Exception
	{
		f_dsm.setDataLocation("createSubOrganization", "createSubOrganization");
		
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel.typeUsername(username)

				//Enter a valid password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				//Click Sign In
				.signIn();

		//go to My Account page
		MyAccountMainPage myAccountPage = header.goToMyAccount();

		MyAccountSidebarWidget myAccountSidebar = myAccountPage.getSidebar();

		//go to Organizations and Buyers page
		OrganizationsAndBuyersPage orgAndBuyersPage = myAccountSidebar.goToOrganizationsAndBuyersPage();

		OrganizationListWidget orgListWidget = orgAndBuyersPage.getOrganizationListWidget();

		//go to Create Organization page
		CreateOrganizationPage createOrgPage = orgListWidget.goToCreateOrganizationPage();

		CreateOrganizationWidget createOrgWidget = createOrgPage.getCreateOrganizationWidget();

		subOrganizationName = f_dsm.getInputParameter("ORGANIZATION_NAME");

		//add in organization details for the organization
		OrganizationDetailsWidget orgDetailsWidget = createOrgWidget.getOrganizationDetailsWidget();

		orgDetailsWidget.typeOrgName(subOrganizationName);

		//add in address information for the organization
		OrganizationAddressWidget orgAddressWidget = createOrgWidget.getOrganizationAddressWidget();

		orgAddressWidget.typeCity(f_dsm.getInputParameter("ORGANIZATION_CITY"))

		.typeStreetAddress(f_dsm.getInputParameter("ORGANIZATION_ADDRESS"))

		.typeZipCode(f_dsm.getInputParameter("ORGANIZATION_ZIPCODE"));

		//.selectCountry(f_dsm.getInputParameter("ORGANIZATION_COUNTRY"))

		//.selectState(f_dsm.getInputParameter("ORGANIZATION_STATE"));

		//add in contact information for the organization
		OrganizationContactInfoWidget orgContactWidget = createOrgWidget.getOrganizationContactInfoWidget();

		orgContactWidget.typeEmail(f_dsm.getInputParameter("ORGANIZATION_EMAIL"));

		//create a sub organization
		EditOrganizationPage editOrgPage = createOrgWidget.submit();

		editOrgPage.getEditOrganizationWidget().verifyOrgName(subOrganizationName);

	}

	public void turnOffApprovals(String approval) throws Exception
	{
		//change to be done on storefront when code available

		// Approve Organization
		oac.logon(f_dsm.getInputParameter("ACCELERATOR_LOGON_ID"), f_dsm.getInputParameter("ACCELERATOR_PASSWORD"));
		oac.approveAllApprovals();

		//Disable buyer approval inherited attributes for this organization
		oac.addApprovalsToOrganization(organizationName, approval);
	}

	public BuyersApprovalPage approveBuyers(String fullName)
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Enter a valid username
		HeaderWidget header = signInPanel

				.typeUsername(username)

				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				//Click Sign In
				.signIn();

		BuyersApprovalPage buyerApprovalPage = header.goToMyAccount()

				.getSidebar()

				.goToBuyerApprovalPage();

		buyerApprovalPage.getBuyersApprovalWidget()

		.approveBuyer(fullName)

		.verifyMessagePopup(f_dsm.getInputParameter("APPROVED_MESSAGE"))

		.closeMessagePopup();

		return buyerApprovalPage;

	}

	/**
	 * Test case register buyer under existing organization.
	 */
	@Test
	public void FB2BSTOREB2BA_10_01() throws Exception
	{		
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPanel.registerB2B();

		String buyerUsername = f_dsm.getInputParameter("BUYER_LOGONID") + System.currentTimeMillis();

		String firstName = f_dsm.getInputParameter("FIRST_NAME");

		String lastName = f_dsm.getInputParameter("LAST_NAME");

		String fullName = firstName + " " + lastName;

		SignInPageB2B signInPage = rp.selectBuyerRegister()

				//type buyer username
				.typeLogonId(buyerUsername)

				//type organization name
				.typeOrganization(organizationName)

				//type buyer password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				//Verify buyer password verify
				.typeVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))

				//type buyer first name
				.typeFirstName(firstName)

				//type buyer last name
				.typeLastName(lastName)

				//type buyer street address
				.typeStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))

				//Select buyer country
				.selectCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))

				//type or select state for buyer
				.selectStateOrProvince(f_dsm.getInputParameter("STATE"))

				//type buyer city
				.typeCity(f_dsm.getInputParameter("CITY"))

				//type buyer zipcode
				.typeZipCode(f_dsm.getInputParameter("ZIPCODE"))

				//type buyer E-mail
				.typeEmail(f_dsm.getInputParameter("EMAIL"))

				//type buyer home phone number
				.typePhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))

				//Check if preferred language drop down is visible
				.verifyPreferredLanguageDropDownListPresent()			

				//Select buyer preferred language
				.selectPreferredCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))

				//Check if preferred currency drop down is visible
				.verifyPreferredCurrencyDropDownListPresent()

				//Select buyer preferred language
				.selectPreferredLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))

				//Submit registration
				.submitBuyerRegistration();

		//verify registration confirmation message
		signInPage.verifyBuyerRegistrationConfirmationMessage();

		// Approve Buyer
		BuyersApprovalPage buyerApprovalPage = approveBuyers(fullName);

		//Sign out
		HeaderWidget header = buyerApprovalPage.getHeaderWidget().openSignOutDropDownWidget().signOutB2B();

		header.signIn()

		//Enter a valid username
		.typeUsername(buyerUsername)

		//Enter a valid password
		.typePassword(f_dsm.getInputParameter("PASSWORD"))

		//Click Sign In
		.signIn(HeaderWidget.class);
	}

	/**
	 * Test case register buyer and sign in before approved.
	 */
	@Test
	public void FB2BSTOREB2BA_10_02() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPanel.registerB2B();

		String buyerUsername = f_dsm.getInputParameter("BUYER_LOGONID") + System.currentTimeMillis();

		SignInPageB2B signInPage = rp.selectBuyerRegister()

				//type buyer username
				.typeLogonId(buyerUsername)

				//type organization name
				.typeOrganization(organizationName)

				//type buyer password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				//Verify buyer password verify
				.typeVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))

				//type buyer first name
				.typeFirstName(f_dsm.getInputParameter("FIRST_NAME"))

				//type buyer last name
				.typeLastName(f_dsm.getInputParameter("LAST_NAME"))

				//type buyer street address
				.typeStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))

				//Select buyer country
				.selectCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))

				//type or select state for buyer
				.selectStateOrProvince(f_dsm.getInputParameter("STATE"))

				//type buyer city
				.typeCity(f_dsm.getInputParameter("CITY"))

				//type buyer zipcode
				.typeZipCode(f_dsm.getInputParameter("ZIPCODE"))

				//type buyer E-mail
				.typeEmail(f_dsm.getInputParameter("EMAIL"))

				//type buyer home phone number
				.typePhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))

				//Check if preferred language drop down is visible
				.verifyPreferredLanguageDropDownListPresent()			

				//Select buyer preferred language
				.selectPreferredCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))

				//Check if preferred currency drop down is visible
				.verifyPreferredCurrencyDropDownListPresent()

				//Select buyer preferred language
				.selectPreferredLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))

				//Submit registration
				.submitBuyerRegistration();

		//verify registration confirmation message
		signInPage.verifyBuyerRegistrationConfirmationMessage()

		//Enter a valid username
		//.typeUsername(buyerUsername)

		//Enter a valid password
		.typePassword(f_dsm.getInputParameter("PASSWORD"))

		//Click Sign In
		.signIn();

		//Confirm not approved message
		signInPage.verifyNotApprovedMessage();
	}

	/**
	 * Test case register buyer with one mandatory field left blank.
	 */
	@Test
	public void FB2BSTOREB2BA_10_04() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPanel.registerB2B();

		String buyerUsername = f_dsm.getInputParameter("BUYER_LOGONID") + System.currentTimeMillis();

		String firstName = f_dsm.getInputParameter("FIRST_NAME");

		String lastName = f_dsm.getInputParameter("LAST_NAME");

		String fullName = firstName + " " + lastName;

		rp.selectBuyerRegister()

		//type buyer username
		.typeLogonId(buyerUsername)

		//type organization name
		.typeOrganization(organizationName)

		//type buyer password
		.typePassword(f_dsm.getInputParameter("PASSWORD"))

		//Verify buyer password verify
		.typeVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))

		//type buyer first name
		.typeFirstName(firstName)

		//type buyer last name
		.typeLastName(lastName)

		//type buyer street address
		.typeStreetAddressLine1(f_dsm.getInputParameter("INVALID_ADDRESS"))

		//Select buyer country
		.selectCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))

		//type or select state for buyer
		.selectStateOrProvince(f_dsm.getInputParameter("STATE"))

		//type buyer city
		.typeCity(f_dsm.getInputParameter("CITY"))

		//type buyer zipcode
		.typeZipCode(f_dsm.getInputParameter("ZIPCODE"))

		//type buyer E-mail
		.typeEmail(f_dsm.getInputParameter("EMAIL"))

		//type buyer home phone number
		.typePhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))

		//Check if preferred language drop down is visible
		.verifyPreferredLanguageDropDownListPresent()			

		//Select buyer preferred language
		.selectPreferredCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))

		//Check if preferred currency drop down is visible
		.verifyPreferredCurrencyDropDownListPresent()

		//Select buyer preferred language
		.selectPreferredLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))

		//Submit registration
		.submitUnsuccessfulBuyerRegistration();

		rp.verifyEmptyTooltipPresent();

		rp.typeStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))

				.submitBuyerRegistration();

		// Approve Buyer
		BuyersApprovalPage buyerApprovalPage = approveBuyers(fullName);

		//Sign out
		HeaderWidget header = buyerApprovalPage.getHeaderWidget().openSignOutDropDownWidget().signOutB2B();

		header.signIn()

		//Enter a valid username
		.typeUsername(buyerUsername)

		//Enter a valid password
		.typePassword(f_dsm.getInputParameter("PASSWORD"))

		//Click Sign In
		.signIn(HeaderWidget.class);
	}

	/**
	 * Test case register buyer with two or more mandatory fields left blank.
	 */
	@Test
	public void FB2BSTOREB2BA_10_05() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPanel.registerB2B();

		String buyerUsername = f_dsm.getInputParameter("BUYER_LOGONID") + System.currentTimeMillis();

		String firstName = f_dsm.getInputParameter("FIRST_NAME");

		String lastName = f_dsm.getInputParameter("LAST_NAME");

		String fullName = firstName + " " + lastName;

		rp.selectBuyerRegister()

		//type buyer username
		.typeLogonId(buyerUsername)

		//type organization name
		.typeOrganization(organizationName)

		//type buyer password
		.typePassword(f_dsm.getInputParameter("PASSWORD"))

		//Verify buyer password verify
		.typeVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))

		//type buyer first name
		.typeFirstName(firstName)

		//type buyer last name
		.typeLastName(lastName)

		//type buyer street address
		.typeStreetAddressLine1(f_dsm.getInputParameter("INVALID_ADDRESS"))

		//Select buyer country
		.selectCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))

		//type or select state for buyer
		.selectStateOrProvince(f_dsm.getInputParameter("STATE"))

		//type buyer city
		.typeCity(f_dsm.getInputParameter("CITY"))

		//type buyer zipcode
		.typeZipCode(f_dsm.getInputParameter("ZIPCODE"))

		//type buyer E-mail
		.typeEmail(f_dsm.getInputParameter("EMAIL"))

		//type buyer home phone number
		.typePhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))

		//Check if preferred language drop down is visible
		.verifyPreferredLanguageDropDownListPresent()			

		//Select buyer preferred language
		.selectPreferredCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))

		//Check if preferred currency drop down is visible
		.verifyPreferredCurrencyDropDownListPresent()

		//Select buyer preferred language
		.selectPreferredLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))

		//Submit registration
		.submitUnsuccessfulBuyerRegistration();

		rp.verifyEmptyTooltipPresent();

		rp.typeStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))

				.typeLastName(f_dsm.getInputParameter("LAST_NAME"))

				.submitBuyerRegistration();

		// Approve Buyer
		BuyersApprovalPage buyerApprovalPage = approveBuyers(fullName);

		//Sign out
		HeaderWidget header = buyerApprovalPage.getHeaderWidget().openSignOutDropDownWidget().signOutB2B();

		header.signIn()

		//Enter a valid username
		.typeUsername(buyerUsername)

		//Enter a valid password
		.typePassword(f_dsm.getInputParameter("PASSWORD"))

		//Click Sign In
		.signIn(HeaderWidget.class);
	}

	/**
	 * Test case register buyer with invalid entries.
	 */
	@Test
	public void FB2BSTOREB2BA_10_06() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPanel.registerB2B();

		String buyerUsername = f_dsm.getInputParameter("BUYER_LOGONID") + System.currentTimeMillis();

		String firstName = f_dsm.getInputParameter("FIRST_NAME");

		String lastName = f_dsm.getInputParameter("LAST_NAME");

		String fullName = firstName + " " + lastName;

		rp.selectBuyerRegister()

		//type buyer username
		.typeLogonId(buyerUsername)

		//type organization name
		.typeOrganization(organizationName)

		//type buyer password
		.typePassword(f_dsm.getInputParameter("PASSWORD"))

		//Verify buyer password verify
		.typeVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))

		//type buyer first name
		.typeFirstName(firstName)

		//type buyer last name
		.typeLastName(lastName)

		//type buyer street address
		.typeStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))

		//Select buyer country
		.selectCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))

		//type or select state for buyer
		.selectStateOrProvince(f_dsm.getInputParameter("STATE"))

		//type buyer city
		.typeCity(f_dsm.getInputParameter("CITY"))

		//type buyer zipcode
		.typeZipCode(f_dsm.getInputParameter("ZIPCODE"))

		//type buyer E-mail
		.typeEmail(f_dsm.getInputParameter("INVALID_EMAIL"))

		//type buyer home phone number
		.typePhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))

		//Check if preferred language drop down is visible
		.verifyPreferredLanguageDropDownListPresent()			

		//Select buyer preferred language
		.selectPreferredCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))

		//Check if preferred currency drop down is visible
		.verifyPreferredCurrencyDropDownListPresent()

		//Select buyer preferred language
		.selectPreferredLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))

		//Submit registration
		.submitUnsuccessfulBuyerRegistration();

		rp.verifyInvalidTooltipPresent();

		rp.typeEmail(f_dsm.getInputParameter("EMAIL"))

				.submitBuyerRegistration();

		// Approve Buyer
		BuyersApprovalPage buyerApprovalPage = approveBuyers(fullName);

		//Sign out
		HeaderWidget header = buyerApprovalPage.getHeaderWidget().openSignOutDropDownWidget().signOutB2B();

		header.signIn()

		//Enter a valid username
		.typeUsername(buyerUsername)

		//Enter a valid password
		.typePassword(f_dsm.getInputParameter("PASSWORD"))

		//Click Sign In
		.signIn(HeaderWidget.class);
	}

	/**
	 * Test case register buyer with password mismatch for the user.
	 */
	@Test
	public void FB2BSTOREB2BA_10_07() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPanel.registerB2B();

		String buyerUsername = f_dsm.getInputParameter("BUYER_LOGONID") + System.currentTimeMillis();

		String firstName = f_dsm.getInputParameter("FIRST_NAME");

		String lastName = f_dsm.getInputParameter("LAST_NAME");

		String fullName = firstName + " " + lastName;

		rp.selectBuyerRegister()

		//type buyer username
		.typeLogonId(buyerUsername)

		//type organization name
		.typeOrganization(organizationName)

		//type buyer password
		.typePassword(f_dsm.getInputParameter("INVALID_PASSWORD"))

		//Verify buyer password verify
		.typeVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))

		//type buyer first name
		.typeFirstName(firstName)

		//type buyer last name
		.typeLastName(lastName)

		//type buyer street address
		.typeStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))

		//Select buyer country
		.selectCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))

		//type or select state for buyer
		.selectStateOrProvince(f_dsm.getInputParameter("STATE"))

		//type buyer city
		.typeCity(f_dsm.getInputParameter("CITY"))

		//type buyer zipcode
		.typeZipCode(f_dsm.getInputParameter("ZIPCODE"))

		//type buyer E-mail
		.typeEmail(f_dsm.getInputParameter("EMAIL"))

		//type buyer home phone number
		.typePhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))

		//Check if preferred language drop down is visible
		.verifyPreferredLanguageDropDownListPresent()			

		//Select buyer preferred language
		.selectPreferredCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))

		//Check if preferred currency drop down is visible
		.verifyPreferredCurrencyDropDownListPresent()

		//Select buyer preferred language
		.selectPreferredLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))

		//Submit registration
		.submitUnsuccessfulBuyerRegistration();

		rp.verifyMismatchTooltipPresent();

		rp.typePassword(f_dsm.getInputParameter("PASSWORD"))

				.submitBuyerRegistration();

		// Approve Buyer
		BuyersApprovalPage buyerApprovalPage = approveBuyers(fullName);

		//Sign out
		HeaderWidget header = buyerApprovalPage.getHeaderWidget().openSignOutDropDownWidget().signOutB2B();

		header.signIn()

		//Enter a valid username
		.typeUsername(buyerUsername)

		//Enter a valid password
		.typePassword(f_dsm.getInputParameter("PASSWORD"))

		//Click Sign In
		.signIn(HeaderWidget.class);
	}

	/**
	 * Test case register buyer with an invalid password.
	 */
	@Test
	public void FB2BSTOREB2BA_10_08() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPanel.registerB2B();

		String buyerUsername = f_dsm.getInputParameter("BUYER_LOGONID") + System.currentTimeMillis();

		String firstName = f_dsm.getInputParameter("FIRST_NAME");

		String lastName = f_dsm.getInputParameter("LAST_NAME");

		String fullName = firstName + " " + lastName;

		rp.selectBuyerRegister()

		//type buyer username
		.typeLogonId(buyerUsername)

		//type organization name
		.typeOrganization(organizationName)

		//type buyer password
		.typePassword(f_dsm.getInputParameter("INVALID_PASSWORD"))

		//Verify buyer password verify
		.typeVerifyPassword(f_dsm.getInputParameter("INVALID_PASSWORD_VERIFY"))

		//type buyer first name
		.typeFirstName(firstName)

		//type buyer last name
		.typeLastName(lastName)

		//type buyer street address
		.typeStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))

		//Select buyer country
		.selectCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))

		//type or select state for buyer
		.selectStateOrProvince(f_dsm.getInputParameter("STATE"))

		//type buyer city
		.typeCity(f_dsm.getInputParameter("CITY"))

		//type buyer zipcode
		.typeZipCode(f_dsm.getInputParameter("ZIPCODE"))

		//type buyer E-mail
		.typeEmail(f_dsm.getInputParameter("EMAIL"))

		//type buyer home phone number
		.typePhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))

		//Check if preferred language drop down is visible
		.verifyPreferredLanguageDropDownListPresent()			

		//Select buyer preferred language
		.selectPreferredCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))

		//Check if preferred currency drop down is visible
		.verifyPreferredCurrencyDropDownListPresent()

		//Select buyer preferred language
		.selectPreferredLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))

		//Submit registration
		.submitUnsuccessfulBuyerRegistration();

		rp.verifyPasswordErrorMessage();

		rp.typePassword(f_dsm.getInputParameter("PASSWORD"))

				.typeVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))

				.submitBuyerRegistration();

		// Approve Buyer
		BuyersApprovalPage buyerApprovalPage = approveBuyers(fullName);

		//Sign out
		HeaderWidget header = buyerApprovalPage.getHeaderWidget().openSignOutDropDownWidget().signOutB2B();

		header.signIn()

		//Enter a valid username
		.typeUsername(buyerUsername)

		//Enter a valid password
		.typePassword(f_dsm.getInputParameter("PASSWORD"))

		//Click Sign In
		.signIn(HeaderWidget.class);
	}

	/**
	 * Test case register organization but cancel before submitting for approval.
	 */
	@Test
	public void FB2BSTOREB2BA_10_09() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPanel.registerB2B();

		String buyerUsername = f_dsm.getInputParameter("BUYER_LOGONID") + System.currentTimeMillis();

		rp.selectBuyerRegister()

		//type buyer username
		.typeLogonId(buyerUsername)

		//type organization name
		.typeOrganization(organizationName)

		//type buyer password
		.typePassword(f_dsm.getInputParameter("PASSWORD"))

		//Verify buyer password verify
		.typeVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))

		//type buyer first name
		.typeFirstName(f_dsm.getInputParameter("FIRST_NAME"))

		//type buyer last name
		.typeLastName(f_dsm.getInputParameter("LAST_NAME"))

		//type buyer street address
		.typeStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))

		//Select buyer country
		.selectCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))

		//type or select state for buyer
		.selectStateOrProvince(f_dsm.getInputParameter("STATE"))

		//type buyer city
		.typeCity(f_dsm.getInputParameter("CITY"))

		//type buyer zipcode
		.typeZipCode(f_dsm.getInputParameter("ZIPCODE"))

		//type buyer E-mail
		.typeEmail(f_dsm.getInputParameter("EMAIL"))

		//type buyer home phone number
		.typePhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))

		//Check if preferred language drop down is visible
		.verifyPreferredLanguageDropDownListPresent()			

		//Select buyer preferred language
		.selectPreferredCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))

		//Check if preferred currency drop down is visible
		.verifyPreferredCurrencyDropDownListPresent()

		//Select buyer preferred language
		.selectPreferredLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))

		//Cancel registration
		.cancel();
	}

	/**
	 * Test case register buyer under an existing sub-organization.
	 */
	@Test
	public void FB2BSTOREB2BA_10_11() throws Exception
	{
		//Open AuroraB2BEsite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	

		//Click on the SignIn page link on the header
		SignInDropdownWidget signInPanel = frontPage.getHeaderWidget().signIn();

		//Click on the registration button on the SignIn page
		CustomerRegisterationPageB2B rp = signInPanel.registerB2B();

		String buyerUsername = f_dsm.getInputParameter("BUYER_LOGONID") + System.currentTimeMillis();

		String firstName = f_dsm.getInputParameter("FIRST_NAME");

		String lastName = f_dsm.getInputParameter("LAST_NAME");

		String fullName = firstName + " " + lastName;

		SignInPageB2B signInPage = rp.selectBuyerRegister()

				//type buyer username
				.typeLogonId(buyerUsername)

				//type organization name
				.typeOrganization(subOrganizationName + "/" + organizationName)

				//type buyer password
				.typePassword(f_dsm.getInputParameter("PASSWORD"))

				//Verify buyer password verify
				.typeVerifyPassword(f_dsm.getInputParameter("PASSWORD_VERIFY"))

				//type buyer first name
				.typeFirstName(firstName)

				//type buyer last name
				.typeLastName(lastName)

				//type buyer street address
				.typeStreetAddressLine1(f_dsm.getInputParameter("ADDRESS"))

				//Select buyer country
				.selectCountryOrRegion(f_dsm.getInputParameter("COUNTRY"))

				//type or select state for buyer
				.selectStateOrProvince(f_dsm.getInputParameter("STATE"))

				//type buyer city
				.typeCity(f_dsm.getInputParameter("CITY"))

				//type buyer zipcode
				.typeZipCode(f_dsm.getInputParameter("ZIPCODE"))

				//type buyer E-mail
				.typeEmail(f_dsm.getInputParameter("EMAIL"))

				//type buyer home phone number
				.typePhoneNumber(f_dsm.getInputParameter("PHONE_NUMBER"))

				//Check if preferred language drop down is visible
				.verifyPreferredLanguageDropDownListPresent()			

				//Select buyer preferred language
				.selectPreferredCurrency(f_dsm.getInputParameter("PREFERRED_CURRENCY"))

				//Check if preferred currency drop down is visible
				.verifyPreferredCurrencyDropDownListPresent()

				//Select buyer preferred language
				.selectPreferredLanguage(f_dsm.getInputParameter("PREFERRED_LANGUAGE"))

				//Submit registration
				.submitBuyerRegistration();

		//verify registration confirmation message
		signInPage.verifyBuyerRegistrationConfirmationMessage();

		// Approve Buyer
		BuyersApprovalPage buyerApprovalPage = approveBuyers(fullName);

		//Sign out
		HeaderWidget header = buyerApprovalPage.getHeaderWidget().openSignOutDropDownWidget().signOutB2B();

		header.signIn()

		//Enter a valid username
		.typeUsername(buyerUsername)

		//Enter a valid password
		.typePassword(f_dsm.getInputParameter("PASSWORD"))

		//Click Sign In
		.signIn(HeaderWidget.class);
	}
}