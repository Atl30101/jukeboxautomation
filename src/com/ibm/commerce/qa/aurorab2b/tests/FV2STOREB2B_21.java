package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.ShippingAndBillingPage;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.ProductDisplayPageB2B;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.wte.util.WcConfigManager;


/** 
 * This scenario tests adding or removing promotions to/from an order	
 * Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2B_21 extends AbstractAuroraSingleSessionTests
{
	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

	//A Variable to retrieve data from the data file. 
	@DataProvider
	private final TestDataProvider dsm;
	
	private CMC cmc;
	
	/**A variable to hold the promotion identifier created**/
	private String promotionId;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with getConfig().properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_caslFixtures 
	 */		
	@Inject
	public FV2STOREB2B_21(
			Logger log, 
			WcConfigManager config,
			WcWteTestRule wcWebTestRule,
			CaslFoundationTestRule caslTestRule,
			TestDataProvider dataSetManager,
			CMC cmc)
	{
		super(log, wcWebTestRule, caslTestRule);
		
		this.dsm = dataSetManager;
		this.cmc = cmc;
	}

	/**
	 * As a registered shopper, apply a valid promotion from the shopping cart page
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2B_2101() throws Exception
	{		
		//login to CMC
		dsm.setDataLocation("testFV2STOREB2B_2101", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore();
		
		dsm.setDataLocation("testFV2STOREB2B_2101", "Create_Promotion");
		promotionId = cmc.getPromotionId(dsm.getInputParameter("PROMOTION_NAME"));
		if (promotionId == null) {
			promotionId = cmc.createPromotion(dsm.getInputParameter("PROMOTION_NAME"), dsm.getInputParameter("PROMOTION_TYPE"));
			cmc.createPromotionElementforOrderLevelPromotion(dsm.getInputParameter("minimunOrderAmount"), dsm.getInputParameter("discountAmount"));
		}
		if (!cmc.isPromotionActivated(promotionId)) {
			cmc.activatePromotion(promotionId);
		}
		
		dsm.setDataLocation("testFV2STOREB2B_2101", "testFV2STOREB2B_2101");
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log on store
		HeaderWidget header = signIn.typeUsername(dsm.getInputParameter("LOGONID"))
		.typePassword(dsm.getInputParameter("PASSWORD"))
		.signIn();
		
		//cleanup
		CartCleanup(header);
		
		//Open Sub category Apparel->Dresses.
		CategoryPage subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
				dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"));
		
		//click on product image on Category Display page.
		ProductDisplayPageB2B productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByNameB2B(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Change the quantity of the product
		productDisplay.getSkuListWidget().updateQuantity(dsm.getInputParameter("SKU_NAME"),dsm.getInputParameter("ITEM_QTY"));
		
		//click Add to Cart from Product Display page.
		productDisplay.addToCurrentOrder(dsm.getInputParameter("SKU_NAME"));
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();

		//Confirm product is available
		shopCart.verifyItemInShopCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Enter promotion code
		shopCart.typePromoCode(dsm.getInputParameter("PROMO_CODE"));
		
		//Confirm promotion code
		shopCart.verifyPromoCodeIsOnPage(dsm.getInputParameter("PROMO_CODE_POSITION"), dsm.getInputParameter("PROMO_CODE"));
		
		//Confirm product discount
		shopCart.verifyDiscounts(dsm.getInputParameter("DISCOUNT"));
		
		//Confirm order sub total
		shopCart.verifyOrderSubTotal(dsm.getInputParameter("SUBTOTAL"));
		
		//Confirm order total
		shopCart.verifyOrderTotal(dsm.getInputParameter("TOTAL"));
		
		//Remove promotion code
		shopCart.removePromoCode(dsm.getInputParameter("PROMO_CODE_POSITION"));
	}
	
	/**
	 * As a registered shopper, apply a valid catalog entry level promotion from the shipping and billing page
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2B_2102() throws Exception
	{
		dsm.setDataLocation("testFV2STOREB2B_2102", "CMClogon");
		cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
		
		//select store and catalog
		cmc.setLocale(dsm.getInputParameter("locale"));
		cmc.selectStore();
		
		dsm.setDataLocation("testFV2STOREB2B_2102", "Create_Promotion");
		promotionId = cmc.getPromotionId(dsm.getInputParameter("PROMOTION_NAME"));
		if (promotionId == null) {
			promotionId = cmc.createPromotion(dsm.getInputParameter("PROMOTION_NAME"), dsm.getInputParameter("PROMOTION_TYPE"));
			cmc.createPromotionElementforOrderLevelPromotion(dsm.getInputParameter("minimunOrderAmount"), dsm.getInputParameter("discountAmount"),dsm.getInputParameter("elementSubType1"),dsm.getInputParameter("elementSubType2"));
		}
		if (!cmc.isPromotionActivated(promotionId)) {
			cmc.activatePromotion(promotionId);
		}
		
		dsm.setDataLocation("testFV2STOREB2B_2102", "testFV2STOREB2B_2102");
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log on store
		HeaderWidget header = signIn.typeUsername(dsm.getInputParameter("LOGONID"))
		.typePassword(dsm.getInputParameter("PASSWORD"))
		.signIn();
		
		//cleanup
		CartCleanup(header);
		
		//Open Sub category Apparel->Dresses.
		CategoryPage subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
						dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"));
				
		//click on product image on Category Display page.
		ProductDisplayPageB2B productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByNameB2B(dsm.getInputParameter("PRODUCT_NAME"));
				
		//Change the quantity of the product
		productDisplay.getSkuListWidget().updateQuantity(dsm.getInputParameter("SKU_NAME"),dsm.getInputParameter("ITEM_QTY"));
				
		//click Add to Cart from Product Display page.
		productDisplay.addToCurrentOrder(dsm.getInputParameter("SKU_NAME"));
		
		//click on Shopping Cart link from the header
		ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();

		ShippingAndBillingPage shipAndAbill = shopCart.continueToNextStep();
		//Confirm product is available
		shipAndAbill.verifyItemIsInCart(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Enter promotion code
		shipAndAbill.typePromoCode(dsm.getInputParameter("PROMO_CODE"));
		
		//Confirm promotion code
		shipAndAbill.verifyPromoCode(dsm.getInputParameter("PROMO_CODE_POSITION"), dsm.getInputParameter("PROMO_CODE"));
		
		//Confirm product discount
		shipAndAbill.verifyDiscounts(dsm.getInputParameter("DISCOUNT"));
		
		//Confirm order total
		shipAndAbill.verifyOrderSubTotal(dsm.getInputParameter("SUBTOTAL"));
		
		//Confirm order total
		shipAndAbill.verifyOrderTotal(dsm.getInputParameter("TOTAL"));
		
		//Remove promotions
		shipAndAbill.removePromoCode(dsm.getInputParameter("PROMO_CODE_POSITION"));
	}
	
	/**
	 * cleanup method to remove all items from the cart before proceeding.
	 * @param header
	 */
		private void CartCleanup(HeaderWidget header)
		{
			
				try {
					header.verifyMiniCartItemCount("0");
				} catch (Exception e) {
					//cart isn't 0, cleaning up
					header.goToShoppingCartPage().removeAllItems();
				}
			
		}
		
		
		/**
		 * As a registered shopper, apply an invalid promotion from the shipping and billing page
		 * @throws Exception
		 */
		@Test
		public void testFV2STOREB2B_2103() throws Exception
		{		
			//Open the store in the browser.
			AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
			
			//Opens the Sign In page in browser.
			SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
			
			//Log on store
			HeaderWidget header = signIn.typeUsername(dsm.getInputParameter("LOGONID"))
			.typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn();
			
			//cleanup
			CartCleanup(header);
			
			//Open Sub category
			CategoryPage subCat = header.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
							dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"));
					
			//click on product image on Category Display page.
			ProductDisplayPageB2B productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByNameB2B(dsm.getInputParameter("PRODUCT_NAME"));
					
			//Change the quantity of the product
			productDisplay.getSkuListWidget().updateQuantity(dsm.getInputParameter("SKU_NAME"),dsm.getInputParameter("ITEM_QTY"));
					
			//click Add to Cart from Product Display page.
			productDisplay.addToCurrentOrder(dsm.getInputParameter("SKU_NAME"));
		
			//click on Shopping Cart link from the header
			ShopCartPage shopCart = productDisplay.getHeaderWidget().goToShoppingCartPage();

			//Confirm product is available
			shopCart.verifyItemInShopCart(dsm.getInputParameter("PRODUCT_NAME"));
			
			//Enter promotion code
			shopCart.typePromoCode(dsm.getInputParameter("PROMO_CODE"));
			
			//Confirm header message
			shopCart.getHeaderWidget().verifyErrorMessage(dsm.getInputParameter("ERROR_MESSAGE"));
		}
		
		
		/**
		 * Perform teardown operations after the test is done, whether it is successful or not.
		 * @throws Exception
		 */
		@After
		public void testScenarioTearDown() throws Exception
		{
			
			try
			{
				getLog().info("testScenarioTearDown running");
				
				dsm.setDataLocation("testScenarioTearDown", "CMClogon");
				cmc.logon(dsm.getInputParameter("logonId"), dsm.getInputParameter("password"));
				cmc.setLocale(dsm.getInputParameter("locale"));
				cmc.setTimeZone("America/Montreal");
				
				//Select the store to work on
				cmc.selectStore();

				//Deactivate and delete promotion
				cmc.deactivatePromotion(promotionId);
				cmc.deletePromotion(promotionId);
				
				dsm.setDataLocation("testScenarioTearDown", "testScenarioTearDown");
				
				cmc.logoff();
				
				//Open the store in the browser.
				AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
				
				//Opens the Sign In page in browser.
				SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
				
				//Log on store
				HeaderWidget header = signIn.typeUsername(dsm.getInputParameter("LOGONID"))
				.typePassword(dsm.getInputParameter("PASSWORD"))
				.signIn();
				
				//cleanup
				CartCleanup(header);
			}
			catch(RuntimeException e)
			{
				getLog().log(Level.SEVERE, "tearDown threw an exception: ", e);
			}

		}			
			
}
