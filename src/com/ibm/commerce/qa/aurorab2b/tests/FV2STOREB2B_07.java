package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2009, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */


//Import the task libraries for use in this test script

import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.BundleDisplayPage;
import com.ibm.commerce.qa.aurora.page.CategoryPage;
import com.ibm.commerce.qa.aurora.page.ComparePage;
import com.ibm.commerce.qa.aurora.page.KitDisplayPage;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.MyOrderHistoryPage;
import com.ibm.commerce.qa.aurora.page.ProductDisplayPage;
import com.ibm.commerce.qa.aurora.page.QuickInfoPopup;
import com.ibm.commerce.qa.aurora.page.ShopCartPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.MyWishListPageB2B;
import com.ibm.commerce.qa.aurorab2b.page.ProductDisplayPageB2B;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.Accelerator;
import com.ibm.commerce.qa.url.CMC;
import com.ibm.commerce.qa.url.DeltaUpdates;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;


/**
 * Scenario: FV2STOREB2B_07
 * Details: Test add to shopping cart flows.
 *
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2B_07 extends AbstractAuroraSingleSessionTests
{

    /**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;
	
	//A Variable to retrieve data from the data file.
	@DataProvider
	private final TestDataProvider dsm;
	
	private final CaslFixturesFactory f_CaslFixtures;
	
	private CMC cmc;
	
	private Accelerator accelerator;
	
	private DeltaUpdates deltaUpdate;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with config.properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_CaslFixtures 
	 */	@Inject
	public FV2STOREB2B_07(
			Logger log, 
			CaslFoundationTestRule caslTestRule,
			WcWteTestRule wcWebTestRule,
			TestDataProvider dataSetManager,	
			CMC cmc,
			Accelerator accelerator,
			DeltaUpdates deltaUpdate,
			CaslFixturesFactory p_CaslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);

		this.dsm = dataSetManager;
		this.cmc = cmc;
		this.accelerator = accelerator;
		this.deltaUpdate = deltaUpdate;
		
		f_CaslFixtures = p_CaslFixtures;
		
	}
	
	 	
		 
	//cleanup method to remove all items from the cart before proceeding.
	
	private void cartCleanup(MyAccountMainPage page)
	{		
			
				//removing items from cart
				OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
				orders.removeAllItemsFromCart();
				
	}

	/**
	 * Test case to add a product to the shopping cart from the product's details page as a registered shopper
	 *
	 * @throws Exception
	 */	
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2B_0701() throws Exception
	{		
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		MyAccountMainPage myAccount = signIn.typeUsername(dsm.getInputParameter("LOGONID"))
											.typePassword(dsm.getInputParameter("PASSWORD"))
											.signIn()
											.goToMyAccount();
		
		//cleanup
		cartCleanup(myAccount);
		
		//Go to department page				
		CategoryPage subCat= myAccount.getHeaderWidget()
				.goToCategoryPageByHierarchy(CategoryPage.class, 
						dsm.getInputParameter("SELECT_TOP_CATEGORY_NAME"),
						dsm.getInputParameter("SELECT_SUB_CATEGORY_NAME"));
						
		
		//click on product image on Category Display page.
		ProductDisplayPageB2B productDisplay = subCat.getCatalogEntryListWidget().goToProductPageByNameB2B(dsm.getInputParameter("PRODUCT_NAME"));
		
		//Select the product attributes			
		productDisplay.getDefiningAttributesWidget()
			.selectAttributeFromDropdown(dsm.getInputParameter("ATTR1_NAME"), dsm.getInputParameter("ATTR1_VALUE"))
			.selectAttributeFromDropdown(dsm.getInputParameter("ATTR2_NAME"), dsm.getInputParameter("ATTR2_VALUE"))
			.selectAttributeFromDropdown(dsm.getInputParameter("ATTR3_NAME"), dsm.getInputParameter("ATTR3_VALUE"))
			.selectAttributeFromDropdown(dsm.getInputParameter("ATTR4_NAME"), dsm.getInputParameter("ATTR4_VALUE"));
		
		//change quantity of sku
		productDisplay.getSkuListWidget().updateQuantity(dsm.getInputParameter("PRODUCT_SKU"), dsm.getInputParameter("ITEM_QTY"));
		
		//click Add to Current Order from Product Display page.
		productDisplay.addToCurrentOrder(dsm.getInputParameter("PRODUCT_SKU"));
		
		//verify quantity in cart
		productDisplay.getHeaderWidget().verifyMiniCartItemCount(dsm.getInputParameter("ITEM_QTY"));
		
		//Confirm the price on minishopping cart
		productDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
		
		//complete order
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
	
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();
	
	}
	
	/**
	 * Add a SKU to the current order from an e-spot
	 * @throws Exception
	 */	
	@Test
	public void testFV2STOREB2B_0703() throws Exception{
		
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		signIn.typeUsername(dsm.getInputParameter("LOGONID"))
				.typePassword(dsm.getInputParameter("PASSWORD"))
				.signIn();
		
		//Click first item in ProductRecommendationWidget
		//Note: Fasteners = ProductDisplayPageB2B class
		//      Lighting = ProductDisplayPage class		
		ProductDisplayPage displayPage = frontPage.getfeaturedProductsWidget().viewCatentryNumber(ProductDisplayPage.class, 1);
		
		//Select the product attributes			
		displayPage.getDefiningAttributesWidget()
			.selectAttributeFromDropdown(dsm.getInputParameter("ATTR1_NAME"), dsm.getInputParameter("ATTR1_VALUE"))
			.selectAttributeFromDropdown(dsm.getInputParameter("ATTR2_NAME"), dsm.getInputParameter("ATTR2_VALUE"))
			.selectAttributeFromDropdown(dsm.getInputParameter("ATTR3_NAME"), dsm.getInputParameter("ATTR3_VALUE"));			
		
		//update quantity
		displayPage.getShopperActionsWidget().updateQuantity(dsm.getInputParameter("QTY"));
		
		//click Add to Current Order from Product Display page.
		displayPage.addToCart();
		
		//complete order
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
	
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();
	}
	
	/** 
	 * Add a Bundle to the shopping cart from the search results page as a registered shopper
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2B_0704() throws Exception 
	{			
		String bundleId = null;

		//login to CMC as admin
		cmc.logon(dsm.getInputParameter("CMC_LOGONID"), dsm.getInputParameter("CMC_PASSWORD"));
		cmc.setLocale(dsm.getInputParameter("locale"));
				
		//select store and 
		cmc.selectStore();
		String masterCatalogId = cmc.selectCatalog();
				
		//opening catalog management tool in cmc
		String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
		//creating a bundle
		dsm.setDataLocation("testFV2STOREB2B_0704", "CMCCreateBundle");
		try
		{
			bundleId = cmc.createProduct(dsm.getInputParameter("parentCategory"), dsm.getInputParameter("partNumber"), dsm.getInputParameter("productName"), dsm.getInputParameter("published"));
		
		} catch (IllegalStateException e) {
			
			bundleId = cmc.getCatalogEntryId(dsm.getInputParameter("productName"));			
		}
		
		System.out.println("Bundle ID: " + bundleId);
				
		//creating first component
		dsm.setDataLocation("testFV2STOREB2B_0704", "CMCcreateComponen1");
		cmc.createComponent(dsm.getInputParameter("parentCatentryPartNumber"), dsm.getInputParameter("componentPartNumber"), dsm.getInputParameter("quantity"), dsm.getInputParameter("sequence"));
		
		//creating second component
		dsm.setDataLocation("testFV2STOREB2B_0704", "CMCcreateComponen2");
		cmc.createComponent(dsm.getInputParameter("parentCatentryPartNumber"), dsm.getInputParameter("componentPartNumber"), dsm.getInputParameter("quantity"), dsm.getInputParameter("sequence"));
			
				//Wait until delta update completes before timeout
				deltaUpdate.beforeWaitForDelta(masterCatalogId);
				getSession().startAtStorePreviewPage(previewURL, AuroraFrontPageB2B.class);
				deltaUpdate.waitForDelta(masterCatalogId, 300);
				
				//Open the store in the browser.
				AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
								
				//Opens the Sign In page in browser.
				SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
								
				//Log in to the store.
				dsm.setDataLocation("testFV2STOREB2B_0704", "testFV2STOREB2B_0704");		
				MyAccountMainPage myAccount = signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
					.signIn().goToMyAccount();				
				
				//cleanup
				cartCleanup(myAccount);
				
				//Perform search for keywork bundle
				BundleDisplayPage bundleDisplay = myAccount.getHeaderWidget().performSearch(BundleDisplayPage.class, dsm.getInputParameter("SEARCH_TERM"));
						
				//Update bundle quantity
				bundleDisplay.updateBundleQuantity(dsm.getInputParameter("FIRST_ITEM_IN_BUNDLE"), (dsm.getInputParameter("FIRST_ITEM_IN_BUNDLE_QUANTITY")));
				
				//Update bundle quantity
				bundleDisplay.updateBundleQuantity(dsm.getInputParameter("SECOND_ITEM_IN_BUNDLE"), (dsm.getInputParameter("SECOND_ITEM_IN_BUNDLE_QUANTITY")));
								
				//Select the product attributes	for both items in bundle				
				bundleDisplay.getComponentsWidget()
							.selectAttributeFromDropdown(dsm.getInputParameter("FIRST_ITEM_IN_BUNDLE"), dsm.getInputParameter("ATTR_BUNDLEITM1_NAME_1"), dsm.getInputParameter("ATTR_BUNDLEITM1_VALUE_1"))
							.selectAttributeFromDropdown(dsm.getInputParameter("FIRST_ITEM_IN_BUNDLE"), dsm.getInputParameter("ATTR_BUNDLEITM1_NAME_2"), dsm.getInputParameter("ATTR_BUNDLEITM1_VALUE_2"))
							.selectAttributeFromDropdown(dsm.getInputParameter("FIRST_ITEM_IN_BUNDLE"), dsm.getInputParameter("ATTR_BUNDLEITM1_NAME_3"), dsm.getInputParameter("ATTR_BUNDLEITM1_VALUE_3"))
							.selectAttributeFromDropdown(dsm.getInputParameter("SECOND_ITEM_IN_BUNDLE"), dsm.getInputParameter("ATTR_BUNDLEITM2_NAME_1"), dsm.getInputParameter("ATTR_BUNDLEITM2_VALUE_2"))
							.selectAttributeFromDropdown(dsm.getInputParameter("SECOND_ITEM_IN_BUNDLE"), dsm.getInputParameter("ATTR_BUNDLEITM2_NAME_2"), dsm.getInputParameter("ATTR_BUNDLEITM2_VALUE_2"));
							
				
				//Add to cart
				bundleDisplay.addToCart();
				
				//Verify product is in mini cart
				bundleDisplay.getHeaderWidget().verifyProductNameInMiniCart(dsm.getInputParameter("FIRST_ITEM_IN_BUNDLE"));
				
				//Verify product is in mini cart
				bundleDisplay.getHeaderWidget().verifyProductNameInMiniCart(dsm.getInputParameter("SECOND_ITEM_IN_BUNDLE"));
						
				//Confirm the item count in mini shopping cart
				bundleDisplay.getHeaderWidget().verifyMiniCartItemCount(dsm.getInputParameter("NUMBER_OF_ITEMS"));
				
				//Confirm the item total price in mini shopping cart
				bundleDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
						
				OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
				
				orders.addPayLaterPaymentMethod();
				
				orders.completeOrder();	

				dsm.setDataLocation("testFV2STOREB2B_0704", "testFV2STOREB2B_0704");
				cmc.logon(dsm.getInputParameter("CMC_LOGONID"), dsm.getInputParameter("CMC_PASSWORD"));
				cmc.deleteCatalogEntry(bundleId);				
				cmc.logoff();				


	}
	
	/**
	 * Add a Kit to the shopping cart from the Wish List of a registered shopper
	 *
	 * 
	 * @throws Exception
	 */
	
	@Test
	public void testFV2STOREB2B_0705() throws Exception 
	{
		

			String kitId = null;
			Boolean wishListsEnabledByDefault = false;
			final String wishlistFeature = "SOAWishlist";
			boolean loggedIntoCMC = false;
			
				//login to CMC as admin
				cmc.logon(dsm.getInputParameter("ADMIN_LOGONID"), dsm.getInputParameter("ADMIN_PASSWORD"));
				loggedIntoCMC = true;
				cmc.setLocale(dsm.getInputParameter("locale"));
				
				//select store and 
				cmc.selectStore();
				String masterCatalogId = cmc.selectCatalog();
				
				//enable wishlists
				wishListsEnabledByDefault = cmc.isChangeFlowOptionEnabled(wishlistFeature);
				if(!wishListsEnabledByDefault){
					cmc.modifyChangeFlowOptions(new String[] {wishlistFeature}, null);
				}
				
				//creating a kit
				dsm.setDataLocation("testFV2STOREB2B_0705", "CMCCreateKit");
				try
				{
					kitId = cmc.createProduct(dsm.getInputParameter("parentCategory"), dsm.getInputParameter("partNumber"), dsm.getInputParameter("productName"), dsm.getInputParameter("published"));
				}catch(IllegalStateException e)
				{
					kitId= cmc.getCatalogEntryId(dsm.getInputParameter("partNumber"));
				}
				//creating first component
				dsm.setDataLocation("testFV2STOREB2B_0705", "CMCcreateComponen1");
				cmc.createComponent(dsm.getInputParameter("parentCatentryPartNumber"), dsm.getInputParameter("componentPartNumber"), dsm.getInputParameter("quantity"), dsm.getInputParameter("sequence"));

				//creating second component
				dsm.setDataLocation("testFV2STOREB2B_0705", "CMCcreateComponen2");
				cmc.createComponent(dsm.getInputParameter("parentCatentryPartNumber"), dsm.getInputParameter("componentPartNumber"), dsm.getInputParameter("quantity"), dsm.getInputParameter("sequence"));
				
				//Creating list price for this kit
				dsm.setDataLocation("testFV2STOREB2B_0705", "CMCListPrice");
				cmc.createListPriceForCatalogEntry(kitId);

				//Creating offer price for this kit
				dsm.setDataLocation("testFV2STOREB2B_0705", "CMCOfferPrice");
				cmc.createOfferPriceForCatalogEntry(kitId);	
				
				//temporarily log off for accelerator
				cmc.logoff();
				loggedIntoCMC = false;

				//Login to Accelerator, create inventory for kit, then log off
				dsm.setDataLocation("testFV2STOREB2B_0705", "testFV2STOREB2B_0705");
				accelerator.logon(dsm.getInputParameter("ADMIN_LOGONID"), dsm.getInputParameter("ADMIN_PASSWORD"));
				dsm.setDataLocation("testFV2STOREB2B_0705", "CMCCreateKit");
				accelerator.addAdHocReceiptForSKU(dsm.getInputParameter("partNumber"), "10", "50", "USD");				
				accelerator.logoff();
				
				//log back into CMC
				dsm.setDataLocation("testFV2STOREB2B_0705", "testFV2STOREB2B_0705");
				cmc.logon(dsm.getInputParameter("ADMIN_LOGONID"), dsm.getInputParameter("ADMIN_PASSWORD"));
				cmc.setLocale(dsm.getInputParameter("locale"));	
				cmc.selectStore();
				cmc.selectCatalog();
				loggedIntoCMC = true; //flag for teardown
				
				//Use store prview to build index. Wait until delta update completes before timeout
				deltaUpdate.beforeWaitForDelta(masterCatalogId);
				String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
				getSession().startAtStorePreviewPage(previewURL, AuroraFrontPageB2B.class);
				deltaUpdate.waitForDelta(masterCatalogId, 120);

				
				dsm.setDataLocation("testFV2STOREB2B_0705", "testFV2STOREB2B_0705");
				
				//Open the store in the browser.
				AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
				
				//Opens the Sign In page in browser.
				SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
				
				//Log in to the store.
				MyAccountMainPage myAccount = signIn.typeUsername(dsm.getInputParameter("LOGONID"))
													.typePassword(dsm.getInputParameter("PASSWORD"))
													.signIn()
													.goToMyAccount();
				
				//cleanup
				cartCleanup(myAccount);
				
				//Go to product display page
				KitDisplayPage productDisplay= myAccount.getHeaderWidget().typeSearchTerm("kit", true, false).executeSearch(KitDisplayPage.class);
								
				
				//Add kit to wish list				
				productDisplay.addToWishlistLoggedIn();
				
				//Go to my account page
				myAccount = productDisplay.getHeaderWidget().goToMyAccount();
				
				//Go to wish list page
				MyWishListPageB2B wishList = myAccount.getSidebar().goToB2BWishListPag();
				
				//Select a wish list	
				wishList.selectWishList(dsm.getInputParameter("WISHLISTNAME"));
				
				//Click Add to cart to add product
				wishList.addToCart(dsm.getInputParameter("KIT_NAME"));
				
				OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
				
				orders.addPayLaterPaymentMethod();
				
				orders.completeOrder();					
				
				wishList.removeAllItems();
				//An error occured during accelerator. Need to re-login to CMC
				if(!loggedIntoCMC){
					dsm.setDataLocation("testFV2STOREB2B_0705", "testFV2STOREB2B_0705");
					cmc.logon(dsm.getInputParameter("ADMIN_LOGONID"), dsm.getInputParameter("ADMIN_PASSWORD"));
					cmc.setLocale(dsm.getInputParameter("locale"));	
					cmc.selectStore();	
					cmc.selectCatalog();
				}
				
				//delete test kit
				if(kitId!=null)
					cmc.deleteCatalogEntry(kitId);
								
				//restore wishlist option
				if(!wishListsEnabledByDefault){
					cmc.modifyChangeFlowOptions(null, new String[] {wishlistFeature});
				}
				
				cmc.logoff();
		
	}
	
	
	
	
	
	/**
	 * Add a SKU to the shopping cart from the product compare page as a registered shopper
	 *
	 * 
	 * @throws Exception
	 */
	
	@Test
	public void testFV2STOREB2B_0706() throws Exception
	{
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		MyAccountMainPage myAccount = signIn.typeUsername(dsm.getInputParameter("LOGONID"))
											.typePassword(dsm.getInputParameter("PASSWORD"))
											.signIn()
											.goToMyAccount();
		
		//cleanup
		cartCleanup(myAccount);
		
		//Navigate to category page
		CategoryPage subCat = myAccount.getHeaderWidget()
				.goToCategoryPageByHierarchy(CategoryPage.class, 
						dsm.getInputParameter("TOP_CAT"),
						dsm.getInputParameter("SUB_CAT"));
		
		//Select compare box that appears on product
		subCat.getCatalogEntryListWidget().allowProductsForComparisonByPosition(new Integer(dsm.getInputParameter("ITEM_POSITION_1")));
		
		//Select compare box that appears on product
		subCat.getCatalogEntryListWidget().allowProductsForComparisonByPosition(new Integer(dsm.getInputParameter("ITEM_POSITION_2")));
		
		//Click compare button
		ComparePage productCompare = subCat.getCatalogEntryListWidget().compareProductsByPostion(new Integer(dsm.getInputParameter("ITEM_POSITION_1")));
		
		//Click add to cart button
		QuickInfoPopup quickInfo = productCompare.goToQuickInfoPopupByPosition(dsm.getInputParameter("BUTTON_POSITION"));
		
		//Spelect attributes for product
		quickInfo.updateValueForAttribute(dsm.getInputParameter("ATTRIBUTE_NAME1"), dsm.getInputParameter("ATTRIBUTE_VALUE1"));		
		quickInfo.updateValueForAttribute(dsm.getInputParameter("ATTRIBUTE_NAME2"), dsm.getInputParameter("ATTRIBUTE_VALUE2"));
		quickInfo.updateValueForAttribute(dsm.getInputParameter("ATTRIBUTE_NAME3"), dsm.getInputParameter("ATTRIBUTE_VALUE3"));
		
		//Click add to cart on quick info
		productCompare = quickInfo.addToCart(ComparePage.class);
		
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();		
		
		
	}
	
	
	/**
	 * Add a bundle to the current order from the department page
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2B_0708() throws Exception{
			
			String bundleId = null;
				
				//login to CMC as admin
				dsm.setDataLocation("testFV2STOREB2B_0708", "testFV2STOREB2B_0708");
				cmc.logon(dsm.getInputParameter("CMC_LOGONID"), dsm.getInputParameter("CMC_PASSWORD"));
				cmc.setLocale(dsm.getInputParameter("locale"));
				
				//select store and 
				cmc.selectStore();
				String masterCatalogId = cmc.selectCatalog();
				
				//opening catalog management tool in cmc
				String previewURL = cmc.storePreview("yyyy/MM/dd", "12HR", "true");
				//creating a bundle
				dsm.setDataLocation("testFV2STOREB2B_0708", "CMCCreateBundle");
				try {
					bundleId = cmc.createProduct(dsm.getInputParameter("parentCategory"), dsm.getInputParameter("partNumber"), dsm.getInputParameter("productName"), dsm.getInputParameter("published"));
					
				} catch (IllegalStateException e) {
					bundleId = cmc.getCatalogEntryId(dsm.getInputParameter("productName"));
				}
				
				System.out.println("Bundle ID: " + bundleId);
				
				//creating first component
				dsm.setDataLocation("testFV2STOREB2B_0708", "CMCcreateComponen1");
				cmc.createComponent(dsm.getInputParameter("parentCatentryPartNumber"), dsm.getInputParameter("componentPartNumber"), dsm.getInputParameter("quantity"), dsm.getInputParameter("sequence"));
		
				//creating second component
				dsm.setDataLocation("testFV2STOREB2B_0708", "CMCcreateComponen2");
				cmc.createComponent(dsm.getInputParameter("parentCatentryPartNumber"), dsm.getInputParameter("componentPartNumber"), dsm.getInputParameter("quantity"), dsm.getInputParameter("sequence"));
			
				//Wait until delta update completes before timeout
				deltaUpdate.beforeWaitForDelta(masterCatalogId);
				getSession().startAtStorePreviewPage(previewURL, AuroraFrontPageB2B.class);
				deltaUpdate.waitForDelta(masterCatalogId, 300);
				cmc.logoff();
				//Open the store in the browser.
				AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
								
				//Opens the Sign In page in browser.
				SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
								
				//Log in to the store.
				dsm.setDataLocation("testFV2STOREB2B_0708", "testFV2STOREB2B_0708");		
				MyAccountMainPage myAccount = signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
					.signIn().goToMyAccount();				
				
				//cleanup
				cartCleanup(myAccount);
				
				//Navigate to category page		
				CategoryPage subCat = myAccount.getHeaderWidget()
						.goToCategoryPageByHierarchy(CategoryPage.class, dsm.getInputParameter("TOP_CAT"), dsm.getInputParameter("SUB_CAT"));
																				
				//click on product image on Category Display page.
				BundleDisplayPage bundleDisplay = subCat.getCatalogEntryListWidget().goToBundleDisplayPageByName(dsm.getInputParameter("BUNDLE_NAME"));
				
				//Update bundle quantity
				bundleDisplay.updateBundleQuantity(dsm.getInputParameter("FIRST_ITEM_IN_BUNDLE"), (dsm.getInputParameter("FIRST_ITEM_IN_BUNDLE_QUANTITY")));
				
				//Update bundle quantity
				bundleDisplay.updateBundleQuantity(dsm.getInputParameter("SECOND_ITEM_IN_BUNDLE"), (dsm.getInputParameter("SECOND_ITEM_IN_BUNDLE_QUANTITY")));
								
				//Select the product attributes	for both items in bundle				
				bundleDisplay.getComponentsWidget()
							.selectAttributeFromDropdown(dsm.getInputParameter("FIRST_ITEM_IN_BUNDLE"), dsm.getInputParameter("ATTR_BUNDLEITM1_NAME_1"), dsm.getInputParameter("ATTR_BUNDLEITM1_VALUE_1"))
							.selectAttributeFromDropdown(dsm.getInputParameter("FIRST_ITEM_IN_BUNDLE"), dsm.getInputParameter("ATTR_BUNDLEITM1_NAME_2"), dsm.getInputParameter("ATTR_BUNDLEITM1_VALUE_2"))
							.selectAttributeFromDropdown(dsm.getInputParameter("FIRST_ITEM_IN_BUNDLE"), dsm.getInputParameter("ATTR_BUNDLEITM1_NAME_3"), dsm.getInputParameter("ATTR_BUNDLEITM1_VALUE_3"))
							.selectAttributeFromDropdown(dsm.getInputParameter("FIRST_ITEM_IN_BUNDLE"), dsm.getInputParameter("ATTR_BUNDLEITM1_NAME_4"), dsm.getInputParameter("ATTR_BUNDLEITM1_VALUE_4"))
							.selectAttributeFromDropdown(dsm.getInputParameter("SECOND_ITEM_IN_BUNDLE"), dsm.getInputParameter("ATTR_BUNDLEITM2_NAME_1"), dsm.getInputParameter("ATTR_BUNDLEITM2_VALUE_1"))
							.selectAttributeFromDropdown(dsm.getInputParameter("SECOND_ITEM_IN_BUNDLE"), dsm.getInputParameter("ATTR_BUNDLEITM2_NAME_2"), dsm.getInputParameter("ATTR_BUNDLEITM2_VALUE_2"))
							.selectAttributeFromDropdown(dsm.getInputParameter("SECOND_ITEM_IN_BUNDLE"), dsm.getInputParameter("ATTR_BUNDLEITM2_NAME_3"), dsm.getInputParameter("ATTR_BUNDLEITM2_VALUE_3"));
				
				//Add to cart
				bundleDisplay.addToCart();
				
				//Confirm the item count in mini shopping cart
				bundleDisplay.getHeaderWidget().verifyMiniCartItemCount(dsm.getInputParameter("NUMBER_OF_ITEMS"));
				
				//Confirm the item total price in mini shopping cart
				bundleDisplay.getHeaderWidget().verifyMiniCartTotalPrice(dsm.getInputParameter("TOTAL"));
						
				OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
				
				orders.addPayLaterPaymentMethod();
				
				orders.completeOrder();
				
				dsm.setDataLocation("testFV2STOREB2B_0708", "testFV2STOREB2B_0708");
				cmc.logon(dsm.getInputParameter("CMC_LOGONID"), dsm.getInputParameter("CMC_PASSWORD"));
				cmc.deleteCatalogEntry(bundleId);
				
				cmc.logoff();
	}
	
	/**
	 * Add a product to the shopping cart from the previous order by re-ordering as a registered shopper
	 *
	 */	
	@Test
	public void testFV2STOREB2B_0709() 
	{		
		//complete flow to reorder using service layer		
		OrdersFixture orders = f_CaslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		orders.removeAllItemsFromCart();
				
		//get orderId to compare to later. 
		final String orderId = orders.addItem(dsm.getInputParameter("SKU_NAME"), dsm.getInputParameterAsNumber("ITEM_QTY", Double.class));
	
		
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();	
		
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log in to the store.
		MyAccountMainPage myAccount= signIn.typeUsername(dsm.getInputParameter("LOGONID")).typePassword(dsm.getInputParameter("PASSWORD"))
			.signIn().goToMyAccount();
		
		
		//Go to my order history page
		MyOrderHistoryPage orderHistory = myAccount.getSidebar().goToOrderHistoryPage();
				
		//Click re-order button
		ShopCartPage shopCart = orderHistory.clickReOrderButton(orderId);
		
		shopCart.verifyItemInShopCart(dsm.getInputParameter("SKU_NAME"));
		
		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();		
		
	}
	
	@After
	public void tearDown() throws Exception {
		
		cmc.removeParameterInGlobalMap("catentryId");
		cmc.removeParameterInGlobalMap("parentCatentryId");
		
	}
	
	
}
