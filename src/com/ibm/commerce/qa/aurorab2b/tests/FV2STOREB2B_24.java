package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Logger;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.MyOrderHistoryPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.HeaderWidget;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.wte.util.WcConfigManager;


/** 
 * This scenario tests adding or removing promotions to/from an order	
 * Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2B_24 extends AbstractAuroraSingleSessionTests
{
	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

	//A Variable to retrieve data from the data file. 
	@DataProvider
	private final TestDataProvider dsm;

	private CaslFixturesFactory f_caslFixtures;
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with getConfig().properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_caslFixtures 
	 */		
	@Inject
	public FV2STOREB2B_24(
			Logger log, 
			WcConfigManager config,
			WcWteTestRule wcWebTestRule,
			CaslFoundationTestRule caslTestRule,
			TestDataProvider dataSetManager,
			CaslFixturesFactory p_CaslFixtures)
	{
		super(log, wcWebTestRule, caslTestRule);
		
		this.dsm = dataSetManager;
		
		f_caslFixtures = p_CaslFixtures;
	}

	/**
	 * Pagination support for order history page
	 * @throws Exception
	 */
	@Test
	public void testFV2STOREB2B_2411() throws Exception
	{		
		//create order history
		
		OrdersFixture orders = f_caslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
		
		int numberOfOrder = dsm.getInputParameterAsNumber("MAXNUMBEROFORDERS", Integer.class);
		for(int i = 0; i<=numberOfOrder; i++){
			orders.addItem(dsm.getInputParameter("SKU_NAME"),dsm.getInputParameterAsNumber("QUANTITIY",Double.class));
			
			orders.addPayLaterPaymentMethod();

			orders.completeOrder();			
		}
		
		//Open the store in the browser.
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);
		
		//Opens the Sign In page in browser.
		SignInDropdownWidget   signIn = frontPage.getHeaderWidget().signIn();
		
		//Log on store
		HeaderWidget header = signIn.typeUsername(dsm.getInputParameter("LOGONID"))
		.typePassword(dsm.getInputParameter("PASSWORD"))
		.signIn();
				
		MyAccountMainPage myAccount = header.goToMyAccount();
		
		MyOrderHistoryPage orderHistory =myAccount.getSidebar().goToOrderHistoryPage();
		
		orderHistory.verifyPaginationSectionVisible().selectNextPage();
		
		orderHistory.getHeaderWidget().openSignOutDropDownWidget().signOutB2B();

	}
	@After
	public void tearDown() throws Exception {
		//cart isn't 0, cleaning up
				OrdersFixture orders = f_caslFixtures.createOrdersFixture(dsm.getInputParameter("LOGONID"), dsm.getInputParameter("PASSWORD"), getConfig().getStoreName());
				orders.deletePaymentMethod();
				orders.removeAllItemsFromCart();
		
	}

	
}
