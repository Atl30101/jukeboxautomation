package com.ibm.commerce.qa.aurorab2b.tests;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.ibm.commerce.casl.foundation.util.CaslFoundationTestRule;
import com.ibm.commerce.qa.aurora.annotations.Sanity;
import com.ibm.commerce.qa.aurora.page.MyAccountMainPage;
import com.ibm.commerce.qa.aurora.page.QuickOrderPage;
import com.ibm.commerce.qa.aurora.util.AuroraModule;
import com.ibm.commerce.qa.aurora.widget.SignInDropdownWidget;
import com.ibm.commerce.qa.aurorab2b.page.AuroraFrontPageB2B;
import com.ibm.commerce.qa.casl.fixtures.CaslFixturesFactory;
import com.ibm.commerce.qa.casl.fixtures.OrdersFixture;
import com.ibm.commerce.qa.common.AbstractAuroraSingleSessionTests;
import com.ibm.commerce.qa.junit.WcWteTestRule;
import com.ibm.commerce.qa.url.StoreManagement;
import com.ibm.commerce.qa.util.junit.runners.GuiceTestRunner;
import com.ibm.commerce.qa.util.junit.runners.TestModules;
import com.ibm.commerce.qa.util.dataset.DataProvider;
import com.ibm.commerce.qa.util.dataset.TestDataProvider;
import com.ibm.commerce.qa.wte.util.WcConfigManager;


/** 
 * Test cases to update items in the shopping cart.
 * Refer to each test case for a detailed use case description
 */
@RunWith(GuiceTestRunner.class)
@TestModules(AuroraModule.class)
public class FV2STOREB2B_36 extends AbstractAuroraSingleSessionTests
{
	/**
	 * The internal copyright field.
	 */
	public static final String COPYRIGHT = com.ibm.commerce.qa.wte.framework.util.IBMCopyright.SHORT_COPYRIGHT;

	//A Variable to retrieve data from the data file. 
	@DataProvider
	private final TestDataProvider dsm;

	private StoreManagement storeManagement;
	
	private CaslFixturesFactory f_caslFixtures;
	
	/**
	 * Test Class object constructor.
	 * 
	 * @param log
	 * 			   logging object 
	 * @param config
	 * 			   object to work with getConfig().properties file
	 * @param session
	 * 			   factory to create browser sessions
	 * @param dataSetManager
	 * 			   object to work with data files
	 * @param p_caslFixtures 
	 */		
	@Inject
	public FV2STOREB2B_36(
			Logger log, 
			WcConfigManager config,
			WcWteTestRule wcWebTestRule,
			CaslFoundationTestRule caslTestRule,
			TestDataProvider dataSetManager,
			StoreManagement storeManagement,
			CaslFixturesFactory p_caslFixtures
			)
	{
		super(log, wcWebTestRule, caslTestRule);
		this.storeManagement = storeManagement;
		this.dsm = dataSetManager;
		f_caslFixtures = p_caslFixtures;
	}

	/**
	 * Test to quick order an item with valid SKU and valid quantity, as a registered shopper.
	 * @throws Exception
	 */
	@Category(Sanity.class)
	@Test
	public void testFV2STOREB2B_3601() throws Exception
	{	
		
		//Enable quick order if not already enabled.		
		storeManagement.enableChangeFlowOption(dsm.getInputParameter("QUICK_ORDER"));
		
		//Open Auroraesite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	
		
		//Click on the SignIn page link on the header
		SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
		
		//Enter a valid username
		MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
				
				//Enter a valid password
				.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
				
				//Click Sign In
				.signIn().goToMyAccount();
		
		//Click on Quick Order link in the footer
		QuickOrderPage quickOrder = myAccount.getFooterWidget().goToQuickOrderPage();
		
		
		//Fill the first row with a SKU and quantity
		//StyleHome Modern Rimmed 3 Piece Sofa Set
		quickOrder.addRowToQuickOrder(Integer.valueOf(dsm.getInputParameter("ROW_NO")), dsm.getInputParameter("SKU_NO"), dsm.getInputParameter("QTY"))
		
		//Click on the order button
		.order()
		
		//Verify the order success message
		.verifySuccessMessage();
		
		
//		complete order using WC commerce service layer
		OrdersFixture orders = f_caslFixtures.createOrdersFixture(dsm.getInputParameter("STORE_USER_NAME"), dsm.getInputParameter("STORE_USER_PASSWORD"),getConfig().getStoreName());

		orders.addPayLaterPaymentMethod();
		
		orders.completeOrder();
		

	}
	/**
	 * Test to quick order an item with valid SKU and in-valid quantity, as a registered shopper.
	 */
	@Test
	public void testFV2STOREB2B_3602() 
	{
		//must enable quick order from CMC
		//Open Auroraesite store
		AuroraFrontPageB2B frontPage = getSession().startAtPage(getConfig().getStoreUrl(), AuroraFrontPageB2B.class);	
				
				//Click on the SignIn page link on the header
				SignInDropdownWidget  signInPage = frontPage.getHeaderWidget().signIn();
				
				//Enter a valid username
				MyAccountMainPage myAccount = signInPage.typeUsername(dsm.getInputParameter("STORE_USER_NAME"))
						
						//Enter a valid password
						.typePassword(dsm.getInputParameter("STORE_USER_PASSWORD"))
						
						//Click Sign In
						.signIn().goToMyAccount();
				
				//Click on Quick Order link in the footer
				QuickOrderPage quickOrder = myAccount.getFooterWidget().goToQuickOrderPage()
				
				//Fill the first row with a SKU and quantity	
				//StyleHome Modern Rimmed 3 Piece Sofa Set
				.addRowToQuickOrder(Integer.valueOf(dsm.getInputParameter("ROW_NO")), dsm.getInputParameter("SKU_NO"), dsm.getInputParameter("QTY"))
				
				//Click on the "Order" button
				.order()
				
				//Verify the tooltip error message
				.verifyErrorTooltipPresent(dsm.getInputParameter("ERROR_MESSAGE"));
				
				
				quickOrder.getHeaderWidget().openSignOutDropDownWidget().signOutB2B();
	}
	
			
			
}
