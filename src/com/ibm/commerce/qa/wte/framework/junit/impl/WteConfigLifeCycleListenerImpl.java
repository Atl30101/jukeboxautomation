package com.ibm.commerce.qa.wte.framework.junit.impl;

import java.util.logging.Logger;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.ibm.commerce.qa.util.junit.rules.util.AbstractLifeCycleEventListener;
import com.ibm.commerce.qa.wte.framework.junit.WteConfigLifeCycleListener;
import com.ibm.commerce.qa.wte.framework.util.WteConfigManager;

public class WteConfigLifeCycleListenerImpl extends AbstractLifeCycleEventListener implements WteConfigLifeCycleListener
{
	private final Logger fLog;

	private final Provider<WteConfigManager> fConfigProvider;
	
	@Inject
	public WteConfigLifeCycleListenerImpl(Logger pLog, Provider<WteConfigManager> pConfigProvider)
	{
		fLog = pLog;
		fConfigProvider = pConfigProvider;
	}

	@Override
	public void oneTimeStartUpAction()
	{
		fLog.info("Loading WTE configuration.");
		fConfigProvider.get();
	}
	
	@Override
	public WteConfigManager getConfig()
	{
		return fConfigProvider.get();
	}
}
