package com.ibm.commerce.qa.wte.framework.junit;

import org.junit.rules.TestRule;

import com.ibm.commerce.qa.wte.framework.test.WebSession;
import com.ibm.commerce.qa.wte.framework.util.WteConfigManager;

/**
 * Implementations of this class allow you to easily configure and get access to
 * the {@link WebSession} and {@link WteConfigManager} from within you JUnit 4
 * test cases.
 * 
 */
public interface WteTestRule extends TestRule
{
	WebSession getSession();

	WteConfigManager getConfig();
}
