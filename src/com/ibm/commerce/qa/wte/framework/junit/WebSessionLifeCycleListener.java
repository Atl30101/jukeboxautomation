package com.ibm.commerce.qa.wte.framework.junit;

import com.ibm.commerce.qa.util.junit.rules.LifeCycleRuleEventListener;
import com.ibm.commerce.qa.wte.framework.test.WebSession;

/**
 * Used to manage a web session automatically for test cases. 
 *
 */
public interface WebSessionLifeCycleListener extends LifeCycleRuleEventListener
{
	public abstract WebSession getSession();
}
