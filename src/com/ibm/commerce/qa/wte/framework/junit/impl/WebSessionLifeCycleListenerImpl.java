package com.ibm.commerce.qa.wte.framework.junit.impl;

import java.util.logging.Logger;

import org.junit.runner.Description;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.ibm.commerce.qa.util.junit.rules.util.AbstractLifeCycleEventListener;
import com.ibm.commerce.qa.wte.framework.junit.WebSessionLifeCycleListener;
import com.ibm.commerce.qa.wte.framework.test.WebSession;

public class WebSessionLifeCycleListenerImpl extends AbstractLifeCycleEventListener implements WebSessionLifeCycleListener
{
	private final Logger fLog;
	
	/**
	 * A provider is used because we don't want to launch the session/browser
	 * prematurely, in case it is never used. It will be launched on demand.
	 */
	private final Provider<WebSession> fSessionProvider;

	private boolean sessionLaunchedOnce;
	
	@Inject
	public WebSessionLifeCycleListenerImpl(Logger pLog, Provider<WebSession> pSessionProvider)
	{
		super();
		fLog = pLog;
		fSessionProvider = pSessionProvider;
	}

	@Override
	public void beforeTestClassAction(Description pDescription)
	{
		// Launch the session
		fLog.info("Launch web session.");
		fSessionProvider.get();
		sessionLaunchedOnce = true;
	}
	
	@Override
	public void afterTestClassAction(Description pDescription)
	{
		fLog.info("Shutdown web session after all test have been executed.");
		fSessionProvider.get().state().shutdown();
	}
	
	@Override
	public void oneTimeShutdownAction()
	{
		if (sessionLaunchedOnce)
		{
			fLog.info("Web session launched at least once, so closing session during shutdown action.");
			fSessionProvider.get().state().shutdown();
		}
	}
	
	@Override
	public WebSession getSession()
	{
		return fSessionProvider.get();
	}
}
