package com.ibm.commerce.qa.wte.framework.junit;

import com.ibm.commerce.qa.util.junit.rules.LifeCycleRuleEventListener;
import com.ibm.commerce.qa.wte.framework.util.WteConfigManager;

public interface WteConfigLifeCycleListener extends LifeCycleRuleEventListener
{

	WteConfigManager getConfig();

}
