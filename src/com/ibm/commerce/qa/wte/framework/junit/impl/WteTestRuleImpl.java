package com.ibm.commerce.qa.wte.framework.junit.impl;

import java.util.logging.Logger;

import com.google.inject.Inject;
import com.ibm.commerce.qa.util.junit.rules.impl.DefaultJUnitLifeCycleRule;
import com.ibm.commerce.qa.wte.framework.junit.WebSessionLifeCycleListener;
import com.ibm.commerce.qa.wte.framework.junit.WteConfigLifeCycleListener;
import com.ibm.commerce.qa.wte.framework.junit.WteTestRule;
import com.ibm.commerce.qa.wte.framework.test.WebSession;
import com.ibm.commerce.qa.wte.framework.util.WteConfigManager;

public class WteTestRuleImpl extends DefaultJUnitLifeCycleRule implements WteTestRule
{
	private final WebSessionLifeCycleListener fSessionListener;
	
	private final WteConfigLifeCycleListener fConfigListener;
	
	@Inject
	public WteTestRuleImpl(Logger pLog, WebSessionLifeCycleListener pListener, WteConfigLifeCycleListener pConfigListener)
	{
		super(pLog);
		
		fSessionListener = pListener;
		fConfigListener = pConfigListener;
		
		addListener(fSessionListener);
		addListener(fConfigListener);
	}

	@Override
	public WebSession getSession()
	{
		return fSessionListener.getSession();
	}

	@Override
	public WteConfigManager getConfig()
	{
		return fConfigListener.getConfig();
	}
}
