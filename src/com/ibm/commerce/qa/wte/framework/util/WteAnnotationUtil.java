/**
 * 
 */
package com.ibm.commerce.qa.wte.framework.util;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2015
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.ibm.commerce.qa.wte.framework.page.PageObject;
import com.ibm.commerce.qa.wte.framework.page.PageObject.PageType;

/**
 * Utility class for working with Annotations in the Wte framework
 */
public abstract class WteAnnotationUtil {
	private WteAnnotationUtil() {
		// empty
	}
	
	private static PageObject getPageObject(final Class<?> p_clazz) {
		Preconditions.checkArgument(p_clazz.isAnnotationPresent(PageObject.class), 
				"@PageObject not present on type, %s", p_clazz);
		
		return p_clazz.getAnnotation(PageObject.class);
	}
	
	/**
	 * Checks if a {@link PageType} in a {@link PageObject} is present on a 
	 * type. 
	 * @param p_clazz Type to check
	 * @param p_checkType PageType to verify against
	 * @return {@code true} if the type has the {@code p_checkType} type
	 */
	public static boolean isPageObjectType(final Class<?> p_clazz, 
			final PageType p_checkType) {
		return isPageObjectTypeOneOf(p_clazz, EnumSet.of(p_checkType));
	}
	
	/**
	 * Checks if a {@link PageObject} is one of an {@link EnumSet} of types
	 * present on a type. 
	 * @param p_clazz Type to check
	 * @param p_checkTypes Valid types
	 * @return {@code true} if the type is valid
	 */
	public static boolean isPageObjectTypeOneOf(final Class<?> p_clazz, 
			final EnumSet<PageType> p_checkTypes) {
		Preconditions.checkNotNull(p_clazz, "clazz == null");
		Preconditions.checkNotNull(p_checkTypes, "checkTypes == null");
		
		boolean result = false;
		if (p_clazz.isAnnotationPresent(PageObject.class)) {
			PageObject po = getPageObject(p_clazz);
			
			result = (p_checkTypes.contains(po.type()));
		}
		
		if (!result && !p_clazz.isInterface() && 
				p_clazz.getSuperclass() != Object.class) {
			result = isPageObjectTypeOneOf(p_clazz.getSuperclass(), 
					p_checkTypes);
		}
		
		if (!result) {
			Class<?>[] interfaces = p_clazz.getInterfaces();
			for (Class<?> i : interfaces) {
				result = isPageObjectTypeOneOf(i, p_checkTypes);
				if (result) {
					break;
				}
			}
		}
		
		return result;
	}
	
	/**
	 * Recursively climbs a class hierarchy tree to search for {@link Field} 
	 * instances which are annotated with one of a set of annotations. 
	 * @param p_clazz 
	 * 			Type to check
	 * @param validTypes
	 * 			Valid {@link PageType}s
	 * @param annotations
	 * 			Valid annotations to search for, none are considered mandatory. 
	 * @param validCheck
	 * 			Pseudo-closure which will be called against every 
	 * 			Field/Annotation pair to confirm it is valid.
	 * @return {@link ImmutableMap} instance which maps {@link Annotation}s to 
	 * 		{@link Field} instances that had them marked
	 */
	public static Map<Class<? extends Annotation>, Field> getAnnotatedFields(
			final Class<?> p_clazz, EnumSet<PageType> validTypes,
			Set<Class<? extends Annotation>> annotations,
			IsValidAnnotated<Field> validCheck) {
		Map<Class<? extends Annotation>, Field> out = 
				Maps.newHashMapWithExpectedSize(annotations.size());
		
		for (Class<?> clazz = p_clazz; clazz != Object.class; clazz = clazz.getSuperclass()) {
			if (!WteAnnotationUtil.isPageObjectTypeOneOf(clazz, validTypes)) {
				continue;
			}
			
			// Check all the fields for annotations
			for (Field f : clazz.getDeclaredFields()) {
				for (Annotation a : f.getAnnotations()) {
					Class<? extends Annotation> annClazz = a.annotationType();
					if (!annotations.contains(annClazz)) {
						continue ;
					}
					
					validCheck.check(annClazz, f);
					
					if (out.containsKey(annClazz)) {
						throw new IllegalArgumentException(String.format(
								"Class<%s> has multiple instances of %s", 
								p_clazz.getSimpleName(), 
								annClazz.getSimpleName()));
					}
					
					out.put(annClazz, f);
				}
			}
		}
		
		return ImmutableMap.copyOf(out);
	}
	
	/**
	 * Recursively climbs a class hierarchy tree to search for {@link Method}s 
	 * instances which are annotated with one of a set of annotations. 
	 * 
	 * <p/>
	 * <strong>Note:</strong> There are no default method restrictions, if they
	 * are required, they must be added into the {@code validCheck} instance.
	 * 
	 * @param p_clazz 
	 * 			Type to check
	 * @param validTypes
	 * 			Valid {@link PageType}s
	 * @param annotations
	 * 			Valid annotations to search for, none are considered mandatory. 
	 * @param validCheck
	 * 			Pseudo-closure which will be called against every 
	 * 			Method/Annotation pair to confirm it is valid.
	 * @return {@link ImmutableMap} instance which maps {@link Annotation}s to 
	 * 		{@link Field} instances that had them marked
	 */
	public static Map<Class<? extends Annotation>, Method> getAnnotatedMethods(
			final Class<?> p_clazz, EnumSet<PageType> validTypes,
			Set<Class<? extends Annotation>> annotations,
			IsValidAnnotated<Method> validCheck) {
		Map<Class<? extends Annotation>, Method> out = Maps.newHashMapWithExpectedSize(annotations.size());
		
		for (Class<?> clazz = p_clazz; clazz != Object.class; clazz = clazz.getSuperclass()) {
			if (!WteAnnotationUtil.isPageObjectTypeOneOf(clazz, validTypes)) {
				continue;
			}
			
			// Check all the fields for annotations
			for (Method m : clazz.getDeclaredMethods()) {
				for (Annotation a : m.getAnnotations()) {
					Class<? extends Annotation> annClazz = a.annotationType();
					if (!annotations.contains(annClazz)) {
						continue ;
					}
					
					validCheck.check(annClazz, m);
					
					if (out.containsKey(annClazz)) {
						throw new IllegalArgumentException(String.format(
								"Class<%s> has multiple instances of %s", 
								p_clazz.getSimpleName(), 
								annClazz.getSimpleName()));
					}
					
					out.put(annClazz, m);
				}
			}
		}
		
		return ImmutableMap.copyOf(out);
	}
	
	/**
	 * Functional interface used to check a {@link Member}/{@link Annotation} 
	 * pair. 
	 *
	 * @param <T> Type of {@link Member} to check
	 */
	public interface IsValidAnnotated<T extends Member> {
		
		/**
		 * Function pointer for what to check against an annotation which is 
		 * tagged against a member. A valid member is any which does not throw
		 * a Runtime Exception. 
		 * 
		 * @param annotation Annotation to differentiate the marker
		 * @param member Member to check
		 */
		public void check(Class<? extends Annotation> annotation, final T member);
	}
	
	
	/**
	 * Get a list of fields annotated by annotation
	 * @param clazz
	 * @param an
	 * @return list of fields
	 */
	public static List<Field> getFieldsByAnnotation(Class<?> clazz,  Class<? extends Annotation> annotation) {
		List<Field> fields = Lists.newArrayList();
		for (Field f : clazz.getDeclaredFields()) {
			if (f.isAnnotationPresent(annotation)) {
				fields.add(f);
			}
		}
		
		return fields;
	}
	
}
