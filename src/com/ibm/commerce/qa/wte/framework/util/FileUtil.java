package com.ibm.commerce.qa.wte.framework.util;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.io.Closeable;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;
/**
 * utility class to help deal with file i/o
 */
public class FileUtil
{
	private static final Logger log = Logger.getLogger(FileUtil.class.getPackage().getName());;

	/**
	 * Unconditionally close a <code>Closeable</code>. Similar in concept to
	 * {@link IOUtils#closeQuietly(Closeable)} except this version also logs the
	 * exception if one occurs while closing.
	 * 
	 * @param closeable the object to close
	 */
	public static void closeQuietly(Closeable closeable)
	{
		try
		{
			if (closeable != null)
			{
				closeable.close();
			}
		}
		catch (IOException e)
		{
			log.log(Level.WARNING, "An exception occurred while trying to close an object.", e);
		}
	}
}
