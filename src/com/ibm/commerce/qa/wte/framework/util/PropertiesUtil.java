package com.ibm.commerce.qa.wte.framework.util;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
/**
 * Utility that handles property files
 *
 */
public class PropertiesUtil
{
	/**
	 * Load properties from a file.
	 * 
	 * @param propertiesPath
	 *            The full path (including filename) of the logging
	 *            configuration properties file or null if everything should be
	 *            logged to stdout.
	 * @return Proeprties class
	 */
	public static Properties load(File propertiesPath)
	{
		if (propertiesPath == null)
		{
			throw new IllegalArgumentException("The properties path must be specified.");
		}

		Properties result = new Properties();

		InputStream is = null;
		try
		{
			is = new FileInputStream(propertiesPath);
			
			result.load(is);

			trimPropertyValues(result);
		}
		catch (Exception e)
		{
			throw new IllegalArgumentException("Exception while trying to load properties from file '" + propertiesPath.getAbsolutePath() + "'.", e);
		}
		finally
		{
			FileUtil.closeQuietly(is);
		}
		
		return result;
	}

	/**
	 * Load properties from a URL.
	 * 
	 * @param propertiesUrl
	 * @return Proeprties class
	 */
	public static Properties load(URL propertiesUrl)
	{
		Properties result = new Properties();
		
		// load properties file
		InputStream is = null;
		try
		{
			is = propertiesUrl.openStream();
			
			result.load(is);
			
			trimPropertyValues(result);
		}
		catch (IOException e)
		{
			throw new IllegalArgumentException("There was an issue opening or loading the stream from the URL '" + propertiesUrl.toExternalForm() + "'", e);
		}
		finally
		{
			FileUtil.closeQuietly(is);
		}

		return result;
	}

	/**
	 * Loads properties from the specified resource.
	 * 
	 * @param propertiesResource
	 *            a slash-separated package and class name in the class path
	 * @return the properties loaded from the resource
	 * @throws IllegalArgumentException
	 *             if the resource doesn't exist
	 */
	public static Properties load(String propertiesResource)
	{
		URL url = ClassLoader.getSystemClassLoader().getResource(propertiesResource);
		
		if (url == null)
		{
			throw new IllegalArgumentException("The resource name '" + propertiesResource + "' could not be loaded as a resource.");
		}
		
		return load(url);
	}
	
	/**
	 * Iterates through all property values and trims any leading or trailing
	 * whitespace.
	 * 
	 * @param props the properties object
	 */
	public static void trimPropertyValues(Properties props)
	{
		/*
		 * As we have encountered this issue before, all values will be trimmed
		 * to ensure no trailing spaces are included in the value, which can
		 * have undesirable effects.
		 */ 
		for (Object keyObj : props.keySet())
		{
			String key = (String)keyObj;
			
			props.setProperty(key, props.getProperty(key).trim());
		}
	}

}
