package com.ibm.commerce.qa.wte.framework.util;


/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

/**
 * Executes a shell process that is expected to terminate immediately after
 * execution. The error output stream is redirected to the standard output
 * stream. The exit code and output is retained for inspection.
 * 
 * 
 */
public class ProcessExecutor
{
	/**
     * The internal copyright field.
     */
	public static final String COPYRIGHT =IBMCopyright.SHORT_COPYRIGHT;
	
	private static Logger log = Logger.getLogger(ProcessExecutor.class.getName());

	private static final String NL = System.getProperty("line.separator");

	private final List<String> commandList;

	private final ProcessBuilder pb;

	private String execOutput;

	private int exitStatus;

	/**
	 * @param command
	 */
	public ProcessExecutor(List<String> command)
	{
		this.commandList = new ArrayList<String>(command);

		this.pb = new ProcessBuilder(this.commandList);
		this.pb.redirectErrorStream(true);
	}
	
	/**
	 * @param command
	 */
	public ProcessExecutor(String... command)
	{
			this.commandList = Arrays.asList(command);

		this.pb = new ProcessBuilder(commandList);
		this.pb.redirectErrorStream(true);
	}
	
	/**
	 * 
	 * @param workingDirectory
	 */
	public void setWorkingDirectory(File workingDirectory)
	{
		this.pb.directory(workingDirectory);
	}
	
	/**
	 * 
	 * @param key
	 * @param value
	 */
	public void addEnvironmentProperty(String key, String value)
	{
		this.pb.environment().put(key, value);
	}

	/**
	 * This command will block until the process terminates.
	 * <p>
	 * All input from the process will have a line separator after it that
	 * conforms to the platform it is executed on.
	 */
	public void execute()
	{
		log.fine("Execute command: " + this.commandList);

		// Start the process
		Process proc = null;
		try
		{
			proc = pb.start();
		}
		catch (IOException e)
		{
			throw new RuntimeException("Failure to start process.", e);
		}

		// Grab and process the stdin/stderr stream
		log.finer("Process stdin/stderr.");
		try
		{
			this.execOutput = processInputStream(proc.getInputStream());
		}
		catch (IOException e)
		{
			throw new RuntimeException(
					"Exception while reading the output from the process.", e);
		}

		// Wait for process to exit and get return code
		log.fine("Wait for process to exit.");
		try
		{
			this.exitStatus = proc.waitFor();
		}
		catch (InterruptedException e)
		{
			throw new RuntimeException("Process interrupted.", e);
		}
	}

	private String processInputStream(InputStream is) throws IOException
	{
		StringBuilder sb = new StringBuilder();

		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader reader = new BufferedReader(isr);

		String line = null;
		while ((line = reader.readLine()) != null)
		{
			sb.append(line + NL);
		}

		return sb.toString();
	}

	/**
	 * 
	 * @return the text returned from executing a process or an empty string if
	 *         no output
	 */
	public String getExecOutput()
	{
		return this.execOutput;
	}
	/**
	 * 
	 * @return exit status
	 */
	public int getExitStatus()
	{
		return this.exitStatus;
	}
}

