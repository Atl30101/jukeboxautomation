package com.ibm.commerce.qa.wte.framework.util;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2013
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.List;
import java.util.Map;

import com.google.common.base.Optional;
import com.google.inject.Module;
import com.ibm.commerce.qa.wte.framework.page.BrowserType;
import com.ibm.commerce.qa.wte.framework.page.ViewPortType;

/**
 *  Interface that included interaction with the config.properties file that includes all the user set parameters that affect automation
 * 
 *
 */
public interface ConfigManager
{
	/**
	 * Returns the string value of any property. This should only be used for
	 * custom properties that are not retrievable from the set of get methods
	 * already in this class. This method will return <code>null</code> if the
	 * property is not found.
	 * <p>
	 * Note: It is recommended to only use this method within subclasses of
	 * {@link ConfigManager}. Consider adding new methods to subclasses to
	 * retrieve custom properties rather than calling this method externally.
	 * 
	 * @param propertyName
	 *            the non-null property name
	 * @return the value or <code>null</code> if not found
	 * @throws NullPointerException
	 *             if the property name was null
	 */
	public String getCustomProperty(String propertyName);

	/**
	 * Returns the string value of any property. This should only be used for
	 * custom properties that are not retrievable from the set of get methods
	 * already in this class. This method will throw an exception if the
	 * property could not be found.
	 * <p>
	 * Note: It is recommended to only use this method within subclasses of
	 * {@link ConfigManager}. Consider adding new methods to subclasses to
	 * retrieve custom properties rather than calling this method externally.
	 * 
	 * @param propertyName
	 *            the non-null property name
	 * @return the value
	 * @throws NullPointerException
	 *             if the property name was null
	 * @throws IllegalArgumentException
	 *             if the property name cannot be found
	 */
	public String loadCustomProperty(String propertyName);

	/**
	 * @return the proxy host name or <code>null</code> if none specified
	 */
	public String getProxyHostName();

	/**
	 * @return the proxy port number
	 */
	public int getProxyPort();

	/**
	 * @return the implicitTimeoutSeconds
	 */
	public long getImplicitTimeoutSeconds();

	/**
	 * @return the pageLoadTimeoutSeconds
	 */
	public long getPageLoadTimeoutSeconds();

	/**
	 * @return the ajaxTimeoutSeconds
	 */
	public long getAjaxTimeoutSeconds();

	/**
	 * @return the scriptTimeoutSeconds
	 */
	public long getScriptTimeoutSeconds();

	/**
	 * @return the browserType
	 */
	public BrowserType getBrowserType();

	/**
	 * @return the ieDriverServerPath
	 */
	public String getIeDriverServerPath();
	
	/**
	 * @return the chromeDriverServerPath
	 */
	public String getChromeDriverServerPath();

	/**
	 * @return a boolean indicating whether native events should be used with
	 *         Selenium 2 WebDriver
	 */
	public boolean getUseNativeEvents();

	/**
	 * The requested state of the initial browser window. One of NORMAL or
	 * MAXIMIZE.
	 * 
	 * @return the window state or <code>null</code> if not set
	 */
	public WindowState getWindowState();

	/**
	 * The requested width in pixels of the initial browser.
	 * 
	 * @return the width or <code>null</code> if not set
	 */
	public Integer getWindowWidth();
	
	/**
	 * The requested height in pixels of the initial browser.
	 * 
	 * @return the height or <code>null</code> if not set
	 */
	public Integer getWindowHeight();
	
	/**
	 * The requested top position in pixels of the initial browser.
	 * 
	 * @return the top position (y-coordinate) or <code>null</code> if not set
	 */
	public Integer getWindowTopPosition();
	
	/**
	 * The requested left position in pixels of the initial browser.
	 * 
	 * @return the left position (x-coordinate) or <code>null</code> if not set
	 */
	public Integer getWindowLeftPosition();

	/**
	 * The page object module that describes page object bindings. This is most
	 * useful when page objects are subclassed, so a mapping can be created
	 * between the superclass and the subclass that needs to be created in its
	 * place. Multiple may be specified in a comma separated list.
	 * 
	 * @return the non-null page module classes
	 */
	public List<Class<? extends Module>> getPageObjectModuleClasses(); 
	
	/**
	 * The requested left position in pixels of the mouse when the browser
	 * window is displayed.
	 * 
	 * @return the left position of the mouse or <code>null</code> if not set
	 */
	public Integer getMouseRelocateLeftPosition();
	
	/**
	 * The requested top position in pixels of the mouse when the browser
	 * window is displayed.
	 * 
	 * @return the top position of the mouse or <code>null</code> if not set
	 */
	public Integer getMouseRelocateTopPosition();
	/**
	 * returns the click delay
	 * @return click delay
	 */
	public long getClickDelayMilliseconds();
	/**
	 * get boolean to determine whether to setup logging or not. Default: True
	 * @return log setup flag
	 */
	public boolean getLogSetupBoolean();
	/**
	 * get boolean to determine whether to capture html source on every page
	 * @return boolean
	 */
	public boolean isStaticHtmlCapture();
	/**
	 * returns save location of HTML source page capture from config.properties
	 * @return directory path
	 */
	public String getStaticHtmlDir();
	/**
	 * returns save location of page captures from config.properties
	 * @return directory path
	 */
	public String getScreenShotPath();
	/**
	 * returns boolean that determines whether to take a screen shot on failed test cases
	 * @return boolean
	 */
	public boolean isScreenShotOnFail();
	/**
	 * returns boolean that determines whether to take a screen shot on every page
	 * @return boolean
	 */
	public boolean isScreenShotOnStep();

	/**
	 * Returns the host name of the remote machine that will run the browser.
	 * 
	 * @return the host name or <code>null</code> if the browser is to run
	 *         locally
	 */
	public String getRemoteBrowserHostName();
	
	/**
	 * Returns the port of the remote machine that will run the browser.
	 * 
	 * @return the port number
	 */
	public int getRemoteBrowserPort();

	/**
	 * Returns a key/value map of extra capabilities to be sent to the Selenium
	 * 2 WebDriver.
	 * 
	 * @return the non-null capabilities map
	 */
	public Map<String, String> getSelenium2WebDriverCapabilities();
	
	/**
	 * returns the view name, based on the browser window width
	 * @param pageWidth
	 * @return view port name
	 * @deprecated use {@link #getViewPort(int)} instead as this should not have used a hard-coded enum
	 */
	@Deprecated
	public ViewPortType getViewPortName(Integer pageWidth);
	
	/**
	 * Returns the upper-case view name associated with the page width
	 * specified. Case is ignored for view ports.
	 * 
	 * @param pageWidth
	 *            the page width
	 * @return the name associated to that width if one is found
	 */
	public Optional<String> getViewPort(int pageWidth);
	
	/**
	 * returns the user agent. Firefox only
	 * @return user agent string
	 */
	public String getUserAgent();
}