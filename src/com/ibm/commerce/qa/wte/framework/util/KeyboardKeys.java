package com.ibm.commerce.qa.wte.framework.util;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

/**
 * Used to place invisible characters in a string.
 * 
 */
public enum KeyboardKeys
{
	/**
	 * The BACKSPACE key.
	 */
	BACKSPACE('\u0008'),
	
	/**
	 * The ENTER key.
	 */
	ENTER('\ue007'),
	
	/**
	 * The TAB key.
	 */
	TAB('\u0009'),
	
	/**
	 * The SHIFT key.
	 */
	SHIFT('\uE008');

	private final char keyRepresentation;

	KeyboardKeys(char keyRepresentation)
	{
		this.keyRepresentation = keyRepresentation;
	}

	public String toString()
	{
		return String.valueOf(keyRepresentation);
	}

}
