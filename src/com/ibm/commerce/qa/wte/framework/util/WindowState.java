package com.ibm.commerce.qa.wte.framework.util;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */
/**
 * enum that handles window states
 */
public enum WindowState
{
	/**
	 * normal, non maximized state
	 */
	NORMAL,
	/**
	 * Maximized window
	 */
	MAXIMIZE;
}
