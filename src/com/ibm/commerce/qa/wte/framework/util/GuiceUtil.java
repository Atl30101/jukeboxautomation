package com.ibm.commerce.qa.wte.framework.util;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */
/**
 * Utility class for Guice
 */
public class GuiceUtil
{
	/**
	 * Removes the Google Guice proxy component from the class name.
	 * <p>
	 * Ex:
	 * <code>com.ibm.commerce.qa.aurora.page.FooterSection$$EnhancerByGuice$$a08a0d4f</code>
	 * 
	 * @param className
	 *            a non-null class name that possibly is a Google Guice proxy
	 * @return the same class name with the proxy component removed
	 */
	public static String removeProxyFromClassName(final String className)
	{
		String result = null;

		if (className.contains("$$"))
		{
			result = className.split("\\$\\$")[0];
		}
		else
		{
			result = className;
		}

		return result;
	}

	/**
	 * Removes the Google Guice proxy class if it exists by getting the
	 * superclass instead.
	 * <p>
	 * Ex:
	 * <code>com.ibm.commerce.qa.aurora.page.FooterSection$$EnhancerByGuice$$a08a0d4f</code>
	 * 
	 * @param <T>
	 *            the type of the class
	 * @param clazz
	 *            the class that possibly is a Google Guice proxy
	 * @return the same class if it wasn't a Google Guice proxy, else the
	 *         superclass
	 */
	public static <T> Class<? super T> removeProxyFromClass(final Class<T> clazz)
	{
		Class<? super T> result = null;

		if (clazz.getName().contains("$$"))
		{
			result = clazz.getSuperclass();
		}
		else
		{
			result = clazz;
		}

		return result;
	}
}
