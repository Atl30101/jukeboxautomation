package com.ibm.commerce.qa.wte.framework.util;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2015
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.io.File;
import java.util.Properties;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.matcher.Matchers;
import com.google.inject.name.Names;
import com.ibm.commerce.qa.util.dataset.DataSetModule;
import com.ibm.commerce.qa.util.props.PropertiesUtil;
import com.ibm.commerce.qa.wte.framework.junit.WebSessionLifeCycleListener;
import com.ibm.commerce.qa.wte.framework.junit.WteConfigLifeCycleListener;
import com.ibm.commerce.qa.wte.framework.junit.WteTestRule;
import com.ibm.commerce.qa.wte.framework.junit.impl.WebSessionLifeCycleListenerImpl;
import com.ibm.commerce.qa.wte.framework.junit.impl.WteConfigLifeCycleListenerImpl;
import com.ibm.commerce.qa.wte.framework.junit.impl.WteTestRuleImpl;
import com.ibm.commerce.qa.wte.s2impl.util.Selenium2PageObjectRelativeResolver;
import com.ibm.commerce.qa.wte.s2impl.util.Selenium2PageValidatorPerformer;

/**
 * The wiring class to automatically create WTE objects and perform AOP.
 * 
 */
public class WteBaseModule extends AbstractModule
{
	@Override
	protected void configure()
	{
		// Bind the configuration properties
		Names.bindProperties(binder(), loadConfigProperties());

		// Bind and initialize the log manager as a singleton
		bind(LoggingConfig.class).asEagerSingleton();
		
		// Bind the configuration property manager as a singleton
		bind(ConfigManager.class).to(WteConfigManager.class).in(Singleton.class);
		
		// Add automatic entry/exit method logging
		LoggingInterceptor ti = new LoggingInterceptor();
		requestInjection(ti);
		bindInterceptor(new LoggingInterceptor.LoggingMatcher(), Matchers.any(), ti);
		bindInterceptor(Matchers.any(), new LoggingInterceptor.LoggingMatcher(), ti);
		
		bind(WebSessionLifeCycleListener.class).to(WebSessionLifeCycleListenerImpl.class).in(Singleton.class);
		bind(WteConfigLifeCycleListener.class).to(WteConfigLifeCycleListenerImpl.class).in(Singleton.class);
		bind(WteTestRule.class).to(WteTestRuleImpl.class).in(Singleton.class);
		
		bind(PageValidatorPerformer.class).to(Selenium2PageValidatorPerformer.class).in(Singleton.class);
		bind(PageObjectRelativeResolver.class).to(Selenium2PageObjectRelativeResolver.class).in(Singleton.class);
	
		// use the DataSetModule for data provider in FVTUtils
		install(new DataSetModule());
	}

	/**
	 * Attempt to load the configuration file. Note that this must be the same 
	 * file as any other installed module's loadConfigProperties, otherwise 
	 * Guice will complain that properties were already configured.
	 * 
	 * @return the non-null properties from the config file
	 */
	private Properties loadConfigProperties()
	{
		String configFileName = System.getProperty("test.config.file", "config.properties");
		
		File configFile = new File(configFileName);
		
		return PropertiesUtil.load(configFile);
	}
}
