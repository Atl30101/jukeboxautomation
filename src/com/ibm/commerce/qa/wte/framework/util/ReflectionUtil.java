package com.ibm.commerce.qa.wte.framework.util;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import com.google.common.base.Preconditions;

/**
 * Utility classes dealing with Java reflection.
 * 
 * 
 *
 */
public class ReflectionUtil
{
	/**
	 * Iterate through the class specified and all its super classes, looking
	 * for the annotation specified on a declared field. Once a field is
	 * located, the search stops and that field is returned.
	 * 
	 * @param clazz
	 *            the class to search for the annotation
	 * @param annotationClass
	 *            the annotation on the declared field to search for
	 * @return the field object or <code>null</code> if none found
	 */
	public static Field getFieldByAnnotation(Class<?> clazz, Class<? extends Annotation> annotationClass)
	{
		Field result = null;
		
		ClassLoop:
		for (Class<?> c = clazz; c != Object.class && c != null; 
				c = c.getSuperclass())
        {
			for (Field field : c.getDeclaredFields())
			{
				if (field.isAnnotationPresent(annotationClass))
				{
					result = field;
					break ClassLoop;
				}
			}
			
			if (c.getInterfaces() != null && c.getInterfaces().length > 0) {
				for (Class<?> i : c.getInterfaces()) {
					result = getFieldByAnnotation(i, annotationClass);
					if (result != null) {
						break ClassLoop;
					}
				}
			}
        }
		
		return result;
	}
	/**
	 * returns a defined field, based on a class specified
	 * @param clazz the class to search for a field
	 * @param fieldClass the class type of the field
	 * @return the field, if found, null otherwise
	 */
	public static Field getFieldByClass(Class<?> clazz, Class<?> fieldClass)
	{
		Field result = null;
		
		ClassLoop:
		for (Class<?> c = clazz; c != Object.class; c = c.getSuperclass())
        {
			for (Field field : c.getDeclaredFields())
			{
			
				if (field.getType().isAssignableFrom(fieldClass))
				{
					field.setAccessible(true);
					result = field;
					
					break ClassLoop;
				}
			}
        }
		
		return result;
	}
	
	/**
	 * Injects an instance with a value stored at the field. This is merely a
	 * shortcut method. This will try to set 
	 * {@link Field#setAccessible(boolean)} if required. 
	 * 
	 * <p/>
	 * All errors are thrown as {@link RuntimeException}. 
	 * @param p_injectee Instance to inject into, may be {@code null}
	 * @param p_field Field instance
	 * @param p_value Value of the field
	 */
	public static void injectField(Object p_injectee, 
			Field p_field, Object p_value) {
		try {
			int mod = p_field.getModifiers();
			if (!Modifier.isPublic(mod)) {
				p_field.setAccessible(true);
			}
			
			p_field.set(p_injectee, p_value);
		} catch (IllegalAccessException iae) {
			throw new RuntimeException(iae);
		}
	}
	
	/**
	 * Injects an instance method with a single parameter as the value. 
	 * This is merely a shortcut method. This will try to set 
	 * {@link Method#setAccessible(boolean)} if required. 
	 * 
	 * <p/>
	 * All errors are thrown as {@link RuntimeException}. 
	 * @param p_injectee Instance to inject into, may be {@code null}
	 * @param p_method Single parameter method to call
	 * @param p_value Value to use as parameter
	 */
	public static void injectMethod(Object p_injectee,
			Method p_method, Object p_value) {
		try {
			int mod = p_method.getModifiers();
			if (!Modifier.isPublic(mod)) {
				p_method.setAccessible(true);
			}
			
			p_method.invoke(p_injectee, p_value);
		} catch (IllegalAccessException iae) {
			throw new RuntimeException(iae);
		} catch (InvocationTargetException ite) {
			throw new RuntimeException(ite);
		}
	}
	
	/**
	 * Figures out whether {@link Member} is Field or Method, and calls the
	 * appropriate static method in {@link ReflectionUtil}. 
	 * @param p_injectee Instance to inject into, may be {@code null}
	 * @param p_member Single parameter method to call, or non-final field
	 * @param p_value Value to use as parameter
	 */
	public static void injectMember(Object p_injectee,
			Member p_member, Object p_value) {
		Class<?> memClass = p_member.getClass();
		Preconditions.checkArgument(memClass == Method.class ||
				memClass == Field.class);
		
		if (memClass == Field.class) {
			injectField(p_injectee, (Field)p_member, p_value);
		} else {
			injectMethod(p_injectee, (Method)p_member, p_value);
		}
	}
}
