package com.ibm.commerce.qa.wte.framework.util;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.junit.Test;

import com.google.inject.Inject;
import com.google.inject.matcher.AbstractMatcher;
import com.ibm.commerce.qa.wte.framework.page.PageObject;
import com.ibm.commerce.qa.wte.framework.test.WebSession;

/**
 * Logs a message about entering and then exiting the method.
 * 
 * 
 * 
 */
public class LoggingInterceptor implements MethodInterceptor
{
	/**
	 * Guice mandated class that matched the methods with the specified annotations.
	 * Only those methods will be intercepted
	 *
	 */
	public static class LoggingMatcher extends AbstractMatcher<Object>
	{
		public boolean matches(Object type)
		{
			boolean result = false;
			
			if (type instanceof Class<?>)
			{
				if ( ((Class<?>)type).isAnnotationPresent(PageObject.class) 
						/*|| ((Class<?>)type).isAnnotationPresent(ChildPage.class)*/)
				{
					result = true;
				}
			}
			else if (type instanceof Method)
			{
				if ( ((Method)type).isAnnotationPresent(Test.class) )
				{
					result = true;
				}
			}

			return result;
		}
	}

	@Inject
	private Logger log;
	

	/**
	 * A log message is written stating that the thread is entering the method
	 * specified. The method is then executed. Finally, a log entry is written
	 * stating the method has exited.
	 */
	@Override
	public Object invoke(MethodInvocation mi) throws Throwable
	{
		
		// Remove guice proxy portion from class to be logged
		String className = GuiceUtil.removeProxyFromClassName(mi.getThis()
				.getClass().getName());

		if (!mi.getMethod().getDeclaringClass().equals(Object.class))
		{
			log.logp(Level.INFO, className, mi.getMethod().getName(),
					"Entering method... " + className+ "." + mi.getMethod().getName());
		}
		
		Object result = null;
		Long startTime = 0L;

		try
		{
			startTime = System.currentTimeMillis();
			result = mi.proceed();
		}
		catch (RuntimeException e)
		{	
			Field sessionField = ReflectionUtil.getFieldByClass(mi.getThis().getClass(), WebSession.class);
			WebSession session;
			 if(sessionField != null){
				 try {
					session = (WebSession)sessionField.get(mi.getThis());
					session.takeScreenshotOnFail(mi.getMethod().getName());
				} catch (Exception e1) {
					log.log(Level.WARNING, "Attempt to take a screen shot failed.", e);
				}
			 }
			throw e;
		}
		finally
		{
			if (!mi.getMethod().getDeclaringClass().equals(Object.class))
			{
				Long durationMs = System.currentTimeMillis() - startTime;
				
				log.logp(Level.INFO, className, mi.getMethod().getName(),
						className+ "." + mi.getMethod().getName() + " ...leaving method (" + durationMs + "ms).");
			}
		}
		return result;
	}

}
