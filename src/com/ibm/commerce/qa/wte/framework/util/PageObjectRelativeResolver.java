package com.ibm.commerce.qa.wte.framework.util;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2015
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import com.google.inject.Injector;
import com.ibm.commerce.qa.wte.framework.page.PageFactory;

/**
 * This helper class resolve the relative page object from the current page object to its children and its descendants
 */
public interface PageObjectRelativeResolver {
	
	/**
	 * Resolve the relative page object by setting the relative page factory properly starting from current page object,
	 * and all the way down to its descendants.
	 * @param pageSection
	 * @param parentFactory
	 * @param index
	 * @param injector
	 * @return the resolved page object
	 */
	public <T> T resolvePageObject(T pageSection, PageFactory parentFactory, int index, Injector injector);
	
	/**
	 * Get the {@PageFactory} injected into the specified page Object
	 * @param pageObject
	 * @return the page factory from the page object
	 */
	public PageFactory getPageFactory(Object pageObject);
	
}
