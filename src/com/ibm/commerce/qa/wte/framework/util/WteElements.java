/**
 * 
 */
package com.ibm.commerce.qa.wte.framework.util;

import static com.google.common.base.Preconditions.checkNotNull;

import com.ibm.commerce.qa.wte.framework.page.WteElement;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Predicate;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2014
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

/**
 * Utility methods for {@link WteElement}
 * 
 * @since Apr 15, 2014
 *
 */
public abstract class WteElements {

	private WteElements() {
		// don't instantiate
	}
	
	/**
	 * Returns a Guava {@link Function} for getting an HTML element's Attribute
	 * from within a WteElement
	 * @param attrName Name of the attribute, e.g. 'id'
	 * @return Non-null Function instance
	 */
	public static Function<WteElement, String> getAttributesFunc(final String attrName){
		return new Function<WteElement, String>(){
			@Override
			public String apply(WteElement e){
				return e.getAttributeValue(attrName);
			}
			
			@Override
			public String toString() {
				return Objects.toStringHelper("WteElements.getAttributeFunc")
						.add("attrName", attrName)
						.toString();
			}
		};
	} 
	
	private final static Function<WteElement, String> RAW_TEXT_FUNCT = 
					new Function<WteElement, String>() {
				@Override
				public String apply(WteElement p_elem) {
					checkNotNull(p_elem, "elem == null");
					return p_elem.getRawText();
				}
				
				@Override
				public String toString() {
					return Objects.toStringHelper("WteElements.getRawTextFunc")
							.toString();
				}
			
			};
	
	/**
	 * Calls {@link WteElement#getRawText()} on a passed {@link WteElement}. 
	 * This method is useful in collection transformations. 
	 * @return static Function instance
	 */
	public static Function<WteElement, String> getRawTextFunc() {
		return RAW_TEXT_FUNCT;
	}
		
	/**
	 * Calls {@link #getText()} on a passed {@link WteElement}. This method
	 * is useful in collection transformations. 
	 */	
	private final static Function<WteElement, String> TEXT_FUNCT = 
			new Function<WteElement, String>() {
				@Override
				public String apply(WteElement p_elem) {
					checkNotNull(p_elem, "elem == null");
					return p_elem.getText();
				}
				
				@Override
				public String toString() {
					return Objects.toStringHelper("WteElements.getTextFunc")
							.toString();
				}
			
		};
	
	/**
	 * Calls {@link WteElement#getText()} on a passed {@link WteElement}. This
	 * method is useful in collection transformations. 
	 * @return static Function instance
	 */	
	public static Function<WteElement, String> getTextFunc() {
		return TEXT_FUNCT;
	} 
	
	private static final Predicate<WteElement> IS_NULL_OR_STALE_PRED = 
			new Predicate<WteElement>() {
				@Override
				public boolean apply(WteElement element) {
					return element == null || element.isStale();
				}
				
				@Override
				public String toString() {
					return Objects.toStringHelper("WteElements.isNullOrStale")
							.toString();
				}
			};
	
	/**
	 * Convenience method to check if a {@link WteElement} instance is 
	 * {@code null} or if {@link WteElement#isStale()} returns {@code true}.
	 * @return instance of {@link Predicate} (same one each time). 
	 */
	public static Predicate<WteElement> isNullOrStaleFunc() {
		return IS_NULL_OR_STALE_PRED;
	}
}
