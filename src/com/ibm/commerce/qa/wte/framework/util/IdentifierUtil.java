package com.ibm.commerce.qa.wte.framework.util;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import com.ibm.commerce.qa.wte.framework.page.Identifier;

/**
 * Utilities that are useful when working with {@link Identifier}s.
 * 
 */
public class IdentifierUtil
{
	/**
	 * Encodes characters that cannot be directly included in XML strings. For
	 * example, in the XPath statement
	 * <code>//a[contains(@id,'This & That "Text"')]</code>, the string between
	 * the single quotes is invalid XML. Instead, pass the raw string into this
	 * method first, to convert it to the XML safe string
	 * <code>This &amp;amp; That &amp;quote;Text&amp;quote;</code>. Do not try
	 * to pass the entire XPath statement, just what belongs in between the main
	 * quotes.
	 * <p>
	 * Currently, this method deals with greater than, less than, ampersand,
	 * double quote a single quote (apostrophe).
	 * 
	 * @param rawString
	 * @return the encoded XML string
	 */
	public static String encodeXmlString(String rawString)
	{
		String result = rawString;

		// It's important the '&' conversion happens first
		result = result.replace("&", "&amp;");
		
		result = result.replace("'", "&apos;");
		result = result.replace("\"", "&quote;");
		result = result.replace(">", "&gt;");
		result = result.replace("<", "&lt;");
		
		return result;
	}

	/**
	 * Returns the same string but with XML escaped characters returned to their
	 * normal state.
	 * <p>
	 * Currently, this method decodes greater than, less than, ampersand, double
	 * quote a single quote (apostrophe).
	 * 
	 * @param encodedString
	 *            the encoded XML string
	 * @return the decoded string
	 */
	public static String decodeXmlString(String encodedString)
	{
		String result = encodedString;
		
		result = result.replace("&apos;", "'");
		result = result.replace("&quote;", "\"");
		result = result.replace("&gt;", ">");
		result = result.replace("&lt;", "<");
		
		// It's important the '&' decode happens last
		result = result.replace("&amp;", "&");
		
		return result;
	}
	
//	/**
//	 * Quick sanity test.
//	 * 
//	 * @param args
//	 */
//	public static void main(String[] args)
//	{
//		final String raw = "Thi<s> & 'Tha'<t> \"Text\"";
//		
//		final String expectedEncoded = "Thi&lt;s&gt; &amp; &apos;Tha&apos;&lt;t&gt; &quote;Text&quote;";
//		
//		String encoded = encodeXmlString(raw);
//		
//		String decoded = decodeXmlString(encoded);
//		
//		System.out.println("Original: '" + raw + "'");
//		
//		System.out.println("Encoded: '" + encoded + "'");
//
//		System.out.println("Decoded: '" + decoded + "'");
//
//		if (!encoded.equals(expectedEncoded))
//		{
//			throw new IllegalStateException("The raw failed to encode properly.");
//		}
//		
//		if (!raw.equals(decoded))
//		{
//			throw new IllegalStateException("The raw and final decoded string do not match.");
//		}
//	}

	/**
	 * Produces the XPath 1.0 function <code>translate</code> that will convert
	 * the string provided to upper-case.
	 * 
	 * @param originalText
	 *            text to put in function to convert
	 * @return the function to use in your XPath expression
	 * @see <a
	 *      href="http://www.w3.org/TR/xpath/#section-String-Functions">http:/
	 *      /www.w3.org/TR/xpath/#section-String-Functions</a>
	 */
	public static String xPathTranslateToUpperCaseFunction(String originalText)
	{
		return "translate(" + originalText + ",'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')";
	}

	/**
	 * Produces the XPath 1.0 function <code>translate</code> that will convert
	 * the string provided to lower-case.
	 * 
	 * @param originalText
	 *            text to put in function to convert
	 * @return the function to use in your XPath expression
	 * @see <a
	 *      href="http://www.w3.org/TR/xpath/#section-String-Functions">http:/
	 *      /www.w3.org/TR/xpath/#section-String-Functions</a>
	 */
	public static String xPathTranslateToLowerCaseFunction(String originalText)
	{
		return "translate(" + originalText + ",'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')";
	}
}
