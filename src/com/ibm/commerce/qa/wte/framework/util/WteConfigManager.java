package com.ibm.commerce.qa.wte.framework.util;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2013
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.common.base.Optional;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.inject.AbstractModule;
import com.google.inject.ConfigurationException;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Module;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import com.ibm.commerce.qa.wte.framework.page.BrowserType;
import com.ibm.commerce.qa.wte.framework.page.ViewPortType;

/**
 * This class represent all the configuration parameters for WTE.
 * <p>
 * Generally it can be extended to create more properties for your specific use
 * of WTE.
 * 
 * 
 * 
 */
public class WteConfigManager implements ConfigManager 
{
	/**
	 * Used instead of returning <code>null</code> for the
	 * {@link ConfigManager#getPageObjectModuleClasses()} if not specified in the
	 * configuration.
	 * 
	 */
	public static final class EmptyModule extends AbstractModule
	{
		@Override
		protected void configure()
		{
			// Do nothing
		}
	}
	
	private final Injector injector;
	
	private final Logger log;
	
	private BrowserType browserType;
	
	private String proxyHostName;
	
	private int proxyPort = 8080;
	
	private String remoteBrowserHostName;
	
	private int remoteBrowserPort = 4444;
	
	private long implicitTimeoutSeconds = 0L;
	
	private long pageLoadTimeoutSeconds = 60L;
	
	private long ajaxTimeoutSeconds = 30L;
	
	private long scriptTimeoutSeconds = 10L;
	
	private long clickDelayMilliseconds = 0L;
	
	private String ieDriverServerPath = "IEDriverServer.exe";
	
	private boolean setLoggerBoolean = false;
	
	private String chromeDriverServerPath = "chromedriver.exe";

	private boolean useNativeEvents = true;
	
	private List<Class<? extends Module>> pageObjectModuleClasses = Collections.emptyList();
	
	private WindowState windowState;
	
	private Integer windowLeftPosition, windowTopPosition;
	
	private Integer windowWidth, windowHeight;

	private Integer mouseRelocateLeftPosition, mouseRelocateTopPosition;
	
	private boolean staticHtmlCapture = false;

	private String staticHtmlDir = "C:/HTMLSource/";
	
	private boolean screenShotOnStep = false;
	
	private boolean screenShotOnFail = true;

	private String screenShotPath = "c:/temp/screenshots/";
	
	private Map<String, String> selenium2WebDriverCapabilities = new HashMap<String, String>();
	
	private NavigableMap<Integer, String> viewports;

	private String userAgent;
	
	private static final String VIEWPORT_NAME_PROPERTY = "VIEWPORT.%d.NAME";
	
	private static final String VIEWPORT_MAX_PROPERTY = "VIEWPORT.%d.WIDTH.MAX";
	
	/**
	 * 
	 * @param injector
	 */
	@Inject
	protected WteConfigManager(Injector injector, Logger log)
	{
		/*
		 * The injector is needed to load custom properties that will be set by
		 * different test buckets. This will allow a generic getProperty to get
		 * any value by its property name.
		 */
		this.injector = injector;
		this.log = log;
	}

	protected Logger getLog()
	{
		return this.log;
	}
	
	@Override
	public String getCustomProperty(String propertyName)
	{
		if (propertyName == null)
		{
			throw new NullPointerException("The property name cannot be null");
		}
		
		String result = null;
		
		try
		{
			result = this.injector.getInstance(Key.get(String.class, Names.named(propertyName)));
		}
		catch (ConfigurationException e)
		{
			getLog().log(Level.FINE, "The property with name '" + propertyName + "' could not be found. This method is meant to return null if not found, so this can be ignored.");
		}
		
		return result;
	}
	
	@Override
	public String loadCustomProperty(String propertyName)
	{
		if (propertyName == null)
		{
			throw new NullPointerException("The property name cannot be null");
		}
		
		String result = null;
		
		try
		{
			result = this.injector.getInstance(Key.get(String.class, Names.named(propertyName)));
		}
		catch (ConfigurationException e)
		{
			throw new IllegalArgumentException("The property with name '" + propertyName + "' could not be found.", e);
		}
		
		return result;
	}
	
	@Override
	public long getImplicitTimeoutSeconds()
	{
		return implicitTimeoutSeconds;
	}

	/**
	 * @param implicitTimeoutSeconds the implicitTimeoutSeconds to set
	 */
	@Inject(optional=true)
	void setImplicitTimeoutSeconds(@Named("IMPLICIT_TIMEOUT_SECONDS") long implicitTimeoutSeconds)
	{
		this.implicitTimeoutSeconds = implicitTimeoutSeconds;
	}
	
	@Override
	public boolean getLogSetupBoolean()
	{
		return setLoggerBoolean;
	}

	/**
	 * @param setLoggerBoolean the setLoggerBoolean to set
	 */
	@Inject(optional=true)
	void setLogSetupBoolean(@Named("SET_LOGGER") boolean setLoggerBoolean)
	{
		this.setLoggerBoolean = setLoggerBoolean;
	}

	@Override
	public long getPageLoadTimeoutSeconds()
	{
		return pageLoadTimeoutSeconds;
	}

	/**
	 * @param pageLoadTimeoutSeconds the pageLoadTimeoutSeconds to set
	 */
	@Inject(optional=true)
	void setPageLoadTimeoutSeconds(@Named("PAGE_LOAD_TIMEOUT_SECONDS") long pageLoadTimeoutSeconds)
	{
		this.pageLoadTimeoutSeconds = pageLoadTimeoutSeconds;
	}

	@Override
	public long getAjaxTimeoutSeconds()
	{
		return ajaxTimeoutSeconds;
	}

	/**
	 * @param ajaxTimeoutSeconds the ajaxTimeoutSeconds to set
	 */
	@Inject(optional=true)
	void setAjaxTimeoutSeconds(@Named("AJAX_TIMEOUT_SECONDS") long ajaxTimeoutSeconds)
	{
		this.ajaxTimeoutSeconds = ajaxTimeoutSeconds;
	}

	@Override
	public long getScriptTimeoutSeconds()
	{
		return scriptTimeoutSeconds;
	}

	/**
	 * @param scriptTimeoutSeconds the scriptTimeoutSeconds to set
	 */
	@Inject(optional=true)
	void setScriptTimeoutSeconds(@Named("SCRIPT_TIMEOUT_SECONDS") long scriptTimeoutSeconds)
	{
		this.scriptTimeoutSeconds = scriptTimeoutSeconds;
	}

	@Override
	public BrowserType getBrowserType()
	{
		return browserType;
	}

	/**
	 * @param browserType the browserType to set
	 */
	@Inject
	void setBrowserType(@Named("BROWSER_TYPE") BrowserType browserType)
	{
		this.browserType = browserType;
	}

	@Override
	public String getIeDriverServerPath()
	{
		return ieDriverServerPath;
	}

	/**
	 * @param ieDriverServerPath the ieDriverServerPath to set
	 */
	@Inject(optional=true)
	void setIeDriverServerPath(@Named("IE_DRIVER_SERVER_PATH") String ieDriverServerPath)
	{
		this.ieDriverServerPath = ieDriverServerPath;
	}

	/**
	 * @return the chromeDriverServerPath
	 */
	@Override
	public String getChromeDriverServerPath()
	{
		return chromeDriverServerPath;
	}

	/**
	 * @param chromeDriverServerPath the chromeDriverServerPath to set
	 */
	@Inject(optional=true)
	void setChromeDriverServerPath(@Named("CHROME_DRIVER_SERVER_PATH") String chromeDriverServerPath)
	{
		this.chromeDriverServerPath = chromeDriverServerPath;
	}

	@Inject(optional=true)
	void setUseNativeEvents(@Named("USE_NATIVE_EVENTS") boolean useNativeEvents)
	{
		this.useNativeEvents = useNativeEvents;
	}
	
	@Override
	public boolean getUseNativeEvents()
	{
		return this.useNativeEvents;
	}

	@Override
	public Integer getWindowHeight()
	{
		return this.windowHeight;
	}

	@Override
	public Integer getWindowLeftPosition()
	{
		return this.windowLeftPosition;
	}

	@Override
	public WindowState getWindowState()
	{
		return this.windowState;
	}

	@Override
	public Integer getWindowTopPosition()
	{
		return this.windowTopPosition;
	}

	@Override
	public Integer getWindowWidth()
	{
		return this.windowWidth;
	}

	/**
	 * @param windowState
	 */
	@Inject(optional=true)
	public void setWindowState(@Named("WINDOW_STATE") WindowState windowState)
	{
		this.windowState = windowState;
	}

	/**
	 * @param windowLeftPosition
	 */
	@Inject(optional=true)
	public void setWindowLeftPosition(@Named("WINDOW_LEFT_POSITION") Integer windowLeftPosition)
	{
		this.windowLeftPosition = windowLeftPosition;
	}

	/**
	 * @param windowTopPosition
	 */
	@Inject(optional=true)
	public void setWindowTopPosition(@Named("WINDOW_TOP_POSITION") Integer windowTopPosition)
	{
		this.windowTopPosition = windowTopPosition;
	}

	/**
	 * @param windowWidth
	 */
	@Inject(optional=true)
	public void setWindowWidth(@Named("WINDOW_WIDTH") Integer windowWidth)
	{
		this.windowWidth = windowWidth;
	}

	/**
	 * @param windowHeight
	 */
	@Inject(optional=true)
	public void setWindowHeight(@Named("WINDOW_HEIGHT") Integer windowHeight)
	{
		this.windowHeight = windowHeight;
	}

	@Override
	public List<Class<? extends Module>> getPageObjectModuleClasses()
	{
		return this.pageObjectModuleClasses;
	}

	/**
	 * 
	 * @param moduleClass
	 */
	@Inject(optional=true)
	public void setPageObjectModuleClasses(@Named("PAGE_OBJECT_MODULE") String moduleClass)
	{
		List<String> names = Splitter.on(',')
				.trimResults()
				.splitToList(moduleClass);
		
		List<Class<? extends Module>> outList = Lists.newArrayListWithExpectedSize(names.size());
		
		for (String className : names) {
			try
			{
				outList.add(Class.forName(className).asSubclass(Module.class));
			}
			catch (ClassNotFoundException e)
			{
				throw new IllegalArgumentException("The page object module class name specified '" + className + "' could not be loaded.", e);
			}
			catch (ClassCastException cce) {
				throw new IllegalArgumentException("The page object module class does not implement Module, '" + className + "' could not be loaded.", cce);
			}
		}
		
		// hold off assigning until no exceptions can be thrown, always valid 
		// state :)
		this.pageObjectModuleClasses = ImmutableList.copyOf(outList);
		
	}

	@Override
	public Integer getMouseRelocateLeftPosition()
	{
		return mouseRelocateLeftPosition;
	}

	@Override
	public Integer getMouseRelocateTopPosition()
	{
		return mouseRelocateTopPosition;
	}

	/**
	 * @param mouseRelocateLeftPosition
	 */
	@Inject(optional=true)
	public void setMouseRelocateLeftPosition(@Named("MOUSE_RELOCATE_LEFT_POSITION") Integer mouseRelocateLeftPosition)
	{
		this.mouseRelocateLeftPosition = mouseRelocateLeftPosition;
	}

	/**
	 * @param mouseRelocateTopPosition
	 */
	@Inject(optional=true)
	public void setMouseRelocateTopPosition(@Named("MOUSE_RELOCATE_TOP_POSITION") Integer mouseRelocateTopPosition)
	{
		this.mouseRelocateTopPosition = mouseRelocateTopPosition;
	}

	@Override
	public long getClickDelayMilliseconds()
	{
		return this.clickDelayMilliseconds;
	}

	/**
	 * @param clickDelayMilliseconds
	 */
	@Inject(optional=true)
	public void setClickDelayMilliseconds(@Named("CLICK_DELAY_MS") long clickDelayMilliseconds)
	{
		this.clickDelayMilliseconds = clickDelayMilliseconds;
	}
	@Override
	public boolean isStaticHtmlCapture() {
		return staticHtmlCapture;
	}
	/**
	 * 
	 * @param staticHtmlCapture
	 */
	@Inject(optional=true)
	public void setStaticHtmlCapture(@Named("STATICHTMLCAPTURE") boolean staticHtmlCapture) {
		
		this.staticHtmlCapture = staticHtmlCapture;
	}
	@Override
	public String getStaticHtmlDir() {
		return staticHtmlDir;
	}
	/**
	 * 
	 * @param staticHtmlDir
	 */
	@Inject(optional=true)
	public void setStaticHtmlDir(@Named("STATICHTMLDIRECTORY") String staticHtmlDir) {
		this.staticHtmlDir = staticHtmlDir;
	}
	
	/** screen shot methods **/
	@Override
	public boolean isScreenShotOnStep() {
		return screenShotOnStep;
	}
	
	/**
	 * @param screenShotOnStep
	 */
	@Inject(optional=true)
	public void setScreenShotOnStep(@Named("SCREENSHOTONSTEP") boolean screenShotOnStep) {
		this.screenShotOnStep = screenShotOnStep;
	}
	@Override
	public boolean isScreenShotOnFail() {
		return screenShotOnFail;
	}
	
	/**
	 * @param screenShotOnFail
	 */
	@Inject(optional=true)
	public void setScreenShotOnFail(@Named("SCREENSHOTONFAIL") boolean screenShotOnFail) {
		this.screenShotOnFail = screenShotOnFail;
	}
	
	@Override
	public String getScreenShotPath() {
		return screenShotPath;
	}
	
	/**
	 * @param screenShotPath
	 */
	@Inject(optional=true)
	public void setScreenShotPath(@Named("SCREENSHOTPATH") String screenShotPath) {
		this.screenShotPath = screenShotPath;
	}

	@Override
	public String getProxyHostName()
	{
		return proxyHostName;
	}

	/**
	 * @param proxyHostName
	 */
	@Inject(optional=true)
	public void setProxyHostName(@Named("PROXY_HOSTNAME") String proxyHostName)
	{
		// If it's an empty string, set it to null
		if (proxyHostName != null && proxyHostName.trim().isEmpty())
		{
			proxyHostName = null;
		}
		this.proxyHostName = proxyHostName;
	}

	@Override
	public String getRemoteBrowserHostName()
	{
		return remoteBrowserHostName;
	}

	/**
	 * @param remoteBrowserHostName
	 */
	@Inject(optional=true)
	public void setRemoteBrowserHostName(@Named("REMOTE_BROWSER_HOSTNAME") String remoteBrowserHostName)
	{
		// If it's an empty string, set it to null
		if (remoteBrowserHostName != null && remoteBrowserHostName.trim().isEmpty())
		{
			remoteBrowserHostName = null;
		}
		this.remoteBrowserHostName = remoteBrowserHostName;
	}

	@Override
	public int getProxyPort()
	{
		return proxyPort;
	}

	/**
	 * @param proxyPort
	 */
	@Inject(optional=true)
	public void setProxyPort(@Named("PROXY_PORT") int proxyPort)
	{
		if (proxyPort < 0)
		{
			throw new IllegalArgumentException("The port specified cannot be negative (" + proxyPort + ").");
		}
		
		this.proxyPort = proxyPort;
	}

	@Override
	public int getRemoteBrowserPort()
	{
		return remoteBrowserPort;
	}

	/**
	 * @param remoteBrowserPort
	 */
	@Inject(optional=true)
	public void setRemoteBrowserPort(@Named("REMOTE_BROWSER_PORT") final int remoteBrowserPort)
	{
		if (remoteBrowserPort < 0)
		{
			throw new IllegalArgumentException("The port specified cannot be negative (" + remoteBrowserPort + ").");
		}
		
		this.remoteBrowserPort = remoteBrowserPort;
	}

	@Override
	public Map<String, String> getSelenium2WebDriverCapabilities()
	{
		return selenium2WebDriverCapabilities;
	}
	
	/**
	 * @param selenium2WebDriverCapabilities
	 */
	@Inject(optional=true)
	public void setSelenium2WebDriverCapabilities(@Named("SELENIUM_WEBDRIVER_CAPABILITIES") final String selenium2WebDriverCapabilities)
	{
		// If it's not an empty string, begin parsing, expecting <key1>=<value1>;<key2>=<value2>;...
		if (selenium2WebDriverCapabilities == null || !selenium2WebDriverCapabilities.trim().isEmpty())
		{
			String[] capabilities = selenium2WebDriverCapabilities.split(";");
			for (String capability : capabilities)
			{
				if (capability.trim().isEmpty())
				{
					throw new IllegalArgumentException("The capabilities property '" + selenium2WebDriverCapabilities + "' is not in the form: key1=value1;key2=value2;...");
				}
				
				String[] capabilityTuple = capability.split("=");
				if (capabilityTuple.length != 2)
				{
					throw new IllegalArgumentException("The capability '" + capability + "' is not in the form: key=value");
				}

				String key = capabilityTuple[0].trim();
				String value = capabilityTuple[1].trim();
				this.selenium2WebDriverCapabilities.put(key, value);
			}
		}
	}

	@Deprecated
	public ViewPortType getViewPortName(Integer pageWidth){
		ViewPortType viewport = ViewPortType.DESKTOP;
		if(populateViewPorts()) {
			viewport = ViewPortType.valueOf(viewports.ceilingEntry(pageWidth).getValue().toUpperCase());
		}
		return viewport;
	}
	
	@Override
	public Optional<String> getViewPort(int pPageWidth)
	{
		Optional<String> result = Optional.absent();
		
		if (populateViewPorts())
		{
			result = Optional.of(viewports.ceilingEntry(pPageWidth).getValue().toUpperCase());
		}
		
		return result;
	}
	
	/**
	 * Must be called by any method prior to trying to access the view ports map.
	 * @return false if no view ports are defined
	 */
	private boolean populateViewPorts()
	{
		boolean isViewPortDefined = true;
		if (this.viewports == null)
		{
			this.viewports = new TreeMap<Integer, String>();
			
			for (int i = 1; ; i++)
			{
				String name = getCustomProperty(String.format(VIEWPORT_NAME_PROPERTY, i));
				Integer ceiling = null;
				String max = null;
				if (name != null)
				{
					max = getCustomProperty(String.format(VIEWPORT_MAX_PROPERTY, i));
					if(max == null) {
						throw new IllegalStateException("Exception processing viewports. viewport '" + name + "' did not have a corresponding 'VIEWPORT." + i + ".WIDTH.MAX' property.");
						
					}
					else if (max.equals("max"))
					{
						ceiling= Integer.MAX_VALUE;
					}
					else {
						ceiling= Integer.parseInt(max);
					}
						
					
					this.viewports.put(ceiling, name);
				}
				else
				{
					if(!viewports.containsKey(Integer.MAX_VALUE)){
						log.warning("One of the viewports WIDTH.MAX must be set to \"max\". Most likely no view ports are defined");
					}
					break;
				}
			}
			
		}
		//if viewports map is empty, view ports are not defined.
		if(this.viewports.size() == 0) {
			isViewPortDefined = false;
		}
		return isViewPortDefined;
	}
	public static void main(String[] args)
	{
		String[] capabilities = "enablePersistentHover=false;;; uh=howdy".split(";");
		for (String string : capabilities)
		{
			System.out.print("'" + string + "'");
		}
		
	}

	/**
	 * @param userAgent
	 */
	@Inject(optional=true)
	public void setUserAgent(@Named("USERAGENT") String userAgent) {
		this.userAgent = userAgent;
	}
	
	@Override
	public String getUserAgent() {
		return this.userAgent;
	}
}
