package com.ibm.commerce.qa.wte.framework.util;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2015
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.LogManager;

import com.google.inject.Inject;
/**
 * class that configures the logging capablities.
 *
 */
public class LoggingConfig
{
	/**
	 * Configures the Java {@link LogManager} in accordance to the
	 * <code>logging.properties</code> found in the working directory
	 * <b>unless</b> either of the following system properties were defined:
	 * <code>java.util.logging.config.class</code> or
	 * <code>java.util.logging.config.file</code>.
	 * @param configManager 
	 * 
	 * @throws IllegalStateException
	 *             if the logging config file was needed and was not readable
	 */
	@Inject
	public LoggingConfig(WteConfigManager configManager)
	{
		boolean logBoolean = configManager.getLogSetupBoolean();
		if (System.getProperty("java.util.logging.config.class") == null
				&& System.getProperty("java.util.logging.config.file") == null
				&& logBoolean)
		{
			File logConfigFile = getLoggingFile();
			
			if (!logConfigFile.canRead())
			{
				throw new IllegalStateException("The logging configuration file '" + logConfigFile.getAbsolutePath() + "' cannot be read.");
			}
			
			InputStream is = null;
			try
			{
				is = new FileInputStream(logConfigFile);
				LogManager.getLogManager().readConfiguration(is);
			}
			catch (IOException e)
			{
				throw new IllegalStateException("I/O exception while reading the logging configuration from '" + logConfigFile.getAbsolutePath() + "'.", e);
			}
			finally
			{
				FileUtil.closeQuietly(is);
			}
		}
	}


	/**
	 * Attempt to load the logging configuration file.
	 * 
	 * @return the non-null file
	 */
	private File getLoggingFile()
	{
		String loggingFileName = System.getProperty("logging.config.file", "logging.properties");
		
		File result = new File(loggingFileName);
		
		return result;
	}
}
