package com.ibm.commerce.qa.wte.framework.util;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2015
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import com.ibm.commerce.qa.wte.framework.page.Identifier;
import com.ibm.commerce.qa.wte.framework.page.PageValidator;


/**
 * PageValidatorPerformer performs the page validation for a given page object
 */
public interface PageValidatorPerformer {
	
	/**
	 * Validate specified page object by checking each {@link Identifier} field annotated with {@link PageValidator}
	 * @param pageObject
	 */
	public <I> void validatePage(I pageObject);
	
}
