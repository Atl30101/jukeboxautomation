package com.ibm.commerce.qa.wte.framework.locator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
/**
 * ByAngular utility to get various By locator for AngularJs
 */
public class ByAngular {
	
	/**
	 * Get By for locating elements by angular binding
	 * 
	 * @param p_webDriver
	 * @param p_binding
	 * @param p_useExactMatch
	 * @return By locator for locating Angular ByBinding
	 */
	public static By byAngularBinding(WebDriver p_webDriver, String p_binding, boolean p_useExactMatch) {
		return new ByAngularBinding(p_webDriver, p_binding, p_useExactMatch);
	}
	
	/**
	 * Get By for locating elements by angular ng-repeat
	 * @param p_webDriver
	 * @param p_repeater
	 * @param p_index
	 * @param p_binding
	 * @return By locator for locating Anuglar ng-repeat
	 */
	public static By byAngularRepeater(WebDriver p_webDriver, String p_repeater, int p_index, String p_binding) {
		return new ByAngularRepeater(p_webDriver, p_repeater, p_index, p_binding);
	}
}
