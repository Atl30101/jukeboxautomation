package com.ibm.commerce.qa.wte.framework.locator;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * 
 * ByAngularBinding handles By locator for angular binding
 * 
 * {{expression}} and <div ng-bind="binding">
 *
 */
public class ByAngularBinding extends By {
	
	/**
	 * TOKEN used in JS as place holder for binding
	 */
	public static final String JS_BINDING_TOKEN = "__BINDING__";
	
	/**
	 * TOKEN used in JS as place holder for exactMatch
	 */
	public static final String JS_EXACTMATCH_TOKEN = "__EXACTMATCH__";
	
	/**
	 * JS file name
	 */
	public static final String BY_BINDING_JS_FILE_NAME = "ByBinding.js";
	
	private final JavascriptExecutor jse;

	private String binding;
	
	private boolean useExactMatch;
	
	/**
	 * Construct ByAngularBinding by specifying webDriver, binding, and useExactMatch
	 * @param p_webDriver 
	 * @param p_binding
	 * @param p_useExactMatch
	 */
	public ByAngularBinding(WebDriver p_webDriver, String p_binding, boolean p_useExactMatch) {
		
		if (p_webDriver instanceof JavascriptExecutor) {
			jse = (JavascriptExecutor)p_webDriver;
		} else {
			throw new UnsupportedOperationException ("The web driver can not execute Javascript.");
		}
		
		binding = StringEscapeUtils.escapeEcmaScript(p_binding);
		useExactMatch = p_useExactMatch;
	}

	
	private String getJavaScript() {
		// Getting the javascript template
		InputStream input = ByAngularBinding.class.getResourceAsStream(BY_BINDING_JS_FILE_NAME);
		String javascript = null;
		
		try {
			javascript = IOUtils.toString(input);
			IOUtils.closeQuietly(input);
		} catch (IOException e) {
			e.printStackTrace();
			throw new IllegalStateException("Failed to get javascript from " + BY_BINDING_JS_FILE_NAME);
		}
		
		return javascript
				.replace(JS_BINDING_TOKEN, binding)
				.replace(JS_EXACTMATCH_TOKEN, String.valueOf(useExactMatch));
	}


	@Override
	public List<WebElement> findElements(SearchContext p_searchContext) {
		SearchContext searchContext;
		if (p_searchContext instanceof WebDriver) {
            searchContext = null;
        } else {
        	searchContext = p_searchContext;
        }
		
        @SuppressWarnings("unchecked")
		List<WebElement> executeScript = (List<WebElement>) jse.executeScript(getJavaScript(), searchContext);
		return executeScript;
	}
	
	@Override
	public String toString() {
		return "ByAnglarBinding" + (useExactMatch ? "(Exact match)" : "(Partial Match)");
	}
	
}
