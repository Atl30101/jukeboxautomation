/**
 * This is the JavaScript template for locating the element by repeater
 * 
 * __REPEATER__ should be replaced with the real String value of binding
 * 
 */




function locateByRepeaterAndBinding(repeater, binding) {

	var matches = [];
	var using = document;
	var rows = [];
	var prefixes = [ 'ng-', 'ng_', 'data-ng-', 'x-ng-', 'ng\\:' ];
	for ( var p = 0; p < prefixes.length; ++p) {
		var attr = prefixes[p] + 'repeat';
		var repeatElems = using.querySelectorAll('[' + attr + ']');
		attr = attr.replace(/\\/g, '');
		for ( var i = 0; i < repeatElems.length; ++i) {
			if (repeatElems[i].getAttribute(attr).indexOf(repeater) != -1) {
				rows.push(repeatElems[i]);
			}
		}
	}

	/*
	 * multiRows is an array of arrays, where each inner array contains one row
	 * of elements.
	 */
	var multiRows = [];
	for ( var p = 0; p < prefixes.length; ++p) {
		var attr = prefixes[p] + 'repeat-start';
		var repeatElems = using.querySelectorAll('[' + attr + ']');
		attr = attr.replace(/\\/g, '');
		for ( var i = 0; i < repeatElems.length; ++i) {
			if (repeatElems[i].getAttribute(attr).indexOf(repeater) != -1) {
				var elem = repeatElems[i];
				var row = [];
				var COMMENT_TYPE = 8;
				var ELEMENT_TYPE = 1;
				while (elem.nodeType != COMMENT_TYPE
						|| (elem.nodeValue && elem.nodeValue.indexOf(repeater) == -1)) {
					if (elem.nodeType == ELEMENT_TYPE) {
						row.push(elem);
					}
					elem = elem.nextSibling;
				}
				multiRows.push(row);
			}
		}
	}
	var bindings = [];
	for ( var i = 0; i < rows.length; ++i) {
		if (rows[i].className.indexOf('ng-binding') != -1) {
			bindings.push(rows[i]);
		}
		var childBindings = rows[i].getElementsByClassName('ng-binding');
		for ( var k = 0; k < childBindings.length; ++k) {
			bindings.push(childBindings[k]);
		}
	}
	for ( var i = 0; i < multiRows.length; ++i) {
		for ( var j = 0; j < multiRows[i].length; ++j) {
			var elem = multiRows[i][j];
			if (elem.className.indexOf('ng-binding') != -1) {
				bindings.push(elem);
			}
			var childBindings = elem.getElementsByClassName('ng-binding');
			for ( var k = 0; k < childBindings.length; ++k) {
				bindings.push(childBindings[k]);
			}
		}
	}
	for ( var j = 0; j < bindings.length; ++j) {
		var dataBinding = angular.element(bindings[j]).data('$binding');
		if (dataBinding) {
			var bindingName = dataBinding.exp || dataBinding[0].exp
					|| dataBinding;
			if (bindingName.indexOf(binding) != -1) {
				matches.push(bindings[j]);
			}
		}
	}
	return matches;
}

function locateByRepeaterAndIndex(repeater, index) {
	var using = document;
	var prefixes = [ 'ng-', 'ng_', 'data-ng-', 'x-ng-', 'ng\\:' ];
	var rows = [];
	for ( var p = 0; p < prefixes.length; ++p) {
		var attr = prefixes[p] + 'repeat';
		var repeatElems = using.querySelectorAll('[' + attr + ']');
		attr = attr.replace(/\\/g, '');
		for ( var i = 0; i < repeatElems.length; ++i) {
			if (repeatElems[i].getAttribute(attr).indexOf(repeater) != -1) {
				rows.push(repeatElems[i]);
			}
		}
	}
	/*
	 * multiRows is an array of arrays, where each inner array contains one row
	 * of elements.
	 */
	var multiRows = [];
	for ( var p = 0; p < prefixes.length; ++p) {
		var attr = prefixes[p] + 'repeat-start';
		var repeatElems = using.querySelectorAll('[' + attr + ']');
		attr = attr.replace(/\\/g, '');
		for ( var i = 0; i < repeatElems.length; ++i) {
			if (repeatElems[i].getAttribute(attr).indexOf(repeater) != -1) {
				var elem = repeatElems[i];
				var row = [];
				var COMMENT_TYPE = 8;
				var ELEMENT_TYPE = 1;
				while (elem.nodeType != COMMENT_TYPE
						|| (elem.nodeValue.indexOf(repeater) == -1)) {
					if (elem.nodeType == ELEMENT_TYPE) {
						row.push(elem);
					}
					elem = elem.nextSibling;
				}
				multiRows.push(row);
			}
		}
	}
	var row = rows[index] || [], multiRow = multiRows[index] || [];
	return [].concat(row, multiRow);
}

function locateByRepeaterAndIndexAndBinding(repeater, index, binding) {
	var matches = [];
	var using = document;
	var rows = [];
	var prefixes = [ 'ng-', 'ng_', 'data-ng-', 'x-ng-', 'ng\\:' ];
	for ( var p = 0; p < prefixes.length; ++p) {
		var attr = prefixes[p] + 'repeat';
		var repeatElems = using.querySelectorAll('[' + attr + ']');
		attr = attr.replace(/\\/g, '');
		for ( var i = 0; i < repeatElems.length; ++i) {
			if (repeatElems[i].getAttribute(attr).indexOf(repeater) != -1) {
				rows.push(repeatElems[i]);
			}
		}
	}
	/*
	 * multiRows is an array of arrays, where each inner array contains one row
	 * of elements.
	 */
	var multiRows = [];
	for ( var p = 0; p < prefixes.length; ++p) {
		var attr = prefixes[p] + 'repeat-start';
		var repeatElems = using.querySelectorAll('[' + attr + ']');
		attr = attr.replace(/\\/g, '');
		for ( var i = 0; i < repeatElems.length; ++i) {
			if (repeatElems[i].getAttribute(attr).indexOf(repeater) != -1) {
				var elem = repeatElems[i];
				var row = [];
				var COMMENT_TYPE = 8;
				var ELEMENT_TYPE = 1;
				while (elem.nodeType != COMMENT_TYPE
						|| (elem.nodeValue && (elem.nodeValue().indexOf(repeater) == -1))) {
					if (elem.nodeType == ELEMENT_TYPE) {
						row.push(elem);
					}
					elem = elem.nextSibling;
				}
				multiRows.push(row);
			}
		}
	}
	var row = rows[index];
	var multiRow = multiRows[index];
	var bindings = [];
	if (row) {
		if (row.className.indexOf('ng-binding') != -1) {
			bindings.push(row);
		}
		var childBindings = row.getElementsByClassName('ng-binding');
		for ( var i = 0; i < childBindings.length; ++i) {
			bindings.push(childBindings[i]);
		}
	}
	if (multiRow) {
		for ( var i = 0; i < multiRow.length; ++i) {
			var rowElem = multiRow[i];
			if (rowElem.className.indexOf('ng-binding') != -1) {
				bindings.push(rowElem);
			}
			var childBindings = rowElem
					.getElementsByClassName('ng-binding');
			for ( var j = 0; j < childBindings.length; ++j) {
				bindings.push(childBindings[j]);
			}
		}
	}
	for ( var i = 0; i < bindings.length; ++i) {
		var dataBinding = angular.element(bindings[i]).data('$binding');
		if (dataBinding) {
			var bindingName = dataBinding.exp || dataBinding[0].exp
					|| dataBinding;
			if (bindingName.indexOf(binding) != -1) {
				matches.push(bindings[i]);
			}
		}
	}
	return matches;
}

function locateByRepeater(repeater) {
	var using = document;
	var rows = [];
	var prefixes = [ 'ng-', 'ng_', 'data-ng-', 'x-ng-', 'ng\\:' ];
	for ( var p = 0; p < prefixes.length; ++p) {
		var attr = prefixes[p] + 'repeat';
		var repeatElems = using.querySelectorAll('[' + attr + ']');
		attr = attr.replace(/\\/g, '');
		for ( var i = 0; i < repeatElems.length; ++i) {
			if (repeatElems[i].getAttribute(attr).indexOf(repeater) != -1) {
				rows.push(repeatElems[i]);
			}
		}
	}
	for ( var p = 0; p < prefixes.length; ++p) {
		var attr = prefixes[p] + 'repeat-start';
		var repeatElems = using.querySelectorAll('[' + attr + ']');
		attr = attr.replace(/\\/g, '');
		for ( var i = 0; i < repeatElems.length; ++i) {
			if (repeatElems[i].getAttribute(attr).indexOf(repeater) != -1) {
				var elem = repeatElems[i];
				var COMMENT_TYPE = 8;
				var ELEMENT_TYPE = 1;
				while (elem.nodeType != COMMENT_TYPE
						|| elem.nodeValue.indexOf(repeater) == -1) {
					if (elem.nodeType == ELEMENT_TYPE) {
						rows.push(elem);
					}
					elem = elem.nextSibling;
				}
			}
		}
	}
	return rows;
}

var repeater = '__REPEATER__';
var index = __INDEX__;
var binding = '__BINDING__';

if (index == -1) {
	if (binding == '') {
		return locateByRepeater(repeater);
	} else {
		return locateByRepeaterAndBinding(repeater, binding);
	}
} else {
	if (binding == '') {
		return locateByRepeaterAndIndex(repeater, index);
	} else {
		return locateByRepeaterAndIndexAndBinding(repeater, index, binding);
	}
}