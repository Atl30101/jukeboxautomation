/**
 * This is the JavaScript template for locating the element by binding
 * It handles both <div ng-bind="binding"> and {{binding}} with both partial and exact match
 * 
 * __BINDING__ should be replaced with the real String value of binding
 * __EXACTMATCH__ should be replaced with the real boolean value of whether or not to use exact match
 * 
 */

/**
 * escape string for regex
 * @param str
 * @returns escaped string
 */
function escapeRegExp(str) {
  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

/**
 * Function to get element by binding
 * 
 * @param binding
 * @param exactMatch
 * @returns {Array}
 */
function locateBinding(binding, exactMatch) {
	var allBindings = document.getElementsByClassName('ng-binding');
	var matches = [];
	for ( var i = 0; i < allBindings.length; ++i) {
		try {
			var dataBinding = angular.element(allBindings[i]).data().$binding;
			var bindingName = dataBinding.exp || dataBinding[0].exp
					|| dataBinding[0];

			if (exactMatch) {
				// escape binding in case it contains regex sensitive char
				// e.g "name | uppercase" should be escaped as "name \| uppercase"
				var escapedBinding = escapeRegExp(binding);
				var matcher = new RegExp('({|\\s|^|\\|)' + escapedBinding
						+ '(}|\\s|$|\\|)');
				if (matcher.test(bindingName)) {
					matches.push(allBindings[i]);
				}
			} else {
				if (bindingName.indexOf(binding) != -1) {
					matches.push(allBindings[i]);
				}
			}
		} catch (err) {
			// in case class ng-binding is used by HTML, ignore it, 
			// otherwise throw it
			if (!err instanceof TypeError) {
				throw err;
			}
		}
	}
	return matches;
}

return locateBinding('__BINDING__', __EXACTMATCH__);