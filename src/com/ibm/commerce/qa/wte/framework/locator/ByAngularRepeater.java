package com.ibm.commerce.qa.wte.framework.locator;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * 
 * ByAngularBinding handles By locator for angular binding
 * 
 * {{expression}} and <div ng-bind="binding">
 *
 */
public class ByAngularRepeater extends By {
	
	/**
	 * TOKEN used in JS as place holder for repeater
	 */
	public static final String JS_REPEATER_TOKEN = "__REPEATER__";
	
	/**
	 * TOKEN used in JS as place holder for index (row in protractor)
	 */
	public static final String JS_INDEX_TOKEN = "__INDEX__";
	
	/**
	 * TOKEN used in JS as place holder for binding (column in protractor)
	 */
	public static final String JS_BINDING_TOKEN = "__BINDING__";
	
	/**
	 * JS file name
	 */
	public static final String BY_REPEATER_JS_FILE_NAME = "ByRepeater.js";
	
	private final JavascriptExecutor jse;

	private String repeat;
	
	private int index;
	
	private String binding;
	
	/**
	 * Construct ByAngularBinding by specifying webDriver, binding, and useExactMatch
	 * @param p_webDriver 
	 * @param p_repeat 
	 * @param p_index 0 based, -1 to specify not set
	 * @param p_binding the binding name to query,  "" to specify not set
	 */
	public ByAngularRepeater(WebDriver p_webDriver, String p_repeat, int p_index, String p_binding) {
		
		if (p_webDriver instanceof JavascriptExecutor) {
			jse = (JavascriptExecutor)p_webDriver;
		} else {
			throw new UnsupportedOperationException ("The web driver can not execute Javascript.");
		}
		
		repeat = StringEscapeUtils.escapeEcmaScript(p_repeat);
		
		index = p_index;
		
		binding = p_binding;
	}

	
	private String getJavaScript() {
		// Getting the javascript template
		InputStream input = ByAngularRepeater.class.getResourceAsStream(BY_REPEATER_JS_FILE_NAME);
		String javascript = null;
		
		try {
			javascript = IOUtils.toString(input);
			IOUtils.closeQuietly(input);
		} catch (IOException e) {
			e.printStackTrace();
			throw new IllegalStateException("Failed to get javascript from " + BY_REPEATER_JS_FILE_NAME);
		}
		
		return javascript
				.replace(JS_REPEATER_TOKEN, repeat)
				.replace(JS_INDEX_TOKEN, "" + index)
				.replace(JS_BINDING_TOKEN, binding);
	}


	@Override
	public List<WebElement> findElements(SearchContext p_searchContext) {
		SearchContext searchContext;
		if (p_searchContext instanceof WebDriver) {
            searchContext = null;
        } else {
        	searchContext = p_searchContext;
        }
		
        @SuppressWarnings("unchecked")
		List<WebElement> executeScript = (List<WebElement>) jse.executeScript(getJavaScript(), searchContext);
		return executeScript;
	}
	
	@Override
	public String toString() {
		return "ByAnglarRepeater";
	}
	
}
