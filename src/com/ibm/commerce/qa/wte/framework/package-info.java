/**
 * This package contains the WTE framework. This includes all the
 * interfaces and utility classes necessary to work with the framework. The
 * package does not contain any implementations of the framework.
 */
package com.ibm.commerce.qa.wte.framework;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2011, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

