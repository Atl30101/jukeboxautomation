package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

/**
 * Defines the type of timeouts that are supported when locating elements.
 */
public enum ElementTimeoutType
{
	/**
	 * Used for elements that should already be present on a loaded page.
	 */
	IMPLICIT,

	/**
	 * Used for elements that determine if a page is loaded. This is generally
	 * used internally when elements are annotated with
	 * {@link PageValidator}.
	 */
	PAGE_LOAD,

	/**
	 * Used for elements that require time for an AJAX call to be processed
	 * before being used.
	 */
	AJAX,

	/**
	 * Used for elements that require execution of a script on the page (that is
	 * not instantaneous) before being used.
	 */
	SCRIPT,

	/**
	 * Used for elements that require an explicit amount of time before being
	 * used. This should only be used in rare cases where a custom timeout needs
	 * to be specified that doesn't fit into the other
	 * {@link ElementTimeoutType} types.
	 */
	EXPLICIT;
}
