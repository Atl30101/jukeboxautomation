package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

/**
 * Used to describe the location of an element by an identifier.
 */
public abstract class Identifier
{
	/**
	 * Used internally by implementations of the framework.
	 * 
	 * 
	 * 
	 */
	public static class ByClassName extends Identifier
	{
		/**
		 * @param className
		 */
		ByClassName(String className)
		{
			super(className);
		}

		@Override
		public void visit(IdentifierVisitor visitor)
		{
			visitor.accept(this);
		}

		@Override
		public boolean isRelativeExpression() {
			// by default, "ClassName" is always relative
			return true;
		}
	}

	/**
	 * Used internally by implementations of the framework.
	 * 
	 * 
	 * 
	 */
	public static class ById extends Identifier
	{
		/**
		 * @param className
		 */
		ById(String id)
		{
			super(id);
		}

		@Override
		public void visit(IdentifierVisitor visitor)
		{
			visitor.accept(this);
		}

		@Override
		public boolean isRelativeExpression() {
			// by default, "id" is always relative
			return true;
		}
	}

	/**
	 * Used internally by implementations of the framework.
	 * 
	 * 
	 * 
	 */
	public static class ByLinkText extends Identifier
	{
		/**
		 * @param className
		 */
		ByLinkText(String linkText)
		{
			super(linkText);
		}

		@Override
		public void visit(IdentifierVisitor visitor)
		{
			visitor.accept(this);
		}

		@Override
		public boolean isRelativeExpression() {
			// by default, "link" is always relative
			return true;
		}
	}

	/**
	 * Used internally by implementations of the framework.
	 * 
	 * 
	 * 
	 */
	public static class ByPartialLinkText extends Identifier
	{
		/**
		 * @param className
		 */
		ByPartialLinkText(String partialLinkText)
		{
			super(partialLinkText);
		}

		@Override
		public void visit(IdentifierVisitor visitor)
		{
			visitor.accept(this);
		}

		@Override
		public boolean isRelativeExpression() {
			// by default, "partial links" are always relative
			return true;
		}
	}

	/**
	 * Used internally by implementations of the framework.
	 * 
	 * 
	 * 
	 */
	public static class ByXPath extends Identifier
	{
		private final boolean isRelative;
		/**
		 * @param className
		 */
		ByXPath(String xPath)
		{
			super(xPath.trim());
			
			String expr = xPath.trim();
			if (expr.isEmpty()) {
				throw new IllegalArgumentException("xPath is empty");
			}
			
			// figure out if it is relative
			boolean tRelative = true;
			for (int i = 0; i < expr.length(); ++i) {
				int c = expr.codePointAt(i);
				if (c == '/') {
					tRelative = false;
					break;
				} else if (c != '(') {
					break;
				}
				// it's an open '(', wait till another char
			}
			isRelative = tRelative;
		}

		@Override
		public void visit(IdentifierVisitor visitor)
		{
			visitor.accept(this);
		}

		@Override
		public boolean isRelativeExpression() {
			return isRelative;
		}
	}
	
	/**
	 * Used internally by implementations of the framework.
	 * 
	 * Allowing to locate element by AngularJs ng-model
	 * 
	 */
	public static class ByModel extends Identifier
	{

		protected ByModel(String model) {
			super(model);
		}

		@Override
		public boolean isRelativeExpression() {
			return true;
		}

		@Override
		public void visit(IdentifierVisitor visitor) {
			visitor.accept(this);
		}
		
	}
	
	/**
	 * Used internally by implementations of the framework.
	 * 
	 * Allowing to locate element by AngularJs ng-bind or {{expression}} for partial match
	 * 
	 */
	public static class ByBinding extends Identifier
	{

		protected ByBinding(String model) {
			super(model);
		}

		@Override
		public boolean isRelativeExpression() {
			return false;
		}

		@Override
		public void visit(IdentifierVisitor visitor) {
			visitor.accept(this);
		}
		
	}
	
	/**
	 * Used internally by implementations of the framework.
	 * 
	 * Allowing to locate element by AngularJs ng-bind or {{expression}} for exact match
	 * 
	 */
	public static class ByExactBinding extends Identifier
	{

		protected ByExactBinding(String model) {
			super(model);
		}

		@Override
		public boolean isRelativeExpression() {
			return false;
		}

		@Override
		public void visit(IdentifierVisitor visitor) {
			visitor.accept(this);
		}
		
	}
	
	/**
	 * Used internally by implementations of the framework.
	 * 
	 * Allowing to locate element by AngularJs ng-repeat or ng-repeat-start
	 * 
	 */
	public static class ByRepeater extends Identifier
	{
		private int index;
		
		private String binding;
		
		/**
		 * 
		 * @param repeater the repeater string
		 * @param index 0 based index to get the specific item, or -1 to mean get all
		 * @param binding the binding to get from the repeater, or "" (empty) string to mean not get any binding
		 */
		protected ByRepeater(String repeater, int p_index, String p_binding) {
			super(repeater);
			
			index = p_index;
			
			binding = p_binding;
		}
		
		protected ByRepeater(String repeater) {
			super(repeater);
			
			index = -1;
			
			binding = "";
		}
		
		protected ByRepeater(String repeater, int p_index) {
			super(repeater);
			
			index = p_index;
			
			binding = "";
		}
		
		protected ByRepeater(String repeater, String p_binding) {
			super(repeater);
			
			index = -1;
			
			binding = p_binding;
		}

		@Override
		public boolean isRelativeExpression() {
			return false;
		}

		@Override
		public void visit(IdentifierVisitor visitor) {
			visitor.accept(this);
		}
		
		/**
		 * get Index
		 * @return index
		 */
		public int getIndex() {
			return index;
		}
		
		/**
		 * get Binding
		 * @return binding
		 */
		public String getBinding() {
			return binding;
		}
		
	}
	
	
	/**
	 * Used internally by implementations of the framework.
	 * 
	 * 
	 * 
	 */
	public static class ByCssSelector extends Identifier
	{
		/**
		 * @param className
		 */
		ByCssSelector(String cssSelector)
		{
			super(cssSelector);
		}

		@Override
		public void visit(IdentifierVisitor visitor)
		{
			visitor.accept(this);
		}

		@Override
		public boolean isRelativeExpression() {
			// by default, "css selector" is always relative
			return true;
		}
	}

	/**
	 * Used internally by implementations of the framework.
	 * 
	 * 
	 * 
	 */
	public static class ByName extends Identifier
	{
		/**
		 * @param className
		 */
		ByName(String name)
		{
			super(name);
		}

		@Override
		public void visit(IdentifierVisitor visitor)
		{
			visitor.accept(this);
		}

		@Override
		public boolean isRelativeExpression() {
			// by default, "name" is always relative
			return true;
		}
	}

	/**
	 * Identifies one or more elements by class name.
	 * 
	 * @param className
	 *            non-null class name of the element
	 * @return the identifier object
	 */
	public static Identifier byClassName(String className)
	{
		return new ByClassName(className);
	}

	/**
	 * Identifies an element by its ID.
	 * 
	 * @param id
	 *            non-null id of the element
	 * @return the identifier object
	 */
	public static Identifier byId(String id)
	{
		return new ById(id);
	}

	/**
	 * Identifies an element by its CSS selector.
	 * 
	 * @param cssSelector
	 *            non-null CSS selector of the element
	 * @return the identifier object
	 */
	public static Identifier byCssSelector(String cssSelector)
	{
		return new ByCssSelector(cssSelector);
	}

	/**
	 * Identifies one or more elements by a subset of the text found within a
	 * link element.
	 * 
	 * @param partialLinkText
	 *            non-null text contained in the text area of a link element
	 * @return the identifier object
	 */
	public static Identifier byPartialLinkText(String partialLinkText)
	{
		return new ByPartialLinkText(partialLinkText);
	}

	/**
	 * Identifies one or more elements by the exact text it displays
	 * 
	 * @param linkText
	 *            non-null text displayed by a link
	 * @return the identifier object
	 */
	public static Identifier byLinkText(String linkText)
	{
		return new ByLinkText(linkText);
	}

	/**
	 * Identifies one or more elements via an XPath expression.
	 * 
	 * @param xPath
	 *            the non-null XPath expression
	 * @return the identifier object
	 */
	public static Identifier byXPath(String xPath)
	{
		return new ByXPath(xPath);
	}
	
	/**
	 * Identifies the element via AngularJs ng-model
	 * @param model
	 * @return the identifier object
	 */
	public static Identifier byModel(String model)
	{
		return new ByModel(model);
	}
	
	/**
	 * Identifies the element via AngularJs ng-bind or {{expression}} with partial match
	 * @param binding
	 * @return the identifier object
	 */
	public static Identifier byBinding(String binding)
	{
		return new ByBinding(binding);
	}
	
	/**
	 * Identifies the element via AngularJs ng-bind or {{expression}} with exact match
	 * @param binding
	 * @return the identifier object
	 */
	public static Identifier byExactBinding(String binding)
	{
		return new ByExactBinding(binding);
	}
	
	/**
	 * Identifies the element via AngularJs ng-repeat or ng-repeat-start
	 * @param repeater
	 * @return the identifier object
	 */
	public static Identifier byRepeater(String repeater)
	{
		return new ByRepeater(repeater);
	}
	
	/**
	 * Identifies the element via AngularJs ng-repeat or ng-repeat-start
	 * @param repeater
	 * @param index 0 based index
	 * @return the identifier object
	 */
	public static Identifier byRepeater(String repeater, int index)
	{
		return new ByRepeater(repeater, index);
	}
	
	/**
	 * Identifies the element via AngularJs ng-repeat or ng-repeat-start
	 * @param repeater
	 * @param binding binding
	 * @return the identifier object
	 */
	public static Identifier byRepeater(String repeater, String binding)
	{
		return new ByRepeater(repeater, binding);
	}
	
	/**
	 * Identifies the element via AngularJs ng-repeat or ng-repeat-start
	 * @param repeater
	 * @param index 0 based index
	 * @param binding binding
	 * @return the identifier object
	 */
	public static Identifier byRepeater(String repeater, int index, String binding)
	{
		return new ByRepeater(repeater, index, binding);
	}
	
	

	/**
	 * Identifies one or more elements that have the name specified as the value
	 * of the "name" attribute.
	 * 
	 * @param name
	 *            the non-null value of the name attribute
	 * @return the identifier object
	 */
	public static Identifier byName(String name)
	{
		return new ByName(name);
	}

	/*
	 * The actual abstract class.
	 */
	
	private final String identExpression;

	/**
	 * Creates new instance, sets the identifier expression. 
	 * @param identExpression
	 */
	protected Identifier(String identExpression)
	{
		Preconditions.checkNotNull(identExpression, "Expression == null");
		this.identExpression = identExpression;
	}
	
	private Identifier newInstance(String p_identExpression)
	{
		Identifier result = null;
		
		try
		{
			result = (Identifier)this.getClass().getDeclaredConstructors()[0].newInstance(p_identExpression);
		}
		catch (IllegalArgumentException e)
		{
			throw new IllegalArgumentException("Could not construct new Identifier object based on the input specified.", e);
		}
		catch (SecurityException e)
		{
			throw new IllegalArgumentException("Could not construct new Identifier object based on the input specified.", e);
		}
		catch (InstantiationException e)
		{
			throw new IllegalArgumentException("Could not construct new Identifier object based on the input specified.", e);
		}
		catch (IllegalAccessException e)
		{
			throw new IllegalArgumentException("Could not construct new Identifier object based on the input specified.", e);
		}
		catch (InvocationTargetException e)
		{
			throw new IllegalArgumentException("Could not construct new Identifier object based on the input specified.", e);
		}

		return result;
	}

	/**
	 * Used to fetch the identifier expression. Note this is mainly used
	 * internally by implementations of the framework.
	 * 
	 * @return the expression
	 */
	public String getExpression()
	{
		return this.identExpression;
	}
	
	/**
	 * Returns {@code true} if the stored expression is relative to a node
	 * @return {@code true} if the expression is relative
	 */
	public abstract boolean isRelativeExpression();

	/**
	 * Used by internal implementations of this framework to identify the type
	 * of identifier used.
	 * 
	 * @param visitor
	 */
	public abstract void visit(IdentifierVisitor visitor);

	/**
	 * Creates a new identifier based on this original, but substituting place
	 * holders with values based on the format found in {@link MessageFormat}.
	 * 
	 * @param arguments
	 *            The objects to substitute into the identifer. They must match
	 *            the type expected.
	 * @return the new identifier
	 * @see MessageFormat#format(String, Object...)
	 */
	public Identifier format(Object... arguments)
	{
		MessageFormat msgFormat = new MessageFormat(getExpression());
		
		return newInstance(msgFormat.format(arguments));
	}

	/**
	 * Creates a new identifier based on this original, but substituting one
	 * character for another.
	 * 
	 * @param oldChar
	 *            old character
	 * @param newChar
	 *            new character
	 * @return the new identifer
	 * @see String#replace(char, char)
	 */
	public Identifier replace(char oldChar, char newChar) 
	{
		return newInstance(this.identExpression.replace(oldChar, newChar));
	}
	
	/**
	 * Creates a new identifier based on this original, but substituting a
	 * character sequence (string) for another.
	 * 
	 * @param target
	 *            the sequence of char values to be replaced
	 * @param replacement
	 *            the replacement sequence of char values
	 * @return the new identifer
	 * @see String#replace(CharSequence, CharSequence)
	 */
	public Identifier replace(CharSequence target, CharSequence replacement)
	{
		return newInstance(this.identExpression.replace(target, replacement));
	}

	/**
	 * Creates a new identifier based on this original, but uses a regular
	 * expression to modify the original with the replacement specified only
	 * once.
	 * 
	 * @param regex
	 *            the regular expression to which this string is to be matched
	 * @param replacement
	 *            the replacement string
	 * @return the new identifier
	 * @see String#replaceAll(String, String)
	 */
	public Identifier replaceFirst(String regex, String replacement)
	{
		return newInstance(this.identExpression.replaceFirst(regex, replacement));
	}

	/**
	 * Creates a new identifier based on this original, but uses a regular
	 * expression to modify the original with the replacement for every match of
	 * the regular expression.
	 * 
	 * @param regex
	 *            the regular expression to which this string is to be matched
	 * @param replacement
	 *            the replacement string
	 * @return the new identifier
	 * @see String#replaceAll(String, String)
	 */
	public Identifier replaceAll(String regex, String replacement)
	{
		return newInstance(this.identExpression.replaceAll(regex, replacement));
	}
	
	@Override
	public String toString() {
		String className = getClass().getEnclosingClass().getSimpleName() + "." + getClass().getSimpleName();
		return Objects.toStringHelper(className)
				.add("expression", getExpression())
				.add("isRelative", isRelativeExpression())
				.toString();
	}
}
