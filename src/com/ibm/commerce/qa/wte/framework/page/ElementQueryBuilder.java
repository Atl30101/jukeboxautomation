package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.concurrent.TimeUnit;

import com.ibm.commerce.qa.wte.framework.util.WteConfigManager;
/**
 * a query builder class definition for elements
 *
 */
public interface ElementQueryBuilder
{
	/**
	 * The element to work with.
	 * 
	 * @param element
	 *            the non-null element
	 * @return the builder
	 */
	public ElementQueryBuilder of(WteElement element);
	/**
	 * adds a staleness modifier to the builder. waits until element is stale.
	 * @return the query builder
	 */
	public ElementQueryBuilder asStale();
	
	/**
	 * adds a visibility modifier to the builder. waits until element is visible.
	 * @return the query builder
	 */
	public ElementQueryBuilder asVisible();
	
	
	/**
	 * The opposite of {@link #asVisible()}. waits until element is not visible.
	 * @return the query builder
	 */
	public ElementQueryBuilder asInvisible();
	
	/**
	 * Wait for the element up to the implicit timeout value (this is a
	 * configuration value). This is the default.
	 * 
	 * @return the builder
	 * @see WteConfigManager#getImplicitTimeoutSeconds()
	 */
	public ElementQueryBuilder withinImplicitTimeout();

	/**
	 * Wait for the element up to the script timeout value (this is a
	 * configuration value).
	 * 
	 * @return the builder
	 * @see WteConfigManager#getScriptTimeoutSeconds()
	 */
	public ElementQueryBuilder withinScriptTimeout();

	/**
	 * Wait for the element up to the Ajax timeout value (this is a
	 * configuration value).
	 * 
	 * @return the builder
	 * @see WteConfigManager#getAjaxTimeoutSeconds()
	 */
	public ElementQueryBuilder withinAjaxTimeout();

	/**
	 * Wait for the element up to the explicit timeout value specified.
	 * 
	 * @param time
	 *            the amount of time
	 * @param unit
	 *            the units of time
	 * @return the builder
	 */
	public ElementQueryBuilder withinExplicitTimeout(long time, TimeUnit unit);

	/**
	 * Executes the query. If successful the method simple returns control to the caller, otherwise it will throw an exception.
	 * 
	 * @throws IllegalStateException if the builder was not constructed correctly or the query failed
	 */
	public void executeOrFail();

	/**
	 * Executes the query.
	 * 
	 * @return <code>true</code> if successful, <code>false</code> otherwise
	 * 
	 * @throws IllegalStateException
	 *             if the builder was not constructed correctly
	 */
	public boolean execute();
}
