package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

/**
 * Used when for example, performing searches for elements to determine how many
 * are required for the search. This should only be used for 
 * {@link PageValidator} annotations, otherwise all instances should be replaced
 * with the appropriate {@link ElementsQtyExpected} instance. 
 * 
 */
public enum ElementQuantity
{
	/**
	 * Expects exactly one element.
	 */
	EXACTLY_ONE,

	/**
	 * Expects one element, but there can be more than one returned by the
	 * search.
	 */
	ONE_OR_MORE,
	
	/**
	 * Expects to get all elements.
	 */
	ALL;
}
