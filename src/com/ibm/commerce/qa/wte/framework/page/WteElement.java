package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import com.ibm.commerce.qa.wte.framework.util.ConfigManager;
import com.ibm.commerce.qa.wte.framework.util.WteConfigManager;

/**
 * Represents an element on a web page. It is expected that the element is
 * present (and visible for some methods).
 * 
 * @see WteConfigManager#getImplicitTimeoutSeconds()
 */
/**
 * @author paresh
 *
 */
public interface WteElement
{
	/**
	 * Get the name of the element. (i.e. the tag name)
	 * 
	 * @return the name
	 */
	public String getName();

	/**
	 * Performs the same action as {@link #getRawText()} except the text is
	 * normalized. Normalization means following the XPath fn:normalize-space
	 * function. It removes leading and trailing spaces from the specified
	 * string, and replaces all internal sequences of white space with one and
	 * returns the result.
	 * 
	 * @return the text
	 * @see WteElement#getRawText()
	 */
	public String getText();

	/**
	 * Returns the text between the opening and closing element tags. Although
	 * this should generally only be used if the element is visible, it will
	 * make a best attempt at fetching the value even when the element is
	 * hidden. Any extra whitespace around or within the text is preserved.
	 * 
	 * @return the normalized text
	 */
	public String getRawText();

	/**
	 * Identifies if the element is visible.
	 * 
	 * @return <code>true</code> if the element is visible, <code>false</code>
	 *         otherwise
	 */
	public boolean isVisible();

	/**
	 * Identifies if the element is enabled.
	 * 
	 * @return <code>true</code> if the element is enabled, <code>false</code>
	 *         otherwise
	 */
	public boolean isEnabled();

	/**
	 * Identifies if the element is selected, like in a list or drop-down box.
	 * 
	 * @return <code>true</code> if the element is selected, <code>false</code>
	 *         otherwise
	 */
	public boolean isSelected();

	/**
	 * Identifies if the element is stale. Staleness occurs once this element
	 * has been removed from the web page's document object model (DOM). Every
	 * instance of an element is unique, so even if an element reappears in the
	 * DOM with the exact same values, it is still considered different and thus
	 * this element remains stale.
	 * <p>
	 * This method is useful in the event an action is performed on a web page,
	 * and the expectation is that you remain on the same web page. For example,
	 * get an element before the action, perform the action, and then test the
	 * original element for staleness afterwards. If it is stale, the particular
	 * instance of the element is no longer in the DOM.
	 * 
	 * @return <code>true</code> if the element is stale, <code>false</code>
	 *         otherwise
	 */
	public boolean isStale();

	/**
	 * Attempts to move the focus to this element. Note that it <b>does not</b>
	 * actually move the mouse to the element.
	 */
	public void focus();
	
	/**
	 * Performs a form submit operation.
	 */
	public void submit();

	/**
	 * Clicks on the element. A delay before clicking can be set with the CLICK
	 * DELAY MS configuration property.
	 * 
	 * @see ConfigManager#getClickDelayMilliseconds()
	 */
	public void click();
	
	/**
	 * Clicks on the element with java script click();
	 * This will click on the element even when the element is not visible. So, use with caution.
	 */
	public void clickWithJS();

	/**
	 * Clicks on the element, then waits for this element to go stale (no longer
	 * on the page). If it remains present, it will make <b>3 more attempts</b>.
	 * The wait time between attempts is based on the PAGE LOAD TIMEOUT
	 * configuration property.
	 * <p>
	 * This version of click is useful for example, when you have Javascript on
	 * a page, link, button, etc., that may prevent the click event from firing
	 * the first time as a result of the browser still processing the script(s).
	 * 
	 * @throws IllegalStateException
	 *             if the element is still present after the retrying or the
	 *             button was not visible in the first place
	 * @see ConfigManager#getPageLoadTimeoutSeconds()
	 */
	public void clickWithRetry();
	
	/**
	 * Clicks on the element, then waits for elementQuery to return true (be visible
	 * on page). If the element is not found, it will make <b>3 more click attempts</b>.
	 * The wait time between attempts is based on the PAGE LOAD TIMEOUT
	 * configuration property.
	 * <p>
	 * This version of click is useful for when clickWithRetry is not applicable
	 * because the element does not become stale. You use a query builder to verify
	 * the the click event has occurred.
	 * 
	 * @param verifyQuery
	 * 				the query builder to verify
	 * @throws IllegalStateException
	 *             if the element is still present after the retrying or the
	 *             button was not visible in the first place
	 * @see ConfigManager#getPageLoadTimeoutSeconds()
	 */
	public void clickWithVerify(IdentifierQueryBuilder verifyQuery);

	/**
	 * UPDATE: Due to issues on some browsers, this method needs to be revised.
	 * For now it will just call {@link #click()}.
	 * <p>
	 * Convenience method that will shift the {@link #focus()} to this element
	 * before performing the {@link #click()}. It has been seen, either due to
	 * issues with implementation, browsers, window size, etc., that shifting
	 * the focus is sometimes necessary before clicking. This method generally
	 * should only be used with <b>buttons</b>.
	 */
	public void focusAndClick();

	/**
	 * Assumes this is a text box of some sort and clears any characters in it.
	 */
	public void clear();

	/**
	 * Assumes this is a text box and sends the characters specified to it.
	 * 
	 * @param characters
	 *            text to appear
	 */
	public void type(CharSequence characters);

	/**
	 * A convenience method that clears the text (if any) from a text box before
	 * typing the specified characters.
	 * 
	 * @param characters
	 *            text to appear
	 */
	public void clearAndType(CharSequence characters);
	
	/**
	 * Retrieves the value of the specified element's attribute.
	 * 
	 * @param attributeName
	 *            name of the attribute
	 * @return the value of the attribute
	 */
	public String getAttributeValue(String attributeName);

	/**
	 * Assumes this is a select element and selects the option with the text
	 * specified.
	 * 
	 * @param text
	 *            the text displayed in the option
	 */
	public void selectByVisibleText(String text);

	/**
	 * Assumes this is a select element and selects the option at specified index
	 * 
	 * @param index
	 *            the 1-based index at which the option should be selected
	 */
	public void selectByIndex(int index);
	
	/**
	 * Assumes this is a select element and selects the option with the value
	 * attribute specified.
	 * 
	 * @param value
	 *            the value of the option
	 */
	public void selectByValue(String value);

	/**
	 * Assumes this is a select element and returns the first option element
	 * located.
	 * 
	 * @return an option element
	 */
	public WteElement getFirstSelectedOption();
	
	/**
	 * Will issue the MouseOver event to the element.
	 */
	public void mouseOver();
	/**
	 * A convenience method that clears the text (if any) from a text box before
	 * typing the specified characters. After typing, it verifies that the text is properly entered.
	 * fails after 4 retries
	 * @param characters
	 */
	public void clearAndTypeWithRetry(CharSequence characters);

	
	/**
	 * Should be used if the page is being scrolled on load and clicking the element fails because 
	 * of that. The function clicks at first attempt then catches not clickable exception, waits for 
	 * 500 millisecond them tries again.
	 */
	void clickWaitRetry();
	
}
