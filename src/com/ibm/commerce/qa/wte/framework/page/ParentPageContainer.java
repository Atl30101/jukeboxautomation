package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import com.google.inject.Inject;

/**
 * Used to retrieve the current parent page object. This is useful when a child
 * page (like a page section or a pop-up) needs to find out what the main parent
 * page is, for instance, when it needs to return to it after a certain button
 * is clicked (like a pop-up).
 * 
 * 
 */
public class ParentPageContainer
{
	private final PageFactory factory;
	private Object page;

	@Inject
	ParentPageContainer(PageFactory factory)
	{
		this.factory = factory;
	}

	/**
	 * Returns the current parent page object
	 * 
	 * @return the page object
	 * @throws IllegalStateException
	 *             if the parent page object for this container has not yet been
	 *             set
	 */
	protected Object getPage()
	{
		if (page == null)
		{
			throw new IllegalStateException(
					"A parent page has not yet been set.");
		}

		return this.page;
	}

	/**
	 * This method should only be used by the framework itself. Do not use this
	 * method directly in your page objects.
	 * <p>
	 * Sets the current parent page for the web session.
	 * 
	 * @param page
	 *            the non-null current parent page object
	 * @throws IllegalArgumentException
	 *             if the page object specified was null
	 */
	public void setPage(Object page)
	{
		if (page == null)
		{
			throw new IllegalArgumentException(
					"You cannot pass a null parent page.");
		}

		this.page = page;
	}

	/**
	 * This method allows you get get the original parent page object
	 * conveniently in its class casted form. In order to do this, pass in the
	 * page object class that is expected to be the parent. By specifying the
	 * expected parent page class, a check will be done to confirm that is the
	 * actual page returned to.
	 * 
	 * @param <T>
	 *            the type of the page class
	 * @param expectedParentPageClass
	 *            the non-null class of the page you expect to be on
	 * @return the original parent page object
	 * @throws IllegalArgumentException
	 *             if the expectedParentPageClass was not the same class as the
	 *             original
	 */
	public <T> T getOriginalParentPage(Class<T> expectedParentPageClass)
	{
		T result = null;
		try
		{
			result = expectedParentPageClass.cast(getPage());
		}
		catch (ClassCastException e)
		{
			throw new IllegalArgumentException(
					"The actual parent page object class is '"
							+ getPage().getClass().getName()
							+ "' while the expected page object class specified was '"
							+ expectedParentPageClass.getName() + "'.", e);
		}

		return result;
	}

	/**
	 * This method allows you get create a new instance of the original parent
	 * page object. This is useful for instance, if during a web flow you
	 * confirmed something on a child page that is supposed to reload the same parent
	 * page you are on. Pass in the page object class that is expected to be the
	 * parent. By specifying the expected parent page class, a check will be
	 * done to confirm that is the actual page returned to.
	 * 
	 * @param <T>
	 *            the type of the page class
	 * @param expectedParentPageClass
	 *            the non-null class of the page you expect to be reloaded
	 * @return a new copy of the original parent page object
	 * @throws IllegalArgumentException
	 *             if the expectedParentPageClass was not the same class as the
	 *             original
	 */
	public <T> T createParentPage(Class<T> expectedParentPageClass)
	{
		// Get the original which page to do class verification for us
		getOriginalParentPage(expectedParentPageClass);
		
		/*
		 * If we got here, the expect class matches the actual, so proceed to
		 * create page.
		 */
		
		return factory.createPage().build(expectedParentPageClass);
	}
	/**
	 * creates and builds a parent page from a pop up window
	 * @param <T>
	 * @param expectedParentPageClass
	 * @return the parent page
	 */
	public <T> T createParentPageFromPopUpWindow(Class<T> expectedParentPageClass)
	{
		// Get the original which page to do class verification for us
		getOriginalParentPage(expectedParentPageClass);
		
		/*
		 * If we got here, the expect class matches the actual, so proceed to
		 * create page.
		 */
		
		return factory.selectFirstWindow().createPage().build(expectedParentPageClass);
	}
}
