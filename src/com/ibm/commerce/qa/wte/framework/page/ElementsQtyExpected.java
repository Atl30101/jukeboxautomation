/**
 * 
 */
package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2014
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

/**
 * Used to verify that the number of elements returned from an execute on a 
 * page. 
 */
public interface ElementsQtyExpected {
	
	/**
	 * Returns true if the size received is accurate.
	 * 
	 * @param actualCount The number of elements found
	 * @return {@code true} if the correct number is found
	 */
	public boolean verifyExpected(int actualCount);
	
	/**
	 * Same call as {@link #verifyExpected(int)} but will throw an 
	 * {@link ElementCountException} if an invalid count is found. 
	 * 
	 * @param actualCount The number of elements found
	 * @throws ElementCountException 
	 * 			if {@link #verifyExpected(int)} returns {@code false}
	 * 
	 * @see #verifyExpected(int)
	 */
	public void verifyExpectedAndFail(int actualCount) 
			throws ElementCountException;
}
