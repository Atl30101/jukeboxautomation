package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

/**
 * Represents one or more element actions that were built using the
 * {@link ElementActionsBuilder}.
 */
public interface ElementCompositeAction
{
	/**
	 * Returns the number of actions contained in this composite.
	 * 
	 * @return the total number of actions
	 */
	public int getNumberOfActions();

	/**
	 * Executes the actions contained in this composite sequentially.
	 */
	public void perform();
}
