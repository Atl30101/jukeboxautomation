package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

/**
 * An enumerated list of view ports used in the framework.
 * 
 * @deprecated This should not have been defined in the framework as view port
 *             types are arbitrary. Instead create a string constants class for
 *             your page objects that defines all the view ports supported.
 */
@Deprecated
public enum ViewPortType
{
	/**
	 *Mobile view port
	 */
	MOBILE,
	
	/**
	 * tablet view port
	 */
	TABLET,
	
	/**
	 * desktop view port
	 */
	DESKTOP,
}
