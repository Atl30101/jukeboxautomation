package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

/**
 * An enumerated list of browsers used in the framework.
 *
 */
public enum BrowserType
{
	/**
	 * A unit test "browser" that can traverse pages but does not execute
	 * Javascript.
	 */
	HTML_UNIT,
	
	/**
	 * The Firefox browser. 
	 */
	FIREFOX,
	
	/**
	 * The Microsoft Internet Explorer browser.
	 */
	INTERNET_EXPLORER,
	
	/**
	 * The Google Chrome browser.
	 */
	CHROME,
	
	/**
	 * The Safari browser.
	 */
	SAFARAI,
	
	/**
	 * The Opera browser.
	 */
	OPERA,
	
	/**
	 * The Android browser.
	 */
	IPHONE,
	/**
	 * The iPhoe browser.
	 */
	ANDROID,
	/**
	 * The Unknown browser type.
	 */
	UNKNOWN;
}
