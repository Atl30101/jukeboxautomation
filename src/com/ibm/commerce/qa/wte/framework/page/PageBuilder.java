package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2015
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

/**
 * A builder used to create a new page object. It is created via the
 * {@link PageFactory}.
 */
public interface PageBuilder
{
	/**
	 * Instantiates the page object from the page class specified. Validation is
	 * done to ensure it is annotated with {@link PageObject}. Any {@link Identifier}
	 * fields or methods annotated with {@link PageValidator} will be
	 * executed to verify the page loaded successfully.
	 * 
	 * @param <T>
	 *            the page type
	 * @param pageClass
	 *            the page class
	 * @return the page object
	 */
	public <T> T build(Class<T> pageClass);

	/**
	 * Performs the same task as {@link #build(Class)}, except it identifies
	 * that a new window has opened for the page.
	 * 
	 * @param <T>
	 *            the page type
	 * @param pageClass
	 *            the page class
	 * @return the page object
	 */
	public <T> T buildAsNewWindow(Class<T> pageClass);
	
	/**
	 * <p>Set the page factory to a specific one (usually the parent page factory for locating the element)
	 * This is mainly for build the page section relative to the parent page, where parent page factory
	 * is used to locate the page section</p>
	 * 
	 * <p>Tester should never call this method</p>
	 * 
	 * @param factory
	 * @return this page builder
	 */
	public PageBuilder withPageFactory(PageFactory factory);
	
	
	/**
	 * To specify the index in case the page has the same section multiple times
	 * @param i for 0-based index
	 * @return this page builder
	 */
	public PageBuilder withIndex(int i);
}
