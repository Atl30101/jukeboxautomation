package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used to identify page object classes. If a type is not specified, it's
 * assumed the page object is the main window. Otherwise use the type to specify
 * different page objects like sections, new windows and frames.
 * 
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface PageObject
{
	/**
	 * Identifies the page object type. 
	 *
	 */
	public enum PageType
	{
		/**
		 * A page object that represents the main web page. This is the default.
		 */
		MAIN_WINDOW,

		/**
		 * A page object that represents a logical division of the main web page
		 * that is not actually in a separate frame or window. This is mainly
		 * done to keep code modular and reuable. Examples include: a header,
		 * footer, a javascript pop-up (that is non-modal).
		 */
		SECTION,
		
		/**
		 * A page object that represents a Widget as used by page composer CMC tool.
		 * This feature has been added in FEP7  Examples include: a header,
		 * footer, Catalog Entry List Widget. This is not functionally different than a section
		 */
		WIDGET,

		/**
		 * A page object that is similar to a child but instead of just a
		 * logical division, this is actually a separate HTML frame or iframe
		 * in the web page.
		 */
		FRAME,

		/**
		 * A page object that represents a new window that spawns from another
		 * web page. This is basically a pop-up that is a completely new browser
		 * window.
		 */
		NEW_WINDOW,

		/**
		 * A page object that represents a web alert window. Such windows are
		 * called "modal", which basically means you cannot click on any other
		 * web page until this window is closed.
		 */
		ALERT
	}

	/**
	 * Identifies the type of web page object this is.
	 * @return the page type
	 */
	PageType type() default PageType.MAIN_WINDOW;

//	/**
//	 * If this page object represents a frame, you need to identify the frame by
//	 * its name or id attribute. The name attribute takes precedence.
//	 * 
//	 * @return the frame name or id
//	 */
//	String frameNameOrId() default "";

//	/**
//	 * If <code>true</code>, before any method on the page object is called, the
//	 * corresponding web page will get the focus. This eliminates the need to
//	 * switch to the page manually before working with elements on the page.
//	 * 
//	 * @return is auto-focus enabled
//	 */
//	boolean autoFocus() default true;
}
