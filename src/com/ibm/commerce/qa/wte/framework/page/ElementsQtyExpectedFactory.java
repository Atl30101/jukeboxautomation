/**
 * 
 */
package com.ibm.commerce.qa.wte.framework.page;

import com.google.inject.assistedinject.Assisted;
import com.google.inject.name.Named;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2014
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

/**
 * Guice-implemented factory. This will create new instances of 
 * {@link ElementsQtyExpected} based on parameters and methods called.
 *
 */
public interface ElementsQtyExpectedFactory {
	
	/**
	 * Creates an {@link ElementsQtyExpected} which will verify if and only if
	 * the exact amount is found. 
	 * @param amount Amount must match
	 * @return new instance
	 */
	@Named("exactly")
	public ElementsQtyExpected exactly(int amount);
	
	/**
	 * Creates an {@link ElementsQtyExpected} which will verify if and only if
	 * the exact amount is one.
	 * @return new instance
	 */
	@Named("exactlyOne")
	public ElementsQtyExpected exactlyOne();
	
	/**
	 * Creates an {@link ElementsQtyExpected} which will verify if and only if
	 * no elements are found.
	 * @return new instance
	 */
	@Named("none")
	public ElementsQtyExpected none();
	
	/**
	 * Creates an {@link ElementsQtyExpected} which will always verify 
	 * {@code true}. 
	 * @return new instance
	 */
	@Named("all")
	public ElementsQtyExpected all();
	
	/**
	 * Creates an {@link ElementsQtyExpected} which will verify if and only if
	 * the actual is more than the amount passed, i.e. {@code actual > amount}.
	 * @param amount value actual must be more than.
	 * @return new instance
	 */
	@Named("moreThan")
	public ElementsQtyExpected moreThan(int amount);
	
	/**
	 * Creates an {@link ElementsQtyExpected} which will verify if and only if
	 * the actual is less than the amount passed, i.e. {@code actual < amount}.
	 * @param amount value actual must be less than.
	 * @return new instance
	 */
	@Named("lessThan")
	public ElementsQtyExpected lessThan(int amount);
	
	/**
	 * Creates an {@link ElementsQtyExpected} which will verify if and only if
	 * the actual is less than the amount passed, i.e. {@code minimum < actual < maximum}.
	 * @param minimum value actual must be greater than
	 * @param maximum value actual must be less than
	 * @return new instance
	 */
	@Named("within")
	public ElementsQtyExpected within(
				@Assisted("min") int minimum, 
				@Assisted("max") int maximum);
	
	
	
}
