package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import com.ibm.commerce.qa.wte.framework.util.KeyboardKeys;

/**
 * Used to build a sequence of actions related to elements on a web page.
 * 
 */
public interface ElementActionsBuilder
{
	/**
	 * Clicks at the current mouse location.
	 * 
	 * @return the builder
	 */
	public ElementActionsBuilder click();

	/**
	 * Click on the element specified
	 * 
	 * @param element
	 * @return the builder
	 */
	public ElementActionsBuilder click(WteElement element);

	/**
	 * clicks and hold at current mouse location
	 * @return the builder
	 */
	public ElementActionsBuilder clickAndHold();
	/**
	 * click and hold on the element specified
	 * @param onElement
	 * @return the builder
	 */
	public ElementActionsBuilder clickAndHold(WteElement onElement);
	/**
	 * Performs a contextClick on the element specified
	 * @param onElement
	 * @return  the builder
	 */
	public ElementActionsBuilder contextClick(WteElement onElement);
	/**
	 * double click at mouse location
	 * @return the builder
	 */
	public ElementActionsBuilder doubleClick();
	/**
	 * double click on the element specified
	 * @param onElement
	 * @return the builder
	 */
	public ElementActionsBuilder doubleClick(WteElement onElement);
	/**
	 * drag and drop from source element to target element
	 * @param source
	 * @param target
	 * @return the builder
	 */
	public ElementActionsBuilder dragAndDrop(WteElement source, WteElement target);
	/**
	 * drag and drop using coordinate values starting from the specified element as a reference point
	 * @param source
	 * @param offset
	 * @param offset2
	 * @return the builder
	 */
	public ElementActionsBuilder dragAndDropBy(WteElement source, int offset, int offset2);
	/**
	 * press a keyboard key down
	 * @param theKey
	 * @return the builder
	 */
	public ElementActionsBuilder keyDown(KeyboardKeys theKey);
	/**
	 * press a keyboard key down at element specified
	 * @param element
	 * @param theKey
	 * @return the builder
	 */
	public ElementActionsBuilder keyDown(WteElement element, KeyboardKeys theKey);
	/**
	 * press a keyboard key up
	 * @param theKey
	 * @return the builder
	 */
	public ElementActionsBuilder keyUp(KeyboardKeys theKey);
	/**
	 * press a keyboard key up at element specified
	 * @param element
	 * @param theKey
	 * @return the builder
	 */
	public ElementActionsBuilder keyUp(WteElement element, KeyboardKeys theKey);
	/**
	 * move the mouse by specified offset
	 * @param offset
	 * @param offset2
	 * @return the builder
	 */
	public ElementActionsBuilder moveByOffset(int offset, int offset2);
	/**
	 * move the mouse the the specified element, with an x,y offset
	 * @param toElement
	 * @param offset
	 * @param offset2
	 * @return the builder
	 */
	public ElementActionsBuilder moveToElement(WteElement toElement, int offset, int offset2);
	/**
	 * move the mouse to the specified element
	 * @param toElement
	 * @return the builder
	 */
	public ElementActionsBuilder moveToElement(WteElement toElement);
	/**
	 * release the mouse at current location
	 * @return the builder
	 */
	public ElementActionsBuilder release();
	/**
	 * release the mouse on the specified element
	 * @param onElement
	 * @return the builder
	 */
	public ElementActionsBuilder release(WteElement onElement);
	/**
	 * send a sequence of keys
	 * @param keysToSend
	 * @return the builder
	 */
	public ElementActionsBuilder sendKeys(CharSequence... keysToSend);
	/**
	 * send a sequence of keys on the specified element
	 * @param element
	 * @param keysToSend
	 * @return the builder
	 */
	public ElementActionsBuilder sendKeys(WteElement element, CharSequence... keysToSend);

	/**
	 * Create a composite action containing all the actions that have been built
	 * so far. Once this call is made, this builder is reset (contains no
	 * actions). This allows you to re-use this builder and allows you to call
	 * the composite action that has been built multiple times if needed.
	 * 
	 * @return  the builder
	 */
	public ElementCompositeAction build();

	/**
	 * A convenience method that first calls {@link #build()} followed by
	 * {@link ElementCompositeAction#perform()}. This will reset the builder.
	 */
	public void perform();
}