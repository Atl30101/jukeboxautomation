package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

/**
 * Used by implementations of the framework to determine what implementation of
 * {@link Identifier} was used.
 */
public interface IdentifierVisitor
{
	/**
	 * Visited the element ID identifier
	 * 
	 * @param byId
	 *            the element ID identifier
	 */
	public void accept(Identifier.ById byId);

	/**
	 * Visited the element class identifier
	 * 
	 * @param byClassName
	 *            the element class identifier
	 */
	public void accept(Identifier.ByClassName byClassName);

	/**
	 * Visited the link text identifier
	 * 
	 * @param byLinkText
	 *            the link element text identifier
	 */
	public void accept(Identifier.ByLinkText byLinkText);
	
	/**
	 * Visited the partial link text identifier
	 * 
	 * @param byPartialLinkText
	 *            the partial link element text identifier
	 */
	public void accept(Identifier.ByPartialLinkText byPartialLinkText);

	/**
	 * Visited the XPath identifier
	 * 
	 * @param byXPath
	 *            the XPath identifier
	 */
	public void accept(Identifier.ByXPath byXPath);

	/**
	 * Visited the CSS selector identifier
	 * 
	 * @param byCssSelector
	 *            the CSS selector identifier
	 */
	public void accept(Identifier.ByCssSelector byCssSelector);

	/**
	 * Visited the element (tag) name identifier
	 * 
	 * @param byName
	 *            the element (tag) name identifier
	 */
	public void accept(Identifier.ByName byName);
	
	/**
	 * Visited the element of AugularJs ng-model
	 * @param byModel
	 */
	public void accept(Identifier.ByModel byModel);
	
	/**
	 * Visited the element of AugularJs ng-bind or {{expression}} by partial match
	 * @param byBinding
	 */
	public void accept(Identifier.ByBinding byBinding);
	
	/**
	 * Visited the element of AugularJs ng-bind or {{expression}} by exact match
	 * @param byBinding
	 */
	public void accept(Identifier.ByExactBinding byBinding);
	
	/**
	 * Visited the element of AugularJs ng-repeat or ng-repeat-start
	 * @param byRepeater
	 */
	public void accept(Identifier.ByRepeater byRepeater);
}
