package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2015
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Logger;

import com.google.inject.Inject;
import com.google.inject.TypeLiteral;
import com.google.inject.matcher.AbstractMatcher;
import com.google.inject.spi.InjectionListener;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;

/**
 * Different implementations of the framework would extend this listener in
 * order to process page validators and set up the parent page container object.
 * 
 */
public abstract class AbstractPageListener implements TypeListener
{
	/**
	 * Matcher used to identify page objects.
	 *
	 */
	public static class PageMatcher extends AbstractMatcher<TypeLiteral<?>>
	{
		@Inject 
		private Logger log;
		/**
		 * @param type to be matched
		 * @return boolean
		 * 
		 */
		@Override
		public boolean matches(TypeLiteral<?> type)
		{
			boolean result = false;
			if (type.getRawType() != null && type.getRawType().isAnnotationPresent(PageObject.class))
			{
				/*
				 * If the log field is still null, it means this is the initial
				 * time the matcher is used, which is not for an actual page
				 * object, but because the Guice listener was initialized. So
				 * simply ignore this case.
				 */
				if (log != null)
				{
					log.fine("Identified '" + type.getRawType().getName()
							+ "' as a parent page object.");
				}
				result = true;
			} 
			
			return result;
		}
	}

	/**
	 * 
	 */
	public AbstractPageListener()
	{
	}

	@Override
	public final <I> void hear(TypeLiteral<I> type, TypeEncounter<I> encounter)
	{
		encounter.register(createInjectionListener(encounter));
	}

	/**
	 * This is where all the work is done. A list of fields and methods that
	 * were annotated with {@link PageValidator} are provided, as well as the
	 * encounter object, that gives you access to Google Guice managed objects.
	 * <p>
	 * Along with doing page validation, you need to set up the
	 * {@link ParentPageContainer} object.
	 * <p>
	 * Note: If you want to retrieve any Guice objects, fetch the providers from
	 * the encounter objects, and store them in fields. DO NOT try and get
	 * instances from the providers until you are actually in the
	 * <code>afterInjection</code> method that you must implement.
	 * 
	 * @param <I>
	 * @param encounter
	 * @param pageValidatorFields
	 * @param pageValidatorMethods
	 * @return the listener
	 */
	protected abstract <I> InjectionListener<I> createInjectionListener(
			final TypeEncounter<I> encounter);
}
