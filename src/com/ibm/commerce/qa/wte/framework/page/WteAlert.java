package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

/**
 * Represents a JS alert popup on a web page. It is expected that alert is visible
 */
public interface WteAlert {
	/**
	 * accepts and close the alert
	 */
	public void accept();
	/**
	 * dismiss and close the alert
	 */
	public void dismiss();
	/**
	 * get text of alert message
	 * @return alert text
	 */
	public String getText();
	/**
	 * send keyboard keys to alert pop up
	 * @param keysToSend
	 */
	public void sendKeys(String keysToSend);

}
