package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.List;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Supplier;
import com.ibm.commerce.qa.wte.framework.util.WteConfigManager;

/**
 * Used to construct a query for locating elements on a page.
 * 
 */
public interface IdentifierQueryBuilder
{
	/**
	 * The element to query based on the identifier specified.
	 * 
	 * @param identifier
	 *            the element identifier
	 * @return {@code this} builder
	 */
	public IdentifierQueryBuilder of(Identifier identifier);

	/**
	 * Specifies the element must be visible. This is the default.
	 * 
	 * @return {@code this} builder
	 */
	public IdentifierQueryBuilder asVisible();

	/**
	 * Specifies the element must be hidden.
	 * 
	 * @return {@code this} builder
	 */
	public IdentifierQueryBuilder asHidden();

	/**
	 * Specifies the element must simply exist.
	 * 
	 * @return {@code this} builder
	 */
	public IdentifierQueryBuilder asExisting();
	
	/**
	 * Tells the query to expect exactly {@code amount} element(s), fail 
	 * otherwise.
	 * @param amount Amount must match
	 * @return {@code this} builder
	 */
	public IdentifierQueryBuilder expectExactly(int amount);
	
	/**
	 * Tells the query to expect exactly {@code 1} element, fail 
	 * otherwise.
	 * @return {@code this} builder
	 */
	public IdentifierQueryBuilder expectExactlyOne();
	
	/**
	 * Tells the query to put no restriction on results, always pass for any 
	 * number returned. This is the default option.
	 * @return {@code this} builder
	 */
	public IdentifierQueryBuilder expectAll();
	
	/**
	 * Tells the query to find more than {@code amount} passed, 
	 * i.e. {@code actual > amount}.
	 * @param amount value actual must be more than.
	 * @return {@code this} builder
	 */
	public IdentifierQueryBuilder expectMoreThan(int amount);
	
	/**
	 * Tells the query to find less than {@code amount} passed, 
	 * i.e. {@code actual < amount}.
	 * @param amount value actual must be more than.
	 * @return {@code this} builder
	 */
	public IdentifierQueryBuilder expectLessThan(int amount);
	
	/**
	 * Tells the query to find within the max and min passed, 
	 * i.e. {@code minimum < actual < maximum}.
	 * @param minimum value actual must be greater than
	 * @param maximum value actual must be less than
	 * @return {@code this} builder
	 */
	public IdentifierQueryBuilder expectWithin(int minimum, int maximum);
	
	/**
	 * Specifies to search under a parent Identifier
	 * @param parent WteElement to search below
	 * 
	 * @return {@code this} builder
	 */
	IdentifierQueryBuilder relativeToElement(Supplier<WteElement> parent);
	
	/**
	 * Specifies to search under the element found in the parent. 
	 * <p/>
	 * The parentQuery is checked assuming it exists and it is unique (only one
	 * instance). 
	 * @param parentQuery Parent query builder
	 * @return {@code this} builder
	 */
	IdentifierQueryBuilder relativeToQueryBuilder(IdentifierQueryBuilder parentQuery);
	
	/**
	 * Creates a <strong>new</strong> query object which will have the current
	 * query as it's parent element. The new query has no other properties set.
	 * @param newIdentifier The new identifier child
	 * @return <strong>new</strong> {@link IdentifierQueryBuilder}. 
	 */
	public IdentifierQueryBuilder asParentToNewQuery(Identifier newIdentifier);
	
	/**
	 * Specifies the element must not exist.
	 * when using findExactlyOne(), it will wait for a full timeout or until the element dones't appear, whatever is first,
	 * if the timeout passed on the element still exists, it will throw an error.
	 * find() does the same, except returning the element after waiting the timeout
	 * findAll(), will return all the elements that exist after waiting the timeout
	 * 
	 * @return {@code this} builder
	 */
	public IdentifierQueryBuilder asNonExistent();

	/**
	 * Wait for the element up to the page load timeout value (this is a
	 * configuration value).
	 * 
	 * @return {@code this} builder
	 * @see WteConfigManager#getPageLoadTimeoutSeconds()
	 */
	public IdentifierQueryBuilder withinPageLoadTimeout();

	/**
	 * Wait for the element up to the implicit timeout value (this is a
	 * configuration value). This is the default.
	 * 
	 * @return {@code this} builder
	 * @see WteConfigManager#getImplicitTimeoutSeconds()
	 */
	public IdentifierQueryBuilder withinImplicitTimeout();

	/**
	 * Wait for the element up to the script timeout value (this is a
	 * configuration value).
	 * 
	 * @return {@code this} builder
	 * @see WteConfigManager#getScriptTimeoutSeconds()
	 */
	public IdentifierQueryBuilder withinScriptTimeout();

	/**
	 * Wait for the element up to the Ajax timeout value (this is a
	 * configuration value).
	 * 
	 * @return {@code this} builder
	 * @see WteConfigManager#getAjaxTimeoutSeconds()
	 */
	public IdentifierQueryBuilder withinAjaxTimeout();

	/**
	 * Wait for the element up to the explicit timeout value specified.
	 * 
	 * @param time
	 *            the amount of time
	 * @param unit
	 *            the units of time
	 * @return {@code this} builder
	 */
	public IdentifierQueryBuilder withinExplicitTimeout(long time, TimeUnit unit);

	/**
	 * Expects to find exactly one occurrence of the element within the timeout
	 * period. If more elements are found (including in different states) this
	 * will throw an exception.
	 * <p>
	 * Note: It is recommended to use this method for all single element
	 * searches, as it will automatically throw an exception if it cannot find
	 * the element, saving the caller the extra code needed to test the result
	 * like in {@link #find()}.
	 * 
	 * @return the element
	 * @throws ElementCountException
	 *             if the query doesn't result in exactly one match
	 * @deprecated Use {@link #executeForOne()} combined with 
	 * 			{@link #expectExactlyOne()} 
	 * @since V7FEP7
	 */
	@Deprecated
	public WteElement findExactlyOne();

	/**
	 * Finds an occurrence of the element within the timeout period. If more
	 * than one is found, it will simply return the first. If elements are found
	 * in states other than the one specified, they will simply be ignored.
	 * <p>
	 * Note: It is recommended to use this method sparingly and instead use
	 * {@link #findExactlyOne()}. This method forces you to check for
	 * <code>null</code> while the other will throw an exception automatically
	 * saving you the time to check for <code>null</code>.
	 * <p>
	 * This method is most useful when fetching similar elements individually
	 * (where they have a number that increments), but you do not know how many
	 * there are. You can then test for <code>null</code> to know you reached
	 * the end of the list.
	 * 
	 * @return the element or <code>null</code> if not found
	 * @deprecated 
	 * 		There are two common usages in the past, here are the 
	 * 		equivalent usages: 
	 * 		<dl>
	 * 			<dt> Checking that a call succeeds by looking for null</dt>
	 * 			<dd> Use {@link #executeChecked()} and check the returned 
	 * 					{@code boolean}.</dd>
	 * 			<dt> Using a checked search and reacting on valid and invalid 
	 * 					results </dt>
	 * 			<dd> Use {@link #executeChecked(List)} and use the results as 
	 * 					required, the result from {@link #executeChecked(List)}
	 * 					will say if the call was successful and the {@link List}
	 * 					will contain valid values or be empty.</dd>
	 * 		</dl>
	 * @since V7FEP7
	 */
	@Deprecated
	public WteElement find();

	/**
	 * Finds all matching elements available within the timeout period. The
	 * moment at least one element is located it will return the current result.
	 * If elements are found in states other than the one specified, they will
	 * simply be ignored.
	 * <p>
	 * Note: This method <b>never returns <code>null</code></b>.
	 * 
	 * @return zero or more elements
	 * 
	 * @deprecated use {@link #execute()}, these are equivalent
	 * @since V7FEP7
	 */
	@Deprecated
	public List<WteElement> findAll();
	
	/**
	 * Executes the query on the page, filling outputBuffer with the elements
	 * found. If the query failed, it will return {@code false} and the buffer
	 * will be empty. 
	 * 
	 * @param outputBuffer buffer to fill, must not be {@code null}.
	 * @return {@code true} if successful.
	 */
	public boolean executeChecked(List<WteElement> outputBuffer);
	
	/**
	 * Executes the query on the current page, ignoring all output values but
	 * validating against the query. This is a convenience method which behaves
	 * the same as {@link #executeChecked(List)}.
	 *  
	 * @return {@code true} if successful. 
	 * @see IdentifierQueryBuilder#executeChecked(List)
	 */
	public boolean executeChecked();
	
	/**
	 * Finds all matching elements available with the given conditions. The
	 * moment at <em>least</em> one element matching the conditions is located 
	 * it will attempt to confirm conditions and return the set or throw an 
	 * exception. 
	 * 
	 * <p/>
	 * If elements are found in states other than the one specified, they will
	 * simply be ignored.
	 * 
	 * <p/>
	 * Note: This method <b>never returns {@code null}</b>.
	 * 
	 * @return Non-null (possibly empty) list of {@link WteElement}s. 
	 */
	public List<WteElement> execute();
	
	/**
	 * Finds an occurrence of the element within the timeout period. If the 
	 * expected element quantity condition allows more than one element to be
	 * returned, it will simply return the first. If elements are found
	 * in states other than the one specified, they will simply be ignored.
	 * 
	 * <p/>
	 * Note: It is recommended to use this method in conjunction with 
	 * {@link #expectExactlyOne()} to force an exact query. Other usages should
	 * be used with caution. 
	 * 
	 * <p/>
	 * This method is equivalent to calling: 
	 * <pre>{@code 
	 * List<WteElement> out = query.execute();
	 * WteElement forOne = out.get(0);
	 * }</pre>
	 *
	 * @return First {@link WteElement} from {@link #execute()}. 
	 */
	public WteElement executeForOne();
	
	/**
	 * Selects a Frame or an iFrame in a page. all future interaction are done in the frame,
	 * until main frame is selected
	 * 
	 * @return {@code this} builder
	 */
	public IdentifierQueryBuilder selectFrame();
	
}
