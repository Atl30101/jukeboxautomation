/**
 * 
 */
package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2015
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import com.google.common.base.Supplier;

/**
 * {@link PageFactory} extension which causes all queries to be relative to 
 * the root specified by 
 * {@link #setIdentifierQueriesRelativeTo(Supplier)}. 
 * 
 * @since V7FEP7
 */
public interface RelativePageFactory extends PageFactory {

	/**
	 * Creates a query the same as {@link PageFactory#createQuery(Identifier)}, 
	 * except all search operations are relative to the {@code parent} set by
	 * {@link #setIdentifierQueriesRelativeTo(Supplier)}.
	 * 
	 * @param identifier Child identifier, who is relative to the {@code parent}
	 * 		element. 
	 * @return new query build object
	 * @see PageFactory#createQuery(Identifier)
	 */
	@Override
	public IdentifierQueryBuilder createQuery(Identifier identifier);
	
	/**
	 * Tells the current factory to use the passed element as a "root" for all
	 * {@link IdentifierQueryBuilder} operations. 
	 * <p/>
	 * Note to implementers, it would be suggested to cache the result of this 
	 * query for performance gains. 
	 * @param rootQuery Query to find the root element, this should not be a 
	 * 		cached query as it causes double caching -- in theory only a space 
	 * 		loss. 
	 */
	public void setIdentifierQueriesRelativeTo(Supplier<WteElement> rootQuery);
	
	/**
	 * Gets the element that the factory is currently relative to. 
	 * @return WteElement for the root of the factory
	 */
	public WteElement getElementIdentifierQueriesRelativeTo();
	
	/**
	 * Creates an {@link IdentifierQueryBuilder} which is executed in the 
	 * context of the entire page (<strong>not</strong> relative to the root set
	 * in {@link #setIdentifierQueriesRelativeTo(Supplier)}). 
	 * 
	 * @param identifier The identifier to build from
	 * @return new builder instance
	 */
	public IdentifierQueryBuilder createPageQuery(Identifier identifier);
	
	/**
	 * <p>Creates a page object builder to create new relative page section.</p>
	 * 
	 * <p>Use this method only when to build a new page section which is relative to the current page object.</p>
	 * 
	 * <p>If the new page object is not relative to this current page object, call {@link PageFactory#createPage()} instead.</p>
	 * 
	 * @return {@link PageBuilder}
	 */
	public PageBuilder createRelativePageSection();
}
