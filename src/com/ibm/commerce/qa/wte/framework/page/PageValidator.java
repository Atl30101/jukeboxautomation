package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use this annotation on {@link Identifier} fields or any method in a page
 * object to confirm that a web page has loaded successfully. When a page object
 * is created, the fields and methods annotated with {@link PageValidator} will
 * validate the web page.
 */
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface PageValidator
{
	/**
	 * Determines the amount of time the framework will wait for an element
	 * before declaring the page load a failure. The default value is
	 * {@link ElementTimeoutType#PAGE_LOAD}.
	 * 
	 * @return the timeout type
	 */
	ElementTimeoutType timeout() default ElementTimeoutType.PAGE_LOAD;
	
	/**
	 * Identifies what state the element should be in. The default is
	 * {@link ElementState#VISIBLE}. The state is ignored when using this
	 * annotation on a method.
	 * 
	 * @return the state of the element
	 */
	ElementState state() default ElementState.VISIBLE;

	/**
	 * Determines the number of elements expected to find when used with a
	 * field. The default is {@link ElementQuantity#EXACTLY_ONE}. Valid values
	 * are {@link ElementQuantity#EXACTLY_ONE} and
	 * {@link ElementQuantity#ONE_OR_MORE}.
	 * 
	 * @return the quantity of elements expected
	 */
	ElementQuantity quantity() default ElementQuantity.EXACTLY_ONE;
	
	/**
	 * determines which breakpoint to ignore when validating a page.
	 * USed in cases where some elements of the page do not exist in different responsive view ports
	 * @return list of ignored view ports
	 * @deprecated use {@link #ignoredViewPorts()} instead as this should not have used a hard-coded enum
	 */
	@Deprecated
	ViewPortType[] ignoredBreakpoints() default {};
	
	/**
	 * Describe view ports (breakpoints) that should be ignored by this
	 * validator. This is useful in cases where some elements exist only in
	 * certain view ports.
	 * <p>
	 * It is highly recommended you create a static class of string constants
	 * with the names of the different view ports you've defined in the
	 * configuration file, and use those constants for values in this attribute.
	 * 
	 * @return list of ignored view ports
	 */
	String[] ignoredViewPorts() default {};
}
