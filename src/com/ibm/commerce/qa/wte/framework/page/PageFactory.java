package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2015
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.List;

import com.google.common.base.Optional;

/**
 * Used by page objects to locate and manipulate elements on a web page, and
 * also to create new page objects.
 */
public interface PageFactory
{
	/**
	 * Create a query to locate elements within a certain time limit given an
	 * {@link Identifier}. This provides the user the ability to specify things
	 * like the expected {@link ElementState} and a {@link ElementTimeoutType}.
	 * For instance, use this method if time is required for an AJAX call to
	 * complete before the element is available.
	 * <p>
	 * This method will automatically set the identifier in
	 * {@link IdentifierQueryBuilder#of(Identifier)}, so do not try to set this
	 * yourself.
	 * 
	 * @param identifier
	 *            the non-null element identifier
	 * @return a query build object
	 */
	public IdentifierQueryBuilder createQuery(Identifier identifier);

	/**
	 * This is a convenience method for calling {@link #createQuery(Identifier)}
	 * followed by {@link IdentifierQueryBuilder#find()}. Note that it expects
	 * the element to be <b>visible</b> and already present on the page.
	 * 
	 * @param identifier
	 *            the element identifier
	 * @return the element or <code>null</code> if not found
	 * @see IdentifierQueryBuilder#find()
	 * 
	 * @deprecated There are two common usages in the past, here are the 
	 * 		equivalent usages: 
	 * 		<dl>
	 * 			<dt> Checking that a call succeeds by looking for null</dt>
	 * 			<dd> Use {@link #executeChecked(Identifier)} and check the returned 
	 * 					{@code boolean}.</dd>
	 * 			<dt> Using a checked search and reacting on valid and invalid 
	 * 					results </dt>
	 * 			<dd> Do not use this mechanism, create a new 
	 * 					{@link IdentifierQueryBuilder} and use 
	 * 					{@link IdentifierQueryBuilder#executeChecked(List)} the
	 * 					List reference to work with results.</dd>
	 * 		</dl>
	 */
	@Deprecated
	public WteElement find(Identifier identifier);
	
	/**
	 * This is a convenience method for calling {@link #createQuery(Identifier)}
	 * followed by {@link IdentifierQueryBuilder#findExactlyOne()}. Note that it
	 * expects the element to be <b>visible</b> and already present on the page.
	 * 
	 * @param identifier
	 *            the element identifier
	 * @return the element
	 * @throws ElementCountException
	 *             if the query doesn't result in exactly one match
	 * @see IdentifierQueryBuilder#findExactlyOne()
	 * 
	 * @deprecated
	 * 			Use {@link #executeForOne(Identifier)} as a similar method, but
	 * 			ideally use a full query and specify 
	 * 			{@link IdentifierQueryBuilder#expectExactlyOne()} and 
	 * 			use {@link IdentifierQueryBuilder#executeForOne()}
	 */
	@Deprecated
	public WteElement findExactlyOne(Identifier identifier);

	/**
	 * This is a convenience method for calling {@link #createQuery(Identifier)}
	 * followed by {@link IdentifierQueryBuilder#findAll()}. Note that it
	 * expects the element to be <b>visible</b> and already present on the page.
	 * 
	 * @param identifier
	 *            the element identifier
	 * @return zero or more elements
	 * @see IdentifierQueryBuilder#findAll()
	 * 
	 * @deprecated
	 * 			Use {@link #execute(Identifier)}
	 */
	@Deprecated
	public List<WteElement> findAll(Identifier identifier);
	
	/**
	 * This is a convenience method for calling {@link #createQuery(Identifier)}
	 * followed by {@link IdentifierQueryBuilder#executeForOne()}. Note that it 
	 * expects the element to be <b>visible</b>, present, and have at least one
	 * on the page.
	 * @param identifier The Element identifier
	 * @return A non-{@code null} element. 
	 * @see IdentifierQueryBuilder#executeForOne()
	 */
	public WteElement executeForOne(Identifier identifier);
	
	/**
	 * This is a convenience method for calling {@link #createQuery(Identifier)}
	 * followed by {@link IdentifierQueryBuilder#executeChecked()}. Note that it 
	 * expects the element to be <b>visible</b>, present, and have at least one
	 * on the page.
	 * @param identifier Element(s) identifier
	 * @return {@code true} if successful. 
	 * @see IdentifierQueryBuilder#executeChecked()
	 */
	public boolean executeChecked(Identifier identifier);
	
	/**
	 * This is a convenience method for calling {@link #createQuery(Identifier)}
	 * followed by {@link IdentifierQueryBuilder#execute()}. Note that it
	 * expects the element(s) to be <b>visible</b> and already present on the 
	 * page.
	 * 
	 * @param identifier
	 *            the element identifier
	 * @return zero or more elements
	 * @see IdentifierQueryBuilder#execute()
	 */
	public List<WteElement> execute(Identifier identifier);

	/**
	 * Create a query on the element specified. This provides the user the
	 * ability to specify things like the {@link ElementTimeoutType}. For
	 * instance, use this method if you want to wait for an element to go stale.
	 * <p>
	 * This method will automatically set the element in
	 * {@link ElementQueryBuilder#of(WteElement)}, so do not try to set this
	 * yourself.
	 * 
	 * @param element
	 *            the element to work with
	 * @return the builder
	 */
	public ElementQueryBuilder createQuery(WteElement element); 
	
	/**
	 * Creates a builder used to construct a sequence of actions to perform
	 * against elements on a page.
	 * 
	 * @return the builder
	 */
	public ElementActionsBuilder createActionSequence();

	/**
	 * Creates a page object builder. It is used to create new page objects.
	 * 
	 * @return the page object builder
	 */
	public PageBuilder createPage();
	
	/**
	 * selects new pop up window based on Window name, aka Window Handler
	 * @param windowName
	 * @return the page Factory object
	 */
	public PageFactory selectWindow(String windowName);
	
	/**
	 * selects the first window created, usually the main parent window
	 * @return the page Factory object
	 */
	public PageFactory selectFirstWindow();
	
	/**
	 * selects the newest window created, usually a pop up
	 * @return the page Factory object
	 */
	public PageFactory selectNewestWindow();
	
	/**
	 * selects the default frame content, used to navigate back to the main page after selecting a frame
	 * @return the page Factory object
	 */
	public PageFactory selectDefaultContent();
	
	/**
	 * accepts and closes a popup javascript alert
	 * @return the page Factory object
	 */
	public WteAlert getAlert();

	/**
	 * Returns the handle of the web page window that currently has the focus.
	 * 
	 * @return the handle
	 */
	public String getWindowHandle();

	/**
	 * Returns the title of the web page window that currently has the focus.
	 * 
	 * @return the title
	 */
	public String getTitle();

	/**
	 * Returns the URL of the web page window that currently has the focus.
	 * 
	 * @return the url
	 */
	public String getUrl();

	/**
	 * Returns the web page source code. This is not a recommended way to deal
	 * with valid web page content. However, it may be useful for retrieving
	 * pages that are XML-like but may not necessarily start with the HTML
	 * element.
	 * <p>
	 * Note: Since this stores the source code in memory, ensure the page size
	 * is reasonable, otherwise you may run out of memory.
	 * 
	 * @return the source code
	 */
	public String getPageSource();
	
	/**
	 * 
	 * @param windowHandle
	 */
	public void confirmWindowClosed(String windowHandle);
	
	/**
	 * method to get Responsive web design port names
	 * The method uses the port names as defined in config.properties and the width of the current window
	 * @return the view port name
	 * @deprecated use {@link #getViewPort()} instead as this should not have used a hard-coded enum
	 */
	@Deprecated
	public ViewPortType getViewPortName();

	/**
	 * Returns the view name associated with the current window page width as
	 * defined in the configuration.
	 * 
	 * @return the name associated to that width if one is found
	 */
	public Optional<String> getViewPort();

}
