package com.ibm.commerce.qa.wte.framework.page;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

/**
 * The state of an element on the page.
 */
public enum ElementState
{
	/**
	 * The element exists.
	 */
	EXISTS,

	/**
	 * The element is hidden.
	 */
	HIDDEN,

	/**
	 * The element is visible.
	 */
	VISIBLE,
	
	/**
	 * The element doesn't exist.
	 */
	NON_EXISTING,
	
	/**
	 * The element is stale, i.e. either it has been removed from DOM, or no longer attached to the DOM
	 */
	STALE;
}
