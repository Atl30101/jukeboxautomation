package com.ibm.commerce.qa.wte.framework.page;

import static com.google.common.base.Preconditions.checkState;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

/**
 * Thrown when the actual number of elements found does not match the number of
 * elements expected.
 */
public class ElementCountException extends RuntimeException
{
	private final int actual;
	private int expected = -1;
	private ElementsQtyExpected expectedQty = null;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6530287170159804770L;

	/**
	 * @param actual elements found
	 * @param expected Condition expected
	 * @param message exception message
	 */
	public ElementCountException(int actual, ElementsQtyExpected expected, 
			String message) {
		super(message +  " - Actual elements: " + actual + ", expected: " + expected);
		this.actual = actual;
		this.expectedQty = expected;
	}
	
	/**
	 * @param actual elements found
	 * @param expected elements expected
	 * @param message exception message
	 */
	public ElementCountException(int actual, int expected, String message)
	{
		super(message + " - Actual elements: " + actual + ", expected: " + expected);
		this.actual = actual;
		this.expected = expected;
	}

	/**
	 * @param actual elements found
	 * @param expected elements expected
	 * @param cause the original exception
	 */
	public ElementCountException(int actual, int expected, Throwable cause)
	{
		super("Actual elements: " + actual + ", expected: " + expected, cause);
		this.actual = actual;
		this.expected = expected;
	}

	/**
	 * @param actual elements found
	 * @param expected elements expected
	 * @param message exception message
	 * @param cause the original exception
	 */
	public ElementCountException(int actual, int expected, String message, Throwable cause)
	{
		super(message + " - Actual elements: " + actual + ", expected: " + expected, cause);
		this.actual = actual;
		this.expected = expected;
	}

	/**
	 * @param actual elements found
	 * @param expected elements expected
	 * @param anyStateActual 
	 * @param message exception message
	 */
	public ElementCountException(int actual, ElementsQtyExpected expected, int anyStateActual, String message)
	{
		super(message + " - Actual elements: " + actual + ", Actual elements in any state: " + anyStateActual 
				+ ", expected: " + expected);
		this.actual = actual;
		this.expectedQty = expected;
	}
	/**
	 * @return the actual
	 */
	public int getActual()
	{
		return actual;
	}

	/**
	 * @return the expected
	 */
	public int getExpected()
	{
		checkState(expected >= 0, 
				"Integer expected value not set");
		return expected;
	}
	
	/**
	 * ElementsQtyExpected if set
	 * @return Non-null reference
	 */
	public ElementsQtyExpected getExpectedQty() {
		checkState(expectedQty != null, "Expected Qty object not set");
		
		return expectedQty;
	}
}
