package com.ibm.commerce.qa.wte.framework.test;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

/**
 * Represents a single connection to a web browser. A session is generally
 * created once for the duration of a test case (e.g. a full JUnit class).
 * <p>
 * Implementations are not thread-safe. Web browser interactions only support
 * single-user access. 
 */
public interface WebSession
{
	/**
	 * Navigate to the specified URL. All previous browser state (cookies, etc.)
	 * is cleared first. Also, all previous windows (other than the main browser
	 * window) will also be closed.
	 * 
	 * @param <T>
	 *            the type of the page
	 * @param url
	 *            the url of the page
	 * @param pageClass
	 *            the class of the page to start at
	 * @return the page object
	 */
	public <T> T startAtPage(String url, Class<T> pageClass);

	/**
	 * Same as {@link #startAtPage(String, Class)}, except this method will also
	 * refresh the page after navigating to it. Only use this method on sites in
	 * which navigating to the same page again doesn't reset the state.
	 * 
	 * @param <T>
	 *            the type of the page
	 * @param url
	 *            the url of the page
	 * @param pageClass
	 *            the class of the page to start at
	 * @return the page object
	 * @see #startAtPage(String, Class)
	 */
	public <T> T startAtPageWithRefresh(String url, Class<T> pageClass);

	
	/**
	 * Navigate to the specified URL, <b>maintaining all previous browser
	 * state</b> such as cookies.
	 * <p>
	 * This method will probably be rarely used, as chances are you do not want
	 * to maintain state between tests.
	 * 
	 * @param <T>
	 *            the type of the page
	 * @param url
	 *            the url of the page
	 * @param pageClass
	 *            the class of the page you want to go to
	 * @return the page object
	 */
	public <T> T continueToPage(String url, Class<T> pageClass);

	/**
	 * Move back one step in the browser's history.
	 * 
	 * @param <T>
	 *            the type of the page
	 * @param pageClass
	 *            the class of the page object you expect to be at
	 * @return a new page object
	 */
	public <T> T goBack(Class<T> pageClass);
	
	/**
	 * Move forward one step in the browser's history.
	 * 
	 * @param <T>
	 *            the type of the page
	 * @param pageClass
	 *            the class of the page object you expect to be at
	 * @return a new page object
	 */
	public <T> T goForward(Class<T> pageClass);
	
	/**
	 * Refresh/reload the current page in the web browser.
	 * 
	 * @param <T>
	 *            the type of the page
	 * @param pageClass
	 *            the class of the page object you expect to be at
	 * @return a new page object
	 */
	public <T> T refresh(Class<T> pageClass);

	/**
	 * Refresh/reload the store preview page in the web browser.
	 * 
	 * @param <T>
	 *            the type of the page
	 * @param pageClass
	 *            the class of the page object you expect to be at
	 * @return a new page object
	 */
	
	public <T> T refreshStorePreviewPage(Class<T> pageClass);	
	
	/**
	 * Navigate to the specified store previewURL. All previous browser state (cookies, etc.)
	 * is cleared first.
	 * The store preview frame is selected, and than page object verification is used.
	 * @param <T>
	 *            the type of the page
	 * @param url
	 *            the url of the page
	 * @param pageClass
	 *            the class of the page to start at
	 * @return the page object
	 */
	public <T> T startAtStorePreviewPage(String url, Class<T> pageClass);

	/**
	 * checks for the STATICHTMLCAPTURE boolean to determine whether static html capture should occur.
	 * if set to true, it Generates the HTML of the current page and saves it in the given directory
	 * the filename is the title of the web page with a counter to avoid overwriting existing captures.
	 */
	public void captureHTML();
	
	/**
	 * checks for SCREENSHOTONSTEP boolean if the framework should capture the page after every step,
	 * saves it to path defined in SCREENSHOTPATH, as screenshot-<timestamp>.png
	 * @param testCaseName 
	 */
	public void takeScreenshotOnStep(String testCaseName);
	/**
	 * takes a screenshot when test case is failing
	 * @param testCaseName name of test case
	 */
	public void takeScreenshotOnFail(String testCaseName);
	
	/**
	 * returns an instance of the {@link State} class which holds methods 
	 * associated with the WebSession state. 
	 * @return State instance
	 */
	public State state();
	
	/**
	 * Holds information regarding the state of the current WebSession
	 * 
	 * @since Apr 21, 2014
	 *
	 */
	public interface State {

		/**
		 * This will create an object that holds the state of the current browser.
		 * This includes items like cookies. This is useful across sessions to
		 * simulate closing a browser and then reopening it to see if it "remembers"
		 * you.
		 * <p>
		 * Unless otherwise noted in different implementations, expect only the
		 * cookies for the current domain to be saved. This is a limitation of some
		 * browsers.
		 * 
		 * @return the session state
		 */
		public StateData saveState();

		/**
		 * Loads the state of a previous session. This will overwrite the state of
		 * the current session. This is used for instance to restore cookies from
		 * one browser instance to the next.
		 * 
		 * @param state the session state
		 */
		public void loadState(StateData state);

		/**
		 * Ends the session. Subsequent calls to methods in this instance will do
		 * nothing.
		 */
		public void shutdown();

		/**
		 * Checks if {@link #shutdown()} was called, if so, returns {@code true}. 
		 * @return {@code true} if {@link #shutdown()} has been called.
		 */
		public boolean isClosed();

		/**
		 * Resets the current WebSession to be as if a new instance had been 
		 * created. 
		 */
		public void reset();

		/**
		 * Resets the session (as if calling {@link #reset()}), but then loads the
		 * {@code state}, as if calling {@link #loadState(StateData)}. 
		 * @param state the state to reload. 
		 * @see #reset()
		 * @see #loadState(StateData)
		 */
		public void resetWithState(StateData state);

		/**
		 * Resets the underlying session, but then will reset the 
		 * {@link StateData} to be what it was before the reset. 
		 * <p/>
		 * This is a convenience for similar to: 
		 * <p/>
		 * <pre><code>
		 * 	WebSessionState currState = session.saveState();
		 * 	session.resetWithState(currState);
		 * </code></pre>
		 */
		public void resetWithCurrentState();
		
		
		/**
		 * Contains the state of a session. This is useful for instance if you want to
		 * close a session, and then open a new one with the same state. Basically this
		 * tests "remember me" scenarios where you close a browser and reopen it
		 * expecting it still to remember you.
		 * <p>
		 * Note that this interface contains no methods at all, as it follows the
		 * Memento Pattern. This means no one but the originator of the state actually
		 * knows how it is stored/used, and the caretaker simply holds on to it and
		 * gives it back to an instance of the originator.
		 * 
		 * 
		 */
		public interface StateData
		{

		}
		

	}

	/**
	 * selects a dojo widget using java script
	 * 
	 * 
	 * @param id the id of the dojo widget
	 * @param value the value to be selected
	 */
	public void dojoWidgetSelect(String name, String value);
	

}
