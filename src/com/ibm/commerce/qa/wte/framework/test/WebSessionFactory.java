package com.ibm.commerce.qa.wte.framework.test;

import java.util.Collection;
import java.util.Set;

import com.ibm.commerce.qa.util.guice.modules.ModuleResolver;

import com.google.inject.Module;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

/**
 * Produces web sessions. For general testing, only one session will ever be
 * used at a time. The only reason for multiple sessions being active
 * simultaneously is if the user wants to test with multiple browsers open and
 * active at the same time.
 * <p/>
 * This is not required, all create methods return the same Session now. 
 */
@Deprecated
public interface WebSessionFactory
{
	/**
	 * Create a web session.
	 * @return a web session
	 */
	public WebSession create();
	
	/**
	 * Creates a {@link WebSession} loading the {@link Collection} of 
	 * {@link Module}s passed. These {@link Module}s will <strong>not</strong>
	 * be run through {@link ModuleResolver#resolveDependentModules(Iterable)}, as
	 * this requires knowledge of all {@link Module} instances being loaded.
	 * 
	 * @param childModules Modules to load under the current parent modules
	 * @return new {@link WebSession} instance
	 */
	public WebSession create(Collection<Class<? extends Module>> childModules);
	
	/**
	 * Creates a new {@link ModuleSetBuilder} to add modules to, adding the 
	 * module passed by calling {@link ModuleSetBuilder#with(Class)}. 
	 * @param module Module to add
	 * @return new Builder instance
	 */
	public ModuleSetBuilder with(Class<? extends Module> module);
	
	/**
	 * Creates a simple builder for adding lots of classes easily. Typically 
	 * easier than using a Collection and adding them by hand. This interface
	 * must behave as a {@link Set}, in that only unique entries are allowed.
	 * 
	 * @since Apr 15, 2014
	 *
	 */
	public interface ModuleSetBuilder {
		
		/**
		 * Adds a module to the internal {@link Set}. These modules can not
		 * safely use the {@link ModuleResolver#resolveDependentModules(Class)} tools
		 * because they are used in a Child Injector, thus can not guarantee 
		 * their dependencies were not already loaded
		 * @param module Module type to add
		 * @return {@code this}
		 */
		public ModuleSetBuilder with(Class<? extends Module> module);
		
		/**
		 * Should be equivalent to calling 
		 * {@link WebSessionFactory#create(Collection)} with the passed values.
		 * 
		 * @return new {@link WebSession} instance
		 */
		public WebSession create();
	}
}
