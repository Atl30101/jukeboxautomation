/**
 * This package contains the Selenium 2 WebDriver implementation of the 
 * Web Test Engine framework. Because the framework is interface-driven, there
 * is no need to look at anything in this package unless you are modifying
 * the WebDriver implementation of the framework itself.
 */
package com.ibm.commerce.qa.wte.s2impl;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2011, 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

