package com.ibm.commerce.qa.wte.s2impl.util;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2015
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.logging.Logger;

import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.google.common.base.Supplier;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;
import com.ibm.commerce.qa.wte.framework.page.Identifier;
import com.ibm.commerce.qa.wte.framework.page.PageFactory;
import com.ibm.commerce.qa.wte.framework.page.PageObject;
import com.ibm.commerce.qa.wte.framework.page.PageSection;
import com.ibm.commerce.qa.wte.framework.page.RelativePageFactory;
import com.ibm.commerce.qa.wte.framework.page.RootIdentifier;
import com.ibm.commerce.qa.wte.framework.page.WteElement;
import com.ibm.commerce.qa.wte.framework.util.GuiceUtil;
import com.ibm.commerce.qa.wte.framework.util.PageObjectRelativeResolver;
import com.ibm.commerce.qa.wte.framework.util.PageValidatorPerformer;
import com.ibm.commerce.qa.wte.framework.util.WteAnnotationUtil;

public class Selenium2PageObjectRelativeResolver implements PageObjectRelativeResolver{
	private Logger log;
	
	private PageValidatorPerformer pvPerformer;
	
	@Inject
	public Selenium2PageObjectRelativeResolver(Logger p_log, PageValidatorPerformer p_pvPerformer) {
		log = p_log;
		pvPerformer = p_pvPerformer;
	}
	

	/**
	 * @Override
	 */
	public <T> T resolvePageObject(T pageSection, final PageFactory parentFactory, final int index, Injector injector) {

		Class clazz = GuiceUtil.removeProxyFromClass(pageSection.getClass());
		
		/*
		 * In order to resolve the relative page objects and do proper page validation
		 * we need to set this current page's factory relative to the right element
		 * 
		 * This page could be the starting page, and it should use the page factory (not relative one)
		 * 	and in this case, we don't have to do anything
		 * 
		 * This page could be a page section within a page, and it could not use relative page factory,
		 *  and we don't do anything either in this case
		 *  
		 * This page could be a page section within a page, and it uses relative page factory. In this case,
		 *  we are expecting either the parent page to tell exactly how to locate this section's root element,
		 *  or there could be a root element defined within the page section. We need to locate that element,
		 *  and set the relative page factory relative to this root element.
		 * 
		 */
		
		PageFactory factory = getPageFactory(pageSection);
		if (factory instanceof RelativePageFactory) {
			
			// by default set it to the html element
			Identifier rootIdent = Identifier.byXPath("/*");
			
			// if root identifier is present, set it
			List<Field> fields = WteAnnotationUtil.getFieldsByAnnotation(clazz, RootIdentifier.class);
			// we should only have 1 root identifier
			if (fields.size() == 1 
					&& Identifier.class.isAssignableFrom(fields.get(0).getType())) {
				log.info(String.format(
						"Page object %s has a root identifier defined.", clazz.getSimpleName()));
				Field rootIdentField = fields.get(0);
				rootIdentField.setAccessible(true);
				try {
					rootIdent = (Identifier)fields.get(0).get(pageSection);
				} catch (IllegalArgumentException e) {
					throw e;
				} catch (IllegalAccessException e) {
					throw new IllegalStateException(e);
				}
			} else {
				log.info(String.format(
						"Page object %s does not have a root identifier defined. Use the document root by default.", clazz.getSimpleName()));
			}
			
			final Identifier ROOT_IDENT = rootIdent;
			
			/*
			 * If the same page section appears multiple times on the page, we could have multiple root elements found
			 * find all of the root elements and get the specified one
			 */
			List<WteElement> rootElements = parentFactory.createQuery(rootIdent).withinPageLoadTimeout().asVisible().expectMoreThan(index).execute();
			
			final WteElement rootElement  = rootElements.get(index);
			
			((RelativePageFactory) factory).setIdentifierQueriesRelativeTo(
					new Supplier<WteElement> () {

						@Override
						public WteElement get() {
							// directly return already resolved element if it is not stale
							if (!rootElement.isStale()) {
								return rootElement;
							} else {
								// otherwise, re-query and return the newly resolved element
								List<WteElement> rootElements = parentFactory.createQuery(ROOT_IDENT).withinPageLoadTimeout().asVisible().execute();
								if (rootElements.size() <= index) {
									throw new IllegalArgumentException(
											String.format("There are %d web element found for rootIdentifier '%s', but index '%d' is out of bounds.", 
													rootElements.size(), ROOT_IDENT.toString(), index));
								}
								
								return rootElements.get(index);
							}
						}
					}
				);
		}
		
		// Now, with this current page's page factory properly set, we can start to resolve and do page validation
		return resolve(pageSection, injector);
	}
	
	/**
	 * Resolve and validate each page object recursively
	 * @param pageSection
	 * @param injector
	 * @return
	 */
	private <T> T resolve(T pageSection, Injector injector) {
		
		Class clazz = GuiceUtil.removeProxyFromClass(pageSection.getClass());
		
		// first of all, validate all of already injected page objects
		try {
			validateAlreadyInjectedPageObject(pageSection);
		} catch (IllegalArgumentException e1) {
			throw e1;
		} catch (IllegalAccessException e1) {
			throw new IllegalStateException(e1);
		}

		// get relative page factory
		final PageFactory factory = getPageFactory(pageSection);
		
		if (factory instanceof RelativePageFactory) {
			log.info("Class '" + clazz + "' RelativePageFactory is relative to : " + ((RelativePageFactory) factory).getElementIdentifierQueriesRelativeTo().toString());
		}
		
		
		// Get fields annotated with @PageSection
		List<Field> fields = WteAnnotationUtil.getFieldsByAnnotation(clazz, PageSection.class);
		
		// inject and validate each page section
		for (Field childSectionField : fields) {
			Class<?> sectionClass = childSectionField.getType();
			
			// Some page section could be optional, i.e. may or may not show up
			// For optional page section, the type is Optional
			boolean isOptional = false;
			if (sectionClass == Optional.class) {
				isOptional = true;
				
				ParameterizedType type = (ParameterizedType) childSectionField.getGenericType();
				List<Type> genTypes = Lists.newArrayList(type.getActualTypeArguments());
				Class<?> tClass = (Class<?>)genTypes.get(0); // will only be one
				sectionClass = tClass;
			}
			
			log.info("Resolving and injecting Page Section '" + sectionClass.getSimpleName() + "' from '" + clazz.getSimpleName() + "'");
			
			// get PageSection annotation and identifier from it
			PageSection ps = childSectionField.getAnnotation(PageSection.class);
			final String IDENTIFIER_FIELD_NAME = ps.identifier();
			
			if (Strings.isNullOrEmpty(IDENTIFIER_FIELD_NAME)) {
				throw new IllegalArgumentException(
						String.format(
								"Identifier in annotation PageSection is not defined for Field '%s' in class '%s'",
								childSectionField.getName(), 
								clazz.getSimpleName()));
			}
			
			// get the Identifier field
			final Field IDENTIFIER_FIELD;
			try {
				IDENTIFIER_FIELD = clazz.getDeclaredField(IDENTIFIER_FIELD_NAME);
			} catch (NoSuchFieldException e) {
				throw new IllegalArgumentException(
						String.format(
								"Field '%s' is not defined in class '%s'",
								IDENTIFIER_FIELD_NAME, 
								clazz.getSimpleName()));
			}
			
			IDENTIFIER_FIELD.setAccessible(true);
			final Identifier identifier;
			try {
				identifier = (Identifier)IDENTIFIER_FIELD.get(pageSection);
			} catch (IllegalArgumentException e) {
				throw e;
			} catch (IllegalAccessException e) {
				throw new IllegalStateException(e);
			}
			
			if (identifier == null) {
				throw new IllegalStateException("Identifier can not be resolved.");
			}
			
			// use Identifier to get the Wte element
			WteElement sectionRootElement = null;
			try {
				sectionRootElement = factory.createQuery(identifier).withinPageLoadTimeout().asVisible().expectExactlyOne().executeForOne();
			} catch (RuntimeException e) {
				/*
				 * if page section is optional, and absent, we can not find it, this is expected
				 * so, only throw the exception when it is not optional
				 */ 
				if (!isOptional) {
					throw e;
				}
			}
			
			if (sectionRootElement != null) {
				final WteElement ROOT_ELEMENT = sectionRootElement;
				/*
				 * calling Guice to get the instance of the page section
				 * by this time, Guice should be able to inject most of the things except:
				 * 	1. page sections annotated with @PageSection
				 *  2. RelativePageFactory's relative to element (if it uses relative page factory)
				 */
				Object childPageSection = injector.getInstance(sectionClass);
				
				// if page section's page factory is relative page factory, set it relative to the root element
				PageFactory childPageFactory = getPageFactory(childPageSection);
				if (childPageFactory instanceof RelativePageFactory) {
					((RelativePageFactory) childPageFactory).setIdentifierQueriesRelativeTo(
							new Supplier<WteElement> () {
	
								@Override
								public WteElement get() {
									// if the root element is not stale, return it directly
									if (!ROOT_ELEMENT.isStale()) {
										return ROOT_ELEMENT;
									} else {
										// otherwise, re-query the element and return it
										return factory.createQuery(identifier).withinPageLoadTimeout().asVisible().expectExactlyOne().executeForOne();
									}
								}
								
							}
						);
				}
				
				/*
				 * After the page factory is set properly, the page section is ready to resolve its sub page sections
				 * calling this same method to recursively resolve the page section
				 */
				Object object = resolve(childPageSection, injector);
				
				// Now, the page section is fully resolved, will assign it to the field
				try {
					childSectionField.setAccessible(true);
					if (isOptional) {
						childSectionField.set(pageSection, Optional.of(object));
					} else {
						childSectionField.set(pageSection, object);
					}
				} catch (IllegalArgumentException e) {
					throw e;
				} catch (IllegalAccessException e) {
					throw new IllegalStateException(e);
				}
			} else if (isOptional) {
				/* 
				 * root element is not present, and it is optional
				 * In this case, we will assign the Optional absent to the field and stop here
				 */
				childSectionField.setAccessible(true);
				try {
					childSectionField.set(pageSection, Optional.absent());
				} catch (IllegalArgumentException e) {
					throw e;
				} catch (IllegalAccessException e) {
					throw new IllegalStateException(e);
				}
			} else {
				// shouldn't be here
				throw new IllegalStateException("Root Element is not found, and the page section is not optional");
			}
		}

		// once we injected all page sections for this page object, it is time to validate it
		pvPerformer.validatePage(pageSection);
		
		
		return pageSection;
	}
	
	/**
	 * @Override
	 */
	public PageFactory getPageFactory(Object pageSection) {
		
		Class clazz = GuiceUtil.removeProxyFromClass(pageSection.getClass());
		
		PageFactory factory = null;
		while (clazz != Object.class) {
			Field[] fields = clazz.getDeclaredFields();
			for (Field f : fields) {
				if (PageFactory.class.isAssignableFrom(f.getType())) {
					try {
						f.setAccessible(true);
						factory = (PageFactory)f.get(pageSection);
					} catch (IllegalArgumentException e) {
						throw e;
					} catch (IllegalAccessException e) {
						throw new IllegalStateException(e);
					}
					break;
				}
			}
			
			clazz = clazz.getSuperclass();
		}
		
		if (factory == null) {
			throw new IllegalStateException("can not get PageFactory from class " + clazz.getSimpleName());
		}
		
		return factory;
	}
	
	/**
	 * validate already injected page object
	 * @param pageObject
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	private <T> void validateAlreadyInjectedPageObject(T pageObject) throws IllegalArgumentException, IllegalAccessException {
		
		Class clazz = GuiceUtil.removeProxyFromClass(pageObject.getClass());
		log.info("Entering to validate already injected page object from '" + clazz.getSimpleName() + "'");
		Class curClazz = clazz;
		while (curClazz != Object.class) {
			Field[] fields = clazz.getDeclaredFields();
			for (Field f : fields) {
				f.setAccessible(true);
				Object obj = f.get(pageObject);
				
				if (obj != null && GuiceUtil.removeProxyFromClass(obj.getClass()).isAnnotationPresent(PageObject.class)) {
					pvPerformer.validatePage(obj);
				} else if (obj instanceof Provider && GuiceUtil.removeProxyFromClass(((Provider)obj).get().getClass()).isAnnotationPresent(PageObject.class)) {
					pvPerformer.validatePage(((Provider)obj).get());
				} else if (obj instanceof Optional 
						&& ((Optional)obj).isPresent() 
						&& GuiceUtil.removeProxyFromClass(((Optional)obj).get().getClass()).isAnnotationPresent(PageObject.class)) {
					pvPerformer.validatePage(((Optional)obj).get());
				}
			}
			
			curClazz = curClazz.getSuperclass();
		}
		log.info("Exiting from validating already injected page object from '" + clazz.getSimpleName() + "'");
		
	}
}
