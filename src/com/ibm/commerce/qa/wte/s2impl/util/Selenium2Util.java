package com.ibm.commerce.qa.wte.s2impl.util;

/*
 *-----------------------------------------------------------------
* Licensed Materials - Property of IBM
 *
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2013
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Logger;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.WebDriver;

import com.ibm.commerce.qa.wte.framework.page.BrowserType;
import com.ibm.commerce.qa.wte.framework.util.KeyboardKeys;

/**
 * Contains any utility/helper methods needed by the Selnium implementation of
 * the WTE framework.
 * 
 */
public class Selenium2Util
{
	private static final Logger log = Logger.getLogger(Selenium2Util.class.getName());

	/**
	 * Checks for SSL Certificate error pages and attempts to bypass them. It
	 * should generally be called after any {@link WebDriver#get(String)} call.
	 * <p>
	 * Current implementation checks for the Internet Explorer SSL Certificate
	 * error page and bypasses it.
	 * 
	 * @param browserType the type of browser
	 * @param webDriver the driver
	 */
	public static void bypassSslError(BrowserType browserType, WebDriver webDriver)
	{
		/*
		 * If there is an invalid cert error for Internet explorer, bypass it.
		 * Example: res://ieframe.dll/invalidcert.htm?SSLError=33554432#https://host/webapp/wcs/stores/servlet/LogonForm?catalogId=10001&myAcctMain=1&langId=-1&storeId=10152
		 */
		
		if (browserType == BrowserType.INTERNET_EXPLORER)
		{
			try {
				/*
				 *  IE driver has a bug that it may loose connection to the browser if certificate error occurs
				 *  and we try to get the browser properties, such as current URL or title
				 *  https://code.google.com/p/selenium/issues/detail?id=6511
				 */
				webDriver.getCurrentUrl();
			} catch (NoSuchWindowException e) {
				/*
				 *  looks like refresh may gain connection back
				 *  
				 *  However, this is workaround and is only working now. It may break any time with IE update.
				 */
				webDriver.navigate().refresh();
			}
			if (webDriver.getCurrentUrl().contains("SSLError")) {
				final int MAX_CLICK = 2; // in case it is store preview, it is loading 2 pages, so need to click the link twice
				int retry = 0;
				log.info("Found Internet Explorer invalid certificate page. Attempting to bypass.");
	
				// Click on the continue/override link
				while (retry < MAX_CLICK && webDriver.getCurrentUrl().contains("SSLError")) {
					webDriver.get("javascript:document.getElementById('overridelink').click();");
					retry++;
				}
			}
		}
	}
	/**
	 * converts a KeyboardKey to a Selenium Key implementation.
	 * If Key is unknown, this utility method throws an illegalStateException
	 * @param key
	 * @return Selenium Keys
	 */
	public static Keys convertKKeysToSeleniumKeys(KeyboardKeys key)
	{
		switch(key){
			case TAB: 
				return Keys.TAB;
			case SHIFT:
				return Keys.SHIFT;
			case BACKSPACE:
				return Keys.BACK_SPACE;
			case ENTER:
				return Keys.ENTER;
			default:
				throw new IllegalArgumentException("Unknown Key used");
		}
		
	}
}
