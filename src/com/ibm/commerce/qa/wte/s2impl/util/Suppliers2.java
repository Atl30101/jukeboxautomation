/**
 * 
 */
package com.ibm.commerce.qa.wte.s2impl.util;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;

/* 
 * This code is considered fully transferable to the Guava project with no 
 * restrictions or attributions required. It may be modified, redistributed, and
 * used under the terms of the Guava project
 */

/**
 * Extension to {@link Suppliers} to add needed functions.
 *
 */
public abstract class Suppliers2 {

	private Suppliers2() {
		// don't instantiate me
	}

	/**
	 * Returns a supplier that caches the instance supplied by the delegate and 
	 * removes the cached value if the {@link Predicate} returns {@code true}.  
	 * Subsequent calls to {@code get()} return the cached value if the
	 * expiration {@link Predicate} does not return {@code true}. After the 
	 * expiration {@link Predicate}, a new value is retrieved, cached, and 
	 * returned. 
	 * 
	 * <p/>
	 * See: 
	 * <a href="http://en.wikipedia.org/wiki/Memoization">memoization</a>.
	 * 
	 * <p/>
	 * The returned supplier is thread-safe as long as the {@code expiryCheck} 
	 * is also thread-safe. The supplier's serialized form does not contain the 
	 * cached value, which will be recalculated when {@code get()} is called on 
	 * the re-serialized instance.
	 * 
	 * @param delegate 
	 * 		{@link Supplier} that is delegated to get the memoized instance.
	 * 
	 * @param expiryCheck 
	 * 		Function which returns {@code true} if the value 
	 * 		passed should be considered expired. This check must be thread-safe
	 * 		if being used in a multi-threaded environment. 
	 * @return Supplier which delegates values	
	 */
	public static <T> Supplier<T> memoizeWithExpiration(final Supplier<T> delegate, 
			final Predicate<T> expiryCheck) {
		return new PredicateExipringMemoizingSupplier<T>(delegate, expiryCheck);
	}
	
	/**
	 * Class implementation for 
	 * {@link Suppliers2#memoizeWithExpiration(Supplier, Predicate)}. 
	 * @param <T>
	 */
	static class PredicateExipringMemoizingSupplier<T> 
		implements Supplier<T>, Serializable {

		private final Supplier<T> delegate;
		private final Predicate<T> expiryCheck;
		
		private transient volatile boolean initialized = false;
		private transient volatile T value;
		
		/**
		 * Constructor which sets up Memoizing supplier
		 * @param delegate
		 * @param expiryCheck
		 */
		PredicateExipringMemoizingSupplier(Supplier<T> delegate, Predicate<T> expiryCheck) {
			this.delegate = checkNotNull(delegate);
			this.expiryCheck = checkNotNull(expiryCheck);
		}
		
		@Override
		public T get() {
			/*
			 * Use double-checked locking to run against the expiryCheck, this
			 * usage puts the restriction that expiryCheck must be thread-safe 
			 * if used in a multi-threaded environment.
			 */
			T result = value;
			if (!initialized || expiryCheck.apply(result)) {
				synchronized (this) {
					result = value;
					if (!initialized || expiryCheck.apply(result)) {
						// re-assign result/update value
						result = value = delegate.get();
						initialized = true;
					}
				}
			}
			
			return result;
		}
		
		@Override
		public String toString() {
			return "Suppliers2.memoizeWithExpiration(" + delegate + ", " + 
						expiryCheck + ")";
		}
		
		private static final long serialVersionUID = 0;
	}
}
