package com.ibm.commerce.qa.wte.s2impl.util;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2015
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import com.google.inject.Inject;
import com.ibm.commerce.qa.wte.framework.page.ElementCountException;
import com.ibm.commerce.qa.wte.framework.page.ElementQuantity;
import com.ibm.commerce.qa.wte.framework.page.ElementState;
import com.ibm.commerce.qa.wte.framework.page.ElementTimeoutType;
import com.ibm.commerce.qa.wte.framework.page.Identifier;
import com.ibm.commerce.qa.wte.framework.page.IdentifierQueryBuilder;
import com.ibm.commerce.qa.wte.framework.page.PageFactory;
import com.ibm.commerce.qa.wte.framework.page.PageValidator;
import com.ibm.commerce.qa.wte.framework.page.RelativePageFactory;
import com.ibm.commerce.qa.wte.framework.page.ViewPortType;
import com.ibm.commerce.qa.wte.framework.util.GuiceUtil;
import com.ibm.commerce.qa.wte.framework.util.PageObjectRelativeResolver;
import com.ibm.commerce.qa.wte.framework.util.PageValidatorPerformer;

public class Selenium2PageValidatorPerformer implements PageValidatorPerformer {
	private Logger log;
	
	private PageObjectRelativeResolver pageObjectResolver;
	
	@Inject
	public Selenium2PageValidatorPerformer(Logger p_log, PageObjectRelativeResolver p_pageObjectResolver) {
		log = p_log;
		pageObjectResolver = p_pageObjectResolver;
	}

	@Override
	public <I> void validatePage(I pageObject) {
		String className = GuiceUtil.removeProxyFromClassName(pageObject.getClass().getName());
		log.info("Entering validating page object '" + className + "'");
		
		PageFactory factory = pageObjectResolver.getPageFactory(pageObject);		
		
		log.info("PageObject info: {window handle='"
				+ factory.getWindowHandle() + "', url='"
				+ factory.getUrl() + "', title='" + factory.getTitle()
				+ "', relativeTo='" + ((factory instanceof RelativePageFactory) ? ((RelativePageFactory)factory).getElementIdentifierQueriesRelativeTo() : "document root (/HTML)") + "'}");
		
		List<PageLoadValidatorFieldTuple> pageValidatorFields = getPageLoadValidatorFields(pageObject);
		
		// Validate the page by finding the page load validator fields
		for (PageLoadValidatorFieldTuple fieldTuple : pageValidatorFields)
		{
			log.info("Validating field : " + fieldTuple.getField().getName());
        	String fullyQualifiedFieldName = className + "." + fieldTuple.getField().getName();

        	// Get the field value
        	Identifier elementIdent = null;
        	try
			{
				elementIdent = (Identifier)fieldTuple.getField().get(pageObject);
			}
			catch (IllegalArgumentException e)
			{
				throw new IllegalStateException("Tried to invoke page validator field '" + fullyQualifiedFieldName + "' with unexpected arguments.", e);
			}
			catch (IllegalAccessException e)
			{
				throw new IllegalStateException("Tried to invoke a page validator field '" + fullyQualifiedFieldName + "' with illegal access to its definition.", e);
			}
			
			// Get and validate the quantity expected
			ElementQuantity quantity = fieldTuple.getQuantity();
			switch (quantity)
			{
				case EXACTLY_ONE:
				case ONE_OR_MORE:
					// Do nothing as these are valid
					break;
				default:
					throw new UnsupportedOperationException("The page validator quantity of " + quantity.toString() + " is not allowed. Please fix field '" + fullyQualifiedFieldName + "'.");
			}

			/*
			 * Get the ignored view ports to see if the current view is
			 * the same as one listed in the ignored list and skip
			 * validation.
			 */
			String[] viewports = fieldTuple.getViewPorts();
			if (factory.getViewPort().isPresent()
					&& Arrays.asList(viewports).contains(factory.getViewPort().get()))
			{
				continue;
			}
			
			IdentifierQueryBuilder builder = factory.createQuery(elementIdent);
        	switch (fieldTuple.getState())
        	{
        		case EXISTS:
        			try
        			{
						builder.asExisting();
						switch(fieldTuple.getTimeoutType())
						{
							case PAGE_LOAD:
								builder.withinPageLoadTimeout();
								break;
							case AJAX:
								builder.withinAjaxTimeout();
								break;
							case SCRIPT:
								builder.withinScriptTimeout();
								break;
							default:
								throw new IllegalStateException("The field '" + fullyQualifiedFieldName + "' cannot have a PageValidor with timeout type of '" + fieldTuple.getTimeoutType() + "'.");
							
						}
						log.info("Validating if field '" + fullyQualifiedFieldName + "' element exists within '" + fieldTuple.getTimeoutType() + "' timeout.");
						builder.executeForOne();
					}
					catch (ElementCountException e)
					{
						throw new IllegalStateException("Page object '" + className + "' failed to load due to at least one page validator failing", e);
					}
        			break;
        		case VISIBLE:
					try
					{
						switch(fieldTuple.getTimeoutType())
						{
							case PAGE_LOAD:
								builder.withinPageLoadTimeout();
								break;
							case AJAX:
								builder.withinAjaxTimeout();
								break;
							case SCRIPT:
								builder.withinScriptTimeout();
								break;
							default:
								throw new IllegalStateException("The field '" + fullyQualifiedFieldName + "' cannot have a PageValidor with timeout type of '" + fieldTuple.getTimeoutType() + "'.");
							
						}
						log.info("Validating if field '" + fullyQualifiedFieldName + "' element is visible within '" + fieldTuple.getTimeoutType() + "' timeout.");
						
						if (quantity == ElementQuantity.EXACTLY_ONE)
						{
							builder.executeForOne();
						}
						else
						{
							if (!builder.executeChecked())
							{
								throw new IllegalStateException("Page object '" + className + "' failed to load due to page validator field '" + fullyQualifiedFieldName + "' requesting at least one match and not finding any.");
							}
						}
					}
					catch (ElementCountException e)
					{
						throw new IllegalStateException("Page object '" + className + "' failed to load due to at least one page validator failing.", e);
					}
        			break;
        		default:
	        		throw new UnsupportedOperationException("Cannot currently deal with '" + PageValidator.class.getSimpleName() + "' annotations with state '" + fieldTuple.getState().toString() + "'.");
        	}
		}
		
        // Validate the page by executing the page load validator methods
		List<Method> pageValidatorMethods = getPageLoadValidatorMethods(pageObject);
		String fullyQualifiedMethodName = null; 
		try
		{
			for (Method method : pageValidatorMethods)
			{
				fullyQualifiedMethodName = className + "." + method.getName() + "()";
				log.info("Running validation method '" + fullyQualifiedMethodName + "'.");
				method.setAccessible(true);
				method.invoke(pageObject, (Object[])null);
			}
		}
		catch (IllegalArgumentException e)
		{
			throw new IllegalStateException("Tried to invoke page validator method '" + fullyQualifiedMethodName + "' with unexpected arguments.", e);
		}
		catch (IllegalAccessException e)
		{
			throw new IllegalStateException("Tried to invoke a page validator method '" + fullyQualifiedMethodName + "' with illegal access to its definition.", e);
		}
		catch (InvocationTargetException e)
		{
			throw new IllegalStateException("The page validator method '" + fullyQualifiedMethodName + "' threw an exception.", e);
		}
		
		log.info("Existing validating page object '" + className + "'");
	}
	
	/**
	 * static class that keeps a Tuple of fields, states, quantity and timeoutType
	 */
	protected static class PageLoadValidatorFieldTuple
	{
		private final Field field;
		private final ElementState state;
		private final ElementTimeoutType timeoutType;
		private final ElementQuantity quantity;
		private final String[] ignoredViewPorts;
		
		/**
		 * @param field
		 * @param state
		 * @param timeoutType 
		 * @param quantity 
		 * @param viewports 
		 * @param pIgnoredViewPorts 
		 */
		public PageLoadValidatorFieldTuple(
				Field field, 
				ElementState state, 
				ElementTimeoutType timeoutType,
				ElementQuantity quantity,
				ViewPortType[] viewports,
				String[] pIgnoredViewPorts)
		{
			this.field = field;
			this.state = state;
			this.timeoutType = timeoutType;
			this.quantity = quantity;
			
			/*
			 * If both ignore view port options were used, throw and exception
			 * as one was deprecated.
			 */
			if (viewports.length > 0 && pIgnoredViewPorts.length > 0)
			{
				throw new IllegalArgumentException("Both ignored breakpoint/viewport attributes cannot at the same time in a page validator, as one is a replaced for the other that is deprecated. (Field='" + field.toString() + "')");
			}
			
			// If the deprecated was used, convert to the supported version.
			if (viewports.length > 0)
			{
				this.ignoredViewPorts = new String[viewports.length];
				for (int i = 0; i < viewports.length; i++)
				{
					// Make all names upper-case
					this.ignoredViewPorts[i] = viewports[i].toString().toUpperCase();
				}
			}
			else
			{
				this.ignoredViewPorts = new String[pIgnoredViewPorts.length];
				for (int i = 0; i < pIgnoredViewPorts.length; i++)
				{
					// Make all names upper-case
					this.ignoredViewPorts[i] = pIgnoredViewPorts[i].toUpperCase();
				}
			}
		}
		
		/**
		 * @return the field
		 */
		public Field getField()
		{
			return field;
		}
		
		/**
		 * @return the state
		 */
		public ElementState getState()
		{
			return state;
		}

		/**
		 * @return the timeoutType
		 */
		public ElementTimeoutType getTimeoutType()
		{
			return timeoutType;
		}

		/**
		 * 
		 * @return the quantity
		 */
		public ElementQuantity getQuantity()
		{
			return quantity;
		}
		
		/**
		 * 
		 * @return the viewports
		 */
		public String[] getViewPorts()
		{
			return ignoredViewPorts;
		}
	}

	/**
	 * 
	 * Returns zero or more fields that are annotated with
	 * {@link PageValidator}.
	 * 
	 * @param type 
	 * @return list of fields
	 */
	private <I> List<PageLoadValidatorFieldTuple> getPageLoadValidatorFields(I pageObject)
	{
		Class clazz = GuiceUtil.removeProxyFromClass(pageObject.getClass());
		List<PageLoadValidatorFieldTuple> result = new ArrayList<PageLoadValidatorFieldTuple>();
		for (Class<?> c = clazz; c != Object.class; c = c.getSuperclass())
        {
			for (Field field : c.getDeclaredFields())
			{
				if (field.isAnnotationPresent(PageValidator.class))
				{
					if (!field.getType().equals(Identifier.class))
		        	{
		        		throw new UnsupportedOperationException("Cannot currently annotate anything other than an Identifier field with '" + PageValidator.class.getName() + "'.");
		        	}
					
					PageValidator plv = field.getAnnotation(PageValidator.class);
					
					field.setAccessible(true);
		        	result.add(new PageLoadValidatorFieldTuple(field, plv.state(), plv.timeout(), plv.quantity(), plv.ignoredBreakpoints(), plv.ignoredViewPorts()));
				}

			}
        }
		return result;
	}
	
	private <I> List<Method> getPageLoadValidatorMethods(I pageObject)
	{
		Class clazz = GuiceUtil.removeProxyFromClass(pageObject.getClass());
		List<Method> result = new ArrayList<Method>();
		for (Class<?> c = clazz; c != Object.class; c = c.getSuperclass())
        {
            for (Method method : c.getDeclaredMethods())
            {
                if (method.isAnnotationPresent(PageValidator.class))
                {
                	result.add(method);
                }
            }
        }
		return result;
	}
}
