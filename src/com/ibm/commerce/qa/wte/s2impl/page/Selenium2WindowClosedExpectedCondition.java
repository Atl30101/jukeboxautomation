package com.ibm.commerce.qa.wte.s2impl.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

/**
 * A Selenium {@link ExpectedCondition} implementation that will apply a find
 * function to locate all elements matching the state and quantity requested. If
 * it fails to match the state and quantity, an exception is thrown.
 * 
 */
public class Selenium2WindowClosedExpectedCondition implements ExpectedCondition<Boolean>
{

	private String windowHandle;
	@Override
	public Boolean apply(WebDriver driver) {
		return !driver.getWindowHandles().contains(windowHandle);
	}
	/**
	 * @param windowHandle the id of the window
	 * 
	 */
	public Selenium2WindowClosedExpectedCondition(String windowHandle) {
		this.windowHandle = windowHandle;
	}
	
}
