package com.ibm.commerce.qa.wte.s2impl.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.ibm.commerce.qa.util.guice.modules.DependsOn;
import com.ibm.commerce.qa.wte.framework.page.AbstractPageListener;
import com.ibm.commerce.qa.wte.framework.page.AbstractPageListener.PageMatcher;
import com.ibm.commerce.qa.wte.framework.test.WebSessionFactory;
import com.ibm.commerce.qa.wte.s2impl.test.Selenium2SessionModule;
import com.ibm.commerce.qa.wte.s2impl.test.Selenium2WebSessionFactory;

/**
 * selenium implementation of an abstract module
 *
 */
@DependsOn(Selenium2SessionModule.class)
public class Selenium2BaseModule extends AbstractModule
{
	@Override
	protected void configure()
	{
		bind(WebSessionFactory.class)
				.to(Selenium2WebSessionFactory.class)
				.in(Singleton.class);
		
		/*
		 * This listener will validate the page loaded successfully and set up
		 * the page hierarchy. Please note that due to the nature of a listener,
		 * the matcher will fire once without being injected. You need to expect
		 * and ignore the first occurrence by testing if an injected member is
		 * null.
		 */
		PageMatcher pageMatcher = new AbstractPageListener.PageMatcher();
		requestInjection(pageMatcher);
		bindListener(pageMatcher, new Selenium2PageListener());
	}
}
