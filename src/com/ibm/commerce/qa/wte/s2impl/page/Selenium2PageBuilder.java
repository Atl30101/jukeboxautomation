package com.ibm.commerce.qa.wte.s2impl.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2015
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.lang.reflect.Method;
import java.util.logging.Logger;

import org.junit.Test;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.ibm.commerce.qa.wte.framework.page.PageBuilder;
import com.ibm.commerce.qa.wte.framework.page.PageFactory;
import com.ibm.commerce.qa.wte.framework.page.PageObject;
import com.ibm.commerce.qa.wte.framework.test.WebSession;
import com.ibm.commerce.qa.wte.framework.util.ConfigManager;
import com.ibm.commerce.qa.wte.framework.util.PageObjectRelativeResolver;
import com.ibm.commerce.qa.wte.s2impl.test.Selenium2WebSession;
import com.ibm.commerce.qa.wte.s2impl.util.Selenium2Util;
/**
 *  Selenium implementation of PageBuilder
 *
 */
public class Selenium2PageBuilder implements PageBuilder
{
	private final Logger log;
	
	private final Injector injector;
	
	private final ConfigManager configManager;

	private final Selenium2WebSession webSession;
	
	private int index = 0;
	
	private PageFactory factory;
	
	private PageObjectRelativeResolver pageRelativeResolver;
	
	/**
	 * Constructor
	 * @param log
	 * @param injector
	 * @param configManager 
	 * @param factory
	 * @param webSession
	 */
	@Inject
	public Selenium2PageBuilder(Logger log, 
			Injector injector, 
			ConfigManager configManager,
			PageFactory factory,
			WebSession webSession,
			PageObjectRelativeResolver p_pageRelativeResolver)
	{
		this.log = log;
		this.injector = injector;
		this.configManager = configManager;
		this.factory = factory;
		this.webSession = (Selenium2WebSession)webSession;
		this.pageRelativeResolver = p_pageRelativeResolver;
	}

	@Override
	public <T> T build(Class<T> pageClass)
	{
		validatePageObject(pageClass);
		Selenium2Util.bypassSslError(configManager.getBrowserType(), webSession.getS2WebDriver());
		webSession.captureHTML();
		T temp =injector.getInstance(pageClass);
		webSession.takeScreenshotOnStep(getCallingTestName());
		temp = pageRelativeResolver.resolvePageObject(temp, factory, index, injector);
		return temp;
	}
	
	@Override
	public <T> T buildAsNewWindow(Class<T> pageClass)
	{
		validatePageObject(pageClass);
		factory.selectNewestWindow();
		return injector.getInstance(pageClass);
	}

	private <T> void validatePageObject(Class<T> pageClass)
	{
		if (pageClass.getAnnotation(PageObject.class) == null)
		{
			if (pageClass.getSuperclass() != Object.class) {
				validatePageObject(pageClass.getSuperclass());
			} else {
				throw new UnsupportedOperationException("Cannot create a page object instance for class without page type annotation.");
			}
		}
	}
	
	@Override
	public PageBuilder withPageFactory(PageFactory p_factory) {
		factory = p_factory;
		
		return this;
	}
	
	private String getCallingTestName()
	{
		StackTraceElement[] trace = Thread.currentThread().getStackTrace();
		String result = "";
		for(StackTraceElement traceElemet : trace){
			String className = traceElemet.getClassName();
			Method methodObject;
			Test methodAnnotation = null;
			try {
				methodObject = Class.forName(className).getDeclaredMethod(traceElemet.getMethodName());
				methodAnnotation = methodObject.getAnnotation(Test.class);
			} catch (ClassNotFoundException e ) {
				//class or method do not exist, do nothing but break
			} catch (SecurityException e) {
				//class or method do not exist, do nothing but break
			} catch (NoSuchMethodException e) {
				//class or method do not exist, do nothing but break
			}
			catch (NullPointerException e) {
				//class or method do not exist, do nothing but break
				break;
			}
			
			if(methodAnnotation != null) {
				result = traceElemet.getMethodName();
				break;
			}
		}
		
		return result;
	}

	@Override
	public PageBuilder withIndex(int i) {
		index = i;
		return this;
	}
	
}
