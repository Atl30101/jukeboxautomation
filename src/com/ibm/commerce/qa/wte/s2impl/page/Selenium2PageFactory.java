package com.ibm.commerce.qa.wte.s2impl.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2015
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.support.ui.WebDriverWait;

import com.ibm.commerce.qa.wte.framework.page.ElementActionsBuilder;
import com.ibm.commerce.qa.wte.framework.page.ElementQueryBuilder;
import com.ibm.commerce.qa.wte.framework.page.Identifier;
import com.ibm.commerce.qa.wte.framework.page.IdentifierQueryBuilder;
import com.ibm.commerce.qa.wte.framework.page.PageBuilder;
import com.ibm.commerce.qa.wte.framework.page.PageFactory;
import com.ibm.commerce.qa.wte.framework.page.ViewPortType;
import com.ibm.commerce.qa.wte.framework.page.WteAlert;
import com.ibm.commerce.qa.wte.framework.page.WteElement;
import com.ibm.commerce.qa.wte.framework.test.WebSession;
import com.ibm.commerce.qa.wte.framework.util.ConfigManager;
import com.ibm.commerce.qa.wte.s2impl.test.Selenium2WebSession;

import com.google.common.base.Optional;
import com.google.inject.Inject;
import com.google.inject.Provider;
/**
 * Selenium implementation of PageFactory
 *
 */
public class Selenium2PageFactory implements PageFactory
{
	private static final long WEBDRIVER_WAIT_TIME_SEC = 10;

	private final Selenium2WebSession webSession;
	
	private ConfigManager config;
	
	private final Provider<ElementActionsBuilder> f_elemActionsBuilderProvider;
	
	private final Provider<PageBuilder> f_pageBuilderProvider;
	
	private final Provider<ElementQueryBuilder> f_elemQueryBuilderProvider;
	
	private final Provider<IdentifierQueryBuilder> f_identQueryBuilderProvider;
	
	/**
	 * Constructor
	 * @param p_elemActionsBuilderProvider 
	 * @param p_configMgr 
	 * @param p_pageBuilderProvider 
	 * @param p_elemQueryBuilderProvider 
	 * @param p_identQueryBuilderProvider 
	 * @param webSession
	 */
	@Inject
	public Selenium2PageFactory(
			Provider<ElementActionsBuilder> p_elemActionsBuilderProvider, 
			Provider<PageBuilder> p_pageBuilderProvider,
			Provider<ElementQueryBuilder> p_elemQueryBuilderProvider,
			Provider<IdentifierQueryBuilder> p_identQueryBuilderProvider,
			ConfigManager p_configMgr,
			WebSession webSession)
	{
		this.f_elemActionsBuilderProvider = p_elemActionsBuilderProvider;
		this.f_pageBuilderProvider = p_pageBuilderProvider;
		this.f_elemQueryBuilderProvider = p_elemQueryBuilderProvider;
		this.f_identQueryBuilderProvider = p_identQueryBuilderProvider;
		
		this.webSession = (Selenium2WebSession)webSession;
		this.config = p_configMgr;
	}
	
	@Override
	public ElementActionsBuilder createActionSequence()
	{
		return this.f_elemActionsBuilderProvider.get();
	}

	@Override
	public PageBuilder createPage()
	{
		return this.f_pageBuilderProvider.get();
	}


	@Override
	public ElementQueryBuilder createQuery(WteElement element)
	{
		ElementQueryBuilder result = this.f_elemQueryBuilderProvider.get();
		
		result.of(element);
		
		return result;
	}

	@Override
	public IdentifierQueryBuilder createQuery(Identifier identifier)
	{
		return createNewQueryHelper(identifier);
	}

	@Override
	public WteElement find(Identifier identifier)
	{
		return createQuery(identifier)
				.asVisible()
				.expectMoreThan(0)
				.executeForOne();
	}

	@Override
	public List<WteElement> findAll(Identifier identifier)
	{
		return createQuery(identifier)
				.asVisible()
				.expectAll()
				.execute();
	}

	@Override
	public WteElement findExactlyOne(Identifier identifier)
	{
		return createQuery(identifier).expectExactlyOne().executeForOne();
	}
	
	@Override
	public WteElement executeForOne(Identifier p_identifier) {
		return createQuery(p_identifier)
				.asVisible()
				.expectMoreThan(0)
				.executeForOne();
	}

	@Override
	public boolean executeChecked(Identifier p_identifier) {
		return createQuery(p_identifier)
				.asVisible()
				.expectMoreThan(0)
				.executeChecked();
	}

	@Override
	public List<WteElement> execute(Identifier p_identifier) {
		return createQuery(p_identifier)
				.asVisible()
				.expectAll()
				.execute();
	}
	
	@Override
	public PageFactory selectWindow(String windowName)
	{
		Iterator<String> windowIterator = webSession.getS2WebDriver().getWindowHandles().iterator();
	      while(windowIterator.hasNext()) { 
	        String windowHandle = windowIterator.next(); 
	        webSession.getS2WebDriver().switchTo().window(windowHandle);
	        if (webSession.getS2WebDriver().getTitle().equals(windowName)){
	        	break;
	        }
	      }
		return this;
	}
	@Override
	public PageFactory selectFirstWindow()
	{
		String windowHandle = webSession.getS2WebDriver().getWindowHandles().iterator().next();
		 webSession.getS2WebDriver().switchTo().window(windowHandle);
		return this;
	}
	@Override
	public PageFactory selectNewestWindow()
	{
		String windowHandle = "";
		Iterator<String> windowIterator = webSession.getS2WebDriver().getWindowHandles().iterator();
	      while(windowIterator.hasNext()) { 
	        windowHandle = windowIterator.next(); 
	      }
	     webSession.getS2WebDriver().switchTo().window(windowHandle);
		return this;
	}
	@Override
	public PageFactory selectDefaultContent() {
		webSession.getS2WebDriver().switchTo().defaultContent();
		return this;
	}
	@Override
	public WteAlert getAlert() {
		return new Selenium2Alert(webSession.getS2WebDriver().switchTo().alert());
	}

	@Override
	public String getTitle()
	{
		return this.webSession.getS2WebDriver().getTitle();
	}

	@Override
	public String getWindowHandle()
	{
		return this.webSession.getS2WebDriver().getWindowHandle();
	}

	@Override
	public String getUrl()
	{
		return this.webSession.getS2WebDriver().getCurrentUrl();
	}

	@Override
	public String getPageSource()
	{
		return this.webSession.getS2WebDriver().getPageSource();
	}

	@Override
	public void confirmWindowClosed(String windowHandle) {
		WebDriverWait wait = new WebDriverWait(webSession.getS2WebDriver(), WEBDRIVER_WAIT_TIME_SEC);
		wait.until(new Selenium2WindowClosedExpectedCondition(windowHandle));
	}
	
	@Override
	@Deprecated
	public ViewPortType getViewPortName() {
		return config.getViewPortName(new Integer(webSession.getS2WebDriver().manage()
				.window()
				.getSize()
				.getWidth()));
	}
	
	@Override
	public Optional<String> getViewPort()
	{
		return config.getViewPort(webSession.getS2WebDriver().manage()
				.window()
				.getSize()
				.getWidth());
	}

	/**
	 * Creates a new query, running {@link IdentifierQueryBuilder#of(Identifier)} 
	 * against it. 
	 * @param identifier Identifier to create
	 * @return new IdentifierQueryBuilder
	 */
	protected IdentifierQueryBuilder createNewQueryHelper(Identifier identifier) {
		IdentifierQueryBuilder result = this.f_identQueryBuilderProvider.get();
		
		result.of(identifier);

		return result;
	}
	
}
