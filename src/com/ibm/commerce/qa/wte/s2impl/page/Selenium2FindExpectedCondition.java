package com.ibm.commerce.qa.wte.s2impl.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.google.common.collect.Lists;
import com.ibm.commerce.qa.wte.framework.page.ElementState;
import com.ibm.commerce.qa.wte.framework.page.ElementsQtyExpected;

/**
 * A Selenium {@link ExpectedCondition} implementation that will apply a find
 * function to locate all elements matching the state and quantity requested. If
 * it fails to match the state and quantity, an exception is thrown.
 * 
 */
public class Selenium2FindExpectedCondition implements ExpectedCondition<List<WebElement>>
{
	private final By by;
	private final ElementState state;
	private final ElementsQtyExpected qty;
	private final WebElement parent;
	
	/**
	 * Contains the zero or more elements representing the last time
	 * {@link #apply(WebDriver)} was executed (if any).
	 */
	private List<WebElement> currentList = Collections.emptyList();
	
	/**
	 * Contains the zero or more elements in any state representing the last time
	 * {@link #apply(WebDriver)} was executed (if any).
	 */
	private List<WebElement> currentAnyStateList = Collections.emptyList();


	/**
	 * Construct this expected condition.
	 * 
	 * @param by
	 *            the Selenium locator object
	 * @param state
	 *            the state the element(s) should be in
	 * @param qty
	 *            the expected quantity of elements to find
	 */
	public Selenium2FindExpectedCondition(By by, ElementState state, ElementsQtyExpected qty)
	{
		this.by = by;
		this.state = state;
		this.qty = qty;
		this.parent = null;
	}
	
	/**
	 * Construct this expected condition.
	 * 
	 * @param by
	 *            the Selenium locator object
	 * @param state
	 *            the state the element(s) should be in
	 * @param qty
	 *            the expected quantity of elements to find
	 * @param parent
	 * 			  The parent element that the search will search below
	 */
	public Selenium2FindExpectedCondition(By by, ElementState state, 
			ElementsQtyExpected qty, WebElement parent)
	{
		this.by = by;
		this.state = state;
		this.qty = qty;
		this.parent = parent;
	}
	
	@Override
	public List<WebElement> apply(WebDriver webDriver)
	{
		// Get all existing elements, use relative if required: 
		if (parent != null) {
			// we are searching relative to (under) a parent
			currentAnyStateList = parent.findElements(this.by);
		} else {
			// Search the entire page
			currentAnyStateList = webDriver.findElements(this.by);
		}
		
		// Get the full lists to start with
		switch(state)
		{
			case EXISTS:
			case NON_EXISTING:
				currentList = currentAnyStateList;
				break;
			case VISIBLE:
				currentList = findVisibleElements(currentAnyStateList);
				break;
			case HIDDEN:
				currentList = findHiddenElements(currentAnyStateList);
				break;
			default:
				throw new UnsupportedOperationException("The state '"
					+ this.state.toString() + "' is unsupported.");
		}
		
		List<WebElement> result = null;
		
		/*
		 * Truth table for all non-existing state:
		 * 
		 *     |  0  |  1   |  >1  |
		 * -------------------------
		 * Ex 1|0    |throw |throw |
		 * -------------------------
		 * Any1|0    |throw |throw |
		 * -------------------------
		 * All |0    |throw |throw | 
		 */
		if (state == ElementState.NON_EXISTING)
		{
			/*
			 * If the list returned is zero, return a zero list.
			 */
			if (currentList.size() == 0)
			{
				result = Collections.emptyList();
			}
			/*
			 * Otherwise throw an exception.
			 */
			else
			{
				throw new NotFoundException("Unexpected element(s) matching '" + by.toString() + "' were found.");
			}

		}
		/*
		 * Truth table for all states but non-existing:
		 * 
		 *     |  0  |  1   |  >1  |
		 * -------------------------
		 * Ex 1|throw|get(0)|throw |
		 * -------------------------
		 * Any1|throw|get(0)|get(0)|
		 * -------------------------
		 * All |throw|list  |list  | 
		 */
		else
		{
			/*
			 * Check that the results match the expected qty state, otherwise
			 * throw an error
			 */
			if (!qty.verifyExpected(currentList.size())) {
				throw new NotFoundException("Expecting " + qty + " Element(s). Found: " +  currentList.size() + " any state: " + currentAnyStateList.size() + 
						" , condition='" + toString() + "'");
			}
			
			/*
			 * If we got to here, the list is the expected size
			 */
			result = Lists.newArrayList(currentList);
		}
		
		return result;
	}
	
	private List<WebElement> findVisibleElements(List<WebElement> allElements)
	{
		List<WebElement> result = new ArrayList<WebElement>();
		
		for (WebElement webElement : allElements)
		{
			if (webElement.isDisplayed())
			{
				result.add(webElement);
			}
		}
		return result;
	}

	private List<WebElement> findHiddenElements(List<WebElement> allElements)
	{
		List<WebElement> result = new ArrayList<WebElement>();
		
		for (WebElement webElement : allElements)
		{
			if (!webElement.isDisplayed())
			{
				result.add(webElement);
			}
		}
		return result;
	}

	/**
	 * Returns the list of elements from the most recent iteration (if any).
	 * This is useful in the case of {@link ElementState#NON_EXISTING}, where a
	 * timeout occurred because there were still elements existing and you want
	 * to find out what they were.
	 * 
	 * @return zero or more web elements
	 */
	public List<WebElement> getLastResults()
	{
		return this.currentList;
	}
	/**
	 * Returns the list of elements in any state (visibile or not) from the most recent iteration
	 * useful for error reporting purposes.
	 * @return zero or more web elements
	 */
	public List<WebElement> getLastAnyStateResults() {
		return currentAnyStateList;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("element(s) in the state ");
		sb.append(state);
		sb.append(" with a requested quantity of ");
		sb.append(qty);
		sb.append(" located by ");
		sb.append(by);
		
		if (parent != null) {
			sb.append(" under parent ");
			sb.append(parent);
		}
		
		if(currentAnyStateList.size() > 1) {
			sb.append(", but got elements in different states matching '" + by.toString() + "'.");
			
		}
		
		return sb.toString();
	}

	
}
