package com.ibm.commerce.qa.wte.s2impl.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.inject.Inject;
import com.ibm.commerce.qa.wte.framework.page.ElementQueryBuilder;
import com.ibm.commerce.qa.wte.framework.page.ElementState;
import com.ibm.commerce.qa.wte.framework.page.ElementTimeoutType;
import com.ibm.commerce.qa.wte.framework.page.WteElement;
import com.ibm.commerce.qa.wte.framework.test.WebSession;
import com.ibm.commerce.qa.wte.framework.util.ConfigManager;
import com.ibm.commerce.qa.wte.s2impl.test.Selenium2WebSession;

/**
 * Selenium implementation of ElementQueryBuilder
 *
 */
public class Selenium2ElementQueryBuilder implements ElementQueryBuilder
{
	private static final ElementTimeoutType DEFAULT_TIMEOUT_TYPE = ElementTimeoutType.IMPLICIT;
	@SuppressWarnings("unused")
	private final Logger log;
	
	private final ConfigManager configManager;
	
	private final Selenium2WebSession webSession;

	private Selenium2Element element;
	
	private ElementState state;
	
	private boolean timeoutTypeSet;
	private ElementTimeoutType timeoutType;
	private long timeoutTime;
	private TimeUnit timeoutUnit;
	
	/**
	 * Creates new instance of Selenium2ElementQueryBuilder.
	 * 
	 * @param log
	 * @param configManager
	 * @param session
	 */
	@Inject
	Selenium2ElementQueryBuilder(Logger log, 
			ConfigManager configManager,
			WebSession session)
	{
		this.log = log;
		this.configManager = configManager;
		this.webSession = (Selenium2WebSession)session;
	}

	@Override
	public ElementQueryBuilder of(WteElement element)
	{
		if (!(element instanceof Selenium2Element))
		{
			throw new IllegalArgumentException("Cannot use '" + Selenium2ElementQueryBuilder.class.getName() + "' with an element from a different implementation.");
		}
		this.element = (Selenium2Element)element;
		return this;
	}

	@Override
	public ElementQueryBuilder withinAjaxTimeout()
	{
		if (timeoutTypeSet)
		{
			throw new IllegalStateException("The wait type was already set for this builder.");
		}
		
		this.timeoutTypeSet = true;
		this.timeoutType = ElementTimeoutType.AJAX;

		return this;
	}

	@Override
	public ElementQueryBuilder withinScriptTimeout()
	{
		if (timeoutTypeSet)
		{
			throw new IllegalStateException("The wait type was already set for this builder.");
		}
		
		this.timeoutTypeSet = true;
		this.timeoutType = ElementTimeoutType.SCRIPT;

		return this;
	}


	@Override
	public ElementQueryBuilder withinImplicitTimeout()
	{
		if (timeoutTypeSet)
		{
			throw new IllegalStateException("The wait type was already set for this builder.");
		}
		
		this.timeoutTypeSet = true;
		this.timeoutType = ElementTimeoutType.IMPLICIT;

		return this;
	}


	@Override
	public ElementQueryBuilder withinExplicitTimeout(long time,
			TimeUnit unit)
	{
		if (timeoutTypeSet)
		{
			throw new IllegalStateException("The wait type was already set for this builder.");
		}
		
		this.timeoutTypeSet = true;
		this.timeoutType = ElementTimeoutType.EXPLICIT;
		this.timeoutTime = time;
		this.timeoutUnit = unit;

		return this;
	}

	@Override
	public ElementQueryBuilder asStale()
	{
		this.state = ElementState.STALE;
		return this;
	}

	@Override
	public boolean execute()
	{
		if (state == null)
		{
			throw new IllegalStateException("A state such as 'stale' was not set.");
		}
		
		return executeInternal();
	}

	@Override
	public void executeOrFail()
	{
		if (state == null)
		{
			throw new IllegalStateException("A state such as 'stale' was not set.");
		}
		
		if (!executeInternal())
		{
			throw new IllegalStateException("The execution failed waiting for the staleness of elements with a timeout type of " + getCalculatedTimeoutType().toString() + ".");
		}
	}
		
	private boolean executeInternal()
	{
		ElementTimeoutType calcTimeoutType = getCalculatedTimeoutType();
		
		WebDriverWait wait = genWait(calcTimeoutType);
		
		boolean result = false;

		switch (state) {
			case VISIBLE:
				wait.until(ExpectedConditions.visibilityOf(element.toWebElement()));
				result = true;
				break;
			case HIDDEN:
				wait.until(ExpectedConditions.not(ExpectedConditions.visibilityOf(element.toWebElement())));
				result = true;
				break;
			case STALE:
				result = wait.until(ExpectedConditions.stalenessOf(element.toWebElement()));
				break;
			default:
				throw new UnsupportedOperationException("The Element State '"
						+ state + "' is unsupported.");
		}
		
		return result;
	}

	/**
	 * Returns the timeout type. If it is currently <code>null</code>, it will
	 * return the default.
	 * 
	 * @return
	 */
	private ElementTimeoutType getCalculatedTimeoutType()
	{
		return this.timeoutType == null ? DEFAULT_TIMEOUT_TYPE : this.timeoutType;
	}

	private WebDriverWait genWait(ElementTimeoutType timeout)
	{
		WebDriverWait result = null;
		switch(timeout)
		{
			case IMPLICIT:
				result = new WebDriverWait(this.webSession.getS2WebDriver(), 
						configManager.getImplicitTimeoutSeconds());
				break;
			case SCRIPT:
				result = new WebDriverWait(this.webSession.getS2WebDriver(),
						configManager.getScriptTimeoutSeconds());
				break;
			case AJAX:
				result = new WebDriverWait(this.webSession.getS2WebDriver(),
						configManager.getAjaxTimeoutSeconds());
				break;
			case EXPLICIT:
				result = new WebDriverWait(this.webSession.getS2WebDriver(),
						TimeUnit.SECONDS.convert(this.timeoutTime, this.timeoutUnit));
				break;
			default:
				throw new UnsupportedOperationException("The timeout type '"
					+ this.timeoutType.toString() + "' is unsupported.");
		}
		
		return result;
	}

	@Override
	public ElementQueryBuilder asVisible() {
		state = ElementState.VISIBLE;
		return this;
	}

	@Override
	public ElementQueryBuilder asInvisible() {
		state = ElementState.HIDDEN;
		return this;
	}
}
