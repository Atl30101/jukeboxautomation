package com.ibm.commerce.qa.wte.s2impl.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import org.openqa.selenium.Alert;

import com.ibm.commerce.qa.wte.framework.page.WteAlert;

/**
 * Implements a Selenium-backed version of a {@link WteAlert}.
 * 
 */
public class Selenium2Alert implements WteAlert
{
	private final Alert alert;
	
	Selenium2Alert(Alert alert)
	{
		this.alert = alert;
	}

	@Override
	public void accept()
	{
		this.alert.accept();
	}
	@Override
	public void dismiss()
	{
		this.alert.dismiss();
	}
	@Override
	public String getText()
	{
		return this.alert.getText();
	}
	@Override
	public void sendKeys(String keysToSend)
	{
		this.alert.sendKeys(keysToSend);
	}
	/**
	 * 
	 * @return alert
	 */
	public Alert toWebAlert()
	{
		return this.alert;
	}
	
}
