package com.ibm.commerce.qa.wte.s2impl.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.CompositeAction;

import com.google.inject.Inject;
import com.ibm.commerce.qa.wte.framework.page.ElementActionsBuilder;
import com.ibm.commerce.qa.wte.framework.page.ElementCompositeAction;
import com.ibm.commerce.qa.wte.framework.page.WteElement;
import com.ibm.commerce.qa.wte.framework.test.WebSession;
import com.ibm.commerce.qa.wte.framework.util.KeyboardKeys;
import com.ibm.commerce.qa.wte.s2impl.test.Selenium2WebSession;
import com.ibm.commerce.qa.wte.s2impl.util.Selenium2Util;

/**
 * selenium implementation of ElementActionsBuilder
 *
 */
public class Selenium2ElementActionsBuilder implements ElementActionsBuilder
{
	private final Actions actions;

	/**
	 * Construct Selenium2ElementActionsBuilder
	 */
	@Inject
	Selenium2ElementActionsBuilder(WebSession session)
	{
		this.actions = new Actions(((Selenium2WebSession)session).getS2WebDriver());
	}

	@Override
	public ElementCompositeAction build()
	{
		return new Selenium2ElementCompositeAction((CompositeAction)this.actions.build());
	}

	@Override
	public ElementActionsBuilder click()
	{
		actions.click();
		return this;
	}

	@Override
	public ElementActionsBuilder click(WteElement onElement)
	{
		actions.click(toWebElement(onElement));
		return this;
	}

	@Override
	public ElementActionsBuilder clickAndHold()
	{
		actions.clickAndHold();
		return this;
	}

	@Override
	public ElementActionsBuilder clickAndHold(WteElement onElement)
	{
		actions.clickAndHold(toWebElement(onElement));
		return this;
	}

	@Override
	public ElementActionsBuilder contextClick(WteElement onElement)
	{
		actions.contextClick(toWebElement(onElement));
		return this;
	}

	@Override
	public ElementActionsBuilder doubleClick()
	{
		actions.doubleClick();
		return this;
	}

	@Override
	public ElementActionsBuilder doubleClick(WteElement onElement)
	{
		actions.doubleClick(toWebElement(onElement));
		return this;
	}

	@Override
	public ElementActionsBuilder dragAndDrop(WteElement source, WteElement target)
	{
		actions.dragAndDrop(toWebElement(source), toWebElement(target));
		return this;
	}

	@Override
	public ElementActionsBuilder dragAndDropBy(WteElement source, int offset,
			int offset2)
	{
		actions.dragAndDropBy(toWebElement(source), offset, offset2);
		return this;
	}

	@Override
	public ElementActionsBuilder keyDown(KeyboardKeys theKey)
	{
		actions.keyDown(Selenium2Util.convertKKeysToSeleniumKeys(theKey));
		return this;
	}

	@Override
	public ElementActionsBuilder keyDown(WteElement element, KeyboardKeys theKey)
	{
		actions.keyDown(toWebElement(element), Selenium2Util.convertKKeysToSeleniumKeys(theKey));
		return this;
	}

	@Override
	public ElementActionsBuilder keyUp(KeyboardKeys theKey)
	{
		actions.keyUp(Selenium2Util.convertKKeysToSeleniumKeys(theKey));
		return this;
	}

	@Override
	public ElementActionsBuilder keyUp(WteElement element, KeyboardKeys theKey)
	{
		actions.keyUp(toWebElement(element), Selenium2Util.convertKKeysToSeleniumKeys(theKey));
		return this;
	}

	@Override
	public ElementActionsBuilder moveByOffset(int offset, int offset2)
	{
		actions.moveByOffset(offset, offset2);
		return this;
	}

	@Override
	public ElementActionsBuilder moveToElement(WteElement toElement, int offset,
			int offset2)
	{
		actions.moveToElement(toWebElement(toElement), offset, offset2);
		return this;
	}

	@Override
	public ElementActionsBuilder moveToElement(WteElement toElement)
	{
		actions.moveToElement(toWebElement(toElement));
		return this;
	}

	@Override
	public void perform()
	{
		actions.perform();
	}

	@Override
	public ElementActionsBuilder release()
	{
		actions.release();
		return this;
	}

	@Override
	public ElementActionsBuilder release(WteElement onElement)
	{
		actions.release(toWebElement(onElement));
		return this;
	}

	@Override
	public ElementActionsBuilder sendKeys(CharSequence... keysToSend)
	{
		actions.sendKeys(keysToSend);
		return this;
	}

	@Override
	public ElementActionsBuilder sendKeys(WteElement element,
			CharSequence... keysToSend)
	{
		actions.sendKeys(toWebElement(element), keysToSend);
		return this;
	}

	private WebElement toWebElement(WteElement element)
	{
		return ((Selenium2Element)element).toWebElement();
	}

	Actions toSeleniumActions()
	{
		return this.actions;
	}
}
