/**
 * 
 */
package com.ibm.commerce.qa.wte.s2impl.page;

import static com.google.common.base.Preconditions.checkArgument;

import com.google.common.base.Objects;
import com.google.inject.Singleton;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;
import com.ibm.commerce.qa.wte.framework.page.ElementCountException;
import com.ibm.commerce.qa.wte.framework.page.ElementsQtyExpected;
import com.ibm.commerce.qa.wte.framework.page.ElementsQtyExpectedFactory;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2014
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

/**
 * Collection of implementations for {@link ElementsQtyExpected} instances.
 * 
 * <p/>
 * These are bound and instantiated through the 
 * {@link ElementsQtyExpectedFactory}.
 *
 */
public abstract class Selenium2ElementsQtyExpected {

	private Selenium2ElementsQtyExpected() {
		// don't instantiate/extend me >:(
	}
	
	/**
	 * Abstract implementation to save boiler-plate for implementations
	 *
	 */
	protected abstract static class AbstractElementsQtyExpected 
			implements ElementsQtyExpected {
		
		@Override
		public void verifyExpectedAndFail(int p_actualCount)
				throws ElementCountException {
			if (!verifyExpected(p_actualCount)) {
				throw new ElementCountException(p_actualCount, this, 
						"Element Quantity Condition was incorrect");
			}
		}
		
		@Override
		public String toString() {
			return Objects.toStringHelper(getClass())
					.toString();
		}
		
		/*
		 * Singleton versions of #hashCode() and #equals()
		 */
		@Override
		public int hashCode() {
			return Objects.hashCode(getClass());
		}
		
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			
			if (obj == null) {
				return false;
			}
			
			if (!(getClass().isInstance(obj))) {
				return false;
			}
			
			return true;
		}
		
	}
	
	/**
	 * Verifies if and only if one element was found
	 */
	@Singleton
	public static class ExactlyOne extends AbstractElementsQtyExpected {
		
		/**
		 * Default constructor
		 */
		@AssistedInject
		ExactlyOne() {
		}
		
		@Override
		public boolean verifyExpected(int p_actualCount) {
			return (p_actualCount == 1);
		}
		
		// singleton, use Object#equals() and #hashCode()
	}
	
	/**
	 * Verifies if and only if one element was found
	 */
	public static class Exactly extends AbstractElementsQtyExpected {
		
		private final int f_count;
		
		/**
		 * Default constructor
		 * @param amount Exact count
		 */
		@AssistedInject
		Exactly(@Assisted final int amount) {
			f_count = amount;
		}
		
		@Override
		public boolean verifyExpected(int p_actualCount) {
			return (p_actualCount == f_count);
		}
		
		@Override
		public String toString() {
			return Objects.toStringHelper(getClass())
					.add("amount", f_count)
					.toString();
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			return Objects.hashCode(Exactly.class, Integer.valueOf(f_count));
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof Exactly)) {
				return false;
			}
			Exactly other = (Exactly) obj;
			return (f_count == other.f_count);
		}
		
	}
	
	/**
	 * Verifies if and only if no elements are found
	 */
	@Singleton
	public static class None extends AbstractElementsQtyExpected {
		/**
		 * Default constructor
		 */
		@AssistedInject
		None() {
		}
		
		@Override
		public boolean verifyExpected(int p_actualCount) {
			return (p_actualCount == 0);
		}
		
		// singleton, use Object#equals() and #hashCode()
	}
	
	/**
	 * Verifies if and only if no elements are found
	 */
	@Singleton // unfortunately not actually singleton, but we can behave like
			   // it is since it has no state.
	public static class All extends AbstractElementsQtyExpected {
		
		/**
		 * Default constructor
		 */
		@AssistedInject
		All() {
		}
		
		@Override
		public boolean verifyExpected(int p_actualCount) {
			return true ;
		}
		
		@Override
		public void verifyExpectedAndFail(int p_actual) {
			return ;
		}
		
		// singleton, use Object#equals() and #hashCode()
	}
	
	/**
	 * Verifies if and only if more than a minimum is found
	 */
	public static class MoreThan extends AbstractElementsQtyExpected {
		
		private final int f_min;
		
		/**
		 * Default constructor
		 * @param expectedMinimum minimum count
		 */
		@AssistedInject
		MoreThan(@Assisted final int expectedMinimum) {
			f_min = expectedMinimum;
		}

		@Override
		public boolean verifyExpected(int p_actualCount) {
			return (p_actualCount > f_min);
		}
		
		@Override
		public String toString() {
			return Objects.toStringHelper(getClass())
					.add("minimum", f_min)
					.toString();
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			return Objects.hashCode(MoreThan.class, Integer.valueOf(f_min));
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof MoreThan)) {
				return false;
			}
			MoreThan other = (MoreThan) obj;
			return (f_min == other.f_min);
		}
	}
	
	/**
	 * Verifies if and only if less than a maximum is found
	 */
	public static class LessThan extends AbstractElementsQtyExpected {
		private final int f_max;
		
		/**
		 * Default constructor
		 * @param expectedMaximum Maximum count
		 */
		@AssistedInject
		LessThan(@Assisted final int expectedMaximum) {
			f_max = expectedMaximum;
		}

		@Override
		public boolean verifyExpected(int p_actualCount) {
			return (p_actualCount < f_max);
		}
		
		@Override
		public String toString() {
			return Objects.toStringHelper(getClass())
					.add("maximum", f_max)
					.toString();
		}
		
		@Override
		public int hashCode() {
			return Objects.hashCode(LessThan.class, Integer.valueOf(f_max));
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof LessThan)) {
				return false;
			}
			LessThan other = (LessThan) obj;
			return (f_max == other.f_max);
		}
	}
	
	/**
	 * Verifies if and only if more than a minimum and less than a maximum
	 */
	public static class Within extends AbstractElementsQtyExpected {
		
		private final int f_min;
		private final int f_max;
		
		/**
		 * Default constructor
		 * @param expectedMinimum minimum count
		 * @param expectedMaximum maximum count
		 */
		@AssistedInject
		Within(@Assisted("min") final int expectedMinimum, 
				@Assisted("max") final int expectedMaximum) {
			checkArgument(expectedMaximum >= expectedMinimum, 
					"Max must be >= min, max=%s, min=%s", 
					Integer.valueOf(expectedMaximum), 
					Integer.valueOf(expectedMinimum));
			
			f_min = expectedMinimum;
			f_max = expectedMaximum;
		}

		@Override
		public boolean verifyExpected(int p_actualCount) {
			return (p_actualCount > f_min && p_actualCount < f_max);
		}
		
		@Override
		public String toString() {
			return Objects.toStringHelper(getClass())
					.add("minimum", f_min)
					.add("maximum", f_max)
					.toString();
		}
		
		@Override
		public int hashCode() {
			return Objects.hashCode(Within.class, Integer.valueOf(f_min), 
					Integer.valueOf(f_max));
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof Within)) {
				return false;
			}
			Within other = (Within) obj;
			if (f_min != other.f_min) {
				return false;
			}
			return (f_max == other.f_max);
		}
	}
	
}
