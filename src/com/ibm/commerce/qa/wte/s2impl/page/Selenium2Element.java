package com.ibm.commerce.qa.wte.s2impl.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ibm.commerce.qa.wte.framework.page.BrowserType;
import com.ibm.commerce.qa.wte.framework.page.IdentifierQueryBuilder;
import com.ibm.commerce.qa.wte.framework.page.WteElement;
import com.ibm.commerce.qa.wte.framework.test.WebSession;
import com.ibm.commerce.qa.wte.framework.util.ConfigManager;
import com.ibm.commerce.qa.wte.s2impl.test.Selenium2WebSession;
import com.ibm.commerce.qa.wte.s2impl.util.Selenium2Util;

/**
 * Implements a Selenium-backed version of a {@link WteElement}.
 * 
 * 
 * 
 */
public class Selenium2Element implements WteElement
{
	private static final String MOUSE_OVER_ELEMENT_JS = "var elem = arguments[0];"
		+ "if( document.createEvent) {"
		+ "var evObj = document.createEvent('MouseEvents');"
		+ "evObj.initEvent( 'mouseover', true, false );"
		+ "elem.dispatchEvent(evObj);"
		+ "} else if( document.createEventObject ) {"
		+ "elem.fireEvent('onmouseover');" + "}";

	private static final String[] TYPICAL_ATTRIBUTES = { "id", "class", "title", "href", "src", "type", "name" };

	
	private final Logger log;
	private final Selenium2WebSession f_session;
	private final WebElement element;
	private final ConfigManager config;
	
	// Initialized and used if any select tag type methods are called
	private Select select;
	
	/**
	 * Default constructor
	 * @param log
	 * @param config
	 * @param session
	 * @param element
	 */
	Selenium2Element(Logger log, ConfigManager config, 
			WebSession session, WebElement element)
	{
		this.log = log;
		this.f_session = (Selenium2WebSession)session;
		this.element = element;
		this.config = config;
	}

	/**
	 * This will call the Selenium element's clear() method. It's been observed
	 * that a browser may not trigger the javascript event when cleared. Sending
	 * a backspace key afterwards (which should be harmless) seem to fix this.
	 */
	@Override
	public void clear()
	{
		this.element.clear();
		this.element.sendKeys(Keys.BACK_SPACE);

	}

	@Override
	public void click()
	{
		try
		{
			Thread.sleep(config.getClickDelayMilliseconds());
		}
		catch (InterruptedException e)
		{
			throw new IllegalStateException("The sleep before click was interrupted.", e);
		}
		
		try {
			this.element.click();
		} catch (WebDriverException e) {
			// in case it is not clickable, try using javascript to click on it
			if (e.getMessage().contains("not clickable at point")) {
				((JavascriptExecutor)this.f_session.getS2WebDriver()).executeScript("arguments[0].click();", element);
			} else {
				throw e;
			}
		}
		
		Selenium2Util.bypassSslError(config.getBrowserType(), f_session.getS2WebDriver());
	}
	
	@Override
	public void clickWithJS() {
		((JavascriptExecutor)this.f_session.getS2WebDriver()).executeScript("arguments[0].click();", element);
	}
	
	@Override
	public void clickWaitRetry()
	{
		final int MAX_ATTEMPT_COUNT = 4;
		
		WebElement tmpElement = this.element;
		
		boolean success = true;
		for (int i = 1; i <= MAX_ATTEMPT_COUNT; i++)
		{
			try
			{
				tmpElement.click();
				success = true;
				break;
			}
			catch(WebDriverException e)
			{
				/*
				 * If this is the first attempt, then this is a genuine
				 * exception because it means the element was not visible in the
				 * first place to click because the page was scrolling. Otherwise it may be because during the
				 * time between the page load wait and this next iteration the
				 * element did indeed disappear.
				 */
				success=false;
				String ex = e.toString();
				if(!e.toString().contains("Other element would receive the click"))
				{
					throw new IllegalStateException("The element to click was not visible in the first place.", e);
				}
				else
				{
					log.log(Level.WARNING, 
							"Attempt "+ i +" to the element click failed as it wasn't visible. Will wait for 500 ms and try again.", 
							e);
					try {
						Thread.sleep(500);
					} catch (InterruptedException e1) {
						throw new IllegalStateException("The sleep before click was interrupted.", e);
					}
				}
			}
						
		}
		
		if (!success)
		{
			throw new IllegalStateException("The element could not be clicked, even after trying " + MAX_ATTEMPT_COUNT + " times.");
		}
	}
	
	@Override
	public void clickWithRetry()
	{
		final int MAX_ATTEMPT_COUNT = 4;
		
		WebElement tmpElement = this.element;
		
		WebDriverWait wait = new WebDriverWait(f_session.getS2WebDriver(), 
				config.getPageLoadTimeoutSeconds());
		
		boolean success = false;
		for (int i = 1; i <= MAX_ATTEMPT_COUNT; i++)
		{
			try
			{
				tmpElement.click();
				Selenium2Util.bypassSslError(config.getBrowserType(), f_session.getS2WebDriver());
			}
			catch(ElementNotVisibleException e)
			{
				/*
				 * If this is the first attempt, then this is a genuine
				 * exception because it means the element was not visible in the
				 * first place to click. Otherwise it may be because during the
				 * time between the page load wait and this next iteration the
				 * element did indeed disappear.
				 */
				if (i == 1)
				{
					throw new IllegalStateException("The element to click was not visible in the first place.", e);
				}
				else
				{
					log.log(Level.WARNING, 
							"Attempt to retry the element click failed as it wasn't visible. Will now check to see if it indeed went stale.", 
							e);
				}
			}
						
			try
			{
				success = wait.until(ExpectedConditions.stalenessOf(tmpElement));
				assert success;
				break;
			}
			/*
			 * If it could not be found, it's possible that this element object
			 * is in a bad state, so create another copy of the element object
			 * and try again.
			 */
			catch (TimeoutException e)
			{
				log.warning("Attempt " + i + " to click on the element failed.");
				tmpElement = this.element.findElement(By.xpath("."));
			}
		}
		
		if (!success)
		{
			throw new IllegalStateException("The element could not be clicked, even after trying " + MAX_ATTEMPT_COUNT + " time(s).");
		}
	}

	
	/**
	 * @param elementQuery
	 */
	@Override
	public void clickWithVerify(IdentifierQueryBuilder elementQuery)
	{
		final int MAX_ATTEMPT_COUNT = 4;
		
		WebElement tmpElement = this.element;
		
		boolean success = false;

		for (int i = 1; i <= MAX_ATTEMPT_COUNT; i++)
		{
			tmpElement.click();
			Selenium2Util.bypassSslError(config.getBrowserType(), f_session.getS2WebDriver());
			try
			{
					elementQuery.executeForOne();
					success = true;
					break;
			}
			/*
			 * If it could not be found, it's possible that this element object
			 * is in a bad state, so create another copy of the element object
			 * and try again.
			 */
			catch (TimeoutException e)
			{
				log.warning("Attempt " + i + " to click on the element failed, could not verify element.");
				tmpElement = this.element.findElement(By.xpath("."));
			}
		}
		
		if (!success)
		{
			throw new IllegalStateException("The element could not be clicked, even after trying " + MAX_ATTEMPT_COUNT + " time(s).");
		}
	}
	
	
	@Override
	public String getAttributeValue(String attributeName)
	{
		return this.element.getAttribute(attributeName);
	}

	@Override
	public String getName()
	{
		return this.element.getTagName();
	}

	@Override
	public boolean isEnabled()
	{
		return this.element.isEnabled();
	}

	@Override
	public boolean isStale()
	{
		try
		{
			isEnabled();
		}
		catch (StaleElementReferenceException e)
		{
			return true;
		}
		
		return false;
	}

	@Override
	public boolean isSelected()
	{
		return this.element.isSelected();
	}

	@Override
	public boolean isVisible()
	{
		return this.element.isDisplayed();
	}

	@Override
	public void submit()
	{
		this.element.submit();
	}

	@Override
	public void type(CharSequence characters)
	{
		this.element.sendKeys(characters);
	}

	@Override
	public void clearAndType(CharSequence characters)
	{
		// Cannot use this class clear() method, as it sends a backspace.
		this.element.clear(); 
		type(characters);
	}
	
	@Override
	public void clearAndTypeWithRetry(CharSequence characters)
	{
		final int MAX_ATTEMPT_COUNT = 4;
		
		WebElement tmpElement = this.element;
		
		//WebDriverWait wait = new WebDriverWait(this.driver, config.getPageLoadTimeoutSeconds());
		
		boolean success = false;
		for (int i = 1; i <= MAX_ATTEMPT_COUNT; i++)
		{
			this.element.clear();
			
			type(characters);
			if (!tmpElement.getAttribute("value").equals(characters)){
				tmpElement.click();
				log.warning("Attempt " + i + " to clear and type text on the element failed, could not verify text.");
			}
			else {
				success=true;
				break;
			}
			
		}
		
		if (!success)
		{
			throw new IllegalStateException("The element was still empty, even after trying " + MAX_ATTEMPT_COUNT + " time(s) to type.");
		}	
	}

	/**
	 * Returns the Selenium "native" version of an element ({@link WebElement}).
	 * 
	 * @return the Selenium 2 element
	 */
	public WebElement toWebElement()
	{
		return this.element;
	}

	@Override
	public String getText()
	{
		String result = getRawText();
		if (result != null)
		{
			result = result.replaceAll("\\s+", " ").trim();
		}

		return result; 
	}

	/**
	 * If the text is null or empty, it may be because the element is hidden. If
	 * so, this method will attempt to use Javascript directly to fetch the
	 * text.
	 */
	@Override
	public String getRawText()
	{
		String result = element.getText();
		
		/*
		 * If there is no result, check if it's hidden, and if so, fetch the
		 * text directly through Javascript.
		 */
		if ( (result == null || result.isEmpty()) && !isVisible() )
		{
			log.fine("Since element is hidden, attempting to fetch text via Javascript.");
			String command = getBrowserType() == BrowserType.INTERNET_EXPLORER ? "innerText" : "textContent";
			result = (String)((JavascriptExecutor)f_session.getS2WebDriver()).executeScript("return arguments[0]." + command + ";", element);
		}
		return result;
	}

	private void createSelect()
	{
		this.select = new Select(this.element);
	}
	
	@Override
	public void selectByVisibleText(String text)
	{
		if (this.select == null)
		{
			createSelect();
		}
		
		this.select.selectByVisibleText(text);
	}
	
	@Override
	public void selectByValue(String value)
	{
		if (this.select == null)
		{
			createSelect();
		}
		
		this.select.selectByValue(value);
		
	}
	
	@Override
	public void selectByIndex(int index)
	{
		if (this.select == null)
		{
			createSelect();
		}
		
		this.select.selectByIndex(index - 1);
	}
	
	@Override
	public WteElement getFirstSelectedOption()
	{
		if (this.select == null)
		{
			createSelect();
		}
		
		return new Selenium2Element(
				this.log, 
				this.config, 
				this.f_session, 
				this.select.getFirstSelectedOption());
	}

	private BrowserType getBrowserType()
	{
		String userAgent = (String)((JavascriptExecutor) f_session.getS2WebDriver()).executeScript("return navigator.userAgent;");
		if (userAgent.contains("MSIE"))
		{
			return BrowserType.INTERNET_EXPLORER;
		}
		else if(userAgent.contains("Firefox"))
		{
			return BrowserType.FIREFOX;
		}
		else if(userAgent.contains("Safari"))
		{
			return BrowserType.SAFARAI;
		}
		else if(userAgent.contains("Opera"))
		{
			return BrowserType.OPERA;
		}
		else if(userAgent.contains("Chrome"))
		{
			return BrowserType.CHROME;
		}
		else
		{
			return BrowserType.UNKNOWN;
		}
	}

	@Override
	public void mouseOver()
	{
        JavascriptExecutor js = (JavascriptExecutor)f_session.getS2WebDriver();
		js.executeScript(MOUSE_OVER_ELEMENT_JS, element);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("{ ");
		sb.append("name='" + getName() + "',typical_attributes={ ");
		
		boolean first = true;
		for (String attr : TYPICAL_ATTRIBUTES)
		{
			String attrValue = element.getAttribute(attr);
			
			if (attrValue != null)
			{
				if (!first)
				{
					sb.append(", ");
				}
				
				sb.append(attr + "='");
				sb.append(attrValue);
				sb.append("'");

				if (first)
				{
					first = false;
				}
			}
		}
		
		sb.append(" },selenium_to_string='");
		sb.append(element.toString());
		sb.append("' }");
		return sb.toString();
	}

	/**
	 * As of the 2.25 driver, focus seems to work by "typing" an empty string to
	 * the element.
	 */
	@Override
	public void focus()
	{
		type("");
	}

	/**
	 * Note: Focus functionality  has issues with Firefox, as it seems the
	 * focus does not just appear on the element. The mouse seems to "move" to
	 * the element, causing element's focus events to fire.
	 */
	@Override
	public void focusAndClick()
	{
		focus();
		click();
	}
	
	/**
	 * Returns the backing {@link WebElement} instance from Selenium.
	 * @return backing instance
	 */
	public WebElement getBackingWebElement() {
		return this.element;
	}

}
