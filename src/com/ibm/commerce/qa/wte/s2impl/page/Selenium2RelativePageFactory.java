/**
 * 
 */
package com.ibm.commerce.qa.wte.s2impl.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2015
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import java.util.logging.Logger;

import com.ibm.commerce.qa.wte.framework.page.ElementActionsBuilder;
import com.ibm.commerce.qa.wte.framework.page.ElementQueryBuilder;
import com.ibm.commerce.qa.wte.framework.page.Identifier;
import com.ibm.commerce.qa.wte.framework.page.IdentifierQueryBuilder;
import com.ibm.commerce.qa.wte.framework.page.PageBuilder;
import com.ibm.commerce.qa.wte.framework.page.RelativePageFactory;
import com.ibm.commerce.qa.wte.framework.page.WteElement;
import com.ibm.commerce.qa.wte.framework.test.WebSession;
import com.ibm.commerce.qa.wte.framework.util.ConfigManager;
import com.ibm.commerce.qa.wte.framework.util.WteElements;
import com.ibm.commerce.qa.wte.s2impl.util.Suppliers2;

import com.google.common.base.Objects;
import com.google.common.base.Supplier;
import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Implementation for Relative Page factory
 */
public class Selenium2RelativePageFactory extends Selenium2PageFactory
		implements RelativePageFactory {

	private final Logger f_log;
	
	private Supplier<WteElement> f_rootQuery;
	
	/**
	 * Root of page, default elemen identifier
	 */
	protected final static Identifier DEFAULT_ELEMENT_IDENTIFIER = Identifier.byXPath("/*");
	
	/**
	 * Default constructor
	 * @param p_log 
	 * @param p_elemActionsBuilderProvider 
	 * @param p_pageBuilderProvider 
	 * @param p_elemQueryBuilderProvider 
	 * @param p_identQueryBuilderProvider 
	 * @param p_configMgr 
	 * @param webSession
	 */
	@Inject
	public Selenium2RelativePageFactory(Logger p_log,
			Provider<ElementActionsBuilder> p_elemActionsBuilderProvider, 
			Provider<PageBuilder> p_pageBuilderProvider,
			Provider<ElementQueryBuilder> p_elemQueryBuilderProvider,
			Provider<IdentifierQueryBuilder> p_identQueryBuilderProvider,
			ConfigManager p_configMgr,
			WebSession webSession) {
		super(p_elemActionsBuilderProvider, p_pageBuilderProvider, 
				p_elemQueryBuilderProvider, p_identQueryBuilderProvider, 
				p_configMgr,
				webSession);
		
		f_log = p_log;
	}
	
	/**
	 * Gets an IdentifierQueryBuilder for the default query
	 * @return WteElementFunction that looks for the page root
	 */
	private Supplier<WteElement> getDefaultQuery() {
		// simply creates a new query helper and finds the first element
		return new Supplier<WteElement>() {
			@Override
			public WteElement get() {
				return createNewQueryHelper(DEFAULT_ELEMENT_IDENTIFIER)
						.withinPageLoadTimeout()
						.asExisting()
						.expectExactlyOne()
						.executeForOne();
			}
		};
	}

	/* (non-Javadoc)
	 * @see com.ibm.commerce.qa.wte.framework.page.RelativePageFactory#setIdentifierQueriesRelativeTo(com.ibm.commerce.qa.wte.framework.page.WteElement)
	 */
	@Override
	public void setIdentifierQueriesRelativeTo(Supplier<WteElement> p_rootQuery) {
		checkArgument(p_rootQuery != null, "element == null");
		checkState(f_rootQuery == null, "Can only set root query once.");
		
		// Create a query which will cache the element if it can.
		f_rootQuery = Suppliers2.memoizeWithExpiration(p_rootQuery, 
				WteElements.isNullOrStaleFunc());
	}

	@Override
	public WteElement getElementIdentifierQueriesRelativeTo() {
		return getRootElementFunction().get();
	}
	
	/* (non-Javadoc)
	 * @see com.ibm.commerce.qa.wte.framework.page.RelativePageFactory#createPageQuery(com.ibm.commerce.qa.wte.framework.page.Identifier)
	 */
	@Override
	public IdentifierQueryBuilder createPageQuery(Identifier p_identifier) {
		if (p_identifier.isRelativeExpression()) {
			// Expression is relative, yet trying to use it against the root 
			// node -- it is not necessarily an error, but ill-advised
			f_log.info(String.format(
					"Using relative Identifier (%s) in a page-wide (root-based) query, " +
					"did you mean to use RelativePageFactory#createQuery(Identifier)?", 
					p_identifier));
		}
		
		return createNewQueryHelper(p_identifier);
	}
	
	/**
	 * Returns the root element for the factory
	 * @return Root element function for the factory
	 */
	private Supplier<WteElement> getRootElementFunction() {
		if (f_rootQuery != null) {
			// set, don't use default
			return f_rootQuery;
		} else {
			// not set, use default
			return getDefaultQuery();
		}
	}
	
	@Override
	public IdentifierQueryBuilder createQuery(Identifier p_identifier) {
		if (!p_identifier.isRelativeExpression()) {
			// Expression is not relative, yet trying to use relatively
			// it is not necessarily an error, but ill-advised
			f_log.warning(String.format(
					"Using absolute Identifier (%s) in a relative query, " +
					"use RelativePageFactory#createPageQuery(Identifier) instead%n" + 
					"For XPath, a relative query generally starts with \"./\" signifying " + 
					"the \"current\" node.%n" +
					"\tFor example, Identifier.byXPath(\".//span\") is relative, " +
					"and Identifier.byXPath(\"//span\") is absolute from " +
					"the root node.", 
					p_identifier));
		}
		
		return createNewQueryHelper(p_identifier)
				.relativeToElement(getRootElementFunction());
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(getClass())
				.add("rootElement", f_rootQuery)
				.omitNullValues()
				.toString();
	}
	
	@Override
	public PageBuilder createRelativePageSection() {
		PageBuilder pageBuilder = this.createPage();
		pageBuilder.withPageFactory(this);
		return pageBuilder;
	}

}
