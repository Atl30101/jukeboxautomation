package com.ibm.commerce.qa.wte.s2impl.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2015
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import org.openqa.selenium.interactions.CompositeAction;

import com.ibm.commerce.qa.wte.framework.page.ElementCompositeAction;
/**
 * selenium implementation of ElementCompositeAction
 *
 */
public class Selenium2ElementCompositeAction implements ElementCompositeAction
{
	private final CompositeAction compositeAction;
	
	/**
	 * 
	 */
	Selenium2ElementCompositeAction(CompositeAction compositeAction)
	{
		this.compositeAction = compositeAction;
	}

	@Override
	public int getNumberOfActions()
	{
		return this.compositeAction.asList().size();
	}

	@Override
	public void perform()
	{
		this.compositeAction.perform();
	}

}
