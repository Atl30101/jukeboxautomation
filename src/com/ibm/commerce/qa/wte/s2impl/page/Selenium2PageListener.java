package com.ibm.commerce.qa.wte.s2impl.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2015
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.lang.reflect.Method;
import java.util.List;
import java.util.logging.Logger;

import com.google.inject.Provider;
import com.google.inject.spi.InjectionListener;
import com.google.inject.spi.TypeEncounter;
import com.ibm.commerce.qa.wte.framework.page.AbstractPageListener;
import com.ibm.commerce.qa.wte.framework.page.PageFactory;
import com.ibm.commerce.qa.wte.framework.page.PageObject.PageType;
import com.ibm.commerce.qa.wte.framework.page.ParentPageContainer;
import com.ibm.commerce.qa.wte.framework.util.WteAnnotationUtil;

/**
 * This class is used by Guice to identify page objects and perform page load
 * validation.
 * 
 * 
 * 
 */
public class Selenium2PageListener extends AbstractPageListener
{
	/**
	 * The page listener constructor.
	 */
	public Selenium2PageListener()
	{
	}

	@Override
	protected <I> InjectionListener<I> createInjectionListener(
			final TypeEncounter<I> encounter)
	{
		return new InjectionListener<I>()
		{
			/*
			 * Fetch the providers from Guice for objects we want. Do not
			 * directly get the objects (by calling get() on the providers)
			 * here, otherwise a NPE may result. They should not be fetched
			 * until inside the afterInjection method.
			 */
			
			// Fetch the log object provider from guice
			private final Provider<Logger> logProvider = encounter.getProvider(Logger.class);
			
			// Fetch the page factory provider from guice
			private final Provider<PageFactory> pageFactoryProvider = encounter.getProvider(PageFactory.class);

			// Fetch the parent page manager provider
			private final Provider<ParentPageContainer> pageParentContainerProvider = encounter.getProvider(ParentPageContainer.class);
			
			
			@Override
			public void afterInjection(I injectee)
			{
				final Logger log = logProvider.get();
				final PageFactory factory = pageFactoryProvider.get();
				final ParentPageContainer parentPageContainer = pageParentContainerProvider.get();
				
				/*
				 * If this is page has a PageObject annotation (not a child
				 * page), then tell the parent page manager this is a new parent
				 * page.
				 */
				if (WteAnnotationUtil.isPageObjectType(injectee.getClass(), PageType.MAIN_WINDOW))
				{
					parentPageContainer.setPage(injectee);
				}
				
				/*
				 *  no longer doing any page validation here
				 *  When the page section is injected, it may not know its root element (in case of using RelativePageFactory,
				 *  and therefore, it can not do the proper page validation if the identifier is relative to its root element.
				 *  
				 *  The page validation will be postponed to the time that the requested page is injected.
				 *  i.e. after the page is built using PageBuilder, the page validation will take place in following order:
				 *  1. any PageObject injected to the page
				 *  2. any PageObject annotated with @PageSection
				 *  3. the page itself
				 */
			}
		};
	}

}
