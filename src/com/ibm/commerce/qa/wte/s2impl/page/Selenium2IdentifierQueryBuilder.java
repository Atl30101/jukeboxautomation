package com.ibm.commerce.qa.wte.s2impl.page;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Objects;
import com.google.common.base.Objects.ToStringHelper;
import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.ibm.commerce.qa.wte.framework.locator.ByAngular;
import com.ibm.commerce.qa.wte.framework.page.BrowserType;
import com.ibm.commerce.qa.wte.framework.page.ElementCountException;
import com.ibm.commerce.qa.wte.framework.page.ElementState;
import com.ibm.commerce.qa.wte.framework.page.ElementTimeoutType;
import com.ibm.commerce.qa.wte.framework.page.ElementsQtyExpected;
import com.ibm.commerce.qa.wte.framework.page.ElementsQtyExpectedFactory;
import com.ibm.commerce.qa.wte.framework.page.Identifier;
import com.ibm.commerce.qa.wte.framework.page.Identifier.ByBinding;
import com.ibm.commerce.qa.wte.framework.page.Identifier.ByCssSelector;
import com.ibm.commerce.qa.wte.framework.page.Identifier.ByExactBinding;
import com.ibm.commerce.qa.wte.framework.page.Identifier.ByLinkText;
import com.ibm.commerce.qa.wte.framework.page.Identifier.ByModel;
import com.ibm.commerce.qa.wte.framework.page.Identifier.ByRepeater;
import com.ibm.commerce.qa.wte.framework.page.IdentifierQueryBuilder;
import com.ibm.commerce.qa.wte.framework.page.IdentifierVisitor;
import com.ibm.commerce.qa.wte.framework.page.WteElement;
import com.ibm.commerce.qa.wte.framework.test.WebSession;
import com.ibm.commerce.qa.wte.framework.util.ConfigManager;
import com.ibm.commerce.qa.wte.framework.util.WteElements;
import com.ibm.commerce.qa.wte.s2impl.test.Selenium2WebSession;
import com.ibm.commerce.qa.wte.s2impl.util.Suppliers2;

/**
 *  A Selenium {@link IdentifierQueryBuilder} implementation that will apply a find
 * function to locate all elements with the specified identifier query
 *
 */
public class Selenium2IdentifierQueryBuilder implements IdentifierQueryBuilder
{
	private static final ElementTimeoutType DEFAULT_TIMEOUT_TYPE = ElementTimeoutType.IMPLICIT;
	private static final ElementState DEFAULT_STATE = ElementState.VISIBLE;
	
	private final Logger log;
	
	private final ConfigManager configManager;
	
	private final Selenium2WebSession f_session;
	
	private By by;

	private boolean timeoutTypeSet;
	private ElementTimeoutType timeoutType;
	private long timeoutTime;
	private TimeUnit timeoutUnit;
	
	private ElementState state;
	
	private final Provider<IdentifierQueryBuilder> f_pIdentQueryBuilder;
	
	private final ElementsQtyExpectedFactory f_expectedFactory;
	private ElementsQtyExpected f_expectedQty;
	private boolean f_expectedQtySet;
	
	private Supplier<WteElement> rootElementFunction;
	
	/**
	 * 
	 * @param log
	 * @param configManager
	 * @param session
	 */
	@Inject
	Selenium2IdentifierQueryBuilder(
			Logger log, 
			ConfigManager configManager,
			Provider<IdentifierQueryBuilder> p_pIdentQueryBuilder,
			WebSession session, 
			ElementsQtyExpectedFactory expectedFactory)
	{
		this.log = log;
		this.configManager = configManager;
		this.f_session = (Selenium2WebSession)session;
		this.f_pIdentQueryBuilder = p_pIdentQueryBuilder;
		this.f_expectedFactory = expectedFactory;
		
		f_expectedQtySet = false;
		f_expectedQty = expectedFactory.all();
	}

	@Override
	public IdentifierQueryBuilder of(Identifier identifier)
	{
		if (this.by != null)
		{
			throw new IllegalStateException("The identifier was already set for this builder.");
		}

		if (identifier == null)
		{
			throw new IllegalArgumentException("The element query cannot be null.");
		}
		
		// Visit to discover the type and build the By object accordingly
		identifier.visit(new IdentifierVisitor()
		{
			@Override
			public void accept(Identifier.ById byId)
			{
				setBy(By.id(byId.getExpression()));
			}

			@Override
			public void accept(Identifier.ByClassName byClassName)
			{
				setBy(By.className(byClassName.getExpression()));
			}

			@Override
			public void accept(Identifier.ByPartialLinkText byPartialLinkText)
			{
				setBy(By.partialLinkText(byPartialLinkText.getExpression()));
			}

			@Override
			public void accept(Identifier.ByXPath xPath)
			{
				setBy(By.xpath(xPath.getExpression()));
			}

			@Override
			public void accept(Identifier.ByName byName) {
				setBy(By.name(byName.getExpression()));
			}

			@Override
			public void accept(ByLinkText byLinkText)
			{
				setBy(By.linkText(byLinkText.getExpression()));				
			}

			@Override
			public void accept(ByCssSelector byCssSelector)
			{
				setBy(By.cssSelector(byCssSelector.getExpression()));
			}

			@Override
			public void accept(ByModel byModel) {
				setBy(By.xpath(".//*[@ng-model='" + byModel.getExpression() + "']"));
			}

			@Override
			public void accept(ByBinding byBinding) {
				setBy(ByAngular.byAngularBinding(
						f_session.getS2WebDriver(), 
						byBinding.getExpression(), 
						false));
			}
			
			@Override
			public void accept(ByExactBinding byBinding) {
				setBy(ByAngular.byAngularBinding(
						f_session.getS2WebDriver(), 
						byBinding.getExpression(), 
						true));
			}

			@Override
			public void accept(ByRepeater byRepeater) {
				setBy(ByAngular.byAngularRepeater(
						f_session.getS2WebDriver(), 
						byRepeater.getExpression(),
						byRepeater.getIndex(),
						byRepeater.getBinding()));
				
			}
		});

		return this;
	}
	
	/**
	 * Sets the {@code by} field
	 * @param p_newBy By instance
	 */
	protected void setBy(final By p_newBy) {
		Preconditions.checkNotNull(p_newBy);
		
		this.by = p_newBy;
	}

	/**
	 * Returns the timeout type. If it is currently <code>null</code>, it will
	 * return the default.
	 * 
	 * @return
	 */
	private ElementTimeoutType getCalculatedTimeoutType()
	{
		return this.timeoutType == null ? DEFAULT_TIMEOUT_TYPE : this.timeoutType;
	}
	
	/**
	 * Returns the element state. If it is currently <code>null</code>, it will
	 * return the default.
	 * 
	 * @return
	 */
	private ElementState getCalculatedState()
	{
		return this.state == null ? DEFAULT_STATE : this.state;
	}
	
	private WebDriverWait genWait(ElementTimeoutType timeout)
	{
		WebDriverWait result = null;
		WebDriver driver = f_session.getS2WebDriver();
		switch(timeout)
		{
			case IMPLICIT:
				result = new WebDriverWait(driver, configManager.getImplicitTimeoutSeconds());
				break;
			case PAGE_LOAD:
				result = new WebDriverWait(driver, configManager.getPageLoadTimeoutSeconds());
				break;
			case SCRIPT:
				result = new WebDriverWait(driver, configManager.getScriptTimeoutSeconds());
				break;
			case AJAX:
				result = new WebDriverWait(driver, configManager.getAjaxTimeoutSeconds());
				break;
			case EXPLICIT:
				result = new WebDriverWait(driver, TimeUnit.SECONDS.convert(this.timeoutTime, this.timeoutUnit));
				break;
			default:
				throw new UnsupportedOperationException("The timeout type '"
					+ this.timeoutType.toString() + "' is unsupported.");
		}
		
		result.ignoring(StaleElementReferenceException.class);
		

		/*
		 * Due to a bug in the WebDriver v2.25 with the Internet Explorer
		 * driver, a NullPointerException is possible when calling
		 * driver.findElements(). Until this is fixed we need to ignore it. I
		 * believe this is defect
		 * http://code.google.com/p/selenium/issues/detail?id=3800. There also
		 * seems to be an isDisplayed issue where it can sometimes throw
		 * WebDriverException. And finally, a ClassCastException has also been
		 * seen.
		 */
		if (configManager.getBrowserType() == BrowserType.INTERNET_EXPLORER)
		{
			result.ignoring(WebDriverException.class);
			result.ignoring(NullPointerException.class);
			result.ignoring(ClassCastException.class);
		}
		
		return result;
	}
	
	@Override
	@Deprecated
	public WteElement find()
	{
		List<WteElement> elems = Lists.newArrayList();
		if (executeChecked(elems) && 
				!elems.isEmpty()) {
			// success
			return elems.get(0);
		}
		
		return null;
	}

	@Override
	@Deprecated
	public WteElement findExactlyOne()
	{
		return executeForOne();
	}

//	@Deprecated
//	private WteElement find(ElementQuantity qty)
//	{
//		ElementTimeoutType calcTimeoutType = getCalculatedTimeoutType();
//		
//		WebDriverWait wait = genWait(calcTimeoutType);
//
//		ElementState calcElementState = getCalculatedState();
//
//		WebElement weResult = null;
//		
//		// Perform core search
//		Selenium2FindExpectedCondition selenium2FindExpectedCondition;
//		if (!isParentUsed()) {
//			// no parent to use
//			selenium2FindExpectedCondition = new Selenium2FindExpectedCondition(
//					this.by, calcElementState, qty);
//		} else {
//			// we have a parent: 
//			selenium2FindExpectedCondition = 
//					new Selenium2FindExpectedCondition(this.by, 
//							calcElementState, qty, 
//							((Selenium2Element)getParentElement())
//								.getBackingWebElement());
//		}
//		try
//		{
//			List<WebElement> resultingElements = wait.until(selenium2FindExpectedCondition);
//			
//			switch(calcElementState)
//			{
//				case VISIBLE:
//				case EXISTS:
//				case HIDDEN:
//					weResult = resultingElements.get(0);
//					break;
//				case NON_EXISTING:
//					// weResult == null from previous initializer
//					break;
//				default:
//					throw new UnsupportedOperationException("The state '"
//						+ calcElementState.toString() + "' is unsupported. ");
//			}
//		}
//		catch (TimeoutException e)
//		{
//			switch(calcElementState)
//			{
//				case VISIBLE:
//				case EXISTS:
//				case HIDDEN:
//					if (qty == ElementQuantity.EXACTLY_ONE)
//					{
//						throw new ElementCountException(selenium2FindExpectedCondition.getLastResults().size(), 1, 
//								selenium2FindExpectedCondition.getLastAnyStateResults().size() , 
//								"Expecting to find exactly one element.", e);
//					}
//					break;
//				case NON_EXISTING:
//					if (qty == ElementQuantity.EXACTLY_ONE)
//					{
//						throw new ElementCountException(selenium2FindExpectedCondition.getLastResults().size(), 0, 
//								selenium2FindExpectedCondition.getLastAnyStateResults().size(),
//								"Expecting to find zero elements.", e);
//					}
//					else
//					{
//						weResult = selenium2FindExpectedCondition.getLastResults().get(0);
//					}
//					break;
//				default:
//					throw new UnsupportedOperationException("The state '"
//						+ calcElementState.toString() + "' is unsupported.");
//			}
//		}
//
//		// If a web element was found, wrap it as a selenium element
//		WteElement result = null;
//		if (weResult != null)
//		{
//			result = new Selenium2Element(log, configManager, webDriver, weResult);	
//		}
//		
//		return result;
//	}

	@Override
	@Deprecated
	public List<WteElement> findAll()
	{
		return execute();
	}
	
	// cleans up a boilerplate null check, do not use outside executeHelper
	private void executeHelper_throwOrStore(List<RuntimeException> buff, RuntimeException e) {
		if (buff == null) {
			throw e;
		}
		
		buff.add(e);
	}
	
	/**
	 * Executes the current identifier query, results are stored in the output 
	 * buffer.
	 * @param outputBuffer List that should have constant insert time, can be 
	 * 			{@code null} which implies that no output is saved.  
	 * @param exceptions if {@code null}, this means to throw the exception 
	 * 		immediately. 
	 */
	private void executeHelper(List<WteElement> outputBuffer, List<RuntimeException> exceptions) 
		throws ElementCountException {
		ElementTimeoutType calcTimeoutType = getCalculatedTimeoutType();
		
		WebDriverWait wait = genWait(calcTimeoutType);

		ElementState calcElementState = getCalculatedState();
		
		List<WebElement> weResult = null;
		
		// Perform core search
		Selenium2FindExpectedCondition s2FindExpectedCondition;
		if (!isParentUsed()) {
			// No parent for this element
			s2FindExpectedCondition = new Selenium2FindExpectedCondition(
					this.by, calcElementState, f_expectedQty);
		} else {
			// we have a parent: 
			s2FindExpectedCondition = new Selenium2FindExpectedCondition(
					this.by, calcElementState, f_expectedQty, 
					((Selenium2Element)getRootElement()).getBackingWebElement());
		}
		
		
	   
		try
		{
			List<WebElement> resultingElements = Collections.emptyList();
			try {
				resultingElements = wait.until(s2FindExpectedCondition);
			} catch (ElementCountException ece) {
				executeHelper_throwOrStore(exceptions, ece);
				return ;
			}
			
			switch(calcElementState)
			{
				case VISIBLE:
				case EXISTS:
				case HIDDEN:
					weResult = resultingElements;
					break;
				case NON_EXISTING:
					weResult = Collections.emptyList();
					break;
				default:
					executeHelper_throwOrStore(exceptions, new UnsupportedOperationException("The state '"
						+ calcElementState.toString() + "' is unsupported."));
					return ;
			}
		}
		catch (TimeoutException e)
		{
			switch(calcElementState)
			{
				case VISIBLE:
				case EXISTS:
				case HIDDEN:
					//throw new ElementCountException(0, 1, "Expecting to find at least one element.", e);
					executeHelper_throwOrStore(exceptions, e);
					weResult = Collections.emptyList();
					break;
				case NON_EXISTING:
					executeHelper_throwOrStore(exceptions, e);
					weResult = s2FindExpectedCondition.getLastResults();
					break;
				default:
					executeHelper_throwOrStore(exceptions, new UnsupportedOperationException("The state '"
						+ calcElementState.toString() + "' is unsupported."));
					return ;
			}
		}
		// use temp buffer in-case of exception
		List<WteElement> tempOut = Lists.newArrayListWithCapacity(weResult.size());
		for (WebElement webElement : weResult)
		{
			tempOut.add(new Selenium2Element(log, configManager, f_session, webElement));
		}
		
		if (outputBuffer != null) {
			outputBuffer.addAll(tempOut);
		}
	}
	
	@Override
	public boolean executeChecked(List<WteElement> p_outputBuffer) {
		checkNotNull(p_outputBuffer, "outputBuffer == null");
		
		List<WteElement> outBuff = Lists.newLinkedList();
		List<RuntimeException> ebuff = Lists.newArrayListWithCapacity(1);
		executeHelper(outBuff, ebuff);
		
		if (ebuff.isEmpty()) {
			// successful, fill the output
			p_outputBuffer.clear();
			p_outputBuffer.addAll(outBuff);
		} // not successful, don't fill it
		
		return (ebuff.isEmpty());
	}
	
	@Override
	public boolean executeChecked() {
		List<RuntimeException> ebuff = Lists.newArrayListWithCapacity(1);
		executeHelper(null, ebuff);
		
		return (ebuff.isEmpty());
	}

	@Override
	public List<WteElement> execute() {
		List<WteElement> outBuff = Lists.newLinkedList();
		executeHelper(outBuff, null);
		
		return Lists.newArrayList(outBuff);
	}
	
	@Override
	public WteElement executeForOne() {
		checkState(!f_expectedQty.equals(f_expectedFactory.none()), 
				"Can not find \'no\' element, do not use #expectNone() or " +
				"#asNonExistant() with #executeForOne(), " +
				"use #executeChecked() instead");
		
		if (f_expectedQty.equals(f_expectedFactory.all())) {
			expectMoreThan(0);
		}
		
		List<WteElement> elems = execute();
		if (elems.isEmpty()) {
			throw new IllegalStateException("Result from execute() is empty {queryBuilder=" + this + "}");
		}
		return elems.get(0);
	}
	
	private boolean isParentUsed() {
		return rootElementFunction != null;
	}
	
	private WteElement getRootElement() {
		checkState(isParentUsed(), "parent query is not being used");
		return rootElementFunction.get();
	}

	@Override
	public IdentifierQueryBuilder withinExplicitTimeout(long time,
			TimeUnit unit)
	{
		checkState(!timeoutTypeSet, 
				"The wait type was already set for this builder.");
		
		this.timeoutTypeSet = true;
		this.timeoutType = ElementTimeoutType.EXPLICIT;
		this.timeoutTime = time;
		this.timeoutUnit = unit;

		return this;
	}
		
	@Override
	public IdentifierQueryBuilder withinImplicitTimeout()
	{
		checkState(!timeoutTypeSet, 
				"The wait type was already set for this builder.");
		
		this.timeoutTypeSet = true;
		this.timeoutType = ElementTimeoutType.IMPLICIT;

		return this;
	}

	@Override
	public IdentifierQueryBuilder withinPageLoadTimeout()
	{
		checkState(!timeoutTypeSet, 
				"The wait type was already set for this builder.");
		
		this.timeoutTypeSet = true;
		this.timeoutType = ElementTimeoutType.PAGE_LOAD;

		return this;
	}

	private IdentifierQueryBuilder withState(ElementState p_state)
	{
		checkState(this.state == null, 
				"A state was already set for this builder.");
		
		this.state = p_state;
		return this;
	}

	@Override
	public IdentifierQueryBuilder withinAjaxTimeout()
	{
		checkState(!timeoutTypeSet, 
				"The wait type was already set for this builder.");
		
		this.timeoutTypeSet = true;
		this.timeoutType = ElementTimeoutType.AJAX;

		return this;
	}

	@Override
	public IdentifierQueryBuilder withinScriptTimeout()
	{
		checkState(!timeoutTypeSet, 
				"The wait type was already set for this builder.");
		
		this.timeoutTypeSet = true;
		this.timeoutType = ElementTimeoutType.SCRIPT;

		return this;
	}

	@Override
	public IdentifierQueryBuilder asExisting()
	{
		withState(ElementState.EXISTS);
		return this;
	}

	@Override
	public IdentifierQueryBuilder asHidden()
	{
		withState(ElementState.HIDDEN);
		return this;
	}

	@Override
	public IdentifierQueryBuilder asVisible()
	{
		withState(ElementState.VISIBLE);
		return this;
	}
	
	@Override
	public IdentifierQueryBuilder asNonExistent() {
		withState(ElementState.NON_EXISTING);
		expectHelper(f_expectedFactory.none(), true);
		
		return this;
	}
	
	@Override
	public IdentifierQueryBuilder selectFrame() {
		Selenium2Element frameElement = (Selenium2Element)findExactlyOne();
		f_session.getS2WebDriver().switchTo().frame(frameElement.toWebElement());
		return this;
	}
	
	@Override
	public IdentifierQueryBuilder asParentToNewQuery(Identifier p_childIdentifier) {
		return f_pIdentQueryBuilder.get()
				.of(p_childIdentifier)
				.relativeToQueryBuilder(this);
	}

	@Override
	public IdentifierQueryBuilder relativeToElement(Supplier<WteElement> p_parent) {
		checkNotNull(p_parent, "parent == null");
		checkState(rootElementFunction == null, 
				"parent is already set, can only be set once");
		
		rootElementFunction = Suppliers.memoize(p_parent);
		
		return this;
	}

	@Override
	public IdentifierQueryBuilder relativeToQueryBuilder(
			final IdentifierQueryBuilder p_parentBuilder) {
		checkNotNull(p_parentBuilder, "parent can not be null");
		checkState(rootElementFunction == null, 
				"parent is already set, can only be set once.");
		
		rootElementFunction = Suppliers2.memoizeWithExpiration(new Supplier<WteElement>() {
				@Override
				public WteElement get() {
					return p_parentBuilder.executeForOne();
				}
			}, WteElements.isNullOrStaleFunc());
		
		return this;
	}
	
	@Override
	public String toString() {
		ToStringHelper helper = Objects.toStringHelper(getClass())
				.omitNullValues()
				.add("by", by)
				.add("timeoutType", timeoutType)
				.add("elementState", state)
				.add("expectedQuanity", f_expectedQty);
		
		if (isParentUsed()) {
			helper.add("parentElement", getRootElement());
		}
		
		return helper.toString();
	}

	private void expectHelper(ElementsQtyExpected newQty, boolean explicitSet) {
		if (!f_expectedQtySet) {
			// we can set it without checking, we didn't set it yet
			f_expectedQty = newQty;
			
			// for good measure, always set it
			f_expectedQtySet = explicitSet;
		} else {
			// it's already set, error if its not set yet, otherwise no action
			if (!newQty.equals(f_expectedQty)) {
				// it's already set AND its different
				throw new IllegalStateException(String.format(
						"Expected Qty already set to: %s, attempted to reset to: %s", 
						f_expectedQty, newQty));
			}
			
			// already set to this same one, no action.
			log.info(String.format("Duplicate expectXXX() calls made, " +
					"tried reset expected condition to %s", newQty));
		}
	}
	
	@Override
	public IdentifierQueryBuilder expectExactly(int p_amount) {
		expectHelper(f_expectedFactory.exactly(p_amount), true);
		
		return this;
	}

	@Override
	public IdentifierQueryBuilder expectExactlyOne() {
		expectHelper(f_expectedFactory.exactlyOne(), true);
		
		return this;
	}

	@Override
	public IdentifierQueryBuilder expectAll() {
		expectHelper(f_expectedFactory.all(), true);
		
		return this;
	}

	@Override
	public IdentifierQueryBuilder expectMoreThan(int p_amount) {
		expectHelper(f_expectedFactory.moreThan(p_amount), true);
		
		return this;
	}

	@Override
	public IdentifierQueryBuilder expectLessThan(int p_amount) {
		expectHelper(f_expectedFactory.lessThan(p_amount), true);
		
		return this;
	}

	@Override
	public IdentifierQueryBuilder expectWithin(int p_minimum, int p_maximum) {
		expectHelper(f_expectedFactory.within(p_minimum, p_maximum), true);
		
		return this;
	}
	
}
