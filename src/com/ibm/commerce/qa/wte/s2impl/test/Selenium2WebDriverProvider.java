/**
 * 
 */
package com.ibm.commerce.qa.wte.s2impl.test;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2015
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import io.selendroid.client.SelendroidDriver;
import io.selendroid.common.SelendroidCapabilities;

import java.awt.AWTException;
import java.awt.Robot;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.ibm.commerce.qa.wte.framework.util.ConfigManager;
import com.ibm.commerce.qa.wte.framework.util.WindowState;

/**
 * Provider for {@link WebDriver} instances, this will load the appropriate 
 * driver for the data stored in a config file. 
 * 
 * @since Apr 21, 2014
 *
 */
public class Selenium2WebDriverProvider implements Provider<WebDriver> {

	private final Logger log;
	private final ConfigManager config;
	
	/**
	 * Creates new instance of Selenium2WebDriverProvider.
	 * 
	 * @param log
	 * @param config
	 */
	@Inject
	Selenium2WebDriverProvider(Logger log, ConfigManager config) {
		this.log = log;
		this.config = config;
	}
	
	@Override
	public WebDriver get() {
		WebDriver result = null;
		boolean visualDriver = false;
		DesiredCapabilities dc = null;
		
		log.info("Loading Selenium WebDriver corresponding to " + config.getBrowserType().toString() + " browser.");
		
		Proxy proxy = null;
		if (config.getProxyHostName() != null)
		{
			String proxyString = config.getProxyHostName() + ":" + config.getProxyPort();
			log.info("Using proxy with value: '" + proxyString + "'");
			proxy = new Proxy();
			proxy.setHttpProxy(proxyString)
				.setFtpProxy(proxyString)
				.setSslProxy(proxyString);
		}

		/*
		 * If a remote browser host name was provided, set the WebDriver to "grid" mode, to communicate to a Remote WebDriver Server on another machine that actually launches the browser. 
		 */
		URL remoteWebDriverUrl = null;
		if (config.getRemoteBrowserHostName() != null)
		{
			String urlString = null;
			try
			{
				urlString = "http://" + config.getRemoteBrowserHostName() + ":" + config.getRemoteBrowserPort() + "/wd/hub";
				log.info("Using Remote WebDriver with URL: '" + urlString + "'");
				remoteWebDriverUrl = new URL(urlString);
			}
			catch (MalformedURLException e)
			{
				throw new IllegalStateException(
					"There is a problem with the remote web driver URL. Please check the remote host name in the configuration. The URL is: '"
							+ urlString + "'", e);
			}
		}

		/*
		 * handle user agents
		 */
//		String userAgent = null;
//		if(config.getUserAgent() != null){
//			if(config.getBrowserType() == BrowserType.FIREFOX) {
//				userAgent = config.getUserAgent();
//			}
//			
//		}
//		
		/*
		 * FIXME: Move this switch into the enum BrowserType.. 
		 */
		switch (config.getBrowserType())
		{
		case HTML_UNIT:
			dc = DesiredCapabilities.htmlUnit();
			
			// Override with any extra caps from the config
			parseExtraCapabilities(log, dc, config.getSelenium2WebDriverCapabilities());
			
			if (remoteWebDriverUrl != null)
			{
				result = new RemoteWebDriver(remoteWebDriverUrl, dc);
			}
			else
			{
				result = new HtmlUnitDriver(dc);
			}
			
			break;
		case FIREFOX:
			log.warning("KEEP MOUSE AWAY FROM BROWSER WINDOW WHILE TEST IS EXECUTING!");
			visualDriver = true;
			
			dc = DesiredCapabilities.firefox();
			
			//log.info("Use native events in WebDriver: " + config.getUseNativeEvents());
			dc.setCapability(CapabilityType.HAS_NATIVE_EVENTS, config.getUseNativeEvents());

			if (proxy != null)
			{
				dc.setCapability(CapabilityType.PROXY, proxy);
			}

			// Override with any extra caps from the config
			parseExtraCapabilities(log, dc, config.getSelenium2WebDriverCapabilities());
			
			if (remoteWebDriverUrl != null)
			{
				result = new RemoteWebDriver(remoteWebDriverUrl, dc);
			}
			else if(config.getUserAgent() != null){
					FirefoxProfile profile = new FirefoxProfile();
					profile.setPreference("general.useragent.override", config.getUserAgent());
					dc.setCapability(FirefoxDriver.PROFILE, profile);
					
					result = new FirefoxDriver(dc);
			}
			else
			{
				result = new FirefoxDriver(dc);
			}

			break;
		case INTERNET_EXPLORER:
			log.warning("KEEP MOUSE AWAY FROM BROWSER WINDOW WHILE TEST IS EXECUTING!");
			visualDriver = true;
			
			System.setProperty("webdriver.ie.driver", config.getIeDriverServerPath());

			dc = DesiredCapabilities.internetExplorer();
			dc.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true); 

			if (proxy != null)
			{
				dc.setCapability(CapabilityType.PROXY, proxy);
			}

			// Override with any extra caps from the config
			parseExtraCapabilities(log, dc, config.getSelenium2WebDriverCapabilities());
			
			if (remoteWebDriverUrl != null)
			{
				result = new RemoteWebDriver(remoteWebDriverUrl, dc);
			}
			else
			{
				result = new InternetExplorerDriver(dc);
			}

			break;
		case CHROME:
			log.warning("KEEP MOUSE AWAY FROM BROWSER WINDOW WHILE TEST IS EXECUTING!");
			visualDriver = true;
			
			if (proxy != null)
			{
				throw new UnsupportedOperationException("As of this writing, it is unknown how to programmatically set the proxy for Chrome. You need to do it manually.");
			}

			System.setProperty("webdriver.chrome.driver", config.getChromeDriverServerPath());

			log.info("Loading Selenium WebDriver corresponding to " + config.getBrowserType().toString() + " browser.");

			// Override with any extra caps from the config
			parseExtraCapabilities(log, dc, config.getSelenium2WebDriverCapabilities());

			if (remoteWebDriverUrl != null)
			{
				result = new RemoteWebDriver(remoteWebDriverUrl, DesiredCapabilities.chrome());
			}
			else
			{
				result = new ChromeDriver();
			}

			break;
		case ANDROID:
			visualDriver = true;

			
			DesiredCapabilities caps = SelendroidCapabilities.android();
			caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);


			if (remoteWebDriverUrl != null)
			{
				throw new UnsupportedOperationException("Android Driver does not support remote Web Driver");
			}
			else
			{
				try {
					result = new SelendroidDriver(caps);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			break;
		default:
			throw new UnsupportedOperationException("The browser type '"
					+ config.getBrowserType().toString() + "' is unknown.");
		}

		/*
		 * If this is a driver that uses an actual browser window, check and
		 * perform any window configuration actions requested.
		 */
		if (visualDriver)
		{
			// Change the window state if requested
			if (config.getWindowState() != null)
			{
				log.info("Window state requested: " + config.getWindowState());
				switch(config.getWindowState())
				{
					case MAXIMIZE:
					{
						result.manage().window().maximize();
						break;
					}
					case NORMAL:
					{
						log.warning("As of this writing, it is not possible to guarantee a browser window is in the normal (restored) state.");
						break;
					}
					default:
					{
						throw new UnsupportedOperationException("Changing window state to '" + config.getWindowState() + "' is unsupported.");
					}
				}
			}

			/**
			 * If the window state was not set or was set to NORMAL, check if
			 * the window position and size should be altered.
			 */
			if (config.getWindowState() == null || config.getWindowState() == WindowState.NORMAL)
			{
				// Change window location if requested
				if (config.getWindowLeftPosition() != null || config.getWindowTopPosition() != null)
				{
					Point position = result.manage().window().getPosition();
					if (config.getWindowLeftPosition() != null)
					{
						log.info("Window left position requested: " + config.getWindowLeftPosition());
						position = new Point(config.getWindowLeftPosition(), position.getY());
					}
					
					if (config.getWindowTopPosition() != null)
					{
						log.info("Window top position requested: " + config.getWindowTopPosition());
						position = new Point(position.getX(), config.getWindowTopPosition());
					}
					
					result.manage().window().setPosition(position);
				}
				
				// Change the window size if requested
				if (config.getWindowWidth() != null || config.getWindowHeight() != null)
				{
					Dimension size = result.manage().window().getSize();
					if (config.getWindowWidth() != null)
					{
						log.info("Window width requested: " + config.getWindowWidth());
						size = new Dimension(config.getWindowWidth(), size.getHeight());
					}
					
					if (config.getWindowHeight() != null)
					{
						log.info("Window height requested: " + config.getWindowHeight());
						size = new Dimension(size.getWidth(), config.getWindowHeight());
					}
					
					result.manage().window().setSize(size);
				}
			}
		}

		/*
		 * Relocate mouse if a coordinate was specified and this is not remote.
		 */
		if (remoteWebDriverUrl == null 
				&& (config.getMouseRelocateLeftPosition() != null || config.getMouseRelocateTopPosition() != null))
		{
			log.info("Attempting to relocate mouse (away from browser window).");
			
			Point mousePosition = result.manage().window().getPosition();
			
			if (config.getMouseRelocateLeftPosition() != null)
			{
				log.info("Mouse left position requested: " + config.getMouseRelocateLeftPosition());
				mousePosition = new Point(config.getMouseRelocateLeftPosition(), mousePosition.getY());
			}
			
			if (config.getMouseRelocateTopPosition() != null)
			{
				log.info("Mouse top position requested: " + config.getMouseRelocateTopPosition());
				mousePosition = new Point(mousePosition.getX(), config.getMouseRelocateTopPosition());
			}

			try
			{
				Robot r = new Robot();
				r.mouseMove(mousePosition.getX(), mousePosition.getY());
			}
			catch (AWTException e)
			{
				log.log(Level.WARNING, "Could not move the mouse.", e);
			}
		}

		return result;
	}
	
	private static void parseExtraCapabilities(Logger log, DesiredCapabilities cap, Map<String, String> extra)
	{
		for (String key : extra.keySet())
		{
			String value = extra.get(key);
			if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"))
			{
				log.info("Adding extra WebDriver boolean capability setting: '" + key + "'=" + value);
				cap.setCapability(key, Boolean.valueOf(value));
			}
			else
			{
				log.info("Adding extra WebDriver capability setting: '" + key + "'='" + value + "'");
				cap.setCapability(key, value);
			}
		}
	}

}
