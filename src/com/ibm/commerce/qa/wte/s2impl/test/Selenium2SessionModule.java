package com.ibm.commerce.qa.wte.s2impl.test;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2015
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import org.openqa.selenium.WebDriver;

import com.google.inject.PrivateModule;
import com.google.inject.Singleton;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.name.Names;
import com.ibm.commerce.qa.wte.framework.page.ElementActionsBuilder;
import com.ibm.commerce.qa.wte.framework.page.ElementQueryBuilder;
import com.ibm.commerce.qa.wte.framework.page.ElementsQtyExpected;
import com.ibm.commerce.qa.wte.framework.page.ElementsQtyExpectedFactory;
import com.ibm.commerce.qa.wte.framework.page.IdentifierQueryBuilder;
import com.ibm.commerce.qa.wte.framework.page.PageBuilder;
import com.ibm.commerce.qa.wte.framework.page.PageFactory;
import com.ibm.commerce.qa.wte.framework.page.ParentPageContainer;
import com.ibm.commerce.qa.wte.framework.page.RelativePageFactory;
import com.ibm.commerce.qa.wte.framework.test.WebSession;
import com.ibm.commerce.qa.wte.s2impl.page.Selenium2BaseModule;
import com.ibm.commerce.qa.wte.s2impl.page.Selenium2ElementActionsBuilder;
import com.ibm.commerce.qa.wte.s2impl.page.Selenium2ElementQueryBuilder;
import com.ibm.commerce.qa.wte.s2impl.page.Selenium2ElementsQtyExpected;
import com.ibm.commerce.qa.wte.s2impl.page.Selenium2IdentifierQueryBuilder;
import com.ibm.commerce.qa.wte.s2impl.page.Selenium2PageBuilder;
import com.ibm.commerce.qa.wte.s2impl.page.Selenium2PageFactory;
import com.ibm.commerce.qa.wte.s2impl.page.Selenium2RelativePageFactory;

/**
 * This should be used as a child module to {@link Selenium2BaseModule} in order
 * to link one instance of {@link WebDriver} to one session. This allows
 * multiple {@link WebDriver} sessions to be used at once. In other words, if
 * the user wants, they can have multiple tests with multiple browsers active at
 * the same time.
 * 
 * 
 * 
 */
public class Selenium2SessionModule extends PrivateModule
{
	@Override
	protected void configure()
	{
		/*
		 * Simple way to hide the WebDriver injection from other types, the 
		 * WebSession is exposed, but the driver is not 
		 */
		bind(WebSession.class)
				.to(Selenium2WebSession.class)
				.in(Singleton.class);
		expose(WebSession.class);
		
		bind(WebDriver.class)
				.toProvider(Selenium2WebDriverProvider.class);
		
		install(new FactoryModuleBuilder()
			.implement(ElementsQtyExpected.class, Names.named("exactly"), Selenium2ElementsQtyExpected.Exactly.class)
			.implement(ElementsQtyExpected.class, Names.named("exactlyOne"), Selenium2ElementsQtyExpected.ExactlyOne.class)
			.implement(ElementsQtyExpected.class, Names.named("all"), Selenium2ElementsQtyExpected.All.class)
			.implement(ElementsQtyExpected.class, Names.named("none"), Selenium2ElementsQtyExpected.None.class)
			.implement(ElementsQtyExpected.class, Names.named("moreThan"), Selenium2ElementsQtyExpected.MoreThan.class)
			.implement(ElementsQtyExpected.class, Names.named("lessThan"), Selenium2ElementsQtyExpected.LessThan.class)
			.implement(ElementsQtyExpected.class, Names.named("within"), Selenium2ElementsQtyExpected.Within.class)
			.build(ElementsQtyExpectedFactory.class));
		
		bind(PageFactory.class).to(Selenium2PageFactory.class).in(Singleton.class);
		expose(PageFactory.class);
		
		
		bind(RelativePageFactory.class).to(Selenium2RelativePageFactory.class);
		expose(RelativePageFactory.class);
		
		bind(ParentPageContainer.class).in(Singleton.class);
		expose(ParentPageContainer.class);
		
		bind(ElementActionsBuilder.class).to(Selenium2ElementActionsBuilder.class);
	
		bind(IdentifierQueryBuilder.class).to(Selenium2IdentifierQueryBuilder.class);
	
		bind(ElementQueryBuilder.class).to(Selenium2ElementQueryBuilder.class);
	
		bind(PageBuilder.class).to(Selenium2PageBuilder.class);
		
	}
		
}
