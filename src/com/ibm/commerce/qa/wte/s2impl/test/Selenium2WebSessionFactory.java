package com.ibm.commerce.qa.wte.s2impl.test;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.ibm.commerce.qa.wte.framework.test.WebSession;
import com.ibm.commerce.qa.wte.framework.test.WebSessionFactory;
import com.ibm.commerce.qa.wte.framework.util.ConfigManager;

/**
 *  Selenium implementation of WebSessionFactory
 *
 */
public class Selenium2WebSessionFactory implements WebSessionFactory
{
	private final Injector parentInjector;
	
	private final ConfigManager config;
	
	private List<Class<? extends Module>> configModules;
	/**
	 * Constructor
	 * @param parentInjector
	 * @param config
	 */
	@Inject
	public Selenium2WebSessionFactory(Injector parentInjector, ConfigManager config)
	{
		this.parentInjector = parentInjector;
		this.config = config;
	}

	@Override
	public ModuleSetBuilder with(Class<? extends Module> module) {
		return (new ModBuilder()).with(module);
	}
	
	@Override
	public WebSession create() {
		return create(Collections.<Class<? extends Module>>emptyList());
	}

	@Override
	public WebSession create(Collection<Class<? extends Module>> p_childModules) {
		
//		Set<Class<? extends Module>> allMods = Sets.newHashSet();
//		allMods.addAll(p_childModules);
//		allMods.add(Selenium2SessionModule.class);
//		allMods.addAll(getConfigModules());
//		
//		Set<Module> filteredMods = Sets.newHashSet(QAModule.loadModules(allMods));
//		
//		/*
//		 * Create the child injector representing this session by first
//		 * producing the session injector, and then apply the page object module
//		 * which is private, to ensure it is only seen when needed.
//		 */
//		Injector childInjector = parentInjector.createChildInjector(filteredMods);
		return parentInjector.getInstance(WebSession.class);
	}
	
	/*
	 * Reads config values for the modules and stores them for later, no need to 
	 * read twice
	 */
	private List<Class<? extends Module>> getConfigModules() {
		if (this.configModules == null) {
			// Attempt to instantiate the page object module
			this.configModules = ImmutableList.copyOf(this.config.getPageObjectModuleClasses());
		}
		
		return this.configModules;
	}
	
	private class ModBuilder implements ModuleSetBuilder {

		private Set<Class<? extends Module>> mods = Sets.newLinkedHashSet();
		
		public ModBuilder() {
			// default
		}

		@Override
		public ModuleSetBuilder with(Class<? extends Module> p_module) {
			mods.add(p_module);
			return this;
		}

		@Override
		public WebSession create() {
			return Selenium2WebSessionFactory.this.create(mods);
		}
		
	}

}
