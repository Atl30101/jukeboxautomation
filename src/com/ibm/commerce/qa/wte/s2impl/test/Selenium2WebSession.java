package com.ibm.commerce.qa.wte.s2impl.test;

/*
 *-----------------------------------------------------------------
 * Licensed Materials - Property of IBM
 *
 * 
 *
 * WebSphere Commerce
 *
 * (C) Copyright IBM Corp. 2012, 2015
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 *-----------------------------------------------------------------
 */

import static com.google.common.base.Preconditions.checkState;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.ibm.commerce.qa.wte.framework.page.Identifier;
import com.ibm.commerce.qa.wte.framework.page.PageFactory;
import com.ibm.commerce.qa.wte.framework.test.WebSession;
import com.ibm.commerce.qa.wte.framework.util.ConfigManager;
import com.ibm.commerce.qa.wte.framework.util.IBMCopyright;
import com.ibm.commerce.qa.wte.s2impl.util.Selenium2Util;

/**
 * Selenium 2 WebDriver implementation of a WebSession.
 * 
 * 
 *
 */
public class Selenium2WebSession implements WebSession
{
	/**
	 * Shared logger instance
	 */
	protected final Logger log;
	
	private final Provider<WebDriver> pWebDriver;
	
	private WebDriver webDriver;
	
	private final Provider<PageFactory> factory;
	
	private final ConfigManager configManager;
	
	private final Selenium2WebSessionState f_state;
	
	private static int staticHTMLPageNameCounter=0;
	
	private final Identifier previewFrame = Identifier.byId("previewFrame");
	
	private final Identifier previewFrame2 = Identifier.byXPath("//*[@id='previewFrames']/frame[2]");
	
	/**
	 * Creates new instance of Selenium2WebSession.
	 * 
	 * @param log
	 * @param pWebDriver
	 * @param factory
	 * @param configManager
	 */
	@Inject
	Selenium2WebSession(Logger log, 
			Provider<WebDriver> pWebDriver, 
			Provider<PageFactory> factory, 
			ConfigManager configManager)
	{
		this.log = log;
		this.pWebDriver = pWebDriver;
		this.webDriver = pWebDriver.get();
		this.factory = factory;
		this.configManager = configManager;
		
		f_state = new Selenium2WebSessionState();
		
		f_state.setWebDriverOpen(true);
	}
	
	
	/**
	 * Parses the URL from the WebDriver and extracts the domain (i.e. the fully
	 * qualified host name).
	 * 
	 * @return the domain name
	 */
	protected String getDomainFromWebDriver()
	{
		// Get the URL from the WebDriver
		URL url = null;
		try
		{
			url = new URL(this.webDriver.getCurrentUrl());
		}
		catch (MalformedURLException e)
		{
			throw new IllegalStateException("The URL from the webdriver '" + this.webDriver.getCurrentUrl() + "' cannot be parsed as a Java URL.", e);
		}
		
		return url.getHost();
	}

	/**
	 * Check if there are any cookies to process for the current domain the
	 * browser is showing. If so, add them then load the URL specified (which is
	 * usually the same URL as the WebDriver). Due to a limitation of WebDriver
	 * and browsers, you cannot add cookies unless the domain of the cookies
	 * matches the current page webdriver is on - thus loading the URL (again)
	 * will basically make the cookies take effect.
	 */
	private void processCookies(String urlToLoad)
	{
		// If there are no cookies to process, don't do anything
		if (f_state.getCookiesToProcess().size() == 0)
		{
			return;
		}
		
		String webDriverDomain = getDomainFromWebDriver();

		/*
		 * Check if any cookies to process match the webdriver domain. If so,
		 * add them to the webdriver and put them on a delete list.
		 */
		Set<Cookie> cookiesToRemove = new HashSet<Cookie>();
		for (Cookie cookie : f_state.getCookiesToProcess())
		{
			if (cookie.getDomain().equals(webDriverDomain))
			{
				/*
				 * For IE, the 2.25 WebDriver does not appear to add cookies
				 * properly (see
				 * http://code.google.com/p/selenium/issues/detail?id=1227). As
				 * a result, this will be done directly with Javascript.
				 */
				JavascriptExecutor js = (JavascriptExecutor)webDriver;
				String cookieScript = "document.cookie='" + cookie.getName() + "=" + cookie.getValue() + "; path=" + cookie.getPath() + "; domain=" + cookie.getDomain() + "'";
				js.executeScript(cookieScript);

//				this.webDriver.manage().addCookie(cookie);
				
				cookiesToRemove.add(cookie);
			}
		}
		
		/*
		 * If there were any cookies on the delete list, remove them now. This
		 * also means that cookies were added to the webdriver and thus it
		 * should load the URL specified.
		 */
		if (cookiesToRemove.size() > 0)
		{
			for (Cookie cookie : cookiesToRemove)
			{
				f_state.getCookiesToProcess().remove(cookie);
			}
			this.webDriver.get(urlToLoad);
			Selenium2Util.bypassSslError(configManager.getBrowserType(), webDriver);
		}
	}
	
	@Override
	public <T> T continueToPage(String url, Class<T> pageClass)
	{
		this.webDriver.get(url);
		Selenium2Util.bypassSslError(configManager.getBrowserType(), webDriver);
		
		processCookies(url);
		
		// Set the current main window
		f_state.setCurrentPageHandle(this.webDriver.getWindowHandle());
		
		f_state.setWebDriverOpen(true);
		
		return factory.get().createPage().build(pageClass);
	}

	@Override
	public <T> T startAtPage(String url, Class<T> pageClass)
	{
		// If a previous web page was opened, ensure child pages are closed.
		f_state.closeChildPages();

		// Only try to delete cookies if Selenium already opened a web page.
		if (f_state.getCurrentPageHandle() != null)
		{
			this.webDriver.manage().deleteAllCookies();
		}
		
		f_state.emptyCookiesToProcess();

		return continueToPage(url, pageClass);
	}

	@Override
	public <T> T startAtPageWithRefresh(String url, Class<T> pageClass)
	{
		T result = startAtPage(url, pageClass);
		
		result = refresh(pageClass);
		
		return result;
	}
	
	@Override
	public <T> T goBack(Class<T> pageClass)
	{
		this.webDriver.navigate().back();
		
		return factory.get().createPage().build(pageClass);
	}

	@Override
	public <T> T goForward(Class<T> pageClass)
	{
		this.webDriver.navigate().forward();
		
		return factory.get().createPage().build(pageClass);
	}

	@Override
	public <T> T refresh(Class<T> pageClass)
	{
		this.webDriver.navigate().refresh();
		
		return factory.get().createPage().build(pageClass);
	}
	
	@Override
	public <T> T refreshStorePreviewPage(Class<T> pageClass)
	{
		this.webDriver.navigate().refresh();

		try {
			factory.get().createQuery(previewFrame).withinPageLoadTimeout().selectFrame();
		} catch (Exception e) {
			factory.get().createQuery(previewFrame2).withinPageLoadTimeout().selectFrame();
		}
		
		return factory.get().createPage().build(pageClass);
	}

	@Override
	public <T> T startAtStorePreviewPage(String url, Class<T> pageClass)
	{
		// If a previous web page was opened, ensure child pages are closed
		f_state.closeChildPages();

		// Only try to delete cookies if Selenium already opened a web page
		if (f_state.getCurrentPageHandle() != null)
		{
			this.webDriver.manage().deleteAllCookies();
		}
		
		f_state.emptyCookiesToProcess();

		this.webDriver.get(url);
		Selenium2Util.bypassSslError(configManager.getBrowserType(), webDriver);
		
		// Set the current main window
		f_state.setCurrentPageHandle(this.webDriver.getWindowHandle());

		f_state.setWebDriverOpen(true);
		
	
		try {
			factory.get().createQuery(previewFrame).withinPageLoadTimeout().selectFrame();
		} catch (Exception e) {
			// following code is for debuging the preview frame time out issue
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			log.fine("DEBUG Begin: Preview window element timeout issue");
			log.fine(sw.getBuffer().toString());
			log.fine("DEBUG End: Preview window element timeout issue");
			
			factory.get().createQuery(previewFrame2).withinPageLoadTimeout().selectFrame();
		}
		return factory.get().createPage().build(pageClass);
	}
	
	@Override
	public void captureHTML(){
		if(configManager.isStaticHtmlCapture()){
			savePageSource();
		}
	}
	/**
	 * private method used to generate html of the current page and save it in a directory
	 */
	private void savePageSource(){
		String pageSource = this.webDriver.getPageSource();
		String staticHTMLDirectory = configManager.getStaticHtmlDir();
		
		pageSource = pageSource.replaceAll("<head>","");
		//$ANALYSIS-IGNORE
		pageSource = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>\n" + "<!--\n" +IBMCopyright.getLongCopyright() + "-->\n" + "<html  xmlns:wairole='http://www.w3.org/2005/01/wai-rdf/GUIRoleTaxonomy#' xmlns:waistate='http://www.w3.org/2005/07/aaa' lang='en' xml:lang='en'>\n <head>\n <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />\n"+pageSource+"</html>";
		
		File file;
		String fileName;
		File newStaticHTMLDirectory;
		java.io.FileOutputStream fop = null;
		
		try{
			newStaticHTMLDirectory = new File(staticHTMLDirectory);
			if(!newStaticHTMLDirectory.exists()){
				newStaticHTMLDirectory.mkdirs();
			}
			  fileName= staticHTMLDirectory+this.webDriver.getTitle().replaceAll(" ", "").replaceAll(":", "").replaceAll("\"","").replaceAll("'", "").replaceAll("/","")+".html";
	    	  file = new File(fileName);
	    	  fileName = fileName.substring(0,fileName.length()-5);
	    	  if(file.exists()){
	    		  staticHTMLPageNameCounter++;
	    		 fileName= fileName+"_"+staticHTMLPageNameCounter+".html";
	    		 file = new File(fileName);
	    	  }
	    	  file.createNewFile();
         	  fop =new java.io.FileOutputStream(file);
	          fop.write(pageSource.getBytes("UTF-8"));
	          fop.flush();
	          
	         
		}
	    catch(IOException ie){
	      ie.printStackTrace();
	    }
	    finally 
	    {
	    	try {
	    		if (fop != null)
				{
	    			fop.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
	    }
	    
	}
	@Override
	public void takeScreenshotOnStep(String testCaseName){
		if(configManager.isScreenShotOnStep()){
			takeScreenshot(configManager.getScreenShotPath(), testCaseName);
		}
	}
	
	@Override
	public void takeScreenshotOnFail(String testCaseName){
		if(configManager.isScreenShotOnFail()){
			takeScreenshot(configManager.getScreenShotPath(), testCaseName);
		}
	}
	
	private void takeScreenshot(String screenShotPath, String... testCaseName) {
		try {
			//takes a screen shot
			File scrFile = ((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE);
			String screenshotFileName;
			if(testCaseName.length != 0 ){
			//copy srcFile to local system
				 screenshotFileName = screenShotPath + "" + testCaseName[0] + "-Screenshot-" + System.currentTimeMillis() + ".png";
			}
			else {
				 screenshotFileName = screenShotPath + "Screenshot-" + System.currentTimeMillis() + ".png";
				
			}
			try {
				FileUtils.copyFile(scrFile, new File(screenshotFileName));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (WebDriverException e) {
			log.warning("unable to take a screen shot, selenium error: " + e);
		}

	}
	
	/**
	 * Gets the underlying {@link WebDriver} instance. 
	 * <p/>
	 * This method is not exposed to the {@link WebSession} interface, and only
	 * should be used internally. 
	 * @return backing WebDriver instance
	 */
	public WebDriver getS2WebDriver() {
		checkState(!f_state.isClosed(), "WebDriver is not running");
		
		return webDriver;
	}
	
	@Override
	public WebSession.State state() {
		return this.f_state;
	}
	
	/**
	 * Resets the {@link WebDriver} instance to be a new one
	 */
	protected void closeDriver() {
		webDriver = null;
	}
	
	@Override
	public void dojoWidgetSelect(String id, String value) {
		
		JavascriptExecutor js = (JavascriptExecutor)webDriver;
		js.executeScript("dijit.byId('"+ id +"').set('value', '" + value + "')");
		
	}
	private class Selenium2WebSessionState implements WebSession.State {
		/**
		 * Holds a handle to the actual page the session was asked to start with.
		 */
		private String currentMainWebPageHandle;
		
		private Set<Cookie> cookiesToProcess = Collections.emptySet();
		
		private boolean webDriverOpen;
		
		public Selenium2WebSessionState() {
			// nothing to do!
		}
		
		void setCurrentPageHandle(String newHandle) {
			currentMainWebPageHandle = newHandle;
		}
		
		String getCurrentPageHandle() {
			return currentMainWebPageHandle;
		}
		
		/**
		 * @return the cookiesToProcess
		 */
		final Set<Cookie> getCookiesToProcess() {
			return cookiesToProcess;
		}

		/**
		 * Set the cookiesToProcess to be the empty set
		 */
		final void emptyCookiesToProcess() {
			this.cookiesToProcess = Collections.emptySet();
		}

		/**
		 * @param webDriverOpen the webDriverOpen to set
		 */
		final void setWebDriverOpen(boolean webDriverOpen) {
			this.webDriverOpen = webDriverOpen;
		}

		@Override
		public StateData saveState()
		{
			/*
			 * Get the list of cookies from the WebDriver. Note that they are only
			 * for the current domain, but that's all we can get from WebDriver at
			 * this time.
			 */
			Set<Cookie> webDriverCookies = ImmutableSet.copyOf(getS2WebDriver().manage().getCookies());

			// Create the cookies set to save, as it may be different than the original set
			Set<Cookie> cookiesToSave = Sets.newHashSetWithExpectedSize(webDriverCookies.size());
			
			/*
			 * As of this writing, IE WebDriver 2.25 does not seem to save the
			 * domain for cookies, so we need to do that manually based on the
			 * current WebDriver page domain if encountered.
			 */
			String webDriverDomain = getDomainFromWebDriver();
			boolean missingDomain = false;
			for (Cookie cookie : webDriverCookies)
			{
				if (cookie.getDomain() == null)
				{
					cookie = new Cookie(
							cookie.getName(),
							cookie.getValue(),
							webDriverDomain,
							cookie.getPath(),
							cookie.getExpiry(),
							cookie.isSecure());
					missingDomain = true;
				}

				cookiesToSave.add(cookie);
			}
			
			if (missingDomain)
			{
				log.warning("At least one cookie was missing the domain value. The domain '" + webDriverDomain + "' was used.");
			}
			
			return new Selenium2WebSessionStateData(cookiesToSave);
		}
		
		@Override
		public void loadState(StateData state)
		{
			if ( !(state instanceof Selenium2WebSessionStateData) )
			{
				throw new IllegalArgumentException("Only a state object created by the Selenium 2 implementation can be used.");
			}
			
			if (this.currentMainWebPageHandle != null)
			{
				getS2WebDriver().manage().deleteAllCookies();
			}
			
			cookiesToProcess = ((Selenium2WebSessionStateData)state).getCookies();
		}
		
		/**
		 * Closes all open web windows with the exception of the current main
		 * window. This method does nothing if there is no current main window.
		 */
		void closeChildPages()
		{
			if (getCurrentPageHandle() == null)
			{
				return;
			}
			
			Set<String> windowHandles = getS2WebDriver().getWindowHandles();
			for (String handle : windowHandles)
			{
				if (!handle.equals(getCurrentPageHandle()))
				{
					getS2WebDriver().switchTo().window(handle);
					log.warning("Found a window with title '" + getS2WebDriver().getTitle() + "' still open. Will attempt to close.");
					getS2WebDriver().close();
				}
			}
			
			getS2WebDriver().switchTo().window(getCurrentPageHandle());
		}

		@Override
		public void shutdown()
		{
			if (!isClosed())
			{
				try 
				{
					closeChildPages();
					
					/*
					 * Close was not working properly with IE and the IEDriverServer, it
					 * would just hang.
					 */
					if ( !(getS2WebDriver() instanceof InternetExplorerDriver) )
					{
						getS2WebDriver().close();
					}
					
					log.log(Level.INFO, "Dispose of the web driver (quit).");
					getS2WebDriver().quit();
	
					this.currentMainWebPageHandle = null;
					
					closeDriver();
					
					webDriverOpen = false;
				}
				catch (RuntimeException e)
				{
					log.log(Level.WARNING, "The session was requested to close but an exception occurred during the process.", e);
				}
			}
			else
			{
				log.warning("The session was requested to close when it already was closed.");
			}
		}
		
		@Override
		public boolean isClosed() {
			return !webDriverOpen;
		}

		
		@Override
		public void reset() {
			if (!isClosed()) {
				shutdown();
			}
			
			webDriver = pWebDriver.get();
			
			webDriverOpen = true;
		}

		@Override
		public void resetWithState(StateData state) {
			reset();
			
			loadState(state);
		}
		
		@Override
		public void resetWithCurrentState() {
			StateData currState = saveState();
			resetWithState(currState);
		}
	
		
		/**
		 *  Selenium implementation of WebSessionState
		 */
		private class Selenium2WebSessionStateData implements StateData
		{
			private final Set<Cookie> cookies;

			/**
			 * @param cookies
			 */
			Selenium2WebSessionStateData(Set<Cookie> cookies)
			{
				this.cookies = cookies;
			}

			/**
			 * @return the cookies
			 */
			Set<Cookie> getCookies()
			{
				return cookies;
			}
			
		}
	}

	
	
}
